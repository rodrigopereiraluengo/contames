var toFormSeachOnScroll;
$.fn.extend({
	formSearch: function(settings) {
		
		// Default Settings
		settings = $.extend({
			multipleSelectRows: true,
			multiple: false,
			selectRow: true,
			deleteMessageConfirm: i18n[i18n.locale]['formSearch.delete.confirm'],
			deleteMessageConfirmP: i18n[i18n.locale]['formSearch.delete.confirm.p'],
			deleteMessageSuccess: i18n[i18n.locale]['formSearch.delete.success'],
			deleteMessageSuccessP: i18n[i18n.locale]['formSearch.delete.success.p']
		}, settings);
		
		return this.each(function() {
			
			var $form = $(this);
			$form.data('selected', $form.find("tbody tr.active").map(function() {
				return $(this).data('id') || $(this).data();
			})).trigger('row.select');
			
			// Verifica se eh lazy pagination
			
			$(window).on('scroll.formSearch', $.proxy(function() {
				if($(this).find('.search-table .lazipag').length) {
					window.clearTimeout(toFormSeachOnScroll);
					toFormSeachOnScroll = window.setTimeout($.proxy(function() {
						if($(this).find('.search-table .lazipag').visible()) {
							$(this).find('.search-table .lazipag').click().data('callSubmit', true);
						} else {
							$(this).trigger('scrollEnd');
						}
					}, this), 500);
				}
			}, this));
			
			$form.on('submit', function(e, currentTarget) {
				var action = $form.attr('action');
				if(typeof currentTarget == 'undefined') {
					var arrParam = $form.attr('action').split('?');
					action = arrParam[0];
					var params = $form.attr('action').toQueryParams();
					params.page = 1;
					if(arrParam.length > 1) {
						action +='?' + decodeURIComponent($.param(params));
					}
				}
				
				loading(true);
				$.ajax({
					url: action,
					type: $form.attr('method') || 'GET',
					data: $form.serialize() + '&isTable=true',
					success: function(data) {
						if(typeof data == 'string') {
							
							if(currentTarget && $(currentTarget).hasClass('lazipag')) {
								
								var $result = $(data);
								$form.find('.search-table tbody').append($result.find('tbody').html());
								$form.find('.search-table .summary').replaceWith($result.find('.summary'));
								$form.find('.search-table .pagination').replaceWith($result.find('.pagination'));
								$form.trigger('scroll');							
							} else {
								$form.find('.search-table').html(data);
							}
							$form.trigger('search.success');
							
						} else {
							error(data, $form);
						}
						
					}
				}).always(function() {
					loading(false);
				});
				
				return false;
			});
			
			$form.on('click', '.bt-new', function() { $form.trigger('new.click'); });
			$form.on('click', '.bt-edit', function() { $form.trigger('edit.click'); });
			$form.on('click', '.bt-delete', function() { $form.trigger('delete.click'); });
			$form.on('click', 'button[type=submit]', function() {
				if($(this).find('.fa-search').length) {
					$form.data('selected', []);
				}
			});
			$form.on('row.select', function() {
				
				$('.bt-delete', $form).prop('disabled', !$form.data('selected').length);
				$('.bt-edit', $form).prop('disabled', $form.data('selected').length != 1);
				
				if($(this).closest('.cm-modal-data-table').find('.modal-footer .bt-ok').length) {
					
					if(settings.multiple) {
						$(this).closest('.cm-modal-data-table').find('.modal-footer .bt-ok').prop('disabled', !$form.data('selected').length);
					} else {
						if($form.data('selected').length == 1) {
							$(this).closest('.cm-modal-data-table').find('.modal-footer .bt-ok').prop('disabled', false);
						} else {
							$(this).closest('.cm-modal-data-table').find('.modal-footer .bt-ok').prop('disabled', true);
						}
					}
					
				}
								
			}).trigger('row.select');
			
			$form.on("click", ".search-table thead a[href][href!='#'], .pagination a[href][href!='#']", function(e) {
				if(!$(this).data('callSubmit')) {
					$form.attr('action', $(this).attr('href')).trigger('formSearch.' + ($(e.currentTarget).closest('thead').length ? 'order' : 'pagination'), [e.currentTarget]).trigger('submit', [e.currentTarget]);
				}
				e.preventDefault();
			}).on('formSearch.order', function() {
				if($(this).find('.lazipag').length) {
					var arrParam = $(this).attr('action').split('?');
					
					action = arrParam[0];
					var params = $(this).attr('action').toQueryParams();
					params.page = 1;
					if(arrParam.length > 1) {
						action +='?' + decodeURIComponent($.param(params));
					}
					$(this).attr('action', action);
				}
			});
			
			$form.on("dblclick", "tbody tr", function(e) {
				var $row = $(e.currentTarget);
				if(!$row.is('.cm-tr-total')) {
					$row.closest('tbody').find('tr').removeClass('active');
					$row.addClass('active');
					$form.data('selected', [$row.data('id') || $row.data()]);
					
					if($form.closest('.cm-modal-data-table').find('.bt-ok').length && !settings.multiple) {
						$form.closest('.cm-modal-data-table').find('.bt-ok').click();
						return;
					}
					
					$('.bt-edit', $form).trigger('edit.click');
					$form.trigger('row.select');
					
					
				}
				e.stopImmediatePropagation();
			});
			
			$form.on("click", "tbody tr", function(e) {
				if(settings.selectRow) {
					var $row = $(e.currentTarget);
						if(!$row.is('.cm-tr-total')) {
						if(!settings.multipleSelectRows) {
							$form.data('selected', []);
						}
						
						if($row.is('.active')) {
							$row.removeClass('active lay-validator-msgs');
							$form.data('selected', $.grep($form.data('selected'), function(id) { return id != ($row.data('id') || $row.data()); }));
						} else {
							$row.addClass('active');
							$form.data('selected').push(($row.data('id') || $row.data()));
						}
						$form.trigger('row.select', [$row]);
					}
				}
			});
			
			$form.on("click dblclick", "tbody tr button, tbody tr a", function(e) {
				e.stopPropagation();
			});
			
			$form.on('search.success', function() {
				if($form.data('selected').length) {
					$form.find('tbody tr').filter(function() { 
						return $.inArray($(this).data('id') || $(this).data(), $form.data('selected')) != -1; 
					}).addClass('active');
				}
				$form.trigger('row.select');
			});
			
			$form.on('new.click', function() {
				loading(true);
				$.ajax({
					url: $form.data('form'),
					cache: false,
					success: function(result) {
						if(typeof result == 'string') {
							var $element = $(result);
							$('body').append($element);
							$element.on('success', function() {
								$form.submit();
							});
							$form.trigger('new.success', [$element]);
						} else {
							error(result, $form);
						}
					}
				}).always(function() {
					loading(false);
				});
				
			});
			
			$form.on('edit.click', function() {
				loading(true);
				$.ajax({
					url: $form.data('edit') || $form.data('form'),
					data: {id: $form.data('selected')[0]},
					cache: false,
					success: function(result) {
						if(typeof result == 'string') {
							var $element = $(result);
							$('body').append($element);
							$element.on('success', function() {
								$form.submit();
							});
							$form.trigger('edit.success', [$element]);
						} else {
							error(result);
						}
					}
				}).always(function() {
					loading(false);
				});
				
			});
			
			$form.on('delete.click', function() {
				
				var message = settings.deleteMessageConfirm;
				if($form.data('selected').length > 1) {
					message = settings.deleteMessageConfirmP.template({"0": $form.data('selected').length});
				}
				
				msgConfirm(message, function() {
					loading(true);
					var data = {};
					$.each($form.data('selected'), function(i) {
						data['ids[' + i + ']'] = this;
					});
					
					$.ajax({
						url: $form.data('delete'),
						type: 'POST',
						data: data,
						dataType: 'text',
						success: function(data) {
							eval(data);
							if(data.toLowerCase().indexOf('success') != -1) {
								
								message =  settings.deleteMessageSuccess;
								if($form.data('selected').length > 1) {
									message = settings.deleteMessageSuccessP.template({"0": $form.data('selected').length});
								}
								
								msgSuccess(message);
								$form.data('selected', []).trigger('row.select').submit();
							}
						}
					}).always(function() {
						loading(false);
					});
				
				});
			});
								
		});
	},
	autoCompleteEditable: function(params) {
		return this.each(function() {
			
			params = $.extend(true, {
        		form: $.proxy(function() { return $(this).data('form'); }, this),
        		search: $.proxy(function() { return $(this).data('search'); }, this),
        		urlAutoComplete: $.proxy(function() { return $(this).data('urlAutoComplete'); }, this),
        		id: $.proxy(function() { return $(this).data('val') || ''; }, this),
        		reload: $.proxy(function() { return $(this).data('reload') || ''; }, this),
        		onSuccess: $.proxy(function(){return 'setValue';}, this)
        	}, params);
			
			$(this).data('params', params);
			
			var $hidden = $('<input type="hidden" name="' + $(this).attr('name') + '" value="' + ($(this).data('val') || '') + '" />');
			
			$(this).removeAttr('name').before($hidden);
			
			$(this).simpleAutoComplete(params.urlAutoComplete());
						
			$(this).on('autoComplete.select', $.proxy(function(e, $li) {
				$(this).prev().val($li.data('id'));
				$(this).data('val', $li.data('id')).removeData('newVal').change();
			}, this)).on('autoComplete.clean', $.proxy(function() {
				console.log(this);
				try {
					$(this).val('').prev().val('');
				} catch(e) {  }
				
				try {
					$(this).removeData('val').removeAttr('data-val').change();
				} catch(e) {  }
			
			}, this)).on('keyup.autoComplete.newVal', $.proxy(function() {
				try {
					$(this).removeData('val').removeAttr('data-val').data('newVal', $(this).val());
				} catch(e) {
					
				}
			}, this)).bind('keydown', 'return', $.proxy(function() {
				if($(this).data('newVal') && $('.autoComplete-notFound').length) {
					$(this).parent().find('.bt-new').click();
					return false;
				}
				if($('.autocomplete .sel').length) {
					$('.autocomplete .sel').click();
					return false;
				}
			}, this));
			
			$(this).next().find('.bt-new').click($.proxy(function(e) {
    			loading(true);
    			$.ajax({
    				url: contextPath + $(this).data('params').form(),
    				data: {val: $(this).data('val') ? '' : $(this).data('newVal')},
    				success: $.proxy(function(result) {
    					if(typeof result == 'string') {
	    					var $dialog = $(result);
	        				$('body').append($dialog);
	        				$dialog.on('success', $.proxy(function(e, data) { 
	        					$(this).trigger($(this).data('params').onSuccess(), [data]);
	        				}, this));
	        				$(this).trigger('newLoad', [$dialog]);
    					} else {
    						error(result);
    					}
    					$(this).removeData('newVal').val('');
    				}, this)
    			}).always(function() {
    				loading(false);
    			});
    			
    			$(this).trigger('new');
    		}, this));
    		
    		
    		$(this).on('setValue', function(e, val) {
    			$(this).prev().val(val.id);
    			$(this).data('val', val.id);
    		});
			
    		
    		$(this).change(function() {
    			$(this).next().find('.bt-edit').prop('disabled', !$(this).data('val'));
    		}).change();
    		
    		
    		$(this).next().find('.bt-edit').click($.proxy(function() {
    			loading(true);
    			$.ajax({
    				url: contextPath + $(this).data('params').form(),
    				data: {id: $(this).data('params').id()},
    				success: $.proxy(function(result) {
    					if(typeof result == 'string') {
    						$(this).trigger('editLoad');
    	    				var $dialog = $(result);
    	    				$('body').append($dialog);
    	    				
    	    				$dialog.on('success', $.proxy(function(e, data) {
    	    					$(this).trigger($(this).data('params').onSuccess(), [data]);
    	    				}, this));
    	    				$(this).trigger('editLoad', [$dialog]);
        				} else {
        					error(result);
        				}
    				}, this)
    			}).always(function() {
    				loading(false);
    			});
    			
    			$(this).trigger('edit');
    		}, this));
    		
    		
    		$(this).next().find('.bt-search').click($.proxy(function() {
    			loading(true);
    			$.ajax({
    				url: contextPath + $(this).data('params').search(),
    				data: {id: $(this).data('params').id(), dialogFootOk: 'selectSearchItem(this, $(\'#' + $(this).attr('id') + '\'))'},
    				success: $.proxy(function(result) {
    					if(typeof result == 'string') {
    						var $dialog = $(result);
    	    				$('body').append($dialog);
    	    				
    	    				$dialog.on('close', $.proxy(function(e, data) {
    	    					$(this).trigger('reload', [data]);
    	    				}, this));
    	    				
    					} else {
    						error(result);
    					}
    				}, this)
    			}).always(function() {
    				loading(false);
    			});
    			
    			$(this).trigger('search');
    		}, this));
    		
    		$(this).on('reload', function(e, data) {
    			loading(true);
    			var id = $(this).data('params').id();
    			if(data && data.id) {
    				id = data.id;
    			}
    			$.ajax({
    				url: $(this).data('params').reload(),
    				data: { id: id },
    				dataType: 'text',
    				success: $.proxy(function(result) {
    					if(result == '') {
    						$(this).val('').data('val', '').change();
    						$(this).prev().val('');
    					} else {
    						$(this).trigger('setValue', [JSON.parse(result)]);
    					}
    				}, this)
    			}).always(function() {
    				loading(false);
    			});
    		});
			
		});
	},
	setHidden: function(name, val) {
		return this.each(function() {
			
			if($('input[name="' + name + '"]', this).length) {
				$('input[name="' + name + '"]', this).val(val);
			} else {
				$('<input type="hidden" name="' + name + '" value="' + val + '"/>').appendTo(this);
			}
		});
	},
	getHiddenVal: function(name) {
		return $(this).find('input[name="' + name + '"]').val();
	}
});


$(document).on('show.bs.modal', '.modal', function(e) {
	var $this = $(e.target);
	
	if($this.data('dialogDetalhe')) {
		
		var $layplus = $this.find('div[data-lay-detalhe=plus]');
		var $layminus = $this.find('div[data-lay-detalhe=minus]');
		
		$this.on('dialog.detalhe.plus', function() {
			
			$layplus.find('[data-input-detalhe]').each(function() {
				if(!$(this).children().length) {
					$(this).append($layminus.find('[data-input-detalhe=' + $(this).data('inputDetalhe') + ']').children());
				}
			});
			
			$layplus.show();
			$layminus.hide();
			
		}).on('dialog.detalhe.minus', function() {
			
			$layminus.find('[data-input-detalhe]').each(function() {
				if(!$(this).children().length) {
					$(this).append($layplus.find('[data-input-detalhe=' + $(this).data('inputDetalhe') + ']').children());
				}
			});
			
			$layminus.show();
			$layplus.hide();
		
		});
		
		var $bt = $this.find('.btn-dialog-detalhe');
		
		if($this.data('dialogDetalhe') == 'minus') {
			$bt.find('span').addClass('glyphicon-plus');
			$this.trigger('dialog.detalhe.minus');
		} else {
			$bt.find('span').addClass('glyphicon-minus');
			$this.trigger('dialog.detalhe.plus');
		}
		
		$bt.click(function() {
			if($this.data('dialogDetalhe') == 'minus') {
				$this.data('dialogDetalhe', 'plus');
				$bt.find('span').removeClass('glyphicon-plus').addClass('glyphicon-minus');
				$this.trigger('dialog.detalhe.plus');
			} else {
				$this.data('dialogDetalhe', 'minus');
				$bt.find('span').removeClass('glyphicon-minus').addClass('glyphicon-plus');
				$this.trigger('dialog.detalhe.minus');
			}
		});
		
	} else {
		$this.find('.btn-dialog-detalhe').hide();
	}
});


function selectSearchItem(self, $InputAutoCompleteEditable) {
	
	if($(self).closest('.cm-modal-data-table').length) {
		
		var $form = $(self).closest('.cm-modal-data-table').find('form');
		
		
		$InputAutoCompleteEditable.trigger('setValue', [{id: $form.data('selected')[0]}]).removeData('newVal').change();
		
		$(self).closest('.cm-modal-data-table').modal('hide');
		
	}
	
}