/**
 * Remove do string os caracteres de acentuacao, espacos e caracteres especiais 
 * @return String
 */
String.prototype.charClean = function() 
{
	var chars 			= [231, 225, 224, 227, 226, 228, 233, 232, 234, 235, 237, 236, 238, 239, 243, 242, 245, 244, 246, 250, 249, 251, 252, 32, 92, 47];
	var charsReplace	= ['c', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', '_', '_', '_'];
	var valor			= this;
	
	for(var i = 0; i < chars.length; i++) 
	{
		while(valor.indexOf(String.fromCharCode(chars[i])) > -1) 
		{
			valor = valor.replace(String.fromCharCode(chars[i]), charsReplace[i]);
		}
	}
	
	return valor;//.replace(/[^a-zA-Z0-9-]/mg, '_');
};


String.prototype.unformat = function() {
	var text = this;
	if(isNaN(text)){
		text = eval('text.replace(/\\' + i18n[i18n.locale].decimalGroup + '/g, "").replace(/\\' + i18n[i18n.locale].decimalPoint + '/g, ".");');
	}
	return text.toString();
};




String.prototype.repeat = function(n, s) {
	n= parseInt(n) || 1;
    return (s ? this : '') + Array(n+1).join(s?s:this);
};


String.prototype.template = function(arg0) {
	return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = arg0[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
    });
	
};

Number.prototype.formatDecimal = function(decimalPlaces) {
	if(decimalPlaces == undefined) {
		 decimalPlaces = 2;
	}
	return $.formatNumber(this, {format: "#,##0.".repeat(decimalPlaces, "0")});
};

Array.prototype.insert = function (index, item) {
	this.splice(index, 0, item);
};