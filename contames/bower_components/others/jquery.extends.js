$.fn.extend({
	constrainNormal:function(){
		return this.each(function(){
			$(this).blur(function(){
				var text = $(this).val();
				var reg = new RegExp("[^A-Za-z0-9\\s\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF]/g");
				var result = text.replace(reg,"");
				$(this).val(result);
			});
			$(this).unconstrain().constrain({allow:{regex:"^[A-Za-z0-9\\s\u00B8-\u00B9\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF]+$"}});
		});
	},
	constrainAbsolute:function(){
		return this.each(function(){
			this.onpaste = function(){
				var field = this;
				setTimeout(function(){
					var text = $(field).val();
					var reg = new RegExp("[^A-Za-z0-9-_]/g");
					var result = text.replace(reg,'');
					$(field).val(result);
				},10);
			};
			$(this).unconstrain().constrain({allow:{regex:"[A-Za-z0-9-_]"}});
		});
	},
	constrainNumber:function(){
		return this.each(function(){
			$(this).bind('blur', function(){
				var field = this;
				setTimeout(function(){
					var text = $(field).val();
					var reg = new RegExp("[^\\d\\-]/g");
					var result = text.replace(reg,'');
					$(field).val(result);
				},10);
			});
			$(this).unconstrain().constrain({allow: {regex:"[\\d\\-]"}, limit:{"-":1}});
		});
	},
	unconstrain:function(){
		return this.each(function(){
			this.onpaste = null;
			$(this).unbind("keypress.constrain");
		});
	},

	 formatDecimal: function(decimalPlaces)
	 {
		 if(decimalPlaces == undefined)
		 {
			 decimalPlaces = 2;
		 }
		 return this.each(function()
		 {
			 if($(this).data('formatDecimal')) {
				 $(this).unformatDecimal();
			 }
			 $(this).data('formatDecimal', true);
			 this.onpaste = function(){
				 var field = this;
				 setTimeout(function(){
					 var text = $(field).val();
					 var reg = new RegExp("[^\\d\\" + i18n[i18n.locale].decimalPoint + "-]/g");
					 var result = text.replace(reg,'');
					 $(field).val(result);
				 }, 10);
			};
			
			var limitDecimalPoint = {};
			limitDecimalPoint[i18n[i18n.locale]['decimalPoint']] = 1;
			limitDecimalPoint['-'] = 1;
			
			$(this).unformat().constrain({
				allow: {regex:"[\\d\\" + i18n[i18n.locale]['decimalPoint'] + "-]"},
				limit: limitDecimalPoint
			})
			.addClass("text-right")
			.data({
				thousands: i18n[i18n.locale]['decimalGroup'], 
				decimal: i18n[i18n.locale]['decimalPoint'],
				precision: decimalPlaces
			}).bind('blur.formatDecimal', function(){
				var val = $(this).val();
				if(val){
					var number = $.parseNumber(val.unformat(), {format: "#,##0.".repeat(decimalPlaces, "0")});
					$(this).val($.formatNumber(number, {format: "#,##0.".repeat(decimalPlaces, "0")}));
				}
			})
			.addClass("text-right formatDecimal")
			.trigger('blur.formatDecimal');
			this.maxLength = 20;
			
			if(!$(this).data('calculator')) {
				
				$(this).data('calculator', true);
				if($(this).is('input') && $(this).is(':visible')) {
					var htmlButton = '<span class="input-group-btn">';
					htmlButton += '<button class="btn btn-default" type="button"';
					if($(this).prop('disabled')) {
						htmlButton += ' disabled="disabled"';
					}
					htmlButton += '><i class="fa fa-calculator"></i>';
					htmlButton += '</button>';
					htmlButton += '</span>';
					
					$(this).on('propDisabledChange', function() {
						
						$(this).parent().find('button.btn').prop('disabled', $(this).prop('disabled'));
					
						if(!$(this).data('formatDecimal.calculator') && !$(this).prop('disabled')) {
							
							$(this).calculator({
								showAnim: null,
								buttonText: '',
								showOn: 'button',
								decimalChar: i18n.get('decimalPoint'),
								regionalOptions: i18n.locale,
								constrainInput: false,
								precision: decimalPlaces,
								onClose: function(value, inst) { 
									inst.elem.trigger('blur.formatDecimal').trigger('change');
							    }
							})
							.on('keyup.formatDecimal.fontColor', function() {
								var val = $(this).val().unformat().toFloat();
								if(typeof $(this).closest('form').data('tipo') == 'undefined') {
									$(this).removeClass('font-red font-blue').addClass((val < 0) ? 'font-red' : 'font-blue');
								} else {
									var tipo = $(this).closest('form').data('tipo');
									var fontRed = tipo == 'DES' || tipo == 'CPR' ? 'font-blue' : 'font-red';
									var fontBlue = tipo == 'DES' || tipo == 'CPR' ? 'font-red' : 'font-blue';
									$(this).removeClass('font-red font-blue').addClass((val < 0) ? fontRed : fontBlue);
								}
							})
							.trigger('keyup.formatDecimal.fontColor')
							.data('formatDecimal.calculator', true);
							
							$(this).parent().find('button.btn').click($.proxy(function() {
								$.calculator.show(this);
							}, this));
							
						}
					
					
					}).wrap('<div class="input-group"></div>').after(htmlButton).trigger('propDisabledChange');
				}
			}
			
		});
	},
	unformatDecimal:function(){
		return this.each(function(){
			if($(this).data("maxlength")) this.maxLength = $(this).data("maxlength");
			$(this).unconstrain().unbind('blur.formatDecimal').removeClass("text-right").removeData("maxlength");
		});
	},
	formatInteger:function(){
		return this.each(function(){
			this.onpaste = function(){
				var field = this;
				setTimeout(function(){
					var text = $(field).val();
					var reg = new RegExp("[^\\d\\-]/g");
					var result = text.replace(reg,'');
					$(field).val(result);
				},10);
			};
			
			$(this).unformat().constrain({
				allow: {regex:"[\\d\\-]"}
				,limit:{"-":1}
			})
			.bind('blur.formatInteger', function(){
				var val = $(this).val();
				if(val){
					var number = $.parseNumber(val, {format: "#,##0", locale: i18n.locale});
					$(this).val($.formatNumber(number, {format: "#,##0", locale: i18n.locale}));
				}
			})
			.addClass("text-right")
			.trigger('blur.formatInteger');
			this.maxLength = 20;
		});
	},
	unformatInteger:function(){
		return this.each(function(){
			$(this).unconstrain().unbind('blur.formatInteger').removeClass("alignRight").removeData("maxlength");
		});
	},
	unformat:function(){
		return this.each(function(){
			$(this).unformatDecimal().unformatInteger();
		});
	},
	resetValidator: function() {
        return this.each(function() {
            $('.lay-validator-msgs', this).find('.validator-msgs').removeData('msg').removeClass('validator-msgs').end().removeClass('lay-validator-msgs has-error has-feedback');    
        });
    },
    toFocus: function(time) {
        return this.each(function() {
            var $this = $(this);
            window.clearTimeout(atFocusTimeout);
            atFocusTimeout = window.setTimeout(function() { $this.focus(); }, time ? time : 500);
        });
    },
    toSelect: function(time) {
        return this.each(function() {
            var $this = $(this);
            window.clearTimeout(atFocusTimeout);
            atFocusTimeout = window.setTimeout(function() { $this.select(); }, time ? time : 500);
        });
    },
    menucollapse: function() {
        
        return this.each(function() {
            $menu = $(this);
            
            $menu.find('.bt-scrolldown').mousedown(function() {
                $menu.find('.bt-scrollup').show();
                $menu.animate({scrollTop: '+=120px'}, {done: function() {
                    if(($menu.scrollTop() + $menu.innerHeight()) == $menu.get(0).scrollHeight) {
                        $menu.find('.bt-scrolldown').hide();
                    }
                }});
                
            });
            
            $menu.find('.bt-scrollup').mousedown(function() {
                $menu.find('.bt-scrolldown').show();
                $menu.animate({scrollTop: '-=120px'}, {done: function() {
                    if($menu.scrollTop() == 0) {
                        $menu.find('.bt-scrollup').hide();
                    }
                }});
            });
            
            $(window).on('resize', function() {
                $menu.find('.bt-scrollup, .bt-scrolldown').show();
            });
            
        });
        
    },
    hasScrollBar: function() {
        
        var hasScrollBar = {}, e = this.get(0);
        hasScrollBar.vertical = (e.scrollHeight > e.clientHeight) ? true : false;
        hasScrollBar.horizontal = (e.scrollWidth > e.clientWidth) ? true : false;
        return hasScrollBar;
        
    },
    resetValidator: function(target) {
    	return this.each(function() {
    		var $_lay = (typeof target == 'undefined') ? $(this).closest('.lay-validator-msgs') : $('.lay-validator-msgs', target);
    		$_lay.find('.validator-msgs').popover('destroy').removeData('msg').removeClass('validator-msgs').end().removeClass('lay-validator-msgs has-error has-feedback');
    	});
    },
    toFocusEnd: function() {
    	return this.each(function(){
    		$(this).focus();

            // If this function exists...
            if (this.setSelectionRange) {
	            // ... then use it
	            // (Doesn't work in IE)
	
	            // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
	            var len = $(this).val().length * 2;
	            this.setSelectionRange(len, len);
            } else {
	            // ... otherwise replace the contents with itself
	            // (Doesn't work in Google Chrome)
	            $(this).val($(this).val());
            }

            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;
    	});
    },
    fileSize: function() {
    	var size = 0;
    	this.each(function() {
    		if($(this).is(':file')) {
	    		if(this.files) {
	    			for(var i = 0; i < this.files.length; i++) {
	    				size += (this.files[i].size || this.files[i].fileSize); 
	    			}
	    		} else {
	    			try {
	    				var fso = new ActiveXObject("Scripting.FileSystemObject");
	    				size += fso.getFile($(this).val()).size;
	    			} catch(e) {  }
	    		}
	    	}
    	});
    	
    	return size;
    },
    inputCalendar: function() {
    	return this.each(function() {
    		
    		var $input 	= $(this).closest('.input-group').find('input');
    		var $button = $(this).closest('.input-group').find('button');
    		
    		$input.datepicker({
    			language: "pt-BR", 
    			orientation: "top auto", 
    			todayHighlight: true,
    			autoclose: true,
    			showOnFocus: false,
    			format: i18n.get('datepickerPatternDate')
    		}).change(function() {
    			
    			var len = $(this).val().length;
    			
    			if(len == 1 || len == 2) { // Day
					var data = moment($(this).val(), i18n.get('patternDay_' + len));
					$(this).val(data.format(i18n.get('momPatternDate'))).trigger('keyup');
				} else if(len == 4 || len == 5) { // Day and Month
					var data = moment($(this).val(), i18n.get('patternDayMonth_' + len));
					$(this).val(data.format(i18n.get('momPatternDate'))).trigger('keyup');
				} else if(len == 7) { // Year
					var data = moment($(this).val(), i18n.get('momPatternDate'));
					$(this).val(data.format(i18n.get('momPatternDate'))).trigger('keyup');
				}
    			
    		}).attr('placeholder', i18n.get('placeHolderDate')).setMask(i18n.get('maskDate')).on('click focus', function(){
    			$(this).select();
    		});
    		
    		$button.click(function() {
    			$input.datepicker('show');
    			window.setTimeout(function(){ $input.focus(); }, 200);
    		});
    		
    	});
    },
    
    inputDateTime: function(){
    	return this.each(function() {
    		
    		var $input 	= $(this).closest('.input-group').find('input');
    		var $button = $(this).closest('.input-group').find('button');
    		
    		$input
    			.datetimepicker({locale: i18n.locale, format: 'DD/MM/YY HH:mm'})
    			.attr('placeholder', i18n.get('placeHolderDateTime'))
    			.change(function() {
    			
	    			var len = $(this).val().length;
	    			
	    			if(len == 1 || len == 2) { // Day
						var data = moment($(this).val(), i18n.get('patternDay_' + len));
						$(this).val(data.format(i18n.get('momPatternDate'))).trigger('keyup');
					} else if(len == 4 || len == 5) { // Day and Month
						var data = moment($(this).val(), i18n.get('patternDayMonth_' + len));
						$(this).val(data.format(i18n.get('momPatternDate'))).trigger('keyup');
					} else if(len == 7) { // Year
						var data = moment($(this).val(), i18n.get('momPatternDate'));
						$(this).val(data.format(i18n.get('momPatternDate'))).trigger('keyup');
					}
	    			
	    		}).setMask(i18n.get('maskDateTime')).on('click focus', function() {
					$(this).select();
				});
    		
    		$button.click(function() {
    			$input.data('DateTimePicker').show();
    			window.setTimeout(function(){ $input.focus(); }, 200);
    		});
    		
    	});
    },
    
    comboEditable: function(params) {
    	
    	return this.each(function() {
    		
    		var $this = $(this);
    		
    		params = $.extend(true, {
        		form: function() { return $this.data('form'); },
        		search: function() { return $this.data('search'); },
        		loadOptions: function() { return $this.data('loadOptions'); },
        		id: function() { return $this.val(); } 
        	}, params);
        	
    		var $btNew = $this.next().find('.bt-new');
    		var $btEdit = $this.next().find('.bt-edit');
    		var $btSearch = $this.next().find('.bt-search');
    		
    		
    		$this.change(function() {
    			 
    			$btEdit.prop('disabled', !$this.val());
    		
    		}).change();
    		
    		
    		$btNew.click(function(e) {
    			loading(true);
    			
    			$.ajax({
    				url: contextPath + params.form(),
    				success: function(data) {
    					if(typeof data == 'string') {
    						var $dialog = $(data);
            				$('body').append($dialog);
            				
            				$dialog.on('success', function(e, data) {
            					$this.trigger('loadOptions', [data]);
            				});
            				loading(false);
            				
            				$this.trigger('newLoad', [$dialog]);
    					} else {
    						error(data);
    					}
    				}
    			}).always(function() {
    				loading(false);
    			});
    			
    			$this.trigger('new');
    		});
    		
    		
    		$btEdit.click(function() {
    			loading(true);
    			$.get(contextPath + params.form(), {id: params.id()}, function(data) {
    				$this.trigger('editLoad');
    				var $dialog = $(data);
    				$('body').append($dialog);
    				
    				$dialog.on('success', function(e, data) {
    					$this.trigger('loadOptions', [data]);
    				});
    				loading(false);
    				
    				$this.trigger('editLoad', [$dialog]);
    			});
    			$this.trigger('edit');
    		});
    		
    		
    		$btSearch.click(function() {
    			loading(true);
    			$.get(contextPath + params.search(), {id: params.id(), dialogFootOk: 'setComboEditable(this, $(\'#' + $this.attr('id') + '\'))'}, function(data) {
    				
    				var $dialog = $(data);
    				$('body').append($dialog);
    				
    				$dialog.on('close', function(e, data) {
    					$this.trigger('loadOptions', [data]);
    				});
    				loading(false);
    				
    				$this.trigger('searchLoad', [$dialog]);
    			});
    			$this.trigger('search');
    		});
    		
    		$this.on('loadOptions', function(e, data) {
    			var id = params.id();
    			if(data && data.id) {
    				id = data.id;
    			}
    			$.ajax({
    				url: params.loadOptions(),
    				data: {id: id},
    				success: function(result) {
    					$this.html(result).change().trigger('loadOptionsComplete');
    				}
    			});
    		});
    		
		});
	}

});

function setComboEditable(self, $comboEditable) {
	if($(self).closest('.cm-modal-data-table').length) {
		var $form = $(self).closest('.cm-modal-data-table').find('form');
		var id = $form.data('selected')[0];
		if(!$comboEditable.find('option[value=' + id + ']').length) {
			$comboEditable.append('<option value="' + id + '">' + i18n.get('loading') + '</option>');
		}
		$comboEditable.val(id).trigger('setValue', [id]);
		$(self).closest('.cm-modal-data-table').modal('hide');
	}
}

/**
 * jquery.string - Prototype string functions for jQuery
 * (c) 2008 David E. Still (http://stilldesigning.com)
 * Original Prototype extensions (c) 2005-2008 Sam Stephenson (http://prototypejs.org)
 */

jQuery.extend({
	__stringPrototype: {
		/**
		 * ScriptFragmet, specialChar, and JSONFilter borrowed from Prototype 1.6.0.2
		 */
	 	JSONFilter: /^\/\*-secure-([\s\S]*)\*\/\s*$/,
		ScriptFragment: '<script[^>]*>([\\S\\s]*?)<\/script>',
		specialChar: {
			'\b': '\\b',
			'\t': '\\t',
			'\n': '\\n',
			'\f': '\\f',
			'\r': '\\r',
			'\\': '\\\\'
		},
	
		/**
		 * Check if the string is blank (white-space only or empty).
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		blank: function(s) {
			return /^\s*$/.test(this.s(s) || ' ');
		},
		/**
		 * Converts a string separated by dashes into a camelCase equivalent.
		 * For instance, 'foo-bar' would be converted to 'fooBar'.
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		camelize: function(s) {
			var a = this.s(s).split('-'), i;
			s = [a[0]];
			for (i=1; i<a.length; i++){
				s.push(a[i].charAt(0).toUpperCase() + a[i].substring(1));
			}
			s = s.join('');
			return this.r(arguments,0,s);
		},
		/**
		 * Capitalizes the first letter of a string and downcases all the others.
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		capitalize: function(s) {
			s = this.s(s);
			s = s.charAt(0).toUpperCase() + s.substring(1);//.toLowerCase();
			return this.r(arguments,0,s);
		},
		uncapitalize: function(s){
			s = this.s(s);
			s = s.charAt(0).toLowerCase() + s.substring(1);
			return this.r(arguments,0,s);
		},
		/**
		 * Replaces every instance of the underscore character ("_") by a dash ("-").
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		dasherize: function(s) {
			s = this.s(s).split('_').join('-');
			return this.r(arguments,0,s);
		},
		/**
		 * Check if the string is empty.
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		empty: function(s) {
			return $.trim(this.s(s)) === '';
		},
		/**
		 * Tests whether the end of a string matches pattern.
		 * @param {Object} pattern
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		endsWith: function(pattern, s) {
			s = this.s(s);
			var d = s.length - pattern.length;
			return d >= 0 && s.lastIndexOf(pattern) === d;
		},
		/**
		 * escapeHTML from Prototype-1.6.0.2 -- If it's good enough for Webkit and IE, it's good enough for Gecko!
		 * Converts HTML special characters to their entity equivalents.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		escapeHTML: function(s) {
			s = this.s(s).replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
			return this.r(arguments,0,s);
		},
		/**
		 * evalJSON from Prototype-1.6.0.2
		 * Evaluates the JSON in the string and returns the resulting object. If the optional sanitize parameter
		 * is set to true, the string is checked for possible malicious attempts and eval is not called if one
		 * is detected.
		 * @param {String} s string to be evaluated
		 * @return {Object} evaluated JSON result
		 */
		evalJSON: function(sanitize, s) {
			s = this.s(s);
			var json = this.unfilterJSON(false, s);
			try {
				if (!sanitize || this.isJSON(json)) { return eval('(' + json + ')'); }
			} catch (e) { }
			throw new SyntaxError('Badly formed JSON string: ' + s);
		},
		/**
		 * evalScripts from Prototype-1.6.0.2
		 * Evaluates the content of any script block present in the string. Returns an array containing
		 * the value returned by each script.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		evalScripts: function(s) {
			var scriptTags = this.extractScripts(this.s(s)), results = [];
			if (scriptTags.length > 0) {
				for (var i = 0; i < scriptTags.length; i++) {
					results.push(eval(scriptTags[i]));
				}
			}
			return results;
		},
		/**
		 * extractScripts from Prototype-1.6.0.2
		 * Extracts the content of any script block present in the string and returns them as an array of strings.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		extractScripts: function(s) {
			var matchAll = new RegExp(this.ScriptFragment, 'img'), matchOne = new RegExp(this.ScriptFragment, 'im'), scriptMatches = this.s(s).match(matchAll) || [], scriptTags = [];
			if (scriptMatches.length > 0) {
				for (var i = 0; i < scriptMatches.length; i++) {
					scriptTags.push(scriptMatches[i].match(matchOne)[1] || '');
				}
			}
			return scriptTags;
		},
		/**
		 * Returns a string with all occurances of pattern replaced by either a regular string
		 * or the returned value of a function.  Calls sub internally.
		 * @param {Object} pattern RegEx pattern or string to replace
		 * @param {Object} replacement string or function to replace matched patterns
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 * @see sub
		 */
		gsub: function(pattern, replacement, s) {
			s = this.s(s);
			if (jQuery.isFunction(replacement)) { s = this.sub(pattern, replacement, -1, s); }
			/* if replacement is not a function, do this the easy way; it's quicker */
			else { s = s.split(pattern).join(replacement); }
			return this.r(arguments,2,s);
		},
		/**
		 * Check if the string contains a substring.
		 * @param {Object} pattern RegEx pattern or string to find
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean result
		 */
		include: function(pattern, s) {
			return this.s(s).indexOf(pattern) > -1;
		},
		/**
		 * Returns a debug-oriented version of the string (i.e. wrapped in single or double quotes,
		 * with backslashes and quotes escaped).
		 * @param {Object} useDoubleQuotes escape double-quotes instead of single-quotes
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		inspect: function(useDoubleQuotes, s) {
			s = this.s(s);
			var escapedString;
			try {
				escapedString = this.sub(/[\x00-\x1f\\]/, function(match) {
					var character = jQuery.__stringPrototype.specialChar[match[0]];
					return character ? character : '\\u00' + match[0].charCodeAt().toPaddedString(2, 16);
			    }, -1, s);
			} catch(e) { escapedString = s; }
			s = (useDoubleQuotes) ? '"' + escapedString.replace(/"/g, '\\"') + '"' : "'" + escapedString.replace(/'/g, '\\\'') + "'";
			return this.r(arguments,1,s);
		},
		/**
		 * Treats the string as a Prototype-style Template and fills it with object�s properties.
		 * @param {Object} obj object of values to replace in string
		 * @param {Object} pattern RegEx pattern for template replacement (default matches Ruby-style '#{attribute}')
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		interpolate: function(obj, pattern, s) {
			s = this.s(s);
			if (!pattern) { pattern = /(\#\{\s*(\w+)\s*\})/; }
			var gpattern = new RegExp(pattern.source, "g");
			var matches = s.match(gpattern), i;
			for (i=0; i<matches.length; i++) {
				s = s.replace(matches[i], obj[matches[i].match(pattern)[2]]);
			}
			return this.r(arguments,2,s);
		},
		/**
		 * isJSON from Prototype-1.6.0.2
		 * Check if the string is valid JSON by the use of regular expressions. This security method is called internally.
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean result
		 */
		isJSON: function(s) {
			s = this.s(s);
			if (this.blank(s)) { return false; }
			s = s.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
			return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(s);
		},
		/**
		 * Evaluates replacement for each match of pattern in string and returns the original string.
		 * Calls sub internally.
		 * @param {Object} pattern RegEx pattern or string to replace
		 * @param {Object} replacement string or function to replace matched patterns
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 * @see sub
		 */
		scan: function(pattern, replacement, s) {
			s = this.s(s);
			this.sub(pattern, replacement, -1, s);
			return this.r(arguments,2,s);
		},
		/**
		 * Tests whether the beginning of a string matches pattern.
		 * @param {Object} pattern
		 * @param {String} s string to be evaluated
		 * @return {Boolean} boolean of result
		 */
		startsWith: function(pattern, s) {
			return this.s(s).indexOf(pattern) === 0;
		},
		/**
		 * Trims white space from the beginning and end of a string.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		strip: function(s) {
			s = jQuery.trim(this.s(s));
			return this.r(arguments,0,s);
		},
		/**
		 * Strips a string of anything that looks like an HTML script block.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		stripScripts: function(s) {
			s = this.s(s).replace(new RegExp(this.ScriptFragment, 'img'), '');
			return this.r(arguments,0,s);
		},
		/**
		 * Strips a string of any HTML tags.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		stripTags: function(s) {
			s = this.s(s).replace(/<\/?[^>]+>/gi, '');
			return this.r(arguments,0,s);
		},
		/**
		 * Returns a string with the first count occurances of pattern replaced by either a regular string
		 * or the returned value of a function.
		 * @param {Object} pattern RegEx pattern or string to replace
		 * @param {Object} replacement string or function to replace matched patterns
		 * @param {Integer} count number of (default = 1, -1 replaces all)
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		sub: function(pattern, replacement, count, s) {
			s = this.s(s);
			if (pattern.source && !pattern.global) {
				var patternMods = (pattern.ignoreCase)?"ig":"g";
				patternMods += (pattern.multiline)?"m":"";
				pattern = new RegExp(pattern.source, patternMods);
			}
			var sarray = s.split(pattern), matches = s.match(pattern);
			if (jQuery.browser.msie) {
				if (s.indexOf(matches[0]) == 0) sarray.unshift("");
				if (s.lastIndexOf(matches[matches.length-1]) == s.length - matches[matches.length-1].length) sarray.push("");
			}
			count = (count < 0)?(sarray.length-1):count || 1;
			s = sarray[0];
			for (var i=1; i<sarray.length; i++) {
				if (i <= count) {
					if (jQuery.isFunction(replacement)) {
						s += replacement(matches[i-1] || matches) + sarray[i];
					} else { s += replacement + sarray[i]; }
				} else { s += (matches[i-1] || matches) + sarray[i]; }
			}
			return this.r(arguments,3,s);
		},
		/**
		 * 
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		succ: function(s) {
			s = this.s(s);
			s = s.slice(0, s.length - 1) + String.fromCharCode(s.charCodeAt(s.length - 1) + 1);
			return this.r(arguments,0,s);
		},
		/**
		 * Concatenate count number of copies of s together and return result.
		 * @param {Integer} count Number of times to repeat s
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		times: function(count, s) {
			s = this.s(s);
			var newS = "";
			for (var i=0; i<count; i++) {
				newS += s;
			}
			return this.r(arguments,1,newS);
		},
		/**
		 * Returns a JSON string
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		toJSON: function(s) {
			return this.r(arguments,0,this.inspect(true, this.s(s)));
		},
		/**
		 * Parses a URI-like query string and returns an object composed of parameter/value pairs.
		 * This method is mainly targeted at parsing query strings (hence the default value of '&'
		 * for the seperator argument). For this reason, it does not consider anything that is either
		 * before a question mark (which signals the beginning of a query string) or beyond the hash 
		 * symbol ("#"), and runs decodeURIComponent() on each parameter/value pair.
		 * @param {Object} separator string to separate parameters (default = '&')
		 * @param {Object} s
		 * @return {Object} object
		 */
		toQueryParams: function(separator, s) {
			s = this.s(s);
			var paramsList = s.substring(s.indexOf('?')+1).split('#')[0].split(separator || '&'), params = {}, i, key, value, pair;
			for (i=0; i<paramsList.length; i++) {
				pair = paramsList[i].split('=');
				key = decodeURIComponent(pair[0]);
				value = (pair[1])?decodeURIComponent(pair[1]):undefined;
				if (params[key]) {
					if (typeof params[key] == "string") { params[key] = [params[key]]; }
					params[key].push(value);
				} else { params[key] = value; }
			}
			return params;
		},
		/**
		 * truncate from Prototype-1.6.0.2
		 * Truncates a string to the given length and appends a suffix to it (indicating that it is only an excerpt).
		 * @param {Object} length length of string to truncate to
		 * @param {Object} truncation string to concatenate onto truncated string (default = '...')
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		truncate: function(length, truncation, s) {
			s = this.s(s);
			length = length || 30;
			truncation = (!truncation) ? '...' : truncation;
			s = (s.length > length) ? s.slice(0, length - truncation.length) + truncation : String(s);
			return this.r(arguments,2,s);
		},
		/**
		 * Converts a camelized string into a series of words separated by an underscore ("_").
		 * e.g. $.string('borderBottomWidth').underscore().str = 'border_bottom_width'
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		underscore: function(s) {
			s = this.sub(/[A-Z]/, function(c) { return "_"+c.toLowerCase(); }, -1, this.s(s));
			if (s.charAt(0) == "_") s = s.substring(1);
			return this.r(arguments,0,s);
		},
		/**
		 * unescapeHTML from Prototype-1.6.0.2 -- If it's good enough for Webkit and IE, it's good enough for Gecko!
		 * Strips tags and converts the entity forms of special HTML characters to their normal form.
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		unescapeHTML: function(s) {
			s = this.stripTags(this.s(s)).replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>');
			return this.r(arguments,0,s);
		},
		/**
		 * unfilterJSON from Prototype-1.6.0.2.
		 * @param {Function} filter
		 * @param {String} s string to be evaluated
		 * @return {Object} .string object (or string if internal)
		 */
		unfilterJSON: function(filter, s) {
			s = this.s(s);
			filter = filter || this.JSONFilter;
			var filtered = s.match(filter);
			s = (filtered !== null)?filtered[1]:s;
			return this.r(arguments,1,jQuery.trim(s));
		},
		
		/**
		 * Sets .str property and returns $.string object.
		 * @param {String} s string to be evaluated
		 */
		r: function(args, size, s) {
			if (args.length > size || this.str === undefined) {
				return s;
			} else {
				this.str = ''+s;
				return this;
			};
		},
		s: function(s) {
			if (s === '' || s) { return s; }
			if (this.str === '' || this.str) { return this.str; }
			return this;
		}
	},
	string: function(str) {
		if (str === String.prototype) { jQuery.extend(String.prototype, jQuery.__stringPrototype); }
		else { return jQuery.extend({ str: str }, jQuery.__stringPrototype); }
	}
});
jQuery.__stringPrototype.parseQuery = jQuery.__stringPrototype.toQueryParams;
$.string(String.prototype);


/**
 * transform string to integer
 * @returns Number
 */
String.prototype.toInteger = function(){
	return parseInt(this.unFormatNumber().replace(/[^0-9.-]/g, "")) || 0;
};

String.prototype.unFormatNumber = function(){
	var value = this;
	
	value = $.parseNumber(value, {format: "#,##0.0000", locale: i18n.localeAbbr});
	if(isNaN(value))
	{
		return "";
	}
	else
	{
		return new String(value);
	}
};

/**
 * Converte uma string em um valor decimal v�lido desformatando e removendo tudo que n�o � num�rico
 * @return Number
 */
String.prototype.toFloat = function(){
	return parseFloat(this.unFormatNumber()) || 0;
};

/**
 * Converte a string contendo a representa��o RGB em hexadecimal 
 * @return String
 */
String.prototype.rgb2Hexa = function(){
	
	if(this.startsWith("#")){
		return this;
	}
	
	if(!this.startsWith("rgb(")){
		return ""; 
	}
	
	var rgb = this.replace(/rgb\(/g,"").replace(/\)/g,"").replace(/, /g,",").split(",");
	
	var chars = "0123456789ABCDEF";

	return '#' + String(chars.charAt(Math.floor(rgb[0] / 16))) + String(chars.charAt(rgb[0] - (Math.floor(rgb[0] / 16) * 16))) + String(chars.charAt(Math.floor(rgb[1] / 16))) + String(chars.charAt(rgb[1] - (Math.floor(rgb[1] / 16) * 16))) + String(chars.charAt(Math.floor(rgb[2] / 16))) + String(chars.charAt(rgb[2] - (Math.floor(rgb[2] / 16) * 16))); 
};

/**
 * Trim
 * @return String
 */
String.prototype.trim = function(){
	return $.trim(this);
};


/**
 * Verifica se a string é booleana
 * @return Boolean
 */
String.prototype.toBoolean = function(){ return this == 'true'; };

/**
 * Remove todos os numeros de uma string
 * return String
 */
String.prototype.removeNumbers = function(){
	return this.replace(/[^0-9]/g, "");
};

/**
 * Adiciona m�todo lpad() � classe String.
 * Preenche a String � esquerda com o caractere fornecido,
 * at� que ela atinja o tamanho especificado.
 */
String.prototype.lpad = function(pSize, pCharPad)
{
	var str = this;
	var dif = pSize - str.length;
	var ch = String(pCharPad).charAt(0);
	for (; dif>0; dif--) str = ch + str;
	return (str);
}; //String.lpad


/**
 * Adiciona m�todo rpad() � classe String.
 * Preenche a String � direita com o caractere fornecido,
 * at� que ela atinja o tamanho especificado.
 */
String.prototype.rpad = function(pSize, pCharPad)
{
	var str = this;
	var dif = pSize - str.length;
	var ch = String(pCharPad).charAt(0);
	for (; dif>0; dif--) str = str + ch;
	return (str);
}; //String.rpad


/**
 * Adiciona caracter a esquerda da string
 * @param size
 * @param ch
 * @returns {String}
 */
String.prototype.ladd = function(size, caracter)
{
	var str = this;
	var ch = String(caracter).charAt(0);
	for(var i = 0; i < size; i++)
	{
		str = ch + str;
	};
	return (str);
};


/**
 * Adiciona caracter a direita da string
 * @param size
 * @param ch
 * @returns {String}
 */
String.prototype.radd = function(size, caracter)
{
	var str = this;
	var ch = String(caracter).charAt(0);
	for(var i = 0; i < size; i++)
	{
		str = str + ch;
	};
	return (str);
};



/**
 * Verifica se contem a string passada por parametro
 * @param content
 * @returns {Boolean}
 */
String.prototype.contains = function(content) {
	var str = this;
	return str.indexOf(content) > -1;
};



/**
 * Verifica se contem a string passada por parametro
 * @param content
 * @returns {Boolean}
 */
String.prototype.notContains = function(content) {
	var str = this;
	return str.indexOf(content) == -1;
};




String.prototype.fileName = function() {
	var str = this;
	var slash = '/';
	if (str.match(/\\/)) { 
	      slash = '\\'; 
	} 
	return str.substring(str.lastIndexOf(slash) + 1, str.length);
};



/**
 * Elimina caracteres de formata��o e zeros � esquerda da string
 * de n�mero fornecida.
 * @param String pNum
 * 	String de n�mero fornecida para ser desformatada.
 * @return String de n�mero desformatada.
 */
function unformatNumber(pNum)
{
	return String(pNum).replace(/\D/g, "").replace(/^0+/, "");
}



$(document).on('show.bs.modal', '.modal', function(e) {
	if($(e.target).is('.modal')) {
		$('.modal-backdrop:visible:last').hide();
	}
}).on('shown.bs.modal', function(e) {
	if($(e.target).is('.modal')) {
		var $target = $(e.target);
		if($target.attr('id') == 'layLoading') {
			var zindexbg = parseInt($('.modal-backdrop:visible:last').css('z-index'));
			$('.modal-backdrop:visible:last').css('z-index', zindexbg + (40 * $('.modal[id!="layLoading"]').length));
			
			var zindex = parseInt($(e.target).css('z-index'));
			$(e.target).css('z-index', zindex + (40 * $('.modal[id!="layLoading"]').length));
		} else {
			var zindexbg = parseInt($('.modal-backdrop:visible:last').css('z-index'));
			$('.modal-backdrop:visible:last').css('z-index', zindexbg + (10 * $('.modal[id!="layLoading"]').length));
			
			var zindex = parseInt($(e.target).css('z-index'));
			$(e.target).css('z-index', zindex + (10 * $('.modal[id!="layLoading"]').length));
		}
	}
}).on('hide.bs.modal', '.modal', function(e) {
	if($(e.target).is('.modal')) {
		$('.modal-backdrop:hidden:last').show();
		$('.info-help', e.target).popover('destroy');
	}
});

$(document).on('click', 'a.info-help', function(e) {
	
	msgAlert({
		message: '<p class="p-info-help">' + (i18n[i18n.locale][$(e.target).data('message')] || $(e.target).data('message')) + '</p>',
		textAlign: 'text-left'
	});
	
	e.preventDefault();
});


$.propHooks.disabled = {
  set: function(elem, value, name) {
    var ret = (elem[ name ] = value);
    $(elem).trigger("propDisabledChange", [value]);
    return ret;
  }
};