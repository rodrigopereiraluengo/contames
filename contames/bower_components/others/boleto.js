function getBoleto(numero){
    var linha = numero.replace(/[^0-9]/g, '');
    if (linha.length == 47) {
        var barra = calcula_barra(numero);
        var dt = fator_vencimento(barra.substr(5, 4));
        var data = dt ? $.datepicker.formatDate(i18n[i18n.locale]['datepickerPatternDate'], dt).slice(0,10) : '';
        var valor = parseFloat((barra.substr(9, 8) * 1) + '.' + barra.substr(17, 2));
    } else {
        var barra = '';
        var data = '';
        var valor = 0;
    }
    return { 'numero': numero, 'barra': barra, 'data': data, 'valor': valor };
}

function calcula_barra(linha) {
    //var linha = form.linha.value;	// Linha Digitável
    barra = linha.replace(/[^0-9]/g, '');
    //
    // CÁLCULO DO DÍGITO DE AUTOCONFERÊNCIA (DAC)   -   5ª POSIÇÃO
    if (modulo11_banco('34191000000000000001753980229122525005423000') != 1) throw new NumeroBoletoException(null, 'Função "modulo11_banco" está com erro!');
    //
    //if (barra.length == 36) barra = barra + '00000000000';
    if (barra.length < 47) barra = barra + '00000000000'.substr(0, 47 - barra.length);
    if (barra.length != 47) { throw new NumeroBoletoException(null, 'A linha do Código de Barras está incompleta!' + barra.length); }
    //
    barra = barra.substr(0, 4)
						+ barra.substr(32, 15)
						+ barra.substr(4, 5)
						+ barra.substr(10, 10)
						+ barra.substr(21, 10)
						;
    //form.barra.value = barra;
    if (modulo11_banco(barra.substr(0, 4) + barra.substr(5, 39)) != barra.substr(4, 1)) {
        throw new NumeroBoletoException(null, 'Digito verificador ' + barra.substr(4, 1) + ', o correto é ' + modulo11_banco(barra.substr(0, 4) + barra.substr(5, 39)) + '\nO sistema não altera automaticamente o dígito correto na quinta casa!');
    }

    //if (form.barra.value != form.barra2.value) alert('Barras diferentes');
    return (barra);
}
function calcula_linha(barra) {
    //var barra = form.barra.value;	// Codigo da Barra
    linha = barra.replace(/[^0-9]/g, '');
    //
    if (modulo10('399903512') != 8) alert('Função "modulo10" está com erro!');
    if (linha.length != 44) alert('A linha do Código de Barras está incompleta!');
    //
    var campo1 = linha.substr(0, 4) + linha.substr(19, 1) + '.' + linha.substr(20, 4);
    var campo2 = linha.substr(24, 5) + '.' + linha.substr(24 + 5, 5);
    var campo3 = linha.substr(34, 5) + '.' + linha.substr(34 + 5, 5);
    var campo4 = linha.substr(4, 1); 	// Digito verificador
    var campo5 = linha.substr(5, 14); // Vencimento + Valor
    //
    if (modulo11_banco(linha.substr(0, 4) + linha.substr(5, 99)) != campo4) {
        throw new NumeroBoletoException(null, 'Digito verificador ' + campo4 + ', o correto é ' + modulo11_banco(linha.substr(0, 4) + linha.substr(5, 99)) + '\nO sistema não altera automaticamente o dígito correto na quinta casa!');
    }
    //
    if (campo5 == 0) campo5 = '000';
    //
    linha = campo1 + modulo10(campo1)
						+ ' '
						+ campo2 + modulo10(campo2)
						+ ' '
						+ campo3 + modulo10(campo3)
						+ ' '
						+ campo4
						+ ' '
						+ campo5
						;
    //if (form.linha.value != form.linha2.value) alert('Linhas diferentes');
    return (linha);
}
function fator_vencimento(dias) {
    if (parseInt(dias) == 0) return '';
    //Fator contado a partir da data base 07/10/1997
    //*** Ex: 04/07/2000 fator igual a = 1001
    //alert(dias);
    var currentDate, t, d, mes;
    t = new Date();
    currentDate = new Date();
    currentDate.setFullYear(1997, 9, 7); //alert(currentDate.toLocaleString());
    t.setTime(currentDate.getTime() + (1000 * 60 * 60 * 24 * dias)); //alert(t.toLocaleString());
    mes = (currentDate.getMonth() + 1); if (mes < 10) mes = "0" + mes;
    dia = (currentDate.getDate() + 1); if (dia < 10) dia = "0" + dia;
    //campo.value = dia +"."+mes+"."+currentDate.getFullYear();campo.select();campo.focus();
    return t;
}
function modulo10(numero) {
    /*
    select @peso = '121212121212121212121212'
    select @max  = datalength(@numero)
    select @peso = right(@peso, @max)
    set @contador = @max+1
    set @soma     = 0
    loop:
    set @contador = @contador-1
    set @valor    = isnull((ascii(substring(@peso, @contador, 1))-48) * 
    (ascii(substring(@numero, @contador, 1))-48), 0)
    set @soma     = isnull((select (case when (@valor<10) then 
    @valor when  (@valor>9) then @valor-10 end)), 0)+@soma
    set @soma     = isnull((select (case when  (@valor<10) then 
    null when  (@valor>9) then 1 end)), 0)+@soma
    if (@contador >1) goto loop
    select @resto= sum(@soma)%10
    select @retorno = case @resto when 0 then 0 else 10-@resto end
    return(Convert(char(1), @retorno))	
    */
    numero = numero.replace(/[^0-9]/g, '');
    var soma = 0;
    var peso = 2;
    var contador = numero.length - 1;
    //alert(contador);
    //numero = '00183222173';
    //for (var i=0; i <= contador - 1; i++) {
    //alert(10);
    //for (contador=10; contador >= 10 - 1; contador--) {
    while (contador >= 0) {
        //alert(contador);
        //alert(numero.substr(contador,1));
        multiplicacao = (numero.substr(contador, 1) * peso);
        if (multiplicacao >= 10) { multiplicacao = 1 + (multiplicacao - 10); }
        soma = soma + multiplicacao;
        //alert(numero.substr(contador,1)+' * '+peso+' = '+multiplicacao + ' =>' + soma) ;
        //alert(soma);
        if (peso == 2) {
            peso = 1;
        } else {
            peso = 2;
        }
        contador = contador - 1;
    }
    var digito = 10 - (soma % 10);
    //alert(numero + '\n10 - (' + soma + ' % 10) = ' + digito);
    if (digito == 10) digito = 0;
    return digito;
}

function debug(txt) {
    form.t.value = form.t.value + txt + '\n';
}
function modulo11_banco(numero) {
    /*
    SET @SOMA  = 0
    SET @PESO  = 2
    SET @BASE  = 9
    SET @RESTO = 0
    SET @CONTADOR = Len(@VALOR)

				  LOOP:
    BEGIN
    SET @SOMA = @SOMA + (Convert(int, SubString(@VALOR, @CONTADOR, 1)) *	@PESO)
    IF (@PESO < @BASE)
    SET @PESO = @PESO + 1
    ELSE
    SET @PESO = 2
    SET @CONTADOR = @CONTADOR-1
    END
    IF @CONTADOR >= 1 GOTO LOOP

				  IF (@RESTO = 1)
    BEGIN
    SET @RETORNO = (@SOMA % 11)
    END
    ELSE
    BEGIN
    SET @DIGITO = 11 - (@SOMA % 11)
    IF (@DIGITO > 9) SET @DIGITO = 0
    SET @RETORNO = @DIGITO
    END
    -- A UNICA DIFERENCA EH NO RETORNO, SE FOR 0, RETORNA 1
    IF @RETORNO = '0' SET @RETORNO='1'
    RETURN @RETORNO
    */
    numero = numero.replace(/[^0-9]/g, '');
    //debug('Barra: '+numero);
    var soma = 0;
    var peso = 2;
    var base = 9;
    var resto = 0;
    var contador = numero.length - 1;
    //debug('tamanho:'+contador);
    // var numero = "12345678909";
    for (var i = contador; i >= 0; i--) {
        //alert( peso );
        soma = soma + (numero.substring(i, i + 1) * peso);
        //debug( i+': '+numero.substring(i,i+1) + ' * ' + peso + ' = ' +( numero.substring(i,i+1) * peso)+' soma='+ soma);
        if (peso < base) {
            peso++;
        } else {
            peso = 2;
        }
    }
    var digito = 11 - (soma % 11);
    //debug( '11 - ('+soma +'%11='+(soma % 11)+') = '+digito);
    if (digito > 9) digito = 0;
    /* Utilizar o dígito 1(um) sempre que o resultado do cálculo padrão for igual a 0(zero), 1(um) ou 10(dez). */
    if (digito == 0) digito = 1;
    return digito;
}
function NumeroBoletoException(numero, message) {
    this.numero = numero;
    this.message = typeof message == 'undefined' ? "O número do boleto parece inválido!" : message;
    this.toString = function() {
        return this.numero + " " + this.message;
    };
}