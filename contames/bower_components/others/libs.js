$.ajaxSetup({
	cache: false,
	statusCode: {
        401: function() {
            var url = window.location.pathname + window.location.search;
            window.location.href = contextPath + '/entrar?url=' + url;
        }
    },
    beforeSend: function (xhr)  {
       xhr.setRequestHeader("Ajax", "true");       
    }
});

var atFocusTimeout;
var scheme = window.location.href.indexOf('https') == 0 ? 'https' : 'http';


/**
 * Exibe mensagem de erro
 * @param result
 */
function error(result, target) {
    
	var $_lay = (typeof target == 'undefined') ? $('.lay-validator-msgs') : $('.lay-validator-msgs', target);
	resetFormValidator(target);
	var msgs_nocategory = [];
	var msgs_confirm = [];
    
    $.each(result, function(i, val) {
    	var category = this.category;
    	var message = this.message;
		try {
			var $_parent = (typeof target == 'undefined') ? $('*[data-category]:visible') : $('*[data-category]:visible', target);
			var $this = $_parent.filter(function() {
				var is = false;
				$.each($(this).attr('data-category').split(' '), function(i, val) {
					if(val == category) {
						is = true;
					}
				});
				return is;
			});
			
			// Verifica se eh uma tr
			if($this.is('tr')) {
				if(!$this.children('td:first:visible').find('.validator-msgs').length) {
					$this.addClass('lay-validator-msgs').children('td:first:visible').prepend('<span class="validator-msgs"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;</span>');
				}
				if(typeof $this.find('.validator-msgs').data('msgs') == 'undefined') {
					$this.find('.validator-msgs').data('msgs', []);
				}
				$this.find('.validator-msgs').data('msgs').push(this.message);
				msgs_nocategory.push(this.message);
			} else {
				var $parent = $this.closest('div.form-group, div.ui-select');
				$parent.addClass('lay-validator-msgs has-error has-feedback');
				if(!$parent.find('.validator-msgs').length) {
					$this.addClass('validator-msgs').data('msgs', []);
				}
				$parent.find('.validator-msgs').data('msgs').push(this.message);
			}
			
		} catch(e) {
			
			if(category.startsWith('isConfirm')) {
				msgs_confirm.push(function(callback) {
					var $hidden = $form.find('input[name="' + category + '"]');
		    		if($hidden.length || !$hidden.val().toBoolean()) {
		    			msgConfirm(message, function() {
		    				$hidden.val(true);
		    				callback();
		    			});
		    		}
				});
			} else {
				msgs_nocategory.push(this.message);
			}
		}
	});
	
	$_lay = (typeof target == 'undefined') ? $('.lay-validator-msgs') : $('.lay-validator-msgs', target);
	
	$_lay.each(function() {
		var $this = $(this);
		if($this.is('tr')) {
			$this.find('.validator-msgs').popover('destroy');
			window.setTimeout(function() {
				$this.find('.validator-msgs').popover({
					content: $this.find('.validator-msgs').data('msgs').join('<br />'),
					placement: 'right',
					trigger: 'click',
					html: true
				}).click(function(e) {
					e.stopPropagation();
				});
			}, 200);
		} else {
			$this.find('.validator-msgs').popover('destroy');
			window.setTimeout(function() {
				$this.find('.validator-msgs').popover({
					content: $this.find('.validator-msgs').data('msgs').join('<br />'),
					placement: 'top',
					trigger: 'focus click',
					html: true
				});
			}, 200);
		}
			
	});
	
	var $form = typeof target == 'undefined' ? null : target.closest('form');
	var isMsgErrors = false;
	
	if(msgs_nocategory.length > 0) {
		validatorMsg(msgs_nocategory);
		isMsgErrors = true;
	} else {
		var $first = $_lay.first().find('.validator-msgs:eq(0)');
		
		// Verifica se esta dentro de uma tab
		var $closest = $first.closest('.tab-pane:not(.active)');
		if($closest.length) {
			$('a[href="#' + $closest.attr('id') + '"]').tab('show');
		}
		
		$first.toFocus();
		isMsgErrors = $first.length > 0;
	}
    loading(false);
        
    if(!isMsgErrors && msgs_confirm.length > 0) {
    	asyncWaterfall(msgs_confirm, function() {
    		if($form){ $form.submit(); }
    	});
    }
    
    if($form){ $form.trigger('validatorError'); }
}


$(function() {
	$(document).on('submit', 'form', function() {
		resetFormValidator($(this));
	});
});

function resetFormValidator($target) {
	
	if(typeof $target == 'undefined') {
		$target = $('form');
	}
	$('span.validator-msgs').popover('destroy').removeData('msg').remove();
	$target.find('.validator-msgs').popover('destroy').removeData('msg').removeClass('validator-msgs');
	$target.find('.lay-validator-msgs').removeClass('has-error').removeClass('has-feedback').removeClass('lay-validator-msgs');
	
	
}


/**
 * Mensagem de erro personalizada para o validator
 * @param messages
 */
function validatorMsg(messages, _this) {
	$.each(messages, function(i, message) {
		window.setTimeout(function() {
			$.bootstrapGrowl('<p><i class="fa fa-exclamation-triangle"></i>&nbsp;' + message + '</p>', {
				type: 'danger',
				align: 'right',
				offset: {from: 'top', amount: 20},
				width: 'auto'
			});
		}, i*100);
	});
	$(_this).closest('.lay-validator-msgs').find('input, select, textarea').toSelect();
}
var isLoading = false;
function loading(isShow) {
    
	if(isShow && !isLoading) {
        var html = '<div id="layLoading" class="modal" style="display: none">';
		html += '<div class="modal-dialog">';
		html += '<div class="modal-content">';
		html += '<img src="' + contextPath + '/resources/img/loading.gif" />';
		html += '<div id="ajax-progress" class="progress" style="display: none">';
		html += '<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">';
		html += '0%';
		html += '</div>';
		html += '</div>';
		html += '<h3 id="ajax-message">' + i18n[i18n.locale]['waiting'] + '</h3>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		$(html).appendTo($('body'));
		$('#layLoading').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
			$(this).remove();
		});
        isLoading = true;
	} else if(isLoading) {
		$('#layLoading').modal('hide');
        window.setTimeout(function(){isLoading = false;}, 500);
	}
    return isLoading;
}


function focusValidatorElement() {
	window.setTimeout(function(){ 
		$('.lay-validator-msgs-focus').find('input:first, select, textarea').focus();
		$('.lay-validator-msgs-focus').removeClass('lay-validator-msgs-focus');
	}, 50);
}

function msgSuccess(message) {
    $.bootstrapGrowl('<p><i class="fa fa-exclamation-triangle"></i>&nbsp;' + i18n.get(message) || message + '</p>', {
		type: 'success',
		align: 'right',
		offset: {from: 'top', amount: 20},
		width: 'auto'
	});
}

function msgError(message) {
	$.bootstrapGrowl('<p><i class="fa fa-exclamation-triangle"></i>&nbsp;' + i18n.get(message) || message + '</p>', {
		type: 'danger',
		align: 'right',
		offset: {from: 'top', amount: 20},
		width: 'auto'
	});
}

function msgAlert(params, e) {
	
	var _params = {
		title: null,
		message: '',
		fnOk: null,
		textAlign: 'text-center',
		cancel: null,
		size: '',
		stopPropagation: false
	};
	
	var param = $.extend(true, _params, params);
	
	var html = '<div id="dialogAlert" class="modal" style="display: none">';
		html += '<div class="modal-dialog ' + param.size + '">';
		html += '<div class="modal-content">';
		if(param.title) {
			html += '<div class="modal-header">';
			html += '<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>';
			html += '<h4 class="modal-title" id="dialogAlertTitle">' + param.title + '</h4>';
			html += '</div>';
		}
        html += '<div class="modal-body ' + param.textAlign + '">' + (i18n[i18n.locale][param.message] || param.message) + '</div>';
        html += '<div class="modal-footer text-center">';
        if(param.cancel) {
        	html += '<button type="button" id="btDialogAlertCancel" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;<b>' + i18n[i18n.locale]['cancelar'] + '</b></button>';
        }
        html += '<button type="button" id="btDialogAlertOk" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;<b>' + i18n[i18n.locale]['ok'] + '</b></button>';
        html += '</div>';
		html += '</div>';
		html += '</div>';
        html += '</div>'; 
		$(html).appendTo($('body'));
		$('#dialogAlert').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
			$(this).remove();
			if(typeof param.onclose != 'undefined') {
				param.onclose();
			}
		});
        $('#btDialogAlertOk').click(function() {
            $('#dialogAlert').modal('hide');
            if(param.fnOk){ param.fnOk(); }
        }).toFocus();
        
        if(_params.stopPropagation && e) {
        	e.cancelBubble = true;
        }
}

function msgConfirm(message, fnConfirm, fnCancel) {
	
    var html = '<div id="dialogConfirm" class="modal" style="display: none">';
		html += 	'<div class="modal-dialog">';
		html += 		'<div class="modal-content">';
        //html += 		'<div class="modal-header">' + i18n[locale]['Confirmation'] + '</div>';
		
        html += 			'<div class="modal-body text-center"><h3><i class="fa fa-question-circle"></i>&nbsp;' + (i18n[i18n.locale][message] || message) + '</h3></div>';
        html += 			'<div class="modal-footer text-center">';
        html += 				'<div class="row">';
        html += 					'<div class="col-md-6 form-group">';
        html += 						'<button type="button" id="btDialogConfirmYes" class="btn btn-primary btn-block"><i class="fa fa-check"></i>&nbsp;<b>' + i18n[i18n.locale]['sim'] + '</b></button>';
        html += 					'</div>';
        html += 					'<div class="col-md-6 form-group">';
        html += 						'<button type="button" id="btDialogConfirmNo" data-dismiss="modal" class="btn btn-default btn-block"><i class="fa fa-close"></i>&nbsp;<b>' + i18n[i18n.locale]['nao'] + '</b></button>';
        html += 					'</div>';
        html += 				'</div>';
		html += 			'</div>';
		html += 		'</div>';
		html +=		'</div>';
        html += '</div>';
		
        $(html).appendTo($('body'));
		
		$('#dialogConfirm').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
			$(this).remove();
		});
        
        
    
        $('#btDialogConfirmYes').click(function() { 
            fnConfirm();
            $('#dialogConfirm').modal('hide');
        }).toFocus();
    
        $('#btDialogConfirmNo').click(function() {
            $('#dialogConfirm').modal('hide');
            if(fnCancel){ fnCancel(); }
        });
    
        
        window.setTimeout(function(){$('#btDialogConfirmNo').focus();}, 500);
        
}

function updateProgress(percent) {
	if(percent == 0) {
		$('#ajax-progress').hide();
		$('#ajax-loader').show();
	} else {
		loading(true);
		$('#ajax-progress')
			.show()
			.find('div')
			.attr('aria-valuenow', percent || 0)
			.css('width', (percent || 0) + '%')
			.text((percent || 0) + '%');
		$('#ajax-loader').hide();
	}
}

var siprogress;
function verifyProgress(sessionid, viewid, isProgress) {
	if(isProgress) {
		window.clearInterval(siprogress);
		siprogress = setInterval(function() {
			$.ajax({
				url: scheme + '://' + files_server + '/progress',
				data: {'sessionid': sessionid, 'viewid': viewid},
				dataType: 'json',
				cache: false,
				success: function(data) {
					if(data.bytesReceived) {
						var fullperc = parseInt((data.bytesReceived / data.bytesExpected) * 100);
						updateProgress(fullperc);
					}
				}
			});
		}, 3000);
	} else {
		window.clearInterval(siprogress);
		updateProgress(100);
	}
}

function openRelatorioChartLine(self) {
	
	var title = $(self).closest('tr').children('td:eq(0)').children('a').length ? $(self).closest('tr').children('td:eq(0)').children('a').text() : $(self).closest('tr').children('td:eq(0)').children('span').text();
	var labels = $(self).closest('table').children('thead').find('th').filter(function(i) {
		return i > 0 && i < 13;
	}).map(function() {
		return $(this).find('span:eq(0)').text();
	}).get();
	var color = $(self).closest('tr').find('td:eq(13)').children('a').css('color');
	
	var values = $(self).closest('tr').find('td').filter(function(i) {
		return i > 0 && i < 13;
	}).map(function() {
		return $(this).find('a:eq(0)').text().toFloat();
	}).get();
	
	var html = '<canvas id="chartRelatorioLine" class="rel-canvas-chart"></canvas>';
	
	msgAlert({message: html, title: title, size: 'modal-lg'});
	
	var canvasChartRelatorioLine = document.getElementById("chartRelatorioLine").getContext("2d");
	var data = {
	    labels: labels,
	    datasets: [
			{
	            label: title,
	            fillColor: "rgba(255,84,84,0)",
	            strokeColor: color,
	            pointColor: color,
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: color,
	            data: values
	        }
	    ]
	};
	
	chartRelatorioLine = new Chart(canvasChartRelatorioLine).Line(data, {responsive: true, scaleLabel: function(valuePayload) {
		return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
	}});
	
}	

function dialogArquivoConsultar(viewid) {
	loading(true);
	$.ajax({
		url: contextPath + '/arquivo/consultar',
		data: {'viewid': viewid},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
		}
	}).always(function() {
		loading(false);
	});
}
var fnDialogArquivoHidden = function() {
	var viewid = $(this).data('viewid');
	$.get(contextPath + '/arquivo/quantidade', {viewid: viewid}, function(quantidade) {
		$('span.quant', '.bt-dialogArquivo[data-viewid="' + viewid + '"]').text(quantidade);
	});
};
$(document).off('hidden.bs.modal', '#dialogArquivoConsultar, #dialogAnexoArquivo', fnDialogArquivoHidden);
$(document).on('hidden.bs.modal', '#dialogArquivoConsultar, #dialogAnexoArquivo', fnDialogArquivoHidden);

var fnDialogArquivoShow = function() {
	if($('.bt-dialogArquivo', this).length) {
		var viewid = $('.bt-dialogArquivo', this).data('viewid');
		$.get(contextPath + '/arquivo/quantidade', {viewid: viewid}, function(quantidade) {
			$('span.quant', '.bt-dialogArquivo[data-viewid="' + viewid + '"]').text(quantidade);
		});
	}
};
$(document).off('shown.bs.modal', '.modal', fnDialogArquivoShow);
$(document).on('shown.bs.modal', '.modal', fnDialogArquivoShow);

