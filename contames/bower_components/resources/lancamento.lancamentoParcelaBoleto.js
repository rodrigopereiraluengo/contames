$('#dialogLancamentoParcelaBoleto').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#lancamentoParcelaBoletoNumero', '#frmLancamentoParcelaBoleto');

$('#lancamentoParcelaBoletoTipo', '#frmLancamentoParcelaBoleto').change(function() {
	$('#lancamentoParcelaBoletoNumero', '#frmLancamentoParcelaBoleto').unsetMask();
	if($(this).val() == 'CON') {
		$('#lancamentoParcelaBoletoNumero', '#frmLancamentoParcelaBoleto').setMask('999999999999 999999999999 99999999999 99999999999');
	} else {
		$('#lancamentoParcelaBoletoNumero', '#frmLancamentoParcelaBoleto').setMask('99999.99999 99999.999999 99999.999999 9 99999999999999');
	}
	$('#lancamentoParcelaBoletoNumero', '#frmLancamentoParcelaBoleto').toSelect();
}).change();

$('#frmLancamentoParcelaBoleto').submit(function() {
	var isValid = true;
	
	try {
		if($('#lancamentoParcelaBoletoTipo').val() == 'COB') {
			var numeroBoleto = $('#lancamentoParcelaBoletoNumero', '#frmLancamentoParcelaBoleto').val();
			if(numeroBoleto) {
				getBoleto(numeroBoleto);
			}
		}
	} catch(e) {
		isValid = false;
		error([{category: 'boletoNumero', message: i18n[i18n.locale]['boletoNumero.invalid']}]);
	}
	
	if(isValid) {
		loading(true);
		$.ajax({
			url: contextPath + '/lancamento/lancamentoParcelaBoleto',
			type: 'POST',
			data: $(this).serialize(),
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
	}
	
	return false;
});

function lancamentoParcelaBoletoSuccess(lancamentoParcela) {
	$('#dialogLancamentoParcelaBoleto').trigger('close', [{boletoTipo: lancamentoParcela.boletoTipo, boletoNumero: lancamentoParcela.boletoNumero}]).modal('hide');
}