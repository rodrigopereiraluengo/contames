$(function() {
	
	$('#nome').focus();
	
	Recaptcha.create(publickey, 'reCaptcha', {
        theme: "clean",
        callback: function() {
        	$('#recaptcha_response_field').appendTo($('#reCaptchaInput'))
        		.attr('data-category', 'contato.recaptcha_response_field')
        		.attr('data-placement', 'right')
        		.css('border', '').css('width', '50%').addClass('form-control');
    		$('#reCaptcha').find('table').css('width', '100%');
        }});
	
	
	$('#frmContato').on('validatorError', function() {
		Recaptcha.reload('f');
	}).submit(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/contato',
			data: $(this).serialize(),
			type: 'post',
			dataType:'jsonp'
		}).always(function() {
			loading(false);
		});
		return false;
	});
	
	$('#frmContato :submit').prop('disabled', false);
	
});

function success() {
	msgSuccess(i18n.get('contato.enviarEmail.successo'));
	$('#frmContato').trigger('reset'); 
	Recaptcha.reload('f');
}