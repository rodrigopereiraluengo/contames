$('#dialogLancamentoRateio').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#lancamentoRateioTipo').toFocus().change(function() {
	
	$('#layLancamentoRateioPessoa, #layLancamentoRateioRateio').hide();
	
	switch($(this).val()) {
	case 'PES':
		$('#layLancamentoRateioPessoa').show();
		break;
	case 'RAT':
		$('#layLancamentoRateioRateio').show();
		break;
	}
	
}).change();

$('#lancamentoRateioPessoa').toSelect().autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});


$('#lancamentoRateioRateio').comboEditable();

$('#lancamentoRateioPerc, #lancamentoRateioValor').formatDecimal();

$('#lancamentoRateioPerc').change(function() {
	var perc = parseFloat(isNaN($(this).val().unformat())?0:$(this).val().unformat());
	var valor = (perc / 100) * totalLancamentoParcela;
	$('#lancamentoRateioValor').val(valor.formatDecimal());
});

$('#lancamentoRateioValor').change(function() {
	var valor = parseFloat(isNaN($(this).val().unformat())?0:$(this).val().unformat());
	var perc = (valor / totalLancamentoParcela) * 100;
	$('#lancamentoRateioPerc').val(perc.formatDecimal());
});

$('#frmLancamentoRateio').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/lancamentoRateio',
		type: 'POST',
		data: $(this).serialize(),
		success: function(data) {
			if(typeof data == 'string') {
				$('#layLancamentoRateioList').replaceWith(data);
				$('#dialogLancamentoRateio').trigger('close').modal('hide');
				enableDisableLancamentoRateioBt();
			} else {
				error(data, $('#frmLancamentoRateio'));
			}
		}
	}).always(function() {
		loading(false);
	});
	return false;
});
