$('#dialogBaixaCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#baixaData').inputCalendar().toFocus();

$('#baixaBtAdicionarBaixaItem').click(function() {
	
	loading(true);
	
	var dt = $("#baixaData").val() ? Date.parseExact($("#baixaData").val(), i18n[i18n.locale]['patternDate']) : new Date();
	
	var data = {
		'dataDe': dt.moveToFirstDayOfMonth().toString(i18n[i18n.locale]['patternDate']),
		'dataAte': dt.moveToLastDayOfMonth().toString(i18n[i18n.locale]['patternDate']),
		'tipo': $('#frmBaixaCadastro').data('tipo')
	};
	
	$('#tbyBaixaItem tr').each(function(i) {
		data['idsNotIn[' + i + ']'] = $(this).data('lancamentoParcela');
	});
	
	$.ajax({
		url: contextPath + '/baixa/lancamentoParcela',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#baixaBtAdicionarBaixaItem'));
			}
		}
	}).always(function() {
		loading(false);
	});
});

$('#frmBaixaCadastro').on('click', '#tbyBaixaItem tr', function(e) {
	var $row = $(e.currentTarget);
	if($row.is('.active')) {
		$row.removeClass('active');
	} else {
		$row.addClass('active');
	}
	enableDisableBaixaItemBt();
}).on("dblclick", "#tbyBaixaItem tr", function(e) {
	var $row = $(e.currentTarget);
	$row.closest('tbody').find('tr').removeClass('active');
	$row.addClass('active');
	$('.bt-edit', $('#frmBaixaCadastro')).trigger('click');
	enableDisableBaixaItemBt();
	e.stopImmediatePropagation();
});

function enableDisableBaixaItemBt() {
	var selected = $('#tbyBaixaItem tr.active').length;
	$('#baixaBtEditarBaixaItem').prop('disabled', selected != 1);
	$('#baixaBtExcluitBaixaItem').prop('disabled', selected == 0);
}


$('#baixaBtEditarBaixaItem').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/baixa/baixaItem',
		data: {
			viewid: $('#baixaViewid').val(), 
			index: $('#tbyBaixaItem tr.active').data('index'),
			tipo: $('#frmBaixaCadastro').data('tipo')
		},
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result);
			}
		}
	}).always(function() {
		loading(false);
	});
	
});


$('#baixaBtExcluitBaixaItem').click(function() {
	var $selected = $('#tbyBaixaItem tr.active');
	var len = $selected.length;
	var tipo = $('#frmBaixaCadastro').data('tipo');
	var message = i18n[i18n.locale]['baixaItem.' + tipo + '.remover.confirm'];
	if(len > 1) {
		message = i18n[i18n.locale]['baixaItem.' + tipo + '.remover.confirm'].template({"0": len});
	}
	
	msgConfirm(message, function() {
		
		loading(true);
		
		var data = {viewid: $('#baixaViewid').val()};
		
		$('#tbyBaixaItem tr.active').each(function(i, tr) {
			data['indexs[' + i + ']'] = $(tr).data('index');
			if($(tr).data('id') && $(tr).data('status') != 'COM') {
				$('#frmBaixaCadastro').append('<input type="hidden" name="baixaItemDelete[]" value="' + $(tr).data('id') + '"/>');
			}
		});	
		
		$.ajax({
			url: contextPath + '/baixa/removerBaixaItem',
			type: 'post',
			data: data,
			success: function(result) {
				if(typeof result == 'string') {
					$('#layBaixaItem').replaceWith(result);
					enableDisableBaixaItemBt();
					
					message = i18n[i18n.locale]['baixaItem.' + tipo + '.removido.success'];
					if(len > 1) {
						message = i18n[i18n.locale]['baixaItem.' + tipo + '.removido.success.p'].template({"0": len});
					}
					msgSuccess(message);
				} else {
					error(result);
				}
			}
		}).always(function() {
			loading(false);
		});
	
	});
});


$('#frmBaixaCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/baixa/cadastro',
		type: 'POST',
		dataType: 'jsonp',
		data: $(this).serialize()
	}).always(function() {
		loading(false);
	});
	
	return false;
});

function baixaCadastroError(errors) {
	error(errors, $('#frmBaixaCadastro'));
}

function baixaCadastroSuccess(baixa) {
	var tipo = $('#frmBaixaCadastro').data('tipo');
	$('#dialogBaixaCadastro').trigger('success', [baixa]).modal('hide');
	msgSuccess(i18n[i18n.locale]['baixa.' + tipo + '.cadastro.success']);
}