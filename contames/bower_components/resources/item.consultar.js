$(function() {
	$('#frmItemConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.' + $('#frmItemConsultar').data('tipo') + '.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.' + $('#frmItemConsultar').data('tipo') + '.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.' + $('#frmItemConsultar').data('tipo') + '.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.' + $('#frmItemConsultar').data('tipo') + '.success.p')
	});
});

function itemExcluirSuccess(item) {
	$('#frmItemConsultar').trigger('excluir.success', [item]);
}