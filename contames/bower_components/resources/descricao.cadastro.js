$('#dialogDescricaoCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});


$('#descricaoNome').toSelect();


$('#frmDescricaoCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/descricao/cadastro',
		type: 'POST',
		dataType: 'jsonp',
		data: $(this).serialize()
	});
	
	return false;
});

function descricaoCadastroError(errors) {
	error(errors, $('#frmDescricaoCadastro'));
}

function descricaoCadastroSuccess(descricao) {
	$('#dialogDescricaoCadastro').trigger('success', [descricao]).modal('hide');
	loading(false);
	msgSuccess(i18n[i18n.locale]['descricao.' + descricao.descricaoTipo + '.cadastro.success']);
}