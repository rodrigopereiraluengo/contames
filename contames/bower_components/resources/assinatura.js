$(function() {
	
	$('#email').change(function() {
		
		$.ajax({
			url: contextPath + '/assinatura/codigoVerificador',
			data: {'email': $('#email').val()},
			type: 'POST',
			success: function(result) {
				resetFormValidator($('#email').closest('div'));
				if(result) {
					if(typeof result == 'string') {
						msgSuccess(i18n.get('assinatura.cliente.email.enviamosCodigoVerificador', $('#email').val()));
					} else {
						error(result);
					}
				}
			}
		});
		
	}).focus();
	
	$('#tipo').change(function() {
		
		resetFormValidator($('#frmAssinatura'));
		
		$('#razaoSocial').each(function() {
			$(this).val($(this).data('value_' + $('#tipo').val()));
		});
		
		if($(this).val() == 'J') {
			
			$('label[for=razaoSocial]').html(i18n[i18n.locale]['razaoSocial'] + '<span class="required">*</span>');
			$('#razaoSocial').attr('name', 'cliente.razaoSocial');
			
		} else {
			
			$('label[for=razaoSocial]').html(i18n[i18n.locale]['nome'] + '<span class="required">*</span>');
			$('#razaoSocial').attr('name', 'cliente.nome');
			
		}
	}).change();
	
	$('#cep').bind('keydown', 'return', function(e) {
		$('#btBuscarCep').click();
		return false;
	}).setMask(i18n[i18n.locale]['cepMask']);
	
	
	$('#btBuscarCep').click(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/cep/buscar',
			data: {cep: $('#cep').val(), errorFunction: 'pessoaCadastroError'},
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
	});
	
	
	$('#frmAssinatura').submit(function() {
		loading(true);
		$.ajax({
			url: $(this).attr('action'),
			type: $(this).attr('method'),
			data: $(this).serialize(),
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
		
		return false;
	}).find(':submit').prop('disabled', false);

});

function successCep(address) {
	
	$('#endereco, #bairro, #cidade, #uf').val('');
	
	if(address.resultado == 1) {
		$('#endereco').val(address.tipo_logradouro + ' ' + address.logradouro);
		$('#bairro').val(address.bairro);
		$('#numero').toSelect();
	}
	
	if(address.resultado == 1 || address.resultado == 2) {
		$('#cidade').val(address.cidade);
		$('#uf').val(address.uf);
		if(address.resultado == 2) {
			$('#endereco').toSelect();
		}
	}
}


function assinaturaSuccess(result) {
	$('#frmAssinatura :submit').prop('disabled', true);
	window.location.href = checkoutURL + result.token;
}