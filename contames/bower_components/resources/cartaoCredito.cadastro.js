$('#dialogCartaoCreditoCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#cartaoCreditoBandeira').comboEditable();

$('#cartaoCreditoNome').toSelect();

$('#cartaoCreditoTitular').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});

if($('#frmCartaoCreditoCadastro').data('isUtilizado')) {
	
	$('#cartaoCreditoLimite, #cartaoCreditoLimiteDisponivel, #cartaoCreditoLimiteUtilizado').change(function(e) {
	
		$('#cartaoCreditoLimiteDisponivel, #cartaoCreditoLimiteUtilizado').removeClass('font-red font-blue');
		
		var limite = parseFloat($('#cartaoCreditoLimite').val().unformat()) || 0;
		var limiteDisponivel = parseFloat($('#cartaoCreditoLimiteDisponivel').val().unformat()) || 0;
		var limiteUtilizado = parseFloat($('#cartaoCreditoLimiteUtilizado').val().unformat()) || 0;
		
		if($(e.target).attr('id') == 'cartaoCreditoLimiteDisponivel') {
			limiteUtilizado = limite - limiteDisponivel;
		} else {
			limiteDisponivel = limite - limiteUtilizado;
		}
		
		$('#cartaoCreditoLimiteDisponivel').val(limiteDisponivel.formatDecimal());
		$('#cartaoCreditoLimiteUtilizado').val(limiteUtilizado.formatDecimal());
		
		$('#cartaoCreditoLimiteDisponivel').addClass(limiteDisponivel > 0 ? 'font-blue' : 'font-red');
		$('#cartaoCreditoLimiteUtilizado').addClass(limiteUtilizado > 0 ? 'font-red' : 'font-blue');
		
	}).formatDecimal().trigger('change');
	
	$('#cartaoCreditoLimiteUtilizadoReq, #cartaoCreditoLimiteDisponivelReq').show();
	$('#cartaoCreditoDataFatuAntReq, #cartaoCreditoValorFaturaAntReq, #cartaoCreditoLimiteDisponivelAntReq').hide();
	
} else {


	// Limite
	$('#cartaoCreditoLimite, #cartaoCreditoValorFaturaAnt, #cartaoCreditoLimiteDisponivelAnt').change(function() {
		
		$('#cartaoCreditoLimiteDisponivel, #cartaoCreditoLimiteUtilizado').removeClass('font-red font-blue');
		
		var limite = parseFloat($('#cartaoCreditoLimite').val().unformat()) || 0;
		
		var valorFaturaAnt = parseFloat($('#cartaoCreditoValorFaturaAnt').val().unformat()) || parseFloat($('#cartaoCreditoValorFaturaAnt').text().unformat()) || 0;
		var limiteDisponivelAnt = parseFloat($('#cartaoCreditoLimiteDisponivelAnt').val().unformat()) || parseFloat($('#cartaoCreditoLimiteDisponivelAnt').text().unformat()) || 0;
		var limiteUtilizado = (limiteDisponivelAnt > 0 && valorFaturaAnt > 0) ? limite - (limiteDisponivelAnt + valorFaturaAnt) : 0;
		var limiteDisponivel = limite - limiteUtilizado;
		
		if(limite == 0) {
			$('#cartaoCreditoLimiteDisponivel, #cartaoCreditoLimiteUtilizado').html('<span class="text-danger"><strong>[&nbsp;' + i18n[i18n.locale]['conta.saldoLimite.blank'] + '&nbsp;]</strong></span>');
		} else {
			$('#cartaoCreditoLimiteDisponivel').text(limiteDisponivel.formatDecimal());
			$('#cartaoCreditoLimiteUtilizado').text(limiteUtilizado.formatDecimal());
			
			$('#cartaoCreditoLimiteDisponivel').addClass(limiteDisponivel > 0 ? 'font-blue' : 'font-red');
			$('#cartaoCreditoLimiteUtilizado').addClass(limiteUtilizado > 0 ? 'font-red' : 'font-blue');
		}
		
	}).formatDecimal().trigger('change');

}

// Limite Disponivel
$('#cartaoCreditoLimiteDisponivel').formatDecimal().change(function() {
	
	var limite = $('#cartaoCreditoLimite').val().unformat() || 0;
	var limiteDisponivel = parseFloat($(this).val().unformat()) || 0;
	var limiteUtilizado = limite - limiteDisponivel;
	$('#cartaoCreditoLimiteUtilizado').val($.formatNumber(limiteUtilizado));

});


//Limite Utilizado
$('#cartaoCreditoLimiteUtilizado').formatDecimal().change(function() {
	
	var limite = $('#cartaoCreditoLimite').val().unformat() || 0;
	var limiteUtilizado = parseFloat($(this).val().unformat()) || 0;
	var limiteDisponivel = limite - limiteUtilizado;
	
	$('#cartaoCreditoLimiteDisponivel').val($.formatNumber(limiteDisponivel));
	
});


// Fatu Ant
$('#cartaoCreditoDataFatuAnt').inputCalendar();

$('#cartaoCreditoDataFatuAnt, #cartaoCreditoValorFaturaAnt, #cartaoCreditoLimiteDisponivelAnt').change(function() {
	if(!$('#frmCartaoCreditoCadastro').data('isUtilizado')) {
		if($('#cartaoCreditoDataFatuAnt').val().blank() && $('#cartaoCreditoValorFaturaAnt').val().blank() && $('#cartaoCreditoLimiteDisponivelAnt').val().blank()) {
			$('#cartaoCreditoDataFatuAntReq, #cartaoCreditoValorFaturaAntReq, #cartaoCreditoLimiteDisponivelAntReq').hide();
		} else {
			$('#cartaoCreditoDataFatuAntReq, #cartaoCreditoValorFaturaAntReq, #cartaoCreditoLimiteDisponivelAntReq').show();
		}
	}
}).change();

$('#frmCartaoCreditoCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/cartaoCredito/cadastro',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	});
	return false;
});

function cartaoCreditoCadastroError(errors) {
	error(errors, $('#frmCartaoCreditoCadastro'));
}

function cartaoCreditoCadastroSuccess(cartaoCredito) {
	$('#dialogCartaoCreditoCadastro').trigger('success', [cartaoCredito]).modal('hide');
	loading(false);
	msgSuccess(i18n[i18n.locale]['cartaoCredito.cadastro.success']);
}