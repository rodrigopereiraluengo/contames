$('#dialogContaCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#contaBanco').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	}
});

$('#contaIsPoupanca').change(function() {
	if($(this).val().toBoolean()) {
		$('#layContaSaldoLimite, #layContaSaldoLimiteDisp, #layContaTalaoCheque').hide();
	} else {
		$('#layContaSaldoLimite, #layContaSaldoLimiteDisp, #layContaTalaoCheque').show();
	}
}).change();

$('#contaNome').toSelect();

$('#contaTitular').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});

$('#contaSaldoInicial, #contaSaldoLimite').formatDecimal();

$('#contaSaldoInicial').change(function() {
	$('#contaSaldoAtual').removeClass('font-red font-blue');
	if($(this).val()) {
		window.setTimeout(function(){
			$('#contaSaldoAtual').text($('#contaSaldoInicial').val());
			var contaSaldoAtual = $('#contaSaldoInicial').val().unformat().toFloat();
			$('#contaSaldoAtual').addClass(contaSaldoAtual < 0 ? 'font-red' : 'font-blue');
		}, 100);
	} else {
		$('#contaSaldoAtual').html('<span class="text-danger"><strong>[&nbsp;' + i18n[i18n.locale]['conta.saldoAtual.blank'] + '&nbsp;]</strong></span>');
	}
});

$('#contaSaldoLimite, #contaSaldoInicial').change(function() {
	var saldo = 0;
	
	if($('#contaSaldoInicial').is('input')) {
		saldo = parseFloat($('#contaSaldoInicial').val().unformat());
	} else {
		saldo = parseFloat($('#contaSaldoAtual').text().unformat());
	}
	
	if(isNaN(saldo)) {
		saldo = 0;
	}
	
	var limite = parseFloat($('#contaSaldoLimite').val().unformat());
	
	if(isNaN(limite)) {
		$('#contaSaldoLimiteDisp').html('<span class="text-danger"><strong>[&nbsp;' + i18n[i18n.locale]['conta.saldoLimite.blank'] + '&nbsp;]</strong></span>');
	} else {
		$('#contaSaldoLimiteDisp').removeClass('font-red font-blue');
		var limiteDisp = 0; 
		if(saldo >= 0) {
			limiteDisp = limite;
			$('#contaSaldoLimiteDisp').addClass('font-blue');
		} else {
			limiteDisp = limite + saldo;
			$('#contaSaldoLimiteDisp').addClass('font-red');
		}
		$('#contaSaldoLimiteDisp').text(limiteDisp.formatDecimal());
	}
	
});

$('#frmContaCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/conta/cadastro',
		type: 'POST',
		dataType: 'jsonp',
		data: $(this).serialize()
	});
	
	return false;
});

function contaCadastroError(errors) {
	error(errors, $('#frmContaCadastro'));
}

function contaCadastroSuccess(item) {
	$('#dialogContaCadastro').trigger('success', [item]).modal('hide');
	loading(false);
	msgSuccess(i18n[i18n.locale]['conta.cadastro.success']);
}

function dialogAdicionarTalaoCheque() {
	loading(true);
	$.ajax({
		url: contextPath + '/conta/talaoCheque?isAdicionar=true',
		data: {viewid: $('#contaViewid').val()},
		success: function(data) {
			if(typeof data == 'string') {
				$('body').append(data);
				$('#dialogContaTalaoCheque').on('success', function(e, data) {
					$('#layContaTalaoChequeList').replaceWith(data);
					enableDisableTalaoChequeBt(); 
				});
			} else {
				error(data, $('#frmContaCadastro'));
			}
		}
	}).always(function() {
		loading(false);
	});
}

function editarTalaoCheque(self) {
	loading(true);
	if(self) {
		$('#tbyContaTalaoChequeList tr.active').removeClass('active');
		$(self).addClass('active');
		enableDisableTalaoChequeBt();
	}
	
	$.ajax({
		url: contextPath + '/conta/talaoCheque',
		data: {index: $('#tbyContaTalaoChequeList tr.active').data('index'), viewid: $('#contaViewid').val()},
		success: function(data) {
			if(typeof data == 'string') {
				$('body').append(data);
				$('#dialogContaTalaoCheque').on('success', function(e, data) {
					$('#layContaTalaoChequeList').replaceWith(data);
					enableDisableTalaoChequeBt(); 
				});
			} else {
				error(data, $('#frmContaCadastro'));
			}
		}
	}).always(function() {
		loading(false);
	});
}

function selecionaTalaoCheque(self) {
	if($(self).is('.active')) {
		$(self).removeClass('active');
	} else {
		$(self).addClass('active');
	}
	enableDisableTalaoChequeBt();
}

function enableDisableTalaoChequeBt() {
	
	var $tby = $('#tbyContaTalaoChequeList');
	if($tby.length) {
		var selected = $('tr.active', $tby).length;
		$('#contaBtEditarTalaoCheque').prop('disabled', selected != 1);
		$('#contaBtExcluitTalaoCheque').prop('disabled', selected == 0);
	} else {
		$('#contaBtEditarTalaoCheque, #contaBtExcluitTalaoCheque').prop('disabled', true);
	}
	
	if($tby.length && $('tr', $tby).length) {
		$('#contaIsPoupanca').hide().next().show();
	} else {
		$('#contaIsPoupanca').show().next().hide();
	}
	
}
enableDisableTalaoChequeBt();

function removerTalaoCheque() {
	
	var selected = $('#tbyContaTalaoChequeList tr.active');
	var len = selected.length;
	var message = i18n[i18n.locale]['talaoCheque.excluir.confirm'];
	if(selected.length > 1) {
		message = i18n[i18n.locale]['talaoCheque.excluir.confirm.p'].template({"0": len});
	}
	
	msgConfirm(message, function() {
		loading(true);
		var data = {viewid: $('#contaViewid').val()};
		
		selected.each(function(i, tr) {
			data['indexs[' + i + ']'] = $(tr).data('index');
			if($(tr).data('id')) {
				$('#frmContaCadastro').append('<input type="hidden" name="talaoChequeDelete[]" value="' + $(tr).data('id') + '"/>');
			}
		});	
		
		$.ajax({
			url: contextPath + '/conta/removerTalaoCheque',
			type: 'post',
			data: data,
			success: function(data) {
				if(typeof data == 'string') {
					$('#layContaTalaoChequeList').replaceWith(data);
					enableDisableTalaoChequeBt();
					
					message = 'talaoCheque.excluir.success';
					if(len > 1) {
						message = 'talaoCheque.excluir.success.p';
					}
					msgSuccess(i18n[i18n.locale][message].template({"0": len}));
					
				} else {
					error(data, $('#frmContaCadastro'));
				}
				
			}
		}).always(function() {
			loading(false);
		});
	});
}