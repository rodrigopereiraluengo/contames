$(function() {
	
	// Rateio
	$('#relatorioRateioRateio').tokenInput(contextPath + '/relatorioRateio/multipleAutoComplete', {
        preventDuplicates: true,
        minChars: 1,
        searchDelay: 500,
        animateDropdown: false,
        resultsFormatter: function(result) { 
        	if(result.readonly) {
        		return '<li><b>' + result.name + '</b></li>';
        	} else {
        		return '<li>' + result.name + '</li>';
        	}
    	},
        tokenFormatter: function(result) { 
        	return '<li><p>' + result.name + '</p></li>'; 
    	},
        onResult: function(results) {
        	
        	var isPessoa = false;
        	var isRateio = false;
        	var iRateio = 0;
        	$.each(results, function(i) {
        		if(this.tipo == 'PES') {
        			isPessoa = true;
        		}
        		if(this.tipo == 'RAT') {
        			isRateio = true;
        			iRateio = i + 1;
        		}
        	});
        	
        	if(results.length) {
        		if(isPessoa) {
        			results.insert(0, {id: null, name: i18n.get('pessoas'), readonly: true, tipo: null});
        		}
        		if(isRateio) {
        			results.insert(iRateio, {id: null, name: i18n.get('rateios'), readonly: true, tipo: null});
        		}
        	}
        	return results;
        },
        onAdd: function(result) {
        	if(result.tipo == 'PES') {
        		$('#frmRelatorioRateio').append('<input type="hidden" name="pessoaList[]" value="' + result.id + '" />');
        	} else {
        		$('#frmRelatorioRateio').append('<input type="hidden" name="rateioList[]" value="' + result.id + '" />');
        	}
        },
        onDelete: function(result) {
        	if(result.tipo == 'PES') {
        		$('#frmRelatorioRateio').find('input[name="pessoaList[]"][value=' + result.id + ']').remove();
        	} else {
        		$('#frmRelatorioRateio').find('input[name="rateioList[]"][value=' + result.id + ']').remove();
        	}
        }
    });
	
	// Status
	$('#relatorioRateioStatus').multiselect({
		buttonWidth: '100%', 
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
	});
	
	// Formulario
	$('#frmRelatorioRateio').formSearch({
		multipleSelectRows: false,
		selectRow: false
	}).one('scrollEnd', function() {
		relatorioChart();
	}).on('scroll', function() {
		relatorioChart();
	});
	
	$('#btSearch').click(function() {
		$('#frmRelatorioRateio').one('scrollEnd', function() {
			relatorioChart();
		});
		labels = [];
		values = [];
	}).on('formSearch.order', function() {
		labels = [];
		values = [];
	});
	
	$('#btExportXls').click(function(e) {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_' + ($('#tipo').val() == 'DES' ? 'despesas' : 'receitas') + '_rateio.xlsx?' + $('#frmRelatorioRateio').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	$('#btExportPdf').click(function(e) {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_' + ($('#tipo').val() == 'DES' ? 'despesas' : 'receitas') + '_rateio.pdf?' + $('#frmRelatorioRateio').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	// Grafico
	relatorioChart();
	
	$('#isCCred').bootstrapToggle({
		on: i18n.get('MeioPagamento.CCR'),
		off: i18n.get('DocTipo.FT')
    }).change(function() {
    	$('#frmRelatorioRateio').setHidden('isCCred', $(this).prop('checked')).submit();
    	msgSuccess($(this).prop('checked') ? i18n.get('exibirMovsCCred') : i18n.get('exibirFaturas'));
    });
	
});


/**
 * Abre Janela Cadatro Rateio
 * @param rateio_id
 */
function relatorioRateioOpenPes(rateio_id) {
	if(rateio_id) {
		loading(true);
		$.ajax({
			url: contextPath + '/pessoa/cadastro',
			data: {id: rateio_id},
			success: function(result) {
				if(typeof result == 'string') {
					$('body').append(result);
				} else {
					error(result, $('#frmRelatorioRateio'));
				}
			}
		}).always(function(){
			loading(false);
		});
	}
}


/**
 * Abre Janela Movimentos
 * @param data
 */
function relatorioRateioOpenMovs(data) {
	
	data.ano = $('#relatorioRateioAno').val();
	data.tipo = $('#tipo').val();
	data.isCCred = $('#isCCred').prop('checked');
	
	// Rateio
	if(typeof data.rateio_id == 'undefined' || data.rateio_id == 0) {
		if(!$('input[name="rateioList[]"]').each(function(i) {
			data['rateioList[' + i + ']'] = $(this).val();
		}).length) {
			data['rateioList[0]'] = data.rateio_id;
		}
	} else {
		data['rateioList[0]'] = data.rateio_id;
		delete data.rateio_id;
	}
	
	// Pessoa
	if(typeof data.pessoa_id == 'undefined' || data.pessoa_id == 0) {
		if(!$('input[name="pessoaList[]"]').each(function(i) {
			data['pessoaList[' + i + ']'] = $(this).val();
		}).length) {
			data['pessoaList[0]'] = data.pessoa_id;
		}
	} else {
		data['pessoaList[0]'] = data.pessoa_id;
		delete data.pessoa_id;
	}
	
	$('#relatorioRateioStatus :selected').each(function(i, option) {
		data['statusList[' + i + ']'] = $(option).val();
	});
	
	loading(true);
	$.ajax({
		url: contextPath + '/relatorioRateio/movs',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioRateio'));
			}
		}
	}).always(function(){
		loading(false);
	});
}

function formatDecimal(val) {
	 return $.formatNumber(val, {format: "#,##0.".repeat(2, "0")});
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

Chart.defaults.global.responsive = true;
Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=formatDecimal(value)%>";
Chart.defaults.global.multiTooltipTemplate = "<%=formatDecimal(value)%>";

var chartRelatorio;
var toRelatorioChart;
function relatorioChart() {
	window.clearTimeout(toRelatorioChart);
	toRelatorioChart = window.setTimeout(function() {
		
		if(values.length) {
			$('#layChart').show();
		}
		
		if(typeof chartRelatorio != 'undefined'){ 
			chartRelatorio.destroy();
			$('#chartRelatorio').replaceWith('<canvas id="chartRelatorio" class="rel-canvas-chart"></canvas>');
		}
		
		if(typeof labels != 'undefined' && values.length) { 
		
			var ctxChart = document.getElementById("chartRelatorio").getContext("2d");
			var data = [];
			
			$.each(labels, function(i, val) {
				data.push({label : val, value : values[i], color: getRandomColor()});
			});
			
			
			chartRelatorio = new Chart(ctxChart).Pie(data, {responsive: true, maintainAspectRatio: true, scaleLabel: function(valuePayload) {
				return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
			}});
		
		}
	}, 500);
}