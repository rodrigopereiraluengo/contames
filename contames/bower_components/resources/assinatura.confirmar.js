var siConfirmado;
function isConfirmado() {
	siConfirmado = window.setInterval(function(){
		$.ajax({
			url: contextPath + '/assinatura/isConfirmado',
			data: {'token': token},
			cache: false,
			success: function(result) {
				if(result == "1") {
					window.clearInterval(siConfirmado);
					redirecionarLogin();
				}
			}
		});
	}, 1000);
}

var siSeconds;
function redirecionarLogin() {
    var seconds = 4;
    siSeconds = window.setInterval(function() {
        if(seconds == 0) {
            window.clearInterval(siSeconds);
            window.location.href = contextPath + '/entrar';
        }else {
        	if(!$('#lbSeconds').length) {
        		$('#pnConfirmado').addClass('panel-success').removeClass('panel-warning');
        		$('#pMsg').html(i18n.get('assinatura.confirmada'));
        	}
            $('#lbSeconds').text(seconds);
            seconds--;
        }
    }, 1000);
}