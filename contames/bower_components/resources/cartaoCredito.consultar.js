$(function() {
	$('#frmCartaoCreditoConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.cartaoCredito.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.cartaoCredito.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.cartaoCredito.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.cartaoCredito.success.p')
	});
	$('body').on('cartaoCredito.cadastro.success', function() {
		$('#frmCartaoCreditoConsultar').submit();
	});
});

function cartaoCreditoExcluirSuccess(cartaoCredito) {
	$('#frmCartaoCreditoConsultar').submit();
	$('body').trigger('cartaoCredito.excluir.success', [cartaoCredito]);
}