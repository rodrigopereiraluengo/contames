$(function() {
	$('#frmPessoaConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.' + ($('#frmPessoaConsultar').data('usuario') ? 'usuario' : 'pessoa') + '.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.' + ($('#frmPessoaConsultar').data('usuario') ? 'usuario' : 'pessoa') + '.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.' + ($('#frmPessoaConsultar').data('usuario') ? 'usuario' : 'pessoa') + '.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.' + ($('#frmPessoaConsultar').data('usuario') ? 'usuario' : 'pessoa') + '.success.p')
	});
	
	/**
	 * Atualiza nome do Usuario
	 */
	$(document).on('success', '#dialogPessoaCadastro', function(e, pessoa) {
		if($('#breadcrumbUsuario').length) {
			if(pessoa.pessoaTipo == 'F') {
				$('#breadcrumbUsuario').text(pessoa.apelido ? pessoa.apelido : pessoa.nome);
			} else {
				$('#breadcrumbUsuario').text(pessoa.fantasia ? pessoa.fantasia : pessoa.razaoSocial);
			}
		}
	});
	
	
});

function pessoaExcluirSuccess(pessoa) {
	$('#frmPessoaConsultar').trigger('excluir.success', [pessoa]);
}