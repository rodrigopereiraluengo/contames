$(function() {
	
	$('#relatorioDataPeriodoData').change(function() {
		
		$('#relatorioDataDataDe, #relatorioDataDataAte').prop('readonly', true);
		$('#btRelatorioDataDataDe, #btRelatorioDataDataAte').prop('disabled', true);
		
		switch($(this).val()) {
		case 'HOJE':
			var hoje = moment().format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(hoje).trigger('keyup');
			$('#relatorioDataDataAte').val(hoje).trigger('keyup');
			break;
		case 'ONTEM':
			var ontem = moment().subtract(1, 'day').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(ontem).trigger('keyup');
			$('#relatorioDataDataAte').val(ontem).trigger('keyup');
			break;
		case 'AMANHA':
			var amanha = moment().add(1, 'day').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(amanha).trigger('keyup');
			$('#relatorioDataDataAte').val(amanha).trigger('keyup');
			break;
		case 'ONTEM_AMANHA':
			var ontem = moment().subtract(1, 'day').format(i18n.get('momPatternDate'));
			var amanha = moment().add(1, 'day').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(ontem).trigger('keyup');
			$('#relatorioDataDataAte').val(amanha).trigger('keyup');
			break;
		case 'SEMANA_ATUAL':
			var dataDe = moment().day(0).format(i18n.get('momPatternDate'));
			var dataAte = moment().day(6).format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'SEMANA_ANT_PROX':
			var dataDe = moment().day(0).subtract(1, 'week').format(i18n.get('momPatternDate'));
			var dataAte = moment().day(6).add(1, 'week').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'SEMANA_ANT':
			var dataDe = moment().subtract(1, 'week').day(0).format(i18n.get('momPatternDate'));
			var dataAte = moment().subtract(1, 'week').day(6).format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'SEMANA_PROX':
			var dataDe = moment().add(1, 'week').day(0).format(i18n.get('momPatternDate'));
			var dataAte = moment().add(1, 'week').day(6).format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'MES_ATUAL':
			var dataDe = moment().startOf("month").format(i18n.get('momPatternDate'));
			var dataAte = moment().endOf("month").format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'MES_ANT_PROX':
			var dataDe = moment().startOf("month").subtract(1, 'month').format(i18n.get('momPatternDate'));
			var dataAte = moment().endOf("month").add(1,'month').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'MES_ANT':
			var dataDe = moment().startOf("month").subtract(1, 'month').format(i18n.get('momPatternDate'));
			var dataAte = moment().endOf("month").subtract(1,'month').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'MES_PROX':
			var dataDe = moment().startOf("month").add(1, 'month').format(i18n.get('momPatternDate'));
			var dataAte = moment().endOf("month").add(1,'month').format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'BIMESTRE_ATUAL':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			if(mes % 2 == 0) {
				dataDe = moment().subtract(1,'month').startOf("month").format(i18n.get('momPatternDate'));
				dataAte = moment().endOf("month").format(i18n.get('momPatternDate'));
			} else {
				dataDe = moment().startOf("month").format(i18n.get('momPatternDate'));
				dataAte = moment().add(1, 'month').endOf("month").format(i18n.get('momPatternDate'));
			}
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'BIMESTRE_ANT_PROX':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			if(mes % 2 == 0) {
				dataDe = moment().subtract(1,'month').startOf("month");
				dataAte = moment().endOf("month");
			} else {
				dataDe = moment().startOf("month");
				dataAte = moment().add(1, 'month').endOf("month");
			}
			$('#relatorioDataDataDe').val(dataDe.subtract(2, 'month').format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.add(2, 'month').format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'BIMESTRE_ANT':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			if(mes % 2 == 0) {
				dataDe = moment().subtract(1,'month').startOf("month");
				dataAte = moment().endOf("month");
			} else {
				dataDe = moment().startOf("month");
				dataAte = moment().add(1, 'month').endOf("month");
			}
			$('#relatorioDataDataDe').val(dataDe.subtract(2, 'month').format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.subtract(2, 'month').format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'BIMESTRE_PROX':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			if(mes % 2 == 0) {
				dataDe = moment().subtract(1,'month').startOf("month");
				dataAte = moment().endOf("month");
			} else {
				dataDe = moment().startOf("month");
				dataAte = moment().add(1, 'month').endOf("month");
			}
			$('#relatorioDataDataDe').val(dataDe.add(2, 'month').format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.add(2, 'month').format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'TRIMESTRE_ATUAL':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var trimestres = [
				[0,2],
				[3,5],
				[6,8],
				[9,11]
			];
			var trimestre = trimestres[parseInt((mes / 3) - 1)];
			dataDe = moment().set('month', trimestre[0]).startOf("month");
			dataAte = moment().set('month', trimestre[1]).endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'TRIMESTRE_ANT_PROX':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var trimestres = [
				[0,2],
				[3,5],
				[6,8],
				[9,11]
			];
			var trimestre = trimestres[parseInt((mes / 3) - 1)];
			dataDe = moment().set('month', trimestre[0]).subtract(3, 'months').startOf("month");
			dataAte = moment().set('month', trimestre[1]).add(3, 'months').endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'TRIMESTRE_ANT':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var trimestres = [
				[0,2],
				[3,5],
				[6,8],
				[9,11]
			];
			var trimestre = trimestres[parseInt((mes / 3) - 1)];
			dataDe = moment().set('month', trimestre[0]).subtract(3, 'months').startOf("month");
			dataAte = moment().set('month', trimestre[1]).subtract(3, 'months').endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'TRIMESTRE_PROX':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var trimestres = [
				[0,2],
				[3,5],
				[6,8],
				[9,11]
			];
			var trimestre = trimestres[parseInt((mes / 3) - 1)];
			dataDe = moment().set('month', trimestre[0]).add(3, 'months').startOf("month");
			dataAte = moment().set('month', trimestre[1]).add(3, 'months').endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'SEMESTRE_ATUAL':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var semestres = [
				[0,5],
				[6,11]
			];
			var semetre = semestres[parseInt((mes / 6) - 1)];
			dataDe = moment().set('month', semetre[0]).startOf("month");
			dataAte = moment().set('month', semetre[1]).endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'SEMESTRE_ANT_PROX':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var semestres = [
				[0,5],
				[6,11]
			];
			var semetre = semestres[parseInt((mes / 6) - 1)];
			dataDe = moment().set('month', semetre[0]).subtract(6, 'months').startOf("month");
			dataAte = moment().set('month', semetre[1]).add(6, 'months').endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'SEMESTRE_ANT':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var semestres = [
				[0,5],
				[6,11]
			];
			var semetre = semestres[parseInt((mes / 6) - 1)];
			dataDe = moment().set('month', semetre[0]).subtract(6, 'months').startOf("month");
			dataAte = moment().set('month', semetre[1]).subtract(6, 'months').endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'SEMESTRE_PROX':
			var dataDe = null, dataAte = null;
			var mes = moment().month() + 1;
			var semestres = [
				[0,5],
				[6,11]
			];
			var semetre = semestres[parseInt((mes / 6) - 1)];
			dataDe = moment().set('month', semetre[0]).add(6, 'months').startOf("month");
			dataAte = moment().set('month', semetre[1]).add(6, 'months').endOf("month");
			$('#relatorioDataDataDe').val(dataDe.format(i18n.get('momPatternDate'))).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte.format(i18n.get('momPatternDate'))).trigger('keyup');
			break;
		case 'ANO_ATUAL':
			var dataDe = moment().startOf("year").format(i18n.get('momPatternDate'));
			var dataAte = moment().endOf("year").format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe);
			$('#relatorioDataDataAte').val(dataAte);
			break;
		case 'ANO_ANT_PROX':
			var dataDe = moment().subtract(1, 'year').startOf("year").format(i18n.get('momPatternDate'));
			var dataAte = moment().add(1, 'year').endOf("year").format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'ANO_ANT':
			var dataDe = moment().subtract(1, 'year').startOf("year").format(i18n.get('momPatternDate'));
			var dataAte = moment().subtract(1, 'year').endOf("year").format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'ANO_PROX':
			var dataDe = moment().add(1, 'year').startOf("year").format(i18n.get('momPatternDate'));
			var dataAte = moment().add(1, 'year').endOf("year").format(i18n.get('momPatternDate'));
			$('#relatorioDataDataDe').val(dataDe).trigger('keyup');
			$('#relatorioDataDataAte').val(dataAte).trigger('keyup');
			break;
		case 'OUTRO':
			$('#relatorioDataDataDe, #relatorioDataDataAte').prop('readonly', false);
			$('#btRelatorioDataDataDe, #btRelatorioDataDataAte').prop('disabled', false);
			$('#relatorioDataDataDe').toSelect();
			break;
		}
		
	});
	
	
	$('#relatorioDataDataDe, #relatorioDataDataAte').inputCalendar();
	
	
	$('#relatorioDataFavorecido').tokenInput(contextPath + '/pessoa/multipleAutoComplete', {
        preventDuplicates: true,
        minChars: 1,
        searchDelay: 500,
        animateDropdown: false,
        resultsFormatter: function(pessoa) { return '<li>' + pessoa.name + '</li>'; },
        tokenFormatter: function(pessoa) { return '<li><p>' + pessoa.name + '</p></li>'; },
        onAdd: function(pessoa) {
            $('#frmRelatorioData').append('<input type="hidden" name="favorecidoList[]" value="' + pessoa.id + '" />');
        },
        onDelete: function(pessoa) {
        	$('#frmRelatorioData').find('input[name="favorecidoList[]"][value=' + pessoa.id + ']').remove();
        }
    });
	
	
	$('#relatorioDataMeio').multiselect({
		buttonWidth: '100%',
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
        onChange: function(option, checked, select) {
        	
        	// Verifica se ao menos um meio foi selecionado
        	if($('#relatorioDataMeio option:selected').length) {
        		$('#relatorioCartaoCreditoConta').prop('disabled', false);
        	} else {
        		$('#relatorioCartaoCreditoConta').prop('disabled', true).hide();
        	}
        	
            switch($(option).val()) {
            case 'CCR':
            	if(checked) {
            		loading(true);
            		$.ajax({
            			url: contextPath + '/cartaoCredito/carregaMultiSelect',
            			success: function(result) {
        					var html = '<optgroup label="' + i18n.get('cCred') + '" data-meio="CCR">';
    						$.each(result, function() {
    							html += '<option value="cartaoCredito_' + this.id + '" data-id="' + this.id + '">' + this.nome + '</option>';
    						});
    						html += '</optgroup>';
    						$('#relatorioCartaoCreditoConta').append(html);
    						relatorioDataShowHideLbCartaoCreditoConta();
            			}
            		}).always(function() {
            			loading(false);
            		});
            	} else {
            		$('#relatorioCartaoCreditoConta optgroup[data-meio="CCR"]').remove();
            		$('#frmRelatorioData').find('input[name="cartaoCreditoList[]"]').remove();
            		relatorioDataShowHideLbCartaoCreditoConta();
            	}
            	break;
            case 'CDB':
            case 'INT':
            case 'DEA':
            case 'DIN':
            case 'TRA':
            case 'DOC':
            case 'CHQ':
            	if(checked) {
	            	if(!$('#relatorioCartaoCreditoConta optgroup[data-meio="CONTA"]').length) {
	            		loading(true);
	            		$.ajax({
	            			url: contextPath + '/conta/carregaMultiSelect',
	            			success: function(result) {
	        					var html = '<optgroup label="' + i18n.get('contas') + '" data-meio="CONTA">';
	    						$.each(result, function() {
	    							html += '<option value="conta_' + this.id + '" data-id="' + this.id + '">' + this.nome + '</option>';
	    						});
	    						html += '</optgroup>';
	    						$('#relatorioCartaoCreditoConta').append(html);
	    						relatorioDataShowHideLbCartaoCreditoConta();
	            			}
	            		}).always(function() {
	            			loading(false);
	            		});
	            	}
	            	
	            	if($(option).val() == 'CHQ') {
	            		$('#relatorioDataCheque').prop('disabled', false).show();
	            	}
	            	
	            	
            	} else {
            		
            		if(!$('#relatorioDataMeio option:selected').filter(function() {
            			
            			var meios = ['CDB', 'INT', 'DEA', 'DIN', 'TRA', 'DOC', 'CHQ'];
            			
            			return $.inArray($(this).val(), meios) != -1;
            			
            		}).length) {
            			
            			$('#relatorioCartaoCreditoConta optgroup[data-meio="CONTA"]').remove();
            			$('#frmRelatorioData').find('input[name="contaList[]"]').remove();
            			relatorioDataShowHideLbCartaoCreditoConta();
            		}
            		
            		if($(option).val() == 'CHQ') {
	            		$('#relatorioDataCheque').hide().prop('disabled', true);
	            	}
            		
            	}
            	break;
            }
        }
    });
	
	$('#relatorioDataTipoDoc').multiselect({
		buttonWidth: '100%', 
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
	});
	
	$('#relatorioDataStatus').multiselect({
		buttonWidth: '100%', 
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
	});
	
	
	$('#frmRelatorioData').formSearch({
		multipleSelectRows: false,
		selectRow: true
	}).on('click', 'tbody tr', function(e) {
		
		
		var links = [];
		
		$row = $(e.currentTarget);
		if($row.is('.cm-tr-total')) return;
		
		$td = $(e.target);
		
		// Verifica se clicou no Favorecido
		if($td.data('favorecido')) {
			links.push({
				name: i18n.get('pessoa'), 
				url: '/pessoa/cadastro', 
				data: {
					id: $td.data('favorecido')
				}
			});
		}
		
		// Verifica se clicou na Conta
		if($td.data('conta')) {
			links.push({
				name: i18n.get('conta'), 
				url: '/conta/cadastro', 
				data: {
					id: $td.data('conta')
				}
			});
		}
		
		// Verifica se clicou no Cartao de Credito
		if($td.data('cartaoCredito')) {
			links.push({
				name: i18n.get('cartaoCredito'), 
				url: '/cartaoCredito/cadastro', 
				data: {
					id: $td.data('cartaoCredito')
				}
			});
		}
		
		// Verifica se clicou no Cheque
		
				
		// Verifica se eh lancamento
		if($row.data('lancamentoParcela')) {
			links.push({
				name: i18n.get('lancamento'), 
				url: '/lancamento/cadastro', 
				data: {
					id: $row.data('lancamento'),
					lancamentoParcela: $row.data('lancamentoParcela'),
					tipo: $row.data('tipo')
				}
			});
		}
		
		
		// Verifica se eh Pagamento / Recebimento
		if($row.data('baixaItem')) {
			links.push({
				name: i18n.get($row.data('tipo') == 'DES' ? 'pagamento' : 'recebimento'), 
				url: '/baixa/cadastro', 
				data: {
					id: $row.data('baixa'),
					baixaItem: $row.data('baixaItem'),
					tipo: $row.data('tipo')
				}
			});
		}
		
		// Verifica se eh Transferencia
		if($row.data('transferencia')) {
			links.push({
				name: i18n.get('transferencia'), 
				url: '/transferencia/cadastro', 
				data: {
					id: $row.data('transferencia')
				}
			});
		}
		
		
		// Verifica se eh Compensacao
		if($row.data('compensacaoItem')) {
			links.push({
				name: i18n.get('conciliacao'), 
				url: '/compensacao/cadastro', 
				data: {
					id: $row.data('compensacao'),
					compensacaoItem: $row.data('compensacaoItem')
				}
			});
		}
		
	
		if(links.length == 1) {
			relatorioDataOpenLink(links[0]);
		} else {
			var html = '<div class="list-group">';
			$.each(links, function(i, link) {console.log(link);
				html += '<a href="javascript:;" class="list-group-item" data-link-index="' + i + '">' + link.name + '</a>'; 
			});
			html += '</div>'; 
			msgAlert({
				title: i18n.get('irPara'), 
				message: html, 
				cancel: true,
				onclose: function() {
					$('#tbyRelatorioData').find('tr').removeClass('active');
				}
			});
			
			$.each(links, function(i, link) {
				$('a.list-group-item[data-link-index="' + i + '"]').click(function() {
					relatorioDataOpenLink(link);
				});
			});
		}
		
		
		
	}).on('formSearch.order', function() {
		labelsDespesas = [];
		labelsReceitas = [];
		despesas = [];
		receitas = [];
	});;
	
	
	loadChartRelatorioData();
	
	
	$('#btExportXls').click(function(e) {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_data.xlsx?' + $('#frmRelatorioData').serialize());
		} else{
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	$('#btExportPdf').click(function(e) {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_data.pdf?' + $('#frmRelatorioData').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	$('#isCCred').bootstrapToggle({
		on: i18n.get('MeioPagamento.CCR'),
		off: i18n.get('DocTipo.FT')
    }).change(function() {
    	$('#frmRelatorioData').setHidden('isCCred', $(this).prop('checked')).submit();
    	msgSuccess($(this).prop('checked') ? i18n.get('exibirMovsCCred') : i18n.get('exibirFaturas'));
    });
		
});


function relatorioDataOpenLink(link) {
	loading(true);
	$.ajax({
		url: link.url,
		data: link.data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioData'));
			}
		}
	}).always(function() {
		loading(false);
	});
}


function relatorioDataShowHideLbCartaoCreditoConta() {
	
	var labels = $('#relatorioCartaoCreditoConta optgroup').map(function() {
		return $(this).attr('label');
	});
	
	if(labels.length) {
		$('#lbRelatorioCartaoCreditoConta').text(labels.get().join(' / '));
		
		if($('#relatorioCartaoCreditoConta').data('isMultiSelect')) {
			$('#relatorioCartaoCreditoConta').multiselect('rebuild');
		} else {
			$('#relatorioCartaoCreditoConta').data('isMultiSelect', true).multiselect({
				buttonWidth: '100%', 
				nonSelectedText: i18n.get('selecione___'),
				nSelectedText: i18n.get('selecionados'),
				allSelectedText: i18n.get('todos___'),
				onChange: function(option, checked, select) {
					
					// Verifica se eh Cartao de Credito
					if($(option).val().indexOf('conta_') == -1) {
						if(checked) {
							$('#frmRelatorioData').append('<input type="hidden" name="cartaoCreditoList[]" value="' + $(option).data('id') + '" />');
						} else {
							$('#frmRelatorioData').find('input[name="cartaoCreditoList[]"][value=' + $(option).data('id') + ']').remove();
						}
					} else {
						if(checked) {
							$('#frmRelatorioData').append('<input type="hidden" name="contaList[]" value="' + $(option).data('id') + '" />');
						} else {
							$('#frmRelatorioData').find('input[name="contaList[]"][value=' + $(option).data('id') + ']').remove();
						}
					}
					
				}
			});
		}
		
		$('#layRelatorioCartaoCreditoConta').show();
	} else {
		$('#lbRelatorioCartaoCreditoConta').text('');
		$('#relatorioCartaoCreditoConta').removeData('isMultiSelect').multiselect('destroy').hide();
		$('#layRelatorioCartaoCreditoConta').hide();
	}
}

function formatDecimal(val) {
	 return $.formatNumber(val, {format: "#,##0.".repeat(2, "0")});
}
Chart.defaults.global.responsive = true;
Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=formatDecimal(value)%>";
Chart.defaults.global.multiTooltipTemplate = "<%=formatDecimal(value)%>";

var chartRelatorioDataBarra;
function loadChartRelatorioData() {
	
	if(typeof labels == 'undefined') { return; }
	
	if(labels.length) {
		$('#layChart').show();
	}
	
	if(typeof chartRelatorioDataBarra != 'undefined'){
		chartRelatorioDataBarra.destroy();
		$('#chartRelatorioDataBarra').replaceWith('<canvas id="chartRelatorioDataBarra" class="rel-canvas-chart"></canvas>');
	}
	
	var ctxBarra = document.getElementById("chartRelatorioDataBarra").getContext("2d");
	var data = {
	    labels: labels,
	    datasets: [
			{
	            label: i18n.get('despesas'),
	            fillColor: "rgba(255,84,84,1)",
	            strokeColor: "rgba(255,84,84,1)",
	            pointColor: "rgba(255,84,84,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(255,84,84,1)",
	            data: despesas
	        },
	        {
	            label: i18n.get('receitas'),
	            fillColor: "rgba(84,84,255,1)",
	            strokeColor: "rgba(84,84,255,1)",
	            pointColor: "rgba(84,84,255,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(84,84,255,1)",
	            data: receitas
	        }
	    ]
	};
	
	chartRelatorioDataBarra = new Chart(ctxBarra).Bar(data, {responsive: true, scaleLabel: function(valuePayload) {
		return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
	}});
	
}
