$('#dialogItemCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#itemNome').toSelect();

$('#itemUnidade').comboEditable();

$('#itemValor').formatDecimal();

$('#itemDiaVencimento').constrainNumber();

$('#itemEstoque').formatDecimal(3);


$('#trClassif').jstree({
	core: {
		
		strings: {'Loading ...': i18n.get('loading')},
		
		themes: {
            'name': 'proton',
            'responsive': true
        }, 
		
        data: {
			
			url: contextPath + '/classificacao/carrega',
			data: function(node) {
				return {id: (node.id == '#' ? null : node.data.id), tipo: $('#frmItemCadastro').data('tipo')};
			}
			
		},
		
		check_callback: function (operation, node, node_parent, node_position, more) {
			
			if(operation == "move_node") {
				var size = node_parent.parents.length + node.children_d.length;
				if(size > 3) {
					return false;
				}
			}
			
			return true;
		}
	},
	type: {
		'#': {
			"max_depth" : 4,
			"max_children" : 1
		}
	},
	plugins: ["dnd", "wholerow", "types"]


}).on('select_node.jstree', function(node, sel, e) {
	if($('#trClassif').data('selected') && $('#trClassif').data('selected').node.id == sel.node.id) {
		$('#trClassif').jstree('deselect_node', sel.node);
		$('#trClassif').removeData('selected');
		$('#itemClassificacaoId').val('');
	}else {
		$('#trClassif').data('selected', sel);
		if($('#trClassif').jstree('is_closed', sel.node)) {
			$('#trClassif').jstree('open_node', sel.node);
		}
		if(!$('#trClassif').jstree('is_loaded', sel.node)) {
			$('#trClassif').jstree('load_node', sel.node);
		}
		
		$('#itemClassificacaoId').val(sel.node.data.id);
	}
	btClassifEnableDisable();
}).on('rename_node.jstree', function(obj, text) {
	$('#trClassif').data('rename', text); 
	$.ajax({
		url: contextPath + '/classificacao/renomear',
		type: 'post',
		data: {'classificacao.id': text.node.data.id, 'classificacao.nivel': text.node.data.nivel, 'classificacao.nome': text.text},
		dataType: 'jsonp'
	});
	
}).on('move_node.jstree', function(node, parent, position, old_parent, old_position, is_multi, old_instance, new_instance) {
	
	var data = {
		'classificacao.id': parent.node.data.id, 
		'classificacao.classificacao.id': parent.parent == '#' ? null : $('#trClassif').jstree('get_node', parent.parent).data.id, 
		'classificacao.index': parent.position 
	};
	
	var children = $('#trClassif').jstree('get_node', parent.parent).children;
	var i;
	for(i = 0; i < children.length; i++) {
		data['indexList[' + i + '].id'] = $('#trClassif').jstree('get_node', children[i]).data.id;
		data['indexList[' + i + '].index'] = i;
	}
	
	if(parent.parent != parent.old_parent) {
		children = $('#trClassif').jstree('get_node', parent.old_parent).children;
		for(var j = 0; j < children.length; j++) {
			data['indexList[' + (i + j) + '].id'] = $('#trClassif').jstree('get_node', children[j]).data.id;
			data['indexList[' + (i + j) + '].index'] = j;
		}
	}
		
	$.ajax({
		url: contextPath + '/classificacao/mover',
		type: 'POST',
		data: data,
		success: function() {
			msgSuccess(i18n[i18n.locale]['classificacao.salva.sucesso']);
		}
	});
	
}).on('loaded.jstree after_open.jstree', function() {
	if(typeof arrNodeSelected != 'undefined') {
		if(arrNodeSelected.length) {
			if(arrNodeSelected.length == 1) {
				$('#trClassif').jstree('select_node', $('#classificacao_' + arrNodeSelected[0]));
			} else {
				$('#trClassif').jstree('open_node', $('#classificacao_' + arrNodeSelected[0]));
			}
			arrNodeSelected.splice(0, 1);
		}
	}
	
	btClassifEnableDisable();
	showHideNenhumaClassifEncontrada();
});


function novaClassif() {
	loading(true);
	var selected = $('#trClassif').jstree('get_selected');
	var data;
	
	if(selected.length) {
		var node = $('#trClassif').jstree('get_node', selected[0]);
		data = {
				id: node.data.id,
				nivel: node.data.nivel,
				index: node.children.length
			};
	} else {
		data = {
			id: null,
			nivel: null,
			index: $('#trClassif').children('ul').children('li').length
		};
	}
	
	data.tipo = $('#frmItemCadastro').data('tipo');
	
	$.ajax({
		url: contextPath + '/classificacao/nova',
		type: 'POST',
		data: data,
		dataType: 'jsonp'
	});
	
}


function showHideNenhumaClassifEncontrada() {
	if($('#trClassif').find('li').length) {
		$('#trClassif').find('#infoNenhumaClassif').remove();
		$('#trClassif').children('ul').show();
	} else {
		$('#trClassif').children('ul').hide();
		$('#trClassif').append('<div class="alert alert-success text-center cm-table-notfound" role="alert"><strong><i class="fa fa-info-circle"></i>' + i18n[i18n.locale]['nenhumaClassifEncontrada'] + '</strong></div>');
	}
}


function btClassifEnableDisable() {
	$('#btItemEditarClassif, #btItemExcluirClassif').prop('disabled', typeof $('#trClassif').data('selected') == 'undefined');
}


function novaClassifSuccess(classif) {
	var selected = $('#trClassif').jstree('get_selected'), $target;
	if(selected.length) {
		$target = $('#trClassif').jstree('get_node', selected[0]);
	} else {
		$target = '#';
	}
	
	var node = $('#trClassif').jstree().create_node($target , {'text': classif.nome, 'type':'valid_child', data: classif});
	$('#trClassif').jstree('edit', node);
	showHideNenhumaClassifEncontrada();
	loading(false);
}


function renomearClassifError(data) {
	error(data);
	
	$('#trClassif').jstree('set_text', $('#trClassif').data('rename').node, $('#trClassif').data('rename').old);
	
}


function renomearClassifSuccess() {
	msgSuccess(i18n[i18n.locale]['classificacao.salva.sucesso']);
};


function editarClassif() {
	var selected = $('#trClassif').jstree('get_selected');
	$('#trClassif').jstree('edit', selected);
}


function excluirClassif() {
	msgConfirm('classificacao.excluir.confirm', function() {
		loading(true);
		var selected = $('#trClassif').jstree('get_selected');
		var $target = $('#trClassif').jstree('get_node', selected[0]);
		$.ajax({
			url: contextPath + '/classificacao/excluir',
			type: 'POST',
			data: {id: $target.data.id},
			dataType: 'jsonp'
		});
	});
}


function excluirClassifSuccess() {
	try {
		var selected = $('#trClassif').jstree('get_selected');
		$('#trClassif').jstree('delete_node', selected);
		$('#trClassif').removeData('selected');
		btClassifEnableDisable();
		showHideNenhumaClassifEncontrada();
		msgSuccess(i18n[i18n.locale]['classificacao.excluir.sucesso']);
		$('#itemClassificacaoId').val('');
		loading(false);
	}catch(e){console.log(e);}
}





if($('#frmItemCadastro').data('tipo') == 'DES' || $('#frmItemCadastro').data('tipo') == 'REC') {

	$('#itemIsLancAuto').change(function() {
		if($(this).val().toBoolean()) {
			$('#reqItemValor, #reqItemFavorecido, #reqItemDiaVencimento, #reqItemMeio').show();
		} else {
			$('#reqItemValor, #reqItemFavorecido, #reqItemDiaVencimento, #reqItemMeio').hide();
		}
	}).change();
	$('#itemFavorecido').autoCompleteEditable().on('setValue', function(e, pessoa) {
		if(pessoa.pessoaTipo == 'J') {
			if(typeof pessoa.fantasia != 'undefined') {
				$(this).val(pessoa.fantasia);
			} else {
				$(this).val(pessoa.razaoSocial);
			}
		} else {
			if(typeof pessoa.apelido != 'undefined') {
				$(this).val(pessoa.apelido);
			} else {
				$(this).val(pessoa.nome);
			}
		}
	});
	
	
	/**
	 * Meio
	 */
	$('#itemMeio').change(function() {
		if($(this).val() == '') {
			$('#btItemCartaoCredito, #btItemConta').prop('disabled', true).removeClass('btn-primary').addClass('btn-default');
		} else {
			enableDisableBtItemCartaoCredito();
			enableDisableBtItemConta();
		}
	}).change();
	
	
	/**
	 * Conta
	 */
	$('#btItemConta').click(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/conta/dialogSelect',
			data: {
				id: $('#itemConta').val()
			},
			success: function(data) {
				var $element = $(data);
				$('body').append($element);
				
				$element.on('close', function(e, conta) {
					if($('#itemConta').val() != conta.id) {
						if(conta.id) {
							msgSuccess(i18n[i18n.locale]['conta.selecionada'].template({"0": conta.nome}));
						} else {
							msgSuccess(i18n[i18n.locale]['conta.nenhuma.selecionada']);
						}
					}
					$('#itemConta').val(conta.id);
					lightDarkBtItemConta();
				});
			}
		}).always(function() {
			loading(false);
		});
		
	});

} else {
	
	$('#itemValorCompra').formatDecimal();
	
	$('#itemValor, #itemValorCompra').change(function() {
		$('#frmItemCadastro').setHidden('isConfirmValorLTValorCompra', false);
	});
	
}

function enableDisableBtItemConta() {
	if(
	$('#itemMeio').val() != 'CCR' && $('#itemMeio').val() != 'CHQ' 
	|| 
		(
				($('#itemMeio').val() == 'CHQ' || $('#itemMeio').val() == 'CCR') 
				&& $('#frmItemCadastro').data('tipo') == 'REC'
		)
	) {
		$('#btItemConta').prop('disabled', false);
	} else {
		$('#btItemConta').prop('disabled', true);
	}
	lightDarkBtItemConta();
}

function lightDarkBtItemConta() {
	if($('#itemConta').val() && !$('#btItemConta').prop('disabled')) {
		$('#btItemConta').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btItemConta').removeClass('btn-primary').addClass('btn-default');
	}
}


/**
 * Cartao de Credito
 */
$('#btItemCartaoCredito').click(function() {
	if($('#frmItemCadastro').data('tipo')) {
		loading(true);
		$.ajax({
			url: contextPath + '/cartaoCredito/dialogSelect',
			data: {
				id: $('#itemCartaoCredito').val(), 
				isLimite: $('#itemCartaoCreditoIsLimite').val()
			},
			success: function(data) {
				
				var $element = $(data);
				
				$('body').append($element);
				
				$element.on('close', function(e, cartaoCredito) {
					if($('#itemCartaoCredito').val() != cartaoCredito[0].id) {
						if(cartaoCredito[0].id) {
							msgSuccess(i18n[i18n.locale]['cartaoCredito.selecionado'].template({"0": cartaoCredito[0].nome}));
							$('#itemCartaoCreditoIsLimite').val(cartaoCredito[0].isLimite);
						} else {
							msgSuccess(i18n[i18n.locale]['cartaoCredito.nenhum.selecionado']);
							$('#itemCartaoCreditoIsLimite').val('');
						}
					}
					$('#itemCartaoCredito').val(cartaoCredito[0].id);
					$('#itemCartaoCreditoIsLimite').val(cartaoCredito[1]);
					lightDarkBtItemCartaoCredito();
				});
				
			}
		}).always(function() {
			loading(false);
		});
	}
});


function enableDisableBtItemCartaoCredito() {
	if($('#frmItemCadastro').data('tipo') == 'DES' && $('#itemMeio').val() == 'CCR') {
		$('#btItemCartaoCredito').prop('disabled', false);
	} else {
		$('#btItemCartaoCredito').prop('disabled', true);
	}
	lightDarkBtItemCartaoCredito();
}


function lightDarkBtItemCartaoCredito() {
	if($('#itemCartaoCredito').val() && !$('#btItemCartaoCredito').prop('disabled')) {
		$('#btItemCartaoCredito').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btItemCartaoCredito').removeClass('btn-primary').addClass('btn-default');
	}
}


/**
 * Salvar Item
 */
$('#frmItemCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: $(this).attr('action'),
		type: $(this).attr('method'),
		dataType: 'jsonp',
		data: $(this).serialize()
	});
	return false;
});

function itemCadastroError(errors) {
	error(errors, $('#frmItemCadastro'));
}

function itemCadastroSuccess(item) {
	$('#dialogItemCadastro').trigger('success', [item]).modal('hide');
	loading(false);
	msgSuccess(i18n[i18n.locale]['item.' + item.tipo + '.cadastro.success']);
}

function dialogAdicionarItemMidia() {
	loading(true);
	
	$.ajax({
		url: contextPath + '/item/dialogMidia',
		data: {
			viewid: $('#itemCartaoCredito').val()
		},
		success: function(data) {
		
		}
	});
}