$('#dialogLancamentoParcela').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});


$('#lancamentoParcelaData').inputCalendar().toSelect();


$('#btLancamentoParcelaCartaoCredito').click(function() {
	if($('#lancamentoParcelaTipo').val()) {
		loading(true);
		$.ajax({
			url: contextPath + '/cartaoCredito/dialogSelect',
			data: {
				id: $('#lancamentoParcelaCartaoCredito').val(), 
				isLimite: $('#lancamentoParcelaCartaoCreditoIsLimite').val(),
				data: $('#lancamentoParcelaData').val(),
				isReadOnly: $('#frmLancamentoParcela').data('isReadOnly')
			},
			success: function(data) {
				
				var $element = $(data);
				
				$('body').append($element);
				
				$element.on('close', function(e, cartaoCredito) {
					if($('#lancamentoParcelaCartaoCredito').val() != cartaoCredito[0].id) {
						if(cartaoCredito[0].id) {
							msgSuccess(i18n[i18n.locale]['cartaoCredito.selecionado'].template({"0": cartaoCredito[0].nome}));
							$('#lancamentoParcelaCartaoCreditoIsLimite').val(cartaoCredito[0].isLimite);
						} else {
							msgSuccess(i18n[i18n.locale]['cartaoCredito.nenhum.selecionado']);
							$('#lancamentoParcelaCartaoCreditoIsLimite').val('');
						}
					}
					$('#lancamentoParcelaCartaoCredito').val(cartaoCredito[0].id);
					$('#lancamentoParcelaCartaoCreditoIsLimite').val(cartaoCredito[1]);
					lightDarkBtCartaoCredito();
				});
				
			}
		}).always(function() {
			loading(false);
		});
	}
});


$('#btLancamentoParcelaConta').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/conta/dialogSelect',
		data: {
			id: $('#lancamentoParcelaConta').val(),
			isReadOnly: $('#frmLancamentoParcela').data('isReadOnly')
		},
		success: function(data) {
			var $element = $(data);
			$('body').append($element);
			
			$element.on('close', function(e, conta) {
				if($('#lancamentoParcelaConta').val() != conta.id) {
					if(conta.id) {
						msgSuccess(i18n[i18n.locale]['conta.selecionada'].template({"0": conta.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['conta.nenhuma.selecionada']);
					}
				}
				$('#lancamentoParcelaConta').val(conta.id);
				lightDarkBtConta();
			});
		}
	}).always(function() {
		loading(false);
	});
	
});


$('#btLancamentoParcelaCheque').click(function() {
	
	loading(true);
	
	if($('#lancamentoParcelaTipo').val() == 'DES') {
		$.ajax({
			url: contextPath + '/talaoCheque/dialogSelect',
			data: {
				'lancamentoParcela.id': $('#lancamentoParcelaId').val(),
				'cheque.id': $('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(),
				'cheque.talaoCheque.id': $('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val(),
				'cheque.numero': $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(),
				'cheque.data': $('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(),
				'cheque.obs': $('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val(), 
				'isReadOnly': $('#frmLancamentoParcela').data('isReadOnly') 
			},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				
				$dialog.on('close', function(e, cheque) {
					
					if(typeof cheque.talaoCheque != 'undefined') {
						if($('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val() != cheque.talaoCheque.id || $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val() != cheque.numero){
							msgSuccess(i18n[i18n.locale]['cheque.selecionado.success'].template({"0": cheque.numero, "1": cheque.talaoCheque.conta.nome}));
						}
						$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(cheque.id);
						$('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val(cheque.talaoCheque.id);
						$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(cheque.numero);
						$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(cheque.data);
						$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val(cheque.obs);
					} else {
						if($('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val() != "" && $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val() != ""){
							msgSuccess(i18n[i18n.locale]['cheque.nenhum.selecionado']);
						}
						$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val("");
						$('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val("");
						$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val("");
						$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val("");
						$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val("");
					}
					
					$('#frmLancamentoParcela').data('chequedes', cheque);
					
					lightDarkBtCheque();
					
				});
				
			}
		}).always(function() {
			loading(false);
		});
	} else {
		$.ajax({
			url: contextPath + '/cheque/dialogProps',
			data: {
				'lancamentoParcela.id': $('#lancamentoParcelaId').val(),
				'cheque.id': $('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(),
				'cheque.banco.id': $('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoParcela').val(),
				'cheque.numero': $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(),
				'cheque.data': $('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(),
				'cheque.nome.id': $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val(),
				'cheque.obs': $('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val()
			},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('close', function(e, cheque) {
					cheque = $.extend({
						id: '',
						banco: {id: ''},
						nome: {id: ''}
					}, cheque);
					
					try{
						
					
					if(typeof cheque.numero != 'undefined') {
						
						if(
						$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoParcela').val() != cheque.banco.id
						||
						$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val() != cheque.numero
						||
						$('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val() != cheque.nome.id
						) {
							
							var message = i18n[i18n.locale]['cheque.informado.success'].template({"0": cheque.numero});
							msgSuccess(message);
						}
						
						$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(cheque.id);
						$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoParcela').val(cheque.banco.id || '');
						$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(cheque.numero);
						$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(cheque.data);
						if(typeof cheque.nome != 'undefined') { $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val(cheque.nome.id); }
						else { $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val(''); }
						$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val(cheque.obs);
					} else {
						
						if($('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val() != cheque.numero) {
							msgSuccess(i18n[i18n.locale]['cheque.nenhum.informado']);
						}
						
						$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val('');
						$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoParcela').val('');
						$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val('');
						$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val('');
						$('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val('');
						$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val('');
					}
					
					$('#frmLancamentoParcela').data('chequerec', cheque);
					lightDarkBtCheque();
					}catch(e) {
						
					}
				});
			}
		}).always(function() {
			loading(false);
		});
	}
});


if($('#lancamentoParcelaTipo').val() == 'DES') {
	$('#frmLancamentoParcela').data('chequedes', {
		id: $('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(),
		talaoCheque: {id: $('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val()},
		numero: $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(),
		data: $('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(),
		obs: $('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val()
	});
} else {
	$('#frmLancamentoParcela').data('chequerec', {
		id: $('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(),
		banco: {codigo: $('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoParcela').val()},
		numero: $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(),
		data: $('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(),
		nome: {id: $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val()},
		obs: $('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val()
	});
}

$('#btLancamentoParcelaBoleto').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/lancamentoParcelaBoleto',
		data: {boletoTipo: $('#lancamentoParcelaBoletoTipo').val(), boletoNumero: $('#lancamentoParcelaBoletoNumero').val()},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, boleto) {
				if(boleto.boletoNumero) {
					$('#lancamentoParcelaBoletoTipo').val(boleto.boletoTipo);
					$('#lancamentoParcelaBoletoNumero').val(boleto.boletoNumero);
					if(boleto.boletoTipo == 'COB') {
						try {
							var objBoleto = getBoleto(boleto.boletoNumero);
							
							var isData = false;
							if($('#lancamentoParcelaData').val() != objBoleto.data) {
								isData = true;
							}
							
							var isValor = false;
							if($('#lancamentoParcelaValor').val() != objBoleto.valor.formatDecimal()) {
								isValor = true;
							}
							
							if(isData && isValor) {
								msgConfirm(i18n[i18n.locale]['boletoNumero.data.valor.atualizar'].template({"0": objBoleto.data, "1": objBoleto.valor.formatDecimal()}), function() {
									$('#lancamentoParcelaData').val(objBoleto.data);
									$('#lancamentoParcelaValor').val(objBoleto.valor.formatDecimal());
								});
							} else if(isData) {
								msgConfirm(i18n[i18n.locale]['boletoNumero.data.atualizar'].template({"0": objBoleto.data}), function() {
									$('#lancamentoParcelaData').val(objBoleto.data);
								});
							} else if(isValor) {
								msgConfirm(i18n[i18n.locale]['boletoNumero.valor.atualizar'].template({"0": objBoleto.valor.formatDecimal()}), function() {
									$('#lancamentoParcelaValor').val(objBoleto.valor.formatDecimal());
								});
							}
							
						}catch(e){}
					} 
					
					msgSuccess(i18n[i18n.locale]['codigoBarras.informado.success']);
					
				} else {
					$('#lancamentoParcelaBoletoTipo').val('');
					$('#lancamentoParcelaBoletoNumero').val('');
					msgSuccess(i18n[i18n.locale]['codigoBarras.nenhum.informado']);
				}
				lightDarkBtBoleto();
			});
		}
	}).always(function() {
		loading(false);
	});
	
});


$('#btLancamentoParcelaArquivo').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/arquivo/dialogAnexo',
		data: {
				viewid: $('#lancViewid').val(),
				target: 'lancamentoParcela',
				'arquivo.id': $('#lancamentoParcelaArquivoId').val(),
				'arquivo.temp': $('#lancamentoParcelaArquivoTemp').val(),
				'arquivo.nome': $('#lancamentoParcelaArquivoNome').val(),
				'arquivo.contentType': $('#lancamentoParcelaArquivoContentType').val(),
				'arquivo.size': $('#lancamentoParcelaArquivoSize').val()
		},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, arquivo) {
				
				if($('#lancamentoParcelaArquivoNome').val() != arquivo.nome || $('#lancamentoParcelaArquivoContentType').val() != arquivo.contentType || $('#lancamentoParcelaArquivoSize').val() != arquivo.size) {
					if(arquivo.nome) {
						msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#lancamentoParcelaArquivoNome').val()}));
					}
				}
				
				$('#lancamentoParcelaArquivoNome').val(arquivo.nome);
				$('#lancamentoParcelaArquivoContentType').val(arquivo.contentType);
				$('#lancamentoParcelaArquivoSize').val(arquivo.size);
				
				lightDarkBtArquivo();
				
			});
		}
	}).always(function() {
		loading(false);
	});
	
});


$('#lancamentoParcelaValor').formatDecimal();


$('#lancamentoParcelaMeio').change(function() {
	if($(this).val() == '') {
		$('#btLancamentoParcelaCartaoCredito, #btLancamentoParcelaConta, #btLancamentoParcelaCheque, #btLancamentoParcelaBoleto').prop('disabled', true).removeClass('btn-primary').addClass('btn-default');
	} else {
		enableDisableBtCartaoCredito();
		enableDisableBtConta();
		enableDisableBtCheque();
		enableDisableBtBoleto();
	}
}).change();


$('#lancamentoParcelaTipo').change(function() {
	var cheque = $('#frmLancamentoParcela').data('cheque' + $(this).val().toLowerCase());
	if(cheque) {
		if($(this).val() == 'DES') {
			$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(cheque.id);
			$('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoParcela').val(cheque.talaoCheque.id);
			$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(cheque.numero);
			$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(cheque.data);
			$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val(cheque.obs);
		} else if($(this).val() == 'REC') {
			$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoParcela').val(cheque.id);
			$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoParcela').val(cheque.banco.id);
			$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoParcela').val(cheque.numero);
			$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoParcela').val(cheque.data);
			$('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoParcela').val(cheque.nome.id);
			$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoParcela').val(cheque.obs);
		}
	} else {
		$('input[name^="lancamentoParcela.cheque"]', '#frmLancamentoParcela').val("");
	}
});


function enableDisableBtCartaoCredito() {
	if($('#lancamentoParcelaTipo').val() == 'DES' && $('#lancamentoParcelaMeio').val() == 'CCR') {
		$('#btLancamentoParcelaCartaoCredito').prop('disabled', false);
	} else {
		$('#btLancamentoParcelaCartaoCredito').prop('disabled', true);
	}
	lightDarkBtCartaoCredito();
}


function enableDisableBtConta() {
	if(
	$('#lancamentoParcelaMeio').val() != 'CCR' && $('#lancamentoParcelaMeio').val() != 'CHQ' 
	|| 
		(
				($('#lancamentoParcelaMeio').val() == 'CHQ' || $('#lancamentoParcelaMeio').val() == 'CCR') 
				&& $('#lancamentoParcelaTipo').val() == 'REC'
		)
	) {
		$('#btLancamentoParcelaConta').prop('disabled', false);
	} else {
		$('#btLancamentoParcelaConta').prop('disabled', true);
	}
	lightDarkBtConta();
}


function enableDisableBtCheque() {
	if($('#lancamentoParcelaMeio').val() == 'CHQ') {
		$('#btLancamentoParcelaCheque').prop('disabled', false);
	} else {
		$('#btLancamentoParcelaCheque').prop('disabled', true);
	}
	lightDarkBtCheque();
}


function enableDisableBtBoleto() {
	$('#btLancamentoParcelaBoleto').prop('disabled', $('#lancamentoParcelaTipo').val() == 'REC');
	lightDarkBtBoleto();
}


function lightDarkBtCartaoCredito() {
	if($('#lancamentoParcelaCartaoCredito').val() && !$('#btLancamentoParcelaCartaoCredito').prop('disabled')) {
		$('#btLancamentoParcelaCartaoCredito').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoParcelaCartaoCredito').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtConta() {
	if($('#lancamentoParcelaConta').val() && !$('#btLancamentoParcelaConta').prop('disabled')) {
		$('#btLancamentoParcelaConta').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoParcelaConta').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtCheque() {
	
	var isCheque = false;
	if($('#lancamentoParcelaTipo').val() == 'DES') {
		var cheque = $('#frmLancamentoParcela').data('chequedes');
		if(typeof cheque.talaoCheque != 'undefined' && cheque.numero) {
			isCheque = true;
		}
	}
	if($('#lancamentoParcelaTipo').val() == 'REC') {
		var cheque = $('#frmLancamentoParcela').data('chequerec');
		if(typeof cheque.numero != 'undefined' && cheque.numero) {
			isCheque = true;
		}
	}
	
	if(isCheque && !$('#btLancamentoParcelaCheque').prop('disabled')) {
		$('#btLancamentoParcelaCheque').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoParcelaCheque').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtBoleto() {
	if($('#lancamentoParcelaBoletoNumero').val() && !$('#btLancamentoParcelaBoleto').prop('disabled')) {
		$('#btLancamentoParcelaBoleto').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoParcelaBoleto').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtArquivo() {
	if($('#lancamentoParcelaArquivoNome').val() && !$('#btLancamentoParcelaArquivo').prop('disabled')) {
		$('#btLancamentoParcelaArquivo').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoParcelaArquivo').removeClass('btn-primary').addClass('btn-default');
	}
}
lightDarkBtArquivo();


$('#frmLancamentoParcela').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/lancamentoParcela',
		type: 'POST',
		data: $(this).add($('#frmLancamentoCadastro')).serialize(),
		success: function(data) {
			if(typeof data == 'string') {
				$('#layLancamentoParcelaList').replaceWith(data);
				$('#dialogLancamentoParcela').trigger('close').modal('hide');
				enableDisableLancamentoParcelaBt();
			} else {
				error(data, $('#frmLancamentoParcela'));
			}
		}
	}).always(function(){
		loading(false);
	});
	return false;
});