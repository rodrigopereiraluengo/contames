$('#dialogEditarCompensacaoItem').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#compensacaoItemData').inputCalendar();


$('#compensacaoItemFavorecido').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});


$('#compensacaoItemItem').autoCompleteEditable({
	onSuccess: function() { return 'reload'; }
}).on('autoComplete.select', function(e, $li) {
	var tipo = $('#frmEditarCompensacaoItem').data('tipo');
	if($li.data('valor')) {
		var $valor = tipo == 'DES' ? $('#compensacaoItemDebito') : $('#compensacaoItemCredito');
		
		if($li.data('valor') != $valor.val()) {
			msgConfirm(i18n[i18n.locale]['compensacaoItem.' + tipo + '.valor.atualizar.confirm'], function() {
				$valor.val($li.data('valor'));
			});
		}
	}
	
}).on('setValue', function(e, item) {
	$(this).val(item.nome);
});

$('#compensacaoItemDebito, #compensacaoItemCredito').formatDecimal().toSelect();

$('#compensacaoItemDebito').change(function() {
	var debito = parseFloat($('#compensacaoItemDebito').val().unformat());
	var saldoAtual = $('#tbyCompensacaoItem tr.active').prev().length ? $('#tbyCompensacaoItem tr.active').prev().children('td:eq(5)').text().toFloat() : $('#compensacaoSaldoAnterior').text().toFloat();
	var saldo = saldoAtual;
	if(!isNaN(debito)) {
		saldo -= debito;
	}
	$('#compensacaoItemSaldo').removeClass('font-red font-blue').text(saldo.formatDecimal()).addClass(saldo < 0 ? 'font-red' : 'font-blue');
});

$('#compensacaoItemCredito').change(function() {
	var credito = parseFloat($('#compensacaoItemCredito').val().unformat());
	var saldoAtual = $('#tbyCompensacaoItem tr.active').prev().length ? $('#tbyCompensacaoItem tr.active').prev().children('td:eq(5)').text().toFloat() : $('#compensacaoSaldoAnterior').text().toFloat();
	var saldo = saldoAtual;
	if(!isNaN(credito)) {
		saldo += credito;
	}
	$('#compensacaoItemSaldo').removeClass('font-red font-blue').text(saldo.formatDecimal()).addClass(saldo < 0 ? 'font-red' : 'font-blue');
});

$('#frmEditarCompensacaoItem').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/compensacao/editarCompensacaoItem',
		type: 'POST',
		data: $(this).add($('#compensacaoViewid')).serialize() + '&data=' + $('#compensacaoData').val(),
		success: function(result) {
			if(typeof result == 'string') {
				var tipo = $('#frmEditarCompensacaoItem').data('tipo');
				$('#layCompensacaoItem').replaceWith(result);
				$('#dialogEditarCompensacaoItem').modal('hide');
				compensacaoCalcularDiferenca();
				enableDisableCompensacaoItemBt();
				
				msgSuccess(i18n[i18n.locale]['compensacaoItem.item.' + tipo + '.editado.success']);
				
			} else {
				error(result, $('#frmEditarCompensacaoItem'));
			}
		}
	}).always(function() {
		loading(false);
	});
	
	return false;
});