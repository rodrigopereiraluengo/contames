$(function() {
	
	// Classificacao
	$('#relatorioClassificacaoClassificacao').tokenInput(contextPath + '/classificacao/multipleAutoComplete', {
        preventDuplicates: true,
        minChars: 1,
        searchDelay: 500,
        animateDropdown: false,
        resultsFormatter: function(classificacao) { return '<li>' + classificacao.name + '</li>'; },
        tokenFormatter: function(classificacao) { return '<li><p>' + classificacao.name + '</p></li>'; },
        onAdd: function(classificacao) {
            $('#frmRelatorioClassificacao').append('<input type="hidden" name="classificacaoList[]" value="' + classificacao.id + '" />');
        },
        onDelete: function(classificacao) {
        	$('#frmRelatorioClassificacao').find('input[name="classificacaoList[]"][value=' + classificacao.id + ']').remove();
        }
    });
	
	// Status
	$('#relatorioClassificacaoStatus').multiselect({
		buttonWidth: '100%', 
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
	});
	
	// Formulario
	$('#frmRelatorioClassificacao').formSearch({
		multipleSelectRows: false,
		selectRow: true
	}).one('scrollEnd', function() {
		relatorioClassificacaoDesOpenChart();
		relatorioClassificacaoRecOpenChart();
	}).on('scroll', function() {
		relatorioClassificacaoDesOpenChart();
		relatorioClassificacaoRecOpenChart();
	}).on('formSearch.order', function() {
		labelsDespesas = [];
		labelsReceitas = [];
		despesas = [];
		receitas = [];
	});
	
	$('#btSearch').click(function() {
		$('#frmRelatorioClassificacao').one('scrollEnd', function() {
			relatorioClassificacaoDesOpenChart();
			relatorioClassificacaoRecOpenChart();
		});
		labelsDespesas = [];
		labelsReceitas = [];
		despesas = [];
		receitas = [];
	});
	
	
	$('#btExportXls').click(function() {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_classificacao.xlsx?' + $('#frmRelatorioClassificacao').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	$('#btExportPdf').click(function() {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_classificacao.pdf?' + $('#frmRelatorioClassificacao').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	// Grafico
	relatorioClassificacaoDesOpenChart();
	relatorioClassificacaoRecOpenChart();
	
	$('#isCCred').bootstrapToggle({
		on: i18n.get('MeioPagamento.CCR'),
		off: i18n.get('DocTipo.FT')
    }).change(function() {
    	$('#frmRelatorioClassificacao').setHidden('isCCred', $(this).prop('checked')).submit();
    	msgSuccess($(this).prop('checked') ? i18n.get('exibirMovsCCred') : i18n.get('exibirFaturas'));
    });
	
});


/**
 * Abre Tree Classificacao
 * @param classificacao_id
 */
function relatorioClassificacaoOpenClassif(self, _path) {
	var $tr = $(self).closest('tr');
	if(!$tr.data('loaded')) {
		$tr.data('loaded', true);
		loading(true);
		$.ajax({
			url: _path,
			data: $('#frmRelatorioClassificacao').serialize(),
			success: function(result) {
				if(typeof result == 'string') {
					$trLoaded = $(result);
					$tr.after($trLoaded);
					$tr.data({'trLoaded': $trLoaded, 'opened': true});
					$tr.find('i.fa').removeClass('fa-plus-square').addClass('fa-minus-square');
				} else {
					error(result, $('#frmRelatorioClassificacao'));
				}
			}
		}).always(function(){
			loading(false);
		});
	} else {
		
		if($tr.data('opened')) {
			hideTr($tr);
		} else {
			showTr($tr);
		}
		
	}
	
}

function hideTr($tr) {
	if($tr.data('opened')) {
		$tr.data('opened', false).find('i.fa').removeClass('fa-minus-square').addClass('fa-plus-square');
		if($tr.data('trLoaded')) {
			$tr.data('trLoaded').each(function() {
				hideTr($(this));
			}).hide();
		}
	}
}

function showTr($tr) {
	if(!$tr.data('opened')) {
		$tr.data('opened', true).find('i.fa').removeClass('fa-plus-square').addClass('fa-minus-square');
		if($tr.data('trLoaded')) {
			$tr.data('trLoaded').show();
		}
	}
}


/**
 * Abre Janela de Item
 * @param item_id
 */
function relatorioClassificacaoOpenItem(item_id) {
	loading(true);
	$.ajax({
		url: contextPath + '/item/cadastro',
		data: {id: item_id},
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioClassificacao'));
			}
		}
	}).always(function(){
		loading(false);
	});
}


/**
 * Abre Janela Movimentos
 * @param data
 */
function relatorioClassificacaoOpenMovs(data) {
	
	data.ano = $('#relatorioClassificacaoAno').val();
	data.tipo = $('#relatorioClassificacaoTipo').val();
	data.isCCred = $('#isCCred').prop('checked');
	if(typeof data.classificacao_id == 'undefined' || data.classificacao_id == 0) {
		if(!$('input[name="classificacaoList[]"]').each(function(i) {
			data['classificacaoList[' + i + ']'] = $(this).val();
		}).length) {
			data['classificacaoList[0]'] = data.classificacao_id;
		}
	} else {
		data['classificacaoList[0]'] = data.classificacao_id;
		delete data.classificacao_id;
	}
	
	$('#relatorioClassificacaoStatus :selected').each(function(i, option) {
		data['statusList[' + i + ']'] = $(option).val();
	});
	
	loading(true);
	$.ajax({
		url: contextPath + '/relatorioClassificacao/movs',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioClassificacao'));
			}
		}
	}).always(function(){
		loading(false);
	});
}

function formatDecimal(val) {
	 return $.formatNumber(val, {format: "#,##0.".repeat(2, "0")});
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

Chart.defaults.global.responsive = true;
Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=formatDecimal(value)%>";
Chart.defaults.global.multiTooltipTemplate = "<%=formatDecimal(value)%>";


var chartRelatorioClassificacaoDes;
var toRelatorioClassificacaoDesOpenChart;
function relatorioClassificacaoDesOpenChart() {
	window.clearTimeout(toRelatorioClassificacaoDesOpenChart);
	toRelatorioClassificacaoDesOpenChart = window.setTimeout(function() {
	
		if(despesas.length || receitas.length) {
			$('#layChart').show();
		}
		
		if(typeof chartRelatorioClassificacaoDes != 'undefined'){ 
			chartRelatorioClassificacaoDes.destroy();
			$('#chartRelatorioClassificacaoDes').replaceWith('<canvas id="chartRelatorioClassificacaoDes" class="rel-canvas-chart"></canvas>');
		}
		
		if(typeof labelsDespesas != 'undefined' && despesas.length) {
		
			if(labelsDespesas.length || labelsReceitas.length) { $('#layChart').show(); }
			
			var ctxChart = document.getElementById("chartRelatorioClassificacaoDes").getContext("2d");
			var data = [];
			
			$.each(labelsDespesas, function(i, val) {
				data.push({label : val, value : despesas[i], color: getRandomColor()});
			});
			
			
			chartRelatorioClassificacaoDes = new Chart(ctxChart).Pie(data, {responsive: true, maintainAspectRatio: true, scaleLabel: function(valuePayload) {
				return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
			}});
		
		}
	}, 500);
}

var chartRelatorioClassificacaoRec;
var toRelatorioClassificacaoRecOpenChart;
function relatorioClassificacaoRecOpenChart() {
	window.clearTimeout(toRelatorioClassificacaoRecOpenChart);
	toRelatorioClassificacaoRecOpenChart = window.setTimeout(function() {
		
		if(typeof chartRelatorioClassificacaoRec != 'undefined'){ 
			chartRelatorioClassificacaoRec.destroy();
			$('#chartRelatorioClassificacaoRec').replaceWith('<canvas id="chartRelatorioClassificacaoRec" class="rel-canvas-chart"></canvas>');
		}
		
		if(typeof labelsReceitas != 'undefined' && receitas.length) { 
		
			if(labelsDespesas.length || labelsReceitas.length) { $('#layChart').show(); }
			
			var ctxChart = document.getElementById("chartRelatorioClassificacaoRec").getContext("2d");
			var data = [];
			
			$.each(labelsReceitas, function(i, val) {
				data.push({label : val, value : receitas[i], color: getRandomColor()});
			});
			
			chartRelatorioClassificacaoRec = new Chart(ctxChart).Pie(data, {responsive: true, scaleLabel: function(valuePayload) {
				return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
			}});
		}
	}, 500);
}