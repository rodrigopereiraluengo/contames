$(function() {
	
	
	// Favorecido
	$('#relatorioFavorecidoFavorecido').tokenInput(contextPath + '/pessoa/multipleAutoComplete', {
        preventDuplicates: true,
        minChars: 1,
        searchDelay: 500,
        animateDropdown: false,
        resultsFormatter: function(pessoa) { return '<li>' + pessoa.name + '</li>'; },
        tokenFormatter: function(pessoa) { return '<li><p>' + pessoa.name + '</p></li>'; },
        onAdd: function(pessoa) {
            $('#frmRelatorioFavorecido').append('<input type="hidden" name="favorecidoList[]" value="' + pessoa.id + '" />');
        },
        onDelete: function(pessoa) {
        	$('#frmRelatorioFavorecido').find('input[name="favorecidoList[]"][value=' + pessoa.id + ']').remove();
        }
    });
	
	
	// Status
	$('#relatorioFavorecidoStatus').multiselect({
		buttonWidth: '100%', 
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
	});
	
	
	// Formulario
	$('#frmRelatorioFavorecido').formSearch({
		multipleSelectRows: false,
		selectRow: true
	}).one('scrollEnd', function() {
		relatorioChart();
	}).on('scroll', function() {
		relatorioChart();
	}).on('formSearch.order', function() {
		labels = [];
		values = [];
	});
	
	
	// Botao pesquisar
	$('#btSearch').click(function() {
		$('#frmRelatorioFavorecido').one('scrollEnd', function() {
			relatorioChart();
		});
		labels = [];
		values = [];
	});
	
	
	// Exportar Excel
	$('#btExportXls').click(function(e) {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_' + ($('#tipo').val() == 'DES' ? 'pagara' : 'receberde') + '.xlsx?' + $('#frmRelatorioFavorecido').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	// Exportar PDF
	$('#btExportPdf').click(function(e) {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_' + ($('#tipo').val() == 'DES' ? 'pagara' : 'receberde') + '.pdf?' + $('#frmRelatorioFavorecido').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	// Grafico
	relatorioChart();
	
	$('#isCCred').bootstrapToggle({
		on: i18n.get('MeioPagamento.CCR'),
		off: i18n.get('DocTipo.FT')
    }).change(function() {
    	$('#frmRelatorioFavorecido').setHidden('isCCred', $(this).prop('checked')).submit();
    	msgSuccess($(this).prop('checked') ? i18n.get('exibirMovsCCred') : i18n.get('exibirFaturas'));
    });
	
});


/**
 * Abre Janela Cadatro Favorecido
 * @param favorecido_id
 */
function relatorioFavorecidoOpenFav(favorecido_id) {
	if(favorecido_id) {
		loading(true);
		$.ajax({
			url: contextPath + '/pessoa/cadastro',
			data: {id: favorecido_id},
			success: function(result) {
				if(typeof result == 'string') {
					$('body').append(result);
				} else {
					error(result, $('#frmRelatorioFavorecido'));
				}
			}
		}).always(function(){
			loading(false);
		});
	}
}


/**
 * Abre Janela Movimentos
 * @param data
 */
function relatorioFavorecidoOpenMovs(data) {
	
	data.ano = $('#relatorioFavorecidoAno').val();
	data.isCCred = $('#isCCred').prop('checked');
	if(typeof data.tipo == 'undefined') { data.tipo = $('#relatorioFavorecidoTipo').val(); }
	
	if(typeof data.favorecido_id == 'undefined' || data.favorecido_id == 0) {
		if(!$('input[name="favorecidoList[]"]').each(function(i) {
			data['favorecidoList[' + i + ']'] = $(this).val();
		}).length) {
			data['favorecidoList[0]'] = data.favorecido_id;
		}
	} else {
		data['favorecidoList[0]'] = data.favorecido_id;
		delete data.favorecido_id;
	}
	
	$('#relatorioFavorecidoStatus :selected').each(function(i, option) {
		data['statusList[' + i + ']'] = $(option).val();
	});
	
	loading(true);
	$.ajax({
		url: contextPath + '/relatorioFavorecido/movs',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioFavorecido'));
			}
		}
	}).always(function(){
		loading(false);
	});
}

function formatDecimal(val) {
	 return $.formatNumber(val, {format: "#,##0.".repeat(2, "0")});
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

Chart.defaults.global.responsive = true;
Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=formatDecimal(value)%>";
Chart.defaults.global.multiTooltipTemplate = "<%=formatDecimal(value)%>";


var chartRelatorio;
var toRelatorioChart;
function relatorioChart() {
	window.clearTimeout(toRelatorioChart);
	toRelatorioChart = window.setTimeout(function() {
	
		if(values.length) {
			$('#layChart').show();
		}
		
		if(typeof chartRelatorio != 'undefined') { 
			chartRelatorio.destroy();
			$('#chartRelatorio').replaceWith('<canvas id="chartRelatorio" class="rel-canvas-chart"></canvas>');
		}
		
		if(typeof labels != 'undefined' && values.length) { 
		
			var ctxChart = document.getElementById("chartRelatorio").getContext("2d");
			var data = [];
			
			$.each(labels, function(i, val) {
				data.push({label : val, value : values[i], color: getRandomColor()});
			});
			
			
			chartRelatorio = new Chart(ctxChart).Pie(data, {responsive: true, scaleLabel: function(valuePayload) {
				return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
			}});
		
		}
	}, 500);
}