$(function() {
	$('#frmTransferenciaConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.transferencia.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.transferencia.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.transferencia.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.transferencia.success.p')
	});
});

function transferenciaExcluirSuccess(transferencia) {
	$('#frmTransferenciaConsultar').trigger('excluir.success', [transferencia]);
}