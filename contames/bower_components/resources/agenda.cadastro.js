/**
 * Janela Agenda
 */
$('#dialogAgenda').modal({ show: true, keyboard: false, backdrop: 'static' }).on('hidden.bs.modal', function() {
	$(this).remove();
});

window.setTimeout(function(){ $('#agendaNome').focus(); }, 200);


$('#agendaInicio').inputDateTime();

$('#agendaFim').inputDateTime();

$('#agendaPessoa').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});



/**
 * Submeter Formulario
 */
$('#frmAgenda').submit(function() {
	
	loading(true);
	$.ajax({
		url: contextPath + '/agenda/cadastro',
		type: 'POST',
		dataType: 'jsonp',
		data: $(this).serialize()
	}).always(function() {
		loading(false);
	});
	
	return false;
});


function agendaCadastroError(errors) {
	error(errors, $('#frmAgenda'));
}


function agendaCadastroSuccess(agenda) {
	$('#dialogAgenda').trigger('success', [agenda[0]]).modal('hide');
	
}