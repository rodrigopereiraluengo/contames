$('#dialogTransferenciaCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});



$('#transferenciaData').inputCalendar().toSelect();



$('#transferenciaConta').comboEditable().on('loadOptionsComplete', function() {
	var destino = $('#transferenciaDestino').val();
	$('#transferenciaDestino').html($(this).html()).val(destino).change();
});



$('#transferenciaCartaoCredito').on('change.isConfirmSaldoCartaoCredito', function() {
	$('#frmTransferenciaCadastro').setHidden('isConfirmSaldoCartaoCredito', false);
}).comboEditable().change(function() {
	if($('#transferenciaCartaoCredito :selected').data('limite')) {
		$('#btTransferenciaIsLimite').prop('disabled', false);
	} else {
		$('#btTransferenciaIsLimite').prop('disabled', true);
	}
	lightDarkBtTransferenciaIsLimite();
}).change();



$('#transferenciaDestino').comboEditable().on('loadOptionsComplete', function() {
	var conta = $('#transferenciaConta').val();
	$('#transferenciaConta').html($(this).html()).val(conta).change();
});



$('#transferenciaConta, #transferenciaValor').on('change.isConfirmSaldoConta', function() {
	$('#frmTransferenciaCadastro').setHidden('isConfirmSaldoConta', false);
});



if($('#transferenciaConta').val() && $('input[name="transferencia.cheque.id"]').val()) {
	$('#transferenciaConta').data('cheque_' + $('#transferenciaConta').val(), {
		id: $('input[name="transferencia.cheque.id"]').val(),
		talaoCheque: {id: $('input[name="transferencia.cheque.talaoCheque.id"]').val()},
		numero: $('input[name="transferencia.cheque.numero"]').val(),
		obs: $('input[name="transferencia.cheque.obs"]').val()
	});
}



$('#transferenciaConta').on('change.enableDisableBtCheque', function() {
	var cheque = $('#transferenciaConta').data('cheque_' + $('#transferenciaConta').val());
	if(typeof cheque == 'undefined') {
		$('input[name="transferencia.cheque.id"]').val('');
		$('input[name="transferencia.cheque.talaoCheque.id"]').val('');
		$('input[name="transferencia.cheque.numero"]').val('');
		$('input[name="transferencia.cheque.obs"]').val('');
	} else {
		$('input[name="transferencia.cheque.id"]').val(cheque.id);
		$('input[name="transferencia.cheque.talaoCheque.id"]').val(cheque.talaoCheque.id);
		$('input[name="transferencia.cheque.numero"]').val(cheque.numero);
		$('input[name="transferencia.cheque.obs"]').val(cheque.obs);
	}
	enableDisableBtCheque();
}).trigger('change.enableDisableBtCheque');



$('#transferenciaTipo').change(function() {
	enableDisableBtCheque();
	if($(this).val() == 'CON') {
		$('#layTransferenciaCartaoCredito').hide();
		$('#layTransferenciaConta').show();
	} else {
		$('#layTransferenciaCartaoCredito').show();
		$('#layTransferenciaConta').hide();
	}
}).change();



/**
 * Cheque
 */
$('#btTransferenciaCheque').click(function() {
	
	loading(true);
	
	var cheque = $('#transferenciaConta').data('cheque_' + $('#transferenciaConta').val());
	if(typeof cheque == 'undefined') {
		cheque = {
			id: '',
			talaoCheque: {id: ''},
			numero: '',
			obs: ''
		};
	}
	
	$.ajax({
		url: contextPath + '/talaoCheque/dialogSelect',
		data: {
			'cheque.id': cheque.id,
			'cheque.talaoCheque.id': cheque.talaoCheque.id,
			'cheque.numero': cheque.numero,
			'isData': false,
			'cheque.obs': cheque.obs,
			'conta': $('#transferenciaConta').val()
		},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			
			$dialog.on('close', function(e, cheque) {
				
				if(typeof cheque.talaoCheque != 'undefined') {
					
					$('#transferenciaConta')
						.val(cheque.talaoCheque.conta.id)
						.data('cheque_' + cheque.talaoCheque.conta.id, cheque);
										
					if($('input[name="transferencia.cheque.talaoCheque.id"]').val() != cheque.talaoCheque.id || $('input[name="transferencia.cheque.numero"]').val() != cheque.numero){
						msgSuccess(i18n[i18n.locale]['cheque.selecionado.success'].template({"0": cheque.numero, "1": cheque.talaoCheque.conta.nome}));
					}
					$('input[name="transferencia.cheque.id"]').val(cheque.id);
					$('input[name="transferencia.cheque.talaoCheque.id"]').val(cheque.talaoCheque.id);
					$('input[name="transferencia.cheque.numero"]').val(cheque.numero);
					$('input[name="transferencia.cheque.obs"]').val(cheque.obs);
				
				} else {
					
					$('#transferenciaConta').removeData('cheque_' + $('#transferenciaConta').val());
					
					if($('input[name="transferencia.cheque.talaoCheque.id"]').val() != "" && $('input[name="transferencia.cheque.numero"]').val() != ""){
						msgSuccess(i18n[i18n.locale]['cheque.nenhum.selecionado']);
					}
					$('input[name="transferencia.cheque.id"]').val("");
					$('input[name="transferencia.cheque.talaoCheque.id"]').val("");
					$('input[name="transferencia.cheque.numero"]').val("");
					$('input[name="transferencia.cheque.obs"]').val("");
				
				}
								
				lightDarkBtCheque();
				
				$('#transferenciaConta, #transferenciaDestino').trigger('loadOptions');
				
			}).on('hidden.bs.modal', function() {
				$('#transferenciaConta, #transferenciaDestino').trigger('loadOptions');
			});
			
		}
	}).always(function() {
		loading(false);
	});
	
});



function enableDisableBtCheque() {
	if($('#transferenciaTipo').val() == 'CON' && $('#transferenciaConta').val()) {
		$('#btTransferenciaCheque').prop('disabled', false);
	} else {
		$('#btTransferenciaCheque').prop('disabled', true);
	}
	lightDarkBtCheque();
}



function lightDarkBtCheque() {
	
	if($('#transferenciaConta').data('cheque_' + $('#transferenciaConta').val()) && !$('#btTransferenciaCheque').prop('disabled')) {
		$('#btTransferenciaCheque').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btTransferenciaCheque').removeClass('btn-primary').addClass('btn-default');
	}
}


/**
 * Arquivo
 */
$('#btTransferenciaArquivo').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/arquivo/dialogAnexo',
		data: {
				viewid: $('#transferenciaViewid').val(),
				target: 'transferencia',
				'arquivo.id': $('#transferenciaArquivoId').val(),
				'arquivo.temp': $('#transferenciaArquivoTemp').val(),
				'arquivo.nome': $('#transferenciaArquivoNome').val(),
				'arquivo.contentType': $('#transferenciaArquivoContentType').val(),
				'arquivo.size': $('#transferenciaArquivoSize').val()
		},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, arquivo) {
				
				if($('#transferenciaArquivoNome').val() != arquivo.nome || $('#transferenciaArquivoContentType').val() != arquivo.contentType || $('#transferenciaArquivoSize').val() != arquivo.size) {
					if(arquivo.nome) {
						msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#transferenciaArquivoNome').val()}));
					}
				}
				
				$('#transferenciaArquivoNome').val(arquivo.nome);
				$('#transferenciaArquivoContentType').val(arquivo.contentType);
				$('#transferenciaArquivoSize').val(arquivo.size);
				
				lightDarkBtArquivo();
				
			});
		}
	}).always(function() {
		loading(false);
	});
	
});
function lightDarkBtArquivo() {
	if($('#transferenciaArquivoNome').val() && !$('#btTransferenciaArquivo').prop('disabled')) {
		$('#btTransferenciaArquivo').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btTransferenciaArquivo').removeClass('btn-primary').addClass('btn-default');
	}
}
lightDarkBtArquivo();



$('#btTransferenciaIsLimite').click(function() {
	if($('#transferenciaIsLimite').val().toBoolean()) {
		msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.no']);
		$('#transferenciaIsLimite').val(false);
	} else {
		msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.yes']);
		$('#transferenciaIsLimite').val(true);
	}
	lightDarkBtTransferenciaIsLimite();
});

function lightDarkBtTransferenciaIsLimite() {
	if($('#transferenciaIsLimite').val().toBoolean() && !$('#btTransferenciaIsLimite').prop('disabled')) {
		$('#btTransferenciaIsLimite').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btTransferenciaIsLimite').removeClass('btn-primary').addClass('btn-default');
	}
}
lightDarkBtTransferenciaIsLimite();




$('#transferenciaValor').formatDecimal();


$('#frmTransferenciaCadastro').submit(function() {
	
	loading(true);
	$.ajax({
		url: contextPath + '/transferencia/cadastro',
		type: 'POST',
		dataType: 'jsonp',
		data: $(this).serialize()
	}).always(function() {
		loading(false);
	});
	
	return false;
});



function transferenciaCadastroError(errors) {
	error(errors, $('#frmTransferenciaCadastro'));
}


function transferenciaCadastroSuccess(transferencia) {
	$('#dialogTransferenciaCadastro').trigger('success', [transferencia]).modal('hide');
	msgSuccess(i18n[i18n.locale]['transferencia.cadastro.success']);
}