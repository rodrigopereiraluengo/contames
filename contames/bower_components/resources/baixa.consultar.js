$(function() {
	$('#frmBaixaConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.baixa.' + $('#frmBaixaConsultar').data('tipo') + '.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.baixa.' + $('#frmBaixaConsultar').data('tipo') + '.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.baixa.' + $('#frmBaixaConsultar').data('tipo') + '.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.baixa.' + $('#frmBaixaConsultar').data('tipo') + '.success.p')
	});
});

function baixaExcluirSuccess(baixa) {
	$('#frmBaixaConsultar').trigger('excluir.success', [baixa]);
}