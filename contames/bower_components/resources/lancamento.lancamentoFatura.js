$('#dialogLancamentoFatura').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#frmLancamentoFatura').formSearch();

$('#lancamentoFaturaDataDe, #lancamentoFaturaDataAte').inputCalendar();
$('#lancamentoFaturaDataDe').toSelect();

$('#frmLancamentoFatura').on('row.select', function() {
	var selected = $(this).data('selected') || [];
	$('#btLancamentoFaturaOk').prop('disabled', !selected.length);
});


$('#btLancamentoFaturaOk').click(function() {
	
	loading(true);
	
	var data = {viewid: $('#lancViewid').val(), id: $('#lancCartaoCredito').val()};
	
	var $selected = $('#frmLancamentoFatura').data('selected');
	var len = $selected.length;
	var indexLancamentoParcela = 0;
	var indexTransferencia = 0;
	for(var i = 0; i < $('#frmLancamentoFatura').data('selected').length; i++) {
		if($('#frmLancamentoFatura').data('selected')[i].lancamentoParcelaId) {
			data['ids[' + indexLancamentoParcela + ']'] = $('#frmLancamentoFatura').data('selected')[i].lancamentoParcelaId;
			indexLancamentoParcela++;
		} else {
			data['transfIds[' + indexTransferencia + ']'] = $('#frmLancamentoFatura').data('selected')[i].transferenciaId;
			indexTransferencia++;
		}
	}
	
	$.ajax({
		url: contextPath + '/lancamento/adicionarLancamentoFatura',
		type: 'POST',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {

				var message = i18n[i18n.locale]['lancamentoFatura.adicionado.success'];
				if(len > 1) {
					message = i18n[i18n.locale]['lancamentoFatura.adicionado.success.p'].template({"0": len});
				}
				
				$('#layLancamentoFaturaList').replaceWith(result);
				$('#dialogLancamentoFatura').modal('hide');
				$('#lancValorDoc').change();
				msgSuccess(message);
			} else {
				error(result, $('#frmLancamentoFatura'));
			}
		}
	}).always(function() {
		loading(false);
	});
	
});