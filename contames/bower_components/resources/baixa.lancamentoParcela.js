$('#dialogBaixaLancamentoParcela').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#frmBaixaLancamentoParcela').formSearch();

$('#baixaLancamentoParcelaDataDe, #baixaLancamentoParcelaDataAte').inputCalendar();
$('#baixaLancamentoParcelaDataDe').toSelect();

$('#frmBaixaLancamentoParcela').on('row.select', function() {
	var selected = $(this).data('selected') || [];
	$('#btBaixaLancamentoParcelaOk').prop('disabled', !selected.length);
});


$('#btBaixaLancamentoParcelaOk').click(function() {
	
	loading(true);
	
	var data = {viewid: $('#baixaViewid').val()};
	
	var $selected = $('#frmBaixaLancamentoParcela').data('selected');
	var len = $selected.length;
	
	for(var i = 0; i < $('#frmBaixaLancamentoParcela').data('selected').length; i++) {
		data['ids[' + i + ']'] = $('#frmBaixaLancamentoParcela').data('selected')[i];
	}
	
	$.ajax({
		url: contextPath + '/baixa/adicionarLancamentoParcela',
		type: 'POST',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {

				var message = i18n[i18n.locale]['lancamentoParcela.' + $('#baixaLancamentoParcelaTipo').val() + '.adicionado.success'];
				if(len > 1) {
					message = i18n[i18n.locale]['lancamentoParcela.' + $('#baixaLancamentoParcelaTipo').val() + '.adicionado.success.p'].template({"0": len});
				}
				
				$('#layBaixaItem').replaceWith(result);
				$('#dialogBaixaLancamentoParcela').modal('hide');
				
				msgSuccess(message);
			} else {
				error(result, $('#frmBaixaLancamentoParcela'));
			}
		}
	}).always(function() {
		loading(false);
	});
	
});