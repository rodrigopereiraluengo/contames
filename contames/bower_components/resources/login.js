$(function() {
	
	$('#email').focus();
	
	$('#frmLogin').submit(function() {
		
		loading(true);
		$.ajax({
			url: $(this).attr('action'),
			type: $(this).attr('method'),
			data: $(this).serialize(),
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
		
		return false;
	});
	
	$('#frmLogin :submit').prop('disabled', false);
	
});

function success(result) {
	$('#frmLogin').find(':submit').prop('disabled', true);
	loading(true);
	msgSuccess(i18n.get('login.success'));
	
	window.location.href = result.string ? result.string : contextPath + '/resumo';
}