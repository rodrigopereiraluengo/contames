$('#dialogSelectTalaoCheque').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});


$('#dialogSelectTalaoChequeNumero').comboEditable({id: function() {
	return $('#dialogSelectTalaoChequeId').val();
},
loadOptions: function() {
	return contextPath + '/talaoCheque/carregaSelect?numero=' + $('#dialogSelectTalaoChequeNumero').val();
}
}).on('newLoad', function(e, $dialog) {
	
	$dialog.on('close', function(e, conta) {
		
		loading(true);
		
		$.ajax({
			url: contextPath + '/talaoCheque/cadastro',
			data: {contaId: conta.id},
			success: function(data) {
				if(typeof data == 'string') {
					var $dialog = $(data);
					$('body').append($dialog);
					
					$dialog.on('success', function(e, talaoCheque) {
						$('#dialogSelectTalaoChequeNumero').trigger('loadOptions', [talaoCheque]);
					});
					
				} else {
					error(data);
				}
			}
		}).always(function() {
			loading(false);
		});
		
	});
	
	$dialog.on('hide.bs.modal', function() {
		$('#dialogSelectTalaoChequeNumero').trigger('loadOptions');
	});
	
}).change(function() {
	$('#dialogSelectTalaoChequeId').val($('option:selected', this).data('talaoId'));
});

if($('#dialogSelectTalaoChequeNumero').is('select')) {
	$('#dialogSelectTalaoChequeNumero').toFocus();	
} else {
	$('#dialogSelectTalaoChequeObs').toSelect();
}


$('#dialogSelectTalaoChequeData').inputCalendar();

if($('#dialogSelectTalaoChequeNumero').data('conta') && !$('#dialogSelectTalaoChequeNumero').val()) {
	$('#dialogSelectTalaoChequeNumero optgroup[data-conta=' + $('#dialogSelectTalaoChequeNumero').data('conta') + '] option:first').prop('selected', true);
} 

$('#frmDialogSelectTalaoCheque').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/talaoCheque/dialogSelect',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	}).always(function() {
		loading(false);
	});
	
	return false;
});


function dialogSelectTalaoChequeSuccess(cheque) {
	$('#dialogSelectTalaoCheque').trigger('close', [cheque]).modal('hide');
}