$('#dialogPessoaCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#pessoaRazaoSocial').toSelect();

$('#pessoaRazaoSocial, #pessoaFantasia, #pessoaCnpj, #pessoaInscEst, #pessoaInscMun').change(function(){
	$(this).data('value_' + $('#pessoaTipo').val(), $(this).val());
}).change();

if($('#pessoaRazaoSocial').data('val')) {
	$('#pessoaRazaoSocial')
		.val($('#pessoaRazaoSocial').data('val'))
		.data('value_J', $('#pessoaRazaoSocial').data('val'))
		.data('value_F', $('#pessoaRazaoSocial').data('val'));
	
}

$('#pessoaTipo').change(function() {
	
	resetFormValidator($('#frmPessoaCadastro'));
	
	$('#pessoaRazaoSocial, #pessoaFantasia, #pessoaCnpj, #pessoaInscEst, #pessoaInscMun').each(function() {
		$(this).val($(this).data('value_' + $('#pessoaTipo').val()));
	});
	
	if($(this).val() == 'J') {
		
		$('label[for=pessoaRazaoSocial]').html(i18n[i18n.locale]['razaoSocial'] + '&nbsp;<span class="required">*</span>');
		$('#pessoaRazaoSocial').attr('name', 'pessoa.razaoSocial');
		
		$('label[for=pessoaFantasia]').html(i18n[i18n.locale]['fantasia']);
		$('#pessoaFantasia').attr('name', 'pessoa.fantasia');
		
		$('label[for=pessoaCnpj]').html(i18n[i18n.locale]['cnpj']);
		$('#pessoaCnpj').attr('name', 'pessoa.cnpj').attr('maxlength', 18).unsetMask().setMask(i18n[i18n.locale]['cnpjMask']);
		
		$('label[for=pessoaInscEst]').html(i18n[i18n.locale]['inscEst']);
		$('#pessoaInscEst').attr('name', 'pessoa.inscEst');
		
		$('label[for=pessoaInscMun]').html(i18n[i18n.locale]['inscMun']);
		$('#pessoaInscMun').attr('name', 'pessoa.inscMun').attr('maxlength', 20);
		
		$('#layPessoaRazaoSocial, #layPessoaFantasia').removeClass('col-sm-5').addClass('col-sm-6');
		$('#layPessoaSexo').hide();
		
	} else {
		
		$('label[for=pessoaRazaoSocial]').html(i18n[i18n.locale]['nome'] + '&nbsp;<span class="required">*</span>');
		$('#pessoaRazaoSocial').attr('name', 'pessoa.nome');
		
		$('label[for=pessoaFantasia]').html(i18n[i18n.locale]['apelido']);
		$('#pessoaFantasia').attr('name', 'pessoa.apelido');
		
		$('label[for=pessoaCnpj]').html(i18n[i18n.locale]['cpf']);
		$('#pessoaCnpj').attr('name', 'pessoa.cpf').attr('maxlength', 14).unsetMask().setMask(i18n[i18n.locale]['cpfMask']);
		
		$('label[for=pessoaInscEst]').html(i18n[i18n.locale]['rg']);
		$('#pessoaInscEst').attr('name', 'pessoa.rg');
		
		$('label[for=pessoaInscMun]').html(i18n[i18n.locale]['tituloEleitor']);
		$('#pessoaInscMun').attr('name', 'pessoa.tituloEleitor').attr('maxlength', 14);
		
		$('#layPessoaRazaoSocial, #layPessoaFantasia').removeClass('col-sm-6').addClass('col-sm-5');
		$('#layPessoaSexo').show();
	}
}).change();

$('#pessoaCep').bind('keydown', 'return', function(e) {
	$('#btBuscarCep').click();
	return false;
}).setMask(i18n[i18n.locale]['cepMask']);


$('#btBuscarCep').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/cep/buscar',
		data: {cep: $('#pessoaCep').val(), errorFunction: 'pessoaCadastroError'},
		dataType: 'jsonp'
	}).always(function() {
		loading(false);
	});
});


$('#pessoaCategoria').comboEditable();
window.setTimeout(function(){
	$('#pessoaEmail, #pessoaSenha').prop('disabled', false);
},0);

function successCep(address) {
	
	$('#pessoaEndereco, #pessoaBairro, #pessoaCidade, #pessoaUf').val('');
	
	if(address.resultado == 1) {
		$('#pessoaEndereco').val(address.tipo_logradouro + ' ' + address.logradouro);
		$('#pessoaBairro').val(address.bairro);
		$('#pessoaNumero').toSelect();
	}
	
	if(address.resultado == 1 || address.resultado == 2) {
		$('#pessoaCidade').val(address.cidade);
		$('#pessoaUf').val(address.uf);
		if(address.resultado == 2) {
			$('#pessoaEndereco').toSelect();
		}
	}
}

$('#frmPessoaCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/pessoa/cadastro',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	}).always(function() {
		loading(false);
	});
	
	return false;
});

function pessoaCadastroError(errors) {
	error(errors, $('#frmPessoaCadastro'));
}

function pessoaCadastroSuccess(pessoa) {
	var isUsuario = $('#frmPessoaCadastro').data('usuario');
	try {
		$('#dialogPessoaCadastro').trigger('success', [pessoa]).modal('hide');
	} catch(e) {
		console.log(e);
	}
	
	msgSuccess(i18n[i18n.locale][(isUsuario ? 'usuario' : 'pessoa') + '.cadastro.success']);
}