$('#dialogBaixaItem').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#baixaItemValor, #baixaItemDiferenca').formatDecimal();


$('#baixaItemValor, #baixaItemDiferenca').change(function(e) {
	var lancamentoParcelaValor 	= parseFloat($('#baixaItemLancamentoParcelaValor').text().unformat());
	var valor = parseFloat($('#baixaItemValor').val().unformat());
	var diferenca = parseFloat($('#baixaItemDiferenca').val().unformat()); if(isNaN(diferenca)){ diferenca = 0; }
	if($(e.target).attr('id') == 'baixaItemValor') {
		diferenca = valor - lancamentoParcelaValor;
		$('#baixaItemDiferenca').val(diferenca.formatDecimal());
	} else if($(e.target).attr('id') == 'baixaItemDiferenca') {
		valor = lancamentoParcelaValor - -diferenca;
		$('#baixaItemValor').val(valor.formatDecimal());
	}
	if(diferenca == 0) {
		$('#baixaItemMotivo').prop('disabled', true);
		$('#baixaItemMotivo').next().children('button:first').prop('disabled', true);
	} else {
		$('#baixaItemMotivo').prop('disabled', false);
		$('#baixaItemMotivo').next().children('button:first').prop('disabled', false);
	}
}).change();


$('#baixaItemMotivo').comboEditable();


$('#btBaixaItemCartaoCredito').click(function() {
	if($('#lancamentoParcelaTipo').val()) {
		loading(true);
		$.ajax({
			url: contextPath + '/cartaoCredito/dialogSelect',
			data: {
				id: $('#baixaItemCartaoCredito').val(), 
				isLimite: $('#baixaItemIsLimite').val(),
				isReadOnly: $('#frmBaixaItem').data('isReadOnly')
			},
			success: function(data) {
				
				var $element = $(data);
				
				$('body').append($element);
				
				$element.on('close', function(e, cartaoCredito) {
					if($('#baixaItemCartaoCredito').val() != cartaoCredito[0].id) {
						if(cartaoCredito[0].id) {
							msgSuccess(i18n[i18n.locale]['cartaoCredito.selecionado'].template({"0": cartaoCredito[0].nome}));
						} else {
							msgSuccess(i18n[i18n.locale]['cartaoCredito.nenhum.selecionado']);
						}
					}
					$('#baixaItemCartaoCredito').val(cartaoCredito[0].id);
					$('#baixaItemIsLimite').val(cartaoCredito[1]);
					lightDarkBtCartaoCredito();
				});
				
			}
		}).always(function() {
			loading(false);
		});
	}
});


$('#btBaixaItemConta').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/conta/dialogSelect',
		data: {
			id: $('#baixaItemConta').val(),
			isReadOnly: $('#frmBaixaItem').data('isReadOnly') 
		},
		success: function(data) {
			var $element = $(data);
			$('body').append($element);
			
			$element.on('close', function(e, conta) {
				if($('#baixaItemConta').val() != conta.id) {
					if(conta.id) {
						msgSuccess(i18n[i18n.locale]['conta.selecionada'].template({"0": conta.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['conta.nenhuma.selecionada']);
					}
				}
				$('#baixaItemConta').val(conta.id);
				lightDarkBtConta();
			});
		}
	}).always(function() {
		loading(false);
	});
	
});


$('#btBaixaItemCheque').click(function() {

	loading(true);
	
	if($('#lancamentoParcelaTipo').val() == 'DES') {
		$.ajax({
			url: contextPath + '/talaoCheque/dialogSelect',
			data: {
				'lancamentoParcela.id': $('#lancamentoParcelaId').val(), 
				'cheque.id': $('input[name="baixaItem.cheque.id"]').val(),
				'cheque.talaoCheque.id': $('input[name="baixaItem.cheque.talaoCheque.id"]').val(),
				'cheque.numero': $('input[name="baixaItem.cheque.numero"]').val(),
				'cheque.data': $('input[name="baixaItem.cheque.data"]').val(),
				'cheque.obs': $('input[name="baixaItem.cheque.obs"]').val(),
				'isReadOnly': $('#frmBaixaItem').data('isReadOnly')
				
			},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				
				$dialog.on('close', function(e, cheque) {
					
					if(typeof cheque.talaoCheque != 'undefined') {
						if($('input[name="baixaItem.cheque.talaoCheque.id"]').val() != cheque.talaoCheque.id || $('input[name="baixaItem.cheque.numero"]').val() != cheque.numero){
							msgSuccess(i18n[i18n.locale]['cheque.selecionado.success'].template({"0": cheque.numero, "1": cheque.talaoCheque.conta.nome}));
						}
						$('input[name="baixaItem.cheque.id"]').val(cheque.id);
						$('input[name="baixaItem.cheque.talaoCheque.id"]').val(cheque.talaoCheque.id);
						$('input[name="baixaItem.cheque.numero"]').val(cheque.numero);
						$('input[name="baixaItem.cheque.data"]').val(cheque.data);
						$('input[name="baixaItem.cheque.obs"]').val(cheque.obs);
					} else {
						if($('input[name="baixaItem.cheque.talaoCheque.id"]').val() != "" && $('input[name="baixaItem.cheque.numero"]').val() != ""){
							msgSuccess(i18n[i18n.locale]['cheque.nenhum.selecionado']);
						}
						$('input[name="baixaItem.cheque.id"]').val("");
						$('input[name="baixaItem.cheque.talaoCheque.id"]').val("");
						$('input[name="baixaItem.cheque.numero"]').val("");
						$('input[name="baixaItem.cheque.data"]').val("");
						$('input[name="baixaItem.cheque.obs"]').val("");
					}
					
					$('#frmBaixaItem').data('chequedes', cheque);
					
					lightDarkBtCheque();
					
				});
				
			}
		}).always(function() {
			loading(false);
		});
	} else {
		$.ajax({
			url: contextPath + '/cheque/dialogProps',
			data: {
				'cheque.id': $('input[name="baixaItem.cheque.id"]').val(),
				'cheque.banco.id': $('input[name="baixaItem.cheque.banco.id"]').val(),
				'cheque.numero': $('input[name="baixaItem.cheque.numero"]').val(),
				'cheque.data': $('input[name="baixaItem.cheque.data"]').val(),
				'cheque.nome.id': $('input[name="baixaItem.cheque.nome.id"]').val(),
				'cheque.obs': $('input[name="baixaItem.cheque.obs"]').val(),
				'isReadOnly': $('#frmBaixaItem').data('isReadOnly')
			},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('close', function(e, cheque) {
					cheque = $.extend({
						id: '',
						banco: {id: ''},
						nome: {id: ''}
					}, cheque);
					try{
						
					
					if(typeof cheque.numero != 'undefined') {
						
						if(
						$('input[name="baixaItem.cheque.banco.id"]').val() != cheque.banco.id
						||
						$('input[name="baixaItem.cheque.numero"]').val() != cheque.numero
						||
						$('input[name="baixaItem.cheque.nome.id"]').val() != cheque.nome.id
						) {
							
							var message = i18n[i18n.locale]['cheque.informado.success'].template({"0": cheque.numero});
							msgSuccess(message);
						}
						
						$('input[name="baixaItem.cheque.id"]').val(cheque.id);
						$('input[name="baixaItem.cheque.banco.id"]').val(cheque.banco.id || '');
						$('input[name="baixaItem.cheque.numero"]').val(cheque.numero);
						$('input[name="baixaItem.cheque.data"]').val(cheque.data);
						if(typeof cheque.nome != 'undefined') { $('input[name="baixaItem.cheque.nome.id"]').val(cheque.nome.id); }
						else { $('input[name="baixaItem.cheque.nome.id"]').val(''); }
						$('input[name="baixaItem.cheque.obs"]').val(cheque.obs);
					} else {
						
						if($('input[name="baixaItem.cheque.numero"]').val() != cheque.numero) {
							msgSuccess(i18n[i18n.locale]['cheque.nenhum.informado']);
						}
						
						$('input[name="baixaItem.cheque.id"]').val('');
						$('input[name="baixaItem.cheque.banco.id"]').val('');
						$('input[name="baixaItem.cheque.numero"]').val('');
						$('input[name="baixaItem.cheque.data"]').val('');
						$('input[name="baixaItem.cheque.nome.id"]').val('');
						$('input[name="baixaItem.cheque.obs"]').val('');
					}
					
					$('#frmBaixaItem').data('chequerec', cheque);
					lightDarkBtCheque();
					}catch(e) {
						console.log(e);
					}
				});
			}
		}).always(function() {
			loading(false);
		});
	}
});

if($('#lancamentoParcelaTipo').val() == 'DES') {
	$('#frmBaixaItem').data('chequedes', {
		id: $('input[name="baixaItem.cheque.id"]').val(),
		talaoCheque: {id: $('input[name="baixaItem.cheque.talaoCheque.id"]').val()},
		numero: $('input[name="baixaItem.cheque.numero"]').val(),
		data: $('input[name="baixaItem.cheque.data"]').val(),
		obs: $('input[name="baixaItem.cheque.obs"]').val()
	});
} else {
	$('#frmBaixaItem').data('chequerec', {
		id: $('input[name="baixaItem.cheque.id"]').val(),
		banco: {codigo: $('input[name="baixaItem.cheque.banco.id"]').val()},
		numero: $('input[name="baixaItem.cheque.numero"]').val(),
		data: $('input[name="baixaItem.cheque.data"]').val(),
		nome: {id: $('input[name="baixaItem.cheque.nome.id"]').val()},
		obs: $('input[name="baixaItem.cheque.obs"]').val()
	});
}


$('#btBaixaItemBoleto').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/lancamentoParcelaBoleto',
		data: {boletoTipo: $('#baixaItemBoletoTipo').val(), boletoNumero: $('#baixaItemBoletoNumero').val()},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, boleto) {
				if(boleto.boletoNumero) {
					$('#baixaItemBoletoTipo').val(boleto.boletoTipo);
					$('#baixaItemBoletoNumero').val(boleto.boletoNumero);
					if(boleto.boletoTipo == 'COB') {
						try {
							var objBoleto = getBoleto(boleto.boletoNumero);
							
							var isValor = false;
							if($('#baixaItemValor').val() != objBoleto.valor.formatDecimal()) {
								isValor = true;
							}
							
							if(isValor) {
								msgConfirm(i18n[i18n.locale]['boletoNumero.valor.atualizar'].template({"0": objBoleto.valor.formatDecimal()}), function() {
									$('#baixaItemValor').val(objBoleto.valor.formatDecimal()).change();
								});
							}
							
						}catch(e){}
					} 
					
					msgSuccess(i18n[i18n.locale]['codigoBarras.informado.success']);
					
				} else {
					$('#baixaItemBoletoTipo').val('');
					$('#baixaItemBoletoNumero').val('');
					msgSuccess(i18n[i18n.locale]['codigoBarras.nenhum.informado']);
				}
				lightDarkBtBoleto();
			});
		}
	}).always(function() {
		loading(false);
	});
	
});


$('#btBaixaItemArquivo').click(function() {
	
	loading(true);
	$.ajax({
		url: contextPath + '/arquivo/dialogAnexo',
		data: {
				viewid: $('#baixaViewid').val(),
				target: 'baixaItem',
				'arquivo.id': $('#baixaItemArquivoId').val(),
				'arquivo.temp': $('#baixaItemArquivoTemp').val(),
				'arquivo.nome': $('#baixaItemArquivoNome').val(),
				'arquivo.contentType': $('#baixaItemArquivoContentType').val(),
				'arquivo.size': $('#baixaItemArquivoSize').val()
		},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, arquivo) {
				
				if($('#baixaItemArquivoNome').val() != arquivo.nome || $('#baixaItemArquivoContentType').val() != arquivo.contentType || $('#baixaItemArquivoSize').val() != arquivo.size) {
					if(arquivo.nome) {
						msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#baixaItemArquivoNome').val()}));
					}
				}
				
				$('#baixaItemArquivoNome').val(arquivo.nome);
				$('#baixaItemArquivoContentType').val(arquivo.contentType);
				$('#baixaItemArquivoSize').val(arquivo.size);
				
				lightDarkBtArquivo();
				
			});
		}
	}).always(function() {
		loading(false);
	});
	
});



$('#baixaItemMeio').change(function() {
	if($(this).val() == '') {
		$('#btBaixaItemCartaoCredito, #btBaixaItemConta, #btBaixaItemCheque, #btBaixaItemBoleto').prop('disabled', true).removeClass('btn-primary').addClass('btn-default');
	} else {
		enableDisableBtCartaoCredito();
		enableDisableBtConta();
		enableDisableBtConta();
		enableDisableBtCheque();
		enabledDisableBtBoleto();
	}
}).change();

function enableDisableBtCartaoCredito() {
	if($('#lancamentoParcelaTipo').val() == 'DES' && $('#baixaItemMeio').val() == 'CCR') {
		$('#btBaixaItemCartaoCredito').prop('disabled', false);
	} else {
		$('#btBaixaItemCartaoCredito').prop('disabled', true);
	}
	lightDarkBtCartaoCredito();
}


function enableDisableBtConta() {
	if(
	$('#baixaItemMeio').val() != 'CCR' && $('#baixaItemMeio').val() != 'CHQ' 
	|| 
		(
				($('#baixaItemMeio').val() == 'CHQ' || $('#baixaItemMeio').val() == 'CCR') 
				&& $('#lancamentoParcelaTipo').val() == 'REC'
		)
	) {
		$('#btBaixaItemConta').prop('disabled', false);
	} else {
		$('#btBaixaItemConta').prop('disabled', true);
	}
	lightDarkBtConta();
}


function enableDisableBtCheque() {
	if($('#baixaItemMeio').val() == 'CHQ') {
		$('#btBaixaItemCheque').prop('disabled', false);
	} else {
		$('#btBaixaItemCheque').prop('disabled', true);
	}
	lightDarkBtCheque();
}


function enabledDisableBtBoleto() {
	$('#btBaixaItemBoleto').prop('disabled', $('#lancamentoParcelaTipo').val() == 'REC');
	lightDarkBtBoleto();
}


function lightDarkBtCartaoCredito() {
	if($('#baixaItemCartaoCredito').val() && !$('#btBaixaItemCartaoCredito').prop('disabled')) {
		$('#btBaixaItemCartaoCredito').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btBaixaItemCartaoCredito').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtConta() {
	if($('#baixaItemConta').val() && !$('#btBaixaItemConta').prop('disabled')) {
		$('#btBaixaItemConta').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btBaixaItemConta').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtCheque() {
	
	var isCheque = false;
	if($('#lancamentoParcelaTipo').val() == 'DES') {
		var cheque = $('#frmBaixaItem').data('chequedes');
		if(cheque && cheque.talaoCheque.id && cheque.numero) {
			isCheque = true;
		}
	}
	if($('#lancamentoParcelaTipo').val() == 'REC') {
		var cheque = $('#frmBaixaItem').data('chequerec');
		if(cheque && cheque.numero) {
			isCheque = true;
		}
	}
	
	console.log($('#frmBaixaItem').data('chequedes'));
	
	if(isCheque && !$('#btBaixaItemCheque').prop('disabled')) {
		$('#btBaixaItemCheque').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btBaixaItemCheque').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtBoleto() {
	if($('#baixaItemBoletoNumero').val() && !$('#btBaixaItemBoleto').prop('disabled')) {
		$('#btBaixaItemBoleto').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btBaixaItemBoleto').removeClass('btn-primary').addClass('btn-default');
	}
}

function lightDarkBtArquivo() {
	if($('#baixaItemArquivoNome').val() && !$('#btBaixaItemArquivo').prop('disabled')) {
		$('#btBaixaItemArquivo').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btBaixaItemArquivo').removeClass('btn-primary').addClass('btn-default');
	}
}
lightDarkBtArquivo();

function lightDarkBtComprovante() {
	if($('#baixaItemComprovanteNome').val() && !$('#btBaixaItemComprovante').prop('disabled')) {
		$('#btBaixaItemComprovante').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btBaixaItemComprovante').removeClass('btn-primary').addClass('btn-default');
	}
}
lightDarkBtComprovante();


$('#btBaixaItemComprovante').click(function() {
	
	loading(true);
	$.ajax({
		url: contextPath + '/arquivo/dialogAnexo',
		data: {
				viewid: $('#baixaViewid').val(),
				target: 'comprovante',
				'arquivo.id': $('#baixaItemComprovanteId').val(),
				'arquivo.temp': $('#baixaItemComprovanteTemp').val(),
				'arquivo.nome': $('#baixaItemComprovanteNome').val(),
				'arquivo.contentType': $('#baixaItemComprovanteContentType').val(),
				'arquivo.size': $('#baixaItemComprovanteSize').val()
		},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, arquivo) {
				
				if($('#baixaItemComprovanteNome').val() != arquivo.nome || $('#baixaItemComprovanteContentType').val() != arquivo.contentType || $('#baixaItemComprovanteSize').val() != arquivo.size) {
					if(arquivo.nome) {
						msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#baixaItemArquivoNome').val()}));
					}
				}
				
				$('#baixaItemComprovanteNome').val(arquivo.nome);
				$('#baixaItemComprovanteContentType').val(arquivo.contentType);
				$('#baixaItemComprovanteSize').val(arquivo.size);
				
				lightDarkBtComprovante();
				
			});
		}
	}).always(function() {
		loading(false);
	});
		
});


$('#frmBaixaItem').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/baixa/baixaItem',
		type: 'POST',
		data: $(this).add($('#baixaViewid')).serialize(),
		success: function(data) {
			if(typeof data == 'string') {
				var message = i18n[i18n.locale]['baixaItem.' + $('#lancamentoParcelaTipo').val() + '.editado.success']; 
				$('#layBaixaItem').replaceWith(data);
				$('#dialogBaixaItem').modal('hide');
				
				msgSuccess(message);
				
				enableDisableBaixaItemBt();
			} else {
				error(data);
			}
		}
	}).always(function() {
		loading(false);
	});
	return false;
});