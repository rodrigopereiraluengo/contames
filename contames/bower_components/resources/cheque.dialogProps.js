$('#dialogPropsCheque').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#dialogPropsChequeNumero').toFocus();

$('#dialogSelectTalaoChequeData').inputCalendar();


$('#dialogPropsChequeBanco').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	}
});

$('#dialogPropsChequeNome').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});


$('#frmDialogPropsCheque').submit(function() {
	
	$.ajax({
		url: contextPath + '/cheque/dialogPropsValidate',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	});
	
	return false;
});

function dialogPropsChequeSuccess(cheque) {
	$('#dialogPropsCheque').trigger('close', [cheque]).modal('hide');
}