$(function() {
	
	// Item
	$('#relatorioItemItem').tokenInput(contextPath + '/item/multipleAutoComplete?tipo=' + $('#frmRelatorioItem').data('tipo'), {
        preventDuplicates: true,
        minChars: 1,
        searchDelay: 500,
        animateDropdown: false,
        resultsFormatter: function(item) { return '<li>' + item.name + '</li>'; },
        tokenFormatter: function(item) { return '<li><p>' + item.name + '</p></li>'; },
        onAdd: function(item) {
        	if(item.isClassif) {
        		$('#frmRelatorioItem').append('<input type="hidden" name="classifList[]" value="' + item.id + '" />');
        	} else {
        		$('#frmRelatorioItem').append('<input type="hidden" name="itemList[]" value="' + item.id + '" />');
        	}
        },
        onDelete: function(item) {
        	if(item.isClassif) {
        		$('#frmRelatorioItem').find('input[name="classifList[]"][value=' + item.id + ']').remove();
        	} else {
        		$('#frmRelatorioItem').find('input[name="itemList[]"][value=' + item.id + ']').remove();	
        	}
        	
        }
    });
	
	// Status
	$('#relatorioItemStatus').multiselect({
		buttonWidth: '100%', 
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___'),
	});
	
	// Formulario
	$('#frmRelatorioItem').formSearch({
		multipleSelectRows: false,
		selectRow: true
	}).one('scrollEnd', function() {
		relatorioItemOpenChart();
	}).on('scroll', function() {
		relatorioItemOpenChart();
	}).on('formSearch.order', function() {
		labels = [];
		values = [];
	});
	
	$('#btSearch').click(function() {
		$('#frmRelatorioItem').one('scrollEnd', function() {
			relatorioItemOpenChart();
		});
		labels = [];
		values = [];
	});
	
	
	$('#btExportXls').click(function() {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_' + ($('#frmRelatorioItem').data('tipo') == 'DES' ? 'despesas' : 'receitas') + '_classificacao.xlsx?' + $('#frmRelatorioItem').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	
	$('#btExportPdf').click(function() {
		if($('#layChart').is(':visible')) {
			$(this).attr('href', contextPath + '/relatorio_' + ($('#frmRelatorioItem').data('tipo') == 'DES' ? 'despesas' : 'receitas') + '_classificacao.pdf?' + $('#frmRelatorioItem').serialize());
		} else {
			msgError('nenhumRegistroEncontrado');
			e.preventDefault();
		}
	});
	
	// Grafico
	relatorioItemOpenChart();
		
	$('#isCCred').bootstrapToggle({
		on: i18n.get('MeioPagamento.CCR'),
		off: i18n.get('DocTipo.FT')
    }).change(function() {
    	$('#frmRelatorioItem').setHidden('isCCred', $(this).prop('checked')).submit();
    	msgSuccess($(this).prop('checked') ? i18n.get('exibirMovsCCred') : i18n.get('exibirFaturas'));
    });
	
});


/**
 * Abre Janela de Item
 * @param item_id
 */
function relatorioItemOpenItem(item_id) {
	loading(true);
	$.ajax({
		url: contextPath + '/item/cadastro',
		data: {id: item_id},
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioItem'));
			}
		}
	}).always(function(){
		loading(false);
	});
}


/**
 * Abre Janela Movimentos
 * @param data
 */
function relatorioItemOpenMovs(data) {
	
	data.ano = $('#relatorioItemAno').val();
	data.tipo = $('#frmRelatorioItem').data('tipo');
	data.isCCred = $('#isCCred').prop('checked');
	if(typeof data.item_id == 'undefined' || data.item_id == 0) {
		if(!$('input[name="itemList[]"]').each(function(i) {
			data['itemList[' + i + ']'] = $(this).val();
		}).length) {
			data['itemList[0]'] = data.item_id;
		}
	} else {
		data['itemList[0]'] = data.item_id;
		delete data.item_id;
	}
	
	$('#relatorioItemStatus :selected').each(function(i, option) {
		data['statusList[' + i + ']'] = $(option).val();
	});
	
	loading(true);
	$.ajax({
		url: contextPath + '/relatorioItem/movs',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioItem'));
			}
		}
	}).always(function(){
		loading(false);
	});
}

function formatDecimal(val) {
	 return $.formatNumber(val, {format: "#,##0.".repeat(2, "0")});
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

Chart.defaults.global.responsive = true;
Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=formatDecimal(value)%>";
Chart.defaults.global.multiTooltipTemplate = "<%=formatDecimal(value)%>";


var chartRelatorioItem;
var toRelatorioItemOpenChart;
function relatorioItemOpenChart() {
	window.clearTimeout(toRelatorioItemOpenChart);
	toRelatorioItemOpenChart = window.setTimeout(function() {
	
		if(values.length) {
			$('#layChart').show();
		}
		
		if(typeof chartRelatorioItem != 'undefined'){ 
			chartRelatorioItem.destroy();
			$('#chartRelatorioItemDes').replaceWith('<canvas id="chartRelatorioItem" class="rel-canvas-chart"></canvas>');
		}
		
		if(typeof labels != 'undefined' && values.length) {
		
			if(labels.length) { $('#layChart').show(); }
			
			var ctxChart = document.getElementById("chartRelatorioItem").getContext("2d");
			var data = [];
			
			$.each(labels, function(i, val) {
				data.push({label : val, value : values[i], color: getRandomColor()});
			});
			
			
			chartRelatorioItem = new Chart(ctxChart).Pie(data, {responsive: true, maintainAspectRatio: true, scaleLabel: function(valuePayload) {
				return $.formatNumber(valuePayload.value, {format: "#,##0.".repeat(2, "0")});
			}});
		
		}
	}, 500);
}