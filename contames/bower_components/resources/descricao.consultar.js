$('#dialogDescricaoConsultar').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).trigger('close').remove();
});

$('#frmDescricaoConsultar').formSearch({
	deleteMessageConfirm: i18n.get('formSearch.delete.' + $('#frmDescricaoConsultar').data('tipo') + '.confirm'),
	deleteMessageConfirmP: i18n.get('formSearch.delete.' + $('#frmDescricaoConsultar').data('tipo') + '.confirm.p'),
	deleteMessageSuccess: i18n.get('formSearch.delete.' + $('#frmDescricaoConsultar').data('tipo') + '.success'),
	deleteMessageSuccessP: i18n.get('formSearch.delete.' + $('#frmDescricaoConsultar').data('tipo') + '.success.p')
});

function descricaoExcluirSuccess(data) {
	$('#dialogDescricaoConsultar').trigger('excluir.success', [data]);
}