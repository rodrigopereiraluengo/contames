$(function() {
	
	$('#lbAtualizarSenha').click(function(e) {
		
		loading(true);
		$.ajax({
			url: $(this).attr('href'),
			success: function(result) {
				$('body').append(result);
			}
		}).always(function() {
			loading(false);
		});
		
		e.preventDefault();
	});
	
	
	/**
	 * Atualiza nome do Usuario
	 */
	$(document).on('success', '#dialogPessoaCadastro', function(e, pessoa) {
		if($('#menuUsuario_' + pessoa.id).length) {
			if(pessoa.pessoaTipo == 'F') {
				$('#menuUsuario_' + pessoa.id).text(pessoa.apelido ? pessoa.apelido : pessoa.nome);
			} else {
				$('#menuUsuario_' + pessoa.id).text(pessoa.fantasia ? pessoa.fantasia : pessoa.razaoSocial);
			}
		}
	});
	
});

function confirmarSaida(self) {
	
	msgConfirm(i18n.get('confirmar.sair'), function() {
		window.location.href = $(self).attr('href');
	});
	
	return false;
}