$(function() {
	
	$('#senha').focus();
	
	$('#frmAtualizarSenha').submit(function() {
		loading(true);
		$.ajax({
			url: $(this).attr('action'),
			data: $(this).serialize(),
			type: 'post',
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
		
		return false;
	});
	
	
	$('#frmAtualizarSenha :submit').prop('disabled', false);
	
});

function success() {
	
	msgSuccess(i18n.get('login.atualizarSenha.msgSuccesso'));
	
	window.setTimeout(function() {
		window.location.href = contextPath + '/entrar'; 
	}, 3000);
	
}