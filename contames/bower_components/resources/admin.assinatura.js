
function cancelarAssinatura() {
	msgConfirm('assinatura.cancelar.confirmar', function() {
		loading(true);
		$.ajax({
			url: contextPath + '/admin/cancelarAssinatura',
			type: 'post',
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
	});
}


function cancelarSuccess() {
	msgSuccess(i18n.get('assinatura.cancelar.sucesso'));
	window.location.href = contextPath + '/';
}


function reativarAssinatura(status) {
	msgConfirm('assinatura.' + (status == 'STD' ? 'fazerPagamentoAgora' : 'reativar') + '.confirmar', function() {
		loading(true);
		$.ajax({
			url: contextPath + '/admin/reativarAssinatura',
			type: 'post',
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
	});
} 


function reativarSuccess(result) {
	window.location.href = checkoutURL + result.token;
}
