$(function() {
	$('#frmCompensacaoConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.conciliacao.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.conciliacao.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.conciliacao.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.conciliacao.success.p'),
		multipleSelectRows: false
	});
});


function compensacaoExcluirSuccess(compensacao) {
	$('#frmCompensacaoConsultar').trigger('excluir.success', [compensacao]);
}

$(document).on('close', '#dialogSelectConta', function(e, conta) {
	$.ajax({
		url: contextPath + '/compensacao/cadastro',
		data: {'contaId': conta.id || null},
		success: function(data) {
			if(typeof data == 'string') {
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('success', function() {
					$('#frmCompensacaoConsultar').submit();
				});
			} else {
				error(data, $('#frmCompensacaoConsultar'));
			}
		}
	});
});