$(function() {
	
	$('a.btn[href$="migrar"]').click(function(e) {
		
		loading(true);
		$.ajax({
			url: $(this).attr('href'),
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
		
		e.preventDefault();
	});
	
});


function migrarSuccess(result) {
	window.location.href = checkoutURL + result.token;
}