$('#dialogContaTalaoCheque').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#contaTalaoChequeData').inputCalendar();

$('#contaTalaoChequeFolhas').toSelect();


$('#contaTalaoChequeSeqInicio, #contaTalaoChequeFolhas').constrainNumber().blur(function(){
	var seqInicio = parseInt($('#contaTalaoChequeSeqInicio').val().unformat());
	var folhas = parseInt($('#contaTalaoChequeFolhas').val().unformat());
		
	seqFim = (seqInicio + folhas) - 1;
	if(isNaN(seqFim)) {
		$('#lbContaTalaoChequeSeqFim').html('<span class="text-danger"><strong>[&nbsp;' + i18n[i18n.locale]['talaoCheque.folhas.blank'] + '&nbsp;]</strong></span>');
	} else {
		$('#lbContaTalaoChequeSeqFim').text(seqFim);
	}
});

function talaoChequeRestaurarCheque(id, numero) {
	msgConfirm(i18n.get('talaoCheque.cheque.restaurar.confirm', numero), function() {
		$.ajax({
			url: contextPath + '/cheque/restaurar',
			data: {id: id},
			type: 'POST',
			success: function(result) {
				if(typeof result == 'string') {
					$('#pnChequeList').html(result);
					msgSuccess(i18n.get('talaoCheque.cheque.restaurar.success', numero));
				} else {
					error(result, $('#frmContaTalaoCheque'));
				}
			}
		});
	});
}

function talaoChequeCancelarCheque() {
	loading(true);
	$.ajax({
		url: contextPath + '/cheque/dialogCancelar',
		data: {talaoChequeId: $('#talaoChequeId').val()},
		success: function(result) {
			$('body').append(result);
		}
	}).always(function() {
		loading(false);
	});
}

$('#frmContaTalaoCheque').submit(function() {
	var isAdicionar = $(this).data('adicionar');
	loading(true);
	$.ajax({
		url: contextPath + '/conta/talaoCheque',
		data: $(this).serialize(),
		type: 'POST',
		success: function(data) {
			if(typeof data == 'string') {
				$('#dialogContaTalaoCheque').trigger('success', [data]).modal('hide');
				var message = 'talaoCheque.editado.success';
				if(isAdicionar) {
					message = 'talaoCheque.adicionado.success';
				}
				msgSuccess(i18n[i18n.locale][message]);
			} else {
				if(data.id) {
					$('#dialogContaTalaoCheque').trigger('success', [data]).modal('hide');
					msgSuccess(i18n[i18n.locale]['talaoCheque.salvo.success']);
				} else {
					error(data, $('#frmContaTalaoCheque'));
				}
			}
		}
	}).always(function() {
		loading(false);
	});
	
	return false;
});