$('#dialogChequeCancelar').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#dialogChequeCancelarNumero').toFocus();


$('#dialogChequeCancelarMotivo').comboEditable();


$('#frmDialogChequeCancelar').submit(function() {
	
	$.ajax({
		url: contextPath + '/cheque/dialogCancelar',
		type: 'POST',
		data: $(this).serialize(),
		success: function(result) {
			if(typeof result == 'string') {
				var numero = $('#dialogChequeCancelarNumero').val();
				$('#dialogChequeCancelar').trigger('success').modal('hide');
				$('#pnChequeList').html(result);
				msgSuccess(i18n.get('cheque.cancelado.success', numero));
			} else {
				error(result, $('#frmDialogChequeCancelar'));
			}
		}
	});
	
	return false;
});