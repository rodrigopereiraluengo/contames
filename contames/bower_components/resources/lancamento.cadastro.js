


/**
 * Janela Lancamento
 */
$('#dialogLancamentoCadastro').modal({ show: true, keyboard: false, backdrop: 'static' }).on('hidden.bs.modal', function() {
	$(this).remove();
});



/**
 * Data do Lancamento 
 */
$('#lancData').toSelect().inputCalendar();



/**
 * Auto complete do Favorecido
 */
$('#lancFavorecido').autoCompleteEditable().on('setValue', function(e, pessoa) {
	if(pessoa.pessoaTipo == 'J') {
		if(typeof pessoa.fantasia != 'undefined') {
			$(this).val(pessoa.fantasia);
		} else {
			$(this).val(pessoa.razaoSocial);
		}
	} else {
		if(typeof pessoa.apelido != 'undefined') {
			$(this).val(pessoa.apelido);
		} else {
			$(this).val(pessoa.nome);
		}
	}
});



/**
 * Documento Tipo
 */
$('#lancTipoDoc').change(function() {
	if($(this).val() == '') {
		$('#lancDataDoc, #lancNumeroDoc, #btLancDataDoc, #lancValorDoc').prop('disabled', true);
		$('#layLancCartaoCredito, #lancDataDocReq, #lancValorDocReq, #layLancCartaoCreditoLimite, #layLancCartaoCreditoMovs').hide();
	} else {
		$('#lancDataDoc, #lancNumeroDoc, #btLancDataDoc, #lancValorDoc').prop('disabled', false);
		
		if($('#lancDataDoc').val().blank() && !$('#lancData').val().blank()) {
			$('#lancDataDoc').val($('#lancData').val()).toSelect();
		} else if(!$('#lancNumeroDoc').is(':disabled') && $('#lancNumeroDoc').val().blank()) {
			$('#lancNumeroDoc').focus();
		} else if($('#lancValorDoc').val() == ''){
			$('#lancValorDoc').focus();
		}
		
		if($(this).val() == 'FT') {
			
			$('#lancNumeroDoc').prop('disabled', $('#frmLancamentoCadastro').data('tipo') == 'DES');
			if($('#frmLancamentoCadastro').data('tipo') == 'DES') {
				$('#layLancCartaoCredito, #lancDataDocReq, #lancValorDocReq, #layLancCartaoCreditoLimite, #layLancCartaoCreditoMovs').show();
				$('#lancCartaoCredito').change();
			}
		
		} else {
			
			$('#lancNumeroDoc').prop('disabled', false);
			$('#layLancCartaoCredito, #lancDataDocReq, #lancValorDocReq, #layLancCartaoCreditoLimite, #layLancCartaoCreditoMovs').hide();
		
		}
			
	}
}).change();



/**
 * Documento Anexo de Arquivo 
 */
$('#btLancamentoArquivoDoc').click(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/arquivo/dialogAnexo',
		data: {
				target: 'lancamentoDoc',
				'arquivo.id': $('#lancamentoArquivoDocId').val(),
				'arquivo.temp': $('#lancamentoArquivoDocTemp').val(),
				'arquivo.nome': $('#lancamentoArquivoDocNome').val(),
				'arquivo.contentType': $('#lancamentoArquivoDocContentType').val(),
				'arquivo.size': $('#lancamentoArquivoDocSize').val()
		},
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			$dialog.on('close', function(e, arquivo) {
				
				if($('#lancamentoArquivoDocNome').val() != arquivo.nome || $('#lancamentoArquivoDocContentType').val() != arquivo.contentType || $('#lancamentoArquivoDocSize').val() != arquivo.size) {
					if(arquivo.nome) {
						msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
					} else {
						msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#lancamentoArquivoDocNome').val()}));
					}
				}
				
				$('#lancamentoArquivoDocNome').val(arquivo.nome);
				$('#lancamentoArquivoDocContentType').val(arquivo.contentType);
				$('#lancamentoArquivoDocSize').val(arquivo.size);
				lightDarkBtArquivoDoc();
			});
		}
	}).always(function() {
		loading(false);
	});
});



/**
 * Acender apagar botao Anexo Arquivo 
 */
function lightDarkBtArquivoDoc() {
	if($('#lancamentoArquivoDocNome').val()) {
		$('#btLancamentoArquivoDoc').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoArquivoDoc').removeClass('btn-primary').addClass('btn-default');
	}
}
lightDarkBtArquivoDoc();



/**
 * Documento Data
 */
$('#lancDataDoc').inputCalendar();




/**
 * Documento Valor
 */
if(!$('#frmLancamentoCadastro').data('isClosed')) {
	$('#lancValorDoc').formatDecimal().change(function() {
		
		if($('#lancTipoDoc').val() == 'FT') {
			$('#lancCartaoCreditoLimiteDisponivel, #lancCartaoCreditoLimiteUtilizado, #lancCartaoCreditoDiferenca').removeClass('font-red font-blue');
			if($('#lancCartaoCredito').val() != '') {
			
				var valorFatura = parseFloat($('#lancValorDoc').val().unformat()) || 0;
				var totalLancamentoFatura = parseFloat($('#lbTotalLancamentoFatura').text().unformat()) || 0;
				var diferenca = valorFatura - totalLancamentoFatura;
				
				$('#lancCartaoCreditoDiferenca').text(diferenca.formatDecimal()).addClass(diferenca == 0 ? 'font-blue' : 'font-red');
				
				var limiteDisponivel = $('#lancCartaoCreditoLimiteDisponivel').text().unformat().toFloat();
				var limiteUtilizado = $('#lancCartaoCreditoLimiteUtilizado').text().unformat().toFloat();
				
				$('#lancCartaoCreditoLimiteDisponivel').addClass(limiteDisponivel > 0 ? 'font-blue' : 'font-red');
				$('#lancCartaoCreditoLimiteUtilizado').addClass(limiteUtilizado > 0 ? 'font-red' : 'font-blue');
			}
			if(!$(this).val()) {
				$('#lancCartaoCreditoDiferenca').html('<span class="text-danger"><strong>[&nbsp;' + i18n.get('informeoValor') + '&nbsp;]</strong></span>');
			}
		} else {
			$('#frmLancamentoCadastro').setHidden('isConfirmValorDocNotEqual', !($('#lancValorDoc').not(':disabled') && $('#lancValorDoc').val() && $('#lancValorDoc').val() != $('#lbTotalLancamentoItem').text()));
		}
	});
}

if($('#lancCartaoCredito').is('select')) {
	$('#lancCartaoCreditoLimite').text($('#lancCartaoCredito').find(':selected').data('limite'));
	$('#lancCartaoCreditoLimiteDisponivel').text($('#lancCartaoCredito').find(':selected').data('limiteDisponivel'));
	$('#lancCartaoCreditoLimiteUtilizado').text($('#lancCartaoCredito').find(':selected').data('limiteUtilizado'));
}


/**
 * Fatura Cartao de Credito
 */
$('#lancCartaoCredito').comboEditable().on('change', function() {
	
	$('#lancCartaoCreditoLimiteDisponivel, #lancCartaoCreditoLimiteUtilizado').removeClass('font-red font-blue');
	
	if($(this).val()) {
		
		$('#lancBtSelecionarLancamentoFatura').prop('disabled', false);
		
		var lancDataDoc = $('#lancDataDoc').val().blank() ? moment() : moment($('#lancDataDoc').val(), i18n.get('momPatternDate'));
		
		var diaVencimento = $(this).find(':selected').data('diaVencimento');
		
		if(diaVencimento) {
			lancDataDoc.set({'date': diaVencimento});
		}
		
		$('#lancDataDoc').val(lancDataDoc.format(i18n.get('momPatternDate')));
		
		$('#lancCartaoCreditoLimite').text($('#lancCartaoCredito').find(':selected').data('limite'));
		$('#lancCartaoCreditoLimiteDisponivel').text($('#lancCartaoCredito').find(':selected').data('limiteDisponivel'));
		$('#lancCartaoCreditoLimiteUtilizado').text($('#lancCartaoCredito').find(':selected').data('limiteUtilizado'));
		
		var limiteDisponivel = $('#lancCartaoCreditoLimiteDisponivel').text().unformat().toFloat();
		var limiteUtilizado = $('#lancCartaoCreditoLimiteUtilizado').text().unformat().toFloat();
		
		$('#lancCartaoCreditoLimiteDisponivel').addClass(limiteDisponivel > 0 ? 'font-blue' : 'font-red');
		$('#lancCartaoCreditoLimiteUtilizado').addClass(limiteUtilizado > 0 ? 'font-red' : 'font-blue');
		
		$('#lancValorDoc').toSelect();
		
	} else {
		
		$('#lancBtSelecionarLancamentoFatura').prop('disabled', true);
		$('#lancCartaoCreditoLimite, #lancCartaoCreditoLimiteDisponivel, #lancCartaoCreditoLimiteUtilizado').html('<span class="text-danger"><strong>[&nbsp;' + i18n.get('selecioneCartaoCredito') + '&nbsp;]</strong></span>');
		
	}
	
});



/**
 * Novo Limite Disponivel
 */
$('#lancCartaoCreditoLimiteDisponivel').formatDecimal().change(function() {
	
	var limite = $('#lancCartaoCreditoLimite').text().unformat() || 0;
	var limiteDisponivel = parseFloat($(this).val().unformat()) || 0;
	var limiteUtilizado = limite - limiteDisponivel;
	$('#lancCartaoCreditoLimiteUtilizado').val($.formatNumber(limiteUtilizado));

});




/**
 * Janela Selecionar Lancamento Fatura
 */
function dialogSelecionarLancamentoFatura() {
	
	loading(true);
	
	var diasPagar = $('#lancCartaoCredito :selected').data('diasPagar') || 30;
	
	var dataAte = moment($('#lancDataDoc').val(), i18n.get('momPatternDate')).subtract(diasPagar, 'days').add(1, 'month');
	
	var data = {
		'dataAte': dataAte.endOf('month').format(i18n.get('momPatternDate')),
		'dataDe': dataAte.startOf('month').subtract(1, 'month').format(i18n.get('momPatternDate')),
		'cartaoCredito': $('#lancCartaoCredito').val()
	};
	
	$('#tbyLancamentoFaturaList tr').each(function(i) {
		
		if(typeof $(this).data('lancamentoParcela') != 'undefined') {
			data['idsNotIn[' + i + ']'] = $(this).data('lancamentoParcela');
		}
		
		if(typeof $(this).data('transferencia') != 'undefined') {
			data['transfIdNotIn[' + i + ']'] = $(this).data('transferencia');
		}
	
	});
	
	$.ajax({
		url: contextPath + '/lancamento/lancamentoFatura',
		data: data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmLancamentoCadastro'));
			}
		}
	}).always(function() {
		loading(false);
	});
	
}



/**
 * Selecionar Lancamento Fatura
 * @param self
 */
function selecionarLancamentoFatura(self) {
	if($(self).is('.active')) {
		$(self).removeClass('active');
	} else {
		$(self).addClass('active');
	}
	
	enableDisableLancamentoFaturaBtRemover();
	
}



/**
 * Habilitar desabilitar botao remover Lancamento Fatura
 */
function enableDisableLancamentoFaturaBtRemover() {
	var selected = $('#tbyLancamentoFaturaList tr.active').length;
	$('#lancBtRemoverLancamentoFatura').prop('disabled', selected == 0);
}



/**
 * Remover Lancamento Fatura
 */
function removerLancamentoFatura() {

	var $selected = $('#tbyLancamentoFaturaList tr.active');
	var len = $selected.length;
	var message = i18n[i18n.locale]['lancamentoFatura.remover.confirm'];
	if(len > 1) {
		message = i18n[i18n.locale]['lancamentoFatura.remover.confirm.p'].template({"0": len});
	}
	
	
	msgConfirm(message, function() {
		loading(true);
		
		
		var data = {
			'viewid': $('#lancViewid').val(),
			'id': $('#lancCartaoCredito').val()
		};
		
		
		$('#tbyLancamentoFaturaList tr.active').each(function(i) {
			if(typeof $(this).data('lancamentoParcela') != 'undefined') {
				data['ids[' + i + ']'] = $(this).data('lancamentoParcela');
			}
			if(typeof $(this).data('transferencia') != 'undefined') {
				data['ids[' + i + ']'] = $(this).data('transferencia');
			}
			if($(this).data('id')) {
				$('#frmLancamentoCadastro').append('<input type="hidden" name="lancamentoFaturaDelete[]" value="' + $(this).data('id') + '"/>');
			}
		});
		
		
		$.ajax({
			url: contextPath + '/lancamento/removerLancamentoFatura',
			type: 'POST',
			data: data,
			success: function(result) {
				if(typeof result == 'string') {
					$('#layLancamentoFaturaList').replaceWith(result);
					
					message = i18n[i18n.locale]['lancamentoFatura.remover.success'];
					if(len > 1) {
						message = i18n[i18n.locale]['lancamentoFatura.remover.success.p'].template({"0": len});
					}
					msgSuccess(message);
					$('#lancValorDoc').change();
				} else {
					error(result, $('#frmLancamentoCadastro'));
				}
			}
		}).always(function() {
			loading(false);
		});
	});

}



/**
 * Recarrecar combo de Cartao de Credito ao fechar janela de lista Cartao de Credito
 */
$(document).on('hidden.bs.modal', '#dialogCartaoCreditoCadastro', function() {
	$('#lancCartaoCredito').trigger('loadOptions');
});



/**
 * Setar confirmacao ao alterar Favorecido e Tipo Documento
 */
$('#lancTipoDoc, #lancFavorecido').change(function() {
	$('#frmLancamentoCadastro').setHidden('isConfirmTipoDocPessoTipo', false);
});



/**
 * Setar confirmacao ao alterar Nº Documento
 */
$('#lancNumeroDoc').change(function() {
	$('#frmLancamentoCadastro').setHidden('isConfirmNumeroDocExists', false);
});



/**
 * Janela Adicionar Despesa / Receita
 * @param tipo
 */
function dialogAdicionarLancamentoItem(tipo) {
	
	loading(true);
	
	var dialogTitle = 'ItemTipo.' + tipo + 's';
	if(tipo == 'CPR' || tipo == 'VEN') {
		dialogTitle = 'produtos';
	}
	
	var data = {
		tpl: 'dialog',
		dialogId: 'ItemConsultar',
		dialogTitle: dialogTitle,
		status: 'ATI',
		dialogFootOk: 'adicionarLancamentoItem(\'' + tipo + '\')',
		favorecido_id: $('#lancFavorecido').prev().val()
	};
	
	
	$('#tbyLancamentoItemList tr').each(function(i) { 
		data['idsNotIn[' + i + ']'] = $(this).data('itemId');
	});
		
	var url = contextPath + '/cadastros/';
	switch(tipo) {
	case 'DES':
		url += 'despesas';
		break;
	case 'REC':
		url += 'receitas';
		break;
	default:
		url += 'produtos';
	}
	
	$.ajax({
		url: url,
		data: data,
		success: function(data) {
			$('body').append(data);
			$('#dialogItemConsultar').find('.bt-ok').prop('disabled', true);
			$('#frmItemConsultar').on('row.select', function() {
				$('#dialogItemConsultar').find('.bt-ok').prop('disabled', !$('#frmItemConsultar').data('selected').length);
			});
		}
	}).always(function() {
		loading(false);
	});
	
}



/**
 * Adicionar Despesa/Receita selecionados na Janela 
 * @param tipo
 */
function adicionarLancamentoItem(tipo) {

	var selected = $('#frmItemConsultar').data('selected');
	
	if(selected && selected.length) {
		loading(true);
		
		var data = {viewid: $('#lancViewid').val()};
		for(var i = 0; i < selected.length; i++) {
			data['ids[' + i + ']'] = selected[i];
		}
		
		data.valorDoc = $('#lancValorDoc').val(); 
		
		$.ajax({
			url: contextPath + '/lancamento/adicionarLancamentoItem',
			type: 'post',
			data: data,
			success: function(result) {
				
				if(typeof result == 'string') {
					
					$('#layLancamentoItemList').replaceWith(result);
					$('#dialogItemConsultar').modal('hide');
					enableDisableLancamentoParcelaBt();
					msgSuccess(i18n.get('item.' + tipo + '.adicionado.success' + (selected.length == 1 ? '' : '.p')));
					
					var dialogItemValorNull = [];
					
					$('#tbyLancamentoItemList tr').filter(function(){ return $.inArray($(this).data('itemId'), selected) != -1; }).each(function() {
						var valorUnit = parseFloat($(this).find('td:eq(4)').text().unformat());
						var selector = '#tbyLancamentoItemList tr[data-item-id=' + $(this).data('itemId') + ']';
						if(isNaN(valorUnit)) {
							dialogItemValorNull.push(function(callback) {
								editarLancamentoItem($(selector), callback);
							});
						}
					});
					
					
					if(dialogItemValorNull.length) {
						asyncWaterfall(dialogItemValorNull);
					}
					
				} else {
					error(result, $('#frmLancamentoCadastro'));
				}
				
				
			}
		}).always(function() {
			loading(false);
		});
		
	
	}
}



/**
 * Selecionar Despesa Receita
 * @param self
 */
function selecionaLancamentoItem(self) {
	if($(self).is('.active')) {
		$(self).removeClass('active');
	} else {
		$(self).addClass('active');
	}
	enableDisableLancamentoItemBt();
}



/**
 * Habilitar desabilitar Botao Despesa / Receita
 */
function enableDisableLancamentoItemBt() {
	var selected = $('#tbyLancamentoItemList tr.active').length;
	$('#lancBtEditarLancamentoItem').prop('disabled', selected != 1);
	$('#lancBtExcluitLancamentoItem').prop('disabled', selected == 0);
	enableDisableLancamentoParcelaBt();
}



/**
 * Remover Receita / Despesa
 */
function removerLancamentoItem() {
	
	var tipo = $('#frmLancamentoCadastro').data('tipo');
	
	var $rowLancamentoItem = $('#tbyLancamentoItemList tr.active');
	var lenLancamentoItem = $rowLancamentoItem.length;
	msgConfirm(i18n[i18n.locale]['lancamentoItem.' + tipo + '.excluir.confirm' + (lenLancamentoItem == 1 ? '' : '.p')].template({"0": lenLancamentoItem}), function() {
		
		loading(true);
		
		var data = {viewid: $('#lancViewid').val()};
		
		$rowLancamentoItem.each(function(i, tr) {
			data['indexs[' + i + ']'] = $(tr).data('index');
			if($(tr).data('id')) {
				$('#frmLancamentoCadastro').append('<input type="hidden" name="lancamentoItemDelete[]" value="' + $(tr).data('id') + '"/>');
			}
		});	
		
		$.ajax({
			url: contextPath + '/lancamento/removerLancamentoItem',
			type: 'post',
			data: data,
			success: function(result) {
				$('#layLancamentoItemList').replaceWith(result);
				enableDisableLancamentoItemBt();
				
				msgSuccess(i18n[i18n.locale]['lancamentoItem.' + tipo + '.excluido.success' + (lenLancamentoItem == 1 ? '' : '.p')].template({"0": lenLancamentoItem}));
			}
		}).always(function() {
			loading(false);
		});
		
	});
	
}



/**
 * Editar Informacoes Despesa / Receita
 * @param self
 * @param callback
 */
function editarLancamentoItem(self, callback) {
	loading(true);
	if(self) {
		$('#tbyLancamentoItemList tr.active').removeClass('active');
		$(self).addClass('active');
		enableDisableLancamentoItemBt();
	}
	
	$.ajax({
		url: contextPath + '/lancamento/editarLancamentoItem',
		data: {index: $('#tbyLancamentoItemList tr.active').data('index'), viewid: $('#lancViewid').val(), tipo: $('#frmLancamentoCadastro').data('tipo')},
		cache: false,
		success: function(result) {
			var $dialog = $(result);
			$('body').append($dialog);
			
			if(typeof callback == 'function') {
				$dialog.on('hidden.bs.modal', function() {
					callback();
				});
			}
		}
	}).always(function() {
		loading(false);
	});
	
}



/**
 * Janela Adicionar Parcela
 */
function dialogAdicionarLancamentoParcela() {
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/lancamentoParcela',
		data: $('#frmLancamentoCadastro').serialize(),
		success: function(data) {
			if(typeof data == 'string') {
				var $dialog = $(data);
				$('body').append($dialog);
				
				$dialog.on('close', function() {
					msgSuccess(i18n[i18n.locale]['lancamentoParcela.' + $('#frmLancamentoCadastro').data('tipo')  + '.adicionado.success']);
					enableDisableLancBtBaixa();
				});
			} else {
				error(data);
			}
		}
	}).always(function() {
		loading(false);
	});
}



/**
 * Selecionar Parcela
 * @param self
 */
function selecionaLancamentoParcela(self) {
	
	if($(self).is('.active')) {
		$(self).removeClass('active');
	} else {
		$(self).addClass('active');
	}
	
	enableDisableLancamentoParcelaBt();

}



/**
 * Habilitar Desabilitar Botoes Lancamento Parcela
 */
function enableDisableLancamentoParcelaBt() {
	var selected = $('#tbyLancamentoParcelaList tr.active').length;
	$('#lancBtEditarLancamentoParcela').prop('disabled', selected != 1);
	$('#lancBtExcluitLancamentoParcela').prop('disabled', selected == 0);
	
	var item = $('#tbyLancamentoItemList tr, #tbyLancamentoFaturaList tr').length;
	$('#lancBtAdicionarLancamentoParcela').prop('disabled', item == 0);
	
	var parcelas = $('#tbyLancamentoParcelaList tr').length;
	$('#lancBtDividirLancamentoParcela').prop('disabled', parcelas == 0);
	
}



/**
 * Editar Lancamento Parcela
 * @param self
 */
function editarLancamentoParcela(self) {
	loading(true);
	if(self) {
		$('#tbyLancamentoParcelaList tr.active').removeClass('active');
		$(self).addClass('active');
		enableDisableLancamentoParcelaBt();
	}
	
	$.ajax({
		url: contextPath + '/lancamento/lancamentoParcela',
		data: {
			index: $('#tbyLancamentoParcelaList tr.active').data('index'), 
			viewid: $('#lancViewid').val(),
			'lancamento.tipoDoc': $('#lancTipoDoc').val(),
			'lancamento.valorDoc': $('#lancValorDoc').val(),
		},
		cache: false,
		success: function(result) {
			if(typeof result == 'string') {
				var $dialog = $(result);
				$('body').append($dialog);
				
				$dialog.on('close', function() {
					msgSuccess(i18n[i18n.locale]['lancamentoParcela.' + $('#frmLancamentoCadastro').data('tipo')  + '.editado.success']);
					enableDisableLancBtBaixa();
				});
			} else {
				error(result, $('#frmLancamentoCadastro'));
			}
		}
	}).always(function() {
		loading(false);
	});
}



/**
 * Remover Lancamento Parcela
 */
function removerLancamentoParcela() {
	var tipo = $('#frmLancamentoCadastro').data('tipo');
	
	var $selected = $('#tbyLancamentoParcelaList tr.active');
	var len = $selected.length;
	var message = i18n[i18n.locale]['lancamentoParcela.' + tipo + '.remover.confirm'];
	if(len > 1) {
		message = i18n[i18n.locale]['lancamentoParcela.' + tipo + '.remover.confirm.p'].template({"0": len});
	}
	
	
	msgConfirm(message, function() {
	
		loading(true);
		var data = {viewid: $('#lancViewid').val()};
		
		$selected.each(function(i, tr) {
			data['indexs[' + i + ']'] = $(tr).data('index');
			if($(tr).data('id') && $(tr).data('status') != 'BXD' && $(tr).data('status') != 'COM') {
				$('#frmLancamentoCadastro').append('<input type="hidden" name="lancamentoParcelaDelete[]" value="' + $(tr).data('id') + '"/>');
			}
		});	
		
		$.ajax({
			url: contextPath + '/lancamento/removerLancamentoParcela',
			type: 'post',
			data: data,
			success: function(result) {
				if(typeof result == 'string') {
					$('#layLancamentoParcelaList').replaceWith(result);
					enableDisableLancamentoParcelaBt();
					
					var message = i18n[i18n.locale]['lancamentoParcela.' + tipo + '.remover.success'];
					if(len > 1) {
						message = i18n[i18n.locale]['lancamentoParcela.' + tipo + '.remover.success.p'].template({"0": len});
					}
					msgSuccess(message);
					enableDisableLancBtBaixa();
				} else {
					error(result, $('#frmLancamentoCadastro'));
				}
			}
		}).always(function() {
			loading(false);
		});
	});
}



/**
 * Dividir Lancamento Parcela
 */
function dividirLancamentoParcela() {
	msgConfirm(i18n[i18n.locale]['lancamentoParcela.' + $('#frmLancamentoCadastro').data('tipo') + '.dividir.confirm'], function() {
		loading(true);
		$.ajax({
			url: contextPath + '/lancamento/dividirLancamentoParcela',
			data: {
				viewid: $('#lancViewid').val(),
				'tipoDoc': $('#lancTipoDoc').val(),
				'valorDoc': $('#lancValorDoc').val()
			},
			success: function(result) {
				if(typeof result == 'string') {
					$('#layLancamentoParcelaList').replaceWith(result);
				} else {
					error(result, $('#frmLancamentoCadastro'));
				}
			}
		}).always(function() {
			loading(false);
		});
	});
}



/**
 * Janela Adicionar Lancamento Rateio
 */
function dialogAdicionarLancamentoRateio() {
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/lancamentoRateio',
		data: {viewid: $('#lancViewid').val()},
		success: function(data) {
			if(typeof data == 'string') {
				var $dialog = $(data);
				$('body').append($dialog);
				
				$dialog.on('close', function() {
					msgSuccess(i18n[i18n.locale]['lancamentoRateio.adicionado.success']);
				});
			} else {
				error(data, $('#frmLancamentoCadastro'));
			}
		}
	}).always(function() {
		loading(false);
	});
	
}



/**
 * Selecionar Lancamento Rateio
 * @param self
 */
function selecionaLancamentoRateio(self) {
	if($(self).is('.active')) {
		$(self).removeClass('active');
	} else {
		$(self).addClass('active');
	}
	
	enableDisableLancamentoRateioBt();
	
}



/**
 * Habilitar Desabilitar Botoes Lancamento Rateio
 */
function enableDisableLancamentoRateioBt() {
	var selected = $('#tbyLancamentoRateioList tr.active').length;
	$('#lancBtEditarLancamentoRateio').prop('disabled', selected != 1);
	$('#lancBtExcluitLancamentoRateio').prop('disabled', selected == 0);
	
	var item = $('#tbyLancamentoItemList tr, #tbyLancamentoFaturaList tr').length;
	$('#lancBtAdicionarLancamentoParcela').prop('disabled', item == 0);
	
	var parcelas = $('#tbyLancamentoRateioList tr').length;
	$('#lancBtDividirLancamentoRateio').prop('disabled', parcelas == 0);
}



/**
 * Editar Lancamento Rateio
 * @param self
 */
function editarLancamentoRateio(self) {
	loading(true);
	if(self) {
		$('#tbyLancamentoRateioList tr.active').removeClass('active');
		$(self).addClass('active');
		enableDisableLancamentoRateioBt();
	}
	
	$.ajax({
		url: contextPath + '/lancamento/lancamentoRateio',
		data: {index: $('#tbyLancamentoRateioList tr.active').data('index'), viewid: $('#lancViewid').val()},
		cache: false,
		success: function(data) {
			var $dialog = $(data);
			$('body').append($dialog);
			
			$dialog.on('close', function() {
				msgSuccess(i18n[i18n.locale]['lancamentoRateio.editado.success']);
			});
		}
	}).always(function() {
		loading(false);
	});
}



/**
 * Remover Lancamento Rateio
 */
function removerLancamentoRateio() {
	
	var message = i18n[i18n.locale]['lancamentoRateio.remover.confirm'];
	var $selected = $('#tbyLancamentoRateioList tr.active');
	var len = $selected.length;
	if(len > 1) {
		message = i18n[i18n.locale]['lancamentoRateio.remover.confirm.p'].template({"0": len});
	}
	
	msgConfirm(message, function() {
		loading(true);
	
		var data = {viewid: $('#lancViewid').val()};
		
		$selected.each(function(i, tr) {
			data['indexs[' + i + ']'] = $(tr).data('index');
			if($(tr).data('id')) {
				$('#frmLancamentoCadastro').append('<input type="hidden" name="lancamentoRateioDelete[]" value="' + $(tr).data('id') + '"/>');
			}
		});	
		
		$.ajax({
			url: contextPath + '/lancamento/removerLancamentoRateio',
			type: 'post',
			data: data,
			success: function(result) {
				$('#layLancamentoRateioList').replaceWith(result);
				enableDisableLancamentoRateioBt();
				
				message = i18n[i18n.locale]['lancamentoRateio.removido.success'];
				if(len > 1) {
					message = i18n[i18n.locale]['lancamentoRateio.removido.success.p'].template({"0": len});
				}
				msgSuccess(message);
			}
		}).always(function() {
			loading(false);
		});
	});
}



/**
 * Dividir Lancamento Rateio
 */
function dividirLancamentoRateio() {
	msgConfirm(i18n[i18n.locale]['lancamentoRateio.dividir.confirm'], function() {
		loading(true);
		$.ajax({
			url: contextPath + '/lancamento/dividirLancamentoRateio',
			data: {
				viewid: $('#lancViewid').val(),
				'tipoDoc': $('#lancTipoDoc').val(),
				'valorDoc': $('#lancValorDoc').val()
			},
			success: function(result) {
				$('#layLancamentoRateioList').replaceWith(result);
				enableDisableLancamentoRateioBt();
			}
		}).always(function() {
			loading(false);
		});
	});
}



/**
 * Habilita Desabilita Botao Baixa
 */
function enableDisableLancBtBaixa() {
	
	var lenLancamentoParcelaIsMeioLan = 0;
	lenLancamentoParcelaIsMeioLan = $('#tbyLancamentoParcelaList tr').filter(function(){ 
		return (($(this).data('conta') != '' && $(this).data('conta') != null) || ($(this).data('cartaoCredito') != '' && $(this).data('cartaoCredito') != null) || ($(this).data('cheque') != '' && $(this).data('cheque') != null)) && ($(this).data('status') == '' || $(this).data('status') == 'LAN'); 
	}).length;
	
	if($('#lancStatus').val() == 'LAN' && lenLancamentoParcelaIsMeioLan > 0) {
		$('#lancBtBaixa').prop('disabled', false);
	} else {
		$('#lancBtBaixa').prop('disabled', true);
	}
	
}

enableDisableLancBtBaixa();



/**
 * Baixa automatica
 */
$('#lancBtBaixa').click(function() {
	
	$('#frmLancamentoCadastro').setHidden('isBaixa', true).submit();
	
});



/**
 * Submeter Formulario
 */
$('#frmLancamentoCadastro').submit(function() {
	
	loading(true);
	$.ajax({
		url: contextPath + '/lancamento/cadastro',
		type: 'POST',
		dataType: 'jsonp',
		data: $(this).serialize()
	}).always(function() {
		loading(false);
	});
	
	return false;
});



/**
 * Erro Validacao Cadastro
 * @param errors
 */
function lancamentoCadastroError(errors) {
	error(errors, $('#frmLancamentoCadastro'));
}



/**
 * Sucesso Cadastro
 * @param lancamento
 */
function lancamentoCadastroSuccess(lancamento) {
	var tipo = $('#frmLancamentoCadastro').data('tipo');
	$('#dialogLancamentoCadastro').trigger('success', [lancamento[0]]).modal('hide');
	msgSuccess(i18n[i18n.locale]['lancamento.' + tipo + '.cadastro.success']);
	
	if(lancamento[1]) {
		msgSuccess(i18n[i18n.locale]['baixa.' + tipo + '.cadastro.success']);
	}
}