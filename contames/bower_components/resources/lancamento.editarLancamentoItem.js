$('#dialogEditarLancamentoItem').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#lancamentoItemValorUnit').toSelect();

$('#lancamentoItemQuant').formatDecimal(3);

$('#lancamentoItemValorUnit, #lancamentoItemDesconto').formatDecimal();

$('#lancamentoItemQuant, #lancamentoItemValorUnit, #lancamentoItemDesconto').blur(function() {
	
	var quant = parseFloat($('#lancamentoItemQuant').val().unformat());
	var valorUnit = parseFloat($('#lancamentoItemValorUnit').val().unformat());
	var valor = quant * valorUnit;
	var message = '';
	if(isNaN(quant)) { message = i18n[i18n.locale]['quant.notNull']; }
	if(isNaN(valorUnit)){ message = i18n[i18n.locale]['valorUnit.notNull']; }
		
	$('#lancamentoItemValor').html($.formatNumber(valor) || '<span class="text-danger"><strong>[&nbsp;' + message + '&nbsp;]</strong></span>');
	var desconto = parseFloat($('#lancamentoItemDesconto').val().unformat());
	if(isNaN(desconto)) {
		desconto = 0;
	}
	var total = valor - desconto;
	$('#lancamentoItemTotal').html($.formatNumber(total) || '<span class="text-danger"><strong>[&nbsp;' + message + '&nbsp;]</strong></span>');
	
});

$('#frmEditarLancamentoItem').submit(function() {
	
	loading(true);
	
	$.ajax({
		url: contextPath + '/lancamento/editarLancamentoItem',
		type: 'POST',
		data: $(this).serialize() + '&viewid=' + $('#lancViewid').val(),
		success: function(data) {
			if(typeof data == 'string') {
				$('#layLancamentoItemList').replaceWith(data);
				$('#dialogEditarLancamentoItem').modal('hide');
				msgSuccess(i18n[i18n.locale]['lancamentoItem.' + $('#frmLancamentoCadastro').data('tipo') + '.editado.success']);
				enableDisableLancamentoItemBt();
			} else {
				error(data, $('#frmEditarLancamentoItem'));
			}
		}
	}).always(function() {
		loading(false);
	});
	return false;
});