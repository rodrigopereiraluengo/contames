$(function() {
	$('#frmLancamentoConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.lancamento.' + $('#frmLancamentoConsultar').data('tipo') + '.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.lancamento.' + $('#frmLancamentoConsultar').data('tipo') + '.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.lancamento.' + $('#frmLancamentoConsultar').data('tipo') + '.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.lancamento.' + $('#frmLancamentoConsultar').data('tipo') + '.success.p')
	});
});

function lancamentoExcluirSuccess(lancamento) {
	$('#frmLancamentoConsultar').trigger('excluir.success', [lancamento]);
}