

$('#dialogSelectConta').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});


$('#dialogSelectContaId').comboEditable().toFocus();


$('#frmDialogSelectConta').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/conta/dialogSelect',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	}).always(function() {
		loading(false);
	});
	
	return false;
});


function dialogSelectContaSuccess(conta) {
	$('#dialogSelectConta').trigger('close', [conta]).modal('hide');
}