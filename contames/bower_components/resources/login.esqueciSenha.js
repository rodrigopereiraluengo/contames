$(function() {
	
	$('#email').toFocus();
	
	$('#frmEsqueciSenha').submit(function() {
		loading(true);
		$.ajax({
			url: $('#frmEsqueciSenha').attr('action'),
			data: $('#frmEsqueciSenha').serialize(),
			type: 'post',
			dataType: 'jsonp'
		}).always(function() {
			loading(false);
		});
		
		return false;
		
	}).find(':submit').prop('disabled', false);
	
});


function success() {
    msgSuccess(i18n.get('login.esqueciSenha.email.enviado'));
       
    $('#frmEsqueciSenha').trigger('reset');
  
    window.setTimeout(function(){
    	window.location.href = contextPath + '/entrar';
    }, 3000);
}