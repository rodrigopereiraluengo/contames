$('#dialogCompensacaoMovs').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#frmCompensacaoMovs').formSearch();

$('#compensacaoMovsDataDe').inputCalendar().toSelect();

$('#frmCompensacaoMovs').on('row.select', function() {
	var selected = $(this).data('selected') || [];
	$('#btCompensacaoMovsOk').prop('disabled', !selected.length);
});

$('#btCompensacaoMovsOk').click(function() {
	var $selected = $('#frmCompensacaoMovs').data('selected');
	if($selected && $selected.length) {
		loading(true);
		var data = {viewid: $('#compensacaoViewid').val(), data: $('#compensacaoData').val()};
		
		var len = $selected.length;
		var ib = 0;
		var it = 0;
		for(var i = 0; i < $selected.length; i++) {
			if($selected[i]['baixaItem']) {
				
				data['baixaItemIds[' + ib + ']'] = $selected[i]['baixaItem'];
				ib++;
			} else {
				data['transferenciaIds[' + it + ']'] = $selected[i]['transferencia'];
				it++;
			}
		}
		
		
		$.ajax({
			url: contextPath + '/compensacao/adicionarMovs',
			type: 'POST',
			data: data,
			success: function(result) {
				if(typeof result == 'string') {
					$('#layCompensacaoItem').replaceWith(result);
					$('#dialogCompensacaoMovs').modal('hide');
					compensacaoCalcularDiferenca();
					
					var message = i18n[i18n.locale]['compensacao.movs.adicionado.success'];
					if(len > 1) {
						message = i18n[i18n.locale]['compensacao.movs.adicionado.success.p'].template({"0": len});
					}
					msgSuccess(message);
				} else {
					error(result, $('#frmCompensacaoMovs'));
				}
			}
		}).always(function() {
			loading(false);
		});
	
	}
	
});