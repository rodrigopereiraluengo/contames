$('#dialogCompensacaoCadastro').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#compensacaoData').inputCalendar();

$('#compensacaoSaldo').formatDecimal().toSelect();


$('#compensacaoBtSelecionarMovs').click(function() {
	
	if($('#compensacaoData').val().blank()) {
		error([{category: 'data', message: i18n.get('data.notNull')}], $('#frmCompensacaoCadastro'));
	} else {
	
		loading(true);
		
		var compensacaoData = moment($('#compensacaoData').val(), i18n.get('momPatternDate'));
		
		var data = {
			'dataDe': compensacaoData.subtract(1, 'month').format(i18n.get('momPatternDate')),
			'dataAte': $('#compensacaoData').val(),
			'conta': $('#compensacaoContaId').val()
		};
		
		$('#tbyCompensacaoItem tr[data-baixa-item!=""]').each(function(i) {
			data['baixaItemIdsNotIn[' + i + ']'] = $(this).data('baixaItem');
		});
		
		$('#tbyCompensacaoItem tr[data-transferencia!=""]').each(function(i) {
			data['transferenciaIdsNotIn[' + i + ']'] = $(this).data('transferencia');
		});
		
		$.ajax({
			url: contextPath + '/compensacao/movs',
			data: data,
			success: function(result) {
				if(typeof result == 'string') {
					$('body').append(result);
				} else {
					error(result, $('#frmCompensacaoCadastro'));
				}
			}
		}).always(function() {
			loading(false);
		});
	}
});


$('#compensacaoBtAdicionarDebCompensacaoItem, #compensacaoBtAdicionarCredCompensacaoItem').click(function(e) {
	loading(true);
	$.ajax({
		url: contextPath + '/cadastros/' + ($(e.currentTarget).data('tipo') == 'DES' ? 'despesas' : 'receitas'),
		data: {
			tpl: 'dialog',
			dialogId: 'ItemConsultar',
			dialogTitle: ($(e.currentTarget).data('tipo') == 'DES' ? 'despesas' : 'receitas'),
			dialogFootOk: 'adicionarCompensacaoItem(\'' + $(e.currentTarget).data('tipo') + '\')',
			status: 'ATI',
			conta_id: $('#compensacaoContaId').val()
		},
		success: function(result) {
			$('body').append(result);
			$('#dialogItemConsultar').find('.bt-ok').prop('disabled', true);
			$('#frmItemConsultar').on('row.select', function() {
				$('#dialogItemConsultar').find('.bt-ok').prop('disabled', !$('#frmItemConsultar').data('selected').length);
			});
		}
	}).always(function() {
		loading(false);
	});
	
});

function adicionarCompensacaoItem(tipo) {
	if($('#compensacaoData').val()) {
		if($('#frmItemConsultar').data('selected') && $('#frmItemConsultar').data('selected').length) {
			
			loading(true);
			var data = {viewid: $('#compensacaoViewid').val(), data: $('#compensacaoData').val()};
			var $selected = $('#frmItemConsultar').data('selected');
			var len = $selected.length;
			for(var i = 0; i < $selected.length; i++) {
				data['ids[' + i + ']'] = $selected[i];
			}
			
			$.ajax({
				url: contextPath + '/compensacao/adicionarCompensacaoItem',
				type: 'post',
				data: data,
				success: function(result) {
					
					if(typeof result == 'string') {
					
						$('#layCompensacaoItem').replaceWith(result);
						$('#dialogItemConsultar').modal('hide');
						compensacaoCalcularDiferenca();
						
						var message = i18n[i18n.locale]['compensacaoItem.adicionar.' + tipo + '.success'];
						if(len > 1) {
							message = i18n[i18n.locale]['compensacaoItem.adicionar.' + tipo + '.success.p'].template({"0": len});
						}
						msgSuccess(message);
						
						var dialogItemValorNull = [];
										
						$('#tbyCompensacaoItem tr').filter(function() { 
							return $.inArray($(this).data('item'), $selected) != -1 && $(this).children('td:eq(3)').text().empty() && $(this).children('td:eq(3)').text().empty(); 
						}).each(function() {
							var valor = parseFloat($(this).find('td:eq(' + ($(this).data('tipo') == 'DES' ? '3' : '4') + ')').text().unformat());
							var selector = '#tbyCompensacaoItem tr[data-item=' + $(this).data('item') + '][data-index=' + $(this).index() + ']';
							if(isNaN(valor)) {
								dialogItemValorNull.push(function(callback) {
									$(selector).trigger('dblclick', [callback]);
								});
							}
						});
										
						if(dialogItemValorNull.length) {
							asyncWaterfall(dialogItemValorNull);
						}
					
					} else {
						error(result, $('#frmCompensacaoCadastro'));
					}
				
				}
			}).always(function() {
				loading(false);
			});
		
		}
	} else {
		error([{category: 'data', message: i18n.get("data.notNull")}], $('#frmCompensacaoCadastro'));
	}
}

$('#frmCompensacaoCadastro').on('click', '#tbyCompensacaoItem tr', function(e) {
	var $row = $(e.currentTarget);
	if($row.is('.active')) {
		$row.removeClass('active');
	} else {
		$row.addClass('active');
	}
	enableDisableCompensacaoItemBt();
}).on("dblclick", "#tbyCompensacaoItem tr", function(e, callback) {
	var $row = $(e.currentTarget);
	$row.closest('tbody').find('tr').removeClass('active');
	$row.addClass('active');
	$('.bt-edit', $('#frmCompensacaoCadastro')).trigger('click', [callback]);
	enableDisableCompensacaoItemBt();
	e.stopImmediatePropagation();
});


function enableDisableCompensacaoItemBt() {
	$('#compensacaoBtEditarCompensacaoItem').prop('disabled', $('#tbyCompensacaoItem tr.active').filter(function(){ return typeof $(this).data('item') != 'undefined'; }).length != 1);
	$('#compensacaoBtExcluitCompensacaoItem').prop('disabled', $('#tbyCompensacaoItem tr.active').length == 0);
}


$('#compensacaoBtEditarCompensacaoItem').click(function(e, callback) {
	$rowItem = $('#tbyCompensacaoItem tr.active').filter(function(){ return typeof $(this).data('item') != 'undefined'; });
	if($rowItem.length) {
		loading(true);
		$.ajax({
			url: contextPath + '/compensacao/editarCompensacaoItem',
			data: {viewid: $('#compensacaoViewid').val(), index: $rowItem.data('index')},
			success: function(result) {
				if(typeof result == 'string') {
					var $dialog = $(result);
					$('body').append($dialog);
					if(typeof callback == 'function') {
						$dialog.on('hidden.bs.modal', function() {
							callback();
						});
					}
				} else {
					error(result, $('#frmCompensacaoCadastro'));
				}
			}
		}).always(function() {
			loading(false);
		});
	}
});



$('#compensacaoBtExcluitCompensacaoItem').click(function() {
	
	var data = {viewid: $('#compensacaoViewid').val()};
	
	var $selected = $('#tbyCompensacaoItem tr.active');
	var len = $selected.length;
	
	var message = i18n[i18n.locale]['compensacao.movimentos.remover.confirm'];
	if(len > 1) {
		message = i18n[i18n.locale]['compensacao.movimentos.remover.confirm.p'].template({"0": len});
	}

	msgConfirm(message, function() {
	
		$selected.each(function(i, tr) {
			data['indexs[' + i + ']'] = $(tr).data('index');
			if($(tr).data('id')) {
				$('#frmCompensacaoCadastro')
					.append('<input type="hidden" name="compensacaoItemDelete[]" value="' + $(tr).data('id') + '"/>');
			}
		});	
		
		$.ajax({
			url: contextPath + '/compensacao/removerCompensacaoItem',
			type: 'post',
			data: data,
			success: function(result) {
				if(typeof result == 'string') {
					$('#layCompensacaoItem').replaceWith(result);
					enableDisableCompensacaoItemBt();
					compensacaoCalcularDiferenca();
					
					message = i18n[i18n.locale]['compensacao.movimentos.removido.success'];
					if(len > 1) {
						message = i18n[i18n.locale]['compensacao.movimentos.removido.success.p'].template({"0": len});
					}
					msgSuccess(message);
					
				} else {
					error(result, $('#frmCompensacaoCadastro'));
				}
			}
		});
	});
});


function compensacaoCalcularDiferenca() {
	try {
		var compensacaoSaldo = parseFloat($('#compensacaoSaldo').val().unformat());
		var saldo = parseFloat($('#compensacaoSaldoAnterior').text().unformat());
		if($('#lbSaldo').length && !isNaN(parseFloat($('#lbSaldo').text().unformat()))) {
			saldo = parseFloat($('#lbSaldo').text().unformat());
		}
		var diferenca = compensacaoSaldo - saldo;
		if(isNaN(compensacaoSaldo)) {
			$('#compensacaoDiferenca').html('<span class="text-danger"><strong>[&nbsp;' + i18n[i18n.locale]['compensacao.saldo.blank'] + '&nbsp;]</strong></span>');		
		} else {
			$('#compensacaoDiferenca').removeClass('font-red font-blue').text(diferenca.formatDecimal()).addClass(diferenca < 0 ? 'font-red' : 'font-blue');
		}
	} catch(e) {}
}

$('#compensacaoSaldo').change(compensacaoCalcularDiferenca);

$('#frmCompensacaoCadastro').submit(function() {
	loading(true);
	$.ajax({
		url: contextPath + '/compensacao/cadastro',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	}).always(function() {
		loading(false);
	});
	return false;
});

function compensacaoCadastroSuccess(compensacao) {
	$('#dialogCompensacaoCadastro').trigger('success', [compensacao]).modal('hide');
	msgSuccess(i18n[i18n.locale]['compensacao.cadastro.success']);
}