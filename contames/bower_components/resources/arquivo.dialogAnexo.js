$('#dialogAnexoArquivo').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#dialogAnexoArquivoNome:visible').toFocus();

$('#btDialogAnexoArquivoEditar').click(function() {
	$('input[name=isExcluir]').val(true);
	$('#layDialogAnexoArquivoInput').show();
	$('#layDialogAnexoArquivoNome').hide();
});

$('#frmDialogAnexoArquivo').submit(function(e, isSubmit) {
	
	var $form = $(this);
	
	var submit = function() {
		loading(true);
		$form
			.after('<iframe id="' + $form.attr('target') + '" name="' + $form.attr('target') + '" style="display: none"></iframe>')
			.trigger('submit', [true]);
		
	};
	
	if(isSubmit) {
		return true;
	} else {
		var arquivoNome = $('input[name="arquivo.nome"]', $form).val();
		if(arquivoNome != '' && $('#dialogAnexoArquivoNome').val() == '' && $('#layDialogAnexoArquivoInput').is(':visible')) {
			msgConfirm(i18n[i18n.locale]['arquivo.remover.confirm'].template({"0": arquivoNome}), submit);
		} else {
			submit();
		}
	}
	
	return false;
});

function anexoArquivoError(errors) {
	error(errors);
	$('#' + $('#frmDialogAnexoArquivo').attr('target')).remove();
}