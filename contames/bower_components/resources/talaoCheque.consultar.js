$(function() {
	$('#frmTalaoChequeConsultar').formSearch({
		deleteMessageConfirm: i18n.get('formSearch.delete.talaoCheque.confirm'),
		deleteMessageConfirmP: i18n.get('formSearch.delete.talaoCheque.confirm.p'),
		deleteMessageSuccess: i18n.get('formSearch.delete.talaoCheque.success'),
		deleteMessageSuccessP: i18n.get('formSearch.delete.talaoCheque.success.p')
	});
});

function talaoChequeExcluirSuccess(talaoCheque) {
	$('#frmTalaoChequeConsultar').trigger('excluir.success', [talaoCheque]);
}

$(document).on('close', '#dialogSelectConta', function(e, conta) {
	
	if(conta) {
		
		loading(true);
		
		$.ajax({
			url: contextPath + '/talaoCheque/cadastro',
			data: {contaId: conta.id},
			success: function(data) {
				if(typeof data == 'string') {
					var $dialog = $(data);
					$('body').append($dialog);
					
					$dialog.on('success', function(e, talaoCheque) {
						$('#frmTalaoChequeConsultar').submit();
					});
					
				} else {
					error(data);
				}
			}
		}).always(function() {
			loading(false);
		});
		
	}
});

$(document).on('hide.bs.modal', '#dialogSelectConta, #dialogContaCadastro', function(e, conta) {
	$('#frmTalaoChequeConsultar').submit();
});