$('#dialogRelatorioClassificacaoMovs').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#frmRelatorioClassificacaoMovs').formSearch({
	multipleSelectRows: false,
	selectRow: true
}).on('click', 'tbody tr', function(e) {
	
	
	var links = [];
	
	$row = $(e.currentTarget);
	
	$td = $(e.target);
	
	// Verifica se clicou no Favorecido
	if($td.data('favorecido')) {
		links.push({
			name: i18n.get('pessoa'), 
			url: '/pessoa/cadastro', 
			data: {
				id: $td.data('favorecido')
			}
		});
	}
	
	// Verifica se clicou na Conta
	if($td.data('conta')) {
		links.push({
			name: i18n.get('conta'), 
			url: '/conta/cadastro', 
			data: {
				id: $td.data('conta')
			}
		});
	}
	
	// Verifica se clicou no Cartao de Credito
	if($td.data('cartaoCredito')) {
		links.push({
			name: i18n.get('cartaoCredito'), 
			url: '/cartaoCredito/cadastro', 
			data: {
				id: $td.data('cartaoCredito')
			}
		});
	}
	
	// Verifica se clicou no Cheque
	
			
	// Verifica se eh lancamento
	if($row.data('lancamentoParcela')) {
		links.push({
			name: i18n.get('lancamento'), 
			url: '/lancamento/cadastro', 
			data: {
				id: $row.data('lancamento'),
				lancamentoParcela: $row.data('lancamentoParcela'),
				tipo: $row.data('tipo')
			}
		});
	}
	
	
	// Verifica se eh Pagamento / Recebimento
	if($row.data('baixaItem')) {
		links.push({
			name: i18n.get($row.data('tipo') == 'DES' ? 'pagamento' : 'recebimento'), 
			url: '/baixa/cadastro', 
			data: {
				id: $row.data('baixa'),
				baixaItem: $row.data('baixaItem'),
				tipo: $row.data('tipo')
			}
		});
	}
	
	
	// Verifica se eh Transferencia
	if($row.data('transferencia')) {
		links.push({
			name: i18n.get('transferencia'), 
			url: '/transferencia/cadastro', 
			data: {
				id: $row.data('transferencia')
			}
		});
	}
	
	
	// Verifica se eh Compensacao
	if($row.data('compensacaoItem')) {
		links.push({
			name: i18n.get('conciliacao'), 
			url: '/compensacao/cadastro', 
			data: {
				id: $row.data('compensacao'),
				compensacaoItem: $row.data('compensacaoItem')
			}
		});
	}
	

	if(links.length == 1) {
		relatorioDataOpenLink(links[0]);
	} else {
		var html = '<div class="list-group">';
		$.each(links, function(i, link) {
			html += '<a href="javascript:;" class="list-group-item" data-link-index="' + i + '">' + link.name + '</a>'; 
		});
		html += '</div>'; 
		msgAlert({title: i18n.get('irPara'), message: html, cancel: true});
		
		$.each(links, function(i, link) {
			$('a.list-group-item[data-link-index="' + i + '"]').click(function() {
				relatorioDataOpenLink(link);
			});
		});
	}
	
	
	
});


function relatorioDataOpenLink(link) {
	loading(true);
	$.ajax({
		url: link.url,
		data: link.data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioClassificacao'));
			}
		}
	}).always(function() {
		loading(false);
	});
}