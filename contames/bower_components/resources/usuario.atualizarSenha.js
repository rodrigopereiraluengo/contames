/**
 * Janela Atualizar Senha
 */
$('#dialogUsuarioAtualizarSenha').modal({ show: true, keyboard: false, backdrop: 'static' }).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#senhaAtual').focus();

$('#frmUsuarioAtualizarSenha').submit(function() {
	var $this = $(this);
	loading(true);
	$.ajax({
		url: contextPath + '/usuario/atualizarSenha',
		type: 'POST',
		data: $this.serialize(),
		success: function(result) {
			if(typeof result == 'string') {
				msgSuccess(i18n.get('usuario.atualizarSenha.sucesso'));
				$('#dialogUsuarioAtualizarSenha').modal('hide');
			} else {
				error(result, $this);
			}
		}
	}).always(function() {
		loading(false);
	});
	
	return false;
});