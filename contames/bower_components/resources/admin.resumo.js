var events, agenda;
$(function() {
	
	
	/**
	 * Data do Lancamento 
	 */
	$('#lancamentoRapidoVenc').inputCalendar();
	
	
	/**
	 * Auto complete do Favorecido
	 */
	$('#lancamentoRapidoFavorecido').autoCompleteEditable().on('setValue', function(e, pessoa) {
		if(pessoa.pessoaTipo == 'J') {
			if(typeof pessoa.fantasia != 'undefined') {
				$(this).val(pessoa.fantasia);
			} else {
				$(this).val(pessoa.razaoSocial);
			}
		} else {
			if(typeof pessoa.apelido != 'undefined') {
				$(this).val(pessoa.apelido);
			} else {
				$(this).val(pessoa.nome);
			}
		}
	});
	
	
	/**
	 * Tipo Lancamento
	 */
	$('#lancamentoTipo').change(function() {
		if($(this).val() == 'DES') {
			$('#layDespesa').show();
			$('#layReceita').hide();
			$('#lancamentoRapidoBtBaixa').html('<i class="fa fa-arrow-right font-red"></i>\n<b>' + i18n.get('btBaixa_DES') + '</b>');
			$('#lbLancamentoRapidoFav').text(i18n.get('pagarA'));
		} else {
			$('#layReceita').show();
			$('#layDespesa').hide();
			$('#lancamentoRapidoBtBaixa').html('<i class="fa fa-arrow-left font-blue"></i>\n<b>' + i18n.get('btBaixa_REC') + '</b>');
			$('#lbLancamentoRapidoFav').text(i18n.get('receberDe'));
		}
		
		
		var cheque = $('#frmLancamentoRapido').data('cheque' + $(this).val().toLowerCase());
		if(cheque) {
			if($(this).val() == 'DES') {
				$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoRapido').val(cheque.id);
				$('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoRapido').val(cheque.talaoCheque.id);
				$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val(cheque.numero);
				$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val(cheque.data);
				$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val(cheque.obs);
			} else if($(this).val() == 'REC') {
				$('input[name="lancamentoParcela.cheque.id"]', '#frmLancamentoRapido').val(cheque.id);
				$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoRapido').val(cheque.banco.id);
				$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val(cheque.numero);
				$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val(cheque.data);
				$('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoRapido').val(cheque.nome.id);
				$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val(cheque.obs);
			}
		} else {
			$('input[name^="lancamentoParcela.cheque"]', '#frmLancamentoRapido').val("");
		}
		
		lightDarkBtLancamentoRapidoCheque();
		
		$('#frmLancamentoRapido').data('tipo', $(this).val());
		$('.formatDecimal').trigger('keyup.formatDecimal.fontColor');
		
	});
	
	
	/**
	 * Auto complete LancamentoParcela
	 */
	$('#lancamentoDes').autoCompleteEditable().on('setValue', function(e, item) {
		$(this).val(item.nome);
	});
	
	$('#lancamentoRec').autoCompleteEditable().on('setValue', function(e, item) {
		$(this).val(item.nome);
	});
	
	
	/**
	 * LancamentoParcela Valor
	 */
	$('#lancamentoRapidoValor').formatDecimal();
	
	
	/**
	 * Meio
	 */
	$('#lancamentoRapidoMeio').change(function() {
		if($(this).val() == '') {
			$('#btLancamentoRapidoCartaoCredito, #btLancamentoRapidoConta, #btLancamentoRapidoCheque, #btLancamentoRapidoBoleto').prop('disabled', true).removeClass('btn-primary').addClass('btn-default');
		} else {
			enableDisableBtLancamentoRapidoCartaoCredito();
			enableDisableBtLancamentoRapidoConta();
			enableDisableBtLancamentoRapidoCheque();
			enableDisableBtLancamentoRapidoBoleto();
		}
	}).change();


	
	/**
	 * Conta
	 */
	$('#btLancamentoRapidoConta').click(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/conta/dialogSelect',
			data: {
				id: $('#lancamentoRapidoConta').val()
			},
			success: function(data) {
				var $element = $(data);
				$('body').append($element);
				
				$element.on('close', function(e, conta) {
					if($('#lancamentoRapidoConta').val() != conta.id) {
						if(conta.id) {
							msgSuccess(i18n[i18n.locale]['conta.selecionada'].template({"0": conta.nome}));
						} else {
							msgSuccess(i18n[i18n.locale]['conta.nenhuma.selecionada']);
						}
					}
					$('#lancamentoRapidoConta').val(conta.id);
					lightDarkBtLancamentoRapidoConta();
				});
			}
		}).always(function() {
			loading(false);
		});
		
	});

	


	/**
	 * Cartao de Credito
	 */
	$('#btLancamentoRapidoCartaoCredito').click(function() {
		if($('#lancamentoTipo').val()) {
			loading(true);
			$.ajax({
				url: contextPath + '/cartaoCredito/dialogSelect',
				data: {
					id: $('#lancamentoRapidoCartaoCredito').val(), 
					isLimite: $('#lancamentoRapidoCartaoCreditoIsLimite').val()
				},
				success: function(data) {
					
					var $element = $(data);
					
					$('body').append($element);
					
					$element.on('close', function(e, cartaoCredito) {
						if($('#lancamentoRapidoCartaoCredito').val() != cartaoCredito[0].id) {
							if(cartaoCredito[0].id) {
								msgSuccess(i18n[i18n.locale]['cartaoCredito.selecionado'].template({"0": cartaoCredito[0].nome}));
								$('#lancamentoRapidoCartaoCreditoIsLimite').val(cartaoCredito[0].isLimite);
							} else {
								msgSuccess(i18n[i18n.locale]['cartaoCredito.nenhum.selecionado']);
								$('#lancamentoRapidoCartaoCreditoIsLimite').val('');
							}
						}
						$('#lancamentoRapidoCartaoCredito').val(cartaoCredito[0].id);
						$('#lancamentoRapidoCartaoCreditoIsLimite').val(cartaoCredito[1]);
						lightDarkBtLancamentoRapidoCartaoCredito();
					});
					
				}
			}).always(function() {
				loading(false);
			});
		}
	});


	
	
	
	
	/**
	 * Cheque
	 */
	$('#btLancamentoRapidoCheque').click(function() {
		
		loading(true);
		
		if($('#lancamentoTipo').val() == 'DES') {
			$.ajax({
				url: contextPath + '/talaoCheque/dialogSelect',
				data: {
					'cheque.talaoCheque.id': $('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoRapido').val(),
					'cheque.numero': $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val(),
					'cheque.data': $('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val(),
					'cheque.obs': $('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val()
				},
				success: function(data) {
					var $dialog = $(data);
					$('body').append($dialog);
					
					$dialog.on('close', function(e, cheque) {
						
						if(typeof cheque.talaoCheque != 'undefined') {
							if($('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoRapido').val() != cheque.talaoCheque.id || $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val() != cheque.numero){
								msgSuccess(i18n[i18n.locale]['cheque.selecionado.success'].template({"0": cheque.numero, "1": cheque.talaoCheque.conta.nome}));
							}
							$('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoRapido').val(cheque.talaoCheque.id);
							$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val(cheque.numero);
							$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val(cheque.data);
							$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val(cheque.obs);
						} else {
							if($('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoRapido').val() != "" && $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val() != ""){
								msgSuccess(i18n[i18n.locale]['cheque.nenhum.selecionado']);
							}
							$('input[name="lancamentoParcela.cheque.talaoCheque.id"]', '#frmLancamentoRapido').val("");
							$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val("");
							$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val("");
							$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val("");
						}
						
						$('#frmLancamentoRapido').data('chequedes', cheque);
						
						lightDarkBtLancamentoRapidoCheque();
						
					});
					
				}
			}).always(function() {
				loading(false);
			});
		} else {
			$.ajax({
				url: contextPath + '/cheque/dialogProps',
				data: {
					'cheque.banco.id': $('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoRapido').val(),
					'cheque.numero': $('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val(),
					'cheque.data': $('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val(),
					'cheque.nome.id': $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoRapido').val(),
					'cheque.obs': $('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val()
				},
				success: function(data) {
					var $dialog = $(data);
					$('body').append($dialog);
					$dialog.on('close', function(e, cheque) {
						cheque = $.extend({
							id: '',
							banco: {id: ''},
							nome: {id: ''}
						}, cheque);
						
						try{
							
						
						if(typeof cheque.numero != 'undefined') {
							
							if(
							$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoRapido').val() != cheque.banco.id
							||
							$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val() != cheque.numero
							||
							$('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoRapido').val() != cheque.nome.id
							) {
								
								var message = i18n[i18n.locale]['cheque.informado.success'].template({"0": cheque.numero});
								msgSuccess(message);
							}
							
							$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoRapido').val(cheque.banco.id || '');
							$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val(cheque.numero);
							$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val(cheque.data);
							if(typeof cheque.nome != 'undefined') { $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoRapido').val(cheque.nome.id); }
							else { $('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoRapido').val(''); }
							$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val(cheque.obs);
						} else {
							
							if($('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val() != cheque.numero) {
								msgSuccess(i18n[i18n.locale]['cheque.nenhum.informado']);
							}
							
							$('input[name="lancamentoParcela.cheque.banco.id"]', '#frmLancamentoRapido').val('');
							$('input[name="lancamentoParcela.cheque.numero"]', '#frmLancamentoRapido').val('');
							$('input[name="lancamentoParcela.cheque.data"]', '#frmLancamentoRapido').val('');
							$('input[name="lancamentoParcela.cheque.nome.id"]', '#frmLancamentoRapido').val('');
							$('input[name="lancamentoParcela.cheque.obs"]', '#frmLancamentoRapido').val('');
						}
						
						$('#frmLancamentoRapido').data('chequerec', cheque);
						lightDarkBtLancamentoRapidoCheque();
						}catch(e) {
							console.log(e);
						}
					});
				}
			}).always(function() {
				loading(false);
			});
		}
	});
	
	
	
	
	
	
	/**
	 * Boleto
	 */
	$('#btLancamentoRapidoBoleto').click(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/lancamento/lancamentoParcelaBoleto',
			data: {boletoTipo: $('#lancamentoRapidoBoletoTipo').val(), boletoNumero: $('#lancamentoRapidoBoletoNumero').val()},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('close', function(e, boleto) {
					if(boleto.boletoNumero) {
						$('#lancamentoRapidoBoletoTipo').val(boleto.boletoTipo);
						$('#lancamentoRapidoBoletoNumero').val(boleto.boletoNumero);
						if(boleto.boletoTipo == 'COB') {
							try {
								var objBoleto = getBoleto(boleto.boletoNumero);
								
								var isData = false;
								if($('#lancamentoRapidoVenc').val() != objBoleto.data) {
									isData = true;
								}
								
								var isValor = false;
								if($('#lancamentoRapidoValor').val() != objBoleto.valor.formatDecimal()) {
									isValor = true;
								}
								
								if(isData && isValor) {
									msgConfirm(i18n[i18n.locale]['boletoNumero.data.valor.atualizar'].template({"0": objBoleto.data, "1": objBoleto.valor.formatDecimal()}), function() {
										$('#lancamentoRapidoVenc').val(objBoleto.data);
										$('#lancamentoRapidoValor').val(objBoleto.valor.formatDecimal());
									});
								} else if(isData) {
									msgConfirm(i18n[i18n.locale]['boletoNumero.data.atualizar'].template({"0": objBoleto.data}), function() {
										$('#lancamentoRapidoVenc').val(objBoleto.data);
									});
								} else if(isValor) {
									msgConfirm(i18n[i18n.locale]['boletoNumero.valor.atualizar'].template({"0": objBoleto.valor.formatDecimal()}), function() {
										$('#lancamentoRapidoValor').val(objBoleto.valor.formatDecimal());
									});
								}
								
							}catch(e){}
						} 
						
						msgSuccess(i18n[i18n.locale]['codigoBarras.informado.success']);
						
					} else {
						$('#lancamentoRapidoBoletoTipo').val('');
						$('#lancamentoRapidoBoletoNumero').val('');
						msgSuccess(i18n[i18n.locale]['codigoBarras.nenhum.informado']);
					}
					lightDarkBtLancamentoRapidoBoleto();
				});
			}
		}).always(function() {
			loading(false);
		});
		
	});
	
	
	
	
	/**
	 * Arquivo Parcela
	 */
	$('#btLancamentoRapidoArquivoDoc').click(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/arquivo/dialogAnexo',
			data: {
					viewid: $('#lancamentoRapidoArquivoDocTemp').val().split('_')[1],
					target: 'lancamentoRapidoDoc',
					'arquivo.temp': $('#lancamentoRapidoArquivoDocTemp').val(),
					'arquivo.nome': $('#lancamentoRapidoArquivoDocNome').val(),
					'arquivo.contentType': $('#lancamentoRapidoArquivoDocContentType').val(),
					'arquivo.size': $('#lancamentoRapidoArquivoDocSize').val()
			},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('close', function(e, arquivo) {
					
					if($('#lancamentoRapidoArquivoDocNome').val() != arquivo.nome || $('#lancamentoRapidoArquivoDocContentType').val() != arquivo.contentType || $('#lancamentoRapidoArquivoDocSize').val() != arquivo.size) {
						if(arquivo.nome) {
							msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
						} else {
							msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#lancamentoRapidoArquivoDocNome').val()}));
						}
					}
					
					$('#lancamentoRapidoArquivoDocNome').val(arquivo.nome);
					$('#lancamentoRapidoArquivoDocContentType').val(arquivo.contentType);
					$('#lancamentoRapidoArquivoDocSize').val(arquivo.size);
					
					lightDarkBtLancamentoRapidoArquivoDoc();
					
				});
			}
		}).always(function() {
			loading(false);
		});
		
	});
	$('#btLancamentoRapidoArquivo').click(function() {
		loading(true);
		$.ajax({
			url: contextPath + '/arquivo/dialogAnexo',
			data: {
					viewid: $('#lancamentoRapidoArquivoTemp').val().split('_')[1],
					target: 'lancamentoRapido',
					'arquivo.temp': $('#lancamentoRapidoArquivoTemp').val(),
					'arquivo.nome': $('#lancamentoRapidoArquivoNome').val(),
					'arquivo.contentType': $('#lancamentoRapidoArquivoContentType').val(),
					'arquivo.size': $('#lancamentoRapidoArquivoSize').val()
			},
			success: function(data) {
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('close', function(e, arquivo) {
					
					if($('#lancamentoRapidoArquivoNome').val() != arquivo.nome || $('#lancamentoRapidoArquivoContentType').val() != arquivo.contentType || $('#lancamentoRapidoArquivoSize').val() != arquivo.size) {
						if(arquivo.nome) {
							msgSuccess(i18n[i18n.locale]['arquivo.adicionado.success'].template({"0": arquivo.nome}));
						} else {
							msgSuccess(i18n[i18n.locale]['arquivo.removido.success'].template({"0": $('#lancamentoRapidoArquivoNome').val()}));
						}
					}
					
					$('#lancamentoRapidoArquivoNome').val(arquivo.nome);
					$('#lancamentoRapidoArquivoContentType').val(arquivo.contentType);
					$('#lancamentoRapidoArquivoSize').val(arquivo.size);
					
					lightDarkBtLancamentoRapidoArquivo();
					
				});
			}
		}).always(function() {
			loading(false);
		});
		
	});
	
	$.get(contextPath + '/viewid', function(viewid) {
		$('#lancamentoRapidoArquivoDocTemp').val('lancamentoRapidoDoc_' + viewid);	
		$('#lancamentoRapidoArquivoTemp').val('lancamentoRapido_' + viewid);
	});
	
	
	
	
	
	// Salvar Lancamento Rapido
	$('#frmLancamentoRapido').submit(function(e) {
		
		if(e.originalEvent) {
			$('#lancamentoIsBaixa').val(false);
		}
		
		loading(true);
		
		$.ajax({
			url: $(this).attr('action'),
			type: $(this).attr('method'),
			data: $(this).serialize(),
			dataType: 'jsonp'
		}).always(function() {
			loading(true);
		});
		
		return false;
	});
	
	$('#lancamentoRapidoBtCancelar').click(cancelarLancamentoRapido);
	
	$('#lancamentoRapidoBtBaixa').click(function() {
		$('#lancamentoIsBaixa').val(true);
		$('#frmLancamentoRapido').submit();
	});
	
	/**
	 * Setar confirmacao ao alterar Nº Documento
	 */
	$('#lancamentoRapidoNumeroDoc').change(function() {
		$('#frmLancamentoRapido').setHidden('isConfirmNumeroDocExists', false);
	});
		
	// -- FIM Lancamento Rapido --
	
	
	// Contas
	setChangeContaSaldo();
	
	
	// Cartao de Credito
	setChangeCartaoCreditoLimite();
		
	
	// Formulario
	$('#frmResumo').formSearch({
		multipleSelectRows: false,
		selectRow: true
	}).on('click', 'tbody tr', function(e) {
		
		
		
		var ids = {};
		
		$row = $(e.currentTarget);
		
		$td = $(e.target);
		
		// Verifica se clicou no Favorecido
		if($td.data('favorecido')) {
			ids['favorecido'] = $td.data('favorecido');
		}
		
		// Verifica se clicou na Conta
		if($td.data('conta')) {
			ids['conta'] = $td.data('conta');
		}
		
		// Verifica se clicou no Cartao de Credito
		if($td.data('cartaoCredito')) {
			ids['cartaoCredito'] = $td.data('cartaoCredito');
		}
		
		// Verifica se clicou no Cheque
		
				
		// Verifica se eh lancamento
		if($row.data('lancamentoParcela')) {
			ids['lancamentoParcela'] = $row.data('lancamentoParcela');
			ids['lancamento'] = $row.data('lancamento');
			ids['tipo'] = $row.data('tipo');
		}
		
		
		// Verifica se eh Pagamento / Recebimento
		if($row.data('baixaItem')) {
			ids['baixaItem'] = $row.data('baixaItem');
			ids['baixa'] = $row.data('baixa');
			ids['tipo'] = $row.data('tipo');
		}
		
		
		// Verifica se eh Transferencia
		if($row.data('transferencia')) {
			ids['transferencia'] = $row.data('transferencia');
		}
		
		
		// Verifica se eh Compensacao
		if($row.data('compensacaoItem')) {
			ids['compensacaoItem'] = $row.data('compensacaoItem');
			ids['compensacao'] = $row.data('compensacao');
		}
		
		relatorioDataOpenDialogLink(ids);
		
	});
	
	
	$('input[name=periodoData]').change(function(){
		$('#frmResumo').submit();
	});
	
	
	/**
	 * Recarregar combo de Cartao de Credito ao fechar janela de lista Cartao de Credito
	 */
	$(document).on('hidden.bs.modal', '#dialogContaCadastro', function() {
		loadContaSaldo();
		$('#frmResumo').submit();
	});
	
	/**
	 * Recarregar combo de Cartao de Credito ao fechar janela de lista Cartao de Credito
	 */
	$(document).on('hidden.bs.modal', '#dialogCartaoCreditoCadastro', function() {
		loadCartaoCreditoLimite();
		$('#frmResumo').submit();
	});
	
	/**
	 * Atualizar nome do Favorecido
	 */
	$(document).on('success', '#dialogPessoaCadastro', function(e, pessoa) {
		$('#frmResumo').submit();
	});
	
	/**
	 * Atualizar Lancamento
	 */
	$(document).on('success', '#dialogLancamentoCadastro', function(e, pessoa) {
		$('#frmResumo').submit();
	});
	
	/**
	 * Atualizar Baixa
	 */
	$(document).on('success', '#dialogBaixaCadastro', function(e, pessoa) {
		$('#frmResumo').submit();
	});
	
	/**
	 * Atualizar Transferencia
	 */
	$(document).on('success', '#dialogTransferenciaCadastro', function(e, pessoa) {
		$('#frmResumo').submit();
	});
	
	/**
	 * Atualizar Conciliacao
	 */
	$(document).on('success', '#dialogCompensacaoCadastro', function(e, pessoa) {
		$('#frmResumo').submit();
	});
	
	$(document).on('hidden.bs.modal', '.modal', function() {
		$('#frmResumo tr.active').removeClass('active');
	});
	
	$(document).on('mouseenter', '#cal-slide-content ul li', function() {
		$(this).addClass('agenda-item-hover');
	});
	
	$(document).on('mouseleave', '#cal-slide-content ul li', function() {
		$(this).removeClass('agenda-item-hover');
	});
	
	$(document).on('mouseup', '#cal-slide-content ul li', function(e) {
		if($(this).find('a').data('eventId')) {
			$('.agenda-item-selected').removeClass('agenda-item-selected');
			$(this).addClass('agenda-item-selected');
			$('.bt-edit, .bt-delete', '#pnAgenda').prop('disabled', false).data('agenda', $(this).find('a').data('eventId'));
		} else {
			
			if($(e.toElement).is('li')) {
				var fn = $(this).find('a').attr('href').replace('javascript:', '');
				eval(fn);
			}
		}
	});
	
	$('#isCCred').bootstrapToggle({
		on: i18n.get('MeioPagamento.CCR'),
		off: i18n.get('DocTipo.FT')
    }).change(function() {
    	$('#frmResumo').setHidden('isCCred', $(this).prop('checked')).submit();
    	msgSuccess($(this).prop('checked') ? i18n.get('exibirMovsCCred') : i18n.get('exibirFaturas'));
    });
	
	var paramsCalendar = $('#usuarios :selected').map(function() {
		return $(this).parent().attr('name') + '=' + $(this).val();
	}).get().join('&');
	
	// Calendar
	agenda = $('#agenda').calendar({
		language: 'pt-BR',
		events_source: contextPath + '/agenda/list?' + paramsCalendar,
		tmpl_path: contextPath + '/resources/tmpls/',
		onAfterViewLoad: function(view) {
			$('#lbAgendaAnoMes').html(this.getTitle());
			$('.btn-group button', '#pnAgenda').removeClass('active');
			$('button[data-calendar-view="' + view + '"]', '#pnAgenda').addClass('active');
		}
    });
	
	$('.btn-group button[data-calendar-nav]', '#pnAgenda').each(function() {
		var $this = $(this);
		$this.click(function() {
			agenda.navigate($this.data('calendar-nav'));
		});
	});

	$('.btn-group button[data-calendar-view]', '#pnAgenda').each(function() {
		var $this = $(this);
		$this.click(function() {
			agenda.view($this.data('calendar-view'));
		});
	});
	
	
	$('#usuarios').change(function() {
		
		var paramsCalendar = $('#usuarios :selected').map(function() {
			return $(this).parent().attr('name') + '=' + $(this).val();
		}).get().join('&');
		agenda.setOptions({events_source: contextPath + '/agenda/list?' + paramsCalendar});
		agenda.view();
	
	}).multiselect({
		buttonWidth: '100%',
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___')
	});
	
	$('.bt-new', '#pnAgenda').click(function() {
		loading(true);
		
		var params = $('#usuarios :selected').map(function() {
			return $(this).parent().attr('name') + '=' + $(this).val();
		}).get().join('&');
		
		$.ajax({
			url: contextPath + '/agenda/cadastro?' + params,
			success: function(data) {
				
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('success', function() {
					agenda.view();
					msgSuccess(i18n.get('evento.salvo.success'));
				}).on('hidden.bs.modal', function() {
					$('.bt-edit, .bt-delete', '#pnAgenda').prop('disabled', true).removeData('agenda');
				});
				
			}
		}).always(function() {
			loading(false);
		});
		
	});
	
	$('.bt-edit', '#pnAgenda').click(function() {
		loading(true);
		
		var params = $('#usuarios :selected').map(function() {
			return $(this).parent().attr('name') + '=' + $(this).val();
		}).get().join('&');
		
		$.ajax({
			url: contextPath + '/agenda/cadastro?' + params,
			data: {id: $(this).data('agenda')},
			success: function(data) {
				
				var $dialog = $(data);
				$('body').append($dialog);
				$dialog.on('success', function() {
					agenda.view();
					msgSuccess(i18n.get('evento.salvo.success'));
				}).on('hidden.bs.modal', function() {
					$('.bt-edit, .bt-delete', '#pnAgenda').prop('disabled', true).removeData('agenda');
				});
				
			}
		}).always(function() {
			loading(false);
		});
		
	});
	
	
	$('.bt-delete', '#pnAgenda').click(function() {
		var id = $(this).data('agenda');
		msgConfirm(i18n.get('evento.confirm.delete'), function() {
			loading(true);
			$.ajax({
				url: contextPath + '/agenda/excluir',
				method: 'POST',
				data: {id: id},
				success: function() {
					agenda.view();
					msgSuccess(i18n.get('evento.delete.success'));
					$('.bt-edit, .bt-delete', '#pnAgenda').prop('disabled', true).removeData('agenda');
				}
			}).always(function() {
				loading(false);
			});
			
		});
		
	});
	
});


/**
 * Conta
 */
function enableDisableBtLancamentoRapidoConta() {
	if(
	$('#lancamentoRapidoMeio').val() != 'CCR' && $('#lancamentoRapidoMeio').val() != 'CHQ' 
	|| 
		(
				($('#lancamentoRapidoMeio').val() == 'CHQ' || $('#lancamentoRapidoMeio').val() == 'CCR') 
				&& $('#lancamentoTipo').val() == 'REC'
		)
	) {
		$('#btLancamentoRapidoConta').prop('disabled', false);
	} else {
		$('#btLancamentoRapidoConta').prop('disabled', true);
	}
	lightDarkBtLancamentoRapidoConta();
}

function lightDarkBtLancamentoRapidoConta() {
	if($('#lancamentoRapidoConta').val() && !$('#btLancamentoRapidoConta').prop('disabled')) {
		$('#btLancamentoRapidoConta').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoRapidoConta').removeClass('btn-primary').addClass('btn-default');
	}
}



/**
 * Cartao de Credito
 */
function enableDisableBtLancamentoRapidoCartaoCredito() {
	if($('#lancamentoTipo').val() == 'DES' && $('#lancamentoRapidoMeio').val() == 'CCR') {
		$('#btLancamentoRapidoCartaoCredito').prop('disabled', false);
	} else {
		$('#btLancamentoRapidoCartaoCredito').prop('disabled', true);
	}
	lightDarkBtLancamentoRapidoCartaoCredito();
}


function lightDarkBtLancamentoRapidoCartaoCredito() {
	if($('#lancamentoRapidoCartaoCredito').val() && !$('#btLancamentoRapidoCartaoCredito').prop('disabled')) {
		$('#btLancamentoRapidoCartaoCredito').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoRapidoCartaoCredito').removeClass('btn-primary').addClass('btn-default');
	}
}



/**
 * Cheque 
 */
function enableDisableBtLancamentoRapidoCheque() {
	if($('#lancamentoRapidoMeio').val() == 'CHQ') {
		$('#btLancamentoRapidoCheque').prop('disabled', false);
	} else {
		$('#btLancamentoRapidoCheque').prop('disabled', true);
	}
	lightDarkBtLancamentoRapidoCheque();
}

function lightDarkBtLancamentoRapidoCheque() {
	
	var isCheque = false;
	if($('#lancamentoTipo').val() == 'DES') {
		var cheque = $('#frmLancamentoRapido').data('chequedes');
		if(typeof cheque != 'undefined' && typeof cheque.talaoCheque != 'undefined' && cheque.numero) {
			isCheque = true;
		}
	}
	if($('#lancamentoTipo').val() == 'REC') {
		var cheque = $('#frmLancamentoRapido').data('chequerec');
		if(typeof cheque != 'undefined' && typeof cheque.numero != 'undefined' && cheque.numero) {
			isCheque = true;
		}
	}
	
	if(isCheque && !$('#btLancamentoRapidoCheque').prop('disabled')) {
		$('#btLancamentoRapidoCheque').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoRapidoCheque').removeClass('btn-primary').addClass('btn-default');
	}
}



/**
 * Boleto
 */
function enableDisableBtLancamentoRapidoBoleto() {
	$('#btLancamentoRapidoBoleto').prop('disabled', $('#lancamentoTipo').val() == 'REC');
	lightDarkBtLancamentoRapidoBoleto();
}

function lightDarkBtLancamentoRapidoBoleto() {
	if($('#lancamentoRapidoBoletoNumero').val() && !$('#btLancamentoRapidoBoleto').prop('disabled')) {
		$('#btLancamentoRapidoBoleto').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoRapidoBoleto').removeClass('btn-primary').addClass('btn-default');
	}
}


/**
 * Arquivo
 */
function lightDarkBtLancamentoRapidoArquivoDoc() {
	if($('#lancamentoRapidoArquivoDocNome').val() && !$('#btLancamentoRapidoDocArquivo').prop('disabled')) {
		$('#btLancamentoRapidoArquivoDoc').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoRapidoArquivoDoc').removeClass('btn-primary').addClass('btn-default');
	}
}
function lightDarkBtLancamentoRapidoArquivo() {
	if($('#lancamentoRapidoArquivoNome').val() && !$('#btLancamentoRapidoArquivo').prop('disabled')) {
		$('#btLancamentoRapidoArquivo').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btLancamentoRapidoArquivo').removeClass('btn-primary').addClass('btn-default');
	}
}



/**
 * Erro Validacao Cadastro
 * @param errors
 */
function lancamentoRapidoError(errors) {
	error(errors, $('#frmLancamentoRapido'));
}


/**
 * Sucesso Cadastro
 * @param lancamento
 */
function lancamentoRapidoSuccess(lancamento) { 
	resetFormValidator($('#frmLancamentoRapido'));
	msgSuccess(i18n.get('lancamentoRapido.realizado.success'));
	$('#frmResumo').submit();
	cancelarLancamentoRapido();
	contaSaldo();
	cartaoCreditoLimite();
}


/**
 * Cancelar Lancamento Rapido
 * @param link
 */
function cancelarLancamentoRapido() {
	$('#lancamentoRapidoVenc').val('');
	$('#lancamentoRapidoFavorecido').trigger('autoComplete.clean');
	$('#lancamentoRapidoTipoDoc, #lancamentoRapidoNumeroDoc').val('');
	$('#lancamentoDes').trigger('autoComplete.clean');
	$('#lancamentoRec').trigger('autoComplete.clean');
	$('#lancamentoRapidoMeio').val('').change();
	$('#lancamentoRapidoValor, #lancamentoRapidoConta, #lancamentoRapidoCartaoCredito, #lancamentoRapidoCartaoCreditoIsLimite').val('');
	$('#lancamentoIsBaixa').val(false);
	$('#frmLancamentoRapido').setHidden('isConfirmNumeroDocExists', false);
	$('input[name^="lancamento"]', '#frmLancamentoRapido').val('');
	$.get(contextPath + '/viewid', function(viewid) {
		$('#lancamentoRapidoArquivoTemp').val('lancamentoRapido_' + viewid);	
		$('#lancamentoRapidoArquivoDocTemp').val('lancamentoRapidoDoc_' + viewid);
	});
	lightDarkBtLancamentoRapidoArquivo();
	lightDarkBtLancamentoRapidoArquivoDoc();
}


function relatorioDataOpenLink(link) {
	loading(true);
	$.ajax({
		url: link.url,
		data: link.data,
		success: function(result) {
			if(typeof result == 'string') {
				$('body').append(result);
			} else {
				error(result, $('#frmRelatorioData'));
			}
		}
	}).always(function() {
		loading(false);
	});
}


function relatorioDataOpenDialogLink(ids) {
	
	var links = [];
	
	// Verifica se clicou no Favorecido
	if(ids.favorecido) {
		links.push({
			name: i18n.get('pessoa'), 
			url: '/pessoa/cadastro', 
			data: {
				id: ids.favorecido
			}
		});
	}
	
	// Verifica se clicou na Conta
	if(ids.conta) {
		links.push({
			name: i18n.get('conta'), 
			url: '/conta/cadastro', 
			data: {
				id: ids.conta
			}
		});
	}
	
	// Verifica se clicou no Cartao de Credito
	if(ids.cartaoCredito) {
		links.push({
			name: i18n.get('cartaoCredito'), 
			url: '/cartaoCredito/cadastro', 
			data: {
				id: ids.cartaoCredito
			}
		});
	}
	
	// Verifica se clicou no Cheque
	
			
	// Verifica se eh lancamento
	if(ids.lancamentoParcela) {
		links.push({
			name: i18n.get('lancamento'), 
			url: '/lancamento/cadastro', 
			data: {
				id: ids.lancamento,
				lancamentoParcela: ids.lancamentoParcela,
				tipo: ids.tipo
			}
		});
	}
	
	
	// Verifica se eh Pagamento / Recebimento
	if(ids.baixaItem) {
		links.push({
			name: i18n.get(ids.tipo == 'DES' ? 'pagamento' : 'recebimento'), 
			url: '/baixa/cadastro', 
			data: {
				id: ids.baixa,
				baixaItem: ids.baixaItem,
				tipo: ids.tipo
			}
		});
	}
	
	
	// Verifica se eh Transferencia
	if(ids.transferencia) {
		links.push({
			name: i18n.get('transferencia'), 
			url: '/transferencia/cadastro', 
			data: {
				id: ids.transferencia
			}
		});
	}
	
	
	// Verifica se eh Compensacao
	if(ids.compensacaoItem) {
		links.push({
			name: i18n.get('conciliacao'), 
			url: '/compensacao/cadastro', 
			data: {
				id: ids.compensacao,
				compensacaoItem: ids.compensacaoItem
			}
		});
	}
	
	if(links.length == 1) {
		relatorioDataOpenLink(links[0]);
	} else {
		var html = '<div class="list-group">';
		$.each(links, function(i, link) {
			html += '<a href="javascript:;" class="list-group-item" data-link-index="' + i + '">' + link.name + '</a>'; 
		});
		html += '</div>'; 
		msgAlert({title: i18n.get('irPara'), message: html, cancel: true});
		
		$.each(links, function(i, link) {
			$('a.list-group-item[data-link-index="' + i + '"]').click(function() {
				relatorioDataOpenLink(link);
			});
		});
	}
	
}



function setChangeContaSaldo() {
	$('#contas').change(contaSaldo).multiselect({
		buttonWidth: '100%',
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___')
	});
}

function contaSaldo() {
	loading(true);
	$.ajax({
		url: contextPath + '/admin/contaSaldo',
		data: $('#contas').serialize(),
		success: function(result) {
			if(typeof result.saldo == 'undefined') {
				
				$('#layContaSaldo').hide();
				$('#laySelecioneConta').show();
				
			} else {
				
				$('#contaSaldo').text(result.saldo.formatDecimal()).removeClass('font-red font-blue').addClass(result.saldo < 0 ? 'font-red' : 'font-blue');
				$('#contaSaldoLimiteDisp').text(result.saldolimitedisp.formatDecimal()).removeClass('font-red font-blue').addClass(result.saldolimitedisp < 0 ? 'font-red' : 'font-blue');
				
				$('#layContaSaldo').show();
				$('#laySelecioneConta').hide();
				
			}
		}
	}).always(function() {
		loading(false);
	});
}

function loadContaSaldo() {
	loading(true);
	$.ajax({
		url: contextPath + '/admin/loadContaSaldo',
		data: $('#contas').serialize(),
		success: function(result) {
			$('#layContas').replaceWith(result);
			setChangeContaSaldo();
			if(!$('#contas :selected').length) {
				$('#layContaSaldo').hide();
				$('#laySelecioneConta').show();
			}
		}
	}).always(function() {
		loading(false);
	});
}


function setChangeCartaoCreditoLimite() {
	$('#cartoesCredito').change(cartaoCreditoLimite).multiselect({
		buttonWidth: '100%',
		nonSelectedText: i18n.get('selecione___'),
		nSelectedText: i18n.get('selecionados'),
		allSelectedText: i18n.get('todos___')
	});
}

function cartaoCreditoLimite() {
	loading(true);
	$.ajax({
		url: contextPath + '/admin/cartaoCreditoLimite',
		data: $('#cartoesCredito').serialize(),
		success: function(result) {
			if(typeof result.limiteutilizado == 'undefined') {
				$('#layCartaoCreditoLimite').hide();
				$('#laySelecioneCartaoCredito').show();
			} else {
				
				$('#layCartaoCreditoLimite').show();
				$('#laySelecioneCartaoCredito').hide();
				
				$('#cartaoCreditoLimiteUtilizado').text(result.limiteutilizado.formatDecimal()).removeClass('font-red font-blue').addClass(result.limiteutilizado > 0 ? 'font-red' : 'font-blue');
				$('#cartaoCreditoLimiteDisponivel').text(result.limitedisponivel.formatDecimal()).removeClass('font-red font-blue').addClass(result.limitedisponivel > 0 ? 'font-blue' : 'font-red');
			}
		}
	}).always(function() {
		loading(false);
	});	
}

function loadCartaoCreditoLimite() {
	loading(true);
	$.ajax({
		url: contextPath + '/admin/loadCartaoCreditoLimite',
		data: $('#cartoesCredito').serialize(),
		success: function(result) {
			$('#layCartaoCredito').replaceWith(result);
			setChangeCartaoCreditoLimite();
			if(!$('#cartoesCredito :selected').length) {
				$('#layCartaoCreditoLimite').hide();
				$('#laySelecioneCartaoCredito').show();
			}
		}
	}).always(function() {
		loading(false);
	});
}

function selecionarEvento(id) {
	$('.bt-edit, .bt-delete', '#pnAgenda').prop('disabled', false).data('agenda', id);
}