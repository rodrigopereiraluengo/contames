$('#dialogSelectCartaoCredito').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).remove();
});

$('#dialogSelectCartaoCreditoNome').comboEditable().toFocus().change(function() {
	
	if($('#dialogSelectCartaoCreditoNome :selected').data('limite')) {
		
		$('#btDialogSelectCartaoCreditoIsLimite').prop('disabled', false);
		
		var data = $('#dialogSelectCartaoCreditoData').val().blank() ? null : moment($('#dialogSelectCartaoCreditoData').val(), i18n.get('momPatternDate'));
		
		var dataFatuAnt = $('#dialogSelectCartaoCreditoNome :selected').data('dataFatuAnt').blank() ? null : moment($('#dialogSelectCartaoCreditoNome :selected').data('dataFatuAnt'), i18n.get('momPatternDate'));
		
		if(dataFatuAnt != null && $('#dialogSelectCartaoCreditoNome :selected').data('diasPagar')) {
			dataFatuAnt = dataFatuAnt.subtract($('#dialogSelectCartaoCreditoNome :selected').data('diasPagar'), 'days').add(1, 'month');
		}
		
		if(typeof $('#dialogSelectCartaoCreditoNome :selected').data('isLimite') == 'undefined') {
			
			if(data != null && dataFatuAnt != null && !$('#dialogSelectCartaoCreditoIsLimite').val()) {
				$('#dialogSelectCartaoCreditoIsLimite').val(!data.isBefore(dataFatuAnt));
			}
			
		} else {
			$('#dialogSelectCartaoCreditoIsLimite').val($('#dialogSelectCartaoCreditoNome :selected').data('isLimite'));
		}
				
	} else {
		$('#btDialogSelectCartaoCreditoIsLimite').prop('disabled', true);
		$('#dialogSelectCartaoCreditoIsLimite').val(false);
	}
	
	lightDarkBtDialogSelectCartaoCreditoIsLimite();

}).on('setValue', function(){
	$(this).change();
}).change();

$('#btDialogSelectCartaoCreditoIsLimite').click(function() {
	
	var data = $('#dialogSelectCartaoCreditoData').val().blank() ? null : moment($('#dialogSelectCartaoCreditoData').val(), i18n.get('momPatternDate'));
	
	var dataFatuAnt = $('#dialogSelectCartaoCreditoNome :selected').data('dataFatuAnt').blank() ? null : moment($('#dialogSelectCartaoCreditoNome :selected').data('dataFatuAnt'), i18n.get('momPatternDate'));
	
	if(dataFatuAnt != null && $('#dialogSelectCartaoCreditoNome :selected').data('diasPagar')) {
		dataFatuAnt = dataFatuAnt.subtract($('#dialogSelectCartaoCreditoNome :selected').data('diasPagar'), 'days').add(1, 'month');
	}
	
	if(data == null || dataFatuAnt == null) {
		
		if($('#dialogSelectCartaoCreditoIsLimite').val().toBoolean()) {
			
			msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.no']);
			$('#dialogSelectCartaoCreditoIsLimite').val(false);
			$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', false);
		
		} else {
			
			msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.yes']);
			$('#dialogSelectCartaoCreditoIsLimite').val(true);
			$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', true);
		
		}
		
		lightDarkBtDialogSelectCartaoCreditoIsLimite();
		
	} else {
		
		if(data.isBefore(dataFatuAnt) && !$('#dialogSelectCartaoCreditoIsLimite').val().toBoolean()) {
			msgConfirm(i18n.get('confirm.data.mov.before.dataFatuAnt'), function() {
				
				msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.yes']);
				$('#dialogSelectCartaoCreditoIsLimite').val(true);
				$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', true);
				lightDarkBtDialogSelectCartaoCreditoIsLimite();
			});
			
		} else if(data.isAfter(dataFatuAnt) && $('#dialogSelectCartaoCreditoIsLimite').val().toBoolean()) {
			
			msgConfirm(i18n.get('confirm.data.mov.after.dataFatuAnt'), function() {
				
				msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.no']);
				$('#dialogSelectCartaoCreditoIsLimite').val(false);
				$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', false);
				lightDarkBtDialogSelectCartaoCreditoIsLimite();
			});
			
		} else {
			
			if($('#dialogSelectCartaoCreditoIsLimite').val().toBoolean()) {
				
				msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.no']);
				$('#dialogSelectCartaoCreditoIsLimite').val(false);
				$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', false);
			
			} else {
				
				msgSuccess(i18n[i18n.locale]['cartaoCredito.isLimite.yes']);
				$('#dialogSelectCartaoCreditoIsLimite').val(true);
				$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', true);
			
			}
			
			lightDarkBtDialogSelectCartaoCreditoIsLimite();
			
		}
		
	}
	
	
});
try {
$('#dialogSelectCartaoCreditoNome :selected').data('isLimite', $('#dialogSelectCartaoCreditoIsLimite').val().toBoolean());
}catch(e){}

function lightDarkBtDialogSelectCartaoCreditoIsLimite() {
	if($('#dialogSelectCartaoCreditoIsLimite').val().toBoolean() && !$('#btDialogSelectCartaoCreditoIsLimite').prop('disabled')) {
		$('#btDialogSelectCartaoCreditoIsLimite').removeClass('btn-default').addClass('btn-primary');
	} else {
		$('#btDialogSelectCartaoCreditoIsLimite').removeClass('btn-primary').addClass('btn-default');
	}
}

try{lightDarkBtDialogSelectCartaoCreditoIsLimite();} catch(e) { }

$('#frmDialogCartaoCredito').submit(function() {
	$.ajax({
		url: contextPath + '/cartaoCredito/dialogSelect',
		type: 'POST',
		data: $(this).serialize(),
		dataType: 'jsonp'
	});
	return false;
});

function dialogSelectCartaoCreditoSuccess(cartaoCredito) {
	
	$('#dialogSelectCartaoCredito').trigger('close', [cartaoCredito]).modal('hide');
	
}