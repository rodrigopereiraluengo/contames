CREATE OR REPLACE FUNCTION fn_cartaocredito_transferencia() RETURNS TRIGGER AS 
$$ 
BEGIN 

IF (TG_OP = 'INSERT') THEN

	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.statusOrigem IN('LAN', 'COM') THEN

		IF COALESCE(NEW.isSaldoSaque, false) THEN
			UPDATE CartaoCredito SET limiteSaqueUtilizado = COALESCE(limiteSaqueUtilizado, 0) + NEW.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limiteSaque, 0) > 0;
		ELSE
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;		
		END IF;

	END IF;

ELSIF (TG_OP = 'UPDATE') THEN

	IF OLD.cartaoCredito_id IS NOT NULL AND NEW.statusOrigem IN('LAN', 'COM') THEN

		IF COALESCE(OLD.isSaldoSaque, false) THEN
			UPDATE CartaoCredito SET limiteSaqueUtilizado = COALESCE(limiteSaqueUtilizado, 0) - OLD.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id;
		ELSE
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id;
		END IF;
		
	END IF;

	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.statusOrigem IN('LAN', 'COM') THEN
		
		IF COALESCE(NEW.isSaldoSaque, false) THEN
			UPDATE CartaoCredito SET limiteSaqueUtilizado = COALESCE(limiteSaqueUtilizado, 0) + NEW.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limiteSaque, 0) > 0;
		ELSE
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;		
		END IF;
		
	END IF;

ELSIF (TG_OP = 'DELETE') THEN
	
	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.statusOrigem IN('LAN', 'COM') THEN
		IF COALESCE(OLD.isSaldoSaque, false) THEN
			UPDATE CartaoCredito SET limiteSaqueUtilizado = COALESCE(limiteSaqueUtilizado, 0) - OLD.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id;
		ELSE
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id;
		END IF;
	END IF;

END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;


CREATE TRIGGER tg_cartaocredito_transferencia
	AFTER INSERT OR UPDATE OR DELETE ON transferencia
	FOR EACH ROW
	EXECUTE PROCEDURE fn_cartaocredito_transferencia();