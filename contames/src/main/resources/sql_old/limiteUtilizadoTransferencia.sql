CREATE OR REPLACE FUNCTION limiteUtilizadoTransferencia(bigint) RETURNS double precision AS 
$$
DECLARE total double precision;
DECLARE transferencia_id bigint;
BEGIN

	SELECT COALESCE(SUM(t.valor), 0) INTO total
	FROM Transferencia t
	WHERE t.status 'LAN'
	AND t.cartaoCredito_id = $1;

	RETURN total;
END;
$$
LANGUAGE plpgsql;