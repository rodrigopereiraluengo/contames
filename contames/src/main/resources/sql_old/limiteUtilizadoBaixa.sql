CREATE OR REPLACE FUNCTION limiteUtilizadoBaixa(bigint) RETURNS double precision AS 
$$
DECLARE total double precision;
DECLARE _baixaItem_id bigint;
BEGIN
	
	SELECT COALESCE(SUM(bi.valor), 0) INTO total
	FROM BaixaItem bi
	WHERE bi.status = 'BXD'
	AND bi.cartaoCredito_id = $1;

	RETURN total;
END;
$$
LANGUAGE plpgsql;