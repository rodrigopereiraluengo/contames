CREATE OR REPLACE FUNCTION fn_cartaocredito_baixaitem() RETURNS TRIGGER AS 
$$ 

BEGIN 

IF (TG_OP = 'INSERT') THEN

	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.status = 'BXD' THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;
	
ELSIF (TG_OP = 'UPDATE') THEN
	
	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.status = 'BXD' THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;

	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.status = 'BXD' THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;
	
ELSIF (TG_OP = 'DELETE') THEN
	
	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.status = 'BXD' THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor, limiteUtilizadoPagar = COALESCE(limiteUtilizadoPagar, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;
		
END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;


CREATE TRIGGER tg_cartaocredito_baixaitem
	AFTER INSERT OR UPDATE OR DELETE ON baixaitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_cartaocredito_baixaitem();