CREATE OR REPLACE FUNCTION limiteUtilizadoLancamento(bigint) RETURNS double precision AS 
$$
DECLARE total double precision;
DECLARE _lancamentoParcela_id bigint;
BEGIN

	SELECT COALESCE(SUM(lp.valor), 0) INTO total
	FROM LancamentoParcela lp
	WHERE lp.status = 'LAN'
	AND lp.cartaoCredito_id = $1;

	RETURN total;
END;
$$
LANGUAGE plpgsql;