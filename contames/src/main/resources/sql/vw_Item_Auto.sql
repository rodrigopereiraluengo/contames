DROP VIEW IF EXISTS vw_item_auto;

CREATE OR REPLACE VIEW vw_item_auto AS 
SELECT 
	i.cliente_id,
	NULL::bigint AS lancamentoparcela_id,
	NULL::bigint lancamento_id,
	(fn_data_dia_util(i.diaVencimento, date_part('MONTH', CURRENT_TIMESTAMP)::int, date_part('YEAR', CURRENT_TIMESTAMP)::int) + '1 month'::interval * mes_sum)::date AS _data,
	i.favorecido_id,
	COALESCE(p.fantasia, p.razaosocial, p.apelido, p.nome) AS favorecido,
	NULL::text tipodoc,
	NULL::text numerodoc,
	NULL::integer numerodoc_clean,
	i.meio,
	i.conta_id,
	i.cartaocredito_id,
	NULL::bigint cheque_id,
	NULL::bigint cheque_numero,
	NULL::bigint talaocheque_id,
	COALESCE(COALESCE(c.nome, cc.nome)) AS meio_conta,
	' - '::text parcela,
	'STD'::text status,
	i.valor,
	i.tipo,
	i.criacao,
	i.usucriacao_id,
	COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
	i.alteracao,
	i.usualteracao_id,
	COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao,
	NULL::bigint arquivo_id,
	NULL::text arquivo_nome,
	NULL::bigint arquivoDoc_id,
	NULL::text arquivoDoc_nome,
	NULL::bigint fatCartaoCredito_id,
	NULL::text fatCartaoCredito_nome,
	NULL::text boletotipo,
	NULL::text boletonumero
FROM Item i
INNER JOIN generate_series(0, 12, 1) AS s(mes_sum) ON 1 = 1
INNER JOIN Pessoa p ON i.favorecido_id = p.id
LEFT JOIN Conta c ON i.conta_id = c.id
LEFT JOIN CartaoCredito cc ON i.cartaoCredito_id = cc.id
LEFT JOIN Pessoa _usucriacao ON i.usuCriacao_id = _usucriacao.id
LEFT JOIN Pessoa _usualteracao ON i.usuAlteracao_id = _usualteracao.id
WHERE i.isLancAuto = true