ALTER TABLE Agenda DROP CONSTRAINT fk_agenda_cliente_id;
ALTER TABLE Agenda ADD CONSTRAINT fk_agenda_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_cliente_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_item_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_item_id FOREIGN KEY (item_id) REFERENCES item (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_pessoa_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_pessoa_id FOREIGN KEY (pessoa_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_conta_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_conta_id FOREIGN KEY (conta_id) REFERENCES conta (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_cartaocredito_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_cartaocredito_id FOREIGN KEY (cartaocredito_id) REFERENCES cartaocredito (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_talaocheque_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_talaocheque_id FOREIGN KEY (talaocheque_id) REFERENCES talaocheque (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_lancamento_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_lancamento_id FOREIGN KEY (lancamento_id) REFERENCES lancamento (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_lancamentoparcela_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_lancamentoparcela_id FOREIGN KEY (lancamentoparcela_id) REFERENCES lancamentoparcela (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_lancamentorateio_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_lancamentorateio_id FOREIGN KEY (lancamentorateio_id) REFERENCES lancamentorateio (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_baixa_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_baixa_id FOREIGN KEY (baixa_id) REFERENCES baixa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_baixaitem_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_baixaitem_id FOREIGN KEY (baixaitem_id) REFERENCES baixaitem (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_transferencia_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_transferencia_id FOREIGN KEY (transferencia_id) REFERENCES transferencia (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_compensacao_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_compensacao_id FOREIGN KEY (compensacao_id) REFERENCES compensacao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_compensacaoitem_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_compensacaoitem_id FOREIGN KEY (compensacaoitem_id) REFERENCES compensacaoitem (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Assinatura DROP CONSTRAINT fk_assinatura_cliente_id;
ALTER TABLE Assinatura ADD CONSTRAINT fk_assinatura_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE AssinaturaPagamento DROP CONSTRAINT fk_assinaturapagamento_assinatura_id;
ALTER TABLE AssinaturaPagamento ADD CONSTRAINT fk_assinaturapagamento_assinatura_id FOREIGN KEY (assinatura_id) REFERENCES assinatura (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Baixa DROP CONSTRAINT fk_baixa_cliente_id;
ALTER TABLE Baixa ADD CONSTRAINT fk_baixa_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_arquivo_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_arquivo_id FOREIGN KEY (arquivo_id) REFERENCES arquivo (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_baixa_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_baixa_id FOREIGN KEY (baixa_id) REFERENCES baixa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_cartaocredito_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_cartaocredito_id FOREIGN KEY (cartaocredito_id) REFERENCES cartaocredito (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_cheque_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_cheque_id FOREIGN KEY (cheque_id) REFERENCES cheque (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_comprovante_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_comprovante_id FOREIGN KEY (comprovante_id) REFERENCES arquivo (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_conta_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_conta_id FOREIGN KEY (conta_id) REFERENCES conta (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE BaixaItem DROP CONSTRAINT fk_baixaitem_motivo_id;
ALTER TABLE BaixaItem ADD CONSTRAINT fk_baixaitem_motivo_id FOREIGN KEY (motivo_id) REFERENCES descricao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE CartaoCredito DROP CONSTRAINT fk_cartaocredito_cliente_id;
ALTER TABLE CartaoCredito ADD CONSTRAINT fk_cartaocredito_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_banco_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_banco_id FOREIGN KEY (banco_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_cliente_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_nome_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_nome_id FOREIGN KEY (nome_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_favorecido_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_favorecido_id FOREIGN KEY (nome_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_talaocheque_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_talaocheque_id FOREIGN KEY (talaocheque_id) REFERENCES talaocheque (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_motivo_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_motivo_id FOREIGN KEY (motivo_id) REFERENCES descricao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Classificacao DROP CONSTRAINT fk_classificacao_classificacao_id;
ALTER TABLE Classificacao ADD CONSTRAINT fk_classificacao_classificacao_id FOREIGN KEY (classificacao_id) REFERENCES classificacao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Classificacao DROP CONSTRAINT fk_classificacao_cliente_id;
ALTER TABLE Classificacao ADD CONSTRAINT fk_classificacao_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Compensacao DROP CONSTRAINT fk_compensacao_cliente_id;
ALTER TABLE Compensacao ADD CONSTRAINT fk_compensacao_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Compensacao DROP CONSTRAINT fk_compensacao_conta_id;
ALTER TABLE Compensacao ADD CONSTRAINT fk_compensacao_conta_id FOREIGN KEY (conta_id) REFERENCES conta (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE CompensacaoItem DROP CONSTRAINT fk_compensacaoitem_compensacao_id;
ALTER TABLE CompensacaoItem ADD CONSTRAINT fk_compensacaoitem_compensacao_id FOREIGN KEY (compensacao_id) REFERENCES compensacao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Conta DROP CONSTRAINT fk_conta_banco_id;
ALTER TABLE Conta ADD CONSTRAINT fk_conta_banco_id FOREIGN KEY (banco_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Conta DROP CONSTRAINT fk_conta_cliente_id;
ALTER TABLE Conta ADD CONSTRAINT fk_conta_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Conta DROP CONSTRAINT fk_conta_titular_id;
ALTER TABLE Conta ADD CONSTRAINT fk_conta_titular_id FOREIGN KEY (titular_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Conta DROP CONSTRAINT fk_conta_ultimacompensacao_id;
ALTER TABLE Conta ADD CONSTRAINT fk_conta_ultimacompensacao_id FOREIGN KEY (ultimacompensacao_id) REFERENCES compensacao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Descricao DROP CONSTRAINT fk_descricao_cliente_id;
ALTER TABLE Descricao ADD CONSTRAINT fk_descricao_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Item DROP CONSTRAINT fk_item_classificacao_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_classificacao_id FOREIGN KEY (classificacao_id) REFERENCES classificacao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Item DROP CONSTRAINT fk_item_cliente_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Item DROP CONSTRAINT fk_item_unidade_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_unidade_id FOREIGN KEY (unidade_id) REFERENCES descricao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Item DROP CONSTRAINT fk_item_conta_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_conta_id FOREIGN KEY (conta_id) REFERENCES conta (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Item DROP CONSTRAINT fk_item_cartaocredito_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_cartaocredito_id FOREIGN KEY (cartaocredito_id) REFERENCES cartaocredito (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Item DROP CONSTRAINT fk_item_favorecido_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_favorecido_id FOREIGN KEY (favorecido_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Lancamento DROP CONSTRAINT fk_lancamento_arquivodoc_id;
ALTER TABLE Lancamento ADD CONSTRAINT fk_lancamento_arquivodoc_id FOREIGN KEY (arquivodoc_id) REFERENCES arquivo (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Lancamento DROP CONSTRAINT fk_lancamento_cliente_id;
ALTER TABLE Lancamento ADD CONSTRAINT fk_lancamento_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE LancamentoItem DROP CONSTRAINT fk_lancamentoitem_lancamento_id;
ALTER TABLE LancamentoItem ADD CONSTRAINT fk_lancamentoitem_lancamento_id FOREIGN KEY (lancamento_id) REFERENCES lancamento (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE LancamentoFatura DROP CONSTRAINT fk_lancamentofatura_lancamento_id;
ALTER TABLE LancamentoFatura ADD CONSTRAINT fk_lancamentofatura_lancamento_id FOREIGN KEY (lancamento_id) REFERENCES lancamento (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE LancamentoParcela DROP CONSTRAINT fk_lancamentoparcela_arquivo_id;
ALTER TABLE LancamentoParcela ADD CONSTRAINT fk_lancamentoparcela_arquivo_id FOREIGN KEY (arquivo_id) REFERENCES arquivo (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE LancamentoParcela DROP CONSTRAINT fk_lancamentoparcela_cartaocredito_id;
ALTER TABLE LancamentoParcela ADD CONSTRAINT fk_lancamentoparcela_cartaocredito_id FOREIGN KEY (cartaocredito_id) REFERENCES cartaocredito (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;
  
ALTER TABLE LancamentoParcela DROP CONSTRAINT fk_lancamentoparcela_cheque_id;
ALTER TABLE LancamentoParcela ADD CONSTRAINT fk_lancamentoparcela_cheque_id FOREIGN KEY (cheque_id) REFERENCES cheque (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE LancamentoParcela DROP CONSTRAINT fk_lancamentoparcela_conta_id;
ALTER TABLE LancamentoParcela ADD CONSTRAINT fk_lancamentoparcela_conta_id FOREIGN KEY (conta_id) REFERENCES conta (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE LancamentoParcela DROP CONSTRAINT fk_lancamentoparcela_lancamento_id;
ALTER TABLE LancamentoParcela ADD CONSTRAINT fk_lancamentoparcela_lancamento_id FOREIGN KEY (lancamento_id) REFERENCES lancamento (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE LancamentoRateio DROP CONSTRAINT fk_lancamentorateio_lancamento_id;
ALTER TABLE LancamentoRateio ADD CONSTRAINT fk_lancamentorateio_lancamento_id FOREIGN KEY (lancamento_id) REFERENCES lancamento (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE LancamentoRateio DROP CONSTRAINT fk_lancamentorateio_pessoa_id;
ALTER TABLE LancamentoRateio ADD CONSTRAINT fk_lancamentorateio_pessoa_id FOREIGN KEY (pessoa_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;
  
ALTER TABLE LancamentoRateio DROP CONSTRAINT fk_lancamentorateio_rateio_id;
ALTER TABLE LancamentoRateio ADD CONSTRAINT fk_lancamentorateio_rateio_id FOREIGN KEY (rateio_id) REFERENCES descricao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Perfil DROP CONSTRAINT fk_perfil_cliente_id;
ALTER TABLE Perfil ADD CONSTRAINT fk_perfil_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Pessoa DROP CONSTRAINT fk_pessoa_categoria_id;
ALTER TABLE Pessoa ADD CONSTRAINT fk_pessoa_categoria_id FOREIGN KEY (categoria_id) REFERENCES descricao (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Pessoa DROP CONSTRAINT fk_pessoa_cliente_id;
ALTER TABLE Pessoa ADD CONSTRAINT fk_pessoa_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE TalaoCheque DROP CONSTRAINT fk_talaocheque_conta_id;
ALTER TABLE TalaoCheque ADD CONSTRAINT fk_talaocheque_conta_id FOREIGN KEY (conta_id) REFERENCES conta (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Transferencia DROP CONSTRAINT fk_transferencia_cliente_id;
ALTER TABLE Transferencia ADD CONSTRAINT fk_transferencia_cliente_id FOREIGN KEY (cliente_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE Transferencia DROP CONSTRAINT fk_transferencia_cheque_id;
ALTER TABLE Transferencia ADD CONSTRAINT fk_transferencia_cheque_id FOREIGN KEY (cheque_id) REFERENCES cheque (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Transferencia DROP CONSTRAINT fk_transferencia_arquivo_id;
ALTER TABLE Transferencia ADD CONSTRAINT fk_transferencia_arquivo_id FOREIGN KEY (arquivo_id) REFERENCES arquivo (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;