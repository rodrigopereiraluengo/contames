CREATE OR REPLACE FUNCTION fn_diaVencimento(INTEGER) RETURNS DATE AS $$
DECLARE _data DATE;
DECLARE diferencaMes INTEGER;

BEGIN
	_data = (date_part('year', current_date) || '-01-' || $1::TEXT)::DATE;
	diferencaMes = 0;
	IF date_part('month', _data) < date_part('month', current_date) THEN
		diferencaMes = date_part('month', current_date) - date_part('month', _data);
	END IF;
	
	RETURN _data + (diferencaMes * '30 days'::INTERVAL);
END;
$$
LANGUAGE plpgsql;