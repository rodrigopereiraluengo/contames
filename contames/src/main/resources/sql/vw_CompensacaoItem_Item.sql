DROP VIEW IF EXISTS vw_compensacaoitem_item;

CREATE OR REPLACE VIEW vw_compensacaoitem_item AS 
 SELECT co.cliente_id,
    ci.id AS compensacaoitem_id,
    ci.compensacao_id,
    co._data,
    ci.favorecido_id,
    COALESCE(p.fantasia, p.razaosocial, p.apelido, p.nome) AS favorecido,
    categ.id AS categoria_id,
    fami.id AS familia_id,
    tipo.id AS tipo_id,
    det.id AS detalhe_id,
    ((COALESCE(categ.nome::text || ' - '::text, ''::text) || COALESCE(fami.nome::text || ' - '::text, ''::text)) || COALESCE(tipo.nome::text || ' - '::text, ''::text)) || COALESCE(det.nome)::text AS classificacao_nome,
    ci.item_id,
    i.nome AS item,
    co.conta_id,
    c.nome AS conta,
    ci.status,
        CASE
            WHEN ci.tipo::text = 'DES'::text THEN ci.debito
            ELSE ci.credito
        END AS valor,
    ci.tipo,
    co.criacao,
    co.usucriacao_id,
    COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
    co.alteracao,
    co.usualteracao_id,
    COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao
   FROM compensacaoitem ci
     JOIN item i ON ci.item_id = i.id
     JOIN compensacao co ON ci.compensacao_id = co.id
     JOIN conta c ON co.conta_id = c.id
     LEFT JOIN pessoa p ON ci.favorecido_id = p.id
     LEFT JOIN pessoa _usucriacao ON co.usucriacao_id = _usucriacao.id
     LEFT JOIN pessoa _usualteracao ON co.usualteracao_id = _usualteracao.id
     LEFT JOIN classificacao det ON i.classificacao_id = det.id
     LEFT JOIN classificacao tipo ON det.classificacao_id = tipo.id
     LEFT JOIN classificacao fami ON tipo.classificacao_id = fami.id
     LEFT JOIN classificacao categ ON fami.classificacao_id = categ.id
  WHERE co.status::text = ANY (ARRAY['STD'::character varying, 'COM'::character varying]::text[]);