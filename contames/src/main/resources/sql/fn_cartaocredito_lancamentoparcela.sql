DROP FUNCTION IF EXISTS fn_cartaocredito_lancamentoparcela();
CREATE OR REPLACE FUNCTION fn_cartaocredito_lancamentoparcela() RETURNS TRIGGER AS 
$$ 
DECLARE r RECORD;
BEGIN 

IF (TG_OP = 'INSERT') THEN

	/* Meio Cartao Credito */
	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.status = 'LAN' AND COALESCE(NEW.isLimite, false) THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;
	
ELSIF (TG_OP = 'UPDATE') THEN
	
	/* Meio Cartao Credito */
	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.status = 'LAN' AND NEW.status != 'COM' AND COALESCE(OLD.isLimite, false) THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;
	
	/* Meio Cartao Credito */
	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.status = 'LAN' AND OLD.status != 'COM' AND COALESCE(NEW.isLimite, false) THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;
		
ELSIF (TG_OP = 'DELETE') THEN
	
	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.status = 'LAN' AND COALESCE(OLD.isLimite, false) THEN
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	END IF;

END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_cartaocredito_lancamentoparcela ON lancamentoparcela CASCADE;
CREATE TRIGGER tg_cartaocredito_lancamentoparcela
	AFTER INSERT OR UPDATE OR DELETE ON lancamentoparcela
	FOR EACH ROW
	EXECUTE PROCEDURE fn_cartaocredito_lancamentoparcela();