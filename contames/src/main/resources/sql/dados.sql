INSERT INTO Plano(id, nome, lancamentos, contas, cartoescredito, bancoDados, disco, valor, usuarios, status, criacao) VALUES (nextval('plano_seq'), 'Plano 1', 100, 1, 2, 100, 500, 9.99, 1, 'ATI', '2015-03-20 10:41:00.000'), (nextval('plano_seq'), 'Plano 2', 300, 2, 3, 300, 1000, 19.99, 2, 'ATI', '2015-03-20 10:41:00.000'), (nextval('plano_seq'), 'Plano 3', 600, 4, 6, 600, 2000, 39.99, 4, 'ATI', '2015-03-20 10:41:00.000'), (nextval('plano_seq'), 'Plano 4', 1000, 8, 10, 1000, 3000, 79.99, 6, 'ATI', '2015-03-20 10:41:00.000');
/*INSERT INTO Pessoa(id, email, senha, pessoaTipo, nome, cep, endereco, numero, bairro, cidade, uf, status, criacao) VALUES (nextval('pessoa_seq'), 'ropelue@gmail.com', 'krirho52', 'F', 'Rodrigo Luengo', '08940-000', 'Rua Virgilina da Conceição Camargo', '58', 'Jardim Alvorada', 'Biritiba-Mirim', 'SP', 'ATI', '2015-03-20 10:41:00.000');
INSERT INTO Assinatura(id, cliente_id, plano_id, isAceitaTermos, vencimento, status, criacao) VALUES(nextval('assinatura_seq'), currval('pessoa_seq'), 3, true, '2015-04-20 10:41:00.000', 'ATI', '2015-03-20 10:41:00.000');
INSERT INTO AssinaturaPagamento(id, assinatura_id, plano_id, valor, status, criacao) VALUES (nextval('assinaturapagamento_seq'), currval('assinatura_seq'), 1, 9.99, 'CON', '2015-04-20 10:41:00.000');

INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'PESSOA_CATEGORIA', 'Banco', 'ATI', currval('pessoa_seq'));

INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'BRADESCO', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'ITAU UNIBANCO', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'SANTANDER', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'BANCO DO BRASIL', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'CAIXA ECONOMICA FEDERAL', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'CITIBANK', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'BANCO REAL', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');
INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'HSBC', currval('descricao_seq'), 'ATI', 1, 1, '2015-03-20 10:41:00.000');


INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'American Express', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Aura', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Diners Club', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Hipercard', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Master Card', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Sorocred', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'VISA', 'ATI', 1);
INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'BNDS', 'ATI', 1);
*/