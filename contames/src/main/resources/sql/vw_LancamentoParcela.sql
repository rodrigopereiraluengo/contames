DROP VIEW IF EXISTS vw_lancamentoparcela CASCADE;
CREATE OR REPLACE VIEW vw_lancamentoparcela AS 
SELECT 
	l.cliente_id,
    lp.id AS lancamentoparcela_id,
    lp.lancamento_id,
    COALESCE(ch._data, lp._data) AS _data,
    l.favorecido_id,
    COALESCE(p.fantasia, p.razaosocial, p.apelido, p.nome) AS favorecido,
    l.tipodoc,
    l.numerodoc,
    replace(regexp_replace(l.numerodoc::text, '[^0-9]*'::text, ''::text), '.', '')::integer AS numerodoc_clean,
    lp.meio,
    lp.conta_id,
    lp.cartaocredito_id,
    lp.cheque_id,
    ch.numero AS cheque_numero,
    ch.talaocheque_id,
    COALESCE(COALESCE(c.nome || ' '|| ch.numero, c.nome, cc.nome)) AS meio_conta,
        CASE
            WHEN l.quantlancamentoparcela = 1 THEN ' - '
            ELSE ((lp.seq + 1) || ' / ') || l.quantlancamentoparcela
        END AS parcela,
    lp.status,
    lp.valor,
    lp.tipo,
    l.criacao,
    l.usucriacao_id,
    COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
    l.alteracao,
    l.usualteracao_id,
    COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao,
    alp.id AS arquivo_id,
    alp.nome AS arquivo_nome,
    al.id AS arquivoDoc_id,
    al.nome AS arquivoDoc_nome,
    l.cartaoCredito_id fatCartaoCredito_id,
    lcc.nome fatCartaoCredito_nome,
    lp.boletotipo,
    lp.boletonumero
FROM lancamentoparcela lp
JOIN lancamento l ON lp.lancamento_id = l.id
JOIN pessoa p ON l.favorecido_id = p.id
LEFT JOIN cheque ch ON lp.cheque_id = ch.id
LEFT JOIN talaocheque tc ON ch.talaocheque_id = tc.id
LEFT JOIN conta c ON lp.conta_id = c.id OR c.id = tc.conta_id
LEFT JOIN cartaocredito cc ON lp.cartaocredito_id = cc.id
LEFT JOIN pessoa _usucriacao ON l.usucriacao_id = _usucriacao.id
LEFT JOIN pessoa _usualteracao ON l.usualteracao_id = _usualteracao.id
LEFT JOIN arquivo AS alp ON lp.arquivo_id = alp.id
LEFT JOIN arquivo AS al ON l.arquivoDoc_id = al.id
LEFT JOIN cartaocredito AS lcc ON l.cartaoCredito_id = lcc.id
WHERE lp.status IN('STD', 'LAN', 'CAN') OR (lp.status = 'COM' AND lp.cartaoCredito_id IS NOT NULL AND NOT EXISTS(SELECT bi.id FROM baixaitem bi WHERE bi.lancamentoParcela_id = lp.id AND bi.status = 'COM'))
UNION SELECT 
	i.cliente_id,
	i.lancamentoparcela_id,
	i.lancamento_id,
	i._data,
	i.favorecido_id,
	i.favorecido,
	i.tipodoc,
	i.numerodoc,
	i.numerodoc_clean,
	i.meio,
	i.conta_id,
	i.cartaocredito_id,
	i.cheque_id,
	i.cheque_numero,
	i.talaocheque_id,
	i.meio_conta,
	i.parcela,
	i.status,
	i.valor,
	i.tipo,
	i.criacao,
	i.usucriacao_id,
	i.usucriacao,
	i.alteracao,
	i.usualteracao_id,
	i.usualteracao,
	i.arquivo_id,
	i.arquivo_nome,
	i.arquivoDoc_id,
	i.arquivoDoc_nome,
	i.fatCartaoCredito_id,
	i.fatCartaoCredito_nome,
	i.boletotipo,
	i.boletonumero
FROM vw_Item_Auto AS i
WHERE fn_dataDiaUtil(i._data::date) > CURRENT_TIMESTAMP