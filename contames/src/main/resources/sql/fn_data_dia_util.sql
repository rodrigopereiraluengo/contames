DROP FUNCTION fn_data_dia_util(dia int, mes int, ano int);
CREATE OR REPLACE FUNCTION fn_data_dia_util(dia int, mes int, ano int) RETURNS DATE AS
$$
DECLARE _data DATE;
BEGIN

	_data := make_date(ano, mes, 1) + (dia - 1);


	RETURN _data;
END;
$$
LANGUAGE 'plpgsql';