DROP VIEW IF EXISTS vw_relatorio_item;
CREATE OR REPLACE VIEW vw_relatorio_item AS 
SELECT 
	r.cliente_id,
	ci.categoria_id,
	ci.familia_id,
	ci.tipo_id,
	ci.detalhe_id,
    COALESCE(ci.item_id, i.id) AS item_id,
    COALESCE(ci.categoria_nome || ' - ', '') || COALESCE(ci.familia_nome || ' - ', '') || COALESCE(ci.tipo_nome || ' - ', '') || COALESCE(ci.detalhe_nome || ' - ', '') || COALESCE(ci.item_nome, i.nome) AS item_nome,
    date_part('year', r._data) AS ano,
    r.tipo,
    r.status,
    date_part('month', r._data) AS mes,
        CASE
            WHEN date_part('month', r._data) = 1 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_1,
        CASE
            WHEN date_part('month', r._data) = 1 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_1,
        CASE
            WHEN date_part('month', r._data) = 1 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_1,
        CASE
            WHEN date_part('month', r._data) = 2 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_2,
        CASE
            WHEN date_part('month', r._data) = 2 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_2,
        CASE
            WHEN date_part('month', r._data) = 2 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_2,
        CASE
            WHEN date_part('month', r._data) = 3 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_3,
        CASE
            WHEN date_part('month', r._data) = 3 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_3,
        CASE
            WHEN date_part('month', r._data) = 3 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_3,
        CASE
            WHEN date_part('month', r._data) = 4 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_4,
        CASE
            WHEN date_part('month', r._data) = 4 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_4,
        CASE
            WHEN date_part('month', r._data) = 4 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_4,
        CASE
            WHEN date_part('month', r._data) = 5 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_5,
        CASE
            WHEN date_part('month', r._data) = 5 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_5,
        CASE
            WHEN date_part('month', r._data) = 5 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_5,
        CASE
            WHEN date_part('month', r._data) = 6 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_6,
        CASE
            WHEN date_part('month', r._data) = 6 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_6,
        CASE
            WHEN date_part('month', r._data) = 6 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_6,
        CASE
            WHEN date_part('month', r._data) = 7 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_7,
        CASE
            WHEN date_part('month', r._data) = 7 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_7,
        CASE
            WHEN date_part('month', r._data) = 7 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_7,
        CASE
            WHEN date_part('month', r._data) = 8 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_8,
        CASE
            WHEN date_part('month', r._data) = 8 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_8,
        CASE
            WHEN date_part('month', r._data) = 8 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_8,
        CASE
            WHEN date_part('month', r._data) = 9 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_9,
        CASE
            WHEN date_part('month', r._data) = 9 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_9,
        CASE
            WHEN date_part('month', r._data) = 9 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_9,
        CASE
            WHEN date_part('month', r._data) = 10 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_10,
        CASE
            WHEN date_part('month', r._data) = 10 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_10,
        CASE
            WHEN date_part('month', r._data) = 10 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_10,
        CASE
            WHEN date_part('month', r._data) = 11 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_11,
        CASE
            WHEN date_part('month', r._data) = 11 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_11,
        CASE
            WHEN date_part('month', r._data) = 11 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_11,
        CASE
            WHEN date_part('month', r._data) = 12 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS despesas_12,
        CASE
            WHEN date_part('month', r._data) = 12 THEN
            CASE
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS receitas_12,
        CASE
            WHEN date_part('month', r._data) = 12 THEN
            CASE
                WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
                WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
                ELSE NULL
            END
            ELSE NULL
        END AS valor_12,
        CASE
            WHEN r.tipo = 'DES' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
            WHEN r.tipo = 'REC' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
            ELSE NULL
        END AS despesas,
        CASE
            WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
            WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
            ELSE NULL
        END AS receitas,
        CASE
            WHEN r.tipo = 'DES' AND r.valor > 0 THEN (- COALESCE(li.perc, 1)) * r.valor
            WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor
            WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor
            WHEN r.tipo = 'DES' AND r.valor < 0 THEN (- COALESCE(li.perc, 1)) * r.valor
            ELSE NULL
        END AS valor,
        isCCred
FROM vw_relatorio_data r
LEFT JOIN lancamentoitem li ON r.lancamento_id = li.lancamento_id
LEFT JOIN compensacaoitem coi ON r.compensacaoitem_id = coi.id AND coi.item_id IS NOT NULL
LEFT JOIN vw_classificacaoitem ci ON li.item_id = ci.item_id OR coi.item_id = ci.item_id
LEFT JOIN Item i ON COALESCE(li.item_id, coi.item_id) = i.id;