CREATE OR REPLACE FUNCTION fn_qtdelancamentos(p_cliente_id bigint) RETURNS integer AS $$
DECLARE qtde INTEGER;
DECLARE pagamento TIMESTAMP;
DECLARE proxpagamento TIMESTAMP;
BEGIN
	
	SELECT ap.pagamento, ap.proxpagamento INTO pagamento, proxpagamento 
	FROM AssinaturaPagamento AS ap
	INNER JOIN Assinatura a ON ap.assinatura_id = a.id
	WHERE a.cliente_id = p_cliente_id
	AND ap.status = 'CON'
	LIMIT 1;

	SELECT COUNT(id) INTO qtde 
	FROM Lancamento 
	WHERE cliente_id = p_cliente_id
	AND criacao BETWEEN pagamento AND proxpagamento;
		
	RETURN qtde;
END;
$$ LANGUAGE plpgsql;
