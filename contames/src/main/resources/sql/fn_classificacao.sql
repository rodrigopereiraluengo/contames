CREATE OR REPLACE FUNCTION fn_qtdeLancamentos(p_cliente_id BIGINT) RETURNS INTEGER AS $$
DECLARE qtde INTEGER;
DECLARE dt_alteracao TIMESTAMP;
BEGIN
	SELECT ap.alteracao INTO dt_alteracao 
	FROM AssinaturaPagamento AS ap
	INNER JOIN Assinatura a ON ap.assinatura_id = a.id
	WHERE a.cliente_id = p_cliente_id
	AND ap.status = 'CON'
	LIMIT 1;

	SELECT COUNT(id) INTO qtde 
	FROM Lancamento 
	WHERE cliente_id = p_cliente_id
	AND criacao BETWEEN dt_alteracao AND dt_alteracao + INTERVAL '1 MONTH';
		

	RETURN qtde;
END;
$$ LANGUAGE plpgsql;