DROP FUNCTION IF EXISTS fn_baixaitem_delete();
CREATE OR REPLACE FUNCTION fn_baixaitem_delete() RETURNS TRIGGER AS 
$$ 

BEGIN 
	
	IF OLD.status = 'BXD' THEN
		
		UPDATE LancamentoParcela SET status = 'LAN' WHERE id = OLD.lancamentoParcela_id;
				
	END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_baixaitem_delete ON baixaitem CASCADE;
CREATE TRIGGER tg_baixaitem_delete
	AFTER DELETE ON baixaitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_baixaitem_delete();