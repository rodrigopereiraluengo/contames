DROP VIEW IF EXISTS vw_relatorio_favorecido;

CREATE OR REPLACE VIEW vw_relatorio_favorecido AS 
 SELECT vw_relatorio_data.cliente_id,
    vw_relatorio_data.favorecido_id,
    vw_relatorio_data.favorecido,
    date_part('year'::text, vw_relatorio_data._data) AS ano,
    vw_relatorio_data.tipo,
    vw_relatorio_data.status,
    date_part('month'::text, vw_relatorio_data._data) AS mes,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 1::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_1,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 1::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_1,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 1::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_1,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 2::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_2,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 2::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_2,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 2::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_2,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 3::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_3,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 3::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_3,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 3::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_3,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 4::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_4,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 4::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_4,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 4::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_4,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 5::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_5,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 5::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_5,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 5::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_5,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 6::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_6,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 6::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_6,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 6::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_6,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 7::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_7,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 7::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_7,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 7::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_7,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 8::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_8,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 8::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_8,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 8::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_8,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 9::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_9,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 9::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_9,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 9::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_9,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 10::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_10,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 10::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_10,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 10::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_10,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 11::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_11,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 11::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_11,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 11::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_11,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 12::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_12,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 12::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_12,
        CASE
            WHEN date_part('month'::text, vw_relatorio_data._data) = 12::double precision THEN
            CASE
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
                WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_12,
        CASE
            WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
            WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
            ELSE NULL::double precision
        END AS despesas,
        CASE
            WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
            WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
            ELSE NULL::double precision
        END AS receitas,
        CASE
            WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor > 0::double precision THEN - vw_relatorio_data.valor
            WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor < 0::double precision THEN vw_relatorio_data.valor
            WHEN vw_relatorio_data.tipo::text = 'REC'::text AND vw_relatorio_data.valor > 0::double precision THEN vw_relatorio_data.valor
            WHEN vw_relatorio_data.tipo::text = 'DES'::text AND vw_relatorio_data.valor < 0::double precision THEN - vw_relatorio_data.valor
            ELSE NULL::double precision
        END AS valor,
        isCCred
   FROM vw_relatorio_data;