DROP FUNCTION IF EXISTS fn_lancamentoitem_produto() CASCADE;
CREATE OR REPLACE FUNCTION fn_lancamentoitem_produto() RETURNS TRIGGER AS 
$$ 
DECLARE tipo VARCHAR(3);
DECLARE status VARCHAR(3);
DECLARE valorUnit DOUBLE PRECISION;
BEGIN 
	
	
	
IF (TG_OP = 'INSERT') THEN

	SELECT l.tipo, l.status INTO tipo, status FROM Lancamento l WHERE l.id = NEW.lancamento_id;

	IF tipo = 'CPR' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque + NEW.quant, valorCompra = NEW.valorUnit WHERE id = NEW.item_id;
	END IF;
	
	IF tipo = 'VEN' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque - NEW.quant WHERE id = NEW.item_id;
	END IF;
	
ELSIF (TG_OP = 'UPDATE') THEN

	SELECT l.tipo, l.status INTO tipo, status FROM Lancamento l WHERE l.id = OLD.lancamento_id;
	
	/* OLD */
	IF tipo = 'CPR' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque - OLD.quant, valorCompra = OLD.valorUnit WHERE id = OLD.item_id;
	END IF;
	
	IF tipo = 'VEN' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque + OLD.quant WHERE id = NEW.item_id;
	END IF;
	
	/* NEW */
	IF tipo = 'CPR' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque + NEW.quant, valorCompra = NEW.valorUnit WHERE id = NEW.item_id;
	END IF;
	
	IF tipo = 'VEN' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque - NEW.quant WHERE id = NEW.item_id;
	END IF;
		
ELSIF (TG_OP = 'DELETE') THEN

	SELECT l.tipo, l.status INTO tipo, status FROM Lancamento l WHERE l.id = NEW.lancamento_id;
	
	SELECT li.valorUnit INTO valorUnit 
	FROM LancamentoItem li 
	WHERE li.status IN('LAN', 'PAG', 'COM') 
	AND li.item_id = OLD.item_id ORDER BY li.id DESC LIMIT 1;
	
	IF tipo = 'CPR' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque - OLD.quant, valorCompra = valorUnit WHERE id = OLD.item_id;
	END IF;
	
	IF tipo = 'VEN' AND status = 'LAN' THEN
		UPDATE Item SET estoque = estoque + OLD.quant WHERE id = OLD.item_id;
	END IF;

END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_lancamentoitem_produto ON lancamentoitem CASCADE;
CREATE TRIGGER tg_lancamentoitem_produto
	AFTER INSERT OR UPDATE OR DELETE ON lancamentoitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_lancamentoitem_produto();