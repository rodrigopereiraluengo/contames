DROP VIEW IF EXISTS vw_relatorio_rateio;

CREATE OR REPLACE VIEW vw_relatorio_rateio AS 
 SELECT r.cliente_id,
    r.favorecido_id,
    r.favorecido,
    rateio.id AS rateio_id,
    p.id AS rateio_pessoa_id,
    COALESCE(rateio.nome, p.fantasia, p.razaosocial, p.apelido, p.nome) AS rateio_nome,
    date_part('year'::text, r._data) AS ano,
    r.tipo,
    r.status,
    date_part('month'::text, r._data) AS mes,
        CASE
            WHEN date_part('month'::text, r._data) = 1::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_1,
        CASE
            WHEN date_part('month'::text, r._data) = 1::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_1,
        CASE
            WHEN date_part('month'::text, r._data) = 1::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_1,
        CASE
            WHEN date_part('month'::text, r._data) = 2::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_2,
        CASE
            WHEN date_part('month'::text, r._data) = 2::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_2,
        CASE
            WHEN date_part('month'::text, r._data) = 2::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_2,
        CASE
            WHEN date_part('month'::text, r._data) = 3::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_3,
        CASE
            WHEN date_part('month'::text, r._data) = 3::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_3,
        CASE
            WHEN date_part('month'::text, r._data) = 3::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_3,
        CASE
            WHEN date_part('month'::text, r._data) = 4::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_4,
        CASE
            WHEN date_part('month'::text, r._data) = 4::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_4,
        CASE
            WHEN date_part('month'::text, r._data) = 4::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_4,
        CASE
            WHEN date_part('month'::text, r._data) = 5::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_5,
        CASE
            WHEN date_part('month'::text, r._data) = 5::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_5,
        CASE
            WHEN date_part('month'::text, r._data) = 5::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_5,
        CASE
            WHEN date_part('month'::text, r._data) = 6::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_6,
        CASE
            WHEN date_part('month'::text, r._data) = 6::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_6,
        CASE
            WHEN date_part('month'::text, r._data) = 6::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_6,
        CASE
            WHEN date_part('month'::text, r._data) = 7::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_7,
        CASE
            WHEN date_part('month'::text, r._data) = 7::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_7,
        CASE
            WHEN date_part('month'::text, r._data) = 7::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_7,
        CASE
            WHEN date_part('month'::text, r._data) = 8::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_8,
        CASE
            WHEN date_part('month'::text, r._data) = 8::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_8,
        CASE
            WHEN date_part('month'::text, r._data) = 8::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_8,
        CASE
            WHEN date_part('month'::text, r._data) = 9::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_9,
        CASE
            WHEN date_part('month'::text, r._data) = 9::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_9,
        CASE
            WHEN date_part('month'::text, r._data) = 9::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_9,
        CASE
            WHEN date_part('month'::text, r._data) = 10::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_10,
        CASE
            WHEN date_part('month'::text, r._data) = 10::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_10,
        CASE
            WHEN date_part('month'::text, r._data) = 10::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_10,
        CASE
            WHEN date_part('month'::text, r._data) = 11::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_11,
        CASE
            WHEN date_part('month'::text, r._data) = 11::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_11,
        CASE
            WHEN date_part('month'::text, r._data) = 11::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_11,
        CASE
            WHEN date_part('month'::text, r._data) = 12::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS despesas_12,
        CASE
            WHEN date_part('month'::text, r._data) = 12::double precision THEN
            CASE
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS receitas_12,
        CASE
            WHEN date_part('month'::text, r._data) = 12::double precision THEN
            CASE
                WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
                WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
                ELSE NULL::double precision
            END
            ELSE NULL::double precision
        END AS valor_12,
        CASE
            WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
            WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
            ELSE NULL::double precision
        END AS despesas,
        CASE
            WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
            WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
            ELSE NULL::double precision
        END AS receitas,
        CASE
            WHEN r.tipo::text = 'DES'::text AND r.valor > 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
            WHEN r.tipo::text = 'REC'::text AND r.valor < 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
            WHEN r.tipo::text = 'REC'::text AND r.valor > 0::double precision THEN COALESCE(lr.perccalc, 1::double precision) * r.valor
            WHEN r.tipo::text = 'DES'::text AND r.valor < 0::double precision THEN (- COALESCE(lr.perccalc, 1::double precision)) * r.valor
            ELSE NULL::double precision
        END AS valor,
        isCCred
   FROM vw_relatorio_data r
     LEFT JOIN lancamentorateio lr ON r.lancamento_id = lr.lancamento_id
     LEFT JOIN descricao rateio ON lr.rateio_id = rateio.id
     LEFT JOIN pessoa p ON lr.pessoa_id = p.id;