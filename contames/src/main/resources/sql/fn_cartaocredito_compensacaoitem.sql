DROP FUNCTION IF EXISTS fn_cartaocredito_compensacaoitem();
CREATE OR REPLACE FUNCTION fn_cartaocredito_compensacaoitem() RETURNS TRIGGER AS 
$$ 
DECLARE _cartaoCredito_id BIGINT;
DECLARE _perc DOUBLE PRECISION;
DECLARE _valorDoc DOUBLE PRECISION;
BEGIN 

IF (TG_OP = 'INSERT') THEN

	SELECT l.cartaoCredito_id, lp.perc, l.valorDoc INTO _cartaoCredito_id, _perc, _valorDoc
	FROM Lancamento l, LancamentoParcela lp, BaixaItem bi
	WHERE l.id = lp.lancamento_id
	AND lp.id = bi.lancamentoParcela_id
	AND bi.id = NEW.baixaItem_id;

	IF NEW.status = 'COM' AND _cartaoCredito_id IS NOT NULL THEN
		
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		
	END IF;
	
ELSIF (TG_OP = 'UPDATE') THEN
	
	SELECT l.cartaoCredito_id, lp.perc, l.valorDoc INTO _cartaoCredito_id, _perc, _valorDoc
	FROM Lancamento l, LancamentoParcela lp, BaixaItem bi
	WHERE l.id = lp.lancamento_id
	AND lp.id = bi.lancamentoParcela_id
	AND bi.id = NEW.baixaItem_id;

	IF OLD.status = 'COM' AND _cartaoCredito_id IS NOT NULL THEN
		
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
	
	END IF;

	IF NEW.status = 'COM' AND _cartaoCredito_id IS NOT NULL THEN
	
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		
	END IF;
	
ELSIF (TG_OP = 'DELETE') THEN

	SELECT l.cartaoCredito_id, lp.perc, l.valorDoc INTO _cartaoCredito_id, _perc, _valorDoc
	FROM Lancamento l, LancamentoParcela lp, BaixaItem bi
	WHERE l.id = lp.lancamento_id
	AND lp.id = bi.lancamentoParcela_id
	AND bi.id = OLD.baixaItem_id;
	
	IF OLD.status = 'COM' AND _cartaoCredito_id IS NOT NULL THEN
	
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		
	END IF;
		
END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_cartaocredito_compensacaoitem ON compensacaoitem CASCADE;
CREATE TRIGGER tg_cartaocredito_compensacaoitem
	AFTER INSERT OR UPDATE OR DELETE ON compensacaoitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_cartaocredito_compensacaoitem();