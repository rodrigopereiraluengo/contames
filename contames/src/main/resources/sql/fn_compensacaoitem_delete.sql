DROP FUNCTION IF EXISTS fn_compensacaoitem_delete();
CREATE OR REPLACE FUNCTION fn_compensacaoitem_delete() RETURNS TRIGGER AS 
$$ 
BEGIN 
	
	IF OLD.baixaItem_id IS NOT NULL THEN
		UPDATE BaixaItem SET status = 'BXD' WHERE id = OLD.baixaItem_id;
	END IF;
	
	IF OLD.transferencia_id IS NOT NULL THEN
		UPDATE Transferencia SET status = 'LAN' WHERE id = OLD.transferencia_id;
	END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_compensacaoitem_delete ON compensacaoitem CASCADE;
CREATE TRIGGER tg_compensacaoitem_delete
	AFTER DELETE ON compensacaoitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_compensacaoitem_delete();