DROP VIEW IF EXISTS vw_compensacaoitem;

CREATE OR REPLACE VIEW vw_compensacaoitem AS 
SELECT co.cliente_id,
    lp.id AS lancamentoparcela_id,
    lp.lancamento_id,
    bi.id AS baixaitem_id,
    bi.baixa_id,
    ci.id AS compensacaoitem_id,
    ci.compensacao_id,
    co._data,
    l.favorecido_id,
    COALESCE(p.fantasia, p.razaosocial, p.apelido, p.nome) AS favorecido,
    l.tipodoc,
    l.numerodoc,
    replace(regexp_replace(l.numerodoc::text, '[^0-9]*'::text, ''::text), '.', '')::integer AS numerodoc_clean,
    bi.meio,
    co.conta_id,
    bi.cheque_id,
    ch.numero AS cheque_numero,
    ch.talaocheque_id,
    COALESCE(COALESCE(c.nome::text || COALESCE(' '::text || ch.numero), c.nome::text)) AS meio_conta,
        CASE
            WHEN l.quantlancamentoparcela = 1 THEN ' - '::text
            ELSE ((lp.seq + 1) || ' / '::text) || l.quantlancamentoparcela
        END AS parcela,
    ci.status,
        CASE
            WHEN ci.tipo::text = 'DES'::text THEN ci.debito
            ELSE ci.credito
        END AS valor,
    ci.tipo,
    co.criacao,
    co.usucriacao_id,
    COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
    co.alteracao,
    co.usualteracao_id,
    COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao,
    al.id arquivoDoc_id,
    al.nome arquivoDoc_nome,
    COALESCE(abi.id, alp.id) arquivo_id,
    COALESCE(abi.nome, alp.nome) arquivo_nome,
    comprov.id comprovante_id,
    comprov.nome comprovante_nome,
    l.cartaoCredito_id fatCartaoCredito_id,
    lcc.nome fatCartaoCredito_nome,
    COALESCE(bi.boletotipo, lp.boletotipo) AS boletotipo,
    COALESCE(bi.boletonumero, lp.boletonumero) AS boletonumero
FROM compensacaoitem ci
JOIN compensacao co ON ci.compensacao_id = co.id
JOIN baixaitem bi ON ci.baixaitem_id = bi.id
JOIN lancamentoparcela lp ON bi.lancamentoparcela_id = lp.id
JOIN lancamento l ON lp.lancamento_id = l.id
JOIN pessoa p ON l.favorecido_id = p.id
JOIN conta c ON co.conta_id = c.id
LEFT JOIN cheque ch ON bi.cheque_id = ch.id
LEFT JOIN talaocheque tc ON ch.talaocheque_id = tc.id AND tc.conta_id = c.id
LEFT JOIN pessoa _usucriacao ON co.usucriacao_id = _usucriacao.id
LEFT JOIN pessoa _usualteracao ON co.usualteracao_id = _usualteracao.id
LEFT JOIN arquivo al ON al.id = l.arquivoDoc_id
LEFT JOIN arquivo alp ON alp.id = lp.arquivo_id
LEFT JOIN arquivo abi ON abi.id = bi.arquivo_id
LEFT JOIN arquivo comprov ON comprov.id = bi.comprovante_id
LEFT JOIN cartaocredito AS lcc ON l.cartaoCredito_id = lcc.id
WHERE co.status = 'COM';