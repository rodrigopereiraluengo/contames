DROP VIEW IF EXISTS vw_baixaitem CASCADE;

CREATE OR REPLACE VIEW vw_baixaitem AS 
SELECT b.cliente_id,
	bi.lancamentoparcela_id,
    lp.lancamento_id,
    bi.id AS baixaitem_id,
    bi.baixa_id,
    COALESCE(ch._data, b._data) AS _data,
    l.favorecido_id,
    COALESCE(p.fantasia, p.razaosocial, p.apelido, p.nome) AS favorecido,
    l.tipodoc,
    l.numerodoc,
    replace(regexp_replace(l.numerodoc::text, '[^0-9]*'::text, ''::text), '.', '')::integer AS numerodoc_clean,
    bi.meio,
    bi.conta_id,
    bi.cartaocredito_id,
    bi.cheque_id,
    ch.numero AS cheque_numero,
    ch.talaocheque_id,
    COALESCE(COALESCE(c.nome || COALESCE(' ' || ch.numero), c.nome, cc.nome)) AS meio_conta,
        CASE
            WHEN l.quantlancamentoparcela = 1 THEN ' - '
            ELSE ((lp.seq + 1) || ' / ') || l.quantlancamentoparcela
        END AS parcela,
    bi.status,
    bi.valor,
    bi.tipo,
    b.criacao,
    b.usucriacao_id,
    COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
    b.alteracao,
    b.usualteracao_id,
    COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao,
    al.id AS arquivoDoc_id,
    al.nome AS arquivoDoc_nome,
    COALESCE(abi.id, alp.id) AS arquivo_id,
    COALESCE(abi.nome, alp.nome) AS arquivo_nome,
    comprov.id AS comprovante_id,
    comprov.nome AS comprovante_nome,
    l.cartaoCredito_id fatCartaoCredito_id,
    lcc.nome fatCartaoCredito_nome,
    COALESCE(bi.boletotipo, lp.boletotipo) AS boletotipo,
    COALESCE(bi.boletonumero, lp.boletonumero) AS boletonumero
FROM baixaitem bi
JOIN baixa b ON bi.baixa_id = b.id
JOIN lancamentoparcela lp ON bi.lancamentoparcela_id = lp.id
JOIN lancamento l ON lp.lancamento_id = l.id
JOIN pessoa p ON l.favorecido_id = p.id
LEFT JOIN cheque ch ON bi.cheque_id = ch.id
LEFT JOIN talaocheque tc ON ch.talaocheque_id = tc.id
LEFT JOIN conta c ON bi.conta_id = c.id OR c.id = tc.conta_id
LEFT JOIN cartaocredito cc ON bi.cartaocredito_id = cc.id
LEFT JOIN pessoa _usucriacao ON b.usucriacao_id = _usucriacao.id
LEFT JOIN pessoa _usualteracao ON b.usualteracao_id = _usualteracao.id
LEFT JOIN arquivo al ON al.id = l.arquivoDoc_id
LEFT JOIN arquivo alp ON alp.id = lp.arquivo_id
LEFT JOIN arquivo abi ON abi.id = bi.arquivo_id
LEFT JOIN arquivo comprov ON comprov.id = bi.comprovante_id
LEFT JOIN cartaocredito AS lcc ON l.cartaoCredito_id = lcc.id
WHERE bi.status = 'BXD' OR (bi.status = 'COM' AND bi.cartaocredito_id IS NOT NULL);