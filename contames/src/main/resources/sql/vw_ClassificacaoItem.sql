DROP VIEW IF EXISTS vw_classificacaoitem;

CREATE OR REPLACE VIEW vw_classificacaoitem AS 
 SELECT cat.id AS categoria_id,
    cat.nome AS categoria_nome,
    NULL::integer AS familia_id,
    NULL::character varying AS familia_nome,
    NULL::integer AS tipo_id,
    NULL::character varying AS tipo_nome,
    NULL::integer AS detalhe_id,
    NULL::character varying AS detalhe_nome,
    cat.id AS classificacao_id,
    'CAT'::text AS nivel,
    cat.nome AS classificacao_nome,
    i.id AS item_id,
    i.nome AS item_nome
   FROM item i,
    classificacao cat
  WHERE i.classificacao_id = cat.id AND cat.nivel::text = 'CAT'::text
UNION
 SELECT cat.id AS categoria_id,
    cat.nome AS categoria_nome,
    fam.id AS familia_id,
    fam.nome AS familia_nome,
    NULL::integer AS tipo_id,
    NULL::character varying AS tipo_nome,
    NULL::integer AS detalhe_id,
    NULL::character varying AS detalhe_nome,
    fam.id AS classificacao_id,
    'FAM'::text AS nivel,
    fam.nome AS classificacao_nome,
    i.id AS item_id,
    i.nome AS item_nome
   FROM item i,
    classificacao fam,
    classificacao cat
  WHERE i.classificacao_id = fam.id AND fam.nivel::text = 'FAM'::text AND fam.classificacao_id = cat.id
UNION
 SELECT cat.id AS categoria_id,
    cat.nome AS categoria_nome,
    fam.id AS familia_id,
    fam.nome AS familia_nome,
    tip.id AS tipo_id,
    tip.nome AS tipo_nome,
    NULL::integer AS detalhe_id,
    NULL::character varying AS detalhe_nome,
    tip.id AS classificacao_id,
    'TIP'::text AS nivel,
    tip.nome AS classificacao_nome,
    i.id AS item_id,
    i.nome AS item_nome
   FROM item i,
    classificacao tip,
    classificacao fam,
    classificacao cat
  WHERE i.classificacao_id = tip.id AND tip.nivel::text = 'TIP'::text AND tip.classificacao_id = fam.id AND fam.classificacao_id = cat.id
UNION
 SELECT cat.id AS categoria_id,
    cat.nome AS categoria_nome,
    fam.id AS familia_id,
    fam.nome AS familia_nome,
    tip.id AS tipo_id,
    tip.nome AS tipo_nome,
    det.id AS detalhe_id,
    det.nome AS detalhe_nome,
    det.id AS classificacao_id,
    'DET'::text AS nivel,
    det.nome AS classificacao_nome,
    i.id AS item_id,
    i.nome AS item_nome
   FROM item i,
    classificacao det,
    classificacao tip,
    classificacao fam,
    classificacao cat
  WHERE i.classificacao_id = det.id AND det.nivel::text = 'DET'::text AND det.classificacao_id = tip.id AND tip.classificacao_id = fam.id AND fam.classificacao_id = cat.id;