DROP VIEW IF EXISTS vw_compensacaoitem_transferencia;

CREATE OR REPLACE VIEW vw_compensacaoitem_transferencia AS 
SELECT co.cliente_id,
    ci.id AS compensacaoitem_id,
    ci.compensacao_id,
    t.id AS transferencia_id,
    co._data,
    COALESCE(_destino.titular_id, _destino.cliente_id) AS favorecido_id,
    COALESCE(titular.fantasia, titular.razaosocial, titular.apelido, titular.nome, cliente.fantasia, cliente.razaosocial, cliente.apelido, cliente.nome) AS favorecido,
    t.conta_id,
    t.cartaocredito_id,
    t.cheque_id,
    ch.numero AS cheque_numero,
    ch.talaocheque_id,
    COALESCE(COALESCE(c.nome::text || COALESCE(' '::text || ch.numero), c.nome::text, cc.nome::text)) AS origem,
    t.destino_id,
    _destino.nome AS destino,
    t.statusorigem AS status,
    ci.debito AS valor,
    'DES' tipo,
    co.criacao,
    co.usucriacao_id,
    COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
    co.alteracao,
    co.usualteracao_id,
    COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao,
    a.id arquivo_id,
    a.nome arquivo_nome
FROM compensacaoitem ci
JOIN compensacao co ON ci.compensacao_id = co.id
JOIN transferencia t ON ci.transferencia_id = t.id
JOIN conta _destino ON t.destino_id = _destino.id
JOIN pessoa cliente ON t.cliente_id = cliente.id
LEFT JOIN pessoa titular ON _destino.titular_id = titular.id
LEFT JOIN cheque ch ON t.cheque_id = ch.id
LEFT JOIN talaocheque tc ON ch.talaocheque_id = tc.id
LEFT JOIN conta c ON t.conta_id = c.id OR tc.conta_id = c.id
LEFT JOIN cartaocredito cc ON t.cartaocredito_id = cc.id
LEFT JOIN pessoa _usucriacao ON t.usucriacao_id = _usucriacao.id
LEFT JOIN pessoa _usualteracao ON t.usualteracao_id = _usualteracao.id
LEFT JOIN arquivo a ON a.id = t.arquivo_id
WHERE ci.status = 'COM' 
AND ci.tipo = 'DES'
UNION SELECT co.cliente_id,
    ci.id AS compensacaoitem_id,
    ci.compensacao_id,
    t.id AS transferencia_id,
    co._data,
    COALESCE(_destino.titular_id, _destino.cliente_id) AS favorecido_id,
    COALESCE(titular.fantasia, titular.razaosocial, titular.apelido, titular.nome, cliente.fantasia, cliente.razaosocial, cliente.apelido, cliente.nome) AS favorecido,
    t.conta_id,
    t.cartaocredito_id,
    t.cheque_id,
    ch.numero AS cheque_numero,
    ch.talaocheque_id,
    COALESCE(COALESCE(c.nome::text || COALESCE(' '::text || ch.numero), c.nome::text, cc.nome::text)) AS origem,
    t.destino_id,
    _destino.nome AS destino,
    t.statusorigem AS status,
    ci.credito AS valor,
    'REC' tipo,
    co.criacao,
    co.usucriacao_id,
    COALESCE(_usucriacao.fantasia, _usucriacao.razaosocial, _usucriacao.apelido, _usucriacao.nome) AS usucriacao,
    co.alteracao,
    co.usualteracao_id,
    COALESCE(_usualteracao.fantasia, _usualteracao.razaosocial, _usualteracao.apelido, _usualteracao.nome) AS usualteracao,
    a.id arquivo_id,
    a.nome arquivo_nome
FROM compensacaoitem ci
JOIN compensacao co ON ci.compensacao_id = co.id
JOIN transferencia t ON ci.transferencia_id = t.id AND t.destino_id = co.conta_id
JOIN conta _destino ON t.destino_id = _destino.id
JOIN pessoa cliente ON t.cliente_id = cliente.id
LEFT JOIN pessoa titular ON _destino.titular_id = titular.id
LEFT JOIN cheque ch ON t.cheque_id = ch.id
LEFT JOIN talaocheque tc ON ch.talaocheque_id = tc.id
JOIN conta c ON t.conta_id = c.id OR tc.conta_id = c.id
LEFT JOIN cartaocredito cc ON t.cartaocredito_id = cc.id
LEFT JOIN pessoa _usucriacao ON t.usucriacao_id = _usucriacao.id
LEFT JOIN pessoa _usualteracao ON t.usualteracao_id = _usualteracao.id
LEFT JOIN arquivo a ON a.id = t.arquivo_id
WHERE ci.status = 'COM'
AND ci.tipo = 'REC';