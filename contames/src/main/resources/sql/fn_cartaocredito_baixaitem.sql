DROP FUNCTION IF EXISTS fn_cartaocredito_baixaitem();
CREATE OR REPLACE FUNCTION fn_cartaocredito_baixaitem() RETURNS TRIGGER AS 
$$ 
DECLARE _cartaoCredito_id BIGINT;
DECLARE _perc DOUBLE PRECISION;
DECLARE _valorDoc DOUBLE PRECISION;
BEGIN 

IF (TG_OP = 'INSERT') THEN

	SELECT l.cartaoCredito_id, lp.perc, l.valorDoc INTO _cartaoCredito_id, _perc, _valorDoc 
	FROM Lancamento l, LancamentoParcela lp
	WHERE l.id = lp.lancamento_id
	AND lp.id = NEW.lancamentoParcela_id;
	
	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.status = 'BXD' AND COALESCE(NEW.isLimite, false) THEN
		
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	
		IF _cartaoCredito_id IS NOT NULL THEN
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		END IF;
		
	END IF;
	
ELSIF (TG_OP = 'UPDATE') THEN
	
	SELECT l.cartaoCredito_id, lp.perc, l.valorDoc INTO _cartaoCredito_id, _perc, _valorDoc
	FROM Lancamento l, LancamentoParcela lp
	WHERE l.id = lp.lancamento_id
	AND lp.id = NEW.lancamentoParcela_id;

	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.status = 'BXD' AND NEW.status != 'COM' AND COALESCE(OLD.isLimite, false) THEN
		
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id AND COALESCE(limite, 0) > 0;
		
		IF _cartaoCredito_id IS NOT NULL THEN
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		END IF;
	
	END IF;

	IF NEW.cartaoCredito_id IS NOT NULL AND NEW.status = 'BXD' AND OLD.status != 'COM' AND COALESCE(NEW.isLimite, false) THEN
		
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + NEW.valor WHERE id = NEW.cartaoCredito_id AND COALESCE(limite, 0) > 0;
	
		IF _cartaoCredito_id IS NOT NULL THEN
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		END IF;
		
	END IF;
	
ELSIF (TG_OP = 'DELETE') THEN

	SELECT l.cartaoCredito_id, lp.perc, l.valorDoc INTO _cartaoCredito_id, _perc, _valorDoc
	FROM Lancamento l, LancamentoParcela lp
	WHERE l.id = lp.lancamento_id
	AND lp.id = OLD.lancamentoParcela_id;
	
	IF OLD.cartaoCredito_id IS NOT NULL AND OLD.status = 'BXD' AND COALESCE(OLD.isLimite, false) THEN
		
		UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) - OLD.valor WHERE id = OLD.cartaoCredito_id AND COALESCE(limite, 0) > 0;
		
		IF _cartaoCredito_id IS NOT NULL THEN
			UPDATE CartaoCredito SET limiteUtilizado = COALESCE(limiteUtilizado, 0) + (_valorDoc * _perc) WHERE id = _cartaoCredito_id AND COALESCE(limite, 0) > 0;
		END IF;
	
	END IF;
		
END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_cartaocredito_baixaitem ON baixaitem CASCADE;
CREATE TRIGGER tg_cartaocredito_baixaitem
	AFTER INSERT OR UPDATE OR DELETE ON baixaitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_cartaocredito_baixaitem();