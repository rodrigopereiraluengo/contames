DROP FUNCTION IF EXISTS fn_lancamentoparcela_baixaitem_cheque();
CREATE OR REPLACE FUNCTION fn_lancamentoparcela_baixaitem_cheque() RETURNS TRIGGER AS 
$$ 
BEGIN 
	
	IF 
		NOT EXISTS(SELECT id FROM LancamentoParcela WHERE cheque_id = OLD.cheque_id)
	AND
		NOT EXISTS(SELECT id FROM BaixaItem WHERE cheque_id = OLD.cheque_id)
	THEN
		DELETE FROM Cheque WHERE id = OLD.cheque_id;
	END IF;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_lancamentoparcela_cheque ON lancamentoparcela CASCADE;
CREATE TRIGGER tg_lancamentoparcela_cheque
	AFTER DELETE ON lancamentoparcela
	FOR EACH ROW
	EXECUTE PROCEDURE fn_lancamentoparcela_baixaitem_cheque();
	
DROP TRIGGER IF EXISTS tg_baixaitem_cheque ON baixaitem CASCADE;
CREATE TRIGGER fn_lancamentoparcela_baixaitem_cheque
	AFTER DELETE ON baixaitem
	FOR EACH ROW
	EXECUTE PROCEDURE fn_lancamentoparcela_baixaitem_cheque();