CREATE OR REPLACE FUNCTION mindatacompensacao(bigint)
  RETURNS date AS
$BODY$
DECLARE min_data DATE;
BEGIN

	SELECT MAX(_data) + integer '1' INTO min_data FROM Compensacao WHERE conta_id = $1;
	
	IF min_data IS NULL THEN
		SELECT MIN(m._data) INTO min_data
		FROM(
			SELECT 
				COALESCE(c._data, b._data) AS _data
			FROM BaixaItem bi
			INNER JOIN Baixa b ON bi.baixa_id = b.id
			LEFT JOIN Cheque c ON bi.cheque_id = c.id
			WHERE bi.conta_id = $1 AND bi.status = 'BXD'
			UNION SELECT
			 	t._data
			FROM Transferencia t
			WHERE (t.conta_id = $1 AND t.statusOrigem = 'LAN' OR t.destino_id = $1 AND t.statusDestino = 'LAN')
		) m;
	END IF;
	
	/*IF to_char((date_trunc('week', min_data)::date),'Day') = 'Saturday' THEN
		min_data = min_data + integer '2'; 
	END IF;
	
	IF to_char((date_trunc('week', min_data)::date),'Day') = 'Sunday' THEN
		min_data = min_data + integer '1';
	END IF;*/
	
	RETURN min_data;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION mindatacompensacao(bigint)
  OWNER TO postgres;