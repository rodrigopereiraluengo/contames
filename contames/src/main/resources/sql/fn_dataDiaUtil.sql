CREATE OR REPLACE FUNCTION fn_dataDiaUtil(DATE) RETURNS DATE AS $$
BEGIN
	RETURN CASE 
		WHEN date_part('dow', $1) = 0/* Sunday */ THEN $1::DATE - ('2 days')::INTERVAL
		WHEN date_part('dow', $1) = 6/* Saturday */ THEN $1::DATE - ('1 days')::INTERVAL
		ELSE $1
		END;
END;
$$
LANGUAGE plpgsql;