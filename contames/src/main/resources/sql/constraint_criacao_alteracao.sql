ALTER TABLE Agenda DROP CONSTRAINT fk_agenda_usualteracao_id;
ALTER TABLE Agenda ADD CONSTRAINT fk_agenda_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Agenda DROP CONSTRAINT fk_agenda_usucriacao_id;
ALTER TABLE Agenda ADD CONSTRAINT fk_agenda_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_usualteracao_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Arquivo DROP CONSTRAINT fk_arquivo_usucriacao_id;
ALTER TABLE Arquivo ADD CONSTRAINT fk_arquivo_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Baixa DROP CONSTRAINT fk_baixa_usualteracao_id;
ALTER TABLE Baixa ADD CONSTRAINT fk_baixa_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Baixa DROP CONSTRAINT fk_baixa_usucriacao_id;
ALTER TABLE Baixa ADD CONSTRAINT fk_baixa_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cartaocredito DROP CONSTRAINT fk_cartaocredito_usualteracao_id;
ALTER TABLE Cartaocredito ADD CONSTRAINT fk_cartaocredito_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cartaocredito DROP CONSTRAINT fk_cartaocredito_usucriacao_id;
ALTER TABLE Cartaocredito ADD CONSTRAINT fk_cartaocredito_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_usualteracao_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Cheque DROP CONSTRAINT fk_cheque_usucriacao_id;
ALTER TABLE Cheque ADD CONSTRAINT fk_cheque_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Compensacao DROP CONSTRAINT fk_compensacao_usualteracao_id;
ALTER TABLE Compensacao ADD CONSTRAINT fk_compensacao_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Compensacao DROP CONSTRAINT fk_compensacao_usucriacao_id;
ALTER TABLE Compensacao ADD CONSTRAINT fk_compensacao_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Conta DROP CONSTRAINT fk_conta_usualteracao_id;
ALTER TABLE Conta ADD CONSTRAINT fk_conta_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Conta DROP CONSTRAINT fk_conta_usucriacao_id;
ALTER TABLE Conta ADD CONSTRAINT fk_conta_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Descricao DROP CONSTRAINT fk_descricao_usualteracao_id;
ALTER TABLE Descricao ADD CONSTRAINT fk_descricao_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Descricao DROP CONSTRAINT fk_descricao_usucriacao_id;
ALTER TABLE Descricao ADD CONSTRAINT fk_descricao_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Item DROP CONSTRAINT fk_item_usualteracao_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Item DROP CONSTRAINT fk_item_usucriacao_id;
ALTER TABLE Item ADD CONSTRAINT fk_item_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Lancamento DROP CONSTRAINT fk_lancamento_usualteracao_id;
ALTER TABLE Lancamento ADD CONSTRAINT fk_lancamento_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Lancamento DROP CONSTRAINT fk_lancamento_usucriacao_id;
ALTER TABLE Lancamento ADD CONSTRAINT fk_lancamento_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Perfil DROP CONSTRAINT fk_perfil_usualteracao_id;
ALTER TABLE Perfil ADD CONSTRAINT fk_perfil_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Perfil DROP CONSTRAINT fk_perfil_usucriacao_id;
ALTER TABLE Perfil ADD CONSTRAINT fk_perfil_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Pessoa DROP CONSTRAINT fk_pessoa_usualteracao_id;
ALTER TABLE Pessoa ADD CONSTRAINT fk_pessoa_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Pessoa DROP CONSTRAINT fk_pessoa_usucriacao_id;
ALTER TABLE Pessoa ADD CONSTRAINT fk_pessoa_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Talaocheque DROP CONSTRAINT fk_talaocheque_usualteracao_id;
ALTER TABLE Talaocheque ADD CONSTRAINT fk_talaocheque_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Talaocheque DROP CONSTRAINT fk_talaocheque_usucriacao_id;
ALTER TABLE Talaocheque ADD CONSTRAINT fk_talaocheque_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Transferencia DROP CONSTRAINT fk_transferencia_usualteracao_id;
ALTER TABLE Transferencia ADD CONSTRAINT fk_transferencia_usualteracao_id FOREIGN KEY (usualteracao_id)REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE Transferencia DROP CONSTRAINT fk_transferencia_usucriacao_id;
ALTER TABLE Transferencia ADD CONSTRAINT fk_transferencia_usucriacao_id FOREIGN KEY (usucriacao_id) REFERENCES pessoa (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;