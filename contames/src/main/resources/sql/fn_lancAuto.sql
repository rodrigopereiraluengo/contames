CREATE OR REPLACE FUNCTION fn_lancAuto() RETURNS INT AS $$
DECLARE
	_item RECORD;
	_lancamento_id BIGINT;
	_lancamentoItem_id BIGINT;
	_lancamentoParcela_id BIGINT;
BEGIN
	FOR _item IN SELECT 
		i.id,
		fn_dataDiaUtil(fn_diaVencimento(i.diaVencimento)) _data,
		i.meio,
		i.tipo,
		i.valor,
		i.cliente_id,
		i.favorecido_id,
		i.conta_id,
		i.cartaocredito_id,
		i.islimite
	FROM Item i
	WHERE i.isLancAuto
	AND i.status = 'ATI'
	AND fn_dataDiaUtil(fn_diaVencimento(i.diaVencimento)) = fn_dataDiaUtil(current_date)
	AND NOT EXISTS(
		SELECT li.id 
		FROM LancamentoItem li, 
		Lancamento l, 
		LancamentoParcela lp 
		WHERE li.item_id = i.id
		AND l.id = li.lancamento_id
		AND lp.lancamento_id = li.lancamento_id
		AND date_part('month', lp._data) = date_part('month', current_date)
		AND date_part('year', lp._data) = date_part('year', current_date)
		AND l.status IN('LAN', 'BXD', 'COM')
	) LOOP

	/* Lancamento */
	_lancamento_id = nextval('lancamento_seq');
	INSERT INTO Lancamento(
		id, 
		cliente_id, 
		_data, 
		favorecido_id, 
		status, 
		tipo, 
		totalLancamentoItemQuant, 
		totalLancamentoItemValorUnit, 
		totalLancamentoItemValor, 
		totalLancamentoItemDesconto,
		totalLancamentoItem,
		quantLancamentoItem,
		totalLancamentoParcela,
		quantLancamentoParcela,
		totalLancamentoRateio,
		quantLancamentoRateio,
		totalLancamentoFatura,
		quantLancamentoFatura,
		criacao
	) VALUES (
		_lancamento_id,
		_item.cliente_id,
		current_date,
		_item.favorecido_id,
		'LAN',
		_item.tipo,
		1,
		_item.valor,
		_item.valor,
		0,
		_item.valor,
		1,
		_item.valor,
		1,
		0,
		0,
		0,
		0,
		current_timestamp
	);

	/* LancamentoItem */
	_lancamentoItem_id = nextval('lancamentoitem_seq');
	INSERT INTO LancamentoItem(
		id,
		lancamento_id,
		item_id,
		isLancAuto,
		seq,
		quant,
		valorUnit,
		valor,
		desconto,
		total,
		perc
	) VALUES (
		_lancamentoItem_id,
		_lancamento_id,
		_item.id,
		true,
		0,
		1,
		_item.valor,
		_item.valor,
		0,
		_item.valor,
		1
	);
	
	/* LancamentoParcela */
	_lancamentoParcela_id = nextval('lancamentoparcela_seq');
	INSERT INTO LancamentoParcela(
		id,
		lancamento_id,
		seq,
		_data,
		meio,
		cartaoCredito_id,
		isLimite,
		conta_id, 
		valor,
		perc,
		tipo,
		status
	) VALUES (
		_lancamentoParcela_id,
		_lancamento_id,
		0,
		_item._data,
		_item.meio,
		_item.cartaoCredito_id,
		_item.isLimite,
		_item.conta_id,
		_item.valor,
		1,
		_item.tipo,
		'LAN'
	);
	
	END LOOP;
RETURN 0;
END;
$$
LANGUAGE plpgsql;