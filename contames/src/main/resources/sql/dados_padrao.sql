CREATE OR REPLACE FUNCTION fn_dados_padrao() RETURNS TRIGGER AS 
$$ 
DECLARE 
	banco_seq BIGINT;
	cat_seq BIGINT;

BEGIN
	/* Verifica se eh Cliente */
	IF NEW.cliente_id IS NULL THEN

		banco_seq := nextval('descricao_seq');
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(banco_seq, 'PESSOA_CATEGORIA', 'Banco', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'Bradesco', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'Itaú Unibanco', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'Santander', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'Banco do Brasil', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'Caixa Econômica Federal', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'Citibank', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'ABN AMRO', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		INSERT INTO Pessoa(id, pessoaTipo, razaoSocial, categoria_id, status, cliente_id, usucriacao_id, criacao) VALUES(nextval('pessoa_seq'), 'J', 'HSBC', banco_seq, 'ATI', NEW.id, NEW.id, CURRENT_TIMESTAMP);
		
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'American Express', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Aura', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Diners Club', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Hipercard', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Master Card', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'Sorocred', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'VISA', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		INSERT INTO Descricao(id, descricaoTipo, nome, status, cliente_id, criacao, usucriacao_id) VALUES(nextval('descricao_seq'), 'CARTAO_BANDEIRA', 'BNDS', 'ATI', NEW.id, CURRENT_TIMESTAMP, NEW.id);
		
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 0, 'CAT', 'Alimentação', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Restaurante', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Supermecado', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 1, 'CAT', 'Animal de estimação', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Alimentação', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Suprimentos', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Veterinário', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 2, 'CAT', 'Automóvel', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Combustível', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Manutenção', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 3, 'CAT', 'Contas a pagar', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Água e esgoto', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Aluguel', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Condomínio', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Energia Elétrica', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Gás', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Telefone', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Tv por assinatura', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Internet', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 4, 'CAT', 'Educação', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Livros', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Mensalidade escolar', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Outros gastos', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 5, 'CAT', 'Impostos', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Imposto de renda', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','IPTU', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','IPVA', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Outros impostos', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 6, 'CAT', 'Lazer', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Artigos esportivos', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Brinquedos e jogos', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Cinema', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Diversão', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Eventos culturais', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Livros e revistas', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 7, 'CAT', 'Presentes', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Aniversário', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Outros', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Retirada em dinheiro', 'DES', NULL, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 8, 'CAT', 'Saúde', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Dentista', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Hospital', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Médico', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Ótica', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Remédios', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 9, 'CAT', 'Seguro', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Automóvel', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Do locador /do locatário', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Saúde', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Vida', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 10, 'CAT', 'Taxas bancárias', 'DES', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Juros pagos', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Taxa de serviço', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Outras taxas', 'DES', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		
		
		
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 0, 'CAT', 'Outros rendimentos', 'REC', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Ações trabalhistas', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Devolução de impostos', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Loterias', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Pensão recebida para filho', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Presentes recebidos', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Seguro desemprego', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 1, 'CAT', 'Rendimento de aposentadoria', 'REC', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Benefícios do INSS', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Décimo terceira', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Pensões', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 2, 'CAT', 'Rendimento de investimento', 'REC', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Dividendos', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Ganhos de capital', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Juros', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Juros não-tributáveis', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
		cat_seq := nextval('classificacao_seq');
		INSERT INTO Classificacao(id, _index, nivel, nome, tipo, classificacao_id, cliente_id) VALUES(cat_seq, 3, 'CAT', 'Salários e ordenado', 'REC', NULL, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Bônus', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Comissão', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Contrib. empregador', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Horas extras', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Pagamento bruto', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
		INSERT INTO Item(id, status, nome, tipo, classificacao_id, criacao, usucriacao_id, cliente_id) VALUES(nextval('item_seq'), 'ATI','Pagamento líquido', 'REC', cat_seq, CURRENT_TIMESTAMP, NEW.id, NEW.id);
				
	END IF;
	RETURN NEW;
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_dados_padrao ON pessoa CASCADE;
CREATE TRIGGER tg_dados_padrao
	AFTER INSERT ON pessoa
	FOR EACH ROW
	EXECUTE PROCEDURE fn_dados_padrao();