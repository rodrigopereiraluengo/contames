DROP FUNCTION IF EXISTS fn_compensacao_delete();
CREATE OR REPLACE FUNCTION fn_compensacao_delete() RETURNS TRIGGER AS 
$$ 
DECLARE _ultimacompensacao_id BIGINT;

BEGIN 
	
	SELECT id INTO _ultimacompensacao_id FROM Compensacao WHERE conta_id = OLD.conta_id ORDER BY id DESC LIMIT 1;
	
	UPDATE Conta SET ultimaCompensacao_id = _ultimacompensacao_id WHERE id = OLD.conta_id;

RETURN NULL; 
END;
$$ 
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_compensacao_delete ON compensacao CASCADE;
CREATE TRIGGER tg_compensacao_delete
	AFTER DELETE ON compensacao
	FOR EACH ROW
	EXECUTE PROCEDURE fn_compensacao_delete();