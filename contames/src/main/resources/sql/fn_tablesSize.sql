CREATE OR REPLACE FUNCTION fn_tableSize(p_cliente_id BIGINT) RETURNS NUMERIC AS $$
DECLARE r_size NUMERIC;
BEGIN

	SELECT CASE WHEN SUM(size) > 0 THEN (SUM(size) * 2.5) / 1024 / 1024 ELSE 0 END INTO r_size
	FROM (
	SELECT COALESCE(sum(pg_column_size(a)), 0) size
	FROM Arquivo a
	WHERE a.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(b)), 0) size
	FROM Baixa b
	WHERE b.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(bi)) , 0) size
	FROM BaixaItem bi, Baixa b
	WHERE bi.baixa_id = b.id
	AND b.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(cc)), 0) size
	FROM CartaoCredito cc
	WHERE cc.cliente_id = p_cliente_id 
	
	UNION SELECT COALESCE(sum(pg_column_size(ch)), 0) size
	FROM Cheque ch
	WHERE ch.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(cl)), 0) size
	FROM Classificacao cl
	WHERE cl.cliente_id = p_cliente_id 
	
	UNION SELECT COALESCE(sum(pg_column_size(co)), 0) size
	FROM Compensacao co
	WHERE co.cliente_id = p_cliente_id 
	
	UNION SELECT COALESCE(sum(pg_column_size(coi)), 0) size
	FROM CompensacaoItem coi, Compensacao co
	WHERE co.id = coi.compensacao_id AND co.cliente_id = 1 
	
	UNION SELECT COALESCE(sum(pg_column_size(c)), 0) size
	FROM Conta c
	WHERE c.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(d)), 0) size
	FROM Descricao d
	WHERE d.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(i)), 0) size
	FROM Item i
	WHERE i.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(l)), 0) size
	FROM Lancamento l
	WHERE l.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(lf)), 0) size
	FROM LancamentoFatura lf, Lancamento l
	WHERE lf.lancamento_id = l.id AND l.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(li)), 0) size
	FROM LancamentoItem li, Lancamento l
	WHERE li.lancamento_id = l.id AND l.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(lp)), 0) size
	FROM LancamentoParcela lp, Lancamento l
	WHERE lp.lancamento_id = l.id AND l.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(lr)), 0) size
	FROM LancamentoRateio lr, Lancamento l
	WHERE lr.lancamento_id = l.id AND l.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(p)), 0) size
	FROM Pessoa p
	WHERE p.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(tc)), 0) size
	FROM TalaoCheque tc, Conta c
	WHERE tc.conta_id = c.id AND c.cliente_id = p_cliente_id
	
	UNION SELECT COALESCE(sum(pg_column_size(t)), 0) size
	FROM Transferencia t, Conta c
	WHERE t.conta_id = c.id AND c.cliente_id = p_cliente_id
	
) AS s;
RETURN r_size;
END;
$$
LANGUAGE plpgsql;