<html>
	<body style="font-family: 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif">
		
		
		${msg}
		${signature}
		<br />
		<div>
			<a title="${siteTitle}" href="${siteUrl}"><img alt="${siteTitle}" src="${siteUrl}/resources/img/logo-email.png"></a>
		</div>
		<br />
		<br />
		${noreply}
		
			
	</body>
</html>