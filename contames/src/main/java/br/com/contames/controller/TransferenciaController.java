package br.com.contames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.hibernate.validator.constraints.NotBlank;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Transferencia;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.TransferenciaTipo;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.ChequeRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.TalaoChequeRepository;
import br.com.contames.repository.TransferenciaRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class TransferenciaController {
	
	private final Result result;
	private final Validator validator;
	private final TransferenciaRepository transferenciaRepository;
	private final ContaRepository contaRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final ArquivoRepository arquivoRepository;
	private final TalaoChequeRepository talaoChequeRepository;
	private final ChequeRepository chequeRepository;
	private final UsuarioLogged usuarioLogged;
	private final Environment environment;
	private final Localization localization;
	private final Settings settings;
	private final HttpSession session;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	
	public TransferenciaController(Result result, Validator validator, TransferenciaRepository transferenciaRepository, ContaRepository contaRepository, CartaoCreditoRepository cartaoCreditoRepository, ArquivoRepository arquivoRepository, TalaoChequeRepository talaoChequeRepository, ChequeRepository chequeRepository, UsuarioLogged usuarioLogged, Environment environment, Localization localization, Settings settings, HttpSession session, PlanoRecurso planoRecurso, ArquivoSession arquivoSession) {
		this.result = result;
		this.validator = validator;
		this.transferenciaRepository = transferenciaRepository;
		this.contaRepository = contaRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.arquivoRepository = arquivoRepository;
		this.talaoChequeRepository = talaoChequeRepository;
		this.chequeRepository = chequeRepository;
		this.usuarioLogged = usuarioLogged;
		this.environment = environment;
		this.localization = localization;
		this.settings = settings;
		this.session = session;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
	}

	
	@Get("/lancamentos/transferencias")
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page) { 
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
				
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("t.id, ");
		sqlBuilder.append("t._data, ");
		sqlBuilder.append("CASE WHEN t.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' WHEN t.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' WHEN t.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' WHEN t.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' END status, ");
		sqlBuilder.append("t.status _status, ");
		sqlBuilder.append("CASE WHEN t.tipo = 'CON' THEN '" + localization.getMessage("TransferenciaTipo.CON") + "' ELSE '" + localization.getMessage("TransferenciaTipo.CCR") + "' END tipo, ");
		sqlBuilder.append("COALESCE(c.nome || COALESCE(' " + localization.getMessage("MeioPagamento.CHQ")  + " ' || ch.numero, ''), cc.nome) origem, ");
		sqlBuilder.append("d.nome destino, ");
		sqlBuilder.append("t.valor, ");
		sqlBuilder.append("a.id AS arquivo_id, ");
		sqlBuilder.append("a.nome AS arquivo_nome, ");
		sqlBuilder.append("t.obs ");
		sqlBuilder.append("FROM Transferencia t ");
		sqlBuilder.append("LEFT JOIN Conta c ON t.conta_id = c.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON t.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN Cheque ch ON t.cheque_id = ch.id ");
		sqlBuilder.append("INNER JOIN Conta d ON t.destino_id = d.id ");
		sqlBuilder.append("LEFT JOIN Arquivo a ON t.arquivo_id = a.id ");
		sqlBuilder.append("WHERE t.cliente_id = ?1");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("CAST(t._data AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN t.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' WHEN t.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' WHEN t.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' END)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN t.tipo = 'CON' THEN '" + localization.getMessage("TransferenciaTipo.CON") + "' ELSE '" + localization.getMessage("TransferenciaTipo.CCR") + "' END)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(c.nome, cc.nome))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(d.nome)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(t.valor AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(t.obs)) ILIKE ?3 ");
			sqlBuilder.append(")");
		}
		
		Query countQuery = transferenciaRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Transferencia");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count.intValue()) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
				
		Query query = transferenciaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("transferenciaList", query.getResultList());
		}catch(NoResultException e) {
			
		}
				
		result.forwardTo("/WEB-INF/jsp/transferencia/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(Long id) {
		
		// Gera viewid
		String viewid = StringHelper.createViewId();
		
		Transferencia transferencia = null;
		
		if(id == null) {
			
			transferencia = new Transferencia();
			transferencia.setIsLimite(true);
			
			if(planoRecurso.getBancoDadosDisp() == 0) {
				validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
			}
			
		} else {
			transferencia = transferenciaRepository.find(id);
			
			if(transferencia == null) {
				validator.add(new I18nMessage("transferencia", "transferencia.notFound"));
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		arquivoSession.setAquivoList(viewid, transferencia.getArquivoList());
		
		result
			.include("viewid", viewid)
			.include("transferencia", transferencia)
			.include("contaList", contaRepository.findAllActive())
			.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive());
	}
	
	
	@Post
	public void cadastro(
			@NotNull(message = "{transferencia.notNull}") 
			Transferencia transferencia,
			@NotBlank(message = "{viewid.notBlank}")
			String viewid,
			Boolean isConfirmSaldoConta,
			Boolean isConfirmSaldoCartaoCredito) {
		
		if(transferencia != null && StringUtils.isNotBlank(viewid)) {
			
			Transferencia __transferencia = null;
			if(transferencia.getId() != null) {
				__transferencia = transferenciaRepository.find(transferencia.getId());
			}
			
			if(__transferencia != null && (LancamentoStatus.COM.equals(__transferencia.getStatus()) || LancamentoStatus.COM.equals(__transferencia.getStatusOrigem()) || LancamentoStatus.COM.equals(__transferencia.getStatusDestino()))) {
				transferencia.setStatusOrigem(__transferencia.getStatusOrigem());
				transferencia.setStatusDestino(__transferencia.getStatusDestino());
				transferencia.setStatus(__transferencia.getStatus());
				transferencia.setTipo(__transferencia.getTipo());
				transferencia.setConta(__transferencia.getConta());
				transferencia.setCheque(__transferencia.getCheque());
				transferencia.setCartaoCredito(__transferencia.getCartaoCredito());
				transferencia.setDestino(__transferencia.getDestino());
				transferencia.setValor(__transferencia.getValor());
			}
			
			if(transferencia.getConta() != null && transferencia.getConta().getId() == null) {
				transferencia.setConta(null);
			}
			
			if(transferencia.getCheque() != null && transferencia.getCheque().getNumero() == null) {
				transferencia.setCheque(null);
			}
			if(transferencia.getCheque() != null) {
				
				if(transferencia.getCheque().getTalaoCheque() != null && transferencia.getCheque().getTalaoCheque().getId() != null) {
					transferencia.getCheque().setTalaoCheque(talaoChequeRepository.find(transferencia.getCheque().getTalaoCheque().getId()));
				}
				
				if(transferencia.getCheque().getTalaoCheque() == null) {
					validator.add(new I18nMessage("cheque", "cheque.talaoCheque.notNull"));
				}
				
				transferencia.getCheque().setCliente(usuarioLogged.getCliente());
				transferencia.getCheque().setStatus(transferencia.getStatus());
			}
			
			if(transferencia.getCartaoCredito() != null && transferencia.getCartaoCredito().getId() == null) {
				transferencia.setCartaoCredito(null);
			}
			
			if(transferencia.getDestino() != null && transferencia.getDestino().getId() == null) {
				transferencia.setDestino(null);
			}
			
			validator.validateProperties(transferencia, "data", "status", "tipo", "destino", "valor", "obs");
			
			if(transferencia.getTipo() != null) {
				
				// Origem Conta
				if(transferencia.getTipo().equals(TransferenciaTipo.CON)) {
					
					transferencia.setCartaoCredito(null);
					transferencia.setIsLimite(null);
					
					if(transferencia.getConta() != null) {
						transferencia.setConta(contaRepository.find(transferencia.getConta().getId()));
					}
					
					// Verifica se a conta foi selecionada
					if(transferencia.getConta() == null) {
						validator.add(new I18nMessage("conta", "conta.notNull"));
					} else { 
						
						// Verifica se destino eh igual a origem
						if(transferencia.getDestino() != null && transferencia.getDestino().equals(transferencia.getConta())) {
							validator.add(new I18nMessage("destino", "destino.notEqual.conta"));
						}
						
						
						// Verifica se a conta possui saldo
						if(transferencia.getValor() != null) {
						
							if(isConfirmSaldoConta != null && !isConfirmSaldoConta) {
								Conta conta = contaRepository.find(transferencia.getConta().getId());
								if(conta.getSaldoUtilizavel() < transferencia.getValor()) {
									validator.add(new I18nMessage("isConfirmSaldoConta", "confirm.conta.saldoInsuficiente"));
								}
							}
						}
					}
				} 
				
				// Origem Cartao de Credito
				else {
					
					transferencia.setConta(null);
					transferencia.setCheque(null);
					
					if(transferencia.getCartaoCredito() != null) {
						transferencia.setCartaoCredito(cartaoCreditoRepository.find(transferencia.getCartaoCredito().getId()));
					}
					
					// Verifica se o Cartao de Credito foi informado
					if(transferencia.getCartaoCredito() == null) {
						validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notNull"));
					} else {
						
						// Verifica se o cartao possui saldo
						if(transferencia.getValor() != null) {
							if(isConfirmSaldoCartaoCredito != null && !isConfirmSaldoCartaoCredito) {
								
								CartaoCredito cartaoCredito = cartaoCreditoRepository.find(transferencia.getCartaoCredito().getId());
								Double limiteDisponivel = cartaoCredito.getLimiteDisponivel();
								Transferencia _transferencia = null;
								if(transferencia.getId() != null) {
									_transferencia = transferenciaRepository.find(transferencia.getId());
									if(limiteDisponivel != null && _transferencia != null && cartaoCredito.equals(_transferencia.getCartaoCredito())) {
										limiteDisponivel += _transferencia.getValor();
									}
								}
								if(limiteDisponivel < transferencia.getValor()) {
									validator.add(new I18nMessage("isConfirmSaldoCartaoCredito", "confirm.cartaoCredito.saldoLimiteInsuficiente"));
								}
							}
						}
					}
					
				}
			}
			
		
			// Cheque
			if(transferencia.getCheque() != null) {
				transferencia.getCheque().setValor(transferencia.getValor());
				chequeRepository.save(transferencia.getCheque());
			}
			
		
		
			// Arquivo
			if(transferencia.getArquivo() != null && StringUtils.isBlank(transferencia.getArquivo().getNome())) {
				transferencia.setArquivo(null);
			}
			if(transferencia.getArquivo() != null) {
				arquivoRepository.save(transferencia);
			}
		
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("transferenciaCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		
		transferenciaRepository.save(transferencia);
		
		// Recupera Lista de Arquivos para Salvar
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		for(Arquivo arquivo : arquivoList) {
			arquivo.setTransferencia(transferencia);
			arquivoRepository.save(arquivo);
		}
		
		// Recupera Lista de Arquivos para Deletar
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		for(Arquivo arquivo : arquivoDeleteList) {
			arquivoRepository.delete(arquivo);
		}
		arquivoSession.clear(viewid);
		
		if(transferencia.getCheque()!= null) {
			transferencia.getCheque().setTransferencia(transferencia);
		}
		
		
		cartaoCreditoRepository.atualizaLimiteDisponivel();
		
		
		result.use(Results.jsonp()).withCallback("transferenciaCadastroSuccess").withoutRoot().from(transferencia).exclude("cliente").serialize();
		
	}
	
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		List<Transferencia> transferenciaList = transferenciaRepository.findAll(ids);
		
		for(Transferencia transferencia : transferenciaList) {
			if(LancamentoStatus.COM.equals(transferencia.getStatus())) {
				validator.add(new I18nMessage("transferencia_" + transferencia.getId(), "transferencia.delete.status.COM"));
			}
			
			if(LancamentoStatus.COM.equals(transferencia.getStatusOrigem())) {
				validator.add(new I18nMessage("transferencia_" + transferencia.getId(), "transferencia.delete.statusOrigem.COM"));
			}
			
			if(LancamentoStatus.COM.equals(transferencia.getStatusDestino())) {
				validator.add(new I18nMessage("transferencia_" + transferencia.getId(), "transferencia.delete.statusDestino.COM"));
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		for(Transferencia transferencia : transferenciaList) {
			if(transferencia.getArquivo() != null) {
				arquivoRepository.delete(transferencia.getArquivo());
				transferencia.setArquivo(null);
			}
			
			if(transferencia.getCheque() != null) {
				transferencia.getCheque().setStatus(LancamentoStatus.CAN);
				transferencia.getCheque().setTransferencia(null);
				chequeRepository.save(transferencia.getCheque());
			}
		}
		
		transferenciaRepository.delete(transferenciaList);
		
		cartaoCreditoRepository.atualizaLimiteDisponivel();
		result.use(Results.jsonp()).withCallback("transferenciaExcluirSuccess").withoutRoot().from(ids).serialize();
		
	}
	
}
