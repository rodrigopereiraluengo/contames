package br.com.contames.controller;

import org.hibernate.validator.constraints.NotBlank;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.cep.Address;
import br.com.caelum.vraptor.cep.AddressFinder;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;

@Resource
public class CepController {

	private final AddressFinder addressFinder;
	private final Validator validator;
	private final Result result;
	
	public CepController(AddressFinder addressFinder, Validator validator, Result result) {
		this.addressFinder = addressFinder;
		this.validator = validator;
		this.result = result;
	}
	
	@Get
	public void buscar(@NotBlank(message = "{buscarCep.notBlank}") String cep, @NotBlank(message = "{errorFunction.notBlank}")String errorFunction) {
		
		validator.onErrorUse(Results.jsonp()).withCallback(errorFunction).withoutRoot().from(validator.getErrors()).serialize();
		
		Address address = addressFinder.findAddressByZipCode(cep).asAddressObject();
		
		if(address == null || address.notFound()) {
			validator.add(new I18nMessage("buscar.cep", "buscarCep.notFound"));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback(errorFunction).withoutRoot().from(validator.getErrors()).serialize();
				
		result.use(Results.jsonp()).withCallback("successCep").withoutRoot().from(address).serialize();
		
	}
	
}
