package br.com.contames.controller;

import javax.validation.constraints.Size;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;

@Private
@Resource
public class UsuarioController {

	private final UsuarioLogged usuarioLogged;
	private final PessoaRepository pessoaRepository;
	private final Validator validator;
	private final Result result;
	
	public UsuarioController(UsuarioLogged usuarioLogged, PessoaRepository pessoaRepository, Validator validator, Result result) {
		this.usuarioLogged = usuarioLogged;
		this.pessoaRepository = pessoaRepository;
		this.validator = validator;
		this.result = result;
	}
	
	@Get
	public void atualizarSenha() { }
	
	@Post
	public void atualizarSenha(
			
			@NotBlank(message = "{senhaAtual.notBlank}")
			String senhaAtual, 
			
			@NotBlank(message = "{novaSenha.notBlank}")
			@Size(min = 6, max = 20, message = "{novaSenha.size}")
			String senha, 
			
			@NotBlank(message = "{confirmeSenha.notBlank}")
			String confirmeSenha) {
		
		// Verifica Senha Atual
		if(senhaAtual != null && !usuarioLogged.getUsuario().getSenha().equals(senhaAtual)) {
			validator.add(new I18nMessage("senhaAtual", "senhaAtual.notEqual"));
		}
		
		// Verifica se a Senha foi confirmada
		if(senha != null && confirmeSenha != null && !confirmeSenha.equals(senha)) {
			validator.add(new I18nMessage("confirmeSenha", "confirmeSenha.notEqual"));
		}
		
		// Retorna json com as mensagens de erro
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		// Salva nova Senha
		pessoaRepository.atualizaSenha(usuarioLogged.getUsuario(), senha);
		
		// Ok
		result.use(Results.json()).withoutRoot().from(StringUtils.EMPTY).serialize();
		
	}
	
}
