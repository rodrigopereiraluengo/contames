package br.com.contames.controller;

import java.util.List;

import org.joda.time.LocalDate;

import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.tasks.scheduler.Scheduled;
import br.com.contames.component.CloudFiles;
import br.com.contames.component.EmailHelper;
import br.com.contames.entity.Assinatura;
import br.com.contames.entity.Pessoa;
import br.com.contames.repository.AssinaturaRepository;
import br.com.contames.repository.PessoaRepository;

@Resource
public class ClienteCanceladoScheduleController {

	private final PessoaRepository pessoaRepository;
	private final AssinaturaRepository assinaturaRepository;
	private final Localization localization;
	private final EmailHelper emailHelper;
	private final CloudFiles cloudFiles;
	private final Result result;
	
	public ClienteCanceladoScheduleController(PessoaRepository pessoaRepository, AssinaturaRepository assinaturaRepository, Localization localization, EmailHelper emailHelper, CloudFiles cloudFiles, Result result) {
		this.pessoaRepository = pessoaRepository;
		this.assinaturaRepository = assinaturaRepository;
		this.localization = localization;
		this.emailHelper = emailHelper;
		this.cloudFiles = cloudFiles;
		this.result = result;
	}
	
	@Post @Scheduled(cron = "0 0 6 * * ?", id = "clienteCancelado")
	public void execute() {
		
		// 20 dias cancelado
		List<Assinatura> assinaturaList = assinaturaRepository.findAllCancelado20dias();
		if(assinaturaList != null && !assinaturaList.isEmpty()) {
			for(Assinatura assinatura : assinaturaList) {
				emailHelper
					.addTo(assinatura.getCliente().getEmail(), assinatura.getCliente().getDisplayNome())
					.setSubject(localization.getMessage("email.assinaturaCancelada20dias.subject", LocalDate.now().plusDays(10).toString(localization.getMessage("patternDate"))))
					.setHtmlMsg(localization.getMessage("email.assinaturaCancelada20dias.htmlMsg", assinatura.getCliente().getDisplayNome(), LocalDate.now().plusDays(10).toString(localization.getMessage("patternDate"))))
					.setTextMsg(localization.getMessage("email.assinaturaCancelada20dias.textMsg", assinatura.getCliente().getDisplayNome(), LocalDate.now().plusDays(10).toString(localization.getMessage("patternDate"))))
					.send();
			}
			
		}
		
		// 31 dias cancelado
		assinaturaList = assinaturaRepository.findAllCancelado31dias();
		if(assinaturaList != null && !assinaturaList.isEmpty()) {
			for(Assinatura assinatura : assinaturaList) {
				
				emailHelper
					.addTo(assinatura.getCliente().getEmail(), assinatura.getCliente().getDisplayNome())
					.setSubject(localization.getMessage("email.assinaturaCancelada31dias.subject"))
					.setHtmlMsg(localization.getMessage("email.assinaturaCancelada31dias.htmlMsg", assinatura.getCliente().getDisplayNome()))
					.setTextMsg(localization.getMessage("email.assinaturaCancelada31dias.textMsg", assinatura.getCliente().getDisplayNome()))
					.send();
				
				cloudFiles.deleteContainer("cliente_" + assinatura.getCliente().getId() + "_temp");
				cloudFiles.deleteContainer("cliente_" + assinatura.getCliente().getId());
				
				Pessoa cliente = assinatura.getCliente();
				pessoaRepository.delete(cliente);
				
			}
		}
		
		result.nothing();
		
	}
	
}
