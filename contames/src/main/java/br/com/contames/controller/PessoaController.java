package br.com.contames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.PessoaTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class PessoaController {
		
	private final DescricaoRepository descricaoRepository;
	private final PessoaRepository pessoaRepository;
	private final UsuarioLogged usuarioLogged;
	private final Settings settings;
	private final Localization localization;
	private final Validator validator;
	private final Result result;
	private final HttpServletRequest request;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	private final ArquivoRepository arquivoRepository;
	
	public PessoaController(DescricaoRepository descricaoRepository, PessoaRepository pessoaRepository, UsuarioLogged usuarioLogged, Settings settings, Localization localization, Validator validator, Result result, HttpServletRequest request, PlanoRecurso planoRecurso, ArquivoSession arquivoSession, ArquivoRepository arquivoRepository) {
		this.descricaoRepository = descricaoRepository;
		this.pessoaRepository = pessoaRepository;
		this.usuarioLogged = usuarioLogged;
		this.settings = settings;
		this.localization = localization;
		this.validator = validator;
		this.result = result;
		this.request = request;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
		this.arquivoRepository = arquivoRepository;
	}
	

	@Get({"/cadastros/pessoas", "/usuarios"})
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page, PessoaTipo pessoaTipo, String categoria, Status status) {
		
		boolean isUsuario = request.getRequestURI().endsWith("/usuarios");
		
		if(isUsuario && !usuarioLogged.getIsCliente()) {
			result.notFound();
		}
				
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("razaoNome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
				
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT p.id, ");
		sqlBuilder.append("CASE WHEN p.pessoaTipo = 'J' THEN '" + localization.getMessage("PessoaTipo.J") + "' ELSE '" + localization.getMessage("PessoaTipo.F") + "' END pessoaTipo, ");
		sqlBuilder.append("categ.nome categoria, ");
		sqlBuilder.append("CASE WHEN p.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN p.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END status, ");
		sqlBuilder.append("p.status _status, ");
		sqlBuilder.append("COALESCE(p.razaoSocial, p.nome) razaoNome, ");
		sqlBuilder.append("COALESCE(p.fantasia, p.apelido) fantasiaApelido, ");
		sqlBuilder.append("CASE WHEN p.sexo = 'M' THEN '" + localization.getMessage("PessoaSexo.M") + "' WHEN p.sexo = 'F' THEN '" + localization.getMessage("PessoaSexo.F") + "' ELSE '' END AS sexo, ");
		sqlBuilder.append("COALESCE(p.cnpj, p.cpf) cnpjCpf, ");
		sqlBuilder.append("COALESCE(p.inscEst, p.rg) inscEstRg, ");
		sqlBuilder.append("COALESCE(p.inscMun, p.tituloEleitor) inscMunTituloEleitor, ");
		sqlBuilder.append("p.cep, ");
		sqlBuilder.append("p.endereco, ");
		sqlBuilder.append("p.numero, ");
		sqlBuilder.append("p.complemento, ");
		sqlBuilder.append("p.bairro, ");
		sqlBuilder.append("p.cidade, ");
		sqlBuilder.append("p.uf, ");
		sqlBuilder.append("p.fone1, ");
		sqlBuilder.append("p.fone2, ");
		sqlBuilder.append("p.fone3, ");
		sqlBuilder.append("p.fone4, ");
		sqlBuilder.append("p.email, ");
		sqlBuilder.append("p.site, ");
		sqlBuilder.append("p.obs ");
		sqlBuilder.append("FROM Pessoa p ");
		sqlBuilder.append("LEFT JOIN Descricao AS categ ON p.categoria_id = categ.id ");
		sqlBuilder.append("WHERE (p.cliente_id = ?1 OR p.id = ?1)");
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append(" unaccent(LOWER(CASE WHEN p.pessoaTipo = 'J' THEN '" + localization.getMessage("PessoaTipo.J") + "' ELSE '" + localization.getMessage("PessoaTipo.F") + "' END)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(categ.nome)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(CASE WHEN p.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN p.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(COALESCE(p.razaoSocial, p.nome))) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(CASE WHEN p.sexo = 'M' THEN '" + localization.getMessage("PessoaSexo.M") + "' WHEN p.sexo = 'F' THEN '" + localization.getMessage("PessoaSexo.F") + "' ELSE '' END)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(COALESCE(p.fantasia, p.apelido))) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(COALESCE(p.cnpj, p.cpf))) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(COALESCE(p.inscEst, p.rg))) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(COALESCE(p.inscMun, p.tituloEleitor))) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.cep)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.endereco)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.numero)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.complemento)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.bairro)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.cidade)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.uf)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.fone1)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.fone2)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.fone3)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.fone4)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.email)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.site)) ILIKE ?2");
			sqlBuilder.append(" OR unaccent(LOWER(p.obs)) ILIKE ?2");
			sqlBuilder.append(") ");
		}
		
		if(pessoaTipo != null) {
			sqlBuilder.append(" AND p.pessoaTipo = ?3");
		}
		
		if(categoria != null) {
			sqlBuilder.append(" AND categ.nome = ?4");
		}
		
		if(isUsuario) {
			sqlBuilder.append(" AND p.email IS NOT NULL AND p.senha IS NOT NULL");
		}
		
		if(status != null) {
			sqlBuilder.append(" AND p.status = '" + status + "'");
		}
		
		Query countQuery = pessoaRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Pessoa");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(2, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		if(pessoaTipo != null) {
			countQuery.setParameter(3, pessoaTipo.toString());
		}
		if(categoria != null) {
			countQuery.setParameter(4, categoria);
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
				
		Query query = pessoaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(2, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		if(pessoaTipo != null) {
			query.setParameter(3, pessoaTipo.toString());
		}
		if(categoria != null) {
			query.setParameter(4, categoria);
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("requestURI", request.getRequestURI())
			.include("isUsuario", isUsuario);
		
		try {
			result.include("pessoaList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/pessoa/" + jsp + ".jsp");
	
	}
	
	
	@Get
	public void cadastro(Long id, PessoaTipo pessoaTipo, boolean isUsuario) {
		Pessoa pessoa = null;
		
		String viewid = StringHelper.createViewId();
		
		if(id == null) {
			
			// Verifica se ha disponibilidade de adicionar um novo usuario
			if(isUsuario && planoRecurso.getUsuariosDisp() == 0) {
				validator.add(new I18nMessage("plano.usuarios", "plano.usuarios.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getUsuarios()));
			} 
			
			// Verifica espaco no banco de dados
			if(planoRecurso.getBancoDadosDisp() == 0) {
				validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
			}
			
			pessoa = new Pessoa();
			pessoa.setPessoaTipo(pessoaTipo == null ? isUsuario ? PessoaTipo.F : PessoaTipo.J : pessoaTipo);
			
		} else {
			pessoa = pessoaRepository.find(id);
			
			if(pessoa == null) {
				validator.add(new I18nMessage("pessoa", "pessoa.notFound"));
			}
			
			arquivoSession.setAquivoList(viewid, pessoa.getArquivoList());
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		
		
		
		
		result
			.include("pessoa", pessoa)
			.include("isUsuario", isUsuario)
			.include("viewid", viewid)
			.include("categoriaList", descricaoRepository.findAllActive(DescricaoTipo.PESSOA_CATEGORIA));
		
	}
	
	
	@Post
	public void cadastro(@NotNull(message = "{pessoa.notNull}") Pessoa pessoa, boolean isUsuario, String viewid) {
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		
		// Verifica se a Categoria eh null
		if(pessoa.getCategoria() != null && pessoa.getCategoria().getId() == null) {
			pessoa.setCategoria(null);
		}
		
		
		// Busca categoria para ver se e num categoria deste cliente
		if(pessoa.getCategoria() != null) {
			pessoa.setCategoria(descricaoRepository.find(pessoa.getCategoria().getId(), DescricaoTipo.PESSOA_CATEGORIA));
		}
		
		
		// Verifica se ej pessoa Juridica
		if(PessoaTipo.J.equals(pessoa.getPessoaTipo())) {
			if(StringUtils.isBlank(pessoa.getRazaoSocial())) {
				validator.add(new I18nMessage("razaoSocial", "pessoa.razaoSocial.notBlank"));
			}
			
			pessoa.setApelido(null);
			pessoa.setCpf(null);
			pessoa.setRg(null);
			pessoa.setTituloEleitor(null);
			
		} else {
			if(StringUtils.isBlank(pessoa.getNome())) {
				validator.add(new I18nMessage("razaoSocial", "nome.notBlank"));
			}
			
			pessoa.setFantasia(null);
			pessoa.setCnpj(null);
			pessoa.setInscEst(null);
			pessoa.setInscMun(null);
		}
		
		
		// Verifica se o Usuario eh o Cliente e se o Usuario e diferente da Pessoa
		if(usuarioLogged.getIsCliente() && !usuarioLogged.getUsuario().equals(pessoa)) {
			
			// Verifica se o acesso foi pelo Usuario
			if(isUsuario) {
				
				// Verifica Email
				if(StringUtils.isBlank(pessoa.getEmail())) {
					validator.add(new I18nMessage("email", "email.notBlank"));
				}
				
				// Verifica Senha
				if(StringUtils.isBlank(pessoa.getSenha())) {
					validator.add(new I18nMessage("senha", "senha.notBlank"));
				}
				
			}
			
			// Verifica se a Senha foi informada e o Email nao
			if(StringUtils.isNotBlank(pessoa.getSenha()) && StringUtils.isBlank(pessoa.getEmail())) {
				validator.add(new I18nMessage("email", "senha.notBlank.email.blank"));
			}
			
			// Verifica se o Email e Senhas foram informados
			if(StringUtils.isNotBlank(pessoa.getEmail()) && StringUtils.isNotBlank(pessoa.getSenha())) {
				
				// Verificar se ja nao existe um usuario com este e-mail
				Boolean exists = pessoaRepository.exists(pessoa.getEmail(), pessoa);
				if(exists) {
					validator.add(new I18nMessage("email", "email.exists", pessoa.getEmail()));
				}
			}
			
		}
		
		pessoa.setCliente(usuarioLogged.getCliente());
		pessoa.setCriacao(LocalDateTime.now());
		pessoa.setAlteracao(LocalDateTime.now());
		validator.validate(pessoa);
		
		if(pessoaRepository.exists(pessoa)) {
			validator.add(new I18nMessage("razaoSocial", "pessoa." + pessoa.getPessoaTipo() + ".exists"));
		}
		
		// Verifica se informou email e senha e se possui disponibilidade para criar usuario
		if(StringUtils.isNotBlank(pessoa.getEmail()) && StringUtils.isNotBlank(pessoa.getSenha()) && (planoRecurso.getUsuarios() - planoRecurso.getUsuarios(pessoa.getId())) == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.usuarios.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getUsuarios()));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("pessoaCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		pessoaRepository.save(pessoa, isUsuario);
		
		// Recupera List de Arquivo
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		for(Arquivo arquivo : arquivoList) {
			arquivo.setPessoa(pessoa);
			arquivoRepository.save(arquivo);
		}
		
		// Recupera List de Arquivo para excluir
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		for(Arquivo arquivo : arquivoDeleteList) {
			arquivoRepository.delete(arquivo);
		}
		
		// Limpa ArquivoSession
		arquivoSession.clear(viewid);
		
		result.use(Results.jsonp()).withCallback("pessoaCadastroSuccess").withoutRoot().from(pessoa).exclude("cliente").serialize();
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		for(int i = 0; i < ids.size(); i++) {
			
			if(usuarioLogged.getCliente().getId().equals(ids.get(i))) {
				validator.add(new I18nMessage("pessoa_" + ids.get(i), "pessoa.eh.cliente"));
			}
			
			// Verifica se existe lancamento com esta pessoa
			if(pessoaRepository.existsLancamento(ids.get(i))) {
				validator.add(new I18nMessage("pessoa_" + ids.get(i), "pessoa.lancamento.exists"));
			}
			
			// Verifica se existe rateio em lancamento
			if(pessoaRepository.existsLancamentoRateio(ids.get(i))) {
				validator.add(new I18nMessage("pessoa_" + ids.get(i), "pessoa.lancamentoRateio.exists"));
			}
						
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		List<Pessoa> pessoaList = pessoaRepository.find(ids);
		if(pessoaList != null) {
			for(Pessoa pessoa : pessoaList) {
				List<Arquivo> arquivoList = pessoa.getArquivoList();
				for(Arquivo arquivo : arquivoList) {
					arquivoRepository.delete(arquivo);
				}
			}
		}
		
		pessoaRepository.deleteList(ids);
		
		
		result.use(Results.jsonp()).withCallback("pessoaExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
	
	@Get
	public void autoComplete(PessoaTipo pessoaTipo, String categoria, String query) {
		result
			.include("query", query)
			.include("pessoaList", pessoaRepository.findAllActiveWithCliente(pessoaTipo, categoria, query));
	}
	
	
	@Get
	public void reload(Long id) {
		Pessoa pessoa = null;
		if(usuarioLogged.getCliente().getId().equals(id)) {
			pessoa = usuarioLogged.getCliente();
		} else {
			pessoa = pessoaRepository.find(id);
		}
		
		if(pessoa == null) {
			result.use(Results.json()).withoutRoot().from(StringUtils.EMPTY).serialize();
		} else {
			result.use(Results.json()).withoutRoot().from(pessoa).exclude("cliente").serialize();			
		}
	}
	
	
	@Get
	public void multipleAutoComplete(String q) {
		List<Pessoa> pessoaList = pessoaRepository.search(q);
		result.include("pessoaList", pessoaList);
	}
	

}
