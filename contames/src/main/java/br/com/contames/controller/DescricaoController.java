package br.com.contames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Descricao;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class DescricaoController {

	private final UsuarioLogged usuarioLogged;
	private final DescricaoRepository descricaoRepository;
	private final Settings settings;
	private final Validator validator;
	private final Result result;
	private final PlanoRecurso planoRecurso;
	
	public DescricaoController(UsuarioLogged usuarioLogged, DescricaoRepository descricaoRepository, Settings settings, Validator validator, Result result, PlanoRecurso planoRecurso) {
		this.usuarioLogged = usuarioLogged;
		this.descricaoRepository = descricaoRepository;
		this.settings = settings;
		this.validator = validator;
		this.result = result;
		this.planoRecurso = planoRecurso;
	}
	
	@Get
	public void consultar(Boolean isTable, DescricaoTipo descricaoTipo, String search, List<OrderBy> orderByList, Integer page) {
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		CriteriaBuilder cb = descricaoRepository.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
		CriteriaQuery<Descricao> cq = cb.createQuery(Descricao.class);
		Root<Descricao> descricao = cq.from(Descricao.class);
		
		cqCount.select(cb.count(descricao));
		cq.select(descricao);
		
		List<Predicate> predicateList = new ArrayList<Predicate>();
		predicateList.add(cb.equal(descricao.get("descricaoTipo"), descricaoTipo));
		predicateList.add(cb.equal(descricao.get("cliente"), usuarioLogged.getCliente()));
		
		if(StringUtils.isNotBlank(search)) {
			predicateList.add(cb.like(cb.function("unaccent", String.class, cb.lower(descricao.<String>get("nome"))), StringHelper.unAccent(search.toLowerCase()) + "%"));
		}
		
		cqCount.where(predicateList.toArray(new Predicate[predicateList.size()]));
		cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
		
		List<Order> _orderByList = new ArrayList<Order>();
		for(int i = 0; i < orderByList.size(); i++) {
			if(OrderByDirection.ASC.equals(orderByList.get(i).getDirection())) {
				_orderByList.add(cb.asc(descricao.get(orderByList.get(i).getColumn())));
				
			}else {
				_orderByList.add(cb.desc(descricao.get(orderByList.get(i).getColumn())));
			}
		}
		
		if(!_orderByList.isEmpty()) {
			cq.orderBy(_orderByList.toArray(new Order[]{}));
		}
				
		TypedQuery<Long> typedQueryCount = descricaoRepository.getEntityManager().createQuery(cqCount);
		
		Long count = typedQueryCount.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		TypedQuery<Descricao> typedQuery = descricaoRepository.getEntityManager().createQuery(cq);
		
		typedQuery.setFirstResult(firstResult);
		typedQuery.setMaxResults((int) pageSize);
		
		int paginationSize = 0;
		if(count > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
		
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("descricaoList", typedQuery.getResultList());
		} catch(NoResultException e) {
			
		}
	
		result.forwardTo("/WEB-INF/jsp/descricao/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(DescricaoTipo descricaoTipo, Long id) {
		
		// Verifica espaco no banco de dados
		if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(id != null) {
			try {
				result.include("descricao", descricaoRepository.find(id, descricaoTipo));
			}catch(NoResultException e) {
				result.notFound();
			}
		}
		
	}
	
	@Post
	public void cadastro(@NotNull(message = "{descricao.notNull}") Descricao descricao) {
		
		validator.onErrorUse(Results.jsonp()).withCallback("descricaoCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		validator.validateProperties(descricao, "descricaoTipo", "nome", "status");
		
		// Verifica se existe outra descricao com o mesmo nome
		if(descricaoRepository.isNameExists(descricao)) {
			validator.add(new I18nMessage("nome", "descricao." + descricao.getDescricaoTipo() + ".nome.exists"));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("descricaoCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		
		descricaoRepository.save(descricao);
		
		result.use(Results.jsonp()).withCallback("descricaoCadastroSuccess").withoutRoot().from(descricao).exclude("cliente").serialize();
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		descricaoRepository.deleteList(ids);
		result.use(Results.jsonp()).withCallback("descricaoExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
	
	@Get
	@Post
	public void carregaSelect(DescricaoTipo descricaoTipo, Long id) {
		result
			.include("descricaoList", descricaoRepository.findAllActive(descricaoTipo))
			.include("descricaoTipo", descricaoTipo)
			.include("id", id);
	}
	
}
