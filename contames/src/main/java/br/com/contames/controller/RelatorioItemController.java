package br.com.contames.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.record.CFRuleRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingRule;
import org.apache.poi.xssf.usermodel.XSSFFontFormatting;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheetConditionalFormatting;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDate;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.interceptor.download.InputStreamDownload;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.Settings;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.ReportExportType;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class RelatorioItemController {

	private final Result result;
	private final Validator validator;
	private final EntityManager entityManager;
	private final UsuarioLogged usuarioLogged;
	private final Localization localization;
	private final Settings settings;
	private final Connection connection;
	private final HttpServletRequest request;
	private final Environment environment;
		
	public RelatorioItemController(Result result, Validator validator, EntityManager entityManager, UsuarioLogged usuarioLogged, Localization localization, Settings settings, Connection connection, HttpServletRequest request, Environment environment) {
		this.result = result;
		this.validator = validator;
		this.entityManager = entityManager;
		this.usuarioLogged = usuarioLogged;
		this.localization = localization;
		this.settings = settings;
		this.connection = connection;
		this.request = request;
		this.environment = environment;
	}
	
	
	@Get({"/relatorios/despesas/classificacao", "/relatorios/receitas/classificacao"})
	public void relatorio(
		Integer ano,
		List<Long> itemList,
		List<Long> classifList, 
		List<LancamentoStatus> statusList, 
		boolean isTable, 
		List<OrderBy> orderByList, 
		Integer page,
		Long item_id, 
		Boolean isCCred) {
		
		ItemTipo tipo = ItemTipo.DES;
		if(request.getRequestURI().startsWith("/relatorios/receitas")) {
			tipo = ItemTipo.REC;
		}
		
		String jsp = !isTable ? "relatorio" : "relatorio_table";
		
		if(!isTable) {
			
			if(ano == null) {
				ano = LocalDate.now().getYear();
			}
			
			if(statusList == null || statusList.isEmpty()) {
				statusList = Arrays.asList(LancamentoStatus.STD, LancamentoStatus.LAN, LancamentoStatus.BXD, LancamentoStatus.COM);
			}
					
		} else {
			
			if(ano == null) {
				validator.add(new I18nMessage("ano", "ano.notNull"));
			}
			
			if(statusList == null) {
				validator.add(new I18nMessage("statusList", "statusList.notNull"));
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("item_nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("COALESCE(r.item_nome, '" + localization.getMessage("ItemTipo." + tipo + ".naoInformado") + "') item_nome, ");
		sqlBuilder.append("r.ano, ");
		
		for(int i = 1; i <= 12; i++) {
			sqlBuilder.append("SUM(CASE WHEN r.valor_" + i + " < 0 THEN r.valor_" + i + " ELSE 0 END) despesas_" + i + ", ");
			sqlBuilder.append("SUM(CASE WHEN r.valor_" + i + " > 0 THEN r.valor_" + i + " ELSE 0 END) receitas_" + i + ", ");
			sqlBuilder.append("SUM(r.valor_" + i + ") valor_" + i + ", ");
		}
		
		sqlBuilder.append("SUM(CASE WHEN r.valor < 0 THEN r.valor ELSE 0 END) despesas, ");
		sqlBuilder.append("SUM(CASE WHEN r.valor > 0 THEN r.valor ELSE 0 END) receitas, ");
		sqlBuilder.append("SUM(r.valor) valor ");
		sqlBuilder.append("FROM vw_Relatorio_Item r ");
		sqlBuilder.append("WHERE r.cliente_id = ?1 ");
		sqlBuilder.append("AND r.ano = ?2 ");
		
		if(isCCred == null) {
			isCCred = true;
		}
		
		sqlBuilder.append("AND (r.isCCred = " + isCCred + " OR r.isCCred IS NULL) ");
		
		// Item
		if(itemList != null && !itemList.isEmpty()) {
			sqlBuilder.append("AND r.item_id IN(" + StringUtils.join(itemList.toArray(), ", ") + ") ");
		}
		
		// Classif
		if(classifList != null && !classifList.isEmpty()) {
			sqlBuilder.append("AND (r.categoria_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.familia_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.tipo_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.detalhe_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ")) ");
		}
		
		// Item
		if(item_id != null) {
			sqlBuilder.append("AND r.item_id = " + item_id + " ");
		}
		
		// Status
		if(statusList != null && !statusList.isEmpty()) {
			sqlBuilder.append("AND r.status IN('" + StringUtils.join(statusList.toArray(), "', '") + "') ");
		}
		
		// Tipo
		if(tipo != null) {
			if(ItemTipo.DES.equals(tipo)) {
				sqlBuilder.append(" AND r.valor < 0 ");
			} else {
				sqlBuilder.append(" AND r.valor > 0 ");
			}
		}
		
		// GROUP BY
		sqlBuilder.append("GROUP BY ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("COALESCE(r.item_nome, '" + localization.getMessage("ItemTipo." + tipo + ".naoInformado") + "'), ");
		sqlBuilder.append("r.ano ");
		
		// Ano
		StringBuilder sqlBuilderAno = new StringBuilder();
		sqlBuilderAno.append("SELECT ano FROM(SELECT DISTINCT ano::integer FROM (");
		sqlBuilderAno.append(StringUtils.replace(sqlBuilder.toString(), " AND r.ano = ?2", ""));
		sqlBuilderAno.append(") rel ");
		sqlBuilderAno.append("UNION SELECT ano::integer FROM(SELECT EXTRACT(YEAR FROM CURRENT_TIMESTAMP) ano) a ");
		sqlBuilderAno.append(") a ORDER BY ano::integer DESC");
				
		Query anoQuery = entityManager.createNativeQuery(sqlBuilderAno.toString());
		anoQuery.setParameter(1, usuarioLogged.getCliente().getId());
		anoQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		List<Map<String, Object>> resultAnoMap = (List<Map<String, Object>>) anoQuery.getResultList();
		
		
		// Total
		StringBuilder sqlBuilderTotal = new StringBuilder();
		sqlBuilderTotal.append("SELECT  ");
		sqlBuilderTotal.append("_count,  ");
		for(int i = 1; i <= 12; i++) {
			sqlBuilderTotal.append("despesas_" + i + ",  ");
			sqlBuilderTotal.append("receitas_" + i + ",  ");
			sqlBuilderTotal.append("valor_" + i + ",  ");
		}
		sqlBuilderTotal.append("despesas_1 + despesas_2 + despesas_3 + despesas_4 + despesas_5 + despesas_6 + despesas_7 + despesas_8 + despesas_9 + despesas_10 + despesas_11 + despesas_12 despesas, ");
		sqlBuilderTotal.append("receitas_1 + receitas_2 + receitas_3 + receitas_4 + receitas_5 + receitas_6 + receitas_7 + receitas_8 + receitas_9 + receitas_10 + receitas_11 + receitas_12 receitas, ");
		sqlBuilderTotal.append("valor_1 + valor_2 + valor_3 + valor_4 + valor_5 + valor_6 + valor_7 + valor_8 + valor_9 + valor_10 + valor_11 + valor_12 valor ");
		sqlBuilderTotal.append("FROM (");
		sqlBuilderTotal.append("SELECT  ");
		for(int i = 1; i <= 12; i++) {
			sqlBuilderTotal.append("SUM(CASE WHEN r.valor_" + i + " < 0 THEN r.valor_" + i + " ELSE 0 END) despesas_" + i + ", ");
			sqlBuilderTotal.append("SUM(CASE WHEN r.valor_" + i + " > 0 THEN r.valor_" + i + " ELSE 0 END) receitas_" + i + ", ");
			sqlBuilderTotal.append("SUM(COALESCE(valor_" + i + ", 0)) valor_" + i + ", ");
		}
		sqlBuilderTotal.append("COUNT(cliente_id) _count ");
		sqlBuilderTotal.append("FROM(");
		sqlBuilderTotal.append(sqlBuilder);
		sqlBuilderTotal.append(") r");
		sqlBuilderTotal.append(") r");
		
		Query countQuery = entityManager.createNativeQuery(sqlBuilderTotal.toString());
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, ano);
		
		if(tipo != null) {
			countQuery.setParameter(3, tipo.toString());
		}
		countQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		Map<String, Object> resultTotalMap = (Map<String, Object>) countQuery.getSingleResult();
		Long count = (Long) resultTotalMap.get("_count");
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
		
		
		Query query = entityManager.createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, ano);
		
		if(tipo != null) {
			query.setParameter(3, tipo.toString());
		}
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("ano", ano)
			.include("resultTotalMap", resultTotalMap)
			.include("resultAnoMap", resultAnoMap)
			.include("tipo", tipo);
		
		try {
			result.include("relList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/relatorioItem/" + jsp + ".jsp");
	
	}
	
	
	@Get({
		"/relatorio_despesas_classificacao.xlsx", 
		"/relatorio_despesas_classificacao.pdf", 
		"/relatorio_receitas_classificacao.xlsx", 
		"/relatorio_receitas_classificacao.pdf"
	})
	public Download export(
			Integer ano,
			List<Long> itemList,
			List<Long> classifList,
			List<LancamentoStatus> statusList, 
			List<OrderBy> orderByList,
			String ext,
			Boolean isCCred) throws JRException, SQLException, IOException {
		
		ItemTipo tipo = ItemTipo.DES;
		if(request.getRequestURI().startsWith("/relatorio_receitas")) {
			tipo = ItemTipo.REC;
		}
		
		ReportExportType type = request.getRequestURI().endsWith("xlsx") ? ReportExportType.XLSX : ReportExportType.PDF;
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("item_nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, localization.getBundle());
		parameters.put(JRParameter.REPORT_LOCALE, localization.getLocale());
		parameters.put("titulo", localization.getMessage("relatorio.ItemTipo." + tipo, ano.toString()));
		parameters.put("descr", localization.getMessage("ItemTipo." + tipo));
		
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("COALESCE(r.item_nome, '" + localization.getMessage("ItemTipo." + tipo + ".naoInformado") + "') item_nome, ");
		sqlBuilder.append("COALESCE(r.item_nome, '" + localization.getMessage("ItemTipo." + tipo + ".naoInformado") + "') descr, ");
		sqlBuilder.append("r.ano, ");
		
		for(int i = 1; i <= 12; i++) {
			sqlBuilder.append("SUM(r.valor_" + i + ") valor_" + i + ", ");
		}
		
		sqlBuilder.append("SUM(r.valor) valor ");
		sqlBuilder.append("FROM vw_Relatorio_Item r ");
		sqlBuilder.append("WHERE r.cliente_id = ? ");
		sqlBuilder.append("AND r.ano = ? ");
		
		if(isCCred == null) {
			isCCred = true;
		}
		
		sqlBuilder.append("AND (r.isCCred = " + isCCred + " OR r.isCCred IS NULL) ");
		
		// Item
		if(itemList != null && !itemList.isEmpty()) {
			sqlBuilder.append("AND r.item_id IN(" + StringUtils.join(itemList.toArray(), ", ") + ") ");
		}
		
		// Classif
		if(classifList != null && !classifList.isEmpty()) {
			sqlBuilder.append("AND (r.categoria_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.familia_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.tipo_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.detalhe_id IN(" + StringUtils.join(classifList.toArray(), ", ") + ")) ");
		}
		
		// Status
		if(statusList != null && !statusList.isEmpty()) {
			sqlBuilder.append("AND r.status IN('" + StringUtils.join(statusList.toArray(), "', '") + "') ");
		}
		
		// Tipo
		if(tipo != null) {
			if(ItemTipo.DES.equals(tipo)) {
				sqlBuilder.append(" AND r.valor < 0 ");
			} else {
				sqlBuilder.append(" AND r.valor > 0 ");
			}
		}
		
		// GROUP BY
		sqlBuilder.append("GROUP BY ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("COALESCE(r.item_nome, '" + localization.getMessage("ItemTipo." + tipo + ".naoInformado") + "'), ");
		sqlBuilder.append("r.ano ");
		
		
		// Total
		StringBuilder sqlBuilderTotal = new StringBuilder();
		sqlBuilderTotal.append("SELECT  ");
		sqlBuilderTotal.append("_count,  ");
		for(int i = 1; i <= 12; i++) {
			sqlBuilderTotal.append("despesas_" + i + ",  ");
			sqlBuilderTotal.append("receitas_" + i + ",  ");
			sqlBuilderTotal.append("valor_" + i + ",  ");
		}
		sqlBuilderTotal.append("despesas_1 + despesas_2 + despesas_3 + despesas_4 + despesas_5 + despesas_6 + despesas_7 + despesas_8 + despesas_9 + despesas_10 + despesas_11 + despesas_12 despesas, ");
		sqlBuilderTotal.append("receitas_1 + receitas_2 + receitas_3 + receitas_4 + receitas_5 + receitas_6 + receitas_7 + receitas_8 + receitas_9 + receitas_10 + receitas_11 + receitas_12 receitas, ");
		sqlBuilderTotal.append("valor_1 + valor_2 + valor_3 + valor_4 + valor_5 + valor_6 + valor_7 + valor_8 + valor_9 + valor_10 + valor_11 + valor_12 valor ");
		sqlBuilderTotal.append("FROM (");
		sqlBuilderTotal.append("SELECT  ");
		for(int i = 1; i <= 12; i++) {
			sqlBuilderTotal.append("SUM(CASE WHEN r.valor_" + i + " < 0 THEN r.valor_" + i + " ELSE 0 END) despesas_" + i + ", ");
			sqlBuilderTotal.append("SUM(CASE WHEN r.valor_" + i + " > 0 THEN r.valor_" + i + " ELSE 0 END) receitas_" + i + ", ");
			sqlBuilderTotal.append("SUM(COALESCE(valor_" + i + ", 0)) valor_" + i + ", ");
		}
		sqlBuilderTotal.append("COUNT(cliente_id) _count ");
		sqlBuilderTotal.append("FROM(");
		sqlBuilderTotal.append(sqlBuilder);
		sqlBuilderTotal.append(") r");
		sqlBuilderTotal.append(") r");
		
		PreparedStatement preparedStatementTotal = connection.prepareStatement(sqlBuilderTotal.toString());
		preparedStatementTotal.setLong(1, usuarioLogged.getCliente().getId());
		preparedStatementTotal.setInt(2, ano);
		
		ResultSet resultSetTotal = preparedStatementTotal.executeQuery();
		
		
		while(resultSetTotal.next()) {
			for(int i = 1; i <= 12; i++) {
				parameters.put("valor_" + i, resultSetTotal.getDouble("valor_" + i));
			}
			parameters.put("valor", resultSetTotal.getDouble("valor"));
		}
		
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
		
		
		// Consulta Principal
		PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());
		preparedStatement.setLong(1, usuarioLogged.getCliente().getId());
		preparedStatement.setInt(2, ano);
		
		ResultSet resultSet = preparedStatement.executeQuery();
		
		
		InputStream inputStream = null;
		Download download = null;
		String jasperFileName = request.getServletContext().getRealPath("WEB-INF/reports/Relatorio" + (ItemTipo.DES.equals(tipo) ? "Despesa" : "Receita") + ".jasper");
		switch(type) {
		case PDF:
			String jrxmlFileName = request.getServletContext().getRealPath("WEB-INF/reports/Relatorio" + (ItemTipo.DES.equals(tipo) ? "Despesa" : "Receita") + ".jrxml");
			
			if(environment.supports("development")) {
				JasperCompileManager.compileReportToFile(jrxmlFileName);
			}
			inputStream = new ByteArrayInputStream(JasperRunManager.runReportToPdf(jasperFileName, parameters, new JRResultSetDataSource(resultSet)));
			download = new InputStreamDownload(inputStream, "application/pdf", request.getRequestURI().replace("/", ""));
			break;
		case XLSX:
			
			XSSFWorkbook workbook = new XSSFWorkbook(request.getServletContext().getRealPath("WEB-INF/xlsx/Relatorio_Favorecido.xlsx"));
			XSSFSheet relatorio = workbook.getSheet("Relatorio");
			XSSFSheet grafico = workbook.getSheet("Grafico");
			relatorio.getRow(0).getCell(0).setCellValue(localization.getMessage("relatorio.ItemTipo." + tipo + ".classificacao", ano.toString()));
			
			relatorio.getRow(2).getCell(0).setCellValue(localization.getMessage("ItemTipo." + tipo + ".classificacao"));
			for(int i = 1; i <= 12; i++) {
				relatorio.getRow(2).getCell(i).setCellValue(localization.getMessage("mesAbr_" + i));
			}
			relatorio.getRow(2).getCell(13).setCellValue(localization.getMessage("total"));
			
			// Estilos
			Map<String, XSSFCellStyle> mapCellStyle = new HashMap<String, XSSFCellStyle>();
			mapCellStyle.put("descr", relatorio.getRow(3).getCell(0).getCellStyle());
			for(int i = 1; i <= 12; i++) {
				mapCellStyle.put("valor_" + i, relatorio.getRow(3).getCell(i).getCellStyle());
			}
			mapCellStyle.put("valor", relatorio.getRow(3).getCell(13).getCellStyle());
			
			// Despesas
			mapCellStyle.put("despesasNome", relatorio.getRow(4).getCell(0).getCellStyle());
			for(int i = 1; i <= 12; i++) {
				mapCellStyle.put("despesas_" + i, relatorio.getRow(4).getCell(i).getCellStyle());
			}
			mapCellStyle.put("despesas", relatorio.getRow(4).getCell(13).getCellStyle());
			
			// Receitas
			mapCellStyle.put("receitasNome", relatorio.getRow(5).getCell(0).getCellStyle());
			for(int i = 1; i <= 12; i++) {
				mapCellStyle.put("receitas_" + i, relatorio.getRow(5).getCell(i).getCellStyle());
			}
			mapCellStyle.put("receitas", relatorio.getRow(5).getCell(13).getCellStyle());
			
			// Total
			mapCellStyle.put("totalNome", relatorio.getRow(6).getCell(0).getCellStyle());
			for(int i = 1; i <= 12; i++) {
				mapCellStyle.put("total_" + i, relatorio.getRow(6).getCell(i).getCellStyle());
			}
			mapCellStyle.put("total", relatorio.getRow(6).getCell(13).getCellStyle());
			
			relatorio.removeRow(relatorio.getRow(3));
			relatorio.removeRow(relatorio.getRow(4));
			relatorio.removeRow(relatorio.getRow(5));
			relatorio.removeRow(relatorio.getRow(6));
			
			// Registros
			int indexRow = 3;
			int indexRowGrafico = 1;
			while(resultSet.next()) {
				
				XSSFRow row = relatorio.createRow(indexRow);
				
				// Favorecido
				XSSFCell cell = row.createCell(0);
				cell.setCellStyle(mapCellStyle.get("descr"));
				cell.setCellValue(resultSet.getString("descr"));
				
				// Meses
				for(int i = 1; i <= 12; i++) {
					cell = row.createCell(i);
					cell.setCellStyle(mapCellStyle.get("valor_" + i));
					if(resultSet.getDouble("valor_" + i) != 0.0) {
						cell.setCellValue(resultSet.getDouble("valor_" + i));
					}
				}
				
				// Total
				cell = row.createCell(13);
				cell.setCellStyle(mapCellStyle.get("valor"));
				cell.setCellType(Cell.CELL_TYPE_FORMULA);
				cell.setCellFormula("SUM(B" + (indexRow + 1) + ":M" + (indexRow + 1) + ")");
				indexRow++;
				
				row = grafico.createRow(indexRowGrafico);
				
				// Descricao
				cell = row.createCell(0);
				cell.setCellType(Cell.CELL_TYPE_FORMULA);
				cell.setCellFormula("Relatorio!$A" + indexRow);
				
				// Despesas
				if(tipo.equals(ItemTipo.DES)) {
					cell = row.createCell(1);
					cell.setCellType(Cell.CELL_TYPE_FORMULA);
					cell.setCellFormula("IF(Relatorio!$N" + indexRow + "<0,Relatorio!$N" + indexRow + ",0)");	
				} else {
					// Receitas
					cell = row.createCell(1);
					cell.setCellType(Cell.CELL_TYPE_FORMULA);
					cell.setCellFormula("IF(Relatorio!$N" + indexRow + ">0,Relatorio!$N" + indexRow + ",0)");
				}
				
				
				indexRowGrafico++;
				
			}
			
			// Meses
			char[] letras = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'};
			
			// Total =======================================================
			XSSFRow row = relatorio.createRow(indexRow);
			XSSFCell cell = row.createCell(0);
			cell.setCellStyle(mapCellStyle.get("totalNome"));
			cell.setCellValue(localization.getMessage("total"));
			
			// Meses
			for(int i = 1; i <= 12; i++) {
				cell = row.createCell(i);
				cell.setCellStyle(mapCellStyle.get("total_" + 1));
				cell.setCellType(Cell.CELL_TYPE_FORMULA);
				cell.setCellFormula("SUM(" + letras[i] + "4:" + letras[i] + indexRow + ")");
			}
			
			// Total Geral
			cell = row.createCell(13);
			cell.setCellStyle(mapCellStyle.get("total"));
			cell.setCellType(Cell.CELL_TYPE_FORMULA);
			cell.setCellFormula("SUM(B" + (indexRow + 1) + ":M" + (indexRow + 1) + ")");
			// ==========================================================
			
			// Regra DES
			XSSFSheetConditionalFormatting condFormatDes = relatorio.getSheetConditionalFormatting();
			XSSFConditionalFormattingRule ruleCondFormatDes = condFormatDes.createConditionalFormattingRule(CFRuleRecord.ComparisonOperator.LT, "0");
			
			XSSFFontFormatting ruleConfFormatDesPattern = ruleCondFormatDes.createFontFormatting();
            ruleConfFormatDesPattern.setFontColorIndex(IndexedColors.RED.getIndex());
            
            /* Create a Cell Range Address */
            CellRangeAddress[] dataRangeCondFormatDes = {CellRangeAddress.valueOf("B4:N" + relatorio.getPhysicalNumberOfRows())};
                        
            /* Attach rule to cell range */
            condFormatDes.addConditionalFormatting(dataRangeCondFormatDes, ruleCondFormatDes);
			
			
            // Regra REC
			XSSFSheetConditionalFormatting condFormatRec = relatorio.getSheetConditionalFormatting();
			XSSFConditionalFormattingRule ruleCondFormatRec = condFormatRec.createConditionalFormattingRule(CFRuleRecord.ComparisonOperator.GT, "0");
			
			XSSFFontFormatting ruleConfFormatRecPattern = ruleCondFormatRec.createFontFormatting();
            ruleConfFormatRecPattern.setFontColorIndex(IndexedColors.BLUE.getIndex());
            
            /* Create a Cell Range Address */
            CellRangeAddress[] dataRangeCondFormatRec = {CellRangeAddress.valueOf("B4:N" + relatorio.getPhysicalNumberOfRows())};
            
            /* Attach rule to cell range */
            condFormatRec.addConditionalFormatting(dataRangeCondFormatRec, ruleCondFormatRec);
			
            // Names
            Name labels = workbook.getName("labels");
            
            labels.setRefersToFormula("Grafico!$A$2:$A$" + (indexRowGrafico - 1));
			
			Name values = workbook.getName("values");
			values.setFunction(true);
			values.setRefersToFormula("Grafico!$B$2:$B$" + (indexRowGrafico - 1));
			
			// Disponibiliza Planilha
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			workbook.write(out);
			inputStream = new ByteArrayInputStream(out.toByteArray());
			download = new InputStreamDownload(inputStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", request.getRequestURI().replace("/", ""));
			break;
		}
	
		
		return download;
	}
	
	
	
	@Get
	public void movs(
		List<Long> itemList,
		Long item_id,
		@NotNull(message="{ano.notNull}")
		Integer ano, 
		Integer mes, 
		ItemTipo tipo,
		List<LancamentoStatus> statusList,
		Boolean isTable, 
		List<OrderBy> orderByList,
		Integer page, 
		Boolean isCCred) {
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		String jsp = isTable == null || !isTable ? "movs" : "movs_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.lancamentoParcela_id, ");
		sqlBuilder.append("r.lancamento_id, ");
		sqlBuilder.append("r.baixaItem_id, ");
		sqlBuilder.append("r.baixa_id, ");
		sqlBuilder.append("r.transferencia_id, ");
		sqlBuilder.append("r.compensacaoItem_id, ");
		sqlBuilder.append("r.compensacao_id, ");
		sqlBuilder.append("r._data, ");
		sqlBuilder.append("r.favorecido_id, ");
		sqlBuilder.append("r.favorecido, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("r.item, ");
		sqlBuilder.append("CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END descricao, ");
		sqlBuilder.append("r.tipoDoc, ");
		sqlBuilder.append("r.numeroDoc, ");
		sqlBuilder.append("r.numeroDoc_clean, ");
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || '-' || r.numeroDoc, CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, r.numeroDoc) numeroDoc_nome, ");
		sqlBuilder.append("r.meio, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.meio = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
		sqlBuilder.append("WHEN r.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
		sqlBuilder.append("WHEN r.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
		sqlBuilder.append("ELSE ' - ' END meio_nome, ");
		sqlBuilder.append("r.conta_id, ");
		sqlBuilder.append("r.cartaoCredito_id, ");
		sqlBuilder.append("r.cheque_id, ");
		sqlBuilder.append("r.talaoCheque_id, ");
		sqlBuilder.append("r.meio_conta, ");
		sqlBuilder.append("r.destino_id, ");
		sqlBuilder.append("r.destino, ");
		sqlBuilder.append("r.parcela, ");
		sqlBuilder.append("r.status, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN r.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'DES' THEN '" + localization.getMessage("LancamentoStatus.DES.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'REC' THEN '" + localization.getMessage("LancamentoStatus.REC.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
		sqlBuilder.append("WHEN r.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status_nome, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor END valor, ");
		sqlBuilder.append("CASE WHEN li.perc IS NULL THEN r.valor ELSE li.perc * r.valor END valor_des_rec, ");
		sqlBuilder.append("r.tipo, ");
		sqlBuilder.append("r.criacao, ");
		sqlBuilder.append("r.usuCriacao_id, ");
		sqlBuilder.append("r.usuCriacao, ");
		sqlBuilder.append("r.alteracao, ");
		sqlBuilder.append("r.usuAlteracao_id, ");
		sqlBuilder.append("r.usuAlteracao, ");
		sqlBuilder.append("r.fatCartaoCredito_nome, ");
		sqlBuilder.append("r.boletotipo, ");
		sqlBuilder.append("r.boletonumero, ");
		sqlBuilder.append("r.arquivoDoc_id, ");
		sqlBuilder.append("r.arquivoDoc_nome, ");
		sqlBuilder.append("r.arquivo_id, ");
		sqlBuilder.append("r.arquivo_nome, ");
		sqlBuilder.append("r.comprovante_id, ");
		sqlBuilder.append("r.comprovante_nome ");
		sqlBuilder.append("FROM vw_Relatorio_Data r ");
		sqlBuilder.append("LEFT JOIN LancamentoItem li ON r.lancamento_id = li.lancamento_id ");
		sqlBuilder.append("LEFT JOIN CompensacaoItem ci ON r.compensacaoItem_id = ci.id AND ci.item_id IS NOT NULL ");
		sqlBuilder.append("LEFT JOIN Item i ON li.item_id = i.id OR ci.item_id = i.id ");
		sqlBuilder.append("WHERE r.cliente_id = ?1 ");
		sqlBuilder.append("AND date_part('year', r._data) = ?2 ");
		
		if(isCCred == null) {
			isCCred = true;
		}
		
		sqlBuilder.append("AND (r.isCCred = " + isCCred + " OR r.isCCred IS NULL) ");
		
		if(mes != null) {
			sqlBuilder.append("AND date_part('month', r._data) = ?3 ");
		}
		
		// Item
		if(itemList == null || itemList.isEmpty() || (itemList.get(0) == null && item_id == null)) {
			sqlBuilder.append("AND i.id IS NULL ");
		} else if(itemList.get(0) != null && itemList.get(0) != 0l) {
			sqlBuilder.append("AND i.id IN(" + StringUtils.join(itemList.toArray(), ", ") + ") ");
		}
		
		// Tipo
		if(tipo != null) {
			if(ItemTipo.DES.equals(tipo)) {
				sqlBuilder.append("AND (CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -COALESCE(li.perc, 1) * r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -COALESCE(li.perc, 1) * r.valor END) < 0 ");
			} else {
				sqlBuilder.append("AND (CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -COALESCE(li.perc, 1) * r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN COALESCE(li.perc, 1) * r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN COALESCE(li.perc, 1) * r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -COALESCE(li.perc, 1) * r.valor END) > 0 ");
			}
		}
		
		// Status
		if(statusList != null && !statusList.isEmpty()) {
			sqlBuilder.append("AND r.status IN('" + StringUtils.join(statusList.toArray(), "', '") + "') ");
		}
				
		// Total
		Query countQuery = entityManager.createNativeQuery("SELECT COUNT(cliente_id) _count, SUM(valor) _sum, SUM(CASE WHEN tipo = 'DES' THEN valor ELSE 0 END) sum_des, SUM(CASE WHEN tipo = 'REC' THEN valor_des_rec ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, ano);
		if(mes != null) {
			countQuery.setParameter(3, mes);
		}
		if(tipo != null) {
			countQuery.setParameter(4, tipo.toString());
		}
		countQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		Map<String, Object> resultMap = (Map<String, Object>) countQuery.getSingleResult();
		Long count = (Long) resultMap.get("_count");
		Double sum = (Double) resultMap.get("_sum");
		Double sum_des = (Double) resultMap.get("sum_des");
		Double sum_rec = (Double) resultMap.get("sum_rec");
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
		
		
		Query query = entityManager.createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		query.setParameter(2, ano);
		if(mes != null) {
			query.setParameter(3, mes);
		}
		if(tipo != null) {
			query.setParameter(4, tipo.toString());
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("statusList", statusList)
			.include("itemList", itemList)
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("sum", sum)
			.include("sum_des", sum_des)
			.include("sum_rec", sum_rec)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("relList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/relatorioItem/" + jsp + ".jsp");
		
	}
	
}
