package br.com.contames.controller;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.not;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.Cheque;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Item;
import br.com.contames.entity.TalaoCheque;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.ChequeRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.repository.TalaoChequeRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class ContaController {

	private final PessoaRepository pessoaRepository;
	private final ContaRepository contaRepository;
	private final TalaoChequeRepository talaoChequeRepository;
	private final ChequeRepository chequeRepository;
	private final UsuarioLogged usuarioLogged;
	private final Localization localization;
	private final Settings settings;
	private final Result result;
	private final Validator validator;
	private final HttpSession session;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	private final ArquivoRepository arquivoRepository;
	
	public ContaController(PessoaRepository pessoaRepository, ContaRepository contaRepository, TalaoChequeRepository talaoChequeRepository, ChequeRepository chequeRepository, UsuarioLogged usuarioLogged, Localization localization, Settings settings, Result result, Validator validator, HttpSession session, PlanoRecurso planoRecurso, ArquivoSession arquivoSession, ArquivoRepository arquivoRepository) {
		this.pessoaRepository = pessoaRepository;
		this.contaRepository = contaRepository;
		this.talaoChequeRepository = talaoChequeRepository;
		this.chequeRepository = chequeRepository;
		this.usuarioLogged = usuarioLogged;
		this.localization = localization;
		this.settings = settings;
		this.result = result;
		this.validator = validator;
		this.session = session;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
		this.arquivoRepository = arquivoRepository;
	}
	
	
	
	@Get("/cadastros/contas")
	public void consultar(Boolean isTable, Boolean isPoupanca, String search, List<OrderBy> orderByList, Integer page, Status status) {
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
				
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("c.id, ");
		sqlBuilder.append("COALESCE(Banco.fantasia, Banco.razaoSocial) AS banco,");
		sqlBuilder.append("CASE WHEN c.isPrincipal THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END AS isPrincipal, ");
		sqlBuilder.append("CASE WHEN c.isPoupanca THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END AS isPoupanca, ");
		sqlBuilder.append("CASE WHEN c.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN c.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END status, ");
		sqlBuilder.append("c.status _status, ");
		sqlBuilder.append("c.nome, ");
		sqlBuilder.append("COALESCE(Titular.nome, Titular.razaoSocial) AS titular, ");
		sqlBuilder.append("c.agencia, ");
		sqlBuilder.append("c.agenciaNum, ");
		sqlBuilder.append("c.contaNum, ");
		sqlBuilder.append("c.saldoInicial, ");
		sqlBuilder.append("c.saldoAtual, ");
		sqlBuilder.append("c.saldoLimite, ");
		sqlBuilder.append("c.saldoLimiteDisp, ");
		sqlBuilder.append("c.obs ");
		sqlBuilder.append("FROM Conta c ");
		sqlBuilder.append("LEFT JOIN Pessoa AS Banco ON c.banco_id = Banco.id ");
		sqlBuilder.append("LEFT JOIN Pessoa AS Titular ON c.titular_id = Titular.id ");
		sqlBuilder.append("WHERE c.cliente_id = ?1 AND c.status = 'INA'");
		
		if(status != null) {
			sqlBuilder.append(" AND c.status = ?2");
		}
		
		if(isPoupanca != null) {
			sqlBuilder.append(" AND COALESCE(c.isPoupanca, false) = ?3");
		}
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("unaccent(LOWER(COALESCE(Banco.fantasia, Banco.razaoSocial))) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN c.isPrincipal THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN c.isPoupanca THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN c.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN c.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(c.nome)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(Titular.nome, Titular.razaoSocial))) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(c.agencia)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(c.agenciaNum)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(c.contaNum)) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(c.saldoInicial AS VARCHAR))) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(c.saldoAtual AS VARCHAR))) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(c.saldoLimite AS VARCHAR))) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(c.saldoLimiteDisp AS VARCHAR))) ILIKE ?4 ");
			sqlBuilder.append("OR unaccent(LOWER(c.obs)) ILIKE ?4 ");
			sqlBuilder.append(")");
		}
		
		Query countQuery = pessoaRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Conta");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(status != null) {
			countQuery.setParameter(2, status.toString());
		}
		
		if(isPoupanca != null) {
			countQuery.setParameter(3, isPoupanca);
		}
		
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(4, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count.intValue()) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
				
		Query query = pessoaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(status != null) {
			query.setParameter(2, status.toString());
		}
		
		if(isPoupanca != null) {
			query.setParameter(3, isPoupanca);
		}
		
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(4, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("contaList", query.getResultList());
		} catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/conta/" + jsp + ".jsp");
		
	}
	
	
	
	@Get
	public void cadastro(Long id) { 
		
		if(id == null && planoRecurso.getContasDisp() == 0) {
			validator.add(new I18nMessage("plano.contas", "plano.contas.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getContas()));
		}
		
		// Verifica espaco no banco de dados
		if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
		
		// Gera viewid
		String viewid = StringHelper.createViewId();
		Conta conta = null;
		Conta principal = contaRepository.findPrincipal();
		Boolean isCompensacao = false;
		if(id == null) {
			conta = new Conta();
			conta.setTitular(usuarioLogged.getUsuario());
			if(principal == null) {
				conta.setIsPrincipal(true);
			}
		} else {
			conta = contaRepository.find(id);
			
			if(conta != null) {
				isCompensacao = contaRepository.isCompensado(conta);
				
				arquivoSession.setAquivoList(viewid, conta.getArquivoList());
			}
		}
		
		List<TalaoCheque> talaoChequeList = talaoChequeRepository.findAll(conta);
		if(talaoChequeList == null) {
			talaoChequeList = new ArrayList<TalaoCheque>();
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		session.setAttribute("talaoChequeList_" + viewid, talaoChequeList);
		session.setAttribute("isCompensacao_" + viewid, isCompensacao);
		
		result
			.include("conta", conta)
			.include("isCompensacao", isCompensacao)
			.include("principal", principal)
			.include("talaoChequeList", talaoChequeList)
			.include("viewid", viewid);
		
	}
	
	@Post
	public void cadastro(@NotNull(message = "{conta.notNull}") Conta conta, List<Long> talaoChequeDelete, String viewid) {
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		if(BooleanUtils.isTrue(conta.getIsPoupanca())) {
			conta.setSaldoLimite(null);
			conta.setSaldoLimiteDisp(null);
		}
		
		Boolean isCompensacao = (Boolean) session.getAttribute("isCompensacao_" + viewid);
		
		if(isCompensacao != null && isCompensacao) {
			Conta _conta = contaRepository.find(conta.getId());
			
			conta.setSaldoInicial(_conta.getSaldoInicial());
			conta.setSaldoAtual(_conta.getSaldoAtual());
			
		}
		
		validator.validateProperties(conta, "banco", "isPrincipal", "isPoupanca", "status", "nome", "titular", "agencia", "agenciaNum", "contaNum", "saldoInicial", "saldoLimite", "obs");
		
		// Verifica se existe outro conta com este nome
		if(contaRepository.exists(conta)) {
			validator.add(new I18nMessage("nome", "conta.nome.exists"));
		}
		
		@SuppressWarnings("unchecked")
		List<TalaoCheque> talaoChequeList = (List<TalaoCheque>) session.getAttribute("talaoChequeList_" + viewid);
		
		if(talaoChequeList != null && !talaoChequeList.isEmpty()) {
			conta.setIsPoupanca(false);
		}
		
		if(conta.getSaldoAtual() == null) {
			conta.setSaldoAtual(conta.getSaldoInicial());
		}
		
		if(conta.getIsPoupanca() != null && conta.getIsPoupanca()) {
			conta.setSaldoLimite(null);
			conta.setSaldoLimiteDisp(null);
		} else {
				
			// Calcular Limite Disponivel
			if(conta.getSaldoLimite() == null) {
				conta.setSaldoLimiteDisp(null);
			} else {
				Double saldoLimiteDisp = conta.getSaldoLimite();
				Double saldoAtual = conta.getSaldoAtual();
				
				Boolean isCompensado = contaRepository.isCompensado(conta);
				if(isCompensado) {
					Conta _conta = contaRepository.find(conta.getId());
					saldoAtual = _conta.getSaldoAtual();
				}
				
				if(saldoAtual < 0) {
					saldoLimiteDisp = conta.getSaldoLimite() + saldoAtual;
					
					if(saldoLimiteDisp < 0) {
						validator.add(new I18nMessage("saldoLimite", "saldoLimit.min"));
					}
				}
				conta.setSaldoLimiteDisp(saldoLimiteDisp);
			}
		}
		
		// Verifica Banco
		if(conta.getBanco() != null && conta.getBanco().getId() == null) {
			conta.setBanco(null);
		}
		if(conta.getBanco() != null) {
			conta.setBanco(pessoaRepository.findBanco(conta.getBanco().getId()));
		}
		
		
		// Verificar se os taloes marcados para exclusao podem ser excluidos
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		
		
		for(TalaoCheque talaoCheque : talaoChequeList) {
			if(talaoCheque.getId() == null) {
				talaoCheque.setConta(conta);
				talaoCheque.setCriacao(LocalDateTime.now());
				talaoCheque.setUsuCriacao(usuarioLogged.getUsuario());
			} else {
				talaoCheque.setAlteracao(LocalDateTime.now());
				talaoCheque.setUsuAlteracao(usuarioLogged.getUsuario());
			}
			talaoChequeRepository.save(talaoCheque);
			
			// Recupera List de Arquivo Talao de Cheque
			List<Arquivo> arquivoList = arquivoSession.getArquivoList(talaoCheque.getViewid());
			for(Arquivo arquivo : arquivoList) {
				arquivo.setTalaoCheque(talaoCheque);
				arquivoRepository.save(arquivo);
			}
			
			// Recupera List de Arquivo para excluir
			List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(talaoCheque.getViewid());
			for(Arquivo arquivo : arquivoDeleteList) {
				arquivoRepository.delete(arquivo);
			}
			
			arquivoSession.clear(talaoCheque.getViewid());
		}
		
		// Verifica se esta conta e a principal
		if(conta.getIsPrincipal() != null && conta.getIsPrincipal()) {
			
			// Pega a conta principal anterior
			Conta principal = contaRepository.findPrincipal();
			
			// Verifica se existe conta principal e se eh diferente da conta
			if(principal != null && !conta.equals(principal)) {
				
				// Coloca principal como false
				principal.setIsPrincipal(false);
				
				// Salva conta principal
				contaRepository.save(principal);
			}
		}
		
		contaRepository.save(conta);
		
		// Recupera List de Arquivo
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		for(Arquivo arquivo : arquivoList) {
			arquivo.setConta(conta);
			arquivoRepository.save(arquivo);
		}
		
		// Recupera List de Arquivo para excluir
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		for(Arquivo arquivo : arquivoDeleteList) {
			arquivoRepository.delete(arquivo);
		}
		arquivoSession.clear(viewid);
		
		// Lista de Arquivo Talao de Cheque excluir
		if(talaoChequeDelete != null && !talaoChequeDelete.isEmpty()) {
			List<TalaoCheque> talaoChequeDeleteList = talaoChequeRepository.findAll(talaoChequeDelete);
			for(TalaoCheque talaoChequeDel : talaoChequeDeleteList) {
				for(Arquivo arquivoDelete : talaoChequeDel.getArquivoList()) {
					arquivoRepository.delete(arquivoDelete);
				}
			}
			talaoChequeRepository.deleteList(talaoChequeDelete);
		}
		
		contaRepository.updateTalaoFolhas(conta.getId());
				
		result.use(Results.jsonp()).withCallback("contaCadastroSuccess").withoutRoot().from(conta).exclude("cliente").serialize();
		
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		for(int i = 0; i < ids.size(); i++) {
			
			// Verifica se foi selecionada num item como lancamento automatico
			if(contaRepository.existsItemLancAuto(ids.get(i))) {
				validator.add(new I18nMessage("conta_" + ids.get(i), "conta.item.lancAuto.exists"));
			}
			
			// Verifica se existe Baixa
			if(contaRepository.existsBaixa(ids.get(i))) {
				validator.add(new I18nMessage("conta_" + ids.get(i), "conta.baixa.exists"));
			}
			
			// Verifica se existe transferencia
			if(contaRepository.existsTransferencia(ids.get(i))) {
				validator.add(new I18nMessage("conta_" + ids.get(i), "conta.transferencia.exists"));
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		List<Conta> contaList = contaRepository.find(ids);
		if(contaList != null) {
			for(Conta conta : contaList) {
				List<Arquivo> arquivoList = conta.getArquivoList();
				for(Arquivo arquivo : arquivoList) {
					arquivoRepository.delete(arquivo);
				}
				
				List<TalaoCheque> talaoChequeList = conta.getTalaoChequeList();
				if(talaoChequeList != null) {
					for(TalaoCheque talaoCheque : talaoChequeList) {
						List<Arquivo> arquivoDeleteList = talaoCheque.getArquivoList();
						if(arquivoDeleteList != null) {
							for(Arquivo arquivoDelete : arquivoDeleteList) {
								arquivoRepository.delete(arquivoDelete);
							}
						}
					}
				}
				
			}
		}
		
		contaRepository.deleteList(ids);
		
		result.use(Results.jsonp()).withCallback("contaExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
	
	
	@Get
	public void talaoCheque(Integer index, String viewid, Long contaId, Long id) {
		
		if(viewid != null) {
			
			List<TalaoCheque> talaoChequeList = (List<TalaoCheque>) session.getAttribute("talaoChequeList_" + viewid);
			TalaoCheque talaoCheque = null;
			if(index == null) {
				talaoCheque = new TalaoCheque();
				talaoCheque.setData(LocalDate.now());
				if(talaoChequeList == null || talaoChequeList.isEmpty()) {
					talaoCheque.setIndex(0);
					talaoCheque.setSeqInicio(1);
					talaoCheque.setFolhas(20);
					talaoCheque.setSeqFim(20);
					talaoCheque.setStatus(Status.ATI);
				} else {
					TalaoCheque lastTalaoCheque = talaoChequeList.get(talaoChequeList.size() - 1);
					talaoCheque.setIndex(lastTalaoCheque.getIndex() + 1);
					talaoCheque.setSeqInicio(lastTalaoCheque.getSeqFim() + 1);
					talaoCheque.setFolhas(20);
					talaoCheque.setSeqFim(lastTalaoCheque.getSeqFim() + talaoCheque.getFolhas());
					talaoCheque.setStatus(Status.ATI);
				}
			} else {
				talaoCheque = talaoChequeList.get(index);
			}
			
			arquivoSession.setAquivoList(talaoCheque.getViewid(), talaoCheque.getArquivoList());
			
			result
				.include("viewid", viewid)
				.include("talaoCheque", talaoCheque);
		
		} else {
			
			Conta conta = contaRepository.find(contaId);
			if(conta == null) {
				validator.add(new I18nMessage("conta", "conta.notFound"));
			} else {
				
				List<TalaoCheque> talaoChequeList = conta.getTalaoChequeList();
				TalaoCheque talaoCheque = null;
				if(id == null) {
					talaoCheque = new TalaoCheque();
					talaoCheque.setConta(conta);
					talaoCheque.setData(LocalDate.now());
					if(talaoChequeList == null || talaoChequeList.isEmpty()) {
						talaoCheque.setIndex(0);
						talaoCheque.setSeqInicio(1);
						talaoCheque.setFolhas(20);
						talaoCheque.setSeqFim(20);
						talaoCheque.setStatus(Status.ATI);
					} else {
						TalaoCheque lastTalaoCheque = talaoChequeList.get(talaoChequeList.size() - 1);
						talaoCheque.setIndex(lastTalaoCheque.getIndex() + 1);
						talaoCheque.setSeqInicio(lastTalaoCheque.getSeqFim() + 1);
						talaoCheque.setFolhas(20);
						talaoCheque.setSeqFim(lastTalaoCheque.getSeqFim() + talaoCheque.getFolhas());
					}
					
					arquivoSession.setAquivoList(talaoCheque.getViewid(), talaoCheque.getArquivoList());
					
					result.include("talaoCheque", talaoCheque);
				
				} else {
					
					talaoCheque = talaoChequeRepository.find(id);
					
					if(talaoCheque == null) {
						validator.add(new I18nMessage("talaoCheque", "talaoCheque.notFound"));
					} else {
						
						arquivoSession.setAquivoList(talaoCheque.getViewid(), talaoCheque.getArquivoList());
						
						result
							.include("talaoCheque", talaoCheque)
							.include("chequeList", chequeRepository.findAll(talaoCheque));
					}
				}
				
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
	}
	
	@SuppressWarnings("unchecked")
	@Post
	public void talaoCheque(@NotNull(message = "{talaoCheque.notNull}") TalaoCheque talaoCheque, String viewid, Conta conta) {
		
		
		List<TalaoCheque> talaoChequeList = null;
		
		if(viewid != null) {
			// Recupera talaoChequeList da sessao
			talaoChequeList = (List<TalaoCheque>) session.getAttribute("talaoChequeList_" + viewid);
		} else {
			conta = contaRepository.find(conta.getId());
			
			if(conta == null) {
				validator.add(new I18nMessage("conta", "conta.notFound"));
			} else {
				talaoChequeList = conta.getTalaoChequeList();
			}
		}
		
		// Verifica se a talaoChequeList eh null
		if(talaoChequeList == null) {
			validator.add(new I18nMessage("talaoChequeList", "talaoChequeList.notNull"));
		} else {
		
			// Verifica se o talao nao eh null
			if(talaoCheque != null) {
				
				if(talaoCheque.getSeqInicio() != null && talaoCheque.getFolhas() != null) {
					talaoCheque.setSeqFim(((talaoCheque.getSeqInicio() + talaoCheque.getFolhas()) - 1));
				}
				
				validator.validateProperties(talaoCheque, "data", "seqInicio", "folhas", "status", "index");
				
				// Recupera talao anterior
				TalaoCheque talaoChequeAnterior = null;
				if(talaoChequeList != null && !talaoChequeList.isEmpty() && talaoCheque.getIndex() > 0) {
					talaoChequeAnterior = talaoChequeList.get(talaoCheque.getIndex() - 1);
				}
								
				// Verifica se o talao anterior nao eh null
				if(talaoChequeAnterior != null) {
					
					// Verifise se a sequencia informada eh truncada com o talao anterior
					if(talaoCheque.getSeqInicio() != null && talaoCheque.getSeqInicio() <= talaoChequeAnterior.getSeqFim()) {
						validator.add(new I18nMessage("seqInicio", "talaoCheque.seqInicio.invalid", talaoChequeAnterior.getSeqFim()));
					}
					
				}
				
				// Recupera talao proximo
				TalaoCheque talaoChequeProximo = null;
				if(talaoChequeList != null && !talaoChequeList.isEmpty() && (talaoCheque.getIndex() + 1) < talaoChequeList.size()) {
					talaoChequeProximo = talaoChequeList.get(talaoCheque.getIndex() + 1);
				}
								
				// Verifica se o talao proximo nao eh null
				if(talaoChequeProximo != null) {
					
					// Verifise se a sequencia informada eh truncada com o talao anterior
					if(talaoCheque.getSeqFim() != null && talaoCheque.getSeqFim() >= talaoChequeProximo.getSeqInicio()) {
						validator.add(new I18nMessage("seqFim", "talaoCheque.seqFim.invalid", talaoChequeProximo.getSeqInicio()));
					}
					
				}
				
				// Verifica se quantidade de folhas nao confere com o cheque existentes
				if(talaoCheque.getId() != null && talaoCheque.getSeqInicio() != null && talaoCheque.getSeqFim() != null) {
					if(talaoChequeRepository.existsChequeForaIntervaloInicio(talaoCheque)) {
						Integer minSeq = talaoChequeRepository.minCheque(talaoCheque);
						validator.add(new I18nMessage("seqInicio", "talaoCheque.seqInicio.after.cheque", minSeq));
					}
					if(talaoChequeRepository.existsChequeForaIntervaloFim(talaoCheque)) {
						Integer maxSeq = talaoChequeRepository.maxCheque(talaoCheque);
						validator.add(new I18nMessage("seqFim", "talaoCheque.seqFim.before.cheque", maxSeq));
					}
				}
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		// Verifica se a conta eh null, somente quando a alteracao for pela conta
		if(viewid != null) {
			TalaoCheque _talaoCheque = selectFirst(talaoChequeList, having(on(TalaoCheque.class).getIndex(), equalTo(talaoCheque.getIndex()))); 
			if(_talaoCheque == null) {
				talaoChequeList.add(talaoCheque.getIndex(), talaoCheque);
			} else {
				talaoChequeList.set(talaoCheque.getIndex(), talaoCheque);
			}
			
			result.include("talaoChequeList", talaoChequeList).forwardTo("/WEB-INF/jsp/conta/talaoChequeList.jsp");
		} 
		
		// Alteracao direta no talao
		else {
			talaoCheque.setConta(conta);
			if(talaoCheque.getId() == null) {
				conta.getTalaoChequeList().add(talaoCheque);
			}
			talaoChequeRepository.save(talaoCheque);
			
			// Recupera Lista de Arquivos
			List<Arquivo> arquivoList = arquivoSession.getArquivoList(talaoCheque.getViewid());
			for(Arquivo arquivo : arquivoList) {
				arquivo.setTalaoCheque(talaoCheque);
				arquivoRepository.save(arquivo);
			}
			
			// Recupera Lista de Arquivos para Deletar
			List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(talaoCheque.getViewid());
			for(Arquivo arquivo : arquivoDeleteList) {
				arquivoRepository.delete(arquivo);
			}
			
			contaRepository.updateTalaoFolhas(conta.getId());
			result.use(Results.json()).withoutRoot().from(talaoCheque).serialize();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Post
	public void removerTalaoCheque(List<Integer> indexs, String viewid, List<Long> ids) {
		
		List<TalaoCheque> talaoChequeList = null;
		
		if(viewid == null) {
			talaoChequeList = talaoChequeRepository.findAll(ids);
		} else {
			talaoChequeList = (List<TalaoCheque>) session.getAttribute("talaoChequeList_" + viewid);
		}
		
		if(talaoChequeList == null) {
			validator.add(new I18nMessage("talaoChequeList", "talaoChequeList.notFound"));
		} else {
		
			// Verificar se os taloes ja foram utilizados
			if(viewid == null) {
				
				for(TalaoCheque talaoCheque : talaoChequeList) {
					if(talaoChequeRepository.existsCheque(talaoCheque)) {
						validator.add(new I18nMessage("talaoCheque_" + talaoCheque.getId(), "talaoCheque.excluir.exists.cheque"));
					}
				}
				
			} else {
				List<TalaoCheque> selTalaoChequeList = select(talaoChequeList, having(on(TalaoCheque.class).getIndex(), isIn(indexs)));
				for(TalaoCheque talaoCheque : selTalaoChequeList) {
					if(talaoCheque.getId() != null) {
						List<Cheque> chequeList = chequeRepository.talao(talaoCheque.getId());
						if(chequeList != null) {
							validator.add(new I18nMessage("talaoCheque_" + talaoCheque.getId(), "talaoCheque.excluir.exists.cheque"));
						}
					}
				}
			}
		}
			
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		if(viewid == null) {
			
			List<Long> contaIdList = new ArrayList<Long>();
			for(TalaoCheque talaoCheque : talaoChequeList) {
				talaoCheque.getConta().getTalaoChequeList().remove(talaoCheque);
				
				if(contaIdList.contains(talaoCheque.getConta().getId())) {
					contaIdList.add(talaoCheque.getConta().getId());
				}
			}
			
			List<Long> idContaTalaoChequeList = talaoChequeRepository.findContaId(ids);
			for(Long contaId : idContaTalaoChequeList) {
				if(!contaIdList.contains(contaId)) {
					contaIdList.add(contaId);
				}
			}
			
			// Recupera Lista de Arquivo a ser Deletado
			List<TalaoCheque> talaoChequeListDelete = talaoChequeRepository.findAll(ids);
			if(talaoChequeListDelete != null && !talaoChequeListDelete.isEmpty()) {
				for(TalaoCheque talaoChequeDelete : talaoChequeListDelete) {
					List<Arquivo> arquivoListDelete = arquivoSession.getArquivoList(talaoChequeDelete.getViewid());
					for(Arquivo arquivoDelete : arquivoListDelete) {
						arquivoRepository.delete(arquivoDelete);
					}
				}
			}
			
			talaoChequeRepository.deleteList(ids);
			
			
			
			for(Long contaId : contaIdList) {
				contaRepository.updateTalaoFolhas(contaId);
			}
			
			result.use(Results.jsonp()).withCallback("talaoChequeExcluirSuccess").withoutRoot().from(ids).serialize();
			
		} else {
		
			List<TalaoCheque> newTalaoChequeList = select(talaoChequeList, having(on(TalaoCheque.class).getIndex(), not(isIn(indexs))));
			
			for(int i = 0; i < newTalaoChequeList.size(); i++) {
				newTalaoChequeList.get(i).setIndex(i);
			}
		
			session.setAttribute("talaoChequeList_" + viewid, newTalaoChequeList);
					
			result
				.include("talaoChequeList", newTalaoChequeList)
				.forwardTo("/WEB-INF/jsp/conta/talaoChequeList.jsp");
		}
		
		
	}
	
	@Get
	public void dialogSelect(Long id, Boolean isRequired, Boolean isPoupanca, boolean isReadOnly) {
		Conta conta = null;
		if(isReadOnly && id != null) {
			conta = contaRepository.find(id);
		}
		
		result
			.include("id", id)
			.include("isRequired", isRequired)
			.include("isPoupanca", isPoupanca)
			.include("isReadOnly", isReadOnly)
			.include("conta", conta)
			.include("contaList", contaRepository.findAllActive(isPoupanca));
	}
	
	@Post
	public void dialogSelect(@NotNull(message = "{conta.notNull}") Conta conta, Boolean isRequired) {
		
		if(conta != null && conta.getId() != null) {
			
			conta = contaRepository.find(conta.getId());
			
			if(conta == null) {
				validator.add(new I18nMessage("conta", "conta.notFound"));
			}
			
		}
		
		if(isRequired != null && isRequired && (conta == null || conta.getId() == null)) {
			validator.add(new I18nMessage("conta", "conta.notNull"));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		
		result.use(Results.jsonp()).withCallback("dialogSelectContaSuccess").withoutRoot().from(conta).exclude("cliente").serialize();
		
	}
	
	@Get
	public void carregaSelect(Long id, Boolean isPoupanca) {
		result
			.include("id", id)
			.include("contaList", contaRepository.findAllActive(isPoupanca));
	}
	
	@Get
	public void carregaMultiSelect() {
		List<Conta> contaList = contaRepository.findAll();
		if(contaList == null) {
			result.notFound();
		} else {
			result.use(Results.json()).withoutRoot().from(contaList).excludeAll().include("id", "nome", "isPrincipal", "status").serialize();
		}
	}
}
