package br.com.contames.controller;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.tasks.scheduler.Scheduled;

@Resource
public class LancAutoController {
	
	private final Result result;
	private final EntityManager entityManager;
	
	public LancAutoController(Result result, EntityManager entityManager) {
		this.result = result;
		this.entityManager = entityManager;
	}

	@Post
	@Scheduled(cron="0 0 12 * * ?")
	public void execute() {
		entityManager.createNativeQuery("SELECT fn_lancAuto()").getSingleResult();
		result.nothing();
	}
	
}
