package br.com.contames.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.xml.sax.SAXException;

import urn.ebay.api.PayPalAPI.CreateRecurringPaymentsProfileResponseType;
import urn.ebay.api.PayPalAPI.ManageRecurringPaymentsProfileStatusResponseType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.LandingPageType;
import urn.ebay.apis.eBLBaseComponents.StatusChangeActionType;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.util.jpa.extra.Load;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.AllowCliente;
import br.com.contames.annotation.AllowOnInactive;
import br.com.contames.annotation.Private;
import br.com.contames.component.CloudFiles;
import br.com.contames.component.PaypalBuilder;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Assinatura;
import br.com.contames.entity.AssinaturaPagamento;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Migrar;
import br.com.contames.entity.Plano;
import br.com.contames.enumeration.AssinaturaPagamentoStatus;
import br.com.contames.enumeration.AssinaturaStatus;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.PeriodoData;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.AssinaturaPagamentoRepository;
import br.com.contames.repository.AssinaturaRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.MigrarRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.repository.PlanoRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;

@Private
@Resource
public class AdminController {
	
	private final AssinaturaRepository assinaturaRepository;
	private final PlanoRepository planoRepository;
	private final AssinaturaPagamentoRepository assinaturaPagamentoRepository;
	private final MigrarRepository migrarRepository;
	private final ContaRepository contaRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final PessoaRepository pessoaRepository;
	private final CloudFiles cloudFiles;
	private final PaypalBuilder paypalBuilder;
	private final Result result;
	private final Validator validator;
	private final UsuarioLogged usuarioLogged;
	private final PlanoRecurso planoRecurso;
	private final HttpSession session;
	private final Environment environment;
	private final Settings settings;
	
	public AdminController(AssinaturaRepository assinaturaRepository, PlanoRepository planoRepository, AssinaturaPagamentoRepository assinaturaPagamentoRepository, MigrarRepository migrarRepository, ContaRepository contaRepository, CartaoCreditoRepository cartaoCreditoRepository, PessoaRepository pessoaRepository, CloudFiles cloudFiles, PaypalBuilder paypalBuilder, Result result, Validator validator, UsuarioLogged usuarioLogged, PlanoRecurso planoRecurso, HttpSession session, Environment environment, Settings settings) {
		this.assinaturaRepository = assinaturaRepository;
		this.planoRepository = planoRepository;
		this.assinaturaPagamentoRepository = assinaturaPagamentoRepository;
		this.migrarRepository = migrarRepository;
		this.contaRepository = contaRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.pessoaRepository = pessoaRepository;
		this.cloudFiles = cloudFiles;
		this.paypalBuilder = paypalBuilder;
		this.result = result;
		this.validator = validator;
		this.usuarioLogged = usuarioLogged;
		this.planoRecurso = planoRecurso;
		this.session = session;
		this.environment = environment;
		this.settings = settings;
	}

	@Get("/resumo")
	public void resumo(boolean isTable, PeriodoData periodoData, List<OrderBy> orderByList, Integer page, Localization localization, EntityManager entityManager, Boolean isCCred) { 
		
		String jsp = !isTable ? "resumo" : "resumo_table";
		
		if(periodoData == null) {
			periodoData = PeriodoData.SEMANA_ATUAL;
		}
		
		LocalDate dataDe = LocalDate.now();
		LocalDate dataAte = LocalDate.now();
		
		if(periodoData.equals(PeriodoData.SEMANA_ATUAL)) {
			dataDe = dataDe.withDayOfWeek(1).minusDays(1);
			dataAte = dataAte.withDayOfWeek(6);
		} else if(periodoData.equals(PeriodoData.MES_ATUAL)) {
			dataDe = dataDe.withDayOfMonth(1);
			dataAte = dataAte.dayOfMonth().withMaximumValue();
		} else if(periodoData.equals(PeriodoData.ANO_ATUAL)) {
			dataDe = dataDe.withDayOfYear(1);
			dataAte = dataAte.withMonthOfYear(12).dayOfMonth().withMaximumValue();
		}
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.DESC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.lancamentoParcela_id, ");
		sqlBuilder.append("r.lancamento_id, ");
		sqlBuilder.append("r.baixaItem_id, ");
		sqlBuilder.append("r.baixa_id, ");
		sqlBuilder.append("r.transferencia_id, ");
		sqlBuilder.append("r.compensacaoItem_id, ");
		sqlBuilder.append("r.compensacao_id, ");
		sqlBuilder.append("r._data, ");
		sqlBuilder.append("r.favorecido_id, ");
		sqlBuilder.append("r.favorecido, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("r.item, ");
		sqlBuilder.append("CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END descricao, ");
		sqlBuilder.append("r.tipoDoc, ");
		sqlBuilder.append("r.numeroDoc, ");
		sqlBuilder.append("r.numeroDoc_clean, ");
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || '-' || r.numeroDoc, CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, r.numeroDoc) numeroDoc_nome, ");
		sqlBuilder.append("r.fatCartaoCredito_nome, ");
		sqlBuilder.append("r.meio, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.meio = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
		sqlBuilder.append("WHEN r.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
		sqlBuilder.append("WHEN r.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
		sqlBuilder.append("ELSE ' - ' END meio_nome, ");
		sqlBuilder.append("r.conta_id, ");
		sqlBuilder.append("r.cartaoCredito_id, ");
		sqlBuilder.append("r.cheque_id, ");
		sqlBuilder.append("r.talaoCheque_id, ");
		sqlBuilder.append("r.meio_conta, ");
		sqlBuilder.append("r.boletotipo, ");
		sqlBuilder.append("r.boletonumero, ");
		sqlBuilder.append("r.destino_id, ");
		sqlBuilder.append("r.destino, ");
		sqlBuilder.append("r.parcela, ");
		sqlBuilder.append("r.status, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN r.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'DES' THEN '" + localization.getMessage("LancamentoStatus.DES.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'REC' THEN '" + localization.getMessage("LancamentoStatus.REC.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
		sqlBuilder.append("WHEN r.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status_nome, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor END valor, ");
		sqlBuilder.append("r.valor valor_des_rec, ");
		sqlBuilder.append("r.tipo, ");
		sqlBuilder.append("r.criacao, ");
		sqlBuilder.append("r.usuCriacao_id, ");
		sqlBuilder.append("r.usuCriacao, ");
		sqlBuilder.append("r.alteracao, ");
		sqlBuilder.append("r.usuAlteracao_id, ");
		sqlBuilder.append("r.usuAlteracao, ");
		sqlBuilder.append("r.arquivoDoc_id, ");
		sqlBuilder.append("r.arquivoDoc_nome, ");
		sqlBuilder.append("r.arquivo_id, ");
		sqlBuilder.append("r.arquivo_nome, ");
		sqlBuilder.append("r.comprovante_id, ");
		sqlBuilder.append("r.comprovante_nome ");
		sqlBuilder.append("FROM vw_Relatorio_Data r ");
		sqlBuilder.append("WHERE r.cliente_id = ?1 ");
		sqlBuilder.append("AND r._data BETWEEN ?2 AND ?3 ");
		
		if(isCCred == null) {
			isCCred = true;
		}
		
		sqlBuilder.append("AND (r.isCCred = " + isCCred + " OR r.isCCred IS NULL) ");
				
		// Status
		sqlBuilder.append("AND r.status IN('STD', 'LAN', 'BXD', 'COM') ");
		
		// Total
		Query countQuery = entityManager.createNativeQuery("SELECT COUNT(cliente_id) _count, SUM(valor) _sum, SUM(CASE WHEN valor < 0 THEN -valor ELSE 0 END) sum_des, SUM(CASE WHEN tipo = 'REC' THEN valor_des_rec ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, dataDe.toDate());
		countQuery.setParameter(3, dataAte.toDate());
		
		countQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		Map<String, Object> resultMap = (Map<String, Object>) countQuery.getSingleResult();
		Long count = (Long) resultMap.get("_count");
		Double sum = (Double) resultMap.get("_sum");
		Double sum_des = (Double) resultMap.get("sum_des");
		Double sum_rec = (Double) resultMap.get("sum_rec");
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
		
		
		Query query = entityManager.createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, dataDe.toDate());
		query.setParameter(3, dataAte.toDate());
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		// Contas
		List<Long> contas = new ArrayList<Long>();
		Conta contaPrincipal = contaRepository.findPrincipal();
		if(contaPrincipal == null) {
			try {
				contaPrincipal = contaRepository.findAllActive().get(0);
				contas.add(contaPrincipal.getId());
			} catch(Exception e) {
				
			}
			
		} else {
			contas.add(contaPrincipal.getId());
		}
		
		// Cartoes Credito
		List<Long> cartoesCredito = new ArrayList<Long>();
		CartaoCredito cartaoCreditoPrincipal = cartaoCreditoRepository.findPrincipal();
		if(cartaoCreditoPrincipal == null) {
			try {
				cartaoCreditoPrincipal = cartaoCreditoRepository.findAllActive().get(0);
				cartoesCredito.add(cartaoCreditoPrincipal.getId());
			} catch(Exception e) { e.printStackTrace();}
			
		} else {
			
			cartoesCredito.add(cartaoCreditoPrincipal.getId());
		}
		
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("sum", sum)
			.include("sum_des", sum_des)
			.include("sum_rec", sum_rec)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("dataDe", dataDe)
			.include("dataAte", dataAte)
			.include("contaPrincipal", contaPrincipal)
			.include("contaList", contaRepository.findAllActive())
			.include("contaSaldo", getContaSaldo(contas))
			.include("cartaoCreditoPrincipal", cartaoCreditoPrincipal)
			.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive())
			.include("cartaoCreditoSaldo", getCartaoCreditoLimite(cartoesCredito))
			.include("usuarioList", pessoaRepository.findAllUsuarioActive());
		
		try {
			result.include("relList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		
		result.forwardTo("/WEB-INF/jsp/admin/" + jsp + ".jsp");
		
	}
	
	@Get
	public void contaSaldo(List<Long> contas) {
		try {
			result.use(Results.json()).withoutRoot().from(getContaSaldo(contas)).serialize();
		} catch(NoResultException e) {
			result.nothing();
		}
	}
	
	@Get
	public void loadContaSaldo(List<Long> contas) {
		result
			.include("contas", contas)
			.include("contaList", contaRepository.findAllActive())
			.include("contaSaldo", getContaSaldo(contas))
			.forwardTo("/WEB-INF/jsp/admin/contas.jsp");
	}
		
	private Map<String, Object> getContaSaldo(List<Long> contas) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("SUM(c.saldoAtual) saldo, ");
		sqlBuilder.append("SUM(COALESCE(c.saldoLimite, 0)) saldoLimite, ");
		sqlBuilder.append("SUM(COALESCE(c.saldoLimiteDisp, 0)) saldoLimiteDisp ");
		sqlBuilder.append("FROM Conta c ");
		sqlBuilder.append("WHERE c.cliente_id = ?1 ");
		
		if(contas != null && !contas.isEmpty()) {
			sqlBuilder.append("AND c.id IN(" + StringUtils.join(contas.toArray(), ", ") + ") ");
		} else {
			sqlBuilder.append("AND c.id IS NULL ");
		}
		
		Query query = contaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		try {
			return (Map<String, Object>) query.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}
		
	@Get
	public void cartaoCreditoLimite(List<Long> cartoesCredito) {
		try {
			result.use(Results.json()).withoutRoot().from(getCartaoCreditoLimite(cartoesCredito)).serialize();
		} catch(NoResultException e) {
			result.nothing();
		}
	}
	
	@Get
	public void loadCartaoCreditoLimite(List<Long> cartoesCredito) {
		result
			.include("cartoesCredito", cartoesCredito)
			.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive())
			.include("cartaoCreditoSaldo", getCartaoCreditoLimite(cartoesCredito))
			.forwardTo("/WEB-INF/jsp/admin/cartoesCredito.jsp");
	}
	
	private Map<String, Object> getCartaoCreditoLimite(List<Long> cartoesCredito) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("SUM(c.limite) limite, ");
		sqlBuilder.append("SUM(c.limiteDisponivel) limiteDisponivel, ");
		sqlBuilder.append("SUM(c.limiteUtilizado) limiteUtilizado ");
		sqlBuilder.append("FROM CartaoCredito c ");
		sqlBuilder.append("WHERE c.cliente_id = ?1 ");
		
		if(cartoesCredito != null && !cartoesCredito.isEmpty()) {
			sqlBuilder.append("AND c.id IN(" + StringUtils.join(cartoesCredito.toArray(), ", ") + ") ");
		} else {
			sqlBuilder.append("AND c.id IS NULL ");
		}
		
		Query query = contaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		try {
			return (Map<String, Object>) query.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	
	@AllowCliente
	@AllowOnInactive
	@Get("/assinatura")
	public void assinatura(boolean isReativar) {
		
		if(isReativar) {
			Assinatura assinatura = assinaturaRepository.findBy(usuarioLogged.getCliente());
			usuarioLogged.getCliente().setAssinatura(assinatura);
		}
		
		result
			.include("isReativar", isReativar)
			.include("planoRecurso", planoRecurso)
			.include("assinaturaPagamentoList", assinaturaPagamentoRepository.findAllByClient());
		
	}
	
	@AllowCliente
	@Post
	public void cancelarAssinatura() {
		
		try {
			ManageRecurringPaymentsProfileStatusResponseType request = paypalBuilder.changeStatusProfile(usuarioLogged.getCliente().getAssinatura().getProfileId(), StatusChangeActionType.CANCEL);
			
			if(request == null) {
				validator.add(new I18nMessage("assinatura", "assinatura.cancelar.failed"));
			} else {
				
				if(request.getAck().equals(AckCodeType.FAILURE) || request.getAck().equals(AckCodeType.FAILUREWITHWARNING)) {
					validator.add(new I18nMessage("assinatura", "assinatura.cancelar.failed"));
				}
				
			}
			
		} catch (SSLConfigurationException | InvalidCredentialException
				| HttpErrorException | InvalidResponseDataException
				| ClientActionRequiredException | MissingCredentialException
				| OAuthException | IOException | InterruptedException
				| ParserConfigurationException | SAXException e) {
			validator.add(new I18nMessage("assinatura", "assinatura.cancelar.failed"));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("cancelarSuccess").withoutRoot().from(StringUtils.EMPTY).serialize();
	}
	
	@AllowCliente
	@AllowOnInactive
	@Get("/assinatura/plano/migrar")
	public void migrarOutroPlano() {
		
		result
			.include("planoRecurso", planoRecurso)
			.include("planoList", planoRepository.findAllActive());
		
	}
	
	@AllowCliente
	@AllowOnInactive
	@Get("/assinatura/plano/{plano.id}/migrar")
	public void migrar(@Load Plano plano) {
		
		// Verificar se o plano nao esta inativo
		if(!plano.getStatus().equals(Status.ATI)) {
			validator.add(new I18nMessage("plano", "plano.invalido"));
		}
			
		// Verifica se eh possivel migrar para o Plano Selecionado
		if(!verificaPlano(plano)) {
			validator.add(new I18nMessage("plano", "plano.migrar.invalido", plano.getNome()));
		}
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		
		
		AssinaturaPagamento assinaturaPagamento = new AssinaturaPagamento();
		assinaturaPagamento.setAssinatura(usuarioLogged.getCliente().getAssinatura());
		assinaturaPagamento.setCriacao(LocalDateTime.now());
		assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.STD);
		assinaturaPagamento.setLandingPageType(LandingPageType.LOGIN);
		assinaturaPagamento.setPlano(plano);
		assinaturaPagamento.setValor(plano.getValor());
			
		assinaturaPagamentoRepository.save(assinaturaPagamento);
		
		paypalBuilder
			.setNome(plano.getNome())
			.with(assinaturaPagamento)
			.setConfirmaURL(environment.get("paypal.assinatura.migrar.confirmaUrl"))
			.setCancelaURL(environment.get("paypal.assinatura.migrar.cancelaUrl"))
			.setExpressCheckout();
		
		// Gerar registro Migrar
		Migrar migrar = new Migrar();
		migrar.setAssinatura(usuarioLogged.getCliente().getAssinatura());
		migrar.setPlano(plano);
		migrar.setStatus(Status.STD);
		migrar.setOldProfileId(usuarioLogged.getCliente().getAssinatura().getProfileId());
		migrar.setToken(assinaturaPagamento.getToken());
		migrarRepository.save(migrar);
		
		// Salva assinatura com o Token
		assinaturaPagamento.setAlteracao(LocalDateTime.now());
		assinaturaPagamentoRepository.save(assinaturaPagamento);
		
		// Recupera Token do Paypal
		if(StringUtils.isBlank(assinaturaPagamento.getToken())) {
			validator.add(new I18nMessage("assinaturaPagamento", "token.notBlank"));
		}
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("migrarSuccess").withoutRoot().from(assinaturaPagamento).excludeAll().include("token").serialize();
		
	}
	
	@AllowCliente
	@AllowOnInactive
	@Get("/assinatura/plano/migrar/confirmar")
	public void migrarConfirmar(String token, String PayerID) {
		
		Migrar migrar = migrarRepository.findByToken(token);
		AssinaturaPagamento assinaturaPagamento = assinaturaPagamentoRepository.findByToken(token);
		
		if(assinaturaPagamento == null) {
			result.notFound();
		} else {
			
			if(migrar.getStatus().equals(Status.ATI)) {
				result.include("successo", true).include("confirmado", true);
			} else {
				boolean isSucesso = true;
				if(BooleanUtils.isNotTrue(assinaturaPagamento.getIsProcessado())) {
					assinaturaPagamento.setIsProcessado(true);
					CreateRecurringPaymentsProfileResponseType res = paypalBuilder.setNome(migrar.getPlano().getNome()).with(assinaturaPagamento).createRecurringPaymentsProfile();
					if(res == null || !res.getAck().equals(AckCodeType.SUCCESS)) {
						isSucesso = false;
					} else if(res.getAck().equals(AckCodeType.SUCCESS)) {
						migrar.setProfileId(res.getCreateRecurringPaymentsProfileResponseDetails().getProfileID());
						migrarRepository.save(migrar);
						usuarioLogged.getCliente().getAssinatura().setPlano(migrar.getPlano());
						usuarioLogged.getCliente().getAssinatura().setProfileId(res.getCreateRecurringPaymentsProfileResponseDetails().getProfileID());
						assinaturaRepository.save(usuarioLogged.getCliente().getAssinatura());
					}
				}
				assinaturaPagamento.setAlteracao(LocalDateTime.now());
				assinaturaPagamentoRepository.save(assinaturaPagamento);
				result.include("successo", isSucesso).include("confirmado", false).include("assinatura", assinaturaPagamento.getAssinatura());
			}
		
		}
		
	}
	
	@AllowCliente
	@AllowOnInactive
	@Post
	public void reativarAssinatura() {
		
		Assinatura assinatura = usuarioLogged.getCliente().getAssinatura();
		Plano plano = assinatura.getPlano();
		
		// Verifica se a Assinatura ja esta Ativa
		if(assinatura.getStatus().equals(AssinaturaStatus.ATI)) {
			validator.add(new I18nMessage("assinatura", "assinatura.reativar.assinaturaStatus.ATI"));
		}
		
		// Verificar se o plano nao esta inativo
		if(!plano.getStatus().equals(Status.ATI)) {
			validator.add(new I18nMessage("plano", "plano.invalido"));
		}
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		
		AssinaturaPagamento assinaturaPagamento = new AssinaturaPagamento();
		assinaturaPagamento.setAssinatura(usuarioLogged.getCliente().getAssinatura());
		assinaturaPagamento.setCriacao(LocalDateTime.now());
		assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.STD);
		assinaturaPagamento.setLandingPageType(LandingPageType.LOGIN);
		assinaturaPagamento.setPlano(plano);
		assinaturaPagamento.setValor(plano.getValor());
			
		assinaturaPagamentoRepository.save(assinaturaPagamento);
		
		paypalBuilder
			.setNome(plano.getNome())
			.with(assinaturaPagamento)
			.setConfirmaURL(environment.get("paypal.assinatura.reativar.confirmaUrl"))
			.setCancelaURL(environment.get("paypal.assinatura.reativar.cancelaUrl"))
			.setExpressCheckout();
		
		// Salva assinatura com o Token
		assinaturaPagamento.setAlteracao(LocalDateTime.now());
		assinaturaPagamentoRepository.save(assinaturaPagamento);
				
		// Recupera Token do Paypal
		if(StringUtils.isBlank(assinaturaPagamento.getToken())) {
			validator.add(new I18nMessage("assinaturaPagamento", "token.notBlank"));
		}
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("reativarSuccess").withoutRoot().from(assinaturaPagamento).excludeAll().include("token").serialize();
		
	}
	
	@AllowCliente
	@AllowOnInactive
	@Get("/assinatura/reativar/confirmar")
	public void reativarConfirmar(String token, String PayerID) {
		
		AssinaturaPagamento assinaturaPagamento = assinaturaPagamentoRepository.findByToken(token);
		
		if(assinaturaPagamento == null) {
			result.notFound();
		} else {
			Assinatura assinatura = assinaturaPagamento.getAssinatura();
			if(assinatura.getStatus().equals(AssinaturaStatus.ATI)) {
				result.include("successo", true).include("confirmado", true);
			} else {
				boolean isSucesso = true;
				if(BooleanUtils.isNotTrue(assinaturaPagamento.getIsProcessado())) {
					assinaturaPagamento.setIsProcessado(true);
					CreateRecurringPaymentsProfileResponseType res = paypalBuilder.setNome(assinatura.getPlano().getNome()).with(assinaturaPagamento).createRecurringPaymentsProfile();
					if(res == null || !res.getAck().equals(AckCodeType.SUCCESS)) {
						isSucesso = false;
					} else if(res.getAck().equals(AckCodeType.SUCCESS)) {
						assinatura.setProfileId(res.getCreateRecurringPaymentsProfileResponseDetails().getProfileID());
						assinatura.setStatus(AssinaturaStatus.ATI);
						assinaturaRepository.save(assinatura);
					}
				}
				assinaturaPagamento.setAlteracao(LocalDateTime.now());
				assinaturaPagamentoRepository.save(assinaturaPagamento);
				result.include("successo", isSucesso).include("confirmado", false).include("assinatura", assinaturaPagamento.getAssinatura());
			}
		
		}
		
	}
	
	
	@AllowOnInactive
	@Get("/sair")
	public void logout() {
		usuarioLogged.logout();
		session.invalidate();
		result.redirectTo(LoginController.class).login(null);
	}
	
	private boolean verificaPlano(Plano plano) {
		
		Long qtdeConta = assinaturaRepository.qtdeConta();
		Long qtdeCartaoCredito = assinaturaRepository.qtdeCartaoCredito();
		Integer qtdeLancamento = assinaturaRepository.qtdeLancamento();
		BigDecimal tablesSize = assinaturaRepository.tablesSize();
		Long sizeContainer = cloudFiles.getContainerSize("cliente_" + usuarioLogged.getCliente().getId());
		Long qtdeUsuarios = assinaturaRepository.qtdeUsuarios();
		
		if(qtdeConta > plano.getContas()) {
			return false;
		} else if(qtdeCartaoCredito > plano.getCartoesCredito()) {
			return false;
		} else if(qtdeLancamento > plano.getLancamentos()) {
			return false;
		} else if((tablesSize.intValue() / 1024 / 1024) > plano.getBancoDados()) {
			return false;
		} else if((sizeContainer / 1024 / 1024) > plano.getDisco()) {
			return false;
		} else if(qtdeUsuarios > plano.getUsuarios()) {
			return false;
		} else if(plano.equals(usuarioLogged.getCliente().getAssinatura().getPlano())) {
			return false;
		}
		
		return true;
	}
	
}
