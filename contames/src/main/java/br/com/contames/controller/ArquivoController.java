package br.com.contames.controller;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.interceptor.download.InputStreamDownload;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoHelper;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class ArquivoController {

	private final ArquivoRepository arquivoRepository;
	private final ArquivoHelper arquivoHelper;
	private final ArquivoSession arquivoSession;
	private final UsuarioLogged usuarioLogged;
	private final Environment environment;
	private final HttpSession session;
	private final Result result;
	private final Validator validator;
	private final PlanoRecurso planoRecurso;
	private final Settings settings;
	
	public ArquivoController(ArquivoRepository arquivoRepository, ArquivoHelper arquivoHelper, ArquivoSession arquivoSession, UsuarioLogged usuarioLogged, Environment environment, HttpSession session, Result result, Validator validator, PlanoRecurso planoRecurso, Settings settings) {
		this.arquivoRepository = arquivoRepository;
		this.arquivoHelper = arquivoHelper;
		this.arquivoSession = arquivoSession;
		this.usuarioLogged = usuarioLogged;
		this.environment = environment;
		this.session = session;
		this.result = result;
		this.validator = validator;
		this.planoRecurso = planoRecurso;
		this.settings = settings;
	}
	
	
	@Get
	public void dialogAnexo(Arquivo arquivo, String viewid) {
		result
			.include("arquivo", arquivo)
			.include("viewid", viewid);
	}
	
	
	@Post
	public Arquivo dialogAnexoUpload(@NotNull(message = "{arquivo.notNull}") Arquivo arquivo, UploadedFile uploadedFile, boolean isExcluir, String viewid) {
		
		if(arquivo != null) {
			
			if(uploadedFile != null && (planoRecurso.getArmazenamentoUtil() + (uploadedFile.getSize() / 1024 / 1024)) > planoRecurso.getArmazenamento()) {
				validator.add(new I18nMessage("arquivo", "arquivo.uploaded.espacoInsuficiente", planoRecurso.getPlano().getNome(), planoRecurso.getArmazenamento()));
			}
			
			if(viewid != null) {
				
				List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
				
				// Verificar se existe outro arquivo com o mesmo nome
				Arquivo outro = selectFirst(arquivoList, having(on(Arquivo.class).getNome(), equalTo(uploadedFile.getFileName())));
				if(outro != null) {
					validator.add(new I18nMessage("arquivo.nome", "arquivo.nome.existe"));
				}
				
			}
			
			if(!validator.hasErrors()) {
				if(uploadedFile == null && isExcluir) {
					arquivoHelper.deleteTemp(arquivo);
					if(viewid != null) {
						arquivoSession.remove(viewid, arquivo);
					}
				} else {
					arquivoHelper.saveTemp(uploadedFile, arquivo);
					
					/*try {
						Thumbnails
							.of(uploadedFile.getFile())
							.size(100, 100)
							.toFile(new File("c:/Users/Rodrigo Luengo/Desktop/" + uploadedFile.getFileName()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					if(viewid != null) {
						arquivoSession.getArquivoList(viewid).add(arquivo);
					}
				}
			}
		}
		
		validator.onErrorForwardTo(this).errorJSIFrame();
		
		return arquivo;
	}
	
	public void errorJSIFrame() { }
	
	
	@Get({"/download/{arquivo.temp}/{arquivo.nome}", "/download/{arquivo.temp}/{arquivo.id}/{arquivo.nome}", "/arquivo/{arquivo.id}/download/{arquivo.nome}"})
	public Download download(@NotNull Arquivo arquivo) {
		
		if(arquivo.getId() != null) {
			if(!arquivoRepository.exists(arquivo)) {
				result.notFound();
			}
		}
		
		if(validator.hasErrors()) {
			result.notFound();
		} else {
			InputStreamDownload inputStreamDownload = arquivoHelper.downloadTemp(arquivo);
			if(inputStreamDownload == null) {
				result.notFound();
			} else {
				return inputStreamDownload;
			}
		}
		return null;
	}
	
	
	@Get
	public void consultar(Boolean isTable, String viewid, String search, List<OrderBy> orderByList, Integer page) {
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		
		if(StringUtils.isNotBlank(search)) {
			arquivoList = select(arquivoList, having(on(Arquivo.class).getNomeClear(), containsString(StringHelper.unAccent(search))));
		}
		
		Collections.sort(arquivoList, new ArquivoComparator(orderByList));
		
		Long count = Long.decode(String.valueOf(arquivoList.toArray().length));
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		int paginationSize = 0;
		if(count > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
		
		result
			.include("arquivoList", ArrayUtils.subarray(arquivoList.toArray(), firstResult, ((int) pageSize) * paginationSize))
			.include("viewid", viewid)
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.forwardTo("/WEB-INF/jsp/arquivo/" + jsp + ".jsp");
	}
	
	@Get
	public void quantidade(String viewid) {
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		result.use(Results.http()).body(String.valueOf(arquivoList.size()));
	}
	
	public void excluir(String viewid, List<String> ids) {
		
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		
		for(String id : ids) {
			if(NumberUtils.isNumber(id)) {
				Arquivo arquivo = selectFirst(arquivoList, having(on(Arquivo.class).getId(), equalTo(Long.decode(id))));
				arquivoDeleteList.add(arquivo);
			} else {
				Arquivo arquivo = selectFirst(arquivoList, having(on(Arquivo.class).getTemp(), equalTo(id)));
				arquivoDeleteList.add(arquivo);
			}
		}
		arquivoList.removeAll(arquivoDeleteList);
		
		result.use(Results.jsonp()).withCallback("arquivoConsultarExcluirSuccess").from(ids).serialize();
	}
		
}

class ArquivoComparator implements Comparator<Arquivo> {

	private final List<OrderBy> orderByList;
	
	public ArquivoComparator(List<OrderBy> orderByList) {
		this.orderByList = orderByList;
	}
	
	@Override
	public int compare(Arquivo arquivo1, Arquivo arquivo2) {
		
		if(orderByList.size() == 2) {
			OrderBy orderByNome = orderByList.get(0);
			OrderBy orderBySize = orderByList.get(1);
			int nome = orderByNome.getDirection().equals(OrderByDirection.ASC) ? arquivo1.getNome().compareToIgnoreCase(arquivo2.getNome()) : arquivo2.getNome().compareToIgnoreCase(arquivo1.getNome());
			int size = orderBySize.getDirection().equals(OrderByDirection.ASC) ? arquivo1.getSize().compareTo(arquivo2.getSize()) : arquivo2.getSize().compareTo(arquivo1.getSize());
			
			if(nome == 0) {
				return size;
			} else {
				return nome;
			}
		} else {
			OrderBy orderBy = orderByList.get(0);
			if(orderBy.getColumn().equals("nome")) {
				return orderBy.getDirection().equals(OrderByDirection.ASC) ? arquivo1.getNome().compareToIgnoreCase(arquivo2.getNome()) : arquivo2.getNome().compareToIgnoreCase(arquivo1.getNome());
			} else {
				return orderBy.getDirection().equals(OrderByDirection.ASC) ? arquivo1.getSize().compareTo(arquivo2.getSize()) : arquivo2.getSize().compareTo(arquivo1.getSize());
			}
		}
		
	}
	
}
