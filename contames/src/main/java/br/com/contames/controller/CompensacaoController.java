package br.com.contames.controller;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.equalTo;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.Compensacao;
import br.com.contames.entity.CompensacaoItem;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Item;
import br.com.contames.entity.Pessoa;
import br.com.contames.entity.Transferencia;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.BaixaItemRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.CompensacaoItemRepository;
import br.com.contames.repository.CompensacaoRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.ItemRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.repository.TransferenciaRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class CompensacaoController {
	
	private final CompensacaoRepository compensacaoRepository;
	private final CompensacaoItemRepository compensacaoItemRepository;
	private final BaixaItemRepository baixaItemRepository;
	private final ContaRepository contaRepository;
	private final PessoaRepository pessoaRepository;
	private final ItemRepository itemRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final TransferenciaRepository transferenciaRepository;
	private final Result result;
	private final Validator validator;
	private final Localization localization;
	private final UsuarioLogged usuarioLogged;
	private final Settings settings;
	private final HttpSession session;
	private final PlanoRecurso planoRecurso;
	private final ArquivoRepository arquivoRepository;
	private final ArquivoSession arquivoSession;
	
	private CompensacaoController(CompensacaoRepository compensacaoRepository, CompensacaoItemRepository compensacaoItemRepository, BaixaItemRepository baixaItemRepository, ContaRepository contaRepository, PessoaRepository pessoaRepository, ItemRepository itemRepository, CartaoCreditoRepository cartaoCreditoRepository, TransferenciaRepository transferenciaRepository, Result result, Validator validator, Localization localization, UsuarioLogged usuarioLogged, Settings settings, HttpSession session, PlanoRecurso planoRecurso, ArquivoRepository arquivoRepository, ArquivoSession arquivoSession) {
		this.compensacaoRepository = compensacaoRepository;
		this.compensacaoItemRepository = compensacaoItemRepository;
		this.baixaItemRepository = baixaItemRepository;
		this.contaRepository = contaRepository;
		this.pessoaRepository = pessoaRepository;
		this.itemRepository = itemRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.transferenciaRepository = transferenciaRepository;
		this.result = result;
		this.validator = validator;
		this.localization = localization;
		this.usuarioLogged = usuarioLogged;
		this.settings = settings;
		this.session = session;
		this.planoRecurso = planoRecurso;
		this.arquivoRepository = arquivoRepository;
		this.arquivoSession = arquivoSession;
		
	}
	
	@Get("/lancamentos/conciliacao")
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page) { 
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.DESC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
				
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT com.id, ");
		sqlBuilder.append("com._data, ");
		sqlBuilder.append("con.nome conta, ");
		sqlBuilder.append("com.quantCompensacaoItem, ");
		sqlBuilder.append("com.saldoAnterior, ");
		sqlBuilder.append("com.totalCredito, ");
		sqlBuilder.append("com.totalDebito, ");
		sqlBuilder.append("com.saldo, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN com.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN com.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
		sqlBuilder.append("WHEN com.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status, ");
		sqlBuilder.append("com.obs, ");
		sqlBuilder.append("CASE WHEN COALESCE(con.isPrincipal, false) THEN 0 ELSE 1 END order_fav ");
		sqlBuilder.append("FROM Compensacao com, Conta con ");
		sqlBuilder.append("WHERE com.cliente_id = ?1 AND com.conta_id = con.id ");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("CAST(com._data AS VARCHAR) ILIKE ?2 ");
			sqlBuilder.append("OR unaccent(LOWER(con.nome)) ILIKE ?2 ");
			sqlBuilder.append("OR CAST(com.quantCompensacaoItem AS VARCHAR) ILIKE ?2 ");
			sqlBuilder.append("OR CAST(com.saldoAnterior AS VARCHAR) ILIKE ?2 ");
			sqlBuilder.append("OR CAST(com.totalCredito AS VARCHAR) ILIKE ?2 ");
			sqlBuilder.append("OR CAST(com.totalDebito AS VARCHAR) ILIKE ?2 ");
			sqlBuilder.append("OR CAST(com.saldo AS VARCHAR) ILIKE ?2 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE ");
			sqlBuilder.append("WHEN com.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
			sqlBuilder.append("WHEN com.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
			sqlBuilder.append("WHEN com.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
			sqlBuilder.append("END)) ILIKE ?2 ");
			sqlBuilder.append(")");
		}
				
		Query countQuery = compensacaoRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Compensacao");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(2, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY order_fav, ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
						
		Query query = compensacaoRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(2, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("compensacaoList", query.getResultList());
		} catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/compensacao/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(Long id, Long contaId) {
		
		// Verifica espaco no banco de dados
		if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
			validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		}
		
		// Gera viewid
		String viewid = StringHelper.createViewId();
		
		Compensacao compensacao = null;
		List<CompensacaoItem> compensacaoItemList = null;
		boolean isClosed = false;
		
		if(id == null) {
			
			Conta conta = null;
			
			if(contaId == null) {
				conta = contaRepository.findUnique();
			} else {
				conta = contaRepository.findActive(contaId);
			}
			
			if(conta == null) {
				result.forwardTo(ContaController.class).dialogSelect(null, true, null, false);
			} else {
				
				// Recupera a compensacao anterior para esta conta
				Compensacao anterior = compensacaoRepository.findAnterior(conta);
				
				if(anterior != null) {
				
					// Verificar se existe compensacao stand by
					if(LancamentoStatus.STD.equals(anterior.getStatus())) {
						validator.add(new I18nMessage("compensacao", "compensacao.anterior.status.STD"));
					}
					
					// Verificar se existe compensacao com a data de hoje
					if(anterior.getData().equals(LocalDate.now())) {
						validator.add(new I18nMessage("compensacao", "compensacao.data.anterior.igual", anterior.getData().toString(localization.getMessage("patternDate"))));
					}
				
				}
				
				if(!validator.hasErrors()) {
					
					LocalDate data = compensacaoRepository.minData(conta);
					
					
					compensacao = new Compensacao();
					compensacao.setStatus(LancamentoStatus.COM);
					compensacao.setData(data);
					compensacao.setConta(conta);
					compensacao.setSaldoAnterior(conta.getSaldoAtual());
					compensacao.setSaldo(conta.getSaldoAtual());
					compensacao.setDiferenca(0.0);
					session.setAttribute("compensacao_" + viewid, compensacao);
					
					compensacaoItemList = new ArrayList<CompensacaoItem>();
					
					session.setAttribute("compensacaoItemList_" + viewid, compensacaoItemList);
				
				}
			}
				
		} else {
			
			// Carregar Compensacao do Banco de Dados
			compensacao = compensacaoRepository.find(id);
			if(compensacao == null) {
				validator.add(new I18nMessage("compensacao", "compensacao.notFound"));
			} else {
				session.setAttribute("compensacao_" + viewid, compensacao);
				
				compensacaoItemList = compensacaoItemRepository.findAll(compensacao.getId());
				session.setAttribute("compensacaoItemList_" + viewid, compensacaoItemList);
				
				if(LancamentoStatus.STD.equals(compensacao.getStatus()) && !contaRepository.isCompensado(compensacao.getConta())) {
					compensacao.setSaldoAnterior(compensacao.getConta().getSaldoAtual());
					compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
				}
				
				isClosed = compensacaoRepository.isClosed(compensacao);
				
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		session.setAttribute("isClosed_" + viewid, isClosed);
		
		arquivoSession.setAquivoList(viewid, compensacao.getArquivoList());
		
		result
			.include("viewid", viewid)
			.include("compensacao", compensacao)
			.include("compensacaoItemList", compensacaoItemList)
			.include("isClosed", isClosed);
	}
	
	
	
	@Get
	public void movs(
			Boolean isTable, 
			@NotNull(message = "{dataDe.notNull}")
			LocalDate dataDe, 
			@NotNull(message = "{dataAte.notNull}")
			LocalDate dataAte, 
			List<Long> baixaItemIdsNotIn,
			List<Long> transferenciaIdsNotIn,
			String search,
			@NotNull(message = "{conta.notNull}")
			Long conta,
			List<OrderBy> orderByList,
			Integer page) {
		
		if(dataDe != null && dataAte != null) {
			
			// Verificar se a dataAte eh inferior a dataDe
			if(dataAte.isBefore(dataDe)) {
				validator.add(new I18nMessage("dataAte", "dataAte.notIsBefore.dataDe"));
			}
			
			// Verifica se a diferenca entre dataDe e dataAte eh maior que 12 meses
			if(Months.monthsBetween(dataDe, dataAte).getMonths() > 12) {
				validator.add(new I18nMessage("dataAte", "dataAte.months.notMore12Mounths"));
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		String jsp = isTable == null || !isTable ? "movs" : "movs_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		
		
		// BaixaItem
		
		sqlBuilder.append("SELECT bi.baixaItemId, ");
		sqlBuilder.append("NULL transferenciaId, ");
		sqlBuilder.append("bi._data, ");
		sqlBuilder.append("bi.favorecido || COALESCE(' - ' || bi.documento, '') || COALESCE(' - ' || bi.meio, '') || COALESCE(' - ' || bi.parcela, '') descricao, ");
		sqlBuilder.append("bi.valor, ");
		sqlBuilder.append("bi.tipo ");
		sqlBuilder.append("FROM ( ");
		
		sqlBuilder.append("SELECT bi.id baixaItemId, ");
		sqlBuilder.append("COALESCE(ch._data, b._data) _data, ");
		
		sqlBuilder.append("COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome) favorecido, ");
		
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || ' - ' || l.numerodoc, CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, l.numerodoc) || COALESCE(' ' || cc.nome, '') documento, ");
		sqlBuilder.append("CASE WHEN l.quantLancamentoParcela = 1 THEN NULL ELSE (lp.seq + 1) || ' / ' || l.quantLancamentoParcela END parcela, ");
		
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN bi.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
		sqlBuilder.append("WHEN bi.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
		sqlBuilder.append("WHEN bi.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
		sqlBuilder.append("WHEN bi.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
		sqlBuilder.append("WHEN bi.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
		sqlBuilder.append("WHEN bi.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
		sqlBuilder.append("WHEN bi.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
		sqlBuilder.append("END || COALESCE(' ' || banco.fantasia, ' ' || banco.razaoSocial, '') || COALESCE(' ' || lpcc.nome, '') || COALESCE(' ' || ch.numero, '') meio, ");
		
		sqlBuilder.append("bi.valor, ");
		sqlBuilder.append("lp.tipo ");
		sqlBuilder.append("FROM BaixaItem bi ");
		sqlBuilder.append("INNER JOIN Baixa b ON bi.baixa_id = b.id ");
		sqlBuilder.append("INNER JOIN LancamentoParcela lp ON bi.lancamentoParcela_id = lp.id ");
		sqlBuilder.append("INNER JOIN Lancamento l ON lp.lancamento_id = l.id ");
		sqlBuilder.append("INNER JOIN Pessoa p ON l.favorecido_id = p.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON l.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito lpcc ON COALESCE(bi.cartaoCredito_id, lp.cartaoCredito_id) = lpcc.id ");
		sqlBuilder.append("LEFT JOIN Conta c ON COALESCE(bi.conta_id, lp.conta_id) = c.id ");
		sqlBuilder.append("LEFT JOIN Cheque ch ON COALESCE(bi.cheque_id, lp.cheque_id) = ch.id ");
		sqlBuilder.append("LEFT JOIN Pessoa banco ON ch.banco_id = banco.id ");
		sqlBuilder.append("LEFT JOIN TalaoCheque tc ON ch.talaoCheque_id = tc.id ");
		sqlBuilder.append("LEFT JOIN Conta tcc ON tc.conta_id = tcc.id ");
		
		sqlBuilder.append("WHERE b.cliente_id = ?1 ");
		sqlBuilder.append("AND b.status = ?2 ");
		sqlBuilder.append("AND bi.status = ?2 ");
		sqlBuilder.append("AND COALESCE(ch._data, b._data) BETWEEN ?3 AND ?4 AND (bi.conta_id = ?5 OR tc.conta_id = ?5) ");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append("AND (unaccent(LOWER(COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome))) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("END || ' - ' || l.numerodoc, CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("END, l.numerodoc))) ILIKE ?6 ");
			sqlBuilder.append("OR CASE WHEN l.quantLancamentoParcela = 1 THEN '' ELSE (lp.seq + 1) || ' / ' || l.quantLancamentoParcela END ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE ");
			sqlBuilder.append("WHEN bi.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
			sqlBuilder.append("WHEN bi.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
			sqlBuilder.append("WHEN bi.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
			sqlBuilder.append("WHEN bi.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
			sqlBuilder.append("WHEN bi.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
			sqlBuilder.append("WHEN bi.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
			sqlBuilder.append("WHEN bi.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
			sqlBuilder.append("END || COALESCE(' ' || banco.fantasia, ' ' || banco.razaoSocial, '') || COALESCE(' ' || lpcc.nome, '') || COALESCE(' ' || ch.numero, ''))) ILIKE ?6 ");
			sqlBuilder.append("OR CAST(bi.valor AS VARCHAR) ILIKE ?6) ");
		}
		
		Integer indexPosition = 7;
		if(baixaItemIdsNotIn != null && !baixaItemIdsNotIn.isEmpty()) {
			for(int i = 0; i < baixaItemIdsNotIn.size(); i++) {
				sqlBuilder.append(" AND bi.id <> ?" + (i + 7));
			}
			indexPosition += baixaItemIdsNotIn.size();
		}
		
		sqlBuilder.append(") bi ");
		
		// Transferencia
		sqlBuilder.append("UNION SELECT NULL baixaItemId, ");
		sqlBuilder.append("t.id transferenciaId, ");
		sqlBuilder.append("t._data, ");
		sqlBuilder.append("t.transferenciaTipo || ' ' || t.origem || ' " + localization.getMessage("p_") + " ' || t.destino descricao, ");
		sqlBuilder.append("t.valor, ");
		sqlBuilder.append("t.tipo ");
		sqlBuilder.append("FROM (");
		
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("t.id, ");
		sqlBuilder.append("t._data, ");
		sqlBuilder.append("CASE WHEN t.statusOrigem = 'COM' OR t.statusDestino = 'COM' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ELSE CASE WHEN t.statusOrigem = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' WHEN t.statusOrigem = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' WHEN t.statusOrigem = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' END END status, ");
		sqlBuilder.append("'" + localization.getMessage("transf_") + "'::text transferenciaTipo, ");
		sqlBuilder.append("COALESCE(c.nome, cc.nome) origem, ");
		sqlBuilder.append("d.nome destino, ");
		sqlBuilder.append("t.valor, ");
		sqlBuilder.append("t.obs, ");
		sqlBuilder.append("CASE WHEN t.conta_id = ?5 THEN 'DES' ELSE 'REC' END tipo ");
		sqlBuilder.append("FROM Transferencia t ");
		sqlBuilder.append("LEFT JOIN Conta c ON t.conta_id = c.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON t.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN Cheque ch ON t.cheque_id = ch.id ");
		sqlBuilder.append("LEFT JOIN TalaoCheque tc ON ch.talaoCheque_id = tc.id ");
		sqlBuilder.append("INNER JOIN Conta d ON t.destino_id = d.id ");
		sqlBuilder.append("WHERE t.cliente_id = ?1 AND t._data BETWEEN ?3 AND ?4 AND (t.statusOrigem = 'LAN' AND (t.conta_id = ?5 OR tc.conta_id = ?5) OR t.statusDestino = 'LAN' AND t.destino_id = ?5)");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("CAST(t._data AS VARCHAR) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN t.statusOrigem = 'COM' OR t.statusDestino = 'COM' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ELSE CASE WHEN t.statusOrigem = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' WHEN t.statusOrigem = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' WHEN t.statusOrigem = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' END END)) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER('" + localization.getMessage("transf_") + "')) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(c.nome, cc.nome))) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(d.nome)) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(t.valor AS VARCHAR))) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(t.obs)) ILIKE ?6 ");
			sqlBuilder.append(")");
		}
		
		if(transferenciaIdsNotIn != null && !transferenciaIdsNotIn.isEmpty()) {
			for(int i = 0; i < transferenciaIdsNotIn.size(); i++) {
				sqlBuilder.append(" AND t.id <> ?" + (i + indexPosition));
			}
		}
		
		sqlBuilder.append(") t");
						
		Query countQuery = compensacaoRepository.getEntityManager().createNativeQuery("SELECT COUNT(_data) FROM(" + sqlBuilder.toString() + ") AS BaixaItem");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, LancamentoStatus.BXD.toString());
		countQuery.setParameter(3, dataDe.toDate());
		countQuery.setParameter(4, dataAte.toDate());
		countQuery.setParameter(5, conta);
		
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(6, "%" + StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		if(baixaItemIdsNotIn != null && !baixaItemIdsNotIn.isEmpty()) {
			for(int i = 0; i < baixaItemIdsNotIn.size(); i++) {
				countQuery.setParameter((i + 7), baixaItemIdsNotIn.get(i));
			}
		}
		
		if(transferenciaIdsNotIn != null && !transferenciaIdsNotIn.isEmpty()) {
			for(int i = 0; i < transferenciaIdsNotIn.size(); i++) {
				countQuery.setParameter((i + indexPosition), transferenciaIdsNotIn.get(i));
			}
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
						
		Query query = compensacaoRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, LancamentoStatus.BXD.toString());
		query.setParameter(3, dataDe.toDate());
		query.setParameter(4, dataAte.toDate());
		query.setParameter(5, conta);
		
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(6, "%" + StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		if(baixaItemIdsNotIn != null && !baixaItemIdsNotIn.isEmpty()) {
			for(int i = 0; i < baixaItemIdsNotIn.size(); i++) {
				query.setParameter((i + 7), baixaItemIdsNotIn.get(i));
			}
		}
		
		if(transferenciaIdsNotIn != null && !transferenciaIdsNotIn.isEmpty()) {
			for(int i = 0; i < transferenciaIdsNotIn.size(); i++) {
				query.setParameter((i + indexPosition), transferenciaIdsNotIn.get(i));
			}
		}
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("movsList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/compensacao/" + jsp + ".jsp");
		
	}
	
	
	@Post
	public void adicionarMovs(String viewid, LocalDate data, List<Long> baixaItemIds, List<Long> transferenciaIds) {
		
		Compensacao compensacao = (Compensacao) session.getAttribute("compensacao_" + viewid);
		boolean isClosed = (boolean) session.getAttribute("isClosed_" + viewid);
		List<CompensacaoItem> compensacaoItemList = (List<CompensacaoItem>) session.getAttribute("compensacaoItemList_" + viewid);

		
		if(isClosed) {
			
			validator.add(new I18nMessage("compensacao", "compensacao.isClosed.movs"));
		
		} else {
		
			
			
			boolean isMovsSelected = false;
			
			// BaixaItem
			if(baixaItemIds != null && !baixaItemIds.isEmpty()) {
				isMovsSelected = true;
				List<BaixaItem> baixaItemList = baixaItemRepository.findAllBxd(baixaItemIds, data);
				
				if(baixaItemList == null || baixaItemList.isEmpty()) {
					validator.add(new I18nMessage("baixaItemList", "baixaItemList.notFound"));
				} else {
								
					for(BaixaItem baixaItem : baixaItemList) {
						CompensacaoItem compensacaoItem = new CompensacaoItem();
						compensacaoItem.setSeq(compensacaoItemList.size());
						compensacaoItem.setBaixaItem(baixaItem);
						
						if(baixaItem.getTipo().equals(ItemTipo.DES)) {
							compensacaoItem.setDebito(baixaItem.getValor());
						} else {
							compensacaoItem.setCredito(baixaItem.getValor());
						}
						compensacaoItem.setTipo(baixaItem.getTipo());
						compensacaoItemList.add(compensacaoItem);
					}
				}
			}
			
			
			// Transferencia
			if(transferenciaIds != null && !transferenciaIds.isEmpty()) {
				isMovsSelected = true;
				List<Transferencia> transferenciaList = transferenciaRepository.findAll(transferenciaIds, data);
				
				for(Transferencia transferencia : transferenciaList) {
					CompensacaoItem compensacaoItem = new CompensacaoItem();
					compensacaoItem.setSeq(compensacaoItemList.size());
					compensacaoItem.setTransferencia(transferencia);
					ItemTipo tipo = transferencia.getDestino().equals(compensacao.getConta()) ? ItemTipo.REC : ItemTipo.DES;
					if(tipo.equals(ItemTipo.DES)) {
						compensacaoItem.setDebito(transferencia.getValor());
					} else {
						compensacaoItem.setCredito(transferencia.getValor());
					}
					compensacaoItem.setTipo(tipo);
					compensacaoItemList.add(compensacaoItem);
				}
			}
			
			if(!isMovsSelected) {
				validator.add(new I18nMessage("movs", "compensacao.movs.notNull"));
			}
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		
		compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
		
		result
			.include("compensacao", compensacao)
			.include("compensacaoItemList", compensacaoItemList)
			.forwardTo("/WEB-INF/jsp/compensacao/compensacaoItemList.jsp");
		
	}
	
	
	public void adicionarCompensacaoItem(String viewid, List<Long> ids, @NotNull(message = "{data.notNull}") LocalDate data) { 
		
		Compensacao compensacao = (Compensacao) session.getAttribute("compensacao_" + viewid);
		List<CompensacaoItem> compensacaoItemList = (List<CompensacaoItem>) session.getAttribute("compensacaoItemList_" + viewid);
		
		boolean isClosed = (boolean) session.getAttribute("isClosed_" + viewid);
		
		if(isClosed) {
			validator.add(new I18nMessage("compensacao", "compensacao.isClosed.movs"));
		} else {
		
			if(ids != null && !ids.isEmpty()) {
				List<Item> itemList = itemRepository.find(ids);
				
				if(itemList != null && !itemList.isEmpty()) {
					for(Item item : itemList) {  
						
						if(Status.INA.equals(item.getStatus())) {
							validator.add(new I18nMessage("item", "adicionar.item." + item.getTipo() + ".status.INA", item.getDisplayName()));
							break;
						}
						
						CompensacaoItem compensacaoItem = new CompensacaoItem();
						compensacaoItem.setSeq(compensacaoItemList.size());
						compensacaoItem.setItem(item);
						compensacaoItem.setData(data);
						compensacaoItem.setTipo(item.getTipo());
						
						Pessoa favorecido = item.getFavorecido();
						if(favorecido == null && ItemTipo.REC.equals(item.getTipo())) {
							favorecido = compensacao.getConta().getTitular() == null ? usuarioLogged.getCliente() : compensacao.getConta().getTitular();
						}
						
						compensacaoItem.setFavorecido(favorecido);
						
						if(ItemTipo.DES.equals(item.getTipo())) {
							compensacaoItem.setDebito(item.getValor());
						} else {
							compensacaoItem.setCredito(item.getValor());
						}
						compensacaoItemList.add(compensacaoItem);
					}
				}
			}
			
			compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		result
			.include("compensacao", compensacao)
			.include("compensacaoItemList", compensacaoItemList)
			.forwardTo("/WEB-INF/jsp/compensacao/compensacaoItemList.jsp");
		
	}
	
	
	@Get
	public void editarCompensacaoItem(String viewid, Integer index) {
		List<CompensacaoItem> compensacaoItemList = (List<CompensacaoItem>) session.getAttribute("compensacaoItemList_" + viewid);
		boolean isClosed = (boolean) session.getAttribute("isClosed_" + viewid);
		
		CompensacaoItem compensacaoItem = compensacaoItemList.get(index);
		
		if(compensacaoItem != null) {
			arquivoSession.setAquivoList(compensacaoItem.getViewid(), compensacaoItem.getArquivoList());
		}
		
		result
			.include("compensacaoItem", compensacaoItem)
			.include("isClosed", isClosed);
	}
	
	
	@Post
	public void editarCompensacaoItem(@NotNull(message = "{compensacaoItem.notNull}") CompensacaoItem compensacaoItem, @NotBlank(message = "{viewid.notBlank}") String viewid, @NotNull(message = "{data.notNull}") LocalDate data) {
		
		Compensacao compensacao = (Compensacao) session.getAttribute("compensacao_" + viewid);
		List<CompensacaoItem> compensacaoItemList = (List<CompensacaoItem>) session.getAttribute("compensacaoItemList_" + viewid);
		boolean isClosed = (boolean) session.getAttribute("isClosed_" + viewid);
		
		if(compensacaoItem != null && viewid != null) {
			if(compensacao == null) {
				validator.add(new I18nMessage("compensacao", "compensacao.notNull"));
			} else {
			
				CompensacaoItem _compensacaoItem = selectFirst(compensacaoItemList, having(on(CompensacaoItem.class).getSeq(), equalTo(compensacaoItem.getSeq())));
				
				if(isClosed) {
					data = compensacao.getData();
					compensacaoItem.setData(_compensacaoItem.getData());
					compensacaoItem.setSeq(_compensacaoItem.getSeq());
					compensacaoItem.setDebito(_compensacaoItem.getDebito());
					compensacaoItem.setCredito(_compensacaoItem.getCredito());
					compensacaoItem.setSaldo(_compensacaoItem.getSaldo());
				}
				
				validator.validateProperties(compensacaoItem, "seq", "debito", "credito", "obs");
				
				
				if(compensacaoItem.getData().isAfter(data)) {
					validator.add(new I18nMessage("data", "data.notAfter.compensacao.data", data.toString(localization.getMessage("patternDate"))));
				}
				
				if(_compensacaoItem.getItem().getTipo().equals(ItemTipo.DES)) {
					if(compensacaoItem.getDebito() == null) {
						validator.add(new I18nMessage("debito", "debito.notNull"));
					} else if(compensacaoItem.getDebito() == 0) {
						validator.add(new I18nMessage("debito", "valor.notEqualZero"));
					}
					
					compensacaoItem.setCredito(null);
				} else {
					if(compensacaoItem.getCredito() == null) {
						validator.add(new I18nMessage("credito", "credito.notNull"));
					} else if(compensacaoItem.getCredito() == 0) {
						validator.add(new I18nMessage("credito", "valor.notEqualZero"));
					}
					compensacaoItem.setDebito(null);
				}
				
				if(compensacaoItem.getItem() == null || compensacaoItem.getItem().getId() == null) {
					validator.add(new I18nMessage("item", "item." + compensacaoItem.getTipo() + ".notNull"));
				}
				
				
				if(compensacaoItem.getFavorecido() != null && compensacaoItem.getFavorecido().getId() == null) {
					compensacaoItem.setFavorecido(null);
				}
				
				if(compensacaoItem.getFavorecido() != null) {
					compensacaoItem.setFavorecido(pessoaRepository.find(compensacaoItem.getFavorecido().getId()));
				}
				
				
				if(!validator.hasErrors()) {
					_compensacaoItem.setData(compensacaoItem.getData());
					_compensacaoItem.setItem(itemRepository.find(compensacaoItem.getItem().getId()));
					_compensacaoItem.setFavorecido(compensacaoItem.getFavorecido());
					_compensacaoItem.setDebito(compensacaoItem.getDebito());
					_compensacaoItem.setCredito(compensacaoItem.getCredito());
					_compensacaoItem.setObs(compensacaoItem.getObs());
				}
			
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
		
		result
			.include("compensacao", compensacao)
			.include("compensacaoItemList", compensacaoItemList)
			.forwardTo("/WEB-INF/jsp/compensacao/compensacaoItemList.jsp");
		
	}
	
	
	@Post
	public void removerCompensacaoItem(List<Integer> indexs, String viewid) {
				
		Compensacao compensacao = (Compensacao) session.getAttribute("compensacao_" + viewid);
		List<CompensacaoItem> compensacaoItemList = (List<CompensacaoItem>) session.getAttribute("compensacaoItemList_" + viewid);
		boolean isClosed = (boolean) session.getAttribute("isClosed_" + viewid);
		
		if(isClosed) {
			validator.add(new I18nMessage("compensacao", "compensacao.isClosed.removerCompensacaoItem"));
		} else {
		
			List<CompensacaoItem> removeCompensacaoItemList = new ArrayList<CompensacaoItem>();
			for(Integer index : indexs) {
				removeCompensacaoItemList.add(compensacaoItemList.get(index));
			}
			for(CompensacaoItem removeCompensacaoItem : removeCompensacaoItemList) {
				compensacaoItemList.remove(removeCompensacaoItem);
			}
			
			for(int i = 0; i < compensacaoItemList.size(); i++) {
				compensacaoItemList.get(i).setSeq(i);
			}
			
			compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		
		session.setAttribute("compensacaoItemList_" + viewid, compensacaoItemList);
		result
			.include("compensacao", compensacao)
			.include("compensacaoItemList", compensacaoItemList)
			.forwardTo("/WEB-INF/jsp/compensacao/compensacaoItemList.jsp");
	
	}
	
	
	@Post
	public void cadastro(
			@NotNull(message = "{compensacao.notNull}") Compensacao compensacao, 
			@NotBlank(message = "{viewid.notBlank}") String viewid,
			List<Long> compensacaoItemDelete) {
		
		// Recupera compensacao da session
		Compensacao _compensacao = (Compensacao) session.getAttribute("compensacao_" + viewid);
		List<CompensacaoItem> compensacaoItemList = (List<CompensacaoItem>) session.getAttribute("compensacaoItemList_" + viewid);
		
		boolean isClosed = (boolean) session.getAttribute("isClosed_" + viewid);
		
		// Verifica se a compensacao e a view nao sao null
		if(compensacao != null && viewid != null) {
			
			// Verifica se a compensacao da session eh null
			if(_compensacao == null) {
				validator.add(new I18nMessage("compensacao", "compensacao.notFound"));
			} else {
				
				if(isClosed) {
					compensacao.setData(_compensacao.getData());
					compensacao.setStatus(_compensacao.getStatus());
					compensacao.setSaldo(_compensacao.getSaldo());
					
					// Valida compensacao do form
					validator.validateProperties(compensacao, "obs");
					
				} else {
				
					// Valida compensacao do form
					validator.validateProperties(compensacao, "data", "status", "saldo", "obs");
										
					if(compensacao.getData() != null) {
					
						// Verifica se a data eh maior que a data atual
						if(compensacao.getData().isAfter(LocalDate.now())) {
							validator.add(new I18nMessage("data", "compensacao.data.notAfterNow", LocalDate.now().toString(localization.getMessage("patternDate"))));
						}
						
						// Recupera compensacao anterior
						Compensacao anterior = compensacaoRepository.findAnterior(_compensacao.getConta(), _compensacao.getId());
					
						if(anterior != null) {
							
							// Verifica se a data eh inferior a compensacao anterior
							if(compensacao.getData().isBefore(anterior.getData().plusDays(1))) {
								validator.add(new I18nMessage("data", "compensacao.data.anterior.inferior", anterior.getData().plusDays(1).toString(localization.getMessage("patternDate"))));
							}
							
							// Verifica se a data eh igual a compensacao anterior
							if(compensacao.getData().equals(anterior.getData())) {
								validator.add(new I18nMessage("data", "compensacao.data.anterior.igual", anterior.getData().toString(localization.getMessage("patternDate"))));
							}
						
						}
					
					}
					
					// Verifica se existe diferenca
					if(compensacao.getSaldo() != null) {
						_compensacao.setSaldo(compensacao.getSaldo());
						_compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
						if(LancamentoStatus.COM.equals(compensacao.getStatus()) && _compensacao.getDiferenca() != 0.0) {
							validator.add(new I18nMessage("diferenca", "compensacao.diferenca.notValid", new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(_compensacao.getDiferenca())));
						}
					}
					
					// Verifica se existe movimentos na compensacao
					if(compensacaoItemList == null || compensacaoItemList.isEmpty()) {
						validator.add(new I18nMessage("compensacao", "compensacaoItemList.notNull"));
					}
					
					// Verifica se existe movimentos com valor null ou zero
					for(CompensacaoItem compensacaoItem : compensacaoItemList) {
						if(compensacaoItem.getItem() != null) {
							if(compensacaoItem.getItem().getTipo().equals(ItemTipo.DES) && (compensacaoItem.getDebito() == null || compensacaoItem.getDebito() == 0)) {
								validator.add(new I18nMessage("compensacaoItem_" + compensacaoItem.getSeq(), "compensacaoItem.debito.notNullOrZero", compensacaoItem.getItem().getDisplayName()));
							}
							if(compensacaoItem.getItem().getTipo().equals(ItemTipo.REC) && (compensacaoItem.getCredito() == null || compensacaoItem.getCredito() == 0)) {
								validator.add(new I18nMessage("compensacaoItem_" + compensacaoItem.getSeq(), "compensacaoItem.credito.notNullOrZero", compensacaoItem.getItem().getDisplayName()));
							}
						}
						
						// Verifica se eh cheque
						if(ItemTipo.DES.equals(compensacaoItem.getTipo()) && compensacaoItem.getBaixaItem() != null && compensacaoItem.getBaixaItem().getCheque() != null) {
							
							// Busca movs pelo numero do cheque
							List<BaixaItem> baixaItemList = baixaItemRepository.findByCheque(compensacaoItem.getBaixaItem().getCheque());
							if(baixaItemList != null && !baixaItemList.isEmpty()) {
								int isSelected = 0;						
								for(CompensacaoItem _compensacaoItem : compensacaoItemList) {
									if(_compensacaoItem.getBaixaItem() != null && _compensacaoItem.getBaixaItem().getCheque() != null) {
										for(BaixaItem _baixaItem : baixaItemList) {
											if(_compensacaoItem.getBaixaItem().equals(_baixaItem)) {
												isSelected++;
											}
										}
									}
								}
								
								if(isSelected != baixaItemList.size()) {
									validator.add(new I18nMessage("compensacaoItem_" + compensacaoItem.getSeq(), "compensacaoItem.cheque.baixaItem.multiple.not.exists", compensacaoItem.getBaixaItem().getCheque().getNumero(), compensacaoItem.getBaixaItem().getCheque().getTalaoCheque().getConta().getNome()));
								}
							}
						}
						
					}
					
				}
				
				// Transfere compensacao enviada a compensacao da session
				if(!validator.hasErrors()) {
					_compensacao.setObs(compensacao.getObs());
					if(!isClosed) {
						_compensacao.setData(compensacao.getData());
						_compensacao.setStatus(compensacao.getStatus());
						_compensacao.calculaTotalCompensacaoItem(compensacaoItemList);
					}
				}
				
				if(!isClosed) {
					for(CompensacaoItem compensacaoItem : compensacaoItemList) {
						compensacaoItem.setCompensacao(_compensacao);
						compensacaoItem.setStatus(_compensacao.getStatus());
					}
				}
			
			}
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		// Remove CompensacaoItem
		if(!isClosed) {
			if(compensacaoItemDelete != null && !compensacaoItemDelete.isEmpty()) {
				
				compensacaoItemRepository.deleteList(compensacaoItemDelete);
				
			}
		}
		
		compensacaoRepository.save(_compensacao);
		
		// Recupera Lista de Arquivos para Salvar
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		for(Arquivo arquivo : arquivoList) {
			arquivo.setCompensacao(_compensacao);
			arquivoRepository.save(arquivo);
		}
		
		// Recupera Lista de Arquivos para Deletar
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		for(Arquivo arquivo : arquivoDeleteList) {
			arquivoRepository.delete(arquivo);
		}
		arquivoSession.clear(viewid);
		
		for(CompensacaoItem compensacaoItem : compensacaoItemList) {
			compensacaoItem.setCompensacao(_compensacao);
			compensacaoItemRepository.save(compensacaoItem);
			
			// Recupera Lista de Arquivos para Salvar
			arquivoList = arquivoSession.getArquivoList(compensacaoItem.getViewid());
			for(Arquivo arquivo : arquivoList) {
				arquivo.setCompensacaoItem(compensacaoItem);
				arquivoRepository.save(arquivo);
			}
			
			// Recupera Lista de Arquivos para Deletar
			arquivoDeleteList = arquivoSession.getArquivoDeleteList(compensacaoItem.getViewid());
			for(Arquivo arquivo : arquivoDeleteList) {
				arquivoRepository.delete(arquivo);
			}
			arquivoSession.clear(compensacaoItem.getViewid());
		}
		
		session.removeAttribute("compensacao_" + viewid);
		session.removeAttribute("compensacaoItemList_" + viewid);
		session.removeAttribute("isClosed_" + viewid);
		
		if(!isClosed) {
			cartaoCreditoRepository.atualizaLimiteDisponivel();
		}
		
		result.use(Results.jsonp()).withCallback("compensacaoCadastroSuccess").withoutRoot().from(_compensacao).exclude("cliente").serialize();
		
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		List<Compensacao> compensacaoList = compensacaoRepository.findAll(ids);
		
		for(Compensacao compensacao : compensacaoList) {
			boolean isClosed = compensacaoRepository.isClosed(compensacao);
			if(isClosed) {
				validator.add(new I18nMessage("compensacao_" + compensacao.getId(), "compensacao.excluir.isClosed"));
			} else {
				
				List<Arquivo> arquivoList = compensacao.getArquivoList();
				for(Arquivo arquivo : arquivoList) {
					arquivoRepository.delete(arquivo);
				}
				
			}
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		List<CompensacaoItem> compensacaoItemList = compensacaoItemRepository.findAllByCompensacaoIds(ids);
		if(compensacaoItemList != null) {
			for(CompensacaoItem compensacaoItem : compensacaoItemList) {
				for(Arquivo arquivo : compensacaoItem.getArquivoList()) {
					arquivoRepository.delete(arquivo);
				}
			}
		}
		
		compensacaoRepository.deleteList(ids);
		result.use(Results.jsonp()).withCallback("compensacaoExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
}