package br.com.contames.controller;


import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.util.HTMLEntities;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Resource
public class DynamicResourcesController {
	
	private final Result result;
	private final HttpServletResponse response;
	private final HttpServletRequest request;
	private final Environment environment;
	private final Localization localization;
	private final ServletContext context;
	
	public DynamicResourcesController(Result result, HttpServletResponse response, HttpServletRequest request, Environment environment, Localization localization, ServletContext context) {
		this.result = result;
		this.response = response;
		this.request = request;
		this.environment = environment;
		this.localization = localization;
		this.context = context;
	}
		
	@Get("/dynamic/resources/js/i18n.js")
	public void messages() throws ConfigurationException {
		Gson gson = new Gson();
		PropertiesConfiguration settings = new PropertiesConfiguration("settings.properties");
		JsonObject jsonObjectMessages = new JsonObject();
		jsonObjectMessages.add("locales", gson.toJsonTree(settings.getList("locales")));
		for(Object locale : settings.getList("locales")) {
		
			PropertiesConfiguration messagesProperties = new PropertiesConfiguration("messages" + (locale.equals("pt-BR") ? "" : "_" + locale) + ".properties");
			PropertiesConfiguration validationMessagesProperties = new PropertiesConfiguration("ValidationMessages" + (locale.equals("pt-BR") ? "" : "_" + locale) + ".properties");
						
			Iterator<String> iterator = messagesProperties.getKeys();
			JsonObject jsonObject = new JsonObject();
			while(iterator.hasNext()) {
				String key = iterator.next();
				jsonObject.addProperty(key, HTMLEntities.htmlentities(StringUtils.join(messagesProperties.getStringArray(key), ", ")));
			}
			
			iterator = validationMessagesProperties.getKeys();
			while(iterator.hasNext()) {
				String key = iterator.next();
				jsonObject.addProperty(key, HTMLEntities.htmlentities(StringUtils.join(validationMessagesProperties.getStringArray(key), ", ")));
			}
			jsonObjectMessages.add((String) locale, jsonObject);
		}
		response.setContentType("application/javascript");
		
		StringBuilder script = new StringBuilder();
		script.append("String.prototype.htmlToText = function() {return $('<textarea>' + this + '</textarea>').val();};");
		script.append("var i18n = " + jsonObjectMessages + ";");
		script.append("i18n.locale = '" + localization.getLocale().toLanguageTag() + "';");
		script.append("i18n.localeAbbr = '" + localization.getLocale().getLanguage() + "';");
		script.append("i18n.get = function(message) {");
		script.append("var msg = i18n[i18n.locale][message] || message;");
		script.append("if(arguments.length > 1) {");
		script.append("var tpl = [];");
		script.append("for(var i = 1; i < arguments.length; i++) {");
		script.append("tpl[(i - 1).toString()] = arguments[i];");
		script.append("}");
		script.append("return msg.template(tpl).toString().htmlToText();");
		script.append("} else {");
		script.append("return msg.toString().htmlToText();");
		script.append("}");
		script.append("};");
				
		try {
			FileUtils.writeStringToFile(new File(context.getRealPath(".") + "/resources/js/i18n.js"), script.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.use(Results.http()).body(script.toString());
	}
	
	@Get("/dynamic/resources/js/vars.js")
	public void vars() {
		response.setContentType("application/javascript");
		StringBuilder script = new StringBuilder();
		script.append("var contextPath = '" + request.getContextPath() + "',");
		script.append("checkoutURL = '" + environment.get("paypal.checkoutURL") + "',");
		script.append("dateNow = '" + LocalDateTime.now().toString("yyyy-dd-MM HH:mm") + "';");
		result.use(Results.http()).body(script.toString());
	}
	
	@Private
	@Get("/viewid")
	public void viewid() {
		String viewid = new String(Base64.encodeBase64((String.valueOf(new DateTime().toDate().getTime()) + new Random().nextInt(1000)).getBytes()));
		result.use(Results.http()).body(viewid);
	}
	
}
