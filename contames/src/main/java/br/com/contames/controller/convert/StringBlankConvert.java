package br.com.contames.controller.convert;

import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;

@Convert(String.class)
public class StringBlankConvert implements Converter<String> {

	@Override
	public String convert(String value, Class<? extends String> type, ResourceBundle bundle) {
		if(StringUtils.isBlank(StringUtils.trim(value))) {
			return null;
		} else {
			return StringUtils.trim(value);
		}
	}

}
