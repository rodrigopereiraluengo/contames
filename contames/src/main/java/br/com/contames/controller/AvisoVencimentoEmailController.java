package br.com.contames.controller;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDate;

import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.tasks.scheduler.Scheduled;
import br.com.contames.component.EmailHelper;
import br.com.contames.entity.Pessoa;
import br.com.contames.repository.PessoaRepository;

import com.amazonaws.util.StringUtils;

@Resource
public class AvisoVencimentoEmailController {

	private final EntityManager entityManager;
	private final PessoaRepository pessoaRepository;
	private final EmailHelper emailHelper;
	private final Localization localization;
	private final Result result;
	
	public AvisoVencimentoEmailController(EntityManager entityManager, PessoaRepository pessoaRepository, EmailHelper emailHelper, Localization localization, Result result) {
		this.entityManager = entityManager;
		this.pessoaRepository = pessoaRepository;
		this.emailHelper = emailHelper;
		this.localization = localization;
		this.result = result;
	}
	
	@Post @Scheduled(cron = "0 0 3 * * ?", id = "avisoVencimentoEmail")
	public void execute() {
		
		List<Pessoa> clienteList = pessoaRepository.findAllClientes();
		
		if(clienteList != null && !clienteList.isEmpty()) {
		
			for(Pessoa cliente : clienteList) {
				
				StringBuilder sqlBuilder = new StringBuilder();
				sqlBuilder.append("SELECT ");
				sqlBuilder.append("r.cliente_id, ");
				sqlBuilder.append("r.lancamentoParcela_id, ");
				sqlBuilder.append("r.lancamento_id, ");
				sqlBuilder.append("r.baixaItem_id, ");
				sqlBuilder.append("r.baixa_id, ");
				sqlBuilder.append("r.transferencia_id, ");
				sqlBuilder.append("r.compensacaoItem_id, ");
				sqlBuilder.append("r.compensacao_id, ");
				sqlBuilder.append("r._data, ");
				sqlBuilder.append("r.favorecido_id, ");
				sqlBuilder.append("r.favorecido, ");
				sqlBuilder.append("r.item_id, ");
				sqlBuilder.append("r.item, ");
				sqlBuilder.append("CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END descricao, ");
				sqlBuilder.append("r.tipoDoc, ");
				sqlBuilder.append("r.numeroDoc, ");
				sqlBuilder.append("r.numeroDoc_clean, ");
				sqlBuilder.append("COALESCE(CASE ");
				sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
				sqlBuilder.append("END || '-' || r.numeroDoc, CASE ");
				sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
				sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
				sqlBuilder.append("END, r.numeroDoc) numeroDoc_nome, ");
				sqlBuilder.append("r.meio, ");
				sqlBuilder.append("CASE ");
				sqlBuilder.append("WHEN r.meio = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
				sqlBuilder.append("WHEN r.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
				sqlBuilder.append("WHEN r.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
				sqlBuilder.append("WHEN r.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
				sqlBuilder.append("WHEN r.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
				sqlBuilder.append("WHEN r.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
				sqlBuilder.append("WHEN r.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
				sqlBuilder.append("WHEN r.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
				sqlBuilder.append("ELSE ' - ' END meio_nome, ");
				sqlBuilder.append("r.conta_id, ");
				sqlBuilder.append("r.cartaoCredito_id, ");
				sqlBuilder.append("r.cheque_id, ");
				sqlBuilder.append("r.talaoCheque_id, ");
				sqlBuilder.append("r.meio_conta, ");
				sqlBuilder.append("r.destino_id, ");
				sqlBuilder.append("r.destino, ");
				sqlBuilder.append("r.parcela, ");
				sqlBuilder.append("r.status, ");
				sqlBuilder.append("CASE ");
				sqlBuilder.append("WHEN r.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
				sqlBuilder.append("WHEN r.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
				sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'DES' THEN '" + localization.getMessage("LancamentoStatus.DES.BXD") + "' ");
				sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'REC' THEN '" + localization.getMessage("LancamentoStatus.REC.BXD") + "' ");
				sqlBuilder.append("WHEN r.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
				sqlBuilder.append("WHEN r.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
				sqlBuilder.append("END status_nome, ");
				sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor END valor, ");
				sqlBuilder.append("r.valor valor_des_rec, ");
				sqlBuilder.append("r.tipo, ");
				sqlBuilder.append("r.criacao, ");
				sqlBuilder.append("r.usuCriacao_id, ");
				sqlBuilder.append("r.usuCriacao, ");
				sqlBuilder.append("r.alteracao, ");
				sqlBuilder.append("r.usuAlteracao_id, ");
				sqlBuilder.append("r.usuAlteracao ");
				sqlBuilder.append("FROM vw_Relatorio_Data r ");
				sqlBuilder.append("WHERE r.cliente_id = ?1 ");
				sqlBuilder.append("AND fn_dataDiaUtil(r._data) = ?2 ");
				
				sqlBuilder.append("AND (r.isCCred = false OR r.isCCred IS NULL) ");
						
				// Status
				sqlBuilder.append("AND r.status IN('LAN') ");
			
				// Total
				Query countQuery = entityManager.createNativeQuery("SELECT COUNT(cliente_id) _count, SUM(valor) _sum, SUM(CASE WHEN valor < 0 THEN -valor ELSE 0 END) sum_des, SUM(CASE WHEN tipo = 'REC' THEN valor_des_rec ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel");
				countQuery.setParameter(1, cliente.getId());
				countQuery.setParameter(2, LocalDate.now().toDate());
				
				countQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
				Map<String, Object> resultMap = (Map<String, Object>) countQuery.getSingleResult();
				Long count = (Long) resultMap.get("_count");
				Double sum = (Double) resultMap.get("_sum");
				Double sum_des = (Double) resultMap.get("sum_des");
				Double sum_rec = (Double) resultMap.get("sum_rec");
				
				// Movimentos
				sqlBuilder.append(" ORDER BY r._data, r.favorecido");
				Query query = entityManager.createNativeQuery(sqlBuilder.toString());
				query.setParameter(1, cliente.getId());
				query.setParameter(2, LocalDate.now().toDate());
				query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
				List<Map<String, Object>> queryResult = (List<Map<String, Object>>) query.getResultList();
				
				if(count != null && count > 0l) {
					
					StringBuilder tableBuilder = new StringBuilder();
					StringBuilder textBuilder = new StringBuilder();
					tableBuilder.append("<table style=\"width: 100%;font-family: 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif\">");
					tableBuilder.append("<thead>");
					tableBuilder.append("<tr>");
					tableBuilder.append("<th style=\"background-color: #f6f6f6\">" + localization.getMessage("data") + "</th>");
					tableBuilder.append("<th style=\"background-color: #f6f6f6\">" + localization.getMessage("descricao") + "</th>");
					tableBuilder.append("<th style=\"background-color: #f6f6f6\">" + localization.getMessage("valor") + "</th>");
					tableBuilder.append("</tr>");
					tableBuilder.append("</thead>");
					tableBuilder.append("<tbody>");
					
					textBuilder.append(localization.getMessage("data") + "\t");
					textBuilder.append(localization.getMessage("descricao") + "\t");
					textBuilder.append(localization.getMessage("valor") + "\n");
					
					DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale()));
					
					for(Map<String, Object> movimento : queryResult) {
						tableBuilder.append("<tr>");
						tableBuilder.append("<td style=\"text-align: center\">" + new SimpleDateFormat(localization.getMessage("patternDate")).format(movimento.get("_data")) + "</td>");
						tableBuilder.append("<td>" + movimento.get("descricao") + "</td>");
						tableBuilder.append("<td style=\"color: " + (Double.parseDouble(movimento.get("valor").toString()) < 0.0 ? "red": "blue") + "; text-align: right\">" + StringUtils.replace(decimalFormat.format(movimento.get("valor")), "-", "") + "</td>");
						tableBuilder.append("</tr>");
						
						textBuilder.append(new SimpleDateFormat(localization.getMessage("patternDate")).format(movimento.get("_data")) + "\t");
						textBuilder.append(movimento.get("descricao") + "\t");
						textBuilder.append(StringUtils.replace(decimalFormat.format(movimento.get("valor")), "-", "") + "\n");
					}
					tableBuilder.append("</tbody>");
					tableBuilder.append("<tfoot>");
						
					tableBuilder.append("<tr>");
					tableBuilder.append("<td colspan=\"2\" style=\"color: red; background-color: #f6f6f6; text-align: right\">" + localization.getMessage("despesas") + " :</td>");
					tableBuilder.append("<td style=\"color: red; background-color: #f6f6f6; text-align: right\">" + decimalFormat.format(sum_des) + "</td>");
					tableBuilder.append("</tr>");
					
					tableBuilder.append("<tr>");
					tableBuilder.append("<td colspan=\"2\" style=\"color: blue; background-color: #f6f6f6; text-align: right\">" + localization.getMessage("receitas") + " :</td>");
					tableBuilder.append("<td style=\"color: blue; background-color: #f6f6f6; text-align: right\">" + decimalFormat.format(sum_rec) + "</td>");
					tableBuilder.append("</tr>");
					
					tableBuilder.append("<tr>");
					tableBuilder.append("<td colspan=\"2\" style=\"color: " + (sum < 0 ? "red" : "blue") + "; background-color: #f6f6f6; text-align: right\">" + localization.getMessage("total") + " :</td>");
					tableBuilder.append("<td style=\"color: " + (sum < 0 ? "red" : "blue") + "; background-color: #f6f6f6; text-align: right\">" + StringUtils.replace(decimalFormat.format(sum), "-", "") + "</td>");
					tableBuilder.append("</tr>");
					
					tableBuilder.append("</tfoot>");
					tableBuilder.append("</table>");
					
					textBuilder.append("\n");
					textBuilder.append("\n");
					
					textBuilder.append(localization.getMessage("despesas") + ": " + decimalFormat.format(sum_des) + "\n");
					textBuilder.append(localization.getMessage("receitas") + ": " + decimalFormat.format(sum_rec) + "\n");
					textBuilder.append(localization.getMessage("total") + ": " + decimalFormat.format(sum) + "\n");
					textBuilder.append("\n");
					textBuilder.append("\n");
					
					emailHelper
						.addTo(cliente.getEmail(), cliente.getDisplayNome())
						.setSubject(localization.getMessage("email.vencimentos.subject", LocalDate.now().toString(localization.getMessage("patternDate"))))
						.setHtmlMsg(localization.getMessage("email.vencimentos.htmlMsg", cliente.getDisplayNome(), tableBuilder.toString()))
						.setTextMsg(localization.getMessage("email.vencimentos.textMsg", cliente.getDisplayNome(), textBuilder.toString()))
						.send();
					
				}
				
		
			}
			
			
			result.nothing();
		}
				
	}
	
	private DecimalFormat getDecimalFormat() {
		DecimalFormat decimalFormat = new DecimalFormat("####.00");
		decimalFormat.setMaximumFractionDigits(2);
		decimalFormat.setMinimumFractionDigits(2);
		decimalFormat.setMinimumIntegerDigits(1);
		
		DecimalFormatSymbols custom = new DecimalFormatSymbols();
		//custom.setDecimalSeparator('.');
		decimalFormat.setDecimalFormatSymbols(custom);
		return decimalFormat;
	}
	
}
