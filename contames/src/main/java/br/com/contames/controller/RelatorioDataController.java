package br.com.contames.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Min;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.record.CFRuleRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingRule;
import org.apache.poi.xssf.usermodel.XSSFFontFormatting;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheetConditionalFormatting;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.interceptor.download.InputStreamDownload;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.Settings;
import br.com.contames.enumeration.DocTipo;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.MeioPagamento;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.ReportExportType;
import br.com.contames.enumeration.SubtotalTipo;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class RelatorioDataController {

	private final Result result;
	private final Validator validator;
	private final EntityManager entityManager;
	private final UsuarioLogged usuarioLogged;
	private final Localization localization;
	private final Settings settings;
	private final Connection connection;
	private final HttpServletRequest request;
	private final Environment environment;
	
	public RelatorioDataController(Result result, Validator validator, EntityManager entityManager, UsuarioLogged usuarioLogged, Localization localization, Settings settings, Connection connection, HttpServletRequest request, Environment environment) {
		this.result = result;
		this.validator = validator;
		this.entityManager = entityManager;
		this.usuarioLogged = usuarioLogged;
		this.localization = localization;
		this.settings = settings;
		this.connection = connection;
		this.request = request;
		this.environment = environment;
	}
	
	
	@Get("/relatorios/data")
	public void relatorio(
		LocalDate dataDe, 
		LocalDate dataAte, 
		List<Long> favorecidoList, 
		String descricao, 
		List<MeioPagamento> meioList, 
		List<Long> contaList, 
		List<Long> cartaoCreditoList, 
		@Min(value = 1, message="{numero.min}")
		Integer cheque, 
		List<DocTipo> tipoDocList, 
		String numeroDoc, 
		ItemTipo tipo,
		List<LancamentoStatus> statusList, 
		SubtotalTipo subtotal,
		boolean isTable, 
		List<OrderBy> orderByList, 
		Integer page,
		Boolean isCCred) {  
		
		String jsp = !isTable ? "relatorio" : "relatorio_table";
		
		if(!isTable) {
			
			if(dataDe == null) {
				dataDe = LocalDate.now().withMonthOfYear(1).withDayOfMonth(1);
			}
			
			if(dataAte == null) {
				dataAte = LocalDate.now().withMonthOfYear(12).dayOfMonth().withMaximumValue();
			}
			
			if(statusList == null || statusList.isEmpty()) {
				statusList = Arrays.asList(LancamentoStatus.STD, LancamentoStatus.LAN, LancamentoStatus.BXD, LancamentoStatus.COM);
			}
			
			if(subtotal == null) {
				subtotal = SubtotalTipo.MES;
			}
		
		} else {
			
			if(dataDe == null) {
				validator.add(new I18nMessage("dataDe", "dataDe.notNull"));
			}
			
			if(dataAte == null) {
				validator.add(new I18nMessage("dataAte", "dataAte.notNull"));
			}
			
		}
		
		if(dataDe != null && dataAte != null) {
			
			// Verificar se a dataAte eh inferior a dataDe
			if(dataAte.isBefore(dataDe)) {
				validator.add(new I18nMessage("dataAte", "dataAte.notIsBefore.dataDe"));
			}
			
			// Verifica se a diferenca entre dataDe e dataAte eh maior que 36 meses
			if(Months.monthsBetween(dataDe, dataAte).getMonths() > 36) {
				validator.add(new I18nMessage("dataAte", "dataAte.months.notMore36Mounths"));
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.DESC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.lancamentoParcela_id, ");
		sqlBuilder.append("r.lancamento_id, ");
		sqlBuilder.append("r.baixaItem_id, ");
		sqlBuilder.append("r.baixa_id, ");
		sqlBuilder.append("r.transferencia_id, ");
		sqlBuilder.append("r.compensacaoItem_id, ");
		sqlBuilder.append("r.compensacao_id, ");
		sqlBuilder.append("r._data, ");
		sqlBuilder.append("r.favorecido_id, ");
		sqlBuilder.append("r.favorecido, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("r.item, ");
		sqlBuilder.append("CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END descricao, ");
		sqlBuilder.append("r.tipoDoc, ");
		sqlBuilder.append("r.numeroDoc, ");
		sqlBuilder.append("r.numeroDoc_clean, ");
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || '-' || r.numeroDoc, CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, r.numeroDoc) numeroDoc_nome, ");
		sqlBuilder.append("r.meio, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.meio = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
		sqlBuilder.append("WHEN r.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
		sqlBuilder.append("WHEN r.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
		sqlBuilder.append("ELSE ' - ' END meio_nome, ");
		sqlBuilder.append("r.conta_id, ");
		sqlBuilder.append("r.cartaoCredito_id, ");
		sqlBuilder.append("r.cheque_id, ");
		sqlBuilder.append("r.talaoCheque_id, ");
		sqlBuilder.append("r.meio_conta, ");
		sqlBuilder.append("r.destino_id, ");
		sqlBuilder.append("r.destino, ");
		sqlBuilder.append("r.parcela, ");
		sqlBuilder.append("r.status, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN r.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'DES' THEN '" + localization.getMessage("LancamentoStatus.DES.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'REC' THEN '" + localization.getMessage("LancamentoStatus.REC.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
		sqlBuilder.append("WHEN r.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status_nome, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor END valor, ");
		sqlBuilder.append("r.valor valor_des_rec, ");
		sqlBuilder.append("r.tipo, ");
		sqlBuilder.append("r.criacao, ");
		sqlBuilder.append("r.usuCriacao_id, ");
		sqlBuilder.append("r.usuCriacao, ");
		sqlBuilder.append("r.alteracao, ");
		sqlBuilder.append("r.usuAlteracao_id, ");
		sqlBuilder.append("r.usuAlteracao, ");
		sqlBuilder.append("r.isCCred, ");
		sqlBuilder.append("r.fatCartaoCredito_nome, ");
		sqlBuilder.append("r.boletotipo, ");
		sqlBuilder.append("r.boletonumero, ");
		sqlBuilder.append("r.arquivoDoc_id, ");
		sqlBuilder.append("r.arquivoDoc_nome, ");
		sqlBuilder.append("r.arquivo_id, ");
		sqlBuilder.append("r.arquivo_nome, ");
		sqlBuilder.append("r.comprovante_id, ");
		sqlBuilder.append("r.comprovante_nome ");
		sqlBuilder.append("FROM vw_Relatorio_Data r ");
		sqlBuilder.append("WHERE r.cliente_id = ?1 ");
		sqlBuilder.append("AND r._data BETWEEN ?2 AND ?3 ");
		
		if(isCCred == null) {
			isCCred = true;
		}
		
		sqlBuilder.append("AND (r.isCCred = " + isCCred + " OR r.isCCred IS NULL)");
		
		// Favorecido
		if(favorecidoList != null && !favorecidoList.isEmpty()) {
			sqlBuilder.append("AND r.favorecido_id IN(" + StringUtils.join(favorecidoList.toArray(), ", ") + ") ");
		}
		
		// Descricao
		if(StringUtils.isNotBlank(descricao)) {
			sqlBuilder.append("AND unaccent(LOWER(CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END)) ILIKE ?4 ");
		}
		
		// Meio
		if(meioList != null && !meioList.isEmpty()) {
			sqlBuilder.append("AND r.meio IN('" + StringUtils.join(meioList.toArray(), "', '") + "') ");
		}
		
		// Cartao de Credito e Conta
		if(cartaoCreditoList != null && !cartaoCreditoList.isEmpty() && contaList != null && !contaList.isEmpty()) {
			sqlBuilder.append("AND (r.cartaoCredito_id IN(" + StringUtils.join(cartaoCreditoList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.conta_id IN(" + StringUtils.join(contaList.toArray(), ", ") + ")) ");
		} else {
			// Cartao de Credito
			if(cartaoCreditoList != null && !cartaoCreditoList.isEmpty()) {
				sqlBuilder.append("AND r.cartaoCredito_id IN(" + StringUtils.join(cartaoCreditoList.toArray(), ", ") + ") ");
			}
			
			// Conta
			if(contaList != null && !contaList.isEmpty()) {
				sqlBuilder.append("AND r.conta_id IN(" + StringUtils.join(contaList.toArray(), ", ") + ") ");
			}
		}
		
		// Cheque 
		if(cheque != null) {
			sqlBuilder.append("AND CAST(r.cheque_numero AS VARCHAR) ILIKE '" + cheque + "%' ");
		}
		
		// Doc
		if(tipoDocList != null && !tipoDocList.isEmpty()) {
			sqlBuilder.append("AND r.tipoDoc IN('" + StringUtils.join(tipoDocList.toArray(), "', '") + "') ");
		}
		
		// Numero
		if(StringUtils.isNotBlank(numeroDoc)) {
			sqlBuilder.append("AND unaccent(LOWER(r.numeroDoc)) ILIKE ?5 ");
		}
		
		// Tipo
		if(tipo != null) {
			sqlBuilder.append("AND r.tipo = ?6 ");
		}
		
		// Status
		if(statusList != null && !statusList.isEmpty()) {
			sqlBuilder.append("AND r.status IN('" + StringUtils.join(statusList.toArray(), "', '") + "') ");
		}
		
		
		// Total
		Query countQuery = entityManager.createNativeQuery("SELECT COUNT(cliente_id) _count, SUM(valor) _sum, SUM(CASE WHEN valor < 0 THEN -valor ELSE 0 END) sum_des, SUM(CASE WHEN valor > 0 THEN valor ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, dataDe.toDate());
		countQuery.setParameter(3, dataAte.toDate());
		if(StringUtils.isNotBlank(descricao)) {
			countQuery.setParameter(4, StringHelper.unAccent(descricao.toLowerCase()) + "%");
		}
		if(StringUtils.isNotBlank(numeroDoc)) {
			countQuery.setParameter(5, StringHelper.unAccent(numeroDoc.toLowerCase()) + "%");
		}
		if(tipo != null) {
			countQuery.setParameter(6, tipo.toString());
		}
		countQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		Map<String, Object> resultMap = (Map<String, Object>) countQuery.getSingleResult();
		Long count = (Long) resultMap.get("_count");
		Double sum = (Double) resultMap.get("_sum");
		Double sum_des = (Double) resultMap.get("sum_des");
		Double sum_rec = (Double) resultMap.get("sum_rec");
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		// Subtotal
		String column = "to_char(_data, 'yyyyMMdd')";
		switch(subtotal) {
		case MES:
			column = "to_char(_data, 'yyyyMM')";
			break;
		case ANO:
			column = "to_char(_data, 'yyyy')";
			break;
		}
		
		Query subTotalQuery = entityManager.createNativeQuery("SELECT " + column + " AS _data, SUM(valor) _sum, SUM(CASE WHEN valor < 0 THEN -valor ELSE 0 END) sum_des, SUM(CASE WHEN valor > 0 THEN valor ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel GROUP BY " + column + " ORDER BY " + column + "::integer");
		subTotalQuery.setParameter(1, usuarioLogged.getCliente().getId());
		subTotalQuery.setParameter(2, dataDe.toDate());
		subTotalQuery.setParameter(3, dataAte.toDate());
		if(StringUtils.isNotBlank(descricao)) {
			subTotalQuery.setParameter(4, StringHelper.unAccent(descricao.toLowerCase()) + "%");
		}
		if(StringUtils.isNotBlank(numeroDoc)) {
			subTotalQuery.setParameter(5, StringHelper.unAccent(numeroDoc.toLowerCase()) + "%");
		}
		if(tipo != null) {
			subTotalQuery.setParameter(6, tipo.toString());
		}
		subTotalQuery.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		List<Map<String, Object>> listResultMap = (List<Map<String, Object>>) subTotalQuery.getResultList();
		
		Map<String, Map<String, Double>> _resultMapSubtotal = new HashMap<String, Map<String, Double>>();
		for(Map<String, Object> _resultMap : listResultMap) {
			Map<String, Double> mapValue = _resultMapSubtotal.containsKey(_resultMap.get("_data")) ? _resultMapSubtotal.get(_resultMap.get("_data")) : new HashMap<String, Double>();
			mapValue.put("_sum", (Double) _resultMap.get("_sum"));
			mapValue.put("sum_des", (Double) _resultMap.get("sum_des"));
			mapValue.put("sum_rec", (Double) _resultMap.get("sum_rec"));
			_resultMapSubtotal.put((String) _resultMap.get("_data"), mapValue);
		}
		Map<String, Map<String, Double>> resultMapSubtotal = new TreeMap<String, Map<String, Double>>(_resultMapSubtotal);
		
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
		
		
		Query query = entityManager.createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, dataDe.toDate());
		query.setParameter(3, dataAte.toDate());
		if(StringUtils.isNotBlank(descricao)) {
			query.setParameter(4, StringHelper.unAccent(descricao.toLowerCase()) + "%");
		}
		if(StringUtils.isNotBlank(numeroDoc)) {
			query.setParameter(5, StringHelper.unAccent(numeroDoc.toLowerCase()) + "%");
		}
		if(tipo != null) {
			query.setParameter(6, tipo.toString());
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("sum", sum)
			.include("sum_des", sum_des)
			.include("sum_rec", sum_rec)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("dataDe", dataDe)
			.include("dataAte", dataAte)
			.include("subtotal", subtotal)
			.include("resultMapSubtotal", resultMapSubtotal);
		
		try {
			result.include("relList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/relatorioData/" + jsp + ".jsp");
		
	}
	
	@Get("/relatorio_data.{ext}")
	public Download export(LocalDate dataDe, 
			LocalDate dataAte, 
			List<Long> favorecidoList, 
			String descricao, 
			List<MeioPagamento> meioList, 
			List<Long> contaList, 
			List<Long> cartaoCreditoList, 
			@Min(value = 1, message="{numero.min}")
			Integer cheque, 
			List<DocTipo> tipoDocList, 
			String numeroDoc, 
			ItemTipo tipo,
			List<LancamentoStatus> statusList, 
			SubtotalTipo subtotal,
			List<OrderBy> orderByList,
			String ext,
			Boolean isCCred) throws SQLException, JRException, IOException {
		
		ReportExportType type = ReportExportType.valueOf(ext.toUpperCase());
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.DESC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		HashMap parameters = new HashMap();
		
		parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, localization.getBundle());
		parameters.put(JRParameter.REPORT_LOCALE, localization.getLocale());
		parameters.put("dataDe", dataDe.toDate());
		parameters.put("dataAte", dataAte.toDate());
						
		// Subtotal
		String column = "to_char(_data, 'yyyyMMdd') ";
		String patternOrder = localization.getMessage("SubtotalTipo.DIA.pattern");
		String parsePatternOrder = localization.getMessage("SubtotalTipo.DIA.parse.pattern");
		switch(subtotal) {
		case MES:
			column = "to_char(_data, 'yyyyMM') ";
			patternOrder = localization.getMessage("SubtotalTipo.MES.pattern");
			parsePatternOrder = localization.getMessage("SubtotalTipo.MES.parse.pattern");
			break;
		case ANO:
			column = "to_char(_data, 'yyyy') ";
			patternOrder = localization.getMessage("SubtotalTipo.ANO.pattern");
			parsePatternOrder = localization.getMessage("SubtotalTipo.ANO.parse.pattern");
			break;
		}
		parameters.put("patternOrder", patternOrder);
		parameters.put("parsePatternOrder", parsePatternOrder);
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append(column + " _order, ");
		sqlBuilder.append("r.cliente_id, ");
		sqlBuilder.append("r.lancamentoParcela_id, ");
		sqlBuilder.append("r.lancamento_id, ");
		sqlBuilder.append("r.baixaItem_id, ");
		sqlBuilder.append("r.baixa_id, ");
		sqlBuilder.append("r.transferencia_id, ");
		sqlBuilder.append("r.compensacaoItem_id, ");
		sqlBuilder.append("r.compensacao_id, ");
		sqlBuilder.append("r._data, ");
		sqlBuilder.append("r.favorecido_id, ");
		sqlBuilder.append("r.favorecido, ");
		sqlBuilder.append("r.item_id, ");
		sqlBuilder.append("r.item, ");
		sqlBuilder.append("CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END descricao, ");
		sqlBuilder.append("r.tipoDoc, ");
		sqlBuilder.append("r.numeroDoc, ");
		sqlBuilder.append("r.numeroDoc_clean, ");
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || '-' || r.numeroDoc, CASE ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN r.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, r.numeroDoc, '-') numeroDoc_nome, ");
		sqlBuilder.append("r.meio, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.meio = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
		sqlBuilder.append("WHEN r.meio = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
		sqlBuilder.append("WHEN r.meio = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
		sqlBuilder.append("WHEN r.meio = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
		sqlBuilder.append("WHEN r.meio = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
		sqlBuilder.append("ELSE ' - ' END meio_nome, ");
		sqlBuilder.append("r.conta_id, ");
		sqlBuilder.append("r.cartaoCredito_id, ");
		sqlBuilder.append("r.cheque_id, ");
		sqlBuilder.append("r.talaoCheque_id, ");
		sqlBuilder.append("r.meio_conta, ");
		sqlBuilder.append("r.destino_id, ");
		sqlBuilder.append("r.destino, ");
		sqlBuilder.append("r.parcela, ");
		sqlBuilder.append("r.status, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN r.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN r.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'DES' THEN '" + localization.getMessage("LancamentoStatus.DES.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'BXD' AND r.tipo = 'REC' THEN '" + localization.getMessage("LancamentoStatus.REC.BXD") + "' ");
		sqlBuilder.append("WHEN r.status = 'COM' THEN '" + localization.getMessage("LancamentoStatus.COM") + "' ");
		sqlBuilder.append("WHEN r.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status_nome, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN -r.valor ELSE 0 END valor_des, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor ELSE 0 END valor_rec, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor END valor, ");
		sqlBuilder.append("r.valor valor_des_rec, ");
		sqlBuilder.append("r.tipo, ");
		sqlBuilder.append("r.criacao, ");
		sqlBuilder.append("r.usuCriacao_id, ");
		sqlBuilder.append("r.usuCriacao, ");
		sqlBuilder.append("r.alteracao, ");
		sqlBuilder.append("r.usuAlteracao_id, ");
		sqlBuilder.append("r.usuAlteracao ");
		sqlBuilder.append("FROM vw_Relatorio_Data r ");
		sqlBuilder.append("WHERE r.cliente_id = ? ");
		sqlBuilder.append("AND r._data BETWEEN ? AND ? ");
		
		if(isCCred == null) {
			isCCred = true;
		}
		
		sqlBuilder.append("AND (r.isCCred = " + isCCred + " OR r.isCCred IS NULL) ");
		
		// Favorecido
		if(favorecidoList != null && !favorecidoList.isEmpty()) {
			sqlBuilder.append("AND r.favorecido_id IN(" + StringUtils.join(favorecidoList.toArray(), ", ") + ") ");
		}
		
		// Descricao
		if(StringUtils.isNotBlank(descricao)) {
			sqlBuilder.append("AND unaccent(LOWER(CASE WHEN r.transferencia_id IS NULL THEN r.descricao ELSE '" + localization.getMessage("transf_") + " ' || r.meio_conta || ' " + localization.getMessage("p_") + " ' || r.destino END)) ILIKE ? ");
		}
		
		// Meio
		if(meioList != null && !meioList.isEmpty()) {
			sqlBuilder.append("AND r.meio IN('" + StringUtils.join(meioList.toArray(), "', '") + "') ");
		}
		
		// Cartao de Credito e Conta
		if(cartaoCreditoList != null && !cartaoCreditoList.isEmpty() && contaList != null && !contaList.isEmpty()) {
			sqlBuilder.append("AND (r.cartaoCredito_id IN(" + StringUtils.join(cartaoCreditoList.toArray(), ", ") + ") ");
			sqlBuilder.append("OR r.conta_id IN(" + StringUtils.join(contaList.toArray(), ", ") + ")) ");
		} else {
			// Cartao de Credito
			if(cartaoCreditoList != null && !cartaoCreditoList.isEmpty()) {
				sqlBuilder.append("AND r.cartaoCredito_id IN(" + StringUtils.join(cartaoCreditoList.toArray(), ", ") + ") ");
			}
			
			// Conta
			if(contaList != null && !contaList.isEmpty()) {
				sqlBuilder.append("AND r.conta_id IN(" + StringUtils.join(contaList.toArray(), ", ") + ") ");
			}
		}
		
		// Cheque 
		if(cheque != null) {
			sqlBuilder.append("AND CAST(r.cheque_numero AS VARCHAR) ILIKE '" + cheque + "%' ");
		}
		
		// Doc
		if(tipoDocList != null && !tipoDocList.isEmpty()) {
			sqlBuilder.append("AND r.tipoDoc IN('" + StringUtils.join(tipoDocList.toArray(), "', '") + "') ");
		}
		
		// Numero
		if(StringUtils.isNotBlank(numeroDoc)) {
			sqlBuilder.append("AND unaccent(LOWER(r.numeroDoc)) ILIKE ? ");
		}
		
		// Tipo
		if(tipo != null) {
			sqlBuilder.append("AND r.tipo = ? ");
		}
		
		// Status
		if(statusList != null && !statusList.isEmpty()) {
			sqlBuilder.append("AND r.status IN('" + StringUtils.join(statusList.toArray(), "', '") + "') ");
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
			parameters.put(StringHelper.cleanSqlField("order_" + orderByList.get(i).getColumn()), orderByList.get(i).getDirection().toString());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
		
		System.out.println("@@@" + sqlBuilder.toString());
		
		// Consulta Principal
		PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());
		preparedStatement.setLong(1, usuarioLogged.getCliente().getId());
		preparedStatement.setDate(2, new java.sql.Date(dataDe.toDate().getTime()));
		preparedStatement.setDate(3, new java.sql.Date(dataAte.toDate().getTime()));
		int indexParam = 4;
		if(StringUtils.isNotBlank(descricao)) {
			preparedStatement.setString(indexParam, StringHelper.unAccent(descricao.toLowerCase()) + "%");
			indexParam++;
		}
		if(StringUtils.isNotBlank(numeroDoc)) {
			preparedStatement.setString(indexParam, StringHelper.unAccent(numeroDoc.toLowerCase()) + "%");
			indexParam++;
		}
		if(tipo != null) {
			preparedStatement.setString(indexParam, tipo.toString());
			indexParam++;
		}
		ResultSet resultSet = preparedStatement.executeQuery();
		
		
		// Consulta Chart
		PreparedStatement preparedStatementChart = connection.prepareStatement("SELECT " + column + " _order, SUM(valor) _sum, SUM(CASE WHEN valor < 0 THEN -valor ELSE 0 END) sum_des, SUM(CASE WHEN valor > 0 THEN valor ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel GROUP BY " + column + " ORDER BY " + column + "::integer");
		preparedStatementChart.setLong(1, usuarioLogged.getCliente().getId());
		preparedStatementChart.setDate(2, new java.sql.Date(dataDe.toDate().getTime()));
		preparedStatementChart.setDate(3, new java.sql.Date(dataAte.toDate().getTime()));
		indexParam = 4;
		if(StringUtils.isNotBlank(descricao)) {
			preparedStatementChart.setString(indexParam, StringHelper.unAccent(descricao.toLowerCase()) + "%");
			indexParam++;
		}
		if(StringUtils.isNotBlank(numeroDoc)) {
			preparedStatementChart.setString(indexParam, StringHelper.unAccent(numeroDoc.toLowerCase()) + "%");
			indexParam++;
		}
		if(tipo != null) {
			preparedStatementChart.setString(indexParam, tipo.toString());
			indexParam++;
		}
		ResultSet resultSetChart = preparedStatementChart.executeQuery();
		
		System.out.println("@@@" + "SELECT " + column + " _order, SUM(valor) _sum, SUM(CASE WHEN valor < 0 THEN -valor ELSE 0 END) sum_des, SUM(CASE WHEN valor > 0 THEN valor ELSE 0 END) sum_rec FROM(" + sqlBuilder.toString() + ") AS Rel GROUP BY " + column + " ORDER BY " + column + "::integer");
		
		InputStream inputStream = null;
		Download download = null;
		String jasperFileName = request.getServletContext().getRealPath("WEB-INF/reports/RelatorioData.jasper");
		switch(type) {
		case PDF:
			parameters.put("resultSetChart", new JRResultSetDataSource(resultSetChart));
			String jrxmlFileName = request.getServletContext().getRealPath("WEB-INF/reports/RelatorioData.jrxml");
			
			if(environment.supports("development")) {
				JasperCompileManager.compileReportToFile(jrxmlFileName);
			}
			inputStream = new ByteArrayInputStream(JasperRunManager.runReportToPdf(jasperFileName, parameters, new JRResultSetDataSource(resultSet)));
			download = new InputStreamDownload(inputStream, "application/pdf", "relatorio_data.pdf");
			break;
		case XLSX:
			
			XSSFWorkbook workbook = new XSSFWorkbook(request.getServletContext().getRealPath("WEB-INF/xlsx/Relatorio_Data.xlsx"));
			XSSFSheet relatorio = workbook.getSheet("Relatorio");
			relatorio.getRow(0).getCell(0).setCellValue(localization.getMessage("relatorioData", dataDe.toString(localization.getMessage("patternDate")), dataAte.toString(localization.getMessage("patternDate"))));
			
			relatorio.getRow(2).getCell(0).setCellValue(localization.getMessage("data"));
			relatorio.getRow(2).getCell(1).setCellValue(localization.getMessage("descricao"));
			relatorio.getRow(2).getCell(3).setCellValue(localization.getMessage("doc"));
			relatorio.getRow(2).getCell(4).setCellValue(localization.getMessage("meio"));
			relatorio.getRow(2).getCell(5).setCellValue(localization.getMessage("cCredConta"));
			relatorio.getRow(2).getCell(6).setCellValue(localization.getMessage("parcela"));
			relatorio.getRow(2).getCell(7).setCellValue(localization.getMessage("status"));
			relatorio.getRow(2).getCell(8).setCellValue(localization.getMessage("valor"));
			
			
			// Estilos
			
			// Data
			XSSFCellStyle dataCellStyle = relatorio.getRow(3).getCell(0).getCellStyle();
						
			// Descricao
			XSSFCellStyle descricaoCellStyle = relatorio.getRow(3).getCell(1).getCellStyle();
			
			// Meio
			XSSFCellStyle meioCellStyle = relatorio.getRow(3).getCell(3).getCellStyle();
			
			// C. Cred. / Conta
			XSSFCellStyle cCredContaCellStyle = relatorio.getRow(3).getCell(4).getCellStyle();
			
			// Doc
			XSSFCellStyle docCellStyle = relatorio.getRow(3).getCell(5).getCellStyle();
			
			// Parcela
			XSSFCellStyle parcCellStyle = relatorio.getRow(3).getCell(6).getCellStyle();
			
			// Status
			XSSFCellStyle statusCellStyle = relatorio.getRow(3).getCell(7).getCellStyle();
						
			// Valor
			XSSFCellStyle valorCellStyle = relatorio.getRow(3).getCell(8).getCellStyle();
			
			
			// -----------------------------------------------------------------------------------
			// Estilo Subtotal
			XSSFCellStyle subtotalRowStyle = relatorio.getRow(4).getRowStyle();
			
			// Descricao
			XSSFCellStyle subtotalDescrCellStyle = relatorio.getRow(4).getCell(0).getCellStyle();
			
			// Descricao Despesa
			XSSFCellStyle subtotalDescrDesCellStyle = relatorio.getRow(4).getCell(1).getCellStyle();
			
			// Valor Despesa
			XSSFCellStyle subtotalValorDesCellStyle = relatorio.getRow(4).getCell(2).getCellStyle();
			
			// Descricao Receita
			XSSFCellStyle subtotalDescrRecCellStyle = relatorio.getRow(4).getCell(3).getCellStyle();
			
			// Valor Receita
			XSSFCellStyle subtotalValorRecCellStyle = relatorio.getRow(4).getCell(4).getCellStyle();
			
			// Descricao Total
			XSSFCellStyle subtotalDescrTotalCellStyle = relatorio.getRow(4).getCell(5).getCellStyle();
			
			// Valor Total
			XSSFCellStyle subtotalValorTotalCellStyle = relatorio.getRow(4).getCell(6).getCellStyle();
			
			relatorio.removeRow(relatorio.getRow(3));
			relatorio.removeRow(relatorio.getRow(4));
			
			// Registros
			int indexRowRegistros = 3;
			String _order = null;
			int rowPosition = 3;
			List<String> func_sum_des = new ArrayList<String>();
			List<String> func_sum_rec = new ArrayList<String>();
			List<String> func_sum = new ArrayList<String>();
			
			Map<Integer, Integer> periodoList = new HashMap<Integer, Integer>();
			while(resultSet.next()) {
				
				// SUBTOTAL
				if(_order == null) {
					_order = resultSet.getString("_order");
				}
				
				if(!resultSet.getString("_order").equals(_order)) {
					
					XSSFRow rowSubtotal = relatorio.createRow(indexRowRegistros);
					rowSubtotal.setRowStyle(subtotalRowStyle);
					
					// SubTotal
					XSSFCell cellSubTotalDescr = rowSubtotal.createCell(0);
					String subTotalDescr = null;
					if(subtotal.equals(SubtotalTipo.DIA)) {
						subTotalDescr = LocalDate.parse(_order, DateTimeFormat.forPattern("yyyyMMdd")).toString(localization.getMessage("SubtotalTipo.DIA.pattern"));
					} else if (subtotal.equals(SubtotalTipo.MES)) {
						subTotalDescr = LocalDate.parse(_order, DateTimeFormat.forPattern("yyyyMM")).toString(localization.getMessage("SubtotalTipo.MES.pattern"));
					} else if(subtotal.equals(SubtotalTipo.ANO)) {
						subTotalDescr = LocalDate.parse(_order, DateTimeFormat.forPattern("yyyy")).toString(localization.getMessage("SubtotalTipo.ANO.pattern"));
					}
					cellSubTotalDescr.setCellStyle(subtotalDescrCellStyle);
					cellSubTotalDescr.setCellValue(subTotalDescr);
					
					rowSubtotal.createCell(1).setCellStyle(subtotalDescrCellStyle);
					rowSubtotal.createCell(2).setCellStyle(subtotalDescrCellStyle);
										
					// Despesas
					XSSFCell cellSubTotalDescrDes = rowSubtotal.createCell(3);
					cellSubTotalDescrDes.setCellValue(localization.getMessage("despesas") + ": ");
					cellSubTotalDescrDes.setCellStyle(subtotalDescrDesCellStyle);
					
					XSSFCell cellSubTotalValorDes = rowSubtotal.createCell(4);
					cellSubTotalValorDes.setCellStyle(subtotalValorDesCellStyle);
					cellSubTotalValorDes.setCellType(Cell.CELL_TYPE_FORMULA);
					cellSubTotalValorDes.setCellFormula("ABS(SUMIF(I" + (rowPosition + 1) + ":I" + indexRowRegistros + ", \"<0\"))");
					
					// Receitas
					XSSFCell cellSubTotalDescrRec = rowSubtotal.createCell(5);
					cellSubTotalDescrRec.setCellValue(localization.getMessage("receitas") + ": ");
					cellSubTotalDescrRec.setCellStyle(subtotalDescrRecCellStyle);
					
					XSSFCell cellSubTotalValorRec = rowSubtotal.createCell(6);
					cellSubTotalValorRec.setCellStyle(subtotalValorRecCellStyle);
					cellSubTotalValorRec.setCellType(Cell.CELL_TYPE_FORMULA);
					cellSubTotalValorRec.setCellFormula("SUMIF(I" + (rowPosition + 1) + ":I" + indexRowRegistros + ", \">0\")");
					
					// Total
					XSSFCell cellSubTotalDescrTotal = rowSubtotal.createCell(7);
					cellSubTotalDescrTotal.setCellValue(localization.getMessage("total") + ": ");
					cellSubTotalDescrTotal.setCellStyle(subtotalDescrTotalCellStyle);
					
					XSSFCell cellSubTotalValorTotal = rowSubtotal.createCell(8);
					cellSubTotalValorTotal.setCellStyle(subtotalValorTotalCellStyle);
					cellSubTotalValorTotal.setCellType(Cell.CELL_TYPE_FORMULA);
					cellSubTotalValorTotal.setCellFormula("SUM(I" + (rowPosition + 1) + ":I" + indexRowRegistros + ")");
					
					// Merge Total
					relatorio.addMergedRegion(new CellRangeAddress(indexRowRegistros, indexRowRegistros, 0, 2));
					periodoList.put(Integer.decode(_order), indexRowRegistros + 1);
					_order = resultSet.getString("_order");
					indexRowRegistros++;
					rowPosition = indexRowRegistros;
					
					
					// Funcao Soma Total
					func_sum_des.add("E" + indexRowRegistros);
					func_sum_rec.add("G" + indexRowRegistros);
					func_sum.add("I" + indexRowRegistros);
					
				}
				
				XSSFRow rowRegistro = relatorio.createRow(indexRowRegistros);
				
				// Data
				XSSFCell cellData = rowRegistro.createCell(0);
				cellData.setCellStyle(dataCellStyle);
				cellData.setCellValue(resultSet.getDate("_data"));
				
				// Descricao
				XSSFCell cellDescricao = rowRegistro.createCell(1);
				cellDescricao.setCellStyle(descricaoCellStyle);
				cellDescricao.setCellValue(resultSet.getString("descricao"));
								
				// Meio
				XSSFCell cellMeio = rowRegistro.createCell(5);
				cellMeio.setCellStyle(meioCellStyle);
				cellMeio.setCellValue(resultSet.getString("meio_nome"));
								
				// C. Cred. / Conta
				XSSFCell cellCCreditoConta = rowRegistro.createCell(4);
				cellCCreditoConta.setCellStyle(cCredContaCellStyle);
				cellCCreditoConta.setCellValue(resultSet.getString("meio_conta"));
				
				// Doc
				XSSFCell cellDoc = rowRegistro.createCell(3);
				cellDoc.setCellStyle(docCellStyle);
				cellDoc.setCellValue(resultSet.getString("numerodoc_nome"));
				
				// Parcela
				XSSFCell cellParcela = rowRegistro.createCell(6);
				cellParcela.setCellStyle(parcCellStyle);
				cellParcela.setCellValue(resultSet.getString("parcela"));
				
				// Status
				XSSFCell cellStatus = rowRegistro.createCell(7);
				cellStatus.setCellStyle(statusCellStyle);
				cellStatus.setCellValue(resultSet.getString("status_nome"));
				
				// Valor
				XSSFCell cellValor = rowRegistro.createCell(8);
				cellValor.setCellStyle(valorCellStyle);
				cellValor.setCellValue(resultSet.getDouble("valor"));
				
				// Merge Descricao
				relatorio.addMergedRegion(new CellRangeAddress(indexRowRegistros, indexRowRegistros, 1, 2));
				
				
								
				indexRowRegistros++;
			}
			
			
			// SubTotal
			XSSFRow rowSubtotal = relatorio.createRow(indexRowRegistros);
			rowSubtotal.setRowStyle(subtotalRowStyle);
			
			// SubTotal
			XSSFCell cellSubTotalDescr = rowSubtotal.createCell(0);
			String subTotalDescr = null;
			if(subtotal.equals(SubtotalTipo.DIA)) {
				subTotalDescr = LocalDate.parse(_order, DateTimeFormat.forPattern("yyyyMMdd")).toString(localization.getMessage("SubtotalTipo.DIA.pattern"));
			} else if (subtotal.equals(SubtotalTipo.MES)) {
				subTotalDescr = LocalDate.parse(_order, DateTimeFormat.forPattern("yyyyMM")).toString(localization.getMessage("SubtotalTipo.MES.pattern"));
			} else if(subtotal.equals(SubtotalTipo.ANO)) {
				subTotalDescr = LocalDate.parse(_order, DateTimeFormat.forPattern("yyyy")).toString(localization.getMessage("SubtotalTipo.ANO.pattern"));
			}
			cellSubTotalDescr.setCellStyle(subtotalDescrCellStyle);
			cellSubTotalDescr.setCellValue(subTotalDescr);
			
			rowSubtotal.createCell(1).setCellStyle(subtotalDescrCellStyle);
			rowSubtotal.createCell(2).setCellStyle(subtotalDescrCellStyle);
			
			// Despesas
			XSSFCell cellSubTotalDescrDes = rowSubtotal.createCell(3);
			cellSubTotalDescrDes.setCellValue(localization.getMessage("despesas") + ": ");
			cellSubTotalDescrDes.setCellStyle(subtotalDescrDesCellStyle);
			
			XSSFCell cellSubTotalValorDes = rowSubtotal.createCell(4);
			cellSubTotalValorDes.setCellStyle(subtotalValorDesCellStyle);
			cellSubTotalValorDes.setCellType(Cell.CELL_TYPE_FORMULA);
			cellSubTotalValorDes.setCellFormula("ABS(SUMIF(I" + (rowPosition + 1) + ":I" + indexRowRegistros + ", \"<0\"))");
			
			// Receitas
			XSSFCell cellSubTotalDescrRec = rowSubtotal.createCell(5);
			cellSubTotalDescrRec.setCellValue(localization.getMessage("receitas") + ": ");
			cellSubTotalDescrRec.setCellStyle(subtotalDescrRecCellStyle);
			
			XSSFCell cellSubTotalValorRec = rowSubtotal.createCell(6);
			cellSubTotalValorRec.setCellStyle(subtotalValorRecCellStyle);
			cellSubTotalValorRec.setCellType(Cell.CELL_TYPE_FORMULA);
			cellSubTotalValorRec.setCellFormula("SUMIF(I" + (rowPosition + 1) + ":I" + indexRowRegistros + ", \">0\")");
			
			// Total
			XSSFCell cellSubTotalDescrTotal = rowSubtotal.createCell(7);
			cellSubTotalDescrTotal.setCellValue(localization.getMessage("total") + ": ");
			cellSubTotalDescrTotal.setCellStyle(subtotalDescrTotalCellStyle);
			
			XSSFCell cellSubTotalValorTotal = rowSubtotal.createCell(8);
			cellSubTotalValorTotal.setCellStyle(subtotalValorTotalCellStyle);
			cellSubTotalValorTotal.setCellType(Cell.CELL_TYPE_FORMULA);
			cellSubTotalValorTotal.setCellFormula("SUM(I" + (rowPosition + 1) + ":I" + indexRowRegistros + ")");
			
			// Merge Total
			relatorio.addMergedRegion(new CellRangeAddress(indexRowRegistros, indexRowRegistros, 0, 2));
			indexRowRegistros++;
			
			
			// Funcao Soma Total
			func_sum_des.add("E" + indexRowRegistros);
			func_sum_rec.add("G" + indexRowRegistros);
			func_sum.add("I" + indexRowRegistros);
			
			periodoList.put(Integer.decode(_order), indexRowRegistros);
			// -- SubTotal
			
			
			// TOTAL
			XSSFRow rowTotal = relatorio.createRow(indexRowRegistros);
			cellSubTotalDescr = rowTotal.createCell(0);
			
			cellSubTotalDescr.setCellStyle(subtotalDescrCellStyle);
			cellSubTotalDescr.setCellValue(localization.getMessage("totalGeral"));
			
			rowTotal.createCell(1).setCellStyle(subtotalDescrCellStyle);
			rowTotal.createCell(2).setCellStyle(subtotalDescrCellStyle);
			
			// Despesas
			cellSubTotalDescrDes = rowTotal.createCell(3);
			cellSubTotalDescrDes.setCellValue(localization.getMessage("despesas") + ": ");
			cellSubTotalDescrDes.setCellStyle(subtotalDescrDesCellStyle);
			
			cellSubTotalValorDes = rowTotal.createCell(4);
			cellSubTotalValorDes.setCellStyle(subtotalValorDesCellStyle);
			cellSubTotalValorDes.setCellType(Cell.CELL_TYPE_FORMULA);
			cellSubTotalValorDes.setCellFormula("SUM(" + StringUtils.join(func_sum_des.toArray(), ",") + ")");
						
			// Receitas
			cellSubTotalDescrRec = rowTotal.createCell(5);
			cellSubTotalDescrRec.setCellValue(localization.getMessage("receitas") + ": ");
			cellSubTotalDescrRec.setCellStyle(subtotalDescrRecCellStyle);
			
			cellSubTotalValorRec = rowTotal.createCell(6);
			cellSubTotalValorRec.setCellStyle(subtotalValorRecCellStyle);
			cellSubTotalValorRec.setCellType(Cell.CELL_TYPE_FORMULA);
			cellSubTotalValorRec.setCellFormula("SUM(" + StringUtils.join(func_sum_rec.toArray(), ",") + ")");
			
			// Total
			cellSubTotalDescrTotal = rowTotal.createCell(7);
			cellSubTotalDescrTotal.setCellValue(localization.getMessage("total") + ": ");
			cellSubTotalDescrTotal.setCellStyle(subtotalDescrTotalCellStyle);
			
			cellSubTotalValorTotal = rowTotal.createCell(8);
			cellSubTotalValorTotal.setCellStyle(subtotalValorTotalCellStyle);
			cellSubTotalValorTotal.setCellType(Cell.CELL_TYPE_FORMULA);
			cellSubTotalValorTotal.setCellFormula("SUM(" + StringUtils.join(func_sum.toArray(), ",") + ")");
			
			// Merge Total
			relatorio.addMergedRegion(new CellRangeAddress(indexRowRegistros, indexRowRegistros, 0, 2));
			indexRowRegistros++;
			// -- TOTAL
			
			
			// Regra DES
			XSSFSheetConditionalFormatting condFormatDes = relatorio.getSheetConditionalFormatting();
			XSSFConditionalFormattingRule ruleCondFormatDes = condFormatDes.createConditionalFormattingRule(CFRuleRecord.ComparisonOperator.LT, "0");
			
			XSSFFontFormatting ruleConfFormatDesPattern = ruleCondFormatDes.createFontFormatting();
            ruleConfFormatDesPattern.setFontColorIndex(IndexedColors.RED.getIndex());
            
            /* Create a Cell Range Address */
            CellRangeAddress[] dataRangeCondFormatDes = {CellRangeAddress.valueOf("I4:I" + indexRowRegistros)};
                        
            /* Attach rule to cell range */
            condFormatDes.addConditionalFormatting(dataRangeCondFormatDes, ruleCondFormatDes);
			
			
            // Regra REC
			XSSFSheetConditionalFormatting condFormatRec = relatorio.getSheetConditionalFormatting();
			XSSFConditionalFormattingRule ruleCondFormatRec = condFormatRec.createConditionalFormattingRule(CFRuleRecord.ComparisonOperator.GE, "0");
			
			XSSFFontFormatting ruleConfFormatRecPattern = ruleCondFormatRec.createFontFormatting();
            ruleConfFormatRecPattern.setFontColorIndex(IndexedColors.BLUE.getIndex());
            
            /* Create a Cell Range Address */
            CellRangeAddress[] dataRangeCondFormatRec = {CellRangeAddress.valueOf("I4:I" + indexRowRegistros)};
            
            /* Attach rule to cell range */
            condFormatRec.addConditionalFormatting(dataRangeCondFormatRec, ruleCondFormatRec);
            			
			
            // Names
            XSSFSheet grafico = workbook.getSheet("Grafico");
            grafico.getRow(0).getCell(1).setCellValue(localization.getMessage("despesas"));
            grafico.getRow(0).getCell(2).setCellValue(localization.getMessage("receitas"));
            
            int rowIndex = 1;
            XSSFRow row;
            XSSFCell cellPeriodo;
            XSSFCell cellDespesas;
            XSSFCell cellReceitas;
            XSSFCellStyle cellStyleDespesas = null;
            XSSFCellStyle cellStyleReceitas = null;
            while(resultSetChart.next()) {
	            if(rowIndex == 1) {
	            	row = grafico.getRow(rowIndex);
	            	cellPeriodo = row.getCell(0);
	            	cellDespesas = row.getCell(1);
	            	cellReceitas = row.getCell(2);
	            	cellStyleDespesas = cellDespesas.getCellStyle();
	            	cellStyleReceitas = cellReceitas.getCellStyle();
	            } else {
	            	row = grafico.createRow(rowIndex);
	            	cellPeriodo = row.createCell(0);
	            	cellDespesas = row.createCell(1);
	            	cellDespesas.setCellStyle(cellStyleDespesas);
	            	cellReceitas = row.createCell(2);
	            	cellReceitas.setCellStyle(cellStyleReceitas);
		               
	            }
	            
	            // Periodo
	            cellPeriodo.setCellType(Cell.CELL_TYPE_FORMULA);
	            cellPeriodo.setCellFormula("Relatorio!$A$" + periodoList.get(resultSetChart.getInt("_order")));
	            
	            // Despesas
	            cellDespesas.setCellType(Cell.CELL_TYPE_FORMULA);
	            cellDespesas.setCellFormula("Relatorio!$E$" + periodoList.get(resultSetChart.getInt("_order")));
	            
	            // Receitas
	            cellReceitas.setCellType(Cell.CELL_TYPE_FORMULA);
	            cellReceitas.setCellFormula("Relatorio!$G$" + periodoList.get(resultSetChart.getInt("_order")));
	            
	            rowIndex++;
	        }
            
            
            Name namePeriodo = workbook.getName("Periodo");
			namePeriodo.setRefersToFormula("Grafico!$A$2:$A$" + rowIndex);
			
			Name nameDespesas = workbook.getName("Despesas");
			nameDespesas.setRefersToFormula("Grafico!$B$2:$B$" + rowIndex);
			
			Name nameReceitas = workbook.getName("Receitas");
			nameReceitas.setRefersToFormula("Grafico!$C$2:$C$" + rowIndex);
						
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			workbook.write(out);
			inputStream = new ByteArrayInputStream(out.toByteArray());
			download = new InputStreamDownload(inputStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "relatorio_data.xlsx");
			break;
		}
		
		
		
		return download;
		
	}
	
	
}
