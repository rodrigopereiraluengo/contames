package br.com.contames.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.Cheque;
import br.com.contames.entity.TalaoCheque;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.repository.BaixaItemRepository;
import br.com.contames.repository.ChequeRepository;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.repository.LancamentoParcelaRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.repository.TalaoChequeRepository;

@Private
@Resource
public class ChequeController {

	private final ChequeRepository chequeRepository;
	private final PessoaRepository pessoaRepository;
	private final BaixaItemRepository baixaItemRepository;
	private final TalaoChequeRepository talaoChequeRepository;
	private final DescricaoRepository descricaoRepository;
	private final LancamentoParcelaRepository lancamentoParcelaRepository;
	private final Result result;
	private final Validator validator;
	
	public ChequeController(ChequeRepository chequeRepository, PessoaRepository pessoaRepository, BaixaItemRepository baixaItemRepository, TalaoChequeRepository talaoChequeRepository, DescricaoRepository descricaoRepository, LancamentoParcelaRepository lancamentoParcelaRepository, Result result, Validator validator) {
		this.chequeRepository = chequeRepository;
		this.pessoaRepository = pessoaRepository;
		this.baixaItemRepository = baixaItemRepository;
		this.talaoChequeRepository = talaoChequeRepository;
		this.descricaoRepository = descricaoRepository;
		this.lancamentoParcelaRepository = lancamentoParcelaRepository;
		this.result = result;
		this.validator = validator;
	}
	
	
	@Get
	public void dialogProps(Cheque cheque, boolean isReadOnly) {
		
		if(cheque.getBanco() != null && cheque.getBanco().getId() != null) {
			cheque.setBanco(pessoaRepository.find(cheque.getBanco().getId()));
		}
		
		if(cheque.getNome() != null && cheque.getNome().getId() != null) {
			cheque.setNome(pessoaRepository.find(cheque.getNome().getId()));
		}
		
		result
			.include("cheque", cheque)
			.include("isReadOnly", isReadOnly);
		
	}
	
	
	@Post
	public void dialogPropsValidate(@NotNull(message = "{cheque.notNull}") Cheque cheque) {
		
		BaixaItem baixaItem = null;
		if(cheque != null && cheque.getId() != null) {
			baixaItem = baixaItemRepository.findByChequeCom(cheque);
			if(baixaItem != null) {
				cheque.setNumero(baixaItem.getCheque().getNumero());
				cheque.setData(baixaItem.getCheque().getData());
			}
		}
		
		if(cheque != null && cheque.getNumero() != null) {
									
			validator.validateProperties(cheque, "banco", "numero", "nome", "data", "obs");
			
			// Verifica Banco
			if(cheque.getBanco() != null && cheque.getBanco().getId() == null) {
				cheque.setBanco(null);
			}
			if(cheque.getBanco() != null) {
				cheque.setBanco(pessoaRepository.findBanco(cheque.getBanco().getId()));
			}
			
			// Verifica Pessoa
			if(cheque.getNome() != null && cheque.getNome().getId() == null) {
				cheque.setNome(null);
			}
			if(cheque.getNome() != null) {
				cheque.setNome(pessoaRepository.findOrCliente(cheque.getNome().getId()));
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("dialogPropsChequeSuccess").withoutRoot().from(cheque).excludeAll().include("id", "numero", "banco", "data", "nome", "obs").serialize();
		
	}
	
	@Post
	public void restaurar(Long id) {
		
		Cheque cheque = chequeRepository.find(id);
		TalaoCheque talaoCheque = null;
		if(cheque == null) {
			validator.add(new I18nMessage("talaoCheque.cheque", "talaoCheque.cheque.notFound"));
		} else {
			
			talaoCheque = cheque.getTalaoCheque();
			
			if(LancamentoStatus.CAN.equals(cheque.getStatus())) {
				
				if(cheque.getTalaoCheque() == null) {
					validator.add(new I18nMessage("talaoCheque.cheque", "talaoCheque.cheque.restaurar.invalid"));
				} else {
					chequeRepository.delete(cheque).getEntityManager().getEntityManagerFactory().getCache().evict(TalaoCheque.class, talaoCheque.getId());
				}
				
			} else {
				validator.add(new I18nMessage("talaoCheque.cheque", "talaoCheque.cheque.restaurar.invalid.status"));
			}
		}
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		List<Cheque> chequeList = chequeRepository.findAll(talaoCheque);
		
		result.include("chequeList", chequeList).forwardTo("/WEB-INF/jsp/conta/chequeList.jsp");
		
	}
	
	

	@Get
	public void dialogCancelar(Long talaoChequeId) {
		
		result
			.include("motivoList", descricaoRepository.findAllActive(DescricaoTipo.MOTIVO_CAN_CHQ))
			.include("talaoCheque", talaoChequeRepository.find(talaoChequeId));
	}
	
	
	@Post
	public void dialogCancelar(Long talaoChequeId, Cheque cheque) {
		
		TalaoCheque talaoCheque = talaoChequeRepository.find(talaoChequeId);
		if(talaoCheque == null) {
			validator.add(new I18nMessage("talaoCheque", "talaoCheque.notFound"));
		} else {
			cheque.setTalaoCheque(talaoCheque);
		}
		
		Cheque salvo = chequeRepository.findBy(cheque.getNumero(), talaoCheque);
		
		if(cheque.getMotivo() != null && cheque.getMotivo().getId() == null) {
			cheque.setMotivo(null);
		} 
		
		if(cheque.getMotivo() != null) {
			cheque.setMotivo(descricaoRepository.find(cheque.getMotivo().getId(), DescricaoTipo.MOTIVO_CAN_CHQ));
		}
		
		if(salvo == null) {
			if(cheque.getNumero() < talaoCheque.getSeqInicio()) {
				validator.add(new I18nMessage("numero", "cheque.cancelar.numero.before.seqIni", talaoCheque.getSeqInicio()));
			}
			if(cheque.getNumero() > talaoCheque.getSeqFim()) {
				validator.add(new I18nMessage("numero", "cheque.cancelar.numero.after.seqFim", talaoCheque.getSeqFim()));
			}
			
			if(!validator.hasErrors()) {
				cheque.setStatus(LancamentoStatus.CAN);
				chequeRepository.save(cheque);
			}
		} else {
			
			if(LancamentoStatus.COM.equals(salvo.getStatus()) || LancamentoStatus.BXD.equals(salvo.getStatus())) {
				validator.add(new I18nMessage("numero", "cheque.cancelar.status." + salvo.getStatus()));
			} else if(salvo.getLancamentoParcela() != null) {
				salvo.setMotivo(cheque.getMotivo());
				salvo.setStatus(LancamentoStatus.CAN);
				salvo.getLancamentoParcela().setCheque(null);
				lancamentoParcelaRepository.save(salvo.getLancamentoParcela());
				chequeRepository.save(salvo);
			} else if(salvo.getTransferencia() != null) {
				validator.add(new I18nMessage("numero", "cheque.cancelar.status." + salvo.getStatus() + ".transferencia"));
			} else {
				salvo.setMotivo(cheque.getMotivo());
				salvo.setStatus(LancamentoStatus.CAN);
				chequeRepository.save(salvo);
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		chequeRepository.getEntityManager().getEntityManagerFactory().getCache().evict(TalaoCheque.class, talaoCheque.getId());
		
		List<Cheque> chequeList = chequeRepository.findAll(talaoCheque);
		result.include("chequeList", chequeList).forwardTo("/WEB-INF/jsp/conta/chequeList.jsp");
	}
	
}
