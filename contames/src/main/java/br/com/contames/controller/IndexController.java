package br.com.contames.controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.contames.annotation.NoSSL;

@NoSSL
@Resource
public class IndexController {
	
	@Get("/")
	public void index() { }
	
	@Get("/como-funciona")
	public void comoFunciona() { }
	
	@Get("/demonstracao")
	public void demonstracao() { }
	
}