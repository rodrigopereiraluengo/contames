package br.com.contames.controller.convert;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;

@Convert(Locale.class)
public class LocaleConverter implements Converter<Locale> {

	@Override
	public Locale convert(String value, Class<? extends Locale> type, ResourceBundle bundle) {
		
		if(StringUtils.isNotBlank(value)) {
			try {
				return Locale.forLanguageTag(value);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
}
