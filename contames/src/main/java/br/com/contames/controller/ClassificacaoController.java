package br.com.contames.controller;

import java.util.List;

import javax.persistence.NoResultException;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.entity.Classificacao;
import br.com.contames.enumeration.ClassificacaoNivel;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.repository.ClassificacaoRepository;
import br.com.contames.session.UsuarioLogged;

@Private
@Resource
public class ClassificacaoController {

	private final UsuarioLogged usuarioLogged;
	private final ClassificacaoRepository classificacaoRepository;
	private final Localization localization;
	private final Result result;
	private final Validator validator;
	private final PlanoRecurso planoRecurso;
	
	public ClassificacaoController(UsuarioLogged usuarioLogged, ClassificacaoRepository classificacaoRepository, Localization localization, Result result, Validator validator, PlanoRecurso planoRecurso) {
		this.usuarioLogged = usuarioLogged;
		this.classificacaoRepository = classificacaoRepository;
		this.localization = localization;
		this.result = result;
		this.validator = validator;
		this.planoRecurso = planoRecurso;
	}
		
	@SuppressWarnings("incomplete-switch")
	public void nova(Long id, ItemTipo tipo, ClassificacaoNivel nivel, Integer index) {
		
		// Verifica espaco no banco de dados
		if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
		
		Classificacao classificacao = new Classificacao();
		Classificacao parent = null;
		if(id != null) {
			parent = classificacaoRepository.find(id);
			try {
				classificacao.setClassificacao(parent);
			} catch(NoResultException e) {
				
			}
		}
		
		if(parent != null) {
			nivel = parent.getNivel();
		}
		
		if(ClassificacaoNivel.DET.equals(nivel)) {
			validator.add(new I18nMessage("classificacao.nivel", "classificacao.nivel.invalid"));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		if(nivel == null) {
			classificacao.setNivel(ClassificacaoNivel.CAT);
		} else {
			switch(nivel) {
			case CAT:
				classificacao.setNivel(ClassificacaoNivel.FAM);
				break;
			case FAM:
				classificacao.setNivel(ClassificacaoNivel.TIP);
				break;
			case TIP:
				classificacao.setNivel(ClassificacaoNivel.DET);
				break;
			}
		}
		
		classificacao.setIndex(index);
		classificacao.setTipo(tipo);
		classificacao.setCliente(usuarioLogged.getCliente());
		
		String nome = null;
		if(parent == null) {
			nome = localization.getMessage("classificacao") + " " + (classificacao.getIndex() + 1);
		} else {
			nome = parent.getNome() + " " + (classificacao.getIndex() + 1);
		}
		
		classificacao.setNome(nome);
		
		validator.validate(classificacao);
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		classificacaoRepository.save(classificacao).flush();
		
		
		result.use(Results.jsonp()).withCallback("novaClassifSuccess").withoutRoot().from(classificacao).excludeAll().include("id", "nome", "nivel", "index").serialize();
	}
	
	@Post
	public void renomear(Classificacao classificacao) {
		validator.validateProperties(classificacao, "nome");
		
		// Verifica nome igual
		if(classificacaoRepository.isNameExists(classificacao)) {
			validator.add(new I18nMessage("classificacao.nome", "classificacao.nome.exists"));
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("renomearClassifError").withoutRoot().from(validator.getErrors()).serialize();
		
		classificacaoRepository.rename(classificacao);
		
		result.use(Results.jsonp()).withCallback("renomearClassifSuccess").withoutRoot().from("").serialize();
	}
	
	
	public void excluir(Long id) {
		
		// Verifica se tem algum item
		/*if(classificacaoRepository.isFoundItem(id)) {
			validator.add(new I18nMessage("item.classificacao", "item.classificacao.found"));
		}*/
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		classificacaoRepository.remove(id);
		
		result.use(Results.jsonp()).withCallback("excluirClassifSuccess").withoutRoot().from("").serialize();
	}
	
	
	@Get
	public void carrega(Long id, ItemTipo tipo) {
		
		List<Classificacao> classificacaoList = null; 
		if(id == null) {
			classificacaoList = classificacaoRepository.findAll(tipo);
		} else {
			classificacaoList = classificacaoRepository.findAll(id, tipo);
		}
		
		if(classificacaoRepository == null) {
			result.notFound();
		} else {
			result.include("classificacaoList", classificacaoList);
		}
	
	}
	
	
	@Post
	public void mover(Classificacao classificacao, List<Classificacao> indexList) {
		Classificacao parent = null;
		if(classificacao.getClassificacao() != null && classificacao.getClassificacao().getId() != null) {
			parent = classificacaoRepository.find(classificacao.getClassificacao().getId());
		}
		
		if(parent == null) {
			classificacao.setNivel(ClassificacaoNivel.CAT);
		} else if(parent.getNivel().equals(ClassificacaoNivel.CAT)) {
			classificacao.setNivel(ClassificacaoNivel.FAM);
		} else if(parent.getNivel().equals(ClassificacaoNivel.FAM)) {
			classificacao.setNivel(ClassificacaoNivel.TIP);
		} else if(parent.getNivel().equals(ClassificacaoNivel.TIP)) {
			classificacao.setNivel(ClassificacaoNivel.DET);
		}
		
		classificacaoRepository.move(classificacao);
		
		if(indexList != null && !indexList.isEmpty()) {
			for(int i = 0; i < indexList.size(); i++) {
				classificacaoRepository.updateIndex(indexList.get(i));
			}
		}
		
		result.nothing();
	}
	
	
	@Get
	public void multipleAutoComplete(String q) {
		List<Classificacao> classificacaoList = classificacaoRepository.search(q);
		result.include("classificacaoList", classificacaoList);
	}
	
}



