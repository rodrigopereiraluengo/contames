package br.com.contames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDate;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Lancamento;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.repository.ItemRepository;
import br.com.contames.repository.LancamentoRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class CartaoCreditoController {

	private final UsuarioLogged usuarioLogged;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final DescricaoRepository descricaoRepository;
	private final PessoaRepository pessoaRepository;
	private final ItemRepository itemRepository;
	private final LancamentoRepository lancamentoRepository;
	private final Settings settings;
	private final Localization localization;
	private final Validator validator;
	private final Result result;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	private final ArquivoRepository arquivoRepository;
	
	public CartaoCreditoController(UsuarioLogged usuarioLogged, CartaoCreditoRepository cartaoCreditoRepository, DescricaoRepository descricaoRepository, PessoaRepository pessoaRepository, ItemRepository itemRepository, LancamentoRepository lancamentoRepository, Settings settings, Localization localization, Validator validator, Result result, PlanoRecurso planoRecurso, ArquivoSession arquivoSession, ArquivoRepository arquivoRepository) {
		this.usuarioLogged = usuarioLogged;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.descricaoRepository = descricaoRepository;
		this.pessoaRepository = pessoaRepository;
		this.itemRepository = itemRepository;
		this.lancamentoRepository = lancamentoRepository;
		this.settings = settings;
		this.localization = localization;
		this.validator = validator;
		this.result = result;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
		this.arquivoRepository = arquivoRepository;
	}
	
	
	@Get("/cadastros/cartoes")
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page, Status status) {
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("cc.id, ");
		sqlBuilder.append("Bandeira.nome AS bandeira, ");
		sqlBuilder.append("cc.nome, ");
		sqlBuilder.append("COALESCE(Titular.nome, Titular.razaoSocial) AS titular, ");
		sqlBuilder.append("CASE WHEN cc.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' ");
		sqlBuilder.append("WHEN cc.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' ");
		sqlBuilder.append("END AS status, ");
		sqlBuilder.append("cc.status _status, ");
		sqlBuilder.append("CASE WHEN cc.isPrincipal THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END AS isPrincipal, ");
		sqlBuilder.append("cc.diaVencimento, ");
		sqlBuilder.append("cc.diasPagar, ");
		sqlBuilder.append("cc.limite, ");
		sqlBuilder.append("cc.limiteUtilizado, ");
		sqlBuilder.append("cc.limiteDisponivel, ");
		sqlBuilder.append("cc.obs ");
		sqlBuilder.append("FROM CartaoCredito cc ");
		sqlBuilder.append("LEFT JOIN Descricao Bandeira ON cc.bandeira_id = Bandeira.id ");
		sqlBuilder.append("LEFT JOIN Pessoa AS Titular ON cc.titular_id = Titular.id ");
		sqlBuilder.append("WHERE cc.cliente_id = ?1");
		
		if(status != null) {
			sqlBuilder.append(" AND cc.status = ?2");
		}
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("unaccent(LOWER(Bandeira.nome)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(Titular.nome, Titular.razaoSocial))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN cc.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN cc.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN cc.isPrincipal THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(cc.nome)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(cc.diaVencimento AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(cc.diasPagar AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(cc.limite AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(cc.limiteUtilizado AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(cc.limiteDisponivel AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append(")");
		}
		
		Query countQuery = cartaoCreditoRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS CartaoCredito ");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		if(status != null) {
			countQuery.setParameter(2, status.toString());
		}
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
				
		Query query = cartaoCreditoRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		if(status != null) {
			query.setParameter(2, status.toString());
		}
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("cartaoCreditoList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/cartaoCredito/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(Long id) {
		
		if(id == null && planoRecurso.getCartoesCreditoDisp() == 0) {
			validator.add(new I18nMessage("plano.cartoesCredito", "plano.cartoesCredito.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getCartoesCredito()));
		}
		
		// Verifica espaco no banco de dados
		if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
				
		CartaoCredito cartaoCredito = null;
		Lancamento ultimaFatura = null;
		boolean isUtilizado = false;
		if(id == null) {
			CartaoCredito principal = cartaoCreditoRepository.findPrincipal();
			cartaoCredito = new CartaoCredito();
			cartaoCredito.setTitular(usuarioLogged.getUsuario());
			if(principal == null) {
				cartaoCredito.setIsPrincipal(true);
			}
		} else {
			try {
				cartaoCredito = cartaoCreditoRepository.find(id);
				isUtilizado = cartaoCreditoRepository.isUtilizado(cartaoCredito.getId());
				ultimaFatura = lancamentoRepository.ultimaFatura(cartaoCredito);
			}catch(NoResultException e) {
				result.notFound();
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		String viewid = StringHelper.createViewId();
		
		arquivoSession.setAquivoList(viewid, cartaoCredito.getArquivoList());
		
		result
			.include("viewid", viewid)
			.include("cartaoCredito", cartaoCredito)
			.include("isUtilizado", isUtilizado)
			.include("ultimaFatura", ultimaFatura)
			.include("bandeiraList", descricaoRepository.findAllActive(DescricaoTipo.CARTAO_BANDEIRA));
	
	
	}
	
	
	@Post
	public void cadastro(@NotNull(message = "{cartaoCredito.notNull}") CartaoCredito cartaoCredito, String viewid) {
		
		if(cartaoCredito != null) {
			
			boolean isUtilizado = cartaoCreditoRepository.isUtilizado(cartaoCredito.getId());
			
			// Limite
			if(cartaoCredito.getLimite() != null) {
				
				if(!isUtilizado) {
														
					if(cartaoCredito.getDataFatuAnt() != null || cartaoCredito.getValorFaturaAnt() != null || cartaoCredito.getLimiteDisponivelAnt() != null) {
					
						if(cartaoCredito.getDataFatuAnt() == null) {
							validator.add(new I18nMessage("dataFaturaAnt", "dataFatuAnt.notNull"));
						}
						
						if(cartaoCredito.getValorFaturaAnt() == null) {
							validator.add(new I18nMessage("valorFaturaAnt", "valorFaturaAnt.notNull"));
						}
						
						if(cartaoCredito.getLimiteDisponivelAnt() == null) {
							validator.add(new I18nMessage("limiteDisponivelAnt", "limiteDisponivelAnt.notNull"));
						}
						
						if(cartaoCredito.getValorFaturaAnt() != null && cartaoCredito.getLimiteDisponivelAnt() != null) {
							cartaoCredito.setLimiteDisponivel(cartaoCredito.getLimiteDisponivelAnt() + cartaoCredito.getValorFaturaAnt());
							cartaoCredito.setLimiteUtilizado(cartaoCredito.getLimite() - cartaoCredito.getLimiteDisponivel());
						}
					} else {
						cartaoCredito.setLimiteDisponivel(cartaoCredito.getLimite());
						cartaoCredito.setLimiteUtilizado(0.0);
					}
					
				}
			} else {
				cartaoCredito.setValorFaturaAnt(null);
				cartaoCredito.setLimiteDisponivelAnt(null);
				cartaoCredito.setLimiteUtilizado(null);
				cartaoCredito.setLimiteDisponivel(null);
			}
			
			
			// Verifica se este cartaoCredito eh principal
			if(cartaoCredito.getIsPrincipal() != null && cartaoCredito.getIsPrincipal()) {
				
				// Pega a cartaoCredito principal anterior
				CartaoCredito principal = cartaoCreditoRepository.findPrincipal();
				
				// Verifica se existe cartaoCredito principal e se eh diferente da cartaoCredito
				if(principal != null && !cartaoCredito.equals(principal)) {
					
					// Coloca principal como false
					principal.setIsPrincipal(false);
					
					// Salva cartaoCredito principal
					cartaoCreditoRepository.save(principal);
				}
			}
			
			
			// Busca bandeira
			if(cartaoCredito.getBandeira() != null && cartaoCredito.getBandeira().getId() == null) {
				cartaoCredito.setBandeira(null);
			}
			if(cartaoCredito.getBandeira() != null) {
				cartaoCredito.setBandeira(descricaoRepository.find(cartaoCredito.getBandeira().getId(), DescricaoTipo.CARTAO_BANDEIRA));
			}
			
			// Verifica Titular
			if(cartaoCredito.getTitular() != null && cartaoCredito.getTitular().getId() == null) {
				cartaoCredito.setTitular(null);
			}
			if(cartaoCredito.getTitular() != null) {
				cartaoCredito.setTitular(pessoaRepository.findOrCliente(cartaoCredito.getTitular().getId()));
			}
			
			if(cartaoCreditoRepository.exists(cartaoCredito)) {
				validator.add(new I18nMessage("nome", "cartaoCredito.nome.exists"));
			}
			if(isUtilizado) {
				validator.validateProperties(cartaoCredito, "bandeira", "status", "nome", "limite", "limiteDisponivel", "limiteUtilizado", "diaVencimento", "diasPagar");
			} else {
				validator.validateProperties(cartaoCredito, "bandeira", "status", "nome", "limite", "diaVencimento", "diasPagar");
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("cartaoCreditoCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		cartaoCreditoRepository.save(cartaoCredito);
		
		
		// Recupera Lista de Arquivos
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		for(Arquivo arquivo : arquivoList) {
			arquivo.setCartaoCredito(cartaoCredito);
			arquivoRepository.save(arquivo);
		}
		
		
		// Recupera Lista de Arquivos a Deletar
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		for(Arquivo arquivo : arquivoDeleteList) {
			arquivoRepository.delete(arquivo);
		}
		
		// Limpa Lista de Arquivos da Session
		arquivoSession.clear(viewid);
		
		result.use(Results.jsonp()).withCallback("cartaoCreditoCadastroSuccess").withoutRoot().from(cartaoCredito).exclude("cliente").serialize();
		
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		for(int i = 0; i < ids.size(); i++) {
			
			// Verifica se foi selecionada num item como lancamento automatico
			if(cartaoCreditoRepository.existsItemLancAuto(ids.get(i))) {
				validator.add(new I18nMessage("cartaoCredito_" + ids.get(i), "cartaoCredito.item.lancAuto.exists"));
			}
			
			// Verifica se existe Lancamento
			if(cartaoCreditoRepository.existLancamentoParcela(ids.get(i))) {
				validator.add(new I18nMessage("cartaoCredito_" + ids.get(i), "cartaoCredito.lancamentoParcela.exists"));
			}
			
			// Verifica se existe Baixa
			if(cartaoCreditoRepository.existBaixaItem(ids.get(i))) {
				validator.add(new I18nMessage("cartaoCredito_" + ids.get(i), "cartaoCredito.baixaItem.exists"));
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		cartaoCreditoRepository.deleteList(ids);
		
		result.use(Results.jsonp()).withCallback("cartaoCreditoExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
	
	@Get
	public void carregaSelect(Long id) {
		result
			.include("id", id)
			.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive());
		
	}
	
	@Get
	public void dialogSelect(Long id, Boolean isLimite, LocalDate data, boolean isReadOnly) {
		CartaoCredito principal = cartaoCreditoRepository.findPrincipal();
		
		if(id == null && isLimite == null && principal != null && principal.getDataFatuAnt() != null && data != null) {
			LocalDate dataFatuAnt = principal.getDataFatuAnt();
			if(principal.getDiasPagar() != null) {
				dataFatuAnt = dataFatuAnt.minusMonths(1).plusDays(principal.getDiasPagar());
			}
			
			if(data.isBefore(dataFatuAnt)) {
				isLimite = false;
			}
			
		}
		
		CartaoCredito cartaoCredito = null;
		if(isReadOnly && id != null) {
			cartaoCredito = cartaoCreditoRepository.find(id);
		}
		
		result
			.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive())
			.include("isLimite", isLimite == null ? true : isLimite)
			.include("data", data)
			.include("id", id)
			.include("isReadOnly", isReadOnly)
			.include("cartaoCredito", cartaoCredito);
	}
	
	
	@Post
	public void dialogSelect(@NotNull(message="{cartaoCredito.notNull}") CartaoCredito cartaoCredito, Boolean isLimite) {
		
		if(cartaoCredito != null && cartaoCredito.getId() != null) {
			try {
				cartaoCredito = cartaoCreditoRepository.find(cartaoCredito.getId());
			}catch(NoResultException e) {
				validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.invalid"));
			}
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("dialogSelectCartaoCreditoSuccess").withoutRoot().from(new Object[]{cartaoCredito, isLimite}).serialize();
	}
	
	
	@Get
	public void carregaMultiSelect() {
		List<CartaoCredito> cartaoCreditoList = cartaoCreditoRepository.findAll();
		if(cartaoCreditoList == null) {
			result.notFound();
		} else {
			result.use(Results.json()).withoutRoot().from(cartaoCreditoRepository.findAll()).excludeAll().include("id", "nome", "isPrincipal", "status").serialize();
		}
	}
	
}
