package br.com.contames.controller.convert;

import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;

@Convert(LocalDate.class)
public class LocalDateConverter implements Converter<LocalDate> {

	@Override
	public LocalDate convert(String value, Class<? extends LocalDate> type, ResourceBundle bundle) {
		if(StringUtils.isNotBlank(value)) {
			value = value.trim();
			String pattern = bundle.getString("patternDate");
			
			try {
				DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
				return LocalDate.parse(value, formatter);
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

}
