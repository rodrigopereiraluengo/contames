package br.com.contames.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.simplemail.AsyncMailer;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.NoSSL;
import br.com.contames.component.EmailHelper;

@NoSSL
@Resource
public class ContatoController {
	
	private final Environment environment;
	private final HttpServletRequest request;
	private final Localization localization;
	private final Validator validator;
	private final Result result;
	private final AsyncMailer mailer;
	private final EmailHelper _email;
	
	public ContatoController(Environment environment, HttpServletRequest request, Localization localization, Validator validator, Result result, AsyncMailer mailer, EmailHelper _email) {
		this.environment = environment;
		this.request = request;
		this.localization = localization;
		this.validator = validator;
		this.result = result;
		this.mailer = mailer;
		this._email = _email;
	}

	@Get({"/contato", "/suporte"})
	public void contato() { }
	
	@Post("/contato")
	public void contato(
			
			@NotBlank(message = "{nome.notBlank}")
			@Size(max = 64, message = "{nome.size}")
			String nome,
			
			@NotBlank(message = "{email.notBlank}")
			@Email(message = "{email.invalid}")
			@Size(max = 127, message = "{email.size}")
			String email, 
			
			@NotBlank(message = "{assunto.notBlank}")
			@Size(max = 64, message = "{assunto.size}")
			String assunto,
			
			@NotBlank(message = "{mensagem.notBlank}")
			@Size(max = 400, message = "{mensagem.size}")
			String mensagem,
			
			@NotBlank(message = "{capcha.notBlank}")
			String recaptcha_challenge_field,
			
			@NotBlank(message = "{capcha.notBlank}")
			String recaptcha_response_field) {
		
		
		// Verifica se CAPTCHA esta correto
		if(StringUtils.isNotBlank(recaptcha_response_field)) {
			
				Boolean isValid = false;
				try {
					isValid = BooleanUtils.toBoolean(Request.Post("http://www.google.com/recaptcha/api/verify").bodyForm(Form.form()
							.add("privatekey", environment.get("recapcha.privatekey"))
							.add("remoteip", request.getRemoteAddr())
							.add("challenge", recaptcha_challenge_field)
							.add("response", recaptcha_response_field).build())
					.execute()
					.returnContent().asString().split("\\n")[0]);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(!isValid) {
					validator.add(new I18nMessage("captcha", "captcha.invalid"));
				}
			
		}
        
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		// Envia e-mail para o contato
		try {
			SimpleEmail simpleEmail = new SimpleEmail();
			simpleEmail.addTo(localization.getMessage("email.contato.from.email"), "utf-8");
			simpleEmail.setFrom(email, nome);
			simpleEmail.setSubject(assunto);
			simpleEmail.setMsg(mensagem);
			mailer.asyncSend(simpleEmail);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		_email
			.addTo(email, nome)
			.setFrom(localization.getMessage("email.noreply.from.email"), localization.getMessage("email.noreply.from.name"))
			.setSubject(localization.getMessage("email.contato.confirmacao.subject", assunto))
			.setHtmlMsg(localization.getMessage("email.contato.confirmacao.htmlMsg", nome))
			.setTextMsg(localization.getMessage("email.contato.confirmacao.textMsg", nome))
			.send();
		
		result.use(Results.jsonp()).withCallback("success").from(StringUtils.EMPTY).serialize();
		
	}
	
}
