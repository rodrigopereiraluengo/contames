package br.com.contames.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.xml.sax.SAXException;

import urn.ebay.api.PayPalAPI.CreateRecurringPaymentsProfileResponseType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.LandingPageType;
import urn.ebay.apis.eBLBaseComponents.RecurringPaymentsProfileStatusType;
import urn.ebay.apis.eBLBaseComponents.RefundType;
import urn.ebay.apis.eBLBaseComponents.StatusChangeActionType;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.util.jpa.extra.Load;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.ForceSSL;
import br.com.contames.annotation.NoSSL;
import br.com.contames.component.EmailHelper;
import br.com.contames.component.PaypalBuilder;
import br.com.contames.component.Settings;
import br.com.contames.entity.Assinatura;
import br.com.contames.entity.AssinaturaPagamento;
import br.com.contames.entity.Migrar;
import br.com.contames.entity.Pessoa;
import br.com.contames.entity.Plano;
import br.com.contames.enumeration.AssinaturaPagamentoStatus;
import br.com.contames.enumeration.AssinaturaStatus;
import br.com.contames.enumeration.PessoaTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.AssinaturaPagamentoRepository;
import br.com.contames.repository.AssinaturaRepository;
import br.com.contames.repository.MigrarRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.repository.PlanoRepository;
import br.com.contames.session.AssinaturaSession;

import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.ipn.IPNMessage;
import com.paypal.sdk.exceptions.OAuthException;


@Resource
public class AssinaturaController {

	private final Result result;
	private final Validator validator;
	private final Localization localization;
	private final Environment environment;
	private final PlanoRepository planoRepository;
	private final PessoaRepository pessoaRepository;
	private final AssinaturaRepository assinaturaRepository;
	private final AssinaturaPagamentoRepository assinaturaPagamentoRepository;
	private final MigrarRepository migrarRepository;
	private final AssinaturaSession assinaturaSession;
	private final EmailHelper emailHelper;
	private final PaypalBuilder paypalBuilder;
	private final Settings settings;
	private final HttpSession session;
	private final HttpServletRequest request;
	
	public AssinaturaController(Result result, Validator validator, Localization localization, Environment environment, PlanoRepository planoRepository, PessoaRepository pessoaRepository, AssinaturaRepository assinaturaRepository, AssinaturaPagamentoRepository assinaturaPagamentoRepository, MigrarRepository migrarRepository, AssinaturaSession assinaturaSession, EmailHelper emailHelper, PaypalBuilder paypalBuilder, Settings settings, HttpSession session, HttpServletRequest request) {
		this.result = result;
		this.validator = validator;
		this.localization = localization;
		this.environment = environment;
		this.planoRepository = planoRepository;
		this.pessoaRepository = pessoaRepository;
		this.assinaturaRepository = assinaturaRepository;
		this.assinaturaPagamentoRepository = assinaturaPagamentoRepository;
		this.migrarRepository = migrarRepository;
		this.assinaturaSession = assinaturaSession;
		this.emailHelper = emailHelper;
		this.paypalBuilder = paypalBuilder;
		this.settings = settings;
		this.session = session;
		this.request = request;
	}
	
	@NoSSL
	@Get("/planos")
	public List<Plano> planos() {
		return planoRepository.findAllActive();
	}
	
	@ForceSSL
	@Get("/plano/{plano.id}/assinatura")
	public void assinatura(@Load Plano plano) {
		result.include("plano", plano);
	}
	
	@Post
	public void codigoVerificador(@Email(message = "{email.email}") String email) {
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(StringUtils.isNotBlank(email)) {
			
			// Tenta recuperar um usuario com esse email
			Boolean exists = pessoaRepository.exists(email);
			
			// Verifica se o user e null
			if(!exists) {
				
				// Verifica se ja foi criado um codigo para este email
				if(!assinaturaSession.getCodigoVerificador().containsKey(email)) {
					
					String code = RandomStringUtils.randomAlphanumeric(6).toUpperCase();
					
					assinaturaSession.getCodigoVerificador().put(email, code);
					
					emailHelper
						.addTo(email, email)
						.setSubject(localization.getMessage("codigoVerificador"))
						.setHtmlMsg(localization.getMessage("email.assinatura.email.codigoVerificador.html", code))
						.setTextMsg(localization.getMessage("email.assinatura.email.codigoVerificador.plain", code))
						.send();
				}
			} else {
				validator.add(new I18nMessage("email", "assinatura.cliente.email.exists", email));
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(StringUtils.isNotBlank(email)) {
			result.use(Results.json()).withoutRoot().from(email).serialize();
		} else {
			result.nothing();
		}
	}
	
	private Boolean sendEmailCode(String email) {
		Boolean exists = pessoaRepository.exists(email);
		if(!exists && !assinaturaSession.getCodigoVerificador().containsKey(email)) {
			
			String code = RandomStringUtils.randomAlphanumeric(6).toUpperCase();
			
			assinaturaSession.getCodigoVerificador().put(email, code);
			
			emailHelper
				.addTo(email)
				.setSubject(localization.getMessage("codigoVerificador"))
				.setHtmlMsg(localization.getMessage("email.assinatura.email.codigoVerificador.html", code))
				.setTextMsg(localization.getMessage("email.assinatura.email.codigoVerificador.plain", code))
				.send();
			
			return true;
		} else {
			return false;
		}
	}
	
	@Post("/plano/{plano.id}/assinatura")
	public void assinatura(@Load Plano plano, Pessoa cliente, String codigoVerificador, String confirmeSenha, @AssertTrue(message = "{assinatura.isAceitoTermos.assertTrue}") boolean isAceitoTermos, @NotNull(message="{assinatura.landingPageType.notNull}") LandingPageType landingPageType) {
		
		// Verificar se o plano nao esta inativo
		if(!plano.getStatus().equals(Status.ATI)) {
			validator.add(new I18nMessage("plano", "plano.invalido"));
		}
		
		// Verificar se ja nao existe um usuario com este e-mail
		Boolean exists = pessoaRepository.exists(cliente.getEmail());
		if(exists) {
			validator.add(new I18nMessage("email", "email.exists", cliente.getEmail()));
		}
		
		// Valida Cliente
		
		String[] propsValidator = {"email", "senha"};
		
		validator.validateProperties(cliente, propsValidator);

		// Email
		if(StringUtils.isBlank(cliente.getEmail())) {
			validator.add(new I18nMessage("email", "email.notBlank"));
		}
		
		// Verifica se eh producao
		if(environment.supports("production")) {
			
			// Verifica se o codigoVerificador existe
			if(assinaturaSession.getCodigoVerificador().containsKey(cliente.getEmail())) {
				if(!assinaturaSession.getCodigoVerificador().get(cliente.getEmail()).toLowerCase().equals(codigoVerificador.toLowerCase())) {
					validator.add(new I18nMessage("codigoVerificador", "codigoVerificador.invalido"));
				}
			} else if(sendEmailCode(cliente.getEmail())) {
				validator.add(new I18nMessage("codigoVerificador", "assinatura.cliente.email.enviamosCodigoVerificador", cliente.getEmail()));
			}
		}
		
		// Senha
		if(StringUtils.isBlank(cliente.getSenha())) {
			validator.add(new I18nMessage("senha", "senha.notBlank"));
		}
		
		// Confirme a Senha
		if(StringUtils.isBlank(confirmeSenha)) {
			validator.add(new I18nMessage("confirmeSenha", "confirmeSenha.notBlank"));
		}
		
		// Verifica se a senha confere
		if(StringUtils.isNotBlank(confirmeSenha) && ObjectUtils.notEqual(confirmeSenha, cliente.getSenha())) {
			validator.add(new I18nMessage("confirmeSenha", "confirmeSenha.notEqual"));
		}
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		// Pessoa Tipo
		cliente.setPessoaTipo(PessoaTipo.F);
		cliente.setNome(StringUtils.split(cliente.getEmail(), "@")[0]);
		
		Assinatura assinatura = new Assinatura();
		assinatura.setCriacao(LocalDateTime.now());
		assinatura.setVencimento(LocalDateTime.now());
		assinatura.setIsAceitaTermos(isAceitoTermos);
		assinatura.setStatus(AssinaturaStatus.STD);
		assinatura.setPlano(plano);
		assinatura.setCliente(cliente);
		assinatura.setSessionId(session.getId());
		
		AssinaturaPagamento assinaturaPagamento = new AssinaturaPagamento();
		assinaturaPagamento.setAssinatura(assinatura);
		assinaturaPagamento.setPlano(plano);
		assinaturaPagamento.setCriacao(LocalDateTime.now());
		assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.STD);
		assinaturaPagamento.setLandingPageType(landingPageType);
		assinaturaPagamento.setValor(plano.getValor());
		
		assinatura.getAssinaturaPagamentoList().add(assinaturaPagamento);
		
		
		cliente.setCriacao(LocalDateTime.now());
		cliente.setStatus(Status.ATI);
		
		validator.validate(cliente);
		validator.validate(assinatura);
		validator.validate(assinaturaPagamento);
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		pessoaRepository.save(cliente, true);
		assinaturaRepository.save(assinatura);
		assinaturaPagamentoRepository.save(assinaturaPagamento);
				
		paypalBuilder.setNome(plano.getNome()).with(assinaturaPagamento).setExpressCheckout();
		
		assinaturaPagamento.setAlteracao(LocalDateTime.now());
		assinaturaPagamentoRepository.save(assinaturaPagamento);
		
		// Recupera Token do Paypal
		if(StringUtils.isBlank(assinaturaPagamento.getToken())) {
			validator.add(new I18nMessage("assinaturaPagamento", "token.notBlank"));
		}
		
		// Envia json com errors caso a validacao falha
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("assinaturaSuccess").withoutRoot().from(assinaturaPagamento).excludeAll().include("token").serialize();
		
	}
	
	@ForceSSL
	@Get("/assinatura/pagamento/cancelar")
	public void pagamentoCancelar(String token) {
		
		AssinaturaPagamento assinaturaPagamento = assinaturaPagamentoRepository.findByToken(token);
		
		if(assinaturaPagamento == null || (assinaturaPagamento != null && !assinaturaPagamento.getAssinatura().getSessionId().equals(session.getId()))) {
			result.notFound();
		} else {
			
			// Verifica se o status e STANDBY
			if(assinaturaPagamento.getStatus().equals(AssinaturaPagamentoStatus.STD)) {
				
				// Pega data e hora atual
				LocalDateTime date = LocalDateTime.now();
				
				// Verifica se a data atual menos 25 minutos e inferior a data da atualizacao pagamento
				if(date.minusMinutes(25).isAfter(new LocalDateTime(assinaturaPagamento.getAlteracao()))) {
					
					// Gera um novo pagamento no paypal
					paypalBuilder.with(assinaturaPagamento).setExpressCheckout();
					
					// Data de atualizacao
					assinaturaPagamento.setAlteracao(LocalDateTime.now());
										
					// Salva alteracoes em payment
					assinaturaPagamentoRepository.save(assinaturaPagamento);
					
				}
				
			} 
			
			if(!token.equals(assinaturaPagamento.getToken())) {
				result.redirectTo("/assinatura/pagamento/cancelar?token=" + assinaturaPagamento.getToken());
			} else {
				result.include(assinaturaPagamento);
			}
			
		}
		
	}
	
	@ForceSSL
	@Get("/assinatura/confirmar")
	public void confirmar(String token, String PayerID) {
		
		AssinaturaPagamento assinaturaPagamento = assinaturaPagamentoRepository.findByToken(token);
		
		if(assinaturaPagamento == null) {
			result.notFound();
		} else {
			
			if(assinaturaPagamento.getAssinatura().getStatus().equals(AssinaturaStatus.ATI)) {
				result.include("successo", true).include("confirmado", true);
			} else {
				boolean isSucesso = true;
				boolean isConfirmado = true;
				if(BooleanUtils.isNotTrue(assinaturaPagamento.getIsProcessado())) {
					assinaturaPagamento.setIsProcessado(true);
					CreateRecurringPaymentsProfileResponseType res = paypalBuilder.setNome(assinaturaPagamento.getAssinatura().getPlano().getNome()).with(assinaturaPagamento).createRecurringPaymentsProfile();
					if(res == null || !res.getAck().equals(AckCodeType.SUCCESS)) {
						isSucesso = false;
					} else if(res.getAck().equals(AckCodeType.SUCCESS)) {
						if(RecurringPaymentsProfileStatusType.ACTIVEPROFILE.equals(res.getCreateRecurringPaymentsProfileResponseDetails().getProfileStatus())) {
							assinaturaPagamento.getAssinatura().setStatus(AssinaturaStatus.ATI);
						}
						assinaturaPagamento.getAssinatura().setProfileId(res.getCreateRecurringPaymentsProfileResponseDetails().getProfileID());
					}
				}
				assinaturaPagamento.setAlteracao(LocalDateTime.now());
				assinaturaPagamentoRepository.save(assinaturaPagamento);
				result.include("successo", isSucesso).include("confirmado", isConfirmado).include("assinatura", assinaturaPagamento.getAssinatura());
			}
		
		}
	}
	
	
	@Get("/assinatura/isConfirmado")
	public void isConfirmado(String token) {
		AssinaturaPagamento assinaturaPagamento = assinaturaPagamentoRepository.findByToken(token);
		if(assinaturaPagamento == null) {
			result.notFound();
		} else {
			result.use(Results.http()).body(assinaturaPagamento.getAssinatura().getStatus().equals(AssinaturaStatus.ATI) ? "1" : "");
		}
	}
	
	
	@Post
	public void ipn() {
		
		if("refund".equals(request.getParameter("reason_code"))) {
			
			if("Refunded".equals(request.getParameter("payment_status"))) {
				
				AssinaturaPagamento assinaturaPagamento = assinaturaPagamentoRepository.findByTxnId(request.getParameter("parent_txn_id"));
				if(assinaturaPagamento != null && assinaturaPagamento.getStatus().equals(AssinaturaPagamentoStatus.CON)) {
					if(assinaturaPagamento.getIsParcial() != null && assinaturaPagamento.getIsParcial()) {
						emailHelper
							.addTo(assinaturaPagamento.getAssinatura().getCliente().getEmail(), assinaturaPagamento.getAssinatura().getCliente().getDisplayNome())
							.setSubject(localization.getMessage("email.devolucao.parcial.subject", assinaturaPagamento.getPlano().getNome()))
							.setHtmlMsg(localization.getMessage("email.devolucao.parcial.htmlMsg", assinaturaPagamento.getAssinatura().getCliente().getDisplayNome(), assinaturaPagamento.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(assinaturaPagamento.getValorDevolvido())))
							.setTextMsg(localization.getMessage("email.devolucao.parcial.textMsg", assinaturaPagamento.getAssinatura().getCliente().getDisplayNome(), assinaturaPagamento.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(assinaturaPagamento.getValorDevolvido())))
							.send();
					} else {
						emailHelper
							.addTo(assinaturaPagamento.getAssinatura().getCliente().getEmail(), assinaturaPagamento.getAssinatura().getCliente().getDisplayNome())
							.setSubject(localization.getMessage("email.devolucao.completa.subject", assinaturaPagamento.getPlano().getNome()))
							.setHtmlMsg(localization.getMessage("email.devolucao.completa.htmlMsg", assinaturaPagamento.getAssinatura().getCliente().getDisplayNome(), assinaturaPagamento.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(assinaturaPagamento.getValorDevolvido())))
							.setTextMsg(localization.getMessage("email.devolucao.completa.textMsg", assinaturaPagamento.getAssinatura().getCliente().getDisplayNome(), assinaturaPagamento.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(assinaturaPagamento.getValorDevolvido())))
							.send();
					}
					assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.REF);
					assinaturaPagamentoRepository.save(assinaturaPagamento);
				}
				
			}
			
		} else {
			
			IPNMessage ipn = new IPNMessage(request, paypalBuilder.getConfiguration());
			String profileId;
			String profileStatus;
			Assinatura assinatura;
			Pessoa cliente;
			AssinaturaPagamento assinaturaPagamento;
			Migrar migrar;
			if(ipn.validate()) {
			
				switch(ipn.getTransactionType()) {
				case "recurring_payment_profile_created": // Assinatura realizada com sucesso, porem o pagamento ainda nao foi feito
					
					profileId = ipn.getIpnValue("recurring_payment_id");
					profileStatus = ipn.getIpnValue("profile_status");
					if(!profileStatus.endsWith("Profile")) { profileStatus += "Profile"; }
					
					migrar = migrarRepository.findByProfileId(profileId);
					if(migrar != null) {
						
						try {
							paypalBuilder.changeStatusProfile(migrar.getOldProfileId(), StatusChangeActionType.CANCEL);
						} catch (SSLConfigurationException
								| InvalidCredentialException | HttpErrorException
								| InvalidResponseDataException
								| ClientActionRequiredException
								| MissingCredentialException | OAuthException
								| IOException | InterruptedException
								| ParserConfigurationException | SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						migrar.getAssinatura().setPlano(migrar.getPlano());
						migrar.getAssinatura().setProfileId(migrar.getProfileId());
						migrar.getAssinatura().setProfileStatus(RecurringPaymentsProfileStatusType.valueOf(profileStatus.toUpperCase()));
						LocalDateTime vencimento = LocalDateTime.parse(ipn.getIpnValue("next_payment_date"), DateTimeFormat.forPattern(settings.getConfiguration().getString("paypal.date.format")));
						migrar.getAssinatura().setVencimento(vencimento);
						assinaturaRepository.save(migrar.getAssinatura());
					}
					
					assinatura = assinaturaRepository.findByProfileId(profileId);
					
					if(assinatura != null) {
						RecurringPaymentsProfileStatusType profileStatusType = RecurringPaymentsProfileStatusType.valueOf(profileStatus.toUpperCase());
						assinatura.setProfileStatus(profileStatusType);
						if(RecurringPaymentsProfileStatusType.ACTIVEPROFILE.equals(profileStatusType)) {
							assinatura.setStatus(AssinaturaStatus.ATI);
							assinatura.setCanceladoEm(null);
							LocalDateTime vencimento = LocalDateTime.parse(ipn.getIpnValue("next_payment_date"), DateTimeFormat.forPattern(settings.getConfiguration().getString("paypal.date.format")));
							assinatura.setVencimento(vencimento);
							cliente = assinatura.getCliente();
							if(cliente.getDisplayNome() == null || cliente.getAlteracao() == null) {
								String firstName = ipn.getIpnValue("first_name");
								String lastName = ipn.getIpnValue("last_name");
								cliente.setNome(firstName + " " + lastName);
								cliente.setPayerId(ipn.getIpnValue("payer_id"));
								pessoaRepository.save(cliente);
							}
							
							assinaturaRepository.save(assinatura);
												
							if(migrar == null) {
								// E-mail de Boas Vindas
								emailHelper
									.addTo(cliente.getEmail(), cliente.getDisplayNome())
									.setSubject(localization.getMessage("email.assinatura.subject", assinatura.getPlano().getNome()))
									.setHtmlMsg(localization.getMessage("email.assinatura.htmlMsg", cliente.getDisplayNome()))
									.setTextMsg(localization.getMessage("email.assinatura.textMsg", cliente.getDisplayNome()))
									.send();
							} else {
								// E-mail de Migracao
								emailHelper
									.addTo(cliente.getEmail(), cliente.getDisplayNome())
									.setSubject(localization.getMessage("email.migrar.subject", assinatura.getPlano().getNome()))
									.setHtmlMsg(localization.getMessage("email.migrar.htmlMsg", cliente.getDisplayNome(), assinatura.getPlano().getNome()))
									.setTextMsg(localization.getMessage("email.migrar.textMsg", cliente.getDisplayNome(), assinatura.getPlano().getNome()))
									.send();
							}
						
						}
					}
					
					break;
				case "recurring_payment": // Pagamento
					
					profileId = ipn.getIpnValue("recurring_payment_id");
					
					migrar = migrarRepository.findByProfileId(profileId);
					if(migrar != null) {
						if(migrar.getStatus().equals(Status.STD)) {
							migrar.setStatus(Status.ATI);
							migrarRepository.save(migrar);
						}
					}
					
					assinatura = assinaturaRepository.findByProfileId(profileId);
					if(assinatura != null) {
					
						cliente = assinatura.getCliente();
						
						assinaturaPagamento = assinaturaPagamentoRepository.findLastStd(assinatura);
						if(assinaturaPagamento == null) {
							assinaturaPagamento = new AssinaturaPagamento();
							assinaturaPagamento.setAssinatura(assinatura);
							assinaturaPagamento.setPlano(assinatura.getPlano());
							assinaturaPagamento.setCriacao(LocalDateTime.now());
							assinaturaPagamento.setValor(Double.parseDouble(ipn.getIpnValue("amount")));
							assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.STD);
							assinaturaPagamentoRepository.save(assinaturaPagamento);
						} else {
							assinaturaPagamento.setAlteracao(LocalDateTime.now());
						}
						
						
						cliente = assinatura.getCliente();
						if(cliente.getDisplayNome() == null) {
							String firstName = ipn.getIpnValue("first_name");
							String lastName = ipn.getIpnValue("last_name");
							cliente.setNome(firstName + " " + lastName);
							cliente.setPayerId(ipn.getIpnValue("payer_id"));
							pessoaRepository.save(cliente);
						}
						
						if(ipn.getIpnValue("payment_status").equals("Completed")) {
							assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.CON);
							LocalDateTime pagamento = LocalDateTime.parse(ipn.getIpnValue("payment_date"), DateTimeFormat.forPattern(settings.getConfiguration().getString("paypal.date.format")));
							assinaturaPagamento.setPagamento(pagamento);
							assinaturaPagamento.setTxnId(ipn.getIpnValue("txn_id"));
							assinaturaPagamento.setProfileId(profileId);
							LocalDateTime proxPagamento = LocalDateTime.parse(ipn.getIpnValue("next_payment_date"), DateTimeFormat.forPattern(settings.getConfiguration().getString("paypal.date.format")));
							assinaturaPagamento.setProxPagamento(proxPagamento);
							assinaturaPagamentoRepository.save(assinaturaPagamento);
							
							// E-mail de Confirmacao de Pagamento
							emailHelper
								.addTo(cliente.getEmail(), cliente.getDisplayNome())
								.setSubject(localization.getMessage("email.pagamento.confirmado.subject", assinatura.getPlano().getNome()))
								.setHtmlMsg(localization.getMessage("email.pagamento.confirmado.htmlMsg", cliente.getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(assinaturaPagamento.getValor())))
								.setTextMsg(localization.getMessage("email.pagamento.confirmado.textMsg", cliente.getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(assinaturaPagamento.getValor())))
								.send();
						
						}
					}
					
					break;
					
				case "recurring_payment_failed":
					
					profileId = ipn.getIpnValue("recurring_payment_id");
					assinatura = assinaturaRepository.findByProfileId(profileId);
					
					if(assinatura != null) {
						
						assinatura.setStatus(AssinaturaStatus.PEN);
						assinaturaRepository.save(assinatura);
						
						assinaturaPagamento = assinaturaPagamentoRepository.findLastStd(assinatura);
						if(assinaturaPagamento == null) {
							assinaturaPagamento = new AssinaturaPagamento();
							assinaturaPagamento.setAssinatura(assinatura);
							assinaturaPagamento.setPlano(assinatura.getPlano());
							assinaturaPagamento.setCriacao(LocalDateTime.now());
							assinaturaPagamento.setValor(Double.parseDouble(ipn.getIpnValue("amount")));
							assinaturaPagamento.setStatus(AssinaturaPagamentoStatus.STD);
							assinaturaPagamento.setTried(2);
							assinaturaPagamentoRepository.save(assinaturaPagamento);
						} else {
							assinaturaPagamento.setAlteracao(LocalDateTime.now());
							Integer tried = assinaturaPagamento.getTried();
							tried--;
							assinaturaPagamento.setTried(tried);
							assinaturaPagamentoRepository.save(assinaturaPagamento);
						}
					
						Double valor = Double.parseDouble(ipn.getIpnValue("amount"));
						
						// E-mail de Confirmacao de Cancelamento
						emailHelper
							.addTo(assinatura.getCliente().getEmail(), assinatura.getCliente().getDisplayNome())
							.setSubject(localization.getMessage("email.cobranca.naosucessida.subject", assinatura.getPlano().getNome()))
							.setHtmlMsg(localization.getMessage("email.cobranca.naosucessida.htmlMsg", assinatura.getCliente().getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(valor), assinaturaPagamento.getTried()))
							.setTextMsg(localization.getMessage("email.cobranca.naosucessida.textMsg", assinatura.getCliente().getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(valor), assinaturaPagamento.getTried()))
							.send();
						
					}
					
					break;
					
				case "recurring_payment_profile_skipped":
					
					profileId = ipn.getIpnValue("recurring_payment_id");
					assinatura = assinaturaRepository.findByProfileId(profileId);
					
					if(assinatura != null) {
						
						assinatura.setStatus(AssinaturaStatus.PEN);
						assinaturaRepository.save(assinatura);
						
					}
					
					break;
					
				case "recurring_payment_suspended_due_to_max_failed_payment":
				
					profileId = ipn.getIpnValue("recurring_payment_id");
					assinatura = assinaturaRepository.findByProfileId(profileId);
					
					if(assinatura != null) {
						
						assinatura.setStatus(AssinaturaStatus.INA);
						assinaturaRepository.save(assinatura);
						
						Double valor = Double.parseDouble(ipn.getIpnValue("amount"));
						
						// E-mail de Confirmacao de Cancelamento
						emailHelper
							.addTo(assinatura.getCliente().getEmail(), assinatura.getCliente().getDisplayNome())
							.setSubject(localization.getMessage("email.assinatura.suspensa.subject", assinatura.getPlano().getNome()))
							.setHtmlMsg(localization.getMessage("email.assinatura.suspensa.htmlMsg", assinatura.getCliente().getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(valor)))
							.setTextMsg(localization.getMessage("email.assinatura.suspensa.textMsg", assinatura.getCliente().getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(valor)))
							.send();
					}
					
					break;
									
				case "recurring_payment_profile_cancel":
					
					profileId = ipn.getIpnValue("recurring_payment_id");
					
					// Assinatura
					assinatura = assinaturaRepository.findByProfileId(profileId);
					if(assinatura != null && !assinatura.getStatus().equals(AssinaturaStatus.CAN)) {
						profileStatus = ipn.getIpnValue("profile_status");
						if(!profileStatus.endsWith("Profile")) { profileStatus += "Profile"; }
						RecurringPaymentsProfileStatusType profileStatusType = RecurringPaymentsProfileStatusType.valueOf(profileStatus.toUpperCase());
						assinatura.setProfileStatus(profileStatusType);
						assinatura.setAlteracao(LocalDateTime.now());
						assinatura.setStatus(AssinaturaStatus.CAN);
						assinatura.setCanceladoEm(LocalDate.now());
						assinaturaRepository.save(assinatura);
						
						cliente = assinatura.getCliente();
						if(cliente.getDisplayNome() == null) {
							String firstName = ipn.getIpnValue("first_name");
							String lastName = ipn.getIpnValue("last_name");
							cliente.setNome(firstName + " " + lastName);
							cliente.setPayerId(ipn.getIpnValue("payer_id"));
							pessoaRepository.save(cliente);
						}
						
						
						Double valor = Double.parseDouble(ipn.getIpnValue("amount"));
						
						// E-mail de Confirmacao de Cancelamento
						emailHelper
							.addTo(cliente.getEmail(), cliente.getDisplayNome())
							.setSubject(localization.getMessage("email.assinatura.cancelada.subject", assinatura.getPlano().getNome()))
							.setHtmlMsg(localization.getMessage("email.assinatura.cancelada.htmlMsg", cliente.getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(valor)))
							.setTextMsg(localization.getMessage("email.assinatura.cancelada.textMsg", cliente.getDisplayNome(), assinatura.getPlano().getNome(), new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(valor)))
							.send();
						
						// Recupera ultimo pagamento CON para a assinatura
						assinaturaPagamento = assinaturaPagamentoRepository.findLastByStatus(profileId, AssinaturaPagamentoStatus.CON);
						if(assinaturaPagamento != null && assinaturaPagamento.getTxnId() != null) {
						
							LocalDateTime dataPagamento = assinaturaPagamento.getPagamento();
							LocalDateTime proximoPagamento = assinaturaPagamento.getProxPagamento();
							int dias = Days.daysBetween(dataPagamento, proximoPagamento).getDays();
							valor = assinaturaPagamento.getValor();
							double valorDevolver = valor;
							double valorDia = valor / dias;
							
							int diasUtilizado = Days.daysBetween(dataPagamento, LocalDateTime.now()).getDays();
							/*boolean isParcial = false;
							if(diasUtilizado > 0) {
								valorDevolver = valor - (valorDia * diasUtilizado);
								isParcial = true;
							}*/
							
							if(diasUtilizado <= 7) {
								/*if(isParcial) {
									paypalBuilder.refundProfilePayment(RefundType.PARTIAL, valorDevolver, assinaturaPagamento.getAssinatura().getCliente().getPayerId(), assinaturaPagamento.getTxnId(), localization.getMessage("email.devolucao.parcial.subject", assinaturaPagamento.getPlano().getNome()));
									assinaturaPagamento.setIsParcial(true);
									assinaturaPagamento.setValorDevolvido(valorDevolver);
								} else {*/
									paypalBuilder.refundProfilePayment(RefundType.FULL, valorDevolver, assinaturaPagamento.getAssinatura().getCliente().getPayerId(), assinaturaPagamento.getTxnId(), localization.getMessage("email.devolucao.completa.subject", assinaturaPagamento.getPlano().getNome()));
									assinaturaPagamento.setIsParcial(false);
									assinaturaPagamento.setValorDevolvido(valorDevolver);
								//}
								assinaturaPagamentoRepository.save(assinaturaPagamento);
							}
						}
						
					}
					
					
					// Migrar
					migrar = migrarRepository.findByOldProfileId(profileId);
					if(migrar != null) {
						
						
						// Recupera ultimo pagamento CON para a assinatura
						assinaturaPagamento = assinaturaPagamentoRepository.findLastByStatus(profileId, AssinaturaPagamentoStatus.CON);
						if(assinaturaPagamento != null && assinaturaPagamento.getTxnId() != null) {
						
							LocalDateTime dataPagamento = assinaturaPagamento.getPagamento();
							LocalDateTime proximoPagamento = assinaturaPagamento.getProxPagamento();
							int dias = Days.daysBetween(dataPagamento, proximoPagamento).getDays();
							double valor = assinaturaPagamento.getValor();
							double valorDevolver = valor;
							double valorDia = valor / dias;
							
							int diasUtilizado = Days.daysBetween(dataPagamento, LocalDateTime.now()).getDays();
							boolean isParcial = false;
							if(diasUtilizado > 0) {
								valorDevolver = valor - (valorDia * diasUtilizado);
								isParcial = true;
							}
							
							if(valorDevolver > 0) {
								if(isParcial) {
									paypalBuilder.refundProfilePayment(RefundType.PARTIAL, valorDevolver, assinaturaPagamento.getAssinatura().getCliente().getPayerId(), assinaturaPagamento.getTxnId(), localization.getMessage("email.devolucao.parcial.subject", assinaturaPagamento.getPlano().getNome()));
									assinaturaPagamento.setIsParcial(true);
									assinaturaPagamento.setValorDevolvido(valorDevolver);
								} else {
									paypalBuilder.refundProfilePayment(RefundType.FULL, valorDevolver, assinaturaPagamento.getAssinatura().getCliente().getPayerId(), assinaturaPagamento.getTxnId(), localization.getMessage("email.devolucao.completa.subject", assinaturaPagamento.getPlano().getNome()));
									assinaturaPagamento.setIsParcial(false);
									assinaturaPagamento.setValorDevolvido(valorDevolver);
								}
								assinaturaPagamentoRepository.save(assinaturaPagamento);
							}
						}
						
					}
					
					
					break;
				}
				
			}
		}
		
		result.nothing();
	
	}
		
}
