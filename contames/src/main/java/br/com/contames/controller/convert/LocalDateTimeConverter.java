package br.com.contames.controller.convert;

import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;

@Convert(LocalDateTime.class)
public class LocalDateTimeConverter implements Converter<LocalDateTime> {

	@Override
	public LocalDateTime convert(String value, Class<? extends LocalDateTime> type, ResourceBundle bundle) {
		
		if(StringUtils.isNotBlank(value)) {
			value = value.trim();
			String pattern;
			
			if(value.length() == 8) {
				pattern = bundle.getString("patternDate");
			}else if(value.length() == 14) {
				pattern = bundle.getString("patternDateTime");
			}else {
				pattern = bundle.getString("patternTime");
			}
			System.out.println(value.length());
			System.out.println(pattern);
			
			try {
				DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
				return LocalDateTime.parse(value, formatter);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
		return null;
	}

}
