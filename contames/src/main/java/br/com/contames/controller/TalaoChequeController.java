package br.com.contames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.Cheque;
import br.com.contames.entity.Conta;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.entity.TalaoCheque;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.BaixaItemRepository;
import br.com.contames.repository.ChequeRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.LancamentoParcelaRepository;
import br.com.contames.repository.TalaoChequeRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class TalaoChequeController {

	private final TalaoChequeRepository talaoChequeRepository;
	private final ChequeRepository chequeRepository;
	private final ContaRepository contaRepository;
	private final LancamentoParcelaRepository lancamentoParcelaRepository;
	private final BaixaItemRepository baixaItemRepository;
	private final HttpServletRequest request;
	private final Settings settings;
	private final UsuarioLogged usuarioLogged;
	private final Localization localization;
	private final Result result;
	private final Validator validator;
	private final PlanoRecurso planoRecurso;
	
	
	public TalaoChequeController(TalaoChequeRepository talaoChequeRepository, ChequeRepository chequeRepository, ContaRepository contaRepository, LancamentoParcelaRepository lancamentoParcelaRepository, BaixaItemRepository baixaItemRepository, HttpServletRequest request, Settings settings, UsuarioLogged usuarioLogged, Localization localization, Result result, Validator validator, PlanoRecurso planoRecurso) {
		this.talaoChequeRepository = talaoChequeRepository;
		this.chequeRepository = chequeRepository;
		this.contaRepository = contaRepository;
		this.lancamentoParcelaRepository = lancamentoParcelaRepository;
		this.baixaItemRepository = baixaItemRepository;
		this.request = request;
		this.settings = settings;
		this.usuarioLogged = usuarioLogged;
		this.localization = localization;
		this.result = result;
		this.validator = validator;
		this.planoRecurso = planoRecurso;
	}
	
	
	@Get("/cadastros/taloes")
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page, Status status) {
				
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("t.id, ");
		sqlBuilder.append("t._data, ");
		sqlBuilder.append("c.nome AS conta, ");
		sqlBuilder.append("t.seqInicio, ");
		sqlBuilder.append("t.folhas, ");
		sqlBuilder.append("t.seqFim, ");
		sqlBuilder.append("CASE WHEN t.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN t.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END status, ");
		sqlBuilder.append("t.status _status, ");
		sqlBuilder.append("t.obs ");
		sqlBuilder.append("FROM TalaoCheque t, Conta c ");
		sqlBuilder.append("WHERE t.conta_id = c.id AND c.cliente_id = ?1");
		
		if(status != null) {
			sqlBuilder.append(" AND t.status = ?2");
		}
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("CAST(t._data AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(c.nome)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(t.seqInicio AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(t.folhas AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(t.seqFim AS VARCHAR))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN t.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN t.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END)) ILIKE ?3 ");
			sqlBuilder.append(")");
		}
		
		Query countQuery = talaoChequeRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS TalaoCheque");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		if(status != null) {
			countQuery.setParameter(2, status.toString());
		}
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count.intValue()) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
				
		Query query = talaoChequeRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		if(status != null) {
			query.setParameter(2, status.toString());
		}
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize);
		
		try {
			result.include("talaoChequeList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/talaoCheque/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(Long contaId, Long id) {
		
		if(contaId == null && id == null) {
			
			// Verifica se o banco de dados possui espaco
			if(planoRecurso.getBancoDadosDisp() == 0) {
				validator.add(new I18nMessage("plano.bancoDados", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
				validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
			}
			
			Conta conta = contaRepository.findUniqueContaCorrente();
			if(conta == null) {
				result.forwardTo(ContaController.class).dialogSelect(contaId, true, false, false);
			} else {
				result.forwardTo(ContaController.class).talaoCheque(null, null, conta.getId(), id);
			}
		
		} else {
			if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
				validator.add(new I18nMessage("plano.bancoDados", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
				validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
			} else {
				Conta conta = talaoChequeRepository.getConta(id);
				if(conta != null) {
					contaId = conta.getId();
				}
			}
			result.forwardTo(ContaController.class).talaoCheque(null, null, contaId, id);
		}
		
	}
	
	
	@Get
	public void dialogSelect(Cheque cheque, boolean isReadOnly, LancamentoParcela lancamentoParcela, BaixaItem baixaItem) {
		if(isReadOnly) {
			cheque = chequeRepository.find(cheque.getId());
		}
		
		if(lancamentoParcela != null && lancamentoParcela.getId() != null) {
			lancamentoParcela = lancamentoParcelaRepository.find(lancamentoParcela.getId());
		} else {
			lancamentoParcela = null;
		}
		
		if(baixaItem != null && baixaItem.getId() != null) {
			baixaItem = baixaItemRepository.find(baixaItem.getId());
		} else {
			baixaItem = null;
		}
		
		result
			.include("talaoChequeList", talaoChequeRepository.findAllActive())
			.include("cheque", cheque)
			.include("isReadOnly", isReadOnly)
			.include("lancamentoParcela", lancamentoParcela)
			.include("baixaItem", baixaItem);
	}
	
	
	@Post
	public void dialogSelect(@NotNull(message="{cheque.notNull}") Cheque cheque, @NotNull(message="{talaoCheque.notNull}") TalaoCheque talaoCheque) {
		
		BaixaItem baixaItem = null;
		if(cheque != null) {
			baixaItem = baixaItemRepository.findByChequeCom(cheque);
			if(baixaItem != null && baixaItem.getStatus().equals(LancamentoStatus.COM)) {
				String obs = cheque.getObs();
				cheque = baixaItem.getCheque();
				cheque.setObs(obs);
				
				validator.validateProperties(cheque, "obs");
				
				validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
				
			}
		}
		
		if(baixaItem == null && cheque != null && talaoCheque != null && cheque.getNumero() != null) {
			
			talaoCheque = talaoChequeRepository.find(talaoCheque.getId());
			
			if(talaoCheque == null) {
				validator.add(new I18nMessage("cheque", "talaoCheque.notNull"));
			}
			
			if(cheque.getId() != null && !chequeRepository.exists(cheque.getId())) {
				validator.add(new I18nMessage("cheque", "cheque.invalid"));
			}
			
			validator.validateProperties(cheque, "numero", "data", "obs");
			
			validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
			cheque.setTalaoCheque(talaoCheque);
			
			Cheque salvo = chequeRepository.findEqual(cheque);
			if(salvo != null) {
				cheque.setId(salvo.getId());
			}
		}
		
		
		result.use(Results.jsonp()).withCallback("dialogSelectTalaoChequeSuccess").withoutRoot().from(cheque).excludeAll().include("id", "talaoCheque", "talaoCheque.conta", "numero", "data", "obs").serialize();
	}
	
	
	public void carregaSelect(Long id, Integer numero) {
		result
			.include("talaoChequeList", talaoChequeRepository.findAllActive())
			.include("talaoId", id)
			.include("numero", numero);
	}
	
	
}
