package br.com.contames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.Item;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.ClassificacaoRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.repository.ItemRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class ItemController {
		
	private final ClassificacaoRepository classificacaoRepository;
	private final DescricaoRepository descricaoRepository;
	private final ItemRepository itemRepository;
	private final PessoaRepository pessoaRepository;
	private final ContaRepository contaRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final UsuarioLogged usuarioLogged;
	private final Settings settings;
	private final Localization localization;
	private final Validator validator;
	private final Result result;
	private final HttpServletRequest request;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	private final ArquivoRepository arquivoRepository;
	
	public ItemController(ClassificacaoRepository classificacaoRepository, DescricaoRepository descricaoRepository, ItemRepository itemRepository, PessoaRepository pessoaRepository, ContaRepository contaRepository, CartaoCreditoRepository cartaoCreditoRepository, UsuarioLogged usuarioLogged, Settings settings, Localization localization, Validator validator, Result result, HttpServletRequest request, PlanoRecurso planoRecurso, ArquivoSession arquivoSession, ArquivoRepository arquivoRepository) {
		this.classificacaoRepository = classificacaoRepository;
		this.descricaoRepository = descricaoRepository;
		this.itemRepository = itemRepository;
		this.pessoaRepository = pessoaRepository;
		this.contaRepository = contaRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.usuarioLogged = usuarioLogged;
		this.settings = settings;
		this.localization = localization;
		this.validator = validator;
		this.result = result;
		this.request = request;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
		this.arquivoRepository = arquivoRepository;
	}
	

	@Get({"/cadastros/despesas", "/cadastros/receitas", "/cadastros/produtos"})
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page, Status status, List<Long> idsNotIn, Long favorecido_id, Long conta_id) { 
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table"; 
		
		ItemTipo tipo = null;
		if(request.getRequestURI().startsWith("/cadastros/despesas")) {
			tipo = ItemTipo.DES;
		} else if(request.getRequestURI().startsWith("/cadastros/receitas")) {
			tipo = ItemTipo.REC;
		} else {
			tipo = ItemTipo.PRD;
		}
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("classificacao_nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
			
			orderBy = new OrderBy();
			orderBy.setColumn("nome");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(1);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("i.id, ");
		sqlBuilder.append("COALESCE(Categ.nome || ' - ', '') || COALESCE(Fami.nome || ' - ', '') || COALESCE(Tipo.nome || ' - ', '') || COALESCE(Det.nome, '') AS classificacao_nome, ");
		sqlBuilder.append("i.nome, ");
		sqlBuilder.append("CASE WHEN i.status = 'ATI' THEN '" + localization.getMessage("Status.ATI") + "' WHEN i.status = 'INA' THEN '" + localization.getMessage("Status.INA") + "' END status, ");
		sqlBuilder.append("i.status _status, ");
		sqlBuilder.append("i.descricao, ");
		sqlBuilder.append("Unidade.nome AS unidade_nome, ");
		sqlBuilder.append("i.valor, ");
		sqlBuilder.append("i.estoque, ");
		sqlBuilder.append("i.tipo, ");
		sqlBuilder.append("CASE WHEN i.tipo = 'REC' THEN '" + localization.getMessage("ItemTipo.REC") + "' ELSE '" + localization.getMessage("ItemTipo.DES") + "' END AS tipo_nome, ");
		sqlBuilder.append("CASE WHEN i.isLancAuto IS NOT NULL AND i.isLancAuto THEN '" + localization.getMessage("sim") + "' ELSE '" + localization.getMessage("nao") + "' END isLancAuto, ");
		sqlBuilder.append("i.diaVencimento, ");
		sqlBuilder.append("CASE WHEN i.favorecido_id = " + favorecido_id +  " OR (SELECT EXISTS(SELECT li.id FROM LancamentoItem li, Lancamento l WHERE li.lancamento_id = l.id AND l.favorecido_id "  + (favorecido_id == null ? " IS NULL" : " = " + favorecido_id) + " AND li.item_id = i.id)) OR (SELECT EXISTS(SELECT ci.id FROM CompensacaoItem ci, Compensacao c WHERE ci.compensacao_id = c.id AND c.conta_id " + (conta_id == null ? " IS NULL " : " = " + conta_id) + " and ci.item_id = i.id)) THEN 0 ELSE 1 END AS fav_order ");
		sqlBuilder.append("FROM Item i ");
		sqlBuilder.append("LEFT JOIN Classificacao AS Det ON i.classificacao_id = Det.id ");
		sqlBuilder.append("LEFT JOIN Classificacao AS Tipo ON Det.classificacao_id = Tipo.id ");
		sqlBuilder.append("LEFT JOIN Classificacao AS Fami ON Tipo.classificacao_id = Fami.id ");
		sqlBuilder.append("LEFT JOIN Classificacao AS Categ ON Fami.classificacao_id = Categ.id ");
		sqlBuilder.append("LEFT JOIN Descricao AS Unidade ON i.unidade_id = Unidade.id ");
		sqlBuilder.append("WHERE i.cliente_id = ?1 ");
		
		if(tipo != null) {
			sqlBuilder.append("AND i.tipo = ?2 ");
		}
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("unaccent(LOWER(COALESCE(Categ.nome || ' - ', '') || COALESCE(Fami.nome || ' - ', '') || COALESCE(Tipo.nome || ' - ', '') || COALESCE(Det.nome, ''))) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(i.nome)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN i.status = 'ATI' THEN unaccent(LOWER('" + localization.getMessage("Status.ATI") + "')) WHEN i.status = 'INA' THEN unaccent(LOWER('" + localization.getMessage("Status.INA") + "')) END)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(i.descricao)) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(Unidade.nome)) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(i.valor AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE WHEN i.tipo = 'REC' THEN '" + localization.getMessage("ItemTipo.REC") + "' ELSE '" + localization.getMessage("ItemTipo.DES") + "' END)) ILIKE ?3 ");
			sqlBuilder.append("OR CASE WHEN i.isLancAuto IS NOT NULL AND i.isLancAuto THEN unaccent(LOWER('" + localization.getMessage("sim") + "')) ELSE unaccent(LOWER('" + localization.getMessage("nao") + "')) END ILIKE ?3 ");
			sqlBuilder.append("OR CAST(i.diaVencimento AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append(")");
		}
		
		if(status != null) {
			sqlBuilder.append(" AND i.status = ?4");
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			for(int i = 0; i < idsNotIn.size(); i++) {
				sqlBuilder.append(" AND i.id <> ?" + (i + 5));
			}
		}
				
		Query countQuery = itemRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Item");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(tipo != null) {
			countQuery.setParameter(2, tipo.toString());
		}
		
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(3, "%" + StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		if(status != null) {
			countQuery.setParameter(4, status.toString());
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			for(int i = 0; i < idsNotIn.size(); i++) {
				countQuery.setParameter((i + 5), idsNotIn.get(i));
			}
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		if(firstResult > count.intValue()) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY fav_order, ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
				
		Query query = itemRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(tipo != null) {
			query.setParameter(2, tipo.toString());
		}
		
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(3, "%" + StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		if(status != null) {
			query.setParameter(4, status.toString());
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			for(int i = 0; i < idsNotIn.size(); i++) {
				query.setParameter((i + 5), idsNotIn.get(i));
			}
		}
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
		
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("status", status)
			.include("idsNotIn", idsNotIn)
			.include("paginationSize", paginationSize)
			.include("tipo", tipo)
			.include("favorecido_id", favorecido_id);
		
		result.include("itemList", query.getResultList());
		
		result.forwardTo("/WEB-INF/jsp/item/" + jsp + ".jsp");
		
	}
	
	@Get({"/despesa/cadastro", "/receita/cadastro", "/item/cadastro", "/produto/cadastro"})
	public void cadastro(Long id, String val) {
		
		ItemTipo tipo = ItemTipo.DES;
		if(request.getRequestURI().startsWith("/receita/cadastro")) {
			tipo = ItemTipo.REC;
		} else if(request.getRequestURI().startsWith("/produto/cadastro")) {
			tipo = ItemTipo.PRD;
		}
		
		String viewid = StringHelper.createViewId();
		
		if(id == null) {
			
			// Verifica espaco no banco de dados
			if(planoRecurso.getBancoDadosDisp() == 0) {
				validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
			}
			
			Item item = new Item();
			item.setTipo(tipo);
			item.setNome(val);
			result .include("item", item);
		} else {
			Item item = itemRepository.find(id);
			arquivoSession.setAquivoList(viewid, item.getArquivoList());
			result .include("item", itemRepository.find(id));
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		result
			.include("viewid", viewid)
			.include("unidadeList", descricaoRepository.findAllActive(DescricaoTipo.UNIDADE));
		
	}
	
	@Post({"/despesa/cadastro", "/receita/cadastro", "/produto/cadastro"})
	public void cadastro(@NotNull(message = "{item.notNull}") Item item, Boolean isConfirmValorLTValorCompra, String viewid) {
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		ItemTipo tipo = ItemTipo.DES;
		if(request.getRequestURI().startsWith("/receita/cadastro")) {
			tipo = ItemTipo.REC;
		} else if(request.getRequestURI().startsWith("/produto/cadastro")) {
			tipo = ItemTipo.PRD;
		}
		
		item.setTipo(tipo);
		validator.validateProperties(item, "classificacao", "nome", "status", "descricao", "unidade", "valor", "diaVencimento");
		
		// Verifica se a classificacao foi informada
		if(item.getClassificacao() != null && item.getClassificacao().getId() == null) {
			item.setClassificacao(null);
		}
		
		// Verifica se a classificacao e do cliente
		if(item.getClassificacao() != null) {
			item.setClassificacao(classificacaoRepository.find(item.getClassificacao().getId()));
		}
		
		// Verifica se a unidade foi informada
		if(item.getUnidade() != null) {
			item.setUnidade(null);
		}
		
		// Verifica se a unidade e do cliente
		if(item.getUnidade() != null) {
			item.setUnidade(descricaoRepository.find(item.getUnidade().getId(), DescricaoTipo.UNIDADE));
		}
		
		
		// Verifica se existe outro item igual
		if(itemRepository.exists(item)) {
			validator.add(new I18nMessage("nome", "item." + item.getTipo() + ".name.exists"));
		}
		
		// Verifica se valor eh inferior a zero
		if(item.getValor() != null && item.getValor() < 0.00) {
			validator.add(new I18nMessage("valor", "item.valor.notLT0.00"));
		}
		
		// Verifica se o favorecido foi informado
		if(item.getFavorecido() != null && item.getFavorecido().getId() == null) {
			item.setFavorecido(null);
		}
		
		if(item.getFavorecido() != null) {
			item.setFavorecido(pessoaRepository.findOrCliente(item.getFavorecido().getId()));
		}
		
		if(item.getMeio() == null) {
			item.setConta(null);
			item.setCartaoCredito(null);
		} else {
			switch(item.getMeio()) {
			case CCR:
			case CHQ:
				item.setConta(null);
				break;
			default:
				item.setCartaoCredito(null);
			}
		}
		
		// Verifica se a Conta foi informada
		if(item.getConta() != null && item.getConta().getId() == null) {
			item.setConta(null);
		}
		
		if(item.getConta() != null) {
			item.setConta(contaRepository.find(item.getConta().getId()));
		}
		
		// Verifica Cartao de Credito
		if(item.getCartaoCredito() != null && item.getCartaoCredito().getId() == null) {
			item.setCartaoCredito(null);
			item.setIsLimite(null);
		}
		
		if(item.getCartaoCredito() != null) {
			item.setCartaoCredito(cartaoCreditoRepository.find(item.getCartaoCredito().getId()));
		}
		
		// Verifica se eh Lanc. Auto. e se o Valor, Favorecido, Venc dia e Meio foram informados
		if(item.getIsLancAuto() != null && item.getIsLancAuto()) {
			
			// Verifica Valor
			if(item.getValor() == null) {
				validator.add(new I18nMessage("valor", "valor.notNull"));
			}
			
			// Verifica Favorecido
			if(item.getFavorecido() == null) {
				validator.add(new I18nMessage("favorecido", "favorecido." + tipo + ".notNull"));
			}
			
			// Verifica Venc. dia
			if(item.getDiaVencimento() == null) {
				validator.add(new I18nMessage("diaVencimento", "diaVencimento.notNull"));
			}
			
			// Verifica Meio
			if(item.getMeio() == null) {
				validator.add(new I18nMessage("meio", "meio.notNull"));
			}
			
		}
		
		// Verifica se he produto e verifica se o estoque foi informado
		if(ItemTipo.PRD.equals(tipo)) {
			
			if(item.getValorCompra() != null) {
				if(item.getValorCompra() < 0.00) {
					validator.add(new I18nMessage("valorCompra", "valorCompra.notLT0.00"));
				}
			}
			
			if(isConfirmValorLTValorCompra != null && !isConfirmValorLTValorCompra) {
				if(item.getValorCompra() != null && item.getValor() != null && item.getValorCompra() > item.getValor()) {
					validator.add(new I18nMessage("isConfirmValorLTValorCompra", "confirm.item.valorCompra.GT.valor"));
				}
			}
			
			if(item.getEstoque() == null) {
				validator.add(new I18nMessage("estoque", "estoque.notNull"));
			} else if(item.getEstoque() < 0.0001) {
				validator.add(new I18nMessage("estoque", "estoque.notLT0.0001"));
			}
			
		}
		
		
		
		validator.onErrorUse(Results.jsonp()).withCallback("itemCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		// Salva Item
		itemRepository.save(item);
		
		// Recupera List de Arquivo
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		for(Arquivo arquivo : arquivoList) {
			arquivo.setItem(item);
			arquivoRepository.save(arquivo);
		}
		
		// Recupera List de Arquivo para excluir
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		for(Arquivo arquivo : arquivoDeleteList) {
			arquivoRepository.delete(arquivo);
		}
		
		// Limpa ArquivoSession
		arquivoSession.clear(viewid);
		
		result.use(Results.jsonp()).withCallback("itemCadastroSuccess").withoutRoot().from(item).exclude("cliente").serialize();
	}
	
	
	@Post({"/despesa/excluir", "/receita/excluir", "/produto/excluir"})
	public void excluir(List<Long> ids, ItemTipo tipo) {
		
		// Verifica se existem algum lancamento com este item
		for(int i = 0; i < ids.size(); i++) {
			if(itemRepository.existLancamentoItem(ids.get(i))) {
				validator.add(new I18nMessage("item_" + ids.get(i), "item." + tipo + ".lancamentoItem.exists"));
			} else if(itemRepository.existCompensacaoItem(ids.get(i))) {
				validator.add(new I18nMessage("item_" + ids.get(i), "item." + tipo + ".compensacaoItem.exists"));
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		List<Item> itemList = itemRepository.find(ids);
		if(itemList!= null) {
			for(Item item : itemList) {
				List<Arquivo> arquivoList = item.getArquivoList();
				for(Arquivo arquivo : arquivoList) {
					arquivoRepository.delete(arquivo);
				}
			}
		}
		
		itemRepository.deleteList(ids);
				
		result.use(Results.jsonp()).withCallback("itemExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
	@Get
	public void autoComplete(ItemTipo tipo, String query) {
		result
			.include("query", query)
			.include("itemList", itemRepository.findAllActive(tipo, query));
	}
	
	@Get
	public void reload(Long id) {
		Item item = itemRepository.find(id);
		if(item == null) {
			result.use(Results.json()).withoutRoot().from(StringUtils.EMPTY).serialize();
		} else {
			Map<String, Object> mapItem = new HashMap<String, Object>();
			mapItem.put("id", item.getId());
			mapItem.put("nome", item.getDisplayName());
			result.use(Results.json()).withoutRoot().from(mapItem).serialize();			
		}
	}
	
	@Get
	public void multipleAutoComplete(ItemTipo tipo, String q) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT c.id, ");
		sqlBuilder.append("c.tipo, ");
		sqlBuilder.append("c.nome, ");
		sqlBuilder.append("c.cliente_id, ");
		sqlBuilder.append("c.index_cat, ");
		sqlBuilder.append("c.index_fam, ");
		sqlBuilder.append("c.index_tip, ");
		sqlBuilder.append("c.index_det, ");
		sqlBuilder.append("true isClassif ");
		sqlBuilder.append("FROM ( ");
		sqlBuilder.append("SELECT c.id, ");
		sqlBuilder.append("c.tipo, ");
		sqlBuilder.append("c.nome, ");
		sqlBuilder.append("c.cliente_id, ");
		sqlBuilder.append("c._index index_cat, ");
		sqlBuilder.append("NULL::integer index_fam, ");
		sqlBuilder.append("NULL::integer index_tip, ");
		sqlBuilder.append("NULL::integer index_det ");
		sqlBuilder.append("FROM Classificacao c ");
		sqlBuilder.append("WHERE c.nivel = 'CAT' ");
		sqlBuilder.append("UNION SELECT c.id, ");
		sqlBuilder.append("c.tipo, ");
		sqlBuilder.append("cat.nome || '-' || c.nome nome, ");
		sqlBuilder.append("c.cliente_id, ");
		sqlBuilder.append("cat._index index_cat, ");
		sqlBuilder.append("c._index index_fam, ");
		sqlBuilder.append("NULL::integer index_tip, ");
		sqlBuilder.append("NULL::integer index_det ");
		sqlBuilder.append("FROM Classificacao c ");
		sqlBuilder.append("INNER JOIN Classificacao cat ON c.classificacao_id = cat.id ");
		sqlBuilder.append("WHERE c.nivel = 'FAM' ");
		sqlBuilder.append("UNION SELECT c.id, ");
		sqlBuilder.append("c.tipo, ");
		sqlBuilder.append("cat.nome || '-' || fam.nome || '-' || c.nome nome, ");
		sqlBuilder.append("c.cliente_id, ");
		sqlBuilder.append("cat._index index_cat, ");
		sqlBuilder.append("fam._index index_fam, ");
		sqlBuilder.append("c._index index_tip, ");
		sqlBuilder.append("NULL::integer index_det ");
		sqlBuilder.append("FROM Classificacao c ");
		sqlBuilder.append("INNER JOIN Classificacao fam ON c.classificacao_id = fam.id ");
		sqlBuilder.append("INNER JOIN Classificacao cat ON fam.classificacao_id = cat.id ");
		sqlBuilder.append("WHERE c.nivel = 'TIP' ");
		sqlBuilder.append("UNION SELECT c.id, ");
		sqlBuilder.append("c.tipo, ");
		sqlBuilder.append("cat.nome || '-' || fam.nome || '-' || tip.nome || '-' || c.nome nome, ");
		sqlBuilder.append("c.cliente_id, ");
		sqlBuilder.append("cat._index index_cat, ");
		sqlBuilder.append("fam._index index_fam, ");
		sqlBuilder.append("tip._index index_tip, ");
		sqlBuilder.append("c._index index_det ");
		sqlBuilder.append("FROM Classificacao c ");
		sqlBuilder.append("INNER JOIN Classificacao tip ON c.classificacao_id = tip.id ");
		sqlBuilder.append("INNER JOIN Classificacao fam ON tip.classificacao_id = fam.id ");
		sqlBuilder.append("INNER JOIN Classificacao cat ON fam.classificacao_id = cat.id ");
		sqlBuilder.append("WHERE c.nivel = 'DET' ");
		sqlBuilder.append(") c ");
		sqlBuilder.append("WHERE c.cliente_id = ?1 ");
		sqlBuilder.append("AND c.tipo = ?2 ");
		sqlBuilder.append("AND unaccent(LOWER(c.nome)) ILIKE ?3 ");
		sqlBuilder.append("ORDER BY c.index_cat, c.index_fam, c.index_tip, c.index_det ");
		Query query = itemRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		if(tipo != null) {
			query.setParameter(2, tipo.toString());
		}
		query.setParameter(3, StringHelper.unAccent(q.toLowerCase()) + "%");
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		query.setFirstResult(0);
		query.setMaxResults((int) settings.getConfiguration().getFloat("page.size"));
		List<Object> arrResult = new ArrayList<Object>();
		try {
			arrResult.addAll(query.getResultList());
		} catch(NoResultException e) { 
		}
		
		List<Object> arrItems = new ArrayList<Object>();
		arrItems  = itemRepository.findAllActive(tipo, q);
		if(arrItems != null) {
			arrResult.addAll(arrItems);
		}
				
		result.include("itemList", arrResult);
	}
	
	
	public void dialogMidia(String viewid)
	{
		
	}
		
}
