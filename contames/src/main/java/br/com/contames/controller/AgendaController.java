package br.com.contames.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.entity.Agenda;
import br.com.contames.repository.AgendaRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;

@Private
@Resource
public class AgendaController {

	private final AgendaRepository agendaRepository;
	private final PessoaRepository pessoaRepository;
	private final UsuarioLogged usuarioLogged;
	private final Validator validator;
	private final Result result;
	
	public AgendaController(AgendaRepository agendaRepository, PessoaRepository pessoaRepository, UsuarioLogged usuarioLogged, Validator validator, Result result) {
		this.agendaRepository = agendaRepository;
		this.pessoaRepository = pessoaRepository;
		this.usuarioLogged = usuarioLogged;
		this.validator = validator;
		this.result = result;
	}
	
	@Get
	public void cadastro(Long id) {
		if(id != null) {
			result.include("agenda", agendaRepository.find(id));
		}
		result.include("usuarioList", pessoaRepository.findAllUsuarioActive());
	}
	
	@Post
	public void cadastro(Agenda agenda) {
		if(agenda == null) {
			validator.add(new I18nMessage("agenda", "agenda.notNull"));
		} else {
			
			validator.validateProperties(agenda, new String[]{"nome", "tipo", "inicio", "fim", "obs"});
			
			// Verifica se a Pessoa selecionada existe
			if(agenda.getPessoa() != null && agenda.getPessoa().getId() != null) {
				agenda.setPessoa(pessoaRepository.findOrCliente(agenda.getPessoa().getId()));
			} else {
				agenda.setPessoa(null);
			}
			
			// Verifica se periodo nao eh null
			if(agenda.getInicio() != null && agenda.getFim() != null) {
				
				// Verifica se inicio e menor que fim
				if(agenda.getInicio().isAfter(agenda.getFim())) {
					validator.add(new I18nMessage("agenda.inicio", "agenda.inicio.notAfter.fim"));
				}
				
			}
			
			// Verifica se o usuario foi informado
			if(agenda.getUsuario() != null && agenda.getUsuario().getId() != null) {
				agenda.setUsuario(pessoaRepository.findOrCliente(agenda.getUsuario().getId()));
			} else {
				agenda.setUsuario(null);
			}
			
			agenda.setCliente(usuarioLogged.getCliente());
			if(agenda.getId() == null) {
				agenda.setCriacao(LocalDateTime.now());
				agenda.setUsuCriacao(usuarioLogged.getUsuario());
			} else {
				agenda.setAlteracao(LocalDateTime.now());
				agenda.setUsuAlteracao(usuarioLogged.getUsuario());
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("agendaCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		try {
			agendaRepository.save(agenda);
		} catch( javax.validation.ConstraintViolationException e ) {
			e.printStackTrace();
		}
				
		result.use(Results.jsonp()).withCallback("agendaCadastroSuccess").withoutRoot().from(agenda).exclude("cliente").serialize();
		
	}
	
	@Get
	public void list(List<Long> usuarios, Long from, Long to) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("a.id, ");
		sqlBuilder.append("NULL::BIGINT lancamentoparcela_id, ");
		sqlBuilder.append("NULL::BIGINT lancamento_id, ");
		sqlBuilder.append("NULL::BIGINT baixaitem_id, ");
		sqlBuilder.append("NULL::BIGINT baixa_id, ");
		sqlBuilder.append("NULL::BIGINT transferencia_id, ");
		sqlBuilder.append("NULL::BIGINT compensacaoitem_id, ");
		sqlBuilder.append("NULL::BIGINT compensacao_id, ");
		sqlBuilder.append("NULL::BIGINT favorecido_id, ");
		sqlBuilder.append("NULL::BIGINT conta_id, ");
		sqlBuilder.append("NULL::BIGINT cartaoCredito_id, ");
		sqlBuilder.append("NULL::BIGINT talaoCheque_id, ");
		sqlBuilder.append("a.nome, ");
		sqlBuilder.append("NULL::DOUBLE PRECISION valor, ");
		sqlBuilder.append("a.tipo, ");
		sqlBuilder.append("NULL::VARCHAR _tipo, ");
		sqlBuilder.append("a.inicio, ");
		sqlBuilder.append("a.fim, ");
		sqlBuilder.append("a.obs ");
		sqlBuilder.append("FROM Agenda a ");
		sqlBuilder.append("WHERE a.cliente_id = ?1 ");
		if(usuarios != null && !usuarios.isEmpty()) {
			sqlBuilder.append("AND (a.usuario_id IN(" + StringUtils.join(usuarios.toArray(), ", ") + ") OR a.usuario_id IS NULL) ");
		}
		sqlBuilder.append("AND a.inicio >= ?2 ");
		sqlBuilder.append("AND a.fim <= ?3 ");
		sqlBuilder.append("UNION SELECT ");
		sqlBuilder.append("NULL::BIGINT id, ");
		sqlBuilder.append("r.lancamentoparcela_id, ");
		sqlBuilder.append("r.lancamento_id, ");
		sqlBuilder.append("r.baixaitem_id, ");
		sqlBuilder.append("r.baixa_id, ");
		sqlBuilder.append("r.transferencia_id, ");
		sqlBuilder.append("r.compensacaoitem_id, ");
		sqlBuilder.append("r.compensacao_id, ");
		sqlBuilder.append("r.favorecido_id, ");
		sqlBuilder.append("r.conta_id, ");
		sqlBuilder.append("r.cartaoCredito_id, ");
		sqlBuilder.append("r.talaoCheque_id, ");
		sqlBuilder.append("r.descricao nome, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN -r.valor WHEN r.tipo = 'REC' AND r.valor < 0 THEN r.valor WHEN r.tipo = 'REC' AND r.valor > 0 THEN r.valor WHEN r.tipo = 'DES' AND r.valor < 0 THEN -r.valor END valor, ");
		sqlBuilder.append("CASE WHEN r.tipo = 'DES' AND r.valor > 0 THEN 'IMPORTANT'::VARCHAR WHEN r.tipo = 'REC' AND r.valor < 0 THEN 'INFO'::VARCHAR WHEN r.tipo = 'REC' AND r.valor > 0 THEN 'INFO'::VARCHAR WHEN r.tipo = 'DES' AND r.valor < 0 THEN 'IMPORTANT'::VARCHAR END tipo, ");
		sqlBuilder.append("r.tipo _tipo, ");
		sqlBuilder.append("r._data::TIMESTAMP WITHOUT TIME ZONE inicio, ");
		sqlBuilder.append("r._data::TIMESTAMP WITHOUT TIME ZONE fim, ");
		sqlBuilder.append("NULL::VARCHAR obs ");
		sqlBuilder.append("FROM vw_relatorio_data r ");
		sqlBuilder.append("WHERE r.cliente_id = ?1 ");
		sqlBuilder.append("AND r._data BETWEEN ?2 AND ?3 ");
		
		Timestamp inicio = new Timestamp(from);
		Timestamp fim = new Timestamp(to);
		
		Query query = agendaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, inicio);
		query.setParameter(3, fim);
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		try {
			result.include("agendaList", query.getResultList());
		}catch(NoResultException e) {
			
		}
	}
	
	
	@Post
	public void excluir(Long id) {
		Query query = agendaRepository.getEntityManager().createQuery("DELETE FROM Agenda a WHERE a.cliente = ?1 AND a.id = ?2");
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, id);
		try {
			query.executeUpdate();
			result.use(Results.http()).setStatusCode(200);
		} catch(Exception e) {
			result.use(Results.http()).setStatusCode(500);
		}
	}
	
}
