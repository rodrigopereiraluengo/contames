package br.com.contames.controller;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import javax.persistence.NoResultException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.util.jpa.extra.Load;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.ForceSSL;
import br.com.contames.component.EmailHelper;
import br.com.contames.entity.EsqueciSenha;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.AssinaturaRepository;
import br.com.contames.repository.EsqueciSenhaRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;

@ForceSSL
@Resource
public class LoginController {

	private final UsuarioLogged usuarioLogged;
	private final PessoaRepository pessoaRepository;
	private final EsqueciSenhaRepository esqueciSenhaRepository;
	private final AssinaturaRepository assinaturaRepository;
	private final EmailHelper emailHelper;
	private final Result result;
	private final Validator validator;
	private final Environment environment;
	private final Localization localization;
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	
	public LoginController(UsuarioLogged usuarioLogged, PessoaRepository pessoaRepository, EsqueciSenhaRepository esqueciSenhaRepository, AssinaturaRepository assinaturaRepository, EmailHelper emailHelper, Result result, Validator validator, Environment environment, Localization localization, HttpServletRequest request, HttpServletResponse response) {
		this.usuarioLogged = usuarioLogged;
		this.pessoaRepository = pessoaRepository;
		this.esqueciSenhaRepository = esqueciSenhaRepository;
		this.assinaturaRepository = assinaturaRepository;
		this.emailHelper = emailHelper;
		this.result = result;
		this.validator = validator;
		this.environment = environment;
		this.localization = localization;
		this.request = request;
		this.response = response;
	}
		
	@Get("/entrar")
	public void login(String url) {
		if(usuarioLogged.getIsLogged()) {
			result.redirectTo(url == null ? "/resumo" : url);
		} else {
			setCookieLembrar();
		}
		result.include("url", url);
	}

	private void setCookieLembrar() {
		Cookie cookieLembrar = null;
		try {
			if(request.getCookies() != null) {
				for(Cookie cookie : request.getCookies()) {
					if("lembrar".equals(cookie.getName())) {
						cookieLembrar = cookie;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(cookieLembrar != null) {
			String email = cookieLembrar.getValue();
			result.include("email", email).include("lembrar", true);
		}
	}
	
	@Post("/entrar")
	public void login(
			@NotBlank(message="{email.notBlank}")
			@Email(message="{email.email}")
			String email,
			
			@NotBlank(message="{senha.notBlank}")
			String senha,
			Boolean lembrar,
			String url
	) {
		
		if(!validator.hasErrors()) {
			
			try {
				Pessoa pessoa = pessoaRepository.find(email, senha);
				usuarioLogged.setUsuario(pessoa);
				
				if(usuarioLogged.getCliente().getAssinatura() == null) {
					usuarioLogged.getCliente().setAssinatura(assinaturaRepository.findBy(usuarioLogged.getCliente()));
				}
				
				//
				Cookie cookieLembrar = null;
				try {
					if(request.getCookies() != null) {
						for(Cookie cookie : request.getCookies()) {
							if("lembrar".equals(cookie.getName())) {
								cookieLembrar = cookie;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}		
				
				if(lembrar != null && lembrar) {
					if(cookieLembrar == null) {
						try {
							cookieLembrar = new Cookie("lembrar", new String(email.getBytes("cp1251"), "utf8"));
							cookieLembrar.setMaxAge(60 * 60 * 24 * 7);
							response.addCookie(cookieLembrar);
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						try {
							cookieLembrar.setValue(new String(email.getBytes("cp1251"), "utf8"));
							response.addCookie(cookieLembrar);
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else {
					if(cookieLembrar != null) {
						cookieLembrar.setMaxAge(0);
						response.addCookie(cookieLembrar);
					}
				}
				
			} catch(NoResultException e) {
				validator.add(new I18nMessage("login", "login.invalido"));
			}
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		result.use(Results.jsonp()).withCallback("success").from(url == null ? StringUtils.EMPTY : url).serialize();
		
	}
	
	@Get("/esqueci-minha-senha")
	public void esqueciSenha() { 
		
		setCookieLembrar();
		
	}
	
	@Post("/esqueci-minha-senha")
	public void esqueciSenha(@NotBlank(message="{email.notBlank}") @Email(message="{email.invalid}") String email) {
		
		Pessoa usuario = null;
		
		// Verifica se nao possui erros
		if(!validator.hasErrors()) {
			
			// Verifica se o usuario existe
			// Adiciona mensagem de erro
			usuario = pessoaRepository.findByEmail(email);
			if(usuario == null) {
				validator.add(new I18nMessage("email", "email.notFound"));
			}
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		
		// Atualiza EsqueSenha ativo para inativo se existe
		esqueciSenhaRepository.inativar(usuario);
		
		String hash = Base64.encodeBase64URLSafeString((LocalDateTime.now().toString("yyyy-MM-dd") + "|" + usuario.getEmail() + "|" + LocalDateTime.now().toString("HHmmss") + (new Random().nextInt(1000))).getBytes());
		String hashlink = environment.get("login.atualizarSenha.link") + hash;
				
		EsqueciSenha esqueciSenha = new EsqueciSenha();
		esqueciSenha.setUsuario(usuario);
		esqueciSenha.setCriacao(LocalDateTime.now());
		esqueciSenha.setStatus(Status.STD);
		esqueciSenha.setId(hash);
		
		// Salva EsqueciSenha
		esqueciSenhaRepository.save(esqueciSenha);

		// Envia e-mail
		emailHelper
			.addTo(usuario.getEmail(), usuario.getDisplayNome())
			.setFrom(localization.getMessage("email.support.from.email"), localization.getMessage("email.support.from.name"))
			.setSubject(localization.getMessage("login.esqueciSenha.email.subject"))
			.setHtmlMsg(localization.getMessage("login.esqueciSenha.email.htmlMsg", usuario.getDisplayNome(), hashlink))
			.setTextMsg(localization.getMessage("login.esqueciSenha.email.textMsg", usuario.getDisplayNome(), hashlink))
			.send();
				
		result.use(Results.jsonp()).withCallback("success").from(StringUtils.EMPTY).serialize();
		
	}
	
	@Get("/atualizar-senha/{esqueciSenha.id}")
	public EsqueciSenha atualizarSenha(@Load EsqueciSenha esqueciSenha) {
		
		// Verifica se a data e maior que 3 dias
		if(Days.daysBetween(esqueciSenha.getCriacao().toLocalDate(), LocalDate.now()).getDays() > 3) {
			esqueciSenha.setStatus(Status.INA);
			esqueciSenha.setAlteracao(LocalDateTime.now());
			esqueciSenhaRepository.update(esqueciSenha);
		}
		
		return esqueciSenha;
	}
	
	@Post("/atualizar-senha")
	public void atualizarSenha(String id, @NotBlank(message = "{senha.notBlank}") @Size(min = 4, max = 40, message = "{senha.size}") String senha, @NotBlank(message = "{confirmeSenha.notBlank}") String confirmeSenha) {
		
		EsqueciSenha esqueciSenha = esqueciSenhaRepository.find(id);
		
		// Verifica se o passwordForgot existe
		if(esqueciSenha == null) {
			validator.add(new I18nMessage("esqueciSenha", "esqueciSenha.notFound"));
		} else {
			
			// Verifica se o status e StandBy
			if(!esqueciSenha.getStatus().equals(Status.STD)) {
				validator.add(new I18nMessage("esqueciSenha", "esqueciSenha.expired"));
			}
			
			// Verifica se a data e maior que 3 dias
			if(Days.daysBetween(esqueciSenha.getCriacao().toLocalDate(), LocalDate.now()).getDays() > 3) {
				validator.add(new I18nMessage("esqueciSenha", "esqueciSenha.expired"));
				esqueciSenha.setStatus(Status.INA);
				esqueciSenha.setAlteracao(LocalDateTime.now());
				esqueciSenhaRepository.update(esqueciSenha);
			}
			
			// Verifica se nao possui erros
			if(!validator.hasErrors()) {
				
				// Verifica se a senha confere
				if(!senha.equals(confirmeSenha)) {
					validator.add(new I18nMessage("confirmeSenha", "confirmeSenha.notEqual"));
				}
				
			}
			
			validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
			
			esqueciSenha.getUsuario().setSenha(senha);
			esqueciSenha.getUsuario().setAlteracao(LocalDateTime.now());
			pessoaRepository.update(esqueciSenha.getUsuario());
			
			esqueciSenha.setStatus(Status.ATI);
			esqueciSenha.setAlteracao(LocalDateTime.now());
			esqueciSenhaRepository.update(esqueciSenha);
			
			result.use(Results.jsonp()).withCallback("success").from(StringUtils.EMPTY).serialize();
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
	}
		
}
