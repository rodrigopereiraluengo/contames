package br.com.contames.controller;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.not;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.Baixa;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Descricao;
import br.com.contames.entity.Item;
import br.com.contames.entity.Lancamento;
import br.com.contames.entity.LancamentoFatura;
import br.com.contames.entity.LancamentoItem;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.entity.LancamentoRateio;
import br.com.contames.entity.Pessoa;
import br.com.contames.entity.TalaoCheque;
import br.com.contames.entity.Transferencia;
import br.com.contames.enumeration.BoletoTipo;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.DocTipo;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.MeioPagamento;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.enumeration.PessoaTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.BaixaItemRepository;
import br.com.contames.repository.BaixaRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.ChequeRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.repository.ItemRepository;
import br.com.contames.repository.LancamentoFaturaRepository;
import br.com.contames.repository.LancamentoItemRepository;
import br.com.contames.repository.LancamentoParcelaRepository;
import br.com.contames.repository.LancamentoRateioRepository;
import br.com.contames.repository.LancamentoRepository;
import br.com.contames.repository.PessoaRepository;
//import static org.hamcrest.Matchers.
import br.com.contames.repository.TalaoChequeRepository;
import br.com.contames.repository.TransferenciaRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.NumericHelper;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class LancamentoController {
	
	private final PessoaRepository pessoaRepository;
	private final ItemRepository itemRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final ContaRepository contaRepository;
	private final TalaoChequeRepository talaoChequeRepository;
	private final DescricaoRepository descricaoRepository;
	private final ChequeRepository chequeRepository;
	private final LancamentoRepository lancamentoRepository;
	private final LancamentoItemRepository lancamentoItemRepository;
	private final LancamentoFaturaRepository lancamentoFaturaRepository;
	private final LancamentoParcelaRepository lancamentoParcelaRepository;
	private final LancamentoRateioRepository lancamentoRateioRepository;
	private final ArquivoRepository arquivoRepository;
	private final BaixaRepository baixaRepository;
	private final BaixaItemRepository baixaItemRepository;
	private final TransferenciaRepository transferenciaRepository;
	private final UsuarioLogged usuarioLogged;
	private final Environment environment;
	private final Settings settings;
	private final Localization localization;
	private final HttpServletRequest request;
	private final HttpSession session;
	private final Result result;
	private final Validator validator;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	
	public LancamentoController(PessoaRepository pessoaRepository, ItemRepository itemRepository, CartaoCreditoRepository cartaoCreditoRepository, ContaRepository contaRepository, TalaoChequeRepository talaoChequeRepository, DescricaoRepository descricaoRepository, ChequeRepository chequeRepository, LancamentoRepository lancamentoRepository, LancamentoItemRepository lancamentoItemRepository, LancamentoFaturaRepository lancamentoFaturaRepository, LancamentoParcelaRepository lancamentoParcelaRepository, LancamentoRateioRepository lancamentoRateioRepository, ArquivoRepository arquivoRepository, BaixaRepository baixaRepository, BaixaItemRepository baixaItemRepository, TransferenciaRepository transferenciaRepository, UsuarioLogged usuarioLogged, Environment environment, Settings settings, Localization localization, HttpServletRequest request, HttpSession session, Result result, Validator validator, PlanoRecurso planoRecurso, ArquivoSession arquivoSession) {
		this.pessoaRepository = pessoaRepository;
		this.itemRepository = itemRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.contaRepository = contaRepository;
		this.talaoChequeRepository = talaoChequeRepository;
		this.descricaoRepository = descricaoRepository;
		this.chequeRepository = chequeRepository;
		this.lancamentoRepository = lancamentoRepository;
		this.lancamentoItemRepository = lancamentoItemRepository;
		this.lancamentoFaturaRepository = lancamentoFaturaRepository;
		this.lancamentoParcelaRepository = lancamentoParcelaRepository;
		this.lancamentoRateioRepository = lancamentoRateioRepository;
		this.arquivoRepository = arquivoRepository;
		this.baixaRepository = baixaRepository;
		this.baixaItemRepository = baixaItemRepository;
		this.transferenciaRepository = transferenciaRepository;
		this.usuarioLogged = usuarioLogged;
		this.environment = environment;
		this.settings = settings;
		this.localization = localization;
		this.request = request;
		this.session = session;
		this.result = result;
		this.validator = validator;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
	}

	
	@Get({"/lancamentos/despesas", "/lancamentos/receitas", "/lancamentos/compras", "/lancamentos/vendas"})
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page) { 
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		ItemTipo tipo = ItemTipo.DES;
		if(request.getRequestURI().startsWith("/lancamentos/receitas")) {
			tipo = ItemTipo.REC;
		} else if(request.getRequestURI().startsWith("/lancamentos/compras")) {
			tipo = ItemTipo.CPR;
		} else if(request.getRequestURI().startsWith("/lancamentos/vendas")) {
			tipo = ItemTipo.VEN;
		}
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.DESC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
				
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT l.id, ");
		sqlBuilder.append("l._data, ");
		sqlBuilder.append("COALESCE(f.fantasia, f.razaoSocial, f.apelido, f.nome) favorecido, ");
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || ' - ' || l.numerodoc, CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, l.numerodoc) || COALESCE(' ' || cc.nome) documento, ");
		sqlBuilder.append("a.id arquivo_id, ");
		sqlBuilder.append("a.nome arquivo_nome, ");
		sqlBuilder.append("l.quantLancamentoParcela, ");
		sqlBuilder.append("lp._data primeiroVencimento, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN l.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN l.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
		sqlBuilder.append("WHEN l.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status, ");
		sqlBuilder.append("l.status AS _status, ");
		sqlBuilder.append("l.totalLancamentoParcela valor, ");
		sqlBuilder.append("l.obs ");
		sqlBuilder.append("FROM Lancamento l ");
		sqlBuilder.append("INNER JOIN Pessoa f ON l.favorecido_id = f.id ");
		sqlBuilder.append("INNER JOIN LancamentoParcela lp ON l.id = lp.lancamento_id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON l.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN Arquivo a ON l.arquivoDoc_id = a.id ");
		sqlBuilder.append("WHERE l.cliente_id = ?1 ");
		sqlBuilder.append("AND lp.seq = 0 ");
		sqlBuilder.append("AND l.tipo = ?2 ");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("CAST(l._data AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(f.fantasia, f.razaoSocial, f.apelido, f.nome))) ILIKE ?3 ");
			sqlBuilder.append("OR COALESCE(CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("END || ' - ' || l.numerodoc, CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("END, l.numerodoc) || COALESCE(' ' || cc.nome) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(l.quantLancamentoParcela AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE ");
			sqlBuilder.append("WHEN l.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
			sqlBuilder.append("WHEN l.status = 'LAN' THEN '" + localization.getMessage("LancamentoStatus.LAN") + "' ");
			sqlBuilder.append("WHEN l.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
			sqlBuilder.append("END)) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(l.totalLancamentoParcela AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append(")");
		}
				
		Query countQuery = lancamentoRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Lancamento");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, tipo.toString());
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
						
		Query query = lancamentoRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, tipo.toString());
		
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("tipo", tipo);
		
		try {
			result.include("lancamentoList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/lancamento/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(Long id, @NotNull(message = "{tipo.notNull}") ItemTipo tipo) {
		
		if(id == null && planoRecurso.getLancamentosDisp() == 0) {
			validator.add(new I18nMessage("plano.lancamentos", "plano.lancamentos.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getLancamentos()));
		}
		
		// Verifica espaco no banco de dados
		if(planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.bancoDados", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
		
		// Gera viewid
		String viewid = StringHelper.createViewId();
		
		Lancamento lancamento = null;
		List<LancamentoItem> lancamentoItemList = null;
		List<LancamentoFatura> lancamentoFaturaList = null;
		List<LancamentoParcela> lancamentoParcelaList = null;
		List<LancamentoRateio> lancamentoRateioList = null;
				
		boolean isClosed = false;
		
		if(id == null) {
			
			lancamento = new Lancamento();
			lancamento.setStatus(LancamentoStatus.LAN);
			lancamento.setTipo(tipo);
			
			session.setAttribute("lancamento_" + viewid, lancamento);
			
			lancamentoItemList = new ArrayList<LancamentoItem>();
			session.setAttribute("lancamentoItemList_" + viewid, lancamentoItemList);
			
			lancamentoParcelaList = new ArrayList<LancamentoParcela>();
			session.setAttribute("lancamentoParcelaList_" + viewid, lancamentoParcelaList);
			
			lancamentoRateioList = new ArrayList<LancamentoRateio>();
			session.setAttribute("lancamentoRateioList_" + viewid, lancamentoRateioList);
				
		} else {
			
			// Carregar Lancamento do Banco de Dados
			lancamento = lancamentoRepository.find(id);
			if(lancamento == null) {
				validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
			} else {
				
				session.setAttribute("lancamento_" + viewid, lancamento);
				
				lancamentoItemList = lancamentoItemRepository.findAll(id);
				session.setAttribute("lancamentoItemList_" + viewid, lancamentoItemList);
				
				if(DocTipo.FT.equals(lancamento.getTipoDoc())) {
					lancamentoFaturaList = lancamentoFaturaRepository.findAll(id);
					session.setAttribute("lancamentoFaturaList_" + viewid, lancamentoFaturaList);
				}
								
				lancamentoRateioList = lancamentoRateioRepository.findAll(id);
				session.setAttribute("lancamentoRateioList_" + viewid, lancamentoRateioList);
				
				lancamentoParcelaList = lancamentoParcelaRepository.findAll(id);
				session.setAttribute("lancamentoParcelaList_" + viewid, lancamentoParcelaList);
				
				isClosed = lancamentoRepository.isClosed(id);
				
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		arquivoSession.setAquivoList(viewid, lancamento.getArquivoList());
		
		result
			.include("viewid", viewid)
			.include("lancamento", lancamento)
			.include("lancamentoItemList", lancamentoItemList)
			.include("lancamentoFaturaList", lancamentoFaturaList)
			.include("lancamentoParcelaList", lancamentoParcelaList)
			.include("lancamentoRateioList", lancamentoRateioList)
			.include("isClosed", isClosed)
			.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive());
		
	}
	
	
	@Post
	public void adicionarLancamentoItem(String viewid, List<Long> ids, Double valorDoc) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
				
		if(lancamento != null) {
		
			if(ids != null && !ids.isEmpty()) {
				List<Item> itemList = itemRepository.find(ids);
				
				if(itemList != null && !itemList.isEmpty()) {
					for(Item item : itemList) {
						
						if(Status.INA.equals(item.getStatus())) {
							validator.add(new I18nMessage("item", "adicionar.item." + item.getTipo() + ".status.INA", item.getDisplayName()));
							break;
						}
						
						LancamentoItem lancamentoItem = new LancamentoItem();
						lancamentoItem.setItem(item);
						lancamentoItem.setSeq(lancamentoItemList.size());
						lancamentoItem.setValorUnit(item.getValor());
						lancamentoItem.setQuant(1.0);
						
						if(!DocTipo.FT.equals(lancamento.getTipoDoc()) && ObjectUtils.equals(itemList.size(), 1) && ObjectUtils.equals(lancamentoItemList.size(), 0) && valorDoc != null && (item.getValor() == null || ObjectUtils.equals(item.getValor(), 0.0))) {
							lancamentoItem.setValorUnit(valorDoc);
							lancamentoItem.setTotal(valorDoc);
						} else {
							lancamentoItem.setTotal(item.getValor());
						}
						lancamentoItemList.add(lancamentoItem);
					}
				}
			}
		
		} else {
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		lancamento.calculaTotalLancamentoItem(lancamentoItemList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoItemList", lancamentoItemList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoItemList.jsp");
	
	}
	
	
	@Post
	public void removerLancamentoItem(List<Integer> indexs, String viewid) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
		
		if(lancamento == null) {
			
			validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
		
		} else {
			
			lancamentoItemList = select(lancamentoItemList, having(on(LancamentoItem.class).getSeq(), not(isIn(indexs))));
			
			for(int i = 0; i < lancamentoItemList.size(); i++) {
				lancamentoItemList.get(i).setSeq(i);
			}
			
			lancamento.calculaTotalLancamentoItem(lancamentoItemList);
		
			session.setAttribute("lancamentoItemList_" + viewid, lancamentoItemList);
		}
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoItemList", lancamentoItemList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoItemList.jsp");
		
	}
	
	
	@Get
	public void editarLancamentoItem(Integer index, String viewid) {
		List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
		LancamentoItem lancamentoItem = lancamentoItemList.get(index);
		arquivoSession.setAquivoList(lancamentoItem.getViewid(), lancamentoItem.getArquivoList());
		result.include("lancamentoItem", lancamentoItem);
	}
	
	
	@Post 
	public void editarLancamentoItem(@NotNull(message = "{lancamentoItem.notNull}") LancamentoItem lancamentoItem, @NotBlank(message = "{viewid.notBlank}") String viewid) {
		
		if(lancamentoItem != null) {
			validator.validateProperties(lancamentoItem, "obs", "quant", "valorUnit", "desconto");
			
			if(lancamentoItem.getQuant() != null && lancamentoItem.getValorUnit() != null && lancamentoItem.getDesconto() != null) {
				Double total = NumericHelper.around((lancamentoItem.getQuant() * lancamentoItem.getValorUnit()) - lancamentoItem.getDesconto());
				if(total < 0.0) {
					validator.add(new I18nMessage("desconto", "desconto.invalid"));
				}
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
		
		for(LancamentoItem _lancamentoItem : lancamentoItemList) {
			if(_lancamentoItem.getSeq().equals(lancamentoItem.getSeq())) {
				_lancamentoItem.setObs(lancamentoItem.getObs());
				_lancamentoItem.setQuant(lancamentoItem.getQuant());
				_lancamentoItem.setValorUnit(lancamentoItem.getValorUnit());
				_lancamentoItem.setDesconto(lancamentoItem.getDesconto());
				break;
			}
		}
		
		lancamento.calculaTotalLancamentoItem(lancamentoItemList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoItemList", lancamentoItemList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoItemList.jsp");
	}
	
	
	@Get
	public void lancamentoParcela(Integer index, String viewid, Lancamento lancamento) {
		
		Lancamento _lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		LancamentoParcela lancamentoParcela = null;
		if(_lancamento == null) {
			validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
		} else {
			
			boolean isClosed = false;
			if(_lancamento.getId() != null) {
				isClosed = lancamentoRepository.isClosed(_lancamento.getId());
			}
			
			List<LancamentoParcela> lancamentoParcelaList = (List<LancamentoParcela>) session.getAttribute("lancamentoParcelaList_" + viewid);
			List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
			
			// Verifica se eh Fatura
			if(DocTipo.FT.equals(lancamento.getTipoDoc())) {
				if(index == null) {
					Double total = _lancamento.getTotalLancamentoItem() == null ? 0.0 : _lancamento.getTotalLancamentoItem();
					total += isClosed ? _lancamento.getValorDoc() : (lancamento.getValorDoc() == null ? 0.0 : lancamento.getValorDoc());
					
					if(total.equals(_lancamento.getTotalLancamentoParcela())) {
						validator.add(new I18nMessage("totalLancamentoItem", "totalLancamentoItem." + _lancamento.getTipo() + ".nothing"));
					}
				}
			} else {
				
				// Verifica se nao ha itens
				if(lancamentoItemList == null || lancamentoItemList.isEmpty()) {
					validator.add(new I18nMessage("lancamentoItemList", "lancamentoItemList." + _lancamento.getTipo() + ".notNull"));
				} 
				
				// Verifica se existem valores 
				else if(_lancamento.getTotalLancamentoItem() == null || _lancamento.getTotalLancamentoItem() == 0.0) {
					validator.add(new I18nMessage("totalLancamentoItem", "totalLancamentoItem." + _lancamento.getTipo() + ".nothing"));
				}
				
				// Verifica se o total dos itens sao iguais ao total das parcelas
				else if(index == null && _lancamento.getTotalLancamentoItem().equals(_lancamento.getTotalLancamentoParcela())) {
					validator.add(new I18nMessage("totalLancamentoParcela", "totalLancamentoParcela." + _lancamento.getTipo() + ".exceded", new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(_lancamento.getTotalLancamentoItem())));
				}
			
			}
			
			if(!validator.hasErrors()) {
				if(index == null) {
					
					lancamentoParcela = new LancamentoParcela();
					LancamentoParcela lastLancamentoParcela = selectFirst(lancamentoParcelaList, having(on(LancamentoParcela.class).getSeq(), equalTo(lancamentoParcelaList.size() - 1)));
					if(lastLancamentoParcela == null) {
						
						lancamentoParcela.setTipo(_lancamento.getTipo());
						lancamentoParcela.setSeq(0);
						
						if(lancamento.getDataDoc() != null) {
							lancamentoParcela.setData(lancamento.getDataDoc());
						} else if(lancamento.getData() != null) {
							lancamentoParcela.setData(lancamento.getData());
						}
						
						if(DocTipo.FT.equals(lancamento.getTipoDoc())) {
							lancamentoParcela.setValor(lancamento.getValorDoc());
						} else {
							lancamentoParcela.setValor(_lancamento.getTotalLancamentoItem());
						}
						
						
						// Verifica de a dataDoc eh null e se a quant de itens eh 1
						if(lancamentoItemList.size() == 1) {
							
							// Pega o Item
							Item item = lancamentoItemList.get(0).getItem();
							
							// Verifica se o dia vencimento eh null
							if(item.getDiaVencimento() == null) {
								lancamentoParcela.setData(lancamento.getData());
							} else {
								
								// Verifica se a quant de dias do mes eh maior que dia vencimento 
								if(lancamento.getData() != null && lancamento.getData().dayOfMonth().getMaximumValue() >= item.getDiaVencimento()) {
									lancamentoParcela.setData(lancamento.getData().withDayOfMonth(item.getDiaVencimento()));
								}
							}
							
							// Verifica se o meio de Pagamento foi informado
							if(item.getMeio() != null) {
								lancamentoParcela.setMeio(item.getMeio());
							}
							
							// Verifica se a Conta foi informado
							if(item.getConta() != null) {
								lancamentoParcela.setConta(item.getConta());
							}
							
							// Verifica se o Cartao de Credito foi informado
							if(item.getCartaoCredito() != null) {
								lancamentoParcela.setCartaoCredito(item.getCartaoCredito());
								lancamentoParcela.setIsLimite(item.getIsLimite());
							}
							
						}
					
					} else {
						lancamentoParcela.setSeq(lastLancamentoParcela.getSeq() + 1);
						lancamentoParcela.setData(lastLancamentoParcela.getData().plusMonths(1));
						lancamentoParcela.setMeio(lastLancamentoParcela.getMeio());
						lancamentoParcela.setTipo(lastLancamentoParcela.getTipo());
						lancamentoParcela.setCartaoCredito(lastLancamentoParcela.getCartaoCredito());
						lancamentoParcela.setIsLimite(lastLancamentoParcela.getIsLimite());
						lancamentoParcela.setConta(lastLancamentoParcela.getConta());
						
						Double totalLancamentoItem = ObjectUtils.defaultIfNull(_lancamento.getTotalLancamentoItem(), 0.0);
						Double totalLancamentoParcela = ObjectUtils.defaultIfNull(_lancamento.getTotalLancamentoParcela(), 0.0);
						Double valor = lastLancamentoParcela.getValor();
						
						if(DocTipo.FT.equals(lancamento.getTipoDoc())) {
							valor = ((isClosed ? _lancamento.getValorDoc() : lancamento.getValorDoc()) + totalLancamentoItem) - totalLancamentoParcela;
						} else {
							valor = totalLancamentoItem - totalLancamentoParcela;
							if(valor < 0) { valor = 0.0; }
						}
						
						
						if(lastLancamentoParcela.getCartaoCredito() != null && lastLancamentoParcela.getValor() < valor) {
							lancamentoParcela.setValor(lastLancamentoParcela.getValor());
						} else {
							lancamentoParcela.setValor(valor);
						}
					}
					
				} else if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()){
					lancamentoParcela = selectFirst(lancamentoParcelaList, having(on(LancamentoParcela.class).getSeq(), equalTo(index)));
				}
			}
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		arquivoSession.setAquivoList(lancamentoParcela.getViewid(), lancamentoParcela.getArquivoList());
		
		result
			.include("lancamento", _lancamento)
			.include("viewid", viewid)
			.include("lancamentoParcela", lancamentoParcela);
	
	}
	
	
	@Post
	public void lancamentoParcela(@NotNull(message = "{lancamentoParcela.notNull}") LancamentoParcela lancamentoParcela, @NotBlank(message="{viewid.notBlank}") String viewid, Lancamento lancamento) {
		
		Lancamento _lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoParcela> lancamentoParcelaList = (List<LancamentoParcela>) session.getAttribute("lancamentoParcelaList_" + viewid);		
		
		if(_lancamento == null) {
			validator.add(new I18nMessage("lancamento", "lancamento.notNull"));
		} else {
			
			if(lancamentoParcela != null) {
				
				if(lancamentoParcelaList.size() > lancamentoParcela.getSeq()) {
					LancamentoParcela _lancamentoParcela = lancamentoParcelaList.get(lancamentoParcela.getSeq());
					if(LancamentoStatus.BXD.equals(_lancamentoParcela.getStatus()) || LancamentoStatus.COM.equals(_lancamentoParcela.getStatus())) {
						lancamentoParcela.setMeio(_lancamentoParcela.getMeio());
						lancamentoParcela.setCartaoCredito(_lancamentoParcela.getCartaoCredito());
						lancamentoParcela.setIsLimite(_lancamentoParcela.getIsLimite());
						lancamentoParcela.setConta(_lancamentoParcela.getConta());
						lancamentoParcela.setCheque(_lancamentoParcela.getCheque());
						lancamentoParcela.setValor(_lancamentoParcela.getValor());
					}
				}
				
				lancamentoParcela.setTipo(_lancamento.getTipo());
				
				LancamentoParcela lastLancamentoParcela = selectFirst(lancamentoParcelaList, having(on(LancamentoParcela.class).getSeq(), equalTo(lancamentoParcela.getSeq() - 1)));
				
				if(ItemTipo.DES.equals(lancamentoParcela.getTipo())) {
					if(MeioPagamento.CHQ.equals(lancamentoParcela.getMeio()) || MeioPagamento.CCR.equals(lancamentoParcela.getMeio())) {
						lancamentoParcela.setConta(null);
					}
				} else if(ItemTipo.REC.equals(lancamentoParcela.getTipo())) {
					lancamentoParcela.setCartaoCredito(null);
					lancamentoParcela.setIsLimite(null);
					lancamentoParcela.setBoletoTipo(null);
					lancamentoParcela.setBoletoNumero(null);
				}
				
				if(!MeioPagamento.CHQ.equals(lancamentoParcela.getMeio())) {
					lancamentoParcela.setCheque(null);
				}
				
				// Verifica se o mesmo cheque foi utilizado em varios pagamentos / recebimentos
				if(lancamentoParcela.getCheque() != null && lancamentoParcela.getCheque().getNumero() != null) {
					LancamentoParcela outroLancamentoParcelaMesmoCheque = selectFirst(lancamentoParcelaList, 
						having(on(LancamentoParcela.class).getCheque().getTalaoCheque(), equalTo(lancamentoParcela.getCheque().getTalaoCheque()))
						.and(having(on(LancamentoParcela.class).getCheque().getNumero(), equalTo(lancamentoParcela.getCheque().getNumero())))
						.and(having(on(LancamentoParcela.class).getSeq(), not(equalTo(lancamentoParcela.getSeq()))))
						);
					
					if(outroLancamentoParcelaMesmoCheque != null) {
						TalaoCheque talaoCheque = talaoChequeRepository.find(lancamentoParcela.getCheque().getTalaoCheque().getId());
						validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.cheque.numero.exists", lancamentoParcela.getCheque().getNumero(), talaoCheque.getConta().getNome()));
					}
					
				}
				
				
				
				if(!MeioPagamento.CCR.equals(lancamentoParcela.getMeio())) {
					lancamentoParcela.setCartaoCredito(null);
				}
				
				// Verifica se a conta foi informada
				if(lancamentoParcela.getConta() != null && lancamentoParcela.getConta().getId() == null) {
					lancamentoParcela.setConta(null);
				}
				
				// Verifica se o cartao foi informado
				if(lancamentoParcela.getCartaoCredito() != null && lancamentoParcela.getCartaoCredito().getId() == null) {
					lancamentoParcela.setCartaoCredito(null);
				}
				
				// Verifica se o cheque foi informado
				if(lancamentoParcela.getCheque() != null && lancamentoParcela.getCheque().getNumero() == null) {
					lancamentoParcela.setCheque(null);
				}
				
				// Pega Conta do banco de dados
				if(lancamentoParcela.getConta() != null) {
					lancamentoParcela.setConta(contaRepository.find(lancamentoParcela.getConta().getId()));
				}
				
				// Pega Cartao no banco de dados
				if(lancamentoParcela.getCartaoCredito() != null) {
					lancamentoParcela.setCartaoCredito(cartaoCreditoRepository.find(lancamentoParcela.getCartaoCredito().getId()));
				}
				
				// Pega Cheque no banco de dados
				if(lancamentoParcela.getCheque() != null) {
					
					// Pega informacoes do Talao
					if(lancamentoParcela.getCheque().getTalaoCheque() != null && lancamentoParcela.getCheque().getTalaoCheque().getId() != null) {
						lancamentoParcela.getCheque().setTalaoCheque(talaoChequeRepository.find(lancamentoParcela.getCheque().getTalaoCheque().getId()));
					}
					
					// Pega informacoes do Banco
					if(lancamentoParcela.getCheque().getBanco() != null && lancamentoParcela.getCheque().getBanco().getId() != null) {
						lancamentoParcela.getCheque().setBanco(pessoaRepository.find(lancamentoParcela.getCheque().getBanco().getId()));
					}
					
					// Pega informacoes do Nome
					if(lancamentoParcela.getCheque().getNome() != null && lancamentoParcela.getCheque().getNome().getId() != null) {
						lancamentoParcela.getCheque().setNome(pessoaRepository.find(lancamentoParcela.getCheque().getNome().getId()));
					}
					
				}
				
				validator.validateProperties(lancamentoParcela, "seq", "data", "meio", "boletoTipo", "boletoNumero", "obs", "valor");
				
				// Verificar Data
				if(lancamentoParcela.getData() != null) {
				
					// Verifica se a data da Parcela e inferior a data da parcela anterior
					if(lastLancamentoParcela != null && lancamentoParcela.getData().isBefore(lastLancamentoParcela.getData())) {
						validator.add(new I18nMessage("data", "lancamentoParcela.data.isEqualOrAfterTo", lastLancamentoParcela.getData().toString(localization.getMessage("patternDate"))));
					}
					
					// Verifica se a data da Parcela e superior a data da proximo parcela
					LancamentoParcela nextLancamentoParcela = selectFirst(lancamentoParcelaList, having(on(LancamentoParcela.class).getSeq(), equalTo(lancamentoParcela.getSeq() + 1)));
					if(nextLancamentoParcela != null && lancamentoParcela.getData().isAfter(nextLancamentoParcela.getData())) {
						validator.add(new I18nMessage("data", "lancamentoParcela.data.isEqualOrBeforeTo", nextLancamentoParcela.getData().toString(localization.getMessage("patternDate"))));
					}
				
				}
				
				// Verifica se o Total das Parcelas nao excede o Total dos Itens
				if(!DocTipo.FT.equals(lancamento.getTipoDoc())) {
					if(lancamentoParcela.getValor() != null) {
						Double total = lancamentoParcela.getValor();
						if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
							for(LancamentoParcela _lancamentoParcela : lancamentoParcelaList) {
								if(!_lancamentoParcela.getSeq().equals(lancamentoParcela.getSeq())) {
									total += _lancamentoParcela.getValor();
								}
							}
						}
						
						if(NumericHelper.around(total) > _lancamento.getTotalLancamentoItem()) {
							validator.add(new I18nMessage("valor", "totalLancamentoParcela." + _lancamento.getTipo() + ".exceded", new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(_lancamento.getTotalLancamentoItem())));
						}
					}
				}
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(lancamentoParcelaList.size() > lancamentoParcela.getSeq()) {
			LancamentoParcela _lancamentoParcela = lancamentoParcelaList.get(lancamentoParcela.getSeq());
			_lancamentoParcela.setArquivo(lancamentoParcela.getArquivo());
			_lancamentoParcela.setBoletoNumero(lancamentoParcela.getBoletoNumero());
			_lancamentoParcela.setBoletoTipo(lancamentoParcela.getBoletoTipo());
			if(!LancamentoStatus.BXD.equals(_lancamentoParcela.getStatus()) && !LancamentoStatus.COM.equals(_lancamentoParcela.getStatus())) {
				_lancamentoParcela.setCartaoCredito(lancamentoParcela.getCartaoCredito());
				_lancamentoParcela.setIsLimite(lancamentoParcela.getIsLimite());
				_lancamentoParcela.setCheque(lancamentoParcela.getCheque());
				_lancamentoParcela.setConta(lancamentoParcela.getConta());
				_lancamentoParcela.setMeio(lancamentoParcela.getMeio());
				_lancamentoParcela.setValor(lancamentoParcela.getValor());
			}
			_lancamentoParcela.setData(lancamentoParcela.getData());
			_lancamentoParcela.setObs(lancamentoParcela.getObs());
			_lancamentoParcela.setViewid(lancamentoParcela.getViewid());
		} else {
			lancamentoParcelaList.add(lancamentoParcela);
		}
		
		_lancamento.calculaTotalLancamentoParcela(lancamentoParcelaList);
		
		result
			.include("lancamento", _lancamento)
			.include("viewid", viewid)
			.include("lancamentoParcelaList", lancamentoParcelaList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoParcelaList.jsp");
	}
	
	
	@Get
	public void lancamentoParcelaBoleto(BoletoTipo boletoTipo, String boletoNumero) {
		result.include("boletoTipo", boletoTipo).include("boletoNumero", boletoNumero);
	}
	
	
	@Post
	public void lancamentoParcelaBoleto(@NotNull(message = "{lancamentoParcela.notNull}") LancamentoParcela lancamentoParcela) {
		
		if(lancamentoParcela != null) {
			validator.validateProperties(lancamentoParcela, "boletoTipo", "boletoNumero");
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
		result.use(Results.jsonp()).withCallback("lancamentoParcelaBoletoSuccess").withoutRoot().from(lancamentoParcela).serialize();
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Post
	public void removerLancamentoParcela(List<Integer> indexs, String viewid) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoParcela> lancamentoParcelaList = (List<LancamentoParcela>) session.getAttribute("lancamentoParcelaList_" + viewid);
		
		List<LancamentoParcela> deleteLancamentoParcelaList = new ArrayList<LancamentoParcela>();
		for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
			if(indexs.contains(lancamentoParcela.getSeq())) {
				deleteLancamentoParcelaList.add(lancamentoParcela);
			}
		}
		
		for(LancamentoParcela lancamentoParcela : deleteLancamentoParcelaList) {
			if(lancamentoParcela.getId() != null && (LancamentoStatus.BXD.equals(lancamentoParcela.getStatus()) || LancamentoStatus.COM.equals(lancamentoParcela.getStatus()))) {
				validator.add(new I18nMessage("lancamentoParcela_" + lancamentoParcela.getId(), "lancamentoParcela." + lancamento.getTipo() + ".baixaItem.exists"));
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		List<LancamentoParcela> lancamentoParcelaRemove = new ArrayList<LancamentoParcela>();
		for(Integer index : indexs) {
			lancamentoParcelaRemove.add(lancamentoParcelaList.get(index));
		}
		lancamentoParcelaList.removeAll(lancamentoParcelaRemove);
		
		for(int i = 0; i < lancamentoParcelaList.size(); i++) {
			lancamentoParcelaList.get(i).setSeq(i);
		}
		
		lancamento.calculaTotalLancamentoParcela(lancamentoParcelaList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoParcelaList", lancamentoParcelaList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoParcelaList.jsp");
	
	}
	
	
	@SuppressWarnings("unused")
	@Get
	public void dividirLancamentoParcela(String viewid, DocTipo tipoDoc, Double valorDoc) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoParcela> lancamentoParcelaList = (List<LancamentoParcela>) session.getAttribute("lancamentoParcelaList_" + viewid);
		
		lancamento.calculaTotalLancamentoParcela(lancamentoParcelaList);
		
		Double valorBxd = 0.0;
		int quantDividir = lancamentoParcelaList.size();
		for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
			if(LancamentoStatus.BXD.equals(lancamentoParcela.getStatus()) || LancamentoStatus.COM.equals(lancamentoParcela.getStatus())) {
				valorBxd += lancamentoParcela.getValor();
				quantDividir--;
			}
		}
		
		Double valorDivir = (DocTipo.FT.equals(tipoDoc) ? lancamento.getTotalLancamentoItem() + valorDoc: lancamento.getTotalLancamentoItem()) - valorBxd;
		
		
		if(lancamento == null) {
			validator.add(new I18nMessage("lancamento", "lancamento.notNull"));
		} else {
			
			if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
				Double valor = valorDivir / quantDividir;
				for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
					if(!LancamentoStatus.BXD.equals(lancamentoParcela.getStatus()) && !LancamentoStatus.COM.equals(lancamentoParcela.getStatus())) {
						lancamentoParcela.setValor(valor);
					}
				}
			}
			lancamento
				.calculaTotalLancamentoParcela(lancamentoParcelaList)
				.corrigeValoresParcela(tipoDoc, valorDoc, lancamentoParcelaList);
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoParcelaList", lancamentoParcelaList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoParcelaList.jsp");
		
	}
	
	
	@Get
	public void lancamentoRateio(Integer index, String viewid) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
		List<LancamentoParcela> lancamentoParcelaList = (List<LancamentoParcela>) session.getAttribute("lancamentoParcelaList_" + viewid);
		List<LancamentoRateio> lancamentoRateioList = (List<LancamentoRateio>) session.getAttribute("lancamentoRateioList_" + viewid);
		
		LancamentoRateio lancamentoRateio = null;
		if(lancamento == null) {
			
			validator.add(new I18nMessage("lancamento", "lancamento.notNull"));
			
		} else {
			
			if(index == null) {
				
				lancamentoRateio = new LancamentoRateio();
				lancamentoRateio.setSeq(lancamentoRateioList.size());
				
				lancamento.calculaTotal(lancamentoItemList, lancamentoParcelaList, lancamentoRateioList);
				Double rateioRestante = 0.0;
				
				
				if(lancamento.getTotalLancamentoParcela() != null && lancamento.getTotalLancamentoRateio() != null) {
					rateioRestante = lancamento.getTotalLancamentoParcela() - lancamento.getTotalLancamentoRateio();
				}
				
				if(rateioRestante == 0) {
					if(lancamento.getTotalLancamentoParcela() == null || lancamento.getTotalLancamentoParcela() == 0.0) {
						validator.add(new I18nMessage("lancamentoRateio", "lancamentoRateio.valor.nothing"));
					} else {
						validator.add(new I18nMessage("lancamentoRateio", "lancamentoRateio.valor.exceded", new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(lancamento.getTotalLancamentoItem())));
					}
				} else {
					if(lancamento.getTotalLancamentoParcela() != null && lancamento.getTotalLancamentoParcela() != 0.0) {
						if(lancamento.getTotalLancamentoRateio() == null || lancamento.getTotalLancamentoRateio() == 0.0) {
							lancamentoRateio.setPerc(100.0);
						} else {
							lancamentoRateio.setPerc(100 - ((lancamento.getTotalLancamentoRateio() / lancamento.getTotalLancamentoParcela()) * 100));
						}
					}
					lancamentoRateio.setValor(rateioRestante);
				}
			} else {
				try {
					lancamentoRateio = lancamentoRateioList.get(index);
				} catch(Exception e) {
					validator.add(new I18nMessage("lancamentoRateio", "lancamentoRateio.notFound"));
				}
				
			}
			
			validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
			
			arquivoSession.setAquivoList(lancamentoRateio.getViewid(), lancamentoRateio.getArquivoList());
			
			result
				.include("lancamento", lancamento)
				.include("viewid", viewid)
				.include("lancamentoRateio", lancamentoRateio)
				.include("cartaoCreditoList", cartaoCreditoRepository.findAllActive())
				.include("rateioList", descricaoRepository.findAllActive(DescricaoTipo.RATEIO));
			
		}
		
	}
	
	
	@Post
	public void lancamentoRateio(
			@NotNull(message = "{lancamentoRateio.notNull}") 
			LancamentoRateio lancamentoRateio, 
			@NotBlank(message="{viewid.notBlank}") 
			String viewid) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoRateio> lancamentoRateioList = (List<LancamentoRateio>) session.getAttribute("lancamentoRateioList_" + viewid);
				
		if(lancamentoRateio != null && viewid != null) {
			
			// Valida Rateio
			validator.validateProperties(lancamentoRateio, "tipo", "seq", "perc", "valor");
			
			switch(lancamentoRateio.getTipo()) {
			case PES:
				if(lancamentoRateio.getPessoa() == null || lancamentoRateio.getPessoa().getId() == null) {
					validator.add(new I18nMessage("pessoa", "pessoa.notNull"));
				} else {
					Pessoa pessoa = pessoaRepository.find(lancamentoRateio.getPessoa().getId());
					if(pessoa == null && lancamentoRateio.getPessoa().equals(usuarioLogged.getCliente())) {
						pessoa = usuarioLogged.getCliente();
					}
					if(pessoa == null) {
						validator.add(new I18nMessage("pessoa", "pessoa.notFound"));
					} else {
						lancamentoRateio.setPessoa(pessoa);
					}
					lancamentoRateio.setRateio(null);
				}
				break;
			case RAT:
				if(lancamentoRateio.getRateio() == null || lancamentoRateio.getRateio().getId() == null) {
					validator.add(new I18nMessage("rateio", "rateio.notNull"));
				} else {
					try {
						Descricao outro = descricaoRepository.find(lancamentoRateio.getRateio().getId(), DescricaoTipo.RATEIO);
						lancamentoRateio.setRateio(outro);
					}catch(NoResultException e) {
						validator.add(new I18nMessage("rateio", "rateio.notFound"));
					}
					lancamentoRateio.setPessoa(null);
				}
				break;
			}
			
			
			Double total = lancamentoRateio.getValor() == null ? 0.0 : lancamentoRateio.getValor();
			
			// Verifica se existem rateios repetidos
			if(lancamentoRateio.getTipo() != null && lancamentoRateioList != null && !lancamentoRateioList.isEmpty()) {
				
				for(LancamentoRateio _lancamentoRateio : lancamentoRateioList) {
					
					if(_lancamentoRateio.getTipo().equals(lancamentoRateio.getTipo()) 
							&& _lancamentoRateio.getDisplayNome().equals(lancamentoRateio.getDisplayNome()) 
							&& !_lancamentoRateio.getSeq().equals(lancamentoRateio.getSeq())) {
						switch(lancamentoRateio.getTipo()) {
						case RAT:
							validator.add(new I18nMessage("rateio", "lancamentoRateio.rateio.exists"));
							break;
						case PES:
							validator.add(new I18nMessage("pessoa", "lancamentoRateio.pessoa.exists"));
							break;
						}
						
					}
					
					if(!_lancamentoRateio.getSeq().equals(lancamentoRateio.getSeq())) {
						total += _lancamentoRateio.getValor();
					}
					
				}
			}
			
			
			if(NumericHelper.around(total) > lancamento.getTotalLancamentoParcela()) {
				validator.add(new I18nMessage("valor", "lancamentoRateio.valor.exceded", new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(lancamento.getTotalLancamentoItem())));
			}
			
			lancamentoRateio.setArquivoList(arquivoSession.getArquivoList(lancamentoRateio.getViewid()));			
			
			if(!validator.hasErrors()) {
				if(lancamentoRateioList.size() > lancamentoRateio.getSeq()) {
					lancamentoRateioList.set(lancamentoRateio.getSeq(), lancamentoRateio);
				} else {
					lancamentoRateioList.add(lancamentoRateio);
				}
			}

		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		lancamento.calculaTotalLancamentoRateio(lancamentoRateioList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoRateioList", lancamentoRateioList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoRateioList.jsp");
	}
	
	
	@Post
	public void removerLancamentoRateio(List<Integer> indexs, String viewid) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoRateio> lancamentoRateioList = (List<LancamentoRateio>) session.getAttribute("lancamentoRateioList_" + viewid);
		
		List<LancamentoRateio> removeLancamentoRateioList = new ArrayList<LancamentoRateio>();
		
		for(Integer index : indexs) {
			removeLancamentoRateioList.add(lancamentoRateioList.get(index));
		}
		
		lancamentoRateioList.removeAll(removeLancamentoRateioList);
		
		for(int i = 0; i < lancamentoRateioList.size(); i++) {
			lancamentoRateioList.get(i).setSeq(i);
		}
				
		lancamento.calculaTotalLancamentoRateio(lancamentoRateioList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoRateioList", lancamentoRateioList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoRateioList.jsp");
	
	}
	
	
	@Get
	public void dividirLancamentoRateio(String viewid, DocTipo tipoDoc, Double valorDoc) {
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoRateio> lancamentoRateioList = (List<LancamentoRateio>) session.getAttribute("lancamentoRateioList_" + viewid);
		
		Double totalRatear = 0.0;
		
		if(lancamento == null) {
			
			validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
			
		} else {
		
			boolean isClosed = lancamentoRepository.isClosed(lancamento.getId());
			
			if(isClosed && DocTipo.FT.equals(lancamento.getTipoDoc())) {
				valorDoc = lancamento.getValorDoc();
				tipoDoc = lancamento.getTipoDoc();
			}
			
			
			if(lancamento.getTotalLancamentoParcela() != null) {
				totalRatear += lancamento.getTotalLancamentoParcela();
			}
						
			if(totalRatear == 0.0) {
				validator.add(new I18nMessage("lancamentoRateio", "lancamentoRateio.valor.nothing"));
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		Double perc = 0.0;
		Double valor = 0.0;
		int count = lancamentoRateioList.size();
		
		if(totalRatear != 0.0 && count > 0) {
			valor = NumericHelper.around(totalRatear / count);
			perc = 100.0 / count;
			for(LancamentoRateio lancamentoRateio : lancamentoRateioList) {
				lancamentoRateio.setPerc(perc);
				lancamentoRateio.setValor(valor);
			}
		}
		
		lancamento
			.calculaTotalLancamentoRateio(lancamentoRateioList)
			.corrigeValoresRateio(lancamentoRateioList);
		
		
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoRateioList", lancamentoRateioList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoRateioList.jsp");
		
	}
	
	
	@Post
	public void cadastro(
		@NotNull(message = "{lancamento.notNull}") 
		Lancamento lancamento, 
		@NotBlank(message = "{viewid.notBlank}")
		String viewid,
		List<Long> lancamentoItemDelete,
		List<Long> lancamentoFaturaDelete,
		List<Long> lancamentoParcelaDelete,
		List<Long> lancamentoRateioDelete,
		Boolean isConfirmTipoDocPessoTipo,
		Boolean isConfirmNumeroDocExists,
		Boolean isConfirmValorDocNotEqual,
		boolean isBaixa
	) {
		
		
		Lancamento _lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoItem> lancamentoItemList = (List<LancamentoItem>) session.getAttribute("lancamentoItemList_" + viewid);
		List<LancamentoParcela> lancamentoParcelaList = (List<LancamentoParcela>) session.getAttribute("lancamentoParcelaList_" + viewid);
		List<LancamentoRateio> lancamentoRateioList = (List<LancamentoRateio>) session.getAttribute("lancamentoRateioList_" + viewid);
		List<LancamentoFatura> lancamentoFaturaList = null;
		
		// Verifica se o Lancamento nao eh null
		if(lancamento != null) {
			
			if(_lancamento == null) {
				
				validator.add(new I18nMessage("lancamento", "lancamento.notNull"));
			
			} else {
				
				Pessoa favorecido = pessoaRepository.findOrCliente(lancamento.getFavorecido().getId());
				
				_lancamento.setData(lancamento.getData());
				_lancamento.setFavorecido(lancamento.getFavorecido());
				_lancamento.setStatus(lancamento.getStatus());
				_lancamento.setTipoDoc(lancamento.getTipoDoc());
				_lancamento.setNumeroDoc(lancamento.getNumeroDoc());
				_lancamento.setDataDoc(lancamento.getDataDoc());
				_lancamento.setCartaoCredito(lancamento.getCartaoCredito());
				_lancamento.setValorDoc(lancamento.getValorDoc());
				_lancamento.setObs(lancamento.getObs());
				_lancamento.setArquivoDoc(lancamento.getArquivoDoc());
				
				
				_lancamento.calculaTotal(lancamentoItemList, lancamentoParcelaList, lancamentoRateioList);
				
				
				
				// Valida lancamento
				validator.validateProperties(_lancamento, "data", "status", "tipoDoc", "dataDoc", "numeroDoc", "valorDoc", "obs");
				
				
				
				// Valida favorecido
				if(_lancamento.getFavorecido() == null || _lancamento.getFavorecido().getId() == null) {
					validator.add(new I18nMessage("favorecido", "favorecido." + _lancamento.getTipo() + ".notNull"));
				} else {
					if(!pessoaRepository.exists(_lancamento.getFavorecido().getId())) {
						validator.add(new I18nMessage("favorecido", "favorecido." + _lancamento.getTipo() + ".notFound"));
					}
				}
				
				
				// Valida Cartao Credito
				if(_lancamento.getCartaoCredito() != null && _lancamento.getCartaoCredito().getId() == null) {
					_lancamento.setCartaoCredito(null);
				}
			
				
				// Verifica se eh Fatura
				if(DocTipo.FT.equals(_lancamento.getTipoDoc()) && _lancamento.getTipo().equals(ItemTipo.DES)) {
					
					if(_lancamento.getDataDoc() == null) {
						validator.add(new I18nMessage("dataDoc", "dataDoc.notNull"));
					}
					
					if(_lancamento.getValorDoc() == null) {
						validator.add(new I18nMessage("valorDoc", "valorDoc.notNull"));
					}
					
					if(_lancamento.getCartaoCredito() == null) {
						validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notNull"));
					} else {
						CartaoCredito cartaoCredito = cartaoCreditoRepository.find(_lancamento.getCartaoCredito().getId());
					
						if(cartaoCredito == null) {
							validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notFound"));
						} else {
						
							lancamentoFaturaList = (List<LancamentoFatura>) session.getAttribute("lancamentoFaturaList_" + viewid);
						
							// Verifica se ao menos um movimento foi informado
							if(cartaoCredito.getLimiteUtilizado() != 0.0 && (lancamentoFaturaList == null || lancamentoFaturaList.isEmpty())) {
								
								validator.add(new I18nMessage("lancamentoFaturaList", "lancamentoFaturaList.notNull"));
								
							} else {
								_lancamento.calculaTotalLancamentoFatura(lancamentoFaturaList);
								
								// Verifica se os movimentos sao do cartao de credito informado
								for(LancamentoFatura lancamentoFatura : lancamentoFaturaList) {
									
									// Lancamento Parcela
									if(lancamentoFatura.getLancamentoParcela() != null) {
										if(lancamentoFatura.getLancamentoParcela().getStatus().equals(LancamentoStatus.LAN)) {
											if(!lancamentoFatura.getLancamentoParcela().getCartaoCredito().equals(cartaoCredito)) {
												validator.add(new I18nMessage("lancamentoFatura_" + lancamentoFatura.getSeq(), "lancamentoFatura.cartaoCredito.notEqual", lancamentoFatura.getLancamentoParcela().getCartaoCredito().getNome()));
											}
										} else if(lancamentoFatura.getLancamentoParcela().getStatus().equals(LancamentoStatus.BXD)) {
											if(!lancamentoFatura.getLancamentoParcela().getBaixaItem().getCartaoCredito().equals(cartaoCredito)) {
												validator.add(new I18nMessage("lancamentoFatura_" + lancamentoFatura.getSeq(), "lancamentoFatura.cartaoCredito.notEqual", lancamentoFatura.getLancamentoParcela().getCartaoCredito().getNome()));
											}
										}
									}
									
									// Transferencia
									if(lancamentoFatura.getTransferencia() != null && lancamentoFatura.getTransferencia().getStatus().equals(LancamentoStatus.LAN)) {
										if(!lancamentoFatura.getTransferencia().getCartaoCredito().equals(cartaoCredito)) {
											validator.add(new I18nMessage("lancamentoFatura_" + lancamentoFatura.getSeq(), "lancamentoFatura.cartaoCredito.notEqual", lancamentoFatura.getLancamentoParcela().getCartaoCredito().getNome()));
										}
									}
									
								}
								
								//Verifica se existe diferenca
								if(!_lancamento.getTotalLancamentoFatura().equals(_lancamento.getValorDoc())) {
									validator.add(new I18nMessage("valorDoc", "valorDoc.notEqual.totalLancamentoFatura"));
								}
								
							}
							
							if(!validator.hasErrors()) {
								_lancamento.setDiferenca(_lancamento.getValorDoc() - _lancamento.getTotalLancamentoFatura());
								_lancamento.setLimite(cartaoCredito.getLimite());
								_lancamento.setLimiteDisponivel(cartaoCredito.getLimiteDisponivel());
								_lancamento.setLimiteUtilizado(cartaoCredito.getLimiteUtilizado());
							}
						}
					}
					_lancamento.setNumeroDoc(null);
				} else {
					
					// Verifica se ao menos um item foi informado
					if(lancamentoItemList == null || lancamentoItemList.isEmpty()) {
						validator.add(new I18nMessage("lancamentoItemList", "lancamentoItemList." + _lancamento.getTipo() +  ".notNull"));
					}
					
					// Verifica se o total dos itens eh diferente do valor do doc
					if(isConfirmValorDocNotEqual != null && !isConfirmValorDocNotEqual && _lancamento.getValorDoc() != null && _lancamento.getTotalLancamentoItem() != null && !_lancamento.getValorDoc().equals(_lancamento.getTotalLancamentoItem())) {
						validator.add(new I18nMessage("isConfirmValorDocNotEqual", "confirm." + _lancamento.getTipo() + ".valorDoc.notEqual"));
					}
					
					_lancamento.setCartaoCredito(null);
					_lancamento.setDiferenca(null);
					_lancamento.setLimite(null);
					_lancamento.setLimiteDisponivel(null);
					_lancamento.setLimiteUtilizado(null);
				
				}
				
				
				
				// Verifica se o documento eh nota fiscal e ja foi lancado
				if(DocTipo.NF.equals(lancamento.getTipoDoc()) || DocTipo.CF.equals(lancamento.getTipoDoc())) {
					
					// Verifica se o favorecido eh pessoa fisica e se e NF
					if(isConfirmTipoDocPessoTipo != null && !isConfirmTipoDocPessoTipo) {
						if(favorecido != null && favorecido.getPessoaTipo().equals(PessoaTipo.F)) {
							validator.add(new I18nMessage("isConfirmTipoDocPessoTipo", "confirm.tipoDoc." + lancamento.getTipoDoc() + ".favorecido.pessoaTipo.F"));
						}
					}
					
					// Verifica se o Numero da Nota ja existe
					if(isConfirmNumeroDocExists != null && !isConfirmNumeroDocExists && lancamentoRepository.existsDoc(lancamento.getId(), lancamento.getNumeroDoc(), lancamento.getFavorecido())) {
						validator.add(new I18nMessage("isConfirmNumeroDocExists", "confirm.numeroDoc." + lancamento.getTipoDoc() + ".exists", lancamento.getNumeroDoc()));
					}
					
				}
				
				
				// Verifica Saldo das Contas e Cartoes de Crédito
				Map<Conta, Double> valorConta = new HashMap<Conta, Double>();
				Map<CartaoCredito, Double> valorCartaoCredito = new HashMap<CartaoCredito, Double>();
				for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
					
					// Verifica se a parcela foi paga com a conta
					if(lancamentoParcela.getConta() != null && lancamentoParcela.getConta().getId() != null) {
						
						// Verifica se a conta nao existe no hash
						if(!valorConta.containsKey(lancamentoParcela.getConta())) {
							valorConta.put(lancamentoParcela.getConta(), 0.0);
						}
						
						Double valor = valorConta.get(lancamentoParcela.getConta());
						valor += lancamentoParcela.getValor();
						valorConta.put(lancamentoParcela.getConta(), valor);
					}
					
					// Verifica se a parcela foi paga com o cartao de credito
					if(lancamentoParcela.getCartaoCredito() != null && lancamentoParcela.getCartaoCredito().getId() != null && BooleanUtils.isTrue(lancamentoParcela.getIsLimite())) {
						
						// Verifica se a conta nao existe no hash
						if(!valorCartaoCredito.containsKey(lancamentoParcela.getCartaoCredito())) {
							valorCartaoCredito.put(lancamentoParcela.getCartaoCredito(), 0.0);
						}
						
						Double valor = valorCartaoCredito.get(lancamentoParcela.getCartaoCredito());
						valor += lancamentoParcela.getValor();
						valorCartaoCredito.put(lancamentoParcela.getCartaoCredito(), valor);
					}
				
				}
				
				
				// Verifica se eh Despesa
				if(_lancamento.getTipo().equals(ItemTipo.DES)) {
				
					// Verifica saldo das contas
					for(Conta conta : valorConta.keySet()) {
					
						Double valor = valorConta.get(conta);
						
						if(valor > conta.getSaldoUtilizavel()) {
							if(!BooleanUtils.toBoolean(request.getParameter("isConfirmContaSaldoInsuficiente_" + conta.getId()))) {
								validator.add(new I18nMessage("isConfirmContaSaldoInsuficiente_" + conta.getId(), "confirm.conta.saldoInsuficiente", conta.getNome()));
							}
						}
						
					}
					
					// Verifica limite dos cartoes de credito
					for(CartaoCredito cartaoCredito : valorCartaoCredito.keySet()) {
						
						Double valor = valorCartaoCredito.get(cartaoCredito);
						
						if(valor > cartaoCredito.getLimiteDisponivel()) {
							if(!BooleanUtils.toBoolean(request.getParameter("isConfirmCartaoCreditoLimiteInsuficiente_" + cartaoCredito.getId()))) {
								validator.add(new I18nMessage("isConfirmCartaoCreditoLimiteInsuficiente_" + cartaoCredito.getId(), "confirm.cartaoCredito.limiteInsuficiente", cartaoCredito.getNome()));
							}
						}
						
					}
				
				}
				
				// Arquivo
				if(_lancamento.getArquivoDoc() != null && StringUtils.isBlank(_lancamento.getArquivoDoc().getNome())) {
					_lancamento.setArquivoDoc(null);
				}
				arquivoRepository.save(_lancamento);
				
				
				// Verifica se ao menos um pagamento foi informado
				if(lancamentoParcelaList == null || lancamentoParcelaList.isEmpty()) {
					
					validator.add(new I18nMessage("lancamentoParcelaList", "lancamentoParcelaList." + _lancamento.getTipo() + ".notNull"));
				
				} else {
					
					for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
						if(lancamentoParcela.getTipo().equals(ItemTipo.REC) && lancamentoParcela.getCheque() != null && lancamentoParcela.getCheque().getNumero() != null) {
							
							lancamentoParcela.getCheque().setCliente(usuarioLogged.getCliente());
							lancamentoParcela.getCheque().setTalaoCheque(null);
							
							if(lancamentoParcela.getCheque().getBanco() != null && lancamentoParcela.getCheque().getBanco().getId() == null) {
								lancamentoParcela.getCheque().setBanco(null);
							}
							
							if(lancamentoParcela.getCheque().getNome() != null && lancamentoParcela.getCheque().getNome().getId() == null) {
								lancamentoParcela.getCheque().setNome(null);
							}
						
						}
					}
					
				}
				
				
				
				// Verifica se o total dos itens eh diferente do total de pagamento
				if(!DocTipo.FT.equals(_lancamento.getTipoDoc())) {
					if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()  && !_lancamento.getTotalLancamentoItem().equals(_lancamento.getTotalLancamentoParcela())) {
						validator.add(new I18nMessage("totalLancamentoParcela", "totalLancamentoParcela." + _lancamento.getTipo() + ".notEqual.totalLancamentoItem"));
					}
				}
				
				
				// Verifica se existe rateio
				if(lancamentoRateioList != null && !lancamentoRateioList.isEmpty()) {
					
					// Verifica se o total rateado eh diferente do total de itens
					if(!_lancamento.getTotalLancamentoRateio().equals(_lancamento.getTotalLancamentoParcela())) {
						validator.add(new I18nMessage("totalLancamentoRateio", "totalLancamentoRateio.notEqual.totalLancamentoItem", new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localization.getLocale())).format(_lancamento.getTotalLancamentoParcela())));
					}
					
				}
			
			}
			
		}
	
		if(isBaixa && !lancamento.getStatus().equals(LancamentoStatus.LAN)) {
			validator.add(new I18nMessage("status", "lancamento.status.notEqual.LAN.isBaixa." + _lancamento.getTipo()));
		}
		
		
		validator.onErrorUse(Results.jsonp()).withCallback("lancamentoCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		
		// Itens
		for(LancamentoItem lancamentoItem : lancamentoItemList) {
			lancamentoItem.setLancamento(_lancamento);
		}
		
		
		// Parcelas
		Boolean isBaixaOk = false;
		for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
			
			lancamentoParcela.setLancamento(_lancamento);
			
			
			// Cartao de Credito
			if(lancamentoParcela.getCartaoCredito() != null && lancamentoParcela.getCartaoCredito().getId() == null) {
				lancamentoParcela.setCartaoCredito(null);
			}
			
			// Conta
			if(lancamentoParcela.getConta() != null && lancamentoParcela.getConta().getId() == null) {
				lancamentoParcela.setConta(null);
			}
			
			
			// Cheque
			if(lancamentoParcela.getCheque() != null && lancamentoParcela.getCheque().getNumero() == null) {
				lancamentoParcela.setCheque(null);
			}
			
			
			// Verifica se o LancamentoParcela esta ok para fazer baixa
			if(lancamentoParcela.getStatus() == null || lancamentoParcela.getStatus().equals(LancamentoStatus.LAN) || lancamentoParcela.getStatus().equals(LancamentoStatus.CAN) || lancamentoParcela.getStatus().equals(LancamentoStatus.STD)) {
				lancamentoParcela.setStatus(_lancamento.getStatus());
				if(LancamentoStatus.LAN.equals(lancamentoParcela.getStatus()) && (lancamentoParcela.getConta() != null || lancamentoParcela.getCartaoCredito() != null || lancamentoParcela.getCheque() != null)) {
					isBaixaOk = true;
				}
			}
			
			
			// Relaciona Cliente ao Cheque
			if(lancamentoParcela.getCheque() != null) {
				lancamentoParcela.getCheque().setCliente(usuarioLogged.getCliente());
				
				if(lancamentoParcela.getCheque().getBanco() != null && lancamentoParcela.getCheque().getBanco().getId() == null) {
					lancamentoParcela.getCheque().setBanco(null);
				}
				
				if(lancamentoParcela.getCheque().getBanco() != null) {
					lancamentoParcela.getCheque().setBanco(pessoaRepository.findBanco(lancamentoParcela.getCheque().getBanco().getId()));
				}
				
				
				if(lancamentoParcela.getCheque().getNome() != null && lancamentoParcela.getCheque().getNome().getId() == null) {
					lancamentoParcela.getCheque().setNome(null);
				}
				
				if(lancamentoParcela.getCheque().getNome() != null) {
					lancamentoParcela.getCheque().setNome(pessoaRepository.find(lancamentoParcela.getCheque().getNome().getId()));
				}
				lancamentoParcela.getCheque().setStatus(LancamentoStatus.LAN);
			}
			
			// Arquivo
			if(lancamentoParcela.getArquivo() != null && StringUtils.isBlank(lancamentoParcela.getArquivo().getNome())) {
				lancamentoParcela.setArquivo(null);
			}
			arquivoRepository.save(lancamentoParcela);
			
		}
		
		
		// Verifica se o status nao eh LAN
		if(!LancamentoStatus.LAN.equals(_lancamento.getStatus())) {
			isBaixaOk = false;
		}
		
		
		// Rateios
		if(lancamentoRateioList != null && !lancamentoRateioList.isEmpty()) {
			for(LancamentoRateio lancamentoRateio : lancamentoRateioList) {
				lancamentoRateio.setLancamento(_lancamento);
			}
		}
		
		
		
		
		
		// Remover
		// Remove Itens
		if(lancamentoItemDelete != null && !lancamentoItemDelete.isEmpty()) {
			for(Long id : lancamentoItemDelete) {
				LancamentoItem lancamentoItem = lancamentoItemRepository.find(id);
				if(lancamentoItem != null && !lancamentoItem.getArquivoList().isEmpty()) {
					
					List<Arquivo> arquivoList = arquivoSession.getArquivoList(lancamentoItem.getViewid());
					for(Arquivo arquivo : arquivoList) {
						arquivoRepository.delete(arquivo);
					}
					
					List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(lancamentoItem.getViewid());
					for(Arquivo arquivo : arquivoDeleteList) {
						arquivoRepository.delete(arquivo);
					}
					arquivoSession.clear(lancamentoItem.getViewid());
				}
				
			}
			lancamentoItemRepository.deleteList(lancamentoItemDelete);
		}
		
		
		// Remove Fatura
		if(lancamentoFaturaDelete != null && !lancamentoFaturaDelete.isEmpty()) {
			List<LancamentoFatura> _lancamentoFaturaList = lancamentoFaturaRepository.findAll(_lancamento.getId(), lancamentoFaturaDelete);
			if(_lancamentoFaturaList != null && !_lancamentoFaturaList.isEmpty()) {
				for(LancamentoFatura lancamentoFatura : _lancamentoFaturaList) {
					
					// LancamentoParcela
					if(lancamentoFatura.getTransferencia() == null) {
						if(lancamentoFatura.getLancamentoParcela().getBaixaItem() == null) {
							lancamentoFatura.getLancamentoParcela().setStatus(LancamentoStatus.LAN);
						} else {
							lancamentoFatura.getLancamentoParcela().setStatus(LancamentoStatus.BXD);
							lancamentoFatura.getLancamentoParcela().getBaixaItem().setStatus(LancamentoStatus.BXD);
							baixaItemRepository.save(lancamentoFatura.getLancamentoParcela().getBaixaItem());
						}
						lancamentoParcelaRepository.save(lancamentoFatura.getLancamentoParcela());
					} 
					
					// Transferencia
					else {
						lancamentoFatura.getTransferencia().setStatusOrigem(LancamentoStatus.LAN);
						if(lancamentoFatura.getTransferencia().getStatusDestino().equals(LancamentoStatus.LAN)){
							lancamentoFatura.getTransferencia().setStatus(LancamentoStatus.LAN);
						}
						transferenciaRepository.save(lancamentoFatura.getTransferencia());
					}
				
				}
				
				lancamentoFaturaRepository.deleteList(lancamentoFaturaDelete);
			}
		}
				
		
		// Remove Parcelas
		if(lancamentoParcelaDelete != null && !lancamentoParcelaDelete.isEmpty()) {
			List<LancamentoParcela> _lancamentoParcelaDeleteList = lancamentoParcelaRepository.findWithArquivo(lancamentoParcelaDelete);
			if(_lancamentoParcelaDeleteList != null && !_lancamentoParcelaDeleteList.isEmpty()) {
				
				
				for(LancamentoParcela _lancamentoParcelaDelete : _lancamentoParcelaDeleteList) {
					
					// Remove Arquivo
					Arquivo arquivo = _lancamentoParcelaDelete.getArquivo();
					arquivoRepository.delete(arquivo);
					
					// Arquivos a Deletar
					List<Arquivo> arquivoList = _lancamentoParcelaDelete.getArquivoList();
					for(Arquivo arquivoDelete : arquivoList) {
						arquivoRepository.delete(arquivoDelete);
					}
					
					List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(_lancamentoParcelaDelete.getViewid());
					for(Arquivo arquivoDelete : arquivoDeleteList) {
						arquivoRepository.delete(arquivoDelete);
					}
					arquivoSession.clear(_lancamentoParcelaDelete.getViewid());
					
					// Remove Cheque
					if(_lancamentoParcelaDelete.getCheque() != null) {
						chequeRepository.deleteIfUnique(_lancamentoParcelaDelete.getCheque());
					}
					
				}
				
				
			}
			
			
			lancamentoParcelaRepository.deleteList(lancamentoParcelaDelete);
			
		}
		
		
		// Remove Rateios
		if(lancamentoRateioDelete != null && !lancamentoRateioDelete.isEmpty()) {
			
			List<LancamentoRateio> lancamentoRateioListDelete = lancamentoRateioRepository.findAll(lancamentoRateioDelete);
			
			if(lancamentoRateioListDelete != null && !lancamentoRateioListDelete.isEmpty()) {
				for(LancamentoRateio lancamentoRateio : lancamentoRateioListDelete) {
					for(Arquivo arquivo : lancamentoRateio.getArquivoList()) {
						arquivoRepository.delete(arquivo);
					}
				}
			}
			
			lancamentoRateioRepository.deleteList(lancamentoRateioDelete);
		}
			
		
		// Salvar Lancamento
		try {
			
			
			lancamentoRepository.save(_lancamento);
			
			
			for(LancamentoItem lancamentoItem : lancamentoItemList) {
				lancamentoItem.setPerc(lancamentoItem.getTotal() / _lancamento.getTotalLancamentoItem());
				
				lancamentoItemRepository.save(lancamentoItem);
				
				
				List<Arquivo> arquivoList = arquivoSession.getArquivoList(lancamentoItem.getViewid());
				for(Arquivo arquivo : arquivoList) {
					arquivo.setLancamentoItem(lancamentoItem);
					arquivoRepository.save(arquivo);
				}
				
				List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(lancamentoItem.getViewid());
				for(Arquivo arquivo : arquivoDeleteList) {
					arquivoRepository.delete(arquivo);
				}
			}
			
			
			if(lancamentoFaturaList != null && !lancamentoFaturaList.isEmpty()) {
				for(LancamentoFatura lancamentoFatura : lancamentoFaturaList) {
					
					// Verifica se o Lancamento Parcela possui BaixaItem
					if(lancamentoFatura.getLancamentoParcela() != null) {
						if(lancamentoFatura.getLancamentoParcela().getBaixaItem() == null) {
							
							lancamentoFatura.getLancamentoParcela().setStatus(_lancamento.getStatus().equals(LancamentoStatus.LAN) ? LancamentoStatus.COM : LancamentoStatus.LAN);
							lancamentoParcelaRepository.save(lancamentoFatura.getLancamentoParcela());
						
						} else {
							
							lancamentoFatura.getLancamentoParcela().setStatus(_lancamento.getStatus().equals(LancamentoStatus.LAN) ? LancamentoStatus.COM : LancamentoStatus.BXD);
							lancamentoParcelaRepository.save(lancamentoFatura.getLancamentoParcela());
							
							lancamentoFatura.getLancamentoParcela().getBaixaItem().setStatus(_lancamento.getStatus().equals(LancamentoStatus.LAN) ? LancamentoStatus.COM : LancamentoStatus.BXD);
							baixaItemRepository.save(lancamentoFatura.getLancamentoParcela().getBaixaItem());
						
						}
					} else {
						
						lancamentoFatura.getTransferencia().setStatusOrigem(_lancamento.getStatus().equals(LancamentoStatus.LAN) ? LancamentoStatus.COM : LancamentoStatus.LAN);
						transferenciaRepository.save(lancamentoFatura.getTransferencia());
						
					}
					
					// Salva Lancamento Fatura
					lancamentoFaturaRepository.save(lancamentoFatura);
				}
			}
			
			
			// Salva Lancamento Rateio
			for(LancamentoRateio lancamentoRateio : lancamentoRateioList) {
				lancamentoRateio.setPercCalc(lancamentoRateio.getValor() / _lancamento.getTotalLancamentoRateio());
				lancamentoRateioRepository.save(lancamentoRateio);
				
				// Lista de Arquivos a Salvar
				List<Arquivo> arquivoList = arquivoSession.getArquivoList(lancamentoRateio.getViewid());
				for(Arquivo arquivo : arquivoList) {
					arquivo.setLancamentoRateio(lancamentoRateio);
					arquivoRepository.save(arquivo);
				}
				
				// Lista de Arquivos a Remover
				List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(lancamentoRateio.getViewid());
				for(Arquivo arquivo : arquivoDeleteList) {
					arquivoRepository.delete(arquivo);
				}
				arquivoSession.clear(lancamentoRateio.getViewid());
			}
			
			
			// Salva Lancamento Parcela
			for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
				lancamentoParcela.setPerc(lancamentoParcela.getValor() / _lancamento.getTotalLancamentoParcela());
				lancamentoParcelaRepository.save(lancamentoParcela);
				
				// Lista de Arquivos a Salvar
				List<Arquivo> arquivoList = arquivoSession.getArquivoList(lancamentoParcela.getViewid());
				for(Arquivo arquivo : arquivoList) {
					arquivo.setLancamentoParcela(lancamentoParcela);
					arquivoRepository.save(arquivo);
				}
				
				// Lista de Arquivos a Remover
				List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(lancamentoParcela.getViewid());
				for(Arquivo arquivo : arquivoDeleteList) {
					arquivoRepository.delete(arquivo);
				}
				arquivoSession.clear(lancamentoParcela.getViewid());
			}
			
			
			
			// Faz Baixa do Lancamento
			if(isBaixa && isBaixaOk) {
				
				
				List<LancamentoParcela> lancamentoParcelaOutros = new ArrayList<LancamentoParcela>();
				List<LancamentoParcela> lancamentoParcelaCartaoCredito = new ArrayList<LancamentoParcela>();
				for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
					
					if(lancamentoParcela.getCartaoCredito() == null) {
						lancamentoParcelaOutros.add(lancamentoParcela);
					} else {
						lancamentoParcelaCartaoCredito.add(lancamentoParcela);
					}
					
				}
				
				
				// Baixa de movimentos nao cartao de credito
				if(!lancamentoParcelaOutros.isEmpty()) {
				
					Baixa baixa = new Baixa();
					baixa.setCliente(_lancamento.getCliente());
					baixa.setData(_lancamento.getData());
					baixa.setStatus(LancamentoStatus.BXD);
					baixa.setTipo(_lancamento.getTipo());
					baixa.setCriacao(_lancamento.getCriacao());
					baixa.setUsuCriacao(_lancamento.getUsuCriacao());
									
					int seq = 0;
					List<BaixaItem> baixaItemList = new ArrayList<BaixaItem>();
					for(LancamentoParcela lancamentoParcela : lancamentoParcelaOutros) {
						if(LancamentoStatus.LAN.equals(lancamentoParcela.getStatus())  && (lancamentoParcela.getConta() != null || lancamentoParcela.getCheque() != null)) {
							
							BaixaItem baixaItem = new BaixaItem();
							baixaItem.setBaixa(baixa);
							baixaItem.setSeq(seq);
							baixaItem.setLancamentoParcela(lancamentoParcela);
							baixaItem.setMeio(lancamentoParcela.getMeio());
							baixaItem.setCartaoCredito(lancamentoParcela.getCartaoCredito());
							baixaItem.setIsLimite(lancamentoParcela.getIsLimite());
							baixaItem.setConta(lancamentoParcela.getConta());
							baixaItem.setCheque(lancamentoParcela.getCheque());
							baixaItem.setBoletoTipo(lancamentoParcela.getBoletoTipo());
							baixaItem.setBoletoNumero(lancamentoParcela.getBoletoNumero());
							baixaItem.setValor(lancamentoParcela.getValor());
							baixaItem.setDiferenca(0.0);
							baixaItem.setTipo(lancamentoParcela.getTipo());
							baixaItem.setArquivo(lancamentoParcela.getArquivo());
							baixaItem.setStatus(LancamentoStatus.BXD);
							baixaItemList.add(baixaItem);
							seq++;
						}
					}
					
					baixa.calculaTotalBaixaItem(baixaItemList);
					
					baixaRepository.save(baixa);
					baixaItemRepository.save(baixaItemList);
				}
				
				
				// Baixa de movimentos de cartao de credito
				if(!lancamentoParcelaCartaoCredito.isEmpty()) {
					
					
					for(LancamentoParcela lancamentoParcela : lancamentoParcelaCartaoCredito) {
						if(LancamentoStatus.LAN.equals(lancamentoParcela.getStatus())  && (lancamentoParcela.getCartaoCredito() != null || lancamentoParcela.getCheque() != null)) {
							
							Baixa baixa = new Baixa();
							baixa.setCliente(_lancamento.getCliente());
							baixa.setData(lancamentoParcela.getData());
							baixa.setStatus(LancamentoStatus.BXD);
							baixa.setTipo(_lancamento.getTipo());
							baixa.setCriacao(_lancamento.getCriacao());
							baixa.setUsuCriacao(_lancamento.getUsuCriacao());
							
							BaixaItem baixaItem = new BaixaItem();
							baixaItem.setBaixa(baixa);
							baixaItem.setSeq(0);
							baixaItem.setLancamentoParcela(lancamentoParcela);
							baixaItem.setMeio(lancamentoParcela.getMeio());
							baixaItem.setCartaoCredito(lancamentoParcela.getCartaoCredito());
							baixaItem.setIsLimite(lancamentoParcela.getIsLimite());
							baixaItem.setConta(lancamentoParcela.getConta());
							baixaItem.setCheque(lancamentoParcela.getCheque());
							baixaItem.setBoletoTipo(lancamentoParcela.getBoletoTipo());
							baixaItem.setBoletoNumero(lancamentoParcela.getBoletoNumero());
							baixaItem.setValor(lancamentoParcela.getValor());
							baixaItem.setDiferenca(0.0);
							baixaItem.setTipo(lancamentoParcela.getTipo());
							baixaItem.setArquivo(lancamentoParcela.getArquivo());
							baixaItem.setStatus(LancamentoStatus.BXD);
							List<BaixaItem> baixaItemList = new ArrayList<BaixaItem>();
							baixaItemList.add(baixaItem);
							
							baixa.calculaTotalBaixaItem(baixaItemList);
							
							baixaRepository.save(baixa);
							baixaItemRepository.save(baixaItemList);
							
						}
					
					}
					
				}
			
			}
			
			// Recupera Lista de Arquivos
			List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
			for(Arquivo arquivo : arquivoList) {
				arquivo.setLancamento(lancamento);
				arquivoRepository.save(arquivo).flush();
			}
			
			
			// Recupera Lista de Arquivos a Deletar
			List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
			for(Arquivo arquivo : arquivoDeleteList) {
				arquivoRepository.delete(arquivo).flush();
			}
			
			// Limpa Lista de Arquivos da Session
			arquivoSession.clear(viewid);
			
			// Atualiza limite de cartao de credito
			cartaoCreditoRepository.atualizaLimiteDisponivel();
			
			session.removeAttribute("lancamento_" + viewid);
			session.removeAttribute("lancamentoItemList_" + viewid);
			session.removeAttribute("lancamentoParcelaList_" + viewid);
			session.removeAttribute("lancamentoRateioList_" + viewid);
			session.removeAttribute("lancamentoFaturaList_" + viewid);
						
		} catch(ConstraintViolationException e) {
			e.printStackTrace();
		}
						
		result.use(Results.jsonp()).withCallback("lancamentoCadastroSuccess").withoutRoot().from(new Object[]{_lancamento, isBaixa}).serialize();
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		if(ids != null && !ids.isEmpty()) {
			
			for(Long id : ids) {
				
				Lancamento lancamento = lancamentoRepository.find(id);
				
				List<LancamentoItem> lancamentoItemList = lancamentoItemRepository.findAll(lancamento.getId());
				if(lancamentoItemList != null) {
					for(LancamentoItem lancamentoItem : lancamentoItemList) {
						List<Arquivo> arquivoList = lancamentoItem.getArquivoList();
						for(Arquivo arquivo : arquivoList) {
							arquivoRepository.delete(arquivo);
						}
					}
				}
				
				lancamentoItemRepository.delete(lancamento);
				List<LancamentoParcela> lancamentoParcelaList = lancamentoParcelaRepository.findAll(lancamento.getId());
				
				if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
					for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
						
						if(LancamentoStatus.BXD.equals(lancamentoParcela.getStatus()) || LancamentoStatus.COM.equals(lancamentoParcela.getStatus())) {
							validator.add(new I18nMessage("lancamento_" + lancamento.getId(), "lancamentoParcela." + lancamento.getTipo() + ".status." + lancamentoParcela.getStatus()));
							break;
						}
						
					}
				}
			
			}
			
			validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
			
			for(Long id : ids) {
				
				Lancamento lancamento = lancamentoRepository.find(id);
				
				if(lancamento.getArquivoDoc() != null) {
					arquivoRepository.delete(lancamento.getArquivoDoc());
					lancamento.setArquivoDoc(null);
				}
				
				
				
				List<LancamentoFatura> lancamentoFaturaList = lancamentoFaturaRepository.findAll(id);
				if(lancamentoFaturaList != null && !lancamentoFaturaList.isEmpty()) {
					for(LancamentoFatura lancamentoFatura : lancamentoFaturaList) {
						
						
						// Verifica se o Lancamento Parcela possui BaixaItem
						if(lancamentoFatura.getLancamentoParcela() != null) {
							if(lancamentoFatura.getLancamentoParcela().getBaixaItem() == null) {
								
								lancamentoFatura.getLancamentoParcela().setStatus(LancamentoStatus.LAN);
								lancamentoParcelaRepository.save(lancamentoFatura.getLancamentoParcela());
							
							} else {
								
								lancamentoFatura.getLancamentoParcela().setStatus(LancamentoStatus.BXD);
								lancamentoParcelaRepository.save(lancamentoFatura.getLancamentoParcela());
								
								lancamentoFatura.getLancamentoParcela().getBaixaItem().setStatus(LancamentoStatus.BXD);
								baixaItemRepository.save(lancamentoFatura.getLancamentoParcela().getBaixaItem());
							
							}
						}
						
						
						// Verifica se Transferencia
						if(lancamentoFatura.getTransferencia() != null) {
							lancamentoFatura.getTransferencia().setStatusOrigem(LancamentoStatus.LAN);
						}
						
						
						// Excluir Lancamento Fatura
						lancamentoFaturaRepository.delete(lancamentoFatura);
					}
					
					
				}
				
				
				// Lancamento Parcela
				List<LancamentoParcela> lancamentoParcelaList = lancamentoParcelaRepository.findAll(lancamento.getId());
				if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
					
					for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
						
						// Removento Arquivo
						if(lancamentoParcela.getArquivo() != null) {
							arquivoRepository.delete(lancamentoParcela.getArquivo());
							lancamentoParcela.setArquivo(null);
						}
						
						List<Arquivo> arquivoList = lancamentoParcela.getArquivoList();
						for(Arquivo arquivo : arquivoList) {
							arquivoRepository.delete(arquivo);
						}
						
						lancamentoParcelaRepository.delete(lancamentoParcela);
						
					}
					
					
				}
				
				lancamentoRepository.delete(id);
				
				
				cartaoCreditoRepository.atualizaLimiteDisponivel().flush();
			}
		}
		
		result.use(Results.jsonp()).withCallback("lancamentoExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
	
	@Get
	public void movimentosCartaoCredito(String viewid, LocalDate data, Long id) {
		
		CartaoCredito cartaoCredito = null;
		if(id != null) {
			cartaoCredito = cartaoCreditoRepository.find(id);
		}
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoFatura> lancamentoFaturaList = null;
		if(lancamento == null) {
			validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
		} else if(cartaoCredito == null && id != null) {
			validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notFound"));
		} else {
			
			List<Long> idsNotIn = new ArrayList<Long>();
			lancamentoFaturaList = (List<LancamentoFatura>) session.getAttribute("lancamentoFaturaList_" + viewid);
			if(lancamentoFaturaList == null) {
				lancamentoFaturaList = new ArrayList<LancamentoFatura>();
				session.setAttribute("lancamentoFaturaList_" + viewid, lancamentoFaturaList);
			}
			
			if(!lancamentoFaturaList.isEmpty()) {
				
				for(LancamentoFatura lancamentoFatura : lancamentoFaturaList) {
					idsNotIn.add(lancamentoFatura.getLancamentoParcela().getId());
				}
			
			}
			
			List<LancamentoParcela> lancamentoParcelaList = lancamentoParcelaRepository.findAll(cartaoCredito, data, idsNotIn);
			
			
			
			
			if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
				int i = 0;
				for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
					LancamentoFatura lancamentoFatura = new LancamentoFatura();
					lancamentoFatura.setSeq(i);
					lancamentoFatura.setLancamento(lancamento);
					lancamentoFatura.setLancamentoParcela(lancamentoParcela);
					lancamentoFaturaList.add(lancamentoFatura);
					i++;
				}
				
			}
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		lancamento.calculaTotalLancamentoFatura(lancamentoFaturaList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoFaturaList", lancamentoFaturaList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoFaturaList.jsp");
	}
	
	
	
	
	@Get
	public void lancamentoFatura(Boolean isTable,
			@NotNull(message = "{dataDe.notNull}")
			LocalDate dataDe, 
			@NotNull(message = "{dataAte.notNull}")
			LocalDate dataAte,
			List<Long> idsNotIn,
			List<Long> transfIdNotIn,
			@NotNull(message = "{cartaoCredito.notNull}")
			Long cartaoCredito,
			String search, 
			List<OrderBy> orderByList, 
			Integer page) {
		
		String jsp = isTable == null || !isTable ? "lancamentoFatura" : "lancamentoFatura_table";
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		if(dataDe != null && dataAte != null) {
			
			// Verificar se a dataAte eh inferior a dataDe
			if(dataAte.isBefore(dataDe)) {
				validator.add(new I18nMessage("dataAte", "dataAte.notIsBefore.dataDe"));
			}
			
			// Verifica se a diferenca entre dataDe e dataAte eh maior que 12 meses
			if(Months.monthsBetween(dataDe, dataAte).getMonths() > 12) {
				validator.add(new I18nMessage("dataAte", "dataAte.months.notMore12Mounths"));
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT lp.id, ");
		sqlBuilder.append("NULL transferencia_id, ");
		sqlBuilder.append("COALESCE(b._data, lp._data) _data, ");
		sqlBuilder.append("COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome) || ");
		sqlBuilder.append("COALESCE(' ' || CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("END || ' - ' || l.numerodoc, ' ' || CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("END, l.numerodoc, '') || COALESCE(' ' || cc.nome, '') AS descricao, ");
		sqlBuilder.append("CASE WHEN l.quantLancamentoParcela = 1 THEN '-' ELSE (lp.seq + 1) || ' / ' || l.quantLancamentoParcela END parcela, ");
		sqlBuilder.append("lp.valor, ");
		sqlBuilder.append("al.id AS arquivoDoc_id, ");
		sqlBuilder.append("al.nome AS arquivoDoc_nome, ");
		sqlBuilder.append("COALESCE(abi.id, alp.id) AS arquivo_id, ");
		sqlBuilder.append("COALESCE(abi.nome, alp.nome) AS arquivo_nome, ");
		sqlBuilder.append("comprov.id AS comprovante_id, ");
		sqlBuilder.append("comprov.nome AS comprovante_nome, ");
		sqlBuilder.append("l.cartaoCredito_id fatCartaoCredito_id, ");
		sqlBuilder.append("lcc.nome fatCartaoCredito_nome, ");
		sqlBuilder.append("COALESCE(bi.boletotipo, lp.boletotipo) AS boletotipo, ");
		sqlBuilder.append("COALESCE(bi.boletonumero, lp.boletonumero) AS boletonumero ");
		sqlBuilder.append("FROM LancamentoParcela lp ");
		sqlBuilder.append("LEFT JOIN Lancamento l ON lp.lancamento_id = l.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON l.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN Pessoa p ON l.favorecido_id = p.id ");
		sqlBuilder.append("LEFT JOIN BaixaItem bi ON lp.id = bi.lancamentoParcela_id ");
		sqlBuilder.append("LEFT JOIN Baixa b ON b.id = bi.baixa_id ");
		sqlBuilder.append("LEFT JOIN arquivo al ON al.id = l.arquivoDoc_id ");
		sqlBuilder.append("LEFT JOIN arquivo alp ON alp.id = lp.arquivo_id ");
		sqlBuilder.append("LEFT JOIN arquivo abi ON abi.id = bi.arquivo_id ");
		sqlBuilder.append("LEFT JOIN arquivo comprov ON comprov.id = bi.comprovante_id ");
		sqlBuilder.append("LEFT JOIN cartaocredito AS lcc ON l.cartaoCredito_id = lcc.id ");
		sqlBuilder.append("WHERE l.cliente_id = ?1 ");
		sqlBuilder.append("AND COALESCE(b._data, lp._data) >= ?2 ");
		sqlBuilder.append("AND COALESCE(b._data, lp._data) <= ?3 ");
		sqlBuilder.append("AND COALESCE(bi.cartaoCredito_id, lp.cartaoCredito_id) = ?4 ");
		sqlBuilder.append("AND lp.status IN('BXD') ");
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append("AND (COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome) ILIKE ?5 ");
			sqlBuilder.append("OR COALESCE(' ' || CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
			sqlBuilder.append("END || ' - ' || l.numerodoc, ' ' || CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
			sqlBuilder.append("END, l.numerodoc, '') || COALESCE(' ' || cc.nome, '') ILIKE ?5 ");
			sqlBuilder.append("OR CASE WHEN l.quantLancamentoParcela = 1 THEN '-' ELSE (lp.seq + 1) || ' / ' || l.quantLancamentoParcela END ILIKE ?5 ");
			sqlBuilder.append("OR CASE ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
			sqlBuilder.append("END ILIKE ?5 ");
			sqlBuilder.append("OR CAST(lp.valor AS VARCHAR) ILIKE ?5) ");
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			sqlBuilder.append("AND lp.id NOT IN(" + StringUtils.join(idsNotIn.toArray(), ", ")  + ")");
		}
				
		// Transferencia
		sqlBuilder.append("UNION SELECT ");
		sqlBuilder.append("NULL id, ");
		sqlBuilder.append("t.id transferencia_id, ");
		sqlBuilder.append("t._data, ");
		sqlBuilder.append("'" + localization.getMessage("transf_") + "' || ' ' || ");
		sqlBuilder.append("COALESCE(c.nome, cc.nome) || '" + localization.getMessage("p_") + "' || ");
		sqlBuilder.append("d.nome AS descricao, ");
		sqlBuilder.append("'-' parcela, ");
		sqlBuilder.append("t.valor, ");
		sqlBuilder.append("NULL AS arquivoDoc_id, ");
		sqlBuilder.append("NULL AS arquivoDoc_nome, ");
		sqlBuilder.append("a.id AS arquivo_id, ");
		sqlBuilder.append("a.nome AS arquivo_nome, ");
		sqlBuilder.append("NULL AS comprovante_id, ");
		sqlBuilder.append("NULL AS comprovante_nome, ");
		sqlBuilder.append("NULL fatCartaoCredito_id, ");
		sqlBuilder.append("NULL fatCartaoCredito_nome, ");
		sqlBuilder.append("NULL AS boletotipo, ");
		sqlBuilder.append("NULL AS boletonumero ");
		sqlBuilder.append("FROM Transferencia t ");
		sqlBuilder.append("LEFT JOIN Conta c ON t.conta_id = c.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON t.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN Cheque ch ON t.cheque_id = ch.id ");
		sqlBuilder.append("INNER JOIN Conta d ON t.destino_id = d.id ");
		sqlBuilder.append("LEFT JOIN arquivo a ON a.id = t.arquivo_id ");
		sqlBuilder.append("WHERE t.cliente_id = ?1 AND t.statusOrigem = 'LAN' ");
		sqlBuilder.append("AND t._data >= ?2 ");
		sqlBuilder.append("AND t._data <= ?3 ");
		sqlBuilder.append("AND t.cartaoCredito_id = ?4 ");
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("'" + localization.getMessage("transf_") + "' || ' ' || ");
			sqlBuilder.append("unaccent(LOWER(COALESCE(c.nome, cc.nome) || '" + localization.getMessage("p_") + "' || ");
			sqlBuilder.append("d.nome)) ILIKE ?5 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(c.nome, cc.nome))) ILIKE ?5 ");
			sqlBuilder.append("OR unaccent(LOWER(d.nome)) ILIKE ?5 ");
			sqlBuilder.append("OR unaccent(LOWER(CAST(t.valor AS VARCHAR))) ILIKE ?5 ");
			sqlBuilder.append("OR unaccent(LOWER(t.obs)) ILIKE ?5 ");
			sqlBuilder.append(")");
		}
		
		if(transfIdNotIn != null && !transfIdNotIn.isEmpty()) {
			sqlBuilder.append("AND t.id NOT IN(" + StringUtils.join(transfIdNotIn.toArray(), ", ")  + ")");
		}
				
		Query countQuery = baixaRepository.getEntityManager().createNativeQuery("SELECT COUNT(*) FROM(" + sqlBuilder.toString() + ") AS c ");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, dataDe.toDate());
		countQuery.setParameter(3, dataAte.toDate());
		countQuery.setParameter(4, cartaoCredito);
		
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(5, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
						
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
						
		Query query = baixaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, dataDe.toDate());
		query.setParameter(3, dataAte.toDate());
		query.setParameter(4, cartaoCredito);
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(5, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("idsNotIn", idsNotIn)
			.include("transfIdNotIn", transfIdNotIn);
		
		try {
			result.include("lancamentoParcelaList", query.getResultList());
		}catch(NoResultException e) {
			e.printStackTrace();
		}
		
		result.forwardTo("/WEB-INF/jsp/lancamento/" + jsp + ".jsp");
		
	}
	
	
	
	
	
	
	
	
	@Post
	public void adicionarLancamentoFatura(String viewid, Long id, List<Long> ids, List<Long> transfIds) {
		
		CartaoCredito cartaoCredito = null;
		if(id != null) {
			cartaoCredito = cartaoCreditoRepository.find(id);
		}
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoFatura> lancamentoFaturaList = null;
		if(lancamento == null) {
			validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
		} else if(cartaoCredito == null && id != null) {
			validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notFound"));
		} else {
			
			lancamentoFaturaList = (List<LancamentoFatura>) session.getAttribute("lancamentoFaturaList_" + viewid);
			
			if(lancamentoFaturaList == null) {
				lancamentoFaturaList = new ArrayList<LancamentoFatura>();
				session.setAttribute("lancamentoFaturaList_" + viewid, lancamentoFaturaList);
			}
			
			// LancamentoParcela
			if(ids != null && !ids.isEmpty()) {
				List<LancamentoParcela> lancamentoParcelaList = lancamentoParcelaRepository.findAll(cartaoCredito, ids);
				if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
					
					for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
						LancamentoFatura lancamentoFatura = new LancamentoFatura();
						lancamentoFatura.setLancamento(lancamento);
						lancamentoFatura.setLancamentoParcela(lancamentoParcela);
						lancamentoFaturaList.add(lancamentoFatura);
					}
					
				}
			}
			
			
			// Transferencia
			if(transfIds != null && !transfIds.isEmpty()) {
				List<Transferencia> transferenciaList = transferenciaRepository.findAll(cartaoCredito, transfIds);
				if(transferenciaList != null && !transferenciaList.isEmpty()) {
					
					for(Transferencia transferencia : transferenciaList) {
						LancamentoFatura lancamentoFatura = new LancamentoFatura();
						lancamentoFatura.setLancamento(lancamento);
						lancamentoFatura.setTransferencia(transferencia);
						lancamentoFaturaList.add(lancamentoFatura);
					}
					
				}
			}
			
			
			Collections.sort(lancamentoFaturaList, new Comparator<LancamentoFatura>() {
				public int compare(LancamentoFatura lf1, LancamentoFatura lf2) {
					LocalDate data1 = null;
					LocalDate data2 = null;
					
					if(lf1.getTransferencia() == null) {
						data1 = lf1.getLancamentoParcela().getData();
					} else {
						data1 = lf1.getTransferencia().getData();
					}
					
					if(lf2.getTransferencia() == null) {
						data2 = lf2.getLancamentoParcela().getData();
					} else {
						data2 = lf2.getTransferencia().getData();
					}
					
					return data1.compareTo(data2);
				}
			});
			
			for(int i = 0; i < lancamentoFaturaList.size(); i++) {
				lancamentoFaturaList.get(i).setSeq(i);
			}
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		lancamento.calculaTotalLancamentoFatura(lancamentoFaturaList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoFaturaList", lancamentoFaturaList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoFaturaList.jsp");
		
	}
	
	
	@Post
	public void removerLancamentoFatura(String viewid, Long id, List<Long> ids, List<Long> idsTranferencia) {
		
		CartaoCredito cartaoCredito = null;
		if(id != null) {
			cartaoCredito = cartaoCreditoRepository.find(id);
		}
		
		Lancamento lancamento = (Lancamento) session.getAttribute("lancamento_" + viewid);
		List<LancamentoFatura> lancamentoFaturaList = null;
		List<LancamentoFatura> _lancamentoFaturaList = new ArrayList<LancamentoFatura>();
		if(lancamento == null) {
			validator.add(new I18nMessage("lancamento", "lancamento.notFound"));
		} else if(cartaoCredito == null && id != null) {
			validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notFound"));
		} else if(ids == null) {
			validator.add(new I18nMessage("lancamentoFatura", "lancamentoFatura.remover.nothing"));
		} else {
			
			lancamentoFaturaList = (List<LancamentoFatura>) session.getAttribute("lancamentoFaturaList_" + viewid);
			
			for(LancamentoFatura lancamentoFatura : lancamentoFaturaList) {
				
				if(lancamentoFatura.getLancamentoParcela() != null) {
					if(ids != null && !ids.contains(lancamentoFatura.getLancamentoParcela().getId())) {
						_lancamentoFaturaList.add(lancamentoFatura);
					}
				}
				
				if(lancamentoFatura.getTransferencia() != null) {
					if(idsTranferencia != null && !idsTranferencia.contains(lancamentoFatura.getTransferencia().getId())) {
						_lancamentoFaturaList.add(lancamentoFatura);
					}
				}
			
			}
			
			for(int i = 0; i < _lancamentoFaturaList.size(); i++) {
				_lancamentoFaturaList.get(i).setSeq(i + 1);
			}
		
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		lancamento.calculaTotalLancamentoFatura(_lancamentoFaturaList);
		session.setAttribute("lancamentoFaturaList_" + viewid, _lancamentoFaturaList);
		
		result
			.include("lancamento", lancamento)
			.include("lancamentoFaturaList", _lancamentoFaturaList)
			.forwardTo("/WEB-INF/jsp/lancamento/lancamentoFaturaList.jsp");
		
	}
	
	
	
	@Post
	public void rapido(Lancamento lancamento, LancamentoItem lancamentoDes, LancamentoItem lancamentoRec, LancamentoParcela lancamentoParcela, boolean isBaixa, boolean isConfirmNumeroDocExists) {
	
		// Verifica se ha Lancamentos disponiveis
		if(planoRecurso.getLancamentosDisp() == 0) {
			validator.add(new I18nMessage("plano.lancamentos", "plano.lancamentos.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getLancamentos()));
		}
		
		// Verifica espaco no banco de dados
		if(planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.bancoDados", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
		
		
		LancamentoItem lancamentoItem = null;
		
		if(!validator.hasErrors()) {
			
			if(lancamento == null) {
				validator.add(new I18nMessage("lancamento", "lancamento.notNull"));
			} else {
				
				Pessoa favorecido = pessoaRepository.findOrCliente(lancamento.getFavorecido().getId());
				lancamento.setData(LocalDate.now());
				lancamento.setFavorecido(favorecido);
				lancamento.setStatus(LancamentoStatus.LAN);
				
				if(lancamento.getArquivoDoc() != null && StringUtils.isBlank(lancamento.getArquivoDoc().getNome())) {
					lancamento.setArquivoDoc(null);
				}
				
				// LancamentoItem
				List<LancamentoItem> lancamentoItemList = new ArrayList<LancamentoItem>();
				if(lancamento.getTipo() != null) {
					
					if(lancamento.getTipo().equals(ItemTipo.DES)) {
						if(lancamentoDes.getItem() != null && lancamentoDes.getItem().getId() != null) {
							lancamentoDes.setItem(itemRepository.find(lancamentoDes.getItem().getId()));
							if(lancamentoDes.getItem() == null) {
								validator.add(new I18nMessage("lancamentoItem_" + lancamento.getTipo(), "lancamentoItem." + lancamento.getTipo() + ".notFound"));
							} else {
								lancamentoDes.setQuant(1.0);
								if(lancamentoParcela != null) {
									lancamentoDes.setValor(lancamentoParcela.getValor());
									lancamentoDes.setValorUnit(lancamentoParcela.getValor());
								}
								lancamentoItemList.add(lancamentoDes);
								lancamentoItem = lancamentoDes;
							}
						}
					} else {
						if(lancamentoRec.getItem() != null && lancamentoRec.getItem().getId() != null) {
							lancamentoRec.setItem(itemRepository.find(lancamentoRec.getItem().getId()));
							if(lancamentoRec.getItem() == null) {
								validator.add(new I18nMessage("lancamentoItem_" + lancamento.getTipo(), "lancamentoItem." + lancamento.getTipo() + ".notFound"));
							} else {
								lancamentoRec.setQuant(1.0);
								if(lancamentoParcela != null) {
									lancamentoRec.setValor(lancamentoParcela.getValor());
									lancamentoRec.setValorUnit(lancamentoParcela.getValor());
								}
								lancamentoItemList.add(lancamentoRec);
								lancamentoItem = lancamentoRec;
							}
						}
					}
					
				}
				
				// LancamentoParcela
				List<LancamentoParcela> lancamentoParcelaList = new ArrayList<LancamentoParcela>();
				if(lancamentoParcela != null) {
					lancamentoParcelaList.add(lancamentoParcela);
				}
				List<LancamentoRateio> lancamentoRateioList = new ArrayList<LancamentoRateio>();
				lancamento.calculaTotal(lancamentoItemList, lancamentoParcelaList, lancamentoRateioList);
				
				// Verifica se um item foi informado
				if(lancamentoItemList.isEmpty()) {
					validator.add(new I18nMessage("lancamentoItem_" + lancamento.getTipo(), "lancamentoItem." + lancamento.getTipo() + ".selecione"));
				}
				
				// Verifica se a parcela foi informada
				if(lancamentoParcelaList.isEmpty()) {
					validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.required"));
				} else {
					
					// Verifica se a data foi informada
					if(lancamentoParcela.getData() == null) {
						validator.add(new I18nMessage("lancamentoParcela.data", "lancamentoParcela.data.notNull"));
					}
					
					// Verifica se o valor foi informado
					if(lancamentoParcela.getValor() == null) {
						validator.add(new I18nMessage("lancamentoParcela.valor", "lancamentoParcela.valor.notNull"));
					}
					
					// Verifica se o meio foi informado
					if(lancamentoParcela.getMeio() == null) {
						validator.add(new I18nMessage("lancamentoParcela.meio", "lancamentoParcela.meio.notNull"));
					} else {
						
						// Cartao de Credito
						if(lancamentoParcela.getCartaoCredito() != null && lancamentoParcela.getCartaoCredito().getId() == null) {
							lancamentoParcela.setCartaoCredito(null);
						}
						
						if(lancamentoParcela.getCartaoCredito() != null && lancamentoParcela.getCartaoCredito().getId() != null) {
							lancamentoParcela.setCartaoCredito(cartaoCreditoRepository.find(lancamentoParcela.getCartaoCredito().getId()));
							if(lancamentoParcela.getCartaoCredito() == null) {
								validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.cartaoCredito.notFound"));
							}
						}
						
						
						// Conta
						if(lancamentoParcela.getConta() != null && lancamentoParcela.getConta().getId() == null) {
							lancamentoParcela.setConta(null);
						}
						
						if(lancamentoParcela.getConta() != null && lancamentoParcela.getConta().getId() != null) {
							lancamentoParcela.setConta(contaRepository.find(lancamentoParcela.getConta().getId()));
							if(lancamentoParcela.getConta() == null) {
								validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.conta.notFound"));
							}
						}
						
						
						// Cheque
						if(lancamentoParcela.getCheque() != null && lancamentoParcela.getCheque().getNumero() == null) {
							lancamentoParcela.setCheque(null);
						}
						if(lancamentoParcela.getCheque() != null && lancamentoParcela.getCheque().getNumero() != null) {
							
							if(lancamento.getTipo().equals(ItemTipo.DES)) {
								
								// Pega informacoes do Talao
								if(lancamentoParcela.getCheque().getTalaoCheque() != null && lancamentoParcela.getCheque().getTalaoCheque().getId() != null) {
									lancamentoParcela.getCheque().setTalaoCheque(talaoChequeRepository.find(lancamentoParcela.getCheque().getTalaoCheque().getId()));
								}
								
								lancamentoParcela.getCheque().setBanco(null);
								lancamentoParcela.getCheque().setNome(null);
																
							} else {
								
								// Pega informacoes do Banco
								if(lancamentoParcela.getCheque().getBanco() != null && lancamentoParcela.getCheque().getBanco().getId() != null) {
									lancamentoParcela.getCheque().setBanco(pessoaRepository.find(lancamentoParcela.getCheque().getBanco().getId()));
								}
								
								// Pega informacoes do Nome
								if(lancamentoParcela.getCheque().getNome() != null && lancamentoParcela.getCheque().getNome().getId() != null) {
									lancamentoParcela.getCheque().setNome(pessoaRepository.find(lancamentoParcela.getCheque().getNome().getId()));
								}
								
								lancamentoParcela.getCheque().setTalaoCheque(null);
								
							}
							
						}
						
						
						// Arquivo
						if(lancamentoParcela.getArquivo() != null && StringUtils.isBlank(lancamentoParcela.getArquivo().getNome())) {
							lancamentoParcela.setArquivo(null);
						}
						
						
						
						// Verifica se eh Baixa
						if(isBaixa) {
							
							if(lancamento.getTipo().equals(ItemTipo.DES)) {
								
								switch(lancamentoParcela.getMeio()) {
								case CDB:
								case DEA:
								case DIN:
								case DOC:
								case INT:
								case TRA:
									
									// Verifica se a Conta foi selecionada
									if(lancamentoParcela.getConta() == null) {
										validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.conta.notNull"));
									}
									
									break;
								case CCR:
									
									// Verifica se o Cartao de Credito foi selecionado
									if(lancamentoParcela.getCartaoCredito() == null) {
										validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.cartaoCredito.notNull"));
									}
									
									break;
								case CHQ:
									
									// Verifica se o Cheque foi informado
									if(lancamentoParcela.getCheque() == null) {
										validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.cheque.notNull"));
									}
									
									break;
								}
																
							} else {
								
								
								switch(lancamentoParcela.getMeio()) {
								case CDB:
								case DEA:
								case DIN:
								case DOC:
								case INT:
								case TRA:
									
									// Verifica se a Conta foi selecionada
									if(lancamentoParcela.getConta() == null) {
										validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.conta.notNull"));
									}
									
									break;
								case CCR:
									
									// Verifica se o Cartao de Credito foi selecionado
									if(lancamentoParcela.getCartaoCredito() == null) {
										validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.cartaoCredito.notNull"));
									}
									
									break;
								case CHQ:
									
									// Verifica se a Conta foi selecionada
									if(lancamentoParcela.getConta() == null) {
										validator.add(new I18nMessage("lancamentoParcela", "lancamentoParcela.conta.notNull"));
									}
																		
									break;
								}
								
								
							}
							
							
						}
						
					}
					
	
					
				}
				
				// Valida lancamento
				validator.validateProperties(lancamento, "favorecido", "tipo", "tipoDoc", "dataDoc", "numeroDoc", "valorDoc", "obs");
				
				
				// Verifica se o documento eh nota fiscal e ja foi lancado
				if(DocTipo.NF.equals(lancamento.getTipoDoc()) || DocTipo.CF.equals(lancamento.getTipoDoc())) {
					
					// Verifica se o Numero da Nota ja existe
					if(!isConfirmNumeroDocExists && lancamentoRepository.existsDoc(lancamento.getId(), lancamento.getNumeroDoc(), lancamento.getFavorecido())) {
						validator.add(new I18nMessage("isConfirmNumeroDocExists", "confirm.numeroDoc." + lancamento.getTipoDoc() + ".exists", lancamento.getNumeroDoc()));
					}
					
				}
				
			}
			
			if(lancamentoItem != null) {
				lancamentoItem.setLancamento(lancamento);
				lancamentoItem.setSeq(0);
				lancamentoItem.setPerc(1.0);
			}
			
			if(lancamentoParcela != null) {
				
				lancamentoParcela.setLancamento(lancamento);
				lancamentoParcela.setSeq(0);
				lancamentoParcela.setPerc(1.0);
				lancamentoParcela.setTipo(lancamento.getTipo());
				lancamentoParcela.setStatus(LancamentoStatus.LAN);
				// Arquivo
				if(lancamentoParcela.getArquivo() != null && StringUtils.isBlank(lancamentoParcela.getArquivo().getNome())) {
					lancamentoParcela.setArquivo(null);
				}
				
			
				validator.validateProperties(lancamentoParcela, "seq", "data", "meio", "boletoTipo", "boletoNumero", "obs", "valor");
				
			}
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("lancamentoRapidoError").withoutRoot().from(validator.getErrors()).serialize();
		
		// Salva lancamento
		if(lancamento.getArquivoDoc() != null) {
			arquivoRepository.save(lancamento);
		}
		lancamentoRepository.save(lancamento);
		
		lancamentoItemRepository.save(lancamentoItem);
		if(lancamentoParcela.getArquivo() != null) {
			arquivoRepository.save(lancamentoParcela);
		}
		lancamentoParcelaRepository.save(lancamentoParcela);
		
		if(isBaixa) {
			
			Baixa baixa = new Baixa();
			baixa.setData(lancamentoParcela.getData());
			baixa.setTipo(lancamento.getTipo());
			baixa.setQuantBaixaItem(1);
			baixa.setStatus(LancamentoStatus.BXD);
			baixa.setTotalBaixaItem(lancamentoParcela.getValor());
			baixa.setTotalDiferenca(0.0);
			baixa.setTotalLancamentoParcela(lancamentoParcela.getValor());
			baixaRepository.save(baixa);
			
			BaixaItem baixaItem = new BaixaItem();
			baixaItem.setBaixa(baixa);
			baixaItem.setCartaoCredito(lancamentoParcela.getCartaoCredito());
			baixaItem.setConta(lancamentoParcela.getConta());
			baixaItem.setCheque(lancamentoParcela.getCheque());
			baixaItem.setBoletoTipo(lancamentoParcela.getBoletoTipo());
			baixaItem.setBoletoNumero(lancamentoParcela.getBoletoNumero());
			baixaItem.setArquivo(lancamentoParcela.getArquivo());
			baixaItem.setDiferenca(0.0);
			baixaItem.setIsLimite(lancamentoParcela.getIsLimite());
			baixaItem.setLancamentoParcela(lancamentoParcela);
			baixaItem.setMeio(lancamentoParcela.getMeio());
			baixaItem.setSeq(0);
			baixaItem.setStatus(LancamentoStatus.BXD);
			baixaItem.setValor(lancamentoParcela.getValor());
			baixaItem.setTipo(lancamento.getTipo());
			baixaItemRepository.save(baixaItem);
			
		}
		
		result.use(Results.jsonp()).withCallback("lancamentoRapidoSuccess").from(StringUtils.EMPTY).serialize();
	}
	
}
