package br.com.contames.controller;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isIn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.Private;
import br.com.contames.component.ArquivoSession;
import br.com.contames.component.PlanoRecurso;
import br.com.contames.component.Settings;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.Baixa;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Cheque;
import br.com.contames.entity.Conta;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.OrderByDirection;
import br.com.contames.repository.ArquivoRepository;
import br.com.contames.repository.BaixaItemRepository;
import br.com.contames.repository.BaixaRepository;
import br.com.contames.repository.CartaoCreditoRepository;
import br.com.contames.repository.ChequeRepository;
import br.com.contames.repository.CompensacaoItemRepository;
import br.com.contames.repository.ContaRepository;
import br.com.contames.repository.DescricaoRepository;
import br.com.contames.repository.LancamentoParcelaRepository;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.repository.TalaoChequeRepository;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.OrderBy;
import br.com.contames.util.StringHelper;

@Private
@Resource
public class BaixaController {

	private final BaixaRepository baixaRepository;
	private final BaixaItemRepository baixaItemRepository;
	private final ContaRepository contaRepository;
	private final PessoaRepository pessoaRepository;
	private final LancamentoParcelaRepository lancamentoParcelaRepository;
	private final DescricaoRepository descricaoRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final ArquivoRepository arquivoRepository;
	private final CompensacaoItemRepository compensacaoItemRepository;
	private final ChequeRepository chequeRepository;
	private final TalaoChequeRepository talaoChequeRepository;
	private final Result result;
	private final Validator validator; 
	private final HttpServletRequest request;
	private final HttpSession session;
	private final Localization localization;
	private final UsuarioLogged usuarioLogged;
	private final Settings settings;
	private final Environment environment;
	private final PlanoRecurso planoRecurso;
	private final ArquivoSession arquivoSession;
	
	public BaixaController(BaixaRepository baixaRepository, BaixaItemRepository baixaItemRepository, ContaRepository contaRepository, PessoaRepository pessoaRepository, LancamentoParcelaRepository lancamentoParcelaRepository, DescricaoRepository descricaoRepository, CartaoCreditoRepository cartaoCreditoRepository, ArquivoRepository arquivoRepository, CompensacaoItemRepository compensacaoItemRepository, ChequeRepository chequeRepository, TalaoChequeRepository talaoChequeRepository, Result result, Validator validator,  HttpServletRequest request, HttpSession session, Localization localization, UsuarioLogged usuarioLogged, Settings settings, Environment environment, PlanoRecurso planoRecurso, ArquivoSession arquivoSession) {
		this.baixaRepository = baixaRepository;
		this.baixaItemRepository = baixaItemRepository;
		this.contaRepository = contaRepository;
		this.pessoaRepository = pessoaRepository;
		this.lancamentoParcelaRepository = lancamentoParcelaRepository;
		this.descricaoRepository = descricaoRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.arquivoRepository = arquivoRepository;
		this.compensacaoItemRepository = compensacaoItemRepository;
		this.chequeRepository = chequeRepository;
		this.talaoChequeRepository = talaoChequeRepository;
		this.result = result;
		this.validator = validator;
		this.request = request;
		this.session = session;
		this.localization = localization;
		this.usuarioLogged = usuarioLogged;
		this.settings = settings;
		this.environment = environment;
		this.planoRecurso = planoRecurso;
		this.arquivoSession = arquivoSession;
	}
	
	
	@Get({"/lancamentos/pagamentos", "/lancamentos/recebimentos"})
	public void consultar(Boolean isTable, String search, List<OrderBy> orderByList, Integer page) {
		
		String jsp = isTable == null || !isTable ? "consultar" : "consultar_table";
		
		ItemTipo tipo = ItemTipo.DES;
		if(request.getRequestURI().startsWith("/lancamentos/recebimentos")) {
			tipo = ItemTipo.REC;
		}
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.DESC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT b.id, ");
		sqlBuilder.append("b._data, ");
		sqlBuilder.append("b.quantBaixaItem, ");
		sqlBuilder.append("b.totalLancamentoParcela, ");
		sqlBuilder.append("b.totalBaixaItem, ");
		sqlBuilder.append("b.totalDiferenca, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN b.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
		sqlBuilder.append("WHEN b.status = 'BXD' THEN '" + localization.getMessage("LancamentoStatus." + tipo + ".BXD") + "' ");
		sqlBuilder.append("WHEN b.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
		sqlBuilder.append("END status, ");
		sqlBuilder.append("b.status AS _status, ");
		sqlBuilder.append("b.obs ");
		sqlBuilder.append("FROM Baixa b ");
		sqlBuilder.append("WHERE b.cliente_id = ?1 AND b.tipo = ?2");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append(" AND (");
			sqlBuilder.append("CAST(b._data AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(b.quantBaixaItem AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(b.totalLancamentoParcela AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(b.totalBaixaItem AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR CAST(b.totalDiferenca AS VARCHAR) ILIKE ?3 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE ");
			sqlBuilder.append("WHEN b.status = 'STD' THEN '" + localization.getMessage("LancamentoStatus.STD") + "' ");
			sqlBuilder.append("WHEN b.status = 'BXD' THEN '" + localization.getMessage("LancamentoStatus." + tipo + ".BXD") + "' ");
			sqlBuilder.append("WHEN b.status = 'CAN' THEN '" + localization.getMessage("LancamentoStatus.CAN") + "' ");
			sqlBuilder.append("END)) ILIKE ?3 ");
			sqlBuilder.append(")");
		}
				
		Query countQuery = baixaRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS Baixa");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		countQuery.setParameter(2, tipo.toString());
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
						
		Query query = baixaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, tipo.toString());
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("tipo", tipo);
		
		try {
			result.include("baixaList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/baixa/" + jsp + ".jsp");
		
	}
	
	
	@Get
	public void cadastro(Long id, @NotNull(message = "{tipo.notNull}") ItemTipo tipo, String viewid) { 
		
		// Verifica espaco no banco de dados
		if(id == null && planoRecurso.getBancoDadosDisp() == 0) {
			validator.add(new I18nMessage("plano.usuarios", "plano.bancoDados.quota.atingida", planoRecurso.getPlano().getNome(), planoRecurso.getBancoDados()));
		}
		
		Baixa baixa = null;
		List<BaixaItem> baixaItemList = null;
		boolean isClosed = false;
		if(id == null) {
			
			if(viewid == null) {
				baixa = new Baixa();
				baixa.setStatus(LancamentoStatus.BXD);
				baixa.setTipo(tipo);
				baixa.setData(LocalDate.now());
				baixaItemList = new ArrayList<BaixaItem>();
				
				// Gera viewid
				viewid = StringHelper.createViewId();
				
				session.setAttribute("baixa_" + viewid, baixa);
				session.setAttribute("baixaItemList_" + viewid, baixaItemList);
			} else {
				
				baixa = (Baixa) session.getAttribute("baixa_" + viewid);
				tipo = baixa.getTipo();
				baixaItemList = (List<BaixaItem>) session.getAttribute("baixaItemList_" + viewid);
			}
			
			
				
		} else {
			
			// Gera viewid
			viewid = StringHelper.createViewId();
			
			// Carregar Baixa do Banco de Dados
			baixa = baixaRepository.find(id);
			
			if(baixa == null) {
				validator.add(new I18nMessage("baixa", "baixa.notFound"));
			} else {
				baixaItemList = baixaItemRepository.findAll(id);
				session.setAttribute("baixa_" + viewid, baixa);
				session.setAttribute("baixaItemList_" + viewid, baixaItemList);
				
				isClosed = baixaRepository.isClosed(id);
				
				arquivoSession.setAquivoList(viewid, baixa.getArquivoList());
				
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		
		result
			.include("viewid", viewid)
			.include("baixa", baixa)
			.include("tipo", tipo)
			.include("isClosed", isClosed)
			.include("baixaItemList", baixaItemList);
		
	}
	
	
	@Get
	public void lancamentoParcela(Boolean isTable,
			LocalDate dataDe, 
			@NotNull(message = "{dataAte.notNull}")
			LocalDate dataAte,
			List<Long> idsNotIn,
			@NotNull(message = "{tipo.notNull}")
			LancamentoStatus status[],
			Long cartaoCredito,
			ItemTipo tipo,
			String search, 
			List<OrderBy> orderByList, 
			Integer page) {
		
		String jsp = isTable == null || !isTable ? "lancamentoParcela" : "lancamentoParcela_table";
		
		if(dataDe != null && dataAte != null) {
			
			// Verificar se a dataAte eh inferior a dataDe
			if(dataAte.isBefore(dataDe)) {
				validator.add(new I18nMessage("dataAte", "dataAte.notIsBefore.dataDe"));
			}
			
			// Verifica se a diferenca entre dataDe e dataAte eh maior que 36 meses
			if(Months.monthsBetween(dataDe, dataAte).getMonths() > 36) {
				validator.add(new I18nMessage("dataAte", "dataAte.months.notMore36Mounths"));
			}
			
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		if(orderByList == null) { 
			orderByList = new ArrayList<OrderBy>(); 
			OrderBy orderBy = new OrderBy();
			orderBy.setColumn("_data");
			orderBy.setDirection(OrderByDirection.ASC);
			orderBy.setIndex(0);
			orderByList.add(orderBy);
		} else {
			Collections.sort(orderByList, new Comparator<OrderBy>() {
				public int compare(OrderBy orderBy1, OrderBy orderBy2) {
					return Integer.valueOf(orderBy1.getIndex()).compareTo(orderBy2.getIndex());
				}
			});
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT lp.id, ");
		sqlBuilder.append("NULL transferencia_id, ");
		sqlBuilder.append("COALESCE(ch._data, b._data, lp._data) _data, ");
		sqlBuilder.append("COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome) favorecido, ");
		sqlBuilder.append("COALESCE(CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END || ' - ' || l.numerodoc, CASE ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CF' THEN '" + localization.getMessage("DocTipo.CF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'FT' THEN '" + localization.getMessage("DocTipo.FT") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
		sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
		sqlBuilder.append("END, l.numerodoc) || COALESCE(' ' || cc.nome, '') documento, ");
		sqlBuilder.append("CASE WHEN l.quantLancamentoParcela = 1 THEN ' - ' ELSE (lp.seq + 1) || ' / ' || l.quantLancamentoParcela END parcela, ");
		sqlBuilder.append("CASE ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
		sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
		sqlBuilder.append("END || COALESCE(' ' || banco.fantasia, ' ' || banco.razaoSocial, '') || COALESCE(' ' || lpcc.nome, '') || CASE WHEN l.tipo = 'DES' THEN COALESCE(' ' || c.nome, '') ELSE '' END || COALESCE(' ' || tcc.nome, '') || COALESCE(' ' || ch.numero, '') meio, ");
		sqlBuilder.append("lp.valor, ");
		sqlBuilder.append("lp.tipo, ");
		sqlBuilder.append("alp.id AS arquivo_id, ");
		sqlBuilder.append("alp.nome AS arquivo_nome, ");
		sqlBuilder.append("al.id AS arquivoDoc_id, ");
		sqlBuilder.append("al.nome AS arquivoDoc_nome, ");
		sqlBuilder.append("l.cartaoCredito_id fatCartaoCredito_id, ");
		sqlBuilder.append("lcc.nome fatCartaoCredito_nome,");
		sqlBuilder.append("COALESCE(bi.boletotipo, lp.boletotipo) boletotipo,");
		sqlBuilder.append("COALESCE(bi.boletonumero, lp.boletonumero) boletonumero ");
		sqlBuilder.append("FROM LancamentoParcela lp ");
		sqlBuilder.append("INNER JOIN Lancamento l ON lp.lancamento_id = l.id ");
		sqlBuilder.append("INNER JOIN Pessoa p ON l.favorecido_id = p.id ");
		sqlBuilder.append("LEFT JOIN BaixaItem bi ON lp.id = bi.lancamentoParcela_id ");
		sqlBuilder.append("LEFT JOIN Baixa b ON b.id = bi.baixa_id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito cc ON l.cartaoCredito_id = cc.id ");
		sqlBuilder.append("LEFT JOIN CartaoCredito lpcc ON COALESCE(bi.cartaoCredito_id, lp.cartaoCredito_id) = lpcc.id ");
		sqlBuilder.append("LEFT JOIN Conta c ON COALESCE(bi.conta_id, lp.conta_id) = c.id ");
		sqlBuilder.append("LEFT JOIN Cheque ch ON COALESCE(bi.cheque_id, lp.cheque_id) = ch.id ");
		sqlBuilder.append("LEFT JOIN Pessoa banco ON ch.banco_id = banco.id ");
		sqlBuilder.append("LEFT JOIN TalaoCheque tc ON ch.talaoCheque_id = tc.id ");
		sqlBuilder.append("LEFT JOIN Conta tcc ON tc.conta_id = tcc.id ");
		sqlBuilder.append("LEFT JOIN arquivo AS alp ON COALESCE(bi.arquivo_id, lp.arquivo_id) = alp.id ");
		sqlBuilder.append("LEFT JOIN arquivo AS al ON l.arquivoDoc_id = al.id ");
		sqlBuilder.append("LEFT JOIN cartaocredito AS lcc ON l.cartaoCredito_id = lcc.id ");
		
		sqlBuilder.append("WHERE l.cliente_id = ?1 ");
		
		if(status.length == 0) {
			sqlBuilder.append("AND l.status = ?2 ");
			sqlBuilder.append("AND lp.status = ?2 ");
			sqlBuilder.append("AND b.id IS NULL ");
		}
		
		if(dataDe != null) {
			sqlBuilder.append("AND COALESCE(ch._data, b._data, lp._data) >= ?3 ");
		}
		
		sqlBuilder.append("AND COALESCE(ch._data, b._data, lp._data) <= ?4 ");
		sqlBuilder.append("AND l.tipo = ?5 ");
		
		if(StringUtils.isNotBlank(search)) {
			sqlBuilder.append("AND (unaccent(LOWER(COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome))) ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(COALESCE(CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'OU' THEN '" + localization.getMessage("DocTipo.OU") + "' ");
			sqlBuilder.append("END || ' - ' || l.numerodoc, CASE ");
			sqlBuilder.append("WHEN l.tipoDoc = 'NF' THEN '" + localization.getMessage("DocTipo.NF") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'CT' THEN '" + localization.getMessage("DocTipo.CT") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'RC' THEN '" + localization.getMessage("DocTipo.RC") + "' ");
			sqlBuilder.append("WHEN l.tipoDoc = 'OU' THEN '" + localization.getMessage("DocTipo.OU") + "' ");
			sqlBuilder.append("END, l.numerodoc) || COALESCE(' ' || cc.nome, ''))) ILIKE ?6 ");
			sqlBuilder.append("OR CASE WHEN l.quantLancamentoParcela = 1 THEN ' - ' ELSE (lp.seq + 1) || ' / ' || l.quantLancamentoParcela END ILIKE ?6 ");
			sqlBuilder.append("OR unaccent(LOWER(CASE ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CCR' THEN '" + localization.getMessage("MeioPagamento.CCR") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CDB' THEN '" + localization.getMessage("MeioPagamento.CDB") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DIN' THEN '" + localization.getMessage("MeioPagamento.DIN") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'CHQ' THEN '" + localization.getMessage("MeioPagamento.CHQ") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'TRA' THEN '" + localization.getMessage("MeioPagamento.TRA") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DOC' THEN '" + localization.getMessage("MeioPagamento.DOC") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'INT' THEN '" + localization.getMessage("MeioPagamento.INT") + "' ");
			sqlBuilder.append("WHEN COALESCE(bi.meio, lp.meio) = 'DEA' THEN '" + localization.getMessage("MeioPagamento.DEA") + "' ");
			sqlBuilder.append("END || COALESCE(' ' || banco.fantasia, ' ' || banco.razaoSocial, '') || COALESCE(' ' || lpcc.nome, '') || CASE WHEN l.tipo = 'DES' THEN COALESCE(' ' || c.nome, '') ELSE '' END || COALESCE(' ' || tcc.nome, '') || COALESCE(' ' || ch.numero, ''))) ILIKE ?6 ");
			sqlBuilder.append("OR CAST(lp.valor AS VARCHAR) ILIKE ?6) ");
		}
		
		if(cartaoCredito != null) {
			sqlBuilder.append("AND COALESCE(bi.cartaoCredito_id, lp.cartaoCredito_id) = ?7 ");
		}
		
		int paramIndex = 8;
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			for(int i = 0; i < idsNotIn.size(); i++) {
				sqlBuilder.append(" AND lp.id <> ?" + (i + 8));
			}
			paramIndex += idsNotIn.size();
		}
		
		if(status.length > 0) {
			sqlBuilder.append(" AND (");
			for(int i = 0; i < status.length; i++) {
				sqlBuilder.append("COALESCE(bi.status, lp.status) = ?" + (i + paramIndex));
				if(i < (status.length - 1)) {
					sqlBuilder.append(" OR ");
				}
			}
			
			sqlBuilder.append(" ) ");
		}
						
		Query countQuery = baixaRepository.getEntityManager().createNativeQuery("SELECT COUNT(id) FROM(" + sqlBuilder.toString() + ") AS LancamentoParcela");
		countQuery.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(status.length == 0) {
			countQuery.setParameter(2, LancamentoStatus.LAN.toString());
		}
		
		if(dataDe != null) {
			countQuery.setParameter(3, dataDe.toDate());
		}
		
		countQuery.setParameter(4, dataAte.toDate());
		countQuery.setParameter(5, tipo.toString());
		
		if(StringUtils.isNotBlank(search)) {
			countQuery.setParameter(6, "%" + StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		if(cartaoCredito != null) {
			countQuery.setParameter(7, cartaoCredito);
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			for(int i = 0; i < idsNotIn.size(); i++) {
				countQuery.setParameter((i + 8), idsNotIn.get(i));
			}
		}
		
		if(status.length > 0) {
			for(int i = 0; i < status.length; i++) {
				countQuery.setParameter((i + paramIndex), status[i].toString());
			}
		}
		
		Long count = (Long) countQuery.getSingleResult();
		
		float pageSize = settings.getConfiguration().getFloat("page.size");
		if(page == null) {
			page = 1;
		}
		int firstResult = page - 1;
		if(page > 1) {
			firstResult = (page - 1) * (int) pageSize;
		}
		
		if(firstResult > count) {
			firstResult = 0;
			page = 1;
		}
		
		sqlBuilder.append(" ORDER BY ");
		List<String> arrOrder = new ArrayList<String>();
		for(int i = 0; i < orderByList.size(); i++) {
			arrOrder.add(i, StringHelper.cleanSqlField(orderByList.get(i).getColumn()) + " " + orderByList.get(i).getDirection());
		}
		Object[] _arrOrder = arrOrder.toArray();
		sqlBuilder.append(StringUtils.join(_arrOrder, ", "));
						
		Query query = baixaRepository.getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente().getId());
		
		if(status.length == 0) {
			query.setParameter(2, LancamentoStatus.LAN.toString());
		}
		
		if(dataDe != null) {
			query.setParameter(3, dataDe.toDate());
		}
		
		query.setParameter(4, dataAte.toDate());
		query.setParameter(5, tipo.toString());
		
		if(StringUtils.isNotBlank(search)) {
			query.setParameter(6, "%" + StringHelper.unAccent(search.toLowerCase()) + "%");
		}
		
		if(cartaoCredito != null) {
			query.setParameter(7, cartaoCredito);
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			for(int i = 0; i < idsNotIn.size(); i++) {
				query.setParameter((i + 8), idsNotIn.get(i));
			}
		}
		
		if(status.length > 0) {
			for(int i = 0; i < status.length; i++) {
				query.setParameter((i + paramIndex), status[i].toString());
			}
		}
		
		query.setFirstResult(firstResult);
		query.setMaxResults((int) pageSize);
		
		query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
		
		int paginationSize = 0;
		if(count.intValue() > 0) {
			paginationSize = (int) (Math.ceil(count / pageSize));
		}
		
		if(paginationSize == 0) {
			paginationSize = 1;
		}
				
		result
			.include("orderByList", orderByList)
			.include("page", page)
			.include("count", count)
			.include("pageSize", pageSize)
			.include("firstResult", firstResult)
			.include("paginationSize", paginationSize)
			.include("idsNotIn", idsNotIn)
			.include("status", status)
			.include("tipo", tipo);
		
		try {
			result.include("lancamentoParcelaList", query.getResultList());
		}catch(NoResultException e) {
			
		}
		
		result.forwardTo("/WEB-INF/jsp/baixa/" + jsp + ".jsp");
		
	}
	
	
	@Post
	public void adicionarLancamentoParcela(String viewid, @NotNull(message = "{ids.notNull}") List<Long> ids) {
		Baixa baixa = null;
		List<BaixaItem> baixaItemList = null;
		if(ids != null && !ids.isEmpty()) {
			List<LancamentoParcela> lancamentoParcelaList = lancamentoParcelaRepository.findAllLan(ids);
			
			if(lancamentoParcelaList == null || lancamentoParcelaList.isEmpty()) {
				validator.add(new I18nMessage("lancamentoParcelaList", "lancamentoParcelaList.notFound"));
			} else {
			
				baixa = (Baixa) session.getAttribute("baixa_" + viewid);
				baixaItemList = (List<BaixaItem>) session.getAttribute("baixaItemList_" + viewid);
				
				for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
					BaixaItem baixaItem = new BaixaItem();
					baixaItem.setLancamentoParcela(lancamentoParcela);
					baixaItem.setSeq(baixaItemList.size());
					baixaItem.setMeio(lancamentoParcela.getMeio());
					baixaItem.setCartaoCredito(lancamentoParcela.getCartaoCredito());
					baixaItem.setIsLimite(lancamentoParcela.getIsLimite());
					baixaItem.setConta(lancamentoParcela.getConta());
					baixaItem.setCheque(lancamentoParcela.getCheque());
					baixaItem.setBoletoTipo(lancamentoParcela.getBoletoTipo());
					baixaItem.setBoletoNumero(lancamentoParcela.getBoletoNumero());
					baixaItem.setValor(lancamentoParcela.getValor());
					baixaItem.setTipo(lancamentoParcela.getTipo());
					baixaItemList.add(baixaItem);
				}
				
				baixa.calculaTotalBaixaItem(baixaItemList);
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		result
			.include("baixa", baixa)
			.include("baixaItemList", baixaItemList)
			.forwardTo("/WEB-INF/jsp/baixa/baixaItemList.jsp");
		
	}
	
	@Get
	public void baixaItem(@NotNull(message = "{index.notNull}") Integer index, @NotBlank(message = "{viewid.notBlank}") String viewid) {
		
		if(index != null && viewid != null) {
			
			Baixa baixa = (Baixa) session.getAttribute("baixa_" + viewid);
			if(baixa == null) {
				validator.add(new I18nMessage("baixa", "baixa.notFound"));
			} else {
				
				List<BaixaItem> baixaItemList = (List<BaixaItem>) session.getAttribute("baixaItemList_" + viewid);
				
				BaixaItem baixaItem = selectFirst(baixaItemList, having(on(BaixaItem.class).getSeq(), equalTo(index)));
				if(baixaItem == null) {
					validator.add(new I18nMessage("baixaItem", "baixaItem.notFound"));
				} else {
					
					arquivoSession.setAquivoList(baixaItem.getViewid(), baixaItem.getArquivoList());
					
					result
						.include("baixaItem", baixaItem)
						.include("viewid", viewid)
						.include("motivoList", descricaoRepository.findAllActive(DescricaoTipo.MOTIVO_DIFERENCA));
				}
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		
	}

	
	@Post
	public void baixaItem(@NotNull(message = "{baixaItem.notNull}") BaixaItem baixaItem, @NotBlank(message = "{viewid.notBlank}") String viewid) {
		
		Baixa baixa = (Baixa) session.getAttribute("baixa_" + viewid);
		List<BaixaItem> baixaItemList = (List<BaixaItem>) session.getAttribute("baixaItemList_" + viewid);
		
		if(baixaItem != null && StringUtils.isNotBlank(viewid)) {
			
			if(baixa == null) {
				validator.add(new I18nMessage("baixa", "baixa.notFound"));
			} else {
			
				BaixaItem _baixaItem = selectFirst(baixaItemList, having(on(BaixaItem.class).getSeq(), equalTo(baixaItem.getSeq())));
				if(_baixaItem == null) {
					validator.add(new I18nMessage("baixaItem", "baixaItem.notFound"));
				} else {
					baixaItem.setLancamentoParcela(_baixaItem.getLancamentoParcela());
					validateBaixaItem(baixaItem);
				}
				
				// Verifica se nao ha erros
				if(!validator.hasErrors()) {
					// Calculo da diferenca
					Double valor = _baixaItem.getLancamentoParcela().getValor();
					Double valorPago = baixaItem.getValor();
					Double diferenca = valorPago - valor;
					_baixaItem.setDiferenca(diferenca);
					
					_baixaItem.setMeio(baixaItem.getMeio());
					_baixaItem.setCartaoCredito(baixaItem.getCartaoCredito());
					_baixaItem.setIsLimite(baixaItem.getIsLimite());
					_baixaItem.setConta(baixaItem.getConta());
					_baixaItem.setCheque(baixaItem.getCheque());
					_baixaItem.setBoletoTipo(baixaItem.getBoletoTipo());
					_baixaItem.setBoletoNumero(baixaItem.getBoletoNumero());
					_baixaItem.setArquivo(baixaItem.getArquivo());
					_baixaItem.setValor(baixaItem.getValor());
					_baixaItem.setComprovante(baixaItem.getComprovante());
					_baixaItem.setMotivo(baixaItem.getMotivo());
					_baixaItem.setObs(baixaItem.getObs());
					
					baixa.calculaTotalBaixaItem(baixaItemList);
				}
			
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		
		result
			.include("baixa", baixa)
			.include("viewid", viewid)
			.include("baixaItemList", baixaItemList)
			.forwardTo("/WEB-INF/jsp/baixa/baixaItemList.jsp");
		
		
	}
	
	
	@Post
	public void removerBaixaItem(List<Integer> indexs, String viewid) {
		
		Baixa baixa = (Baixa) session.getAttribute("baixa_" + viewid);
		List<BaixaItem> baixaItemList = (List<BaixaItem>) session.getAttribute("baixaItemList_" + viewid);
		
		List<BaixaItem> deleteBaixaItemList = select(baixaItemList, having(on(BaixaItem.class).getSeq(), isIn(indexs)));
		for(BaixaItem baixaItem : deleteBaixaItemList) {
			if(baixaItem.getId() != null && LancamentoStatus.COM.equals(baixaItem.getStatus())) {
				validator.add(new I18nMessage("baixaItem_" + baixaItem.getId(), "baixaItem." + baixaItem.getTipo() + ".compensacaoItem.exists"));
			}
		}
		
		validator.onErrorUse(Results.json()).withoutRoot().from(validator.getErrors()).serialize();
		
		baixaItemList.removeAll(deleteBaixaItemList);
		
		for(int i = 0; i < baixaItemList.size(); i++) {
			baixaItemList.get(i).setSeq(i);
		}
		
		baixa.calculaTotalBaixaItem(baixaItemList);
		
		result
			.include("baixa", baixa)
			.include("baixaItemList", baixaItemList)
			.forwardTo("/WEB-INF/jsp/baixa/baixaItemList.jsp");
	
	}
	
	
	@Post
	public void cadastro(@NotNull(message = "{baixa.notNull}") Baixa baixa, @NotBlank(message = "{viewid.notBlank}") String viewid, List<Long> baixaItemDelete) {
		
		// Recupera baixa da session
		Baixa _baixa = (Baixa) session.getAttribute("baixa_" + viewid);
		
		// Situacao da Baixa
		boolean isClosed = baixaRepository.isClosed(_baixa.getId());
		if(isClosed) {
			baixa.setStatus(_baixa.getStatus());
		}
		
		// Recupera Lista de Movimentos
		List<BaixaItem> baixaItemList = (List<BaixaItem>) session.getAttribute("baixaItemList_" + viewid);
		
		Map<Cheque, List<BaixaItem>> baixaItemChequeMapList = new HashMap<Cheque, List<BaixaItem>>();
		
		
		// Verifica se a baixa nao eh null e se o viewid nao e vazio
		if(baixa != null && StringUtils.isNotBlank(viewid)) {
			
			// Verificar se existe itens
			if(baixaItemList == null || baixaItemList.isEmpty()) {
				
				validator.add(new I18nMessage("baixa.baixaItemList", "baixa.baixaItemList.notNull"));
			
			} else {
				
				_baixa.setData(baixa.getData());
				_baixa.setStatus(baixa.getStatus());
				_baixa.setObs(baixa.getObs());
				
				validator.validateProperties(baixa, "data", "status", "obs");
				
				for(BaixaItem baixaItem : baixaItemList) {
					
					
					// Status nos itens
					if(baixaItem.getStatus() == null || !LancamentoStatus.COM.equals(baixaItem.getStatus())) {
						baixaItem.setStatus(baixa.getStatus());
					}
					
					
					// Verifica se o status nao eh COM
					if(!LancamentoStatus.COM.equals(baixaItem.getStatus())) {
					
						
						// Calculo da diferenca
						Double valor = baixaItem.getLancamentoParcela().getValor();
						Double valorPago = baixaItem.getValor();
						Double diferenca = valorPago - valor;
						baixaItem.setDiferenca(diferenca);
						
						
						validateBaixaItem(baixaItem);
						
						
						// Verifica se o meio eh cheque
						if(baixaItem.getCheque() != null) {
							
							baixaItem.getCheque().setStatus(baixaItem.getStatus());
							
							if(baixaItem.getLancamentoParcela().getTipo().equals(ItemTipo.DES)) {
								
								baixaItem.getCheque().setTalaoCheque(talaoChequeRepository.find(baixaItem.getCheque().getTalaoCheque().getId()));
								baixaItem.getCheque().setBanco(null);
								baixaItem.getCheque().setNome(null);
								
							} else {
								
								baixaItem.getCheque().setTalaoCheque(null);
								
								if(baixaItem.getCheque().getBanco() != null && baixaItem.getCheque().getBanco().getId() == null) {
									baixaItem.getCheque().setBanco(null);
								}
								
								if(baixaItem.getCheque().getNome() != null && baixaItem.getCheque().getNome().getId() == null) {
									baixaItem.getCheque().setNome(null);
								}
							}
						
							if(baixaItem.getCheque().getTalaoCheque() != null) {
								Cheque cheque = chequeRepository.findBy(baixaItem.getCheque().getNumero(), baixaItem.getCheque().getTalaoCheque());
								if(cheque == null) {
									chequeRepository.save(baixaItem.getCheque());
								} else {
									baixaItem.setCheque(cheque);
								}
								
								if(!baixaItemChequeMapList.containsKey(baixaItem.getCheque())) {
									baixaItemChequeMapList.put(baixaItem.getCheque(), new ArrayList<BaixaItem>());
								}
								
								baixaItemChequeMapList.get(baixaItem.getCheque()).add(baixaItem);
							
							} else {
								chequeRepository.save(baixaItem.getCheque());
							}
							
							// Verifica se o cheque eh diferente
							if(baixaItem.getId() == null) {
								if(baixaItem.getLancamentoParcela().getCheque() != null && !baixaItem.getCheque().equals(baixaItem.getLancamentoParcela().getCheque())) {
									baixaItem.getLancamentoParcela().getCheque().setStatus(LancamentoStatus.CAN);
									chequeRepository.save(baixaItem.getLancamentoParcela().getCheque());
								}
							} else {
								BaixaItem _baixaItem = baixaItemRepository.find(baixaItem.getId());
								if(!baixaItem.getCheque().equals(_baixaItem.getCheque())) {
									_baixaItem.getCheque().setStatus(LancamentoStatus.CAN);
									chequeRepository.save(_baixaItem.getCheque());
								}
							
							}
							
						}
					}
					
					// Verifica se o Status nao eh LAN
					if(baixaItem.getStatus().equals(LancamentoStatus.BXD)) {
						if(LancamentoStatus.STD.equals(baixaItem.getLancamentoParcela().getStatus()) || LancamentoStatus.CAN.equals(baixaItem.getLancamentoParcela().getStatus())) {
							validator.add(new I18nMessage("lancamentoParcela_" + baixaItem.getLancamentoParcela().getId(), "baixaItem.lancamentoParcela.status." + baixaItem.getLancamentoParcela().getStatus() + "." + baixaItem.getLancamentoParcela().getTipo() + ".invalid"));
						}
					}
										
					// Verifica se ha arquivo anexado
					if(baixaItem.getArquivo() != null && StringUtils.isBlank(baixaItem.getArquivo().getNome())) {
						baixaItem.setArquivo(null);
					}
					
					if(baixaItem.getComprovante() != null && StringUtils.isBlank(baixaItem.getComprovante().getNome())) {
						baixaItem.setComprovante(null);
					}
					
					arquivoRepository.save(baixaItem);
				
				}
			
			}
			
			// Verifica Saldo das Contas e Cartoes de Crédito
			Map<Conta, Double> valorConta = new HashMap<Conta, Double>();
			Map<CartaoCredito, Double> valorCartaoCredito = new HashMap<CartaoCredito, Double>();
			for(BaixaItem baixaItem : baixaItemList) {
				
				// Verifica se a parcela foi paga com a conta
				if(baixaItem.getConta() != null && baixaItem.getConta().getId() != null) {
					
					// Verifica se a conta nao existe no hash
					if(!valorConta.containsKey(baixaItem.getConta())) {
						valorConta.put(baixaItem.getConta(), 0.0);
					}
					
					Double valor = valorConta.get(baixaItem.getConta());
					valor += baixaItem.getValor();
					valorConta.put(baixaItem.getConta(), valor);
				}
				
				// Verifica se a parcela foi paga com o cartao de credito
				if(baixaItem.getCartaoCredito() != null && baixaItem.getCartaoCredito().getId() != null && BooleanUtils.isTrue(baixaItem.getIsLimite())) {
					
					// Verifica se a conta nao existe no hash
					if(!valorCartaoCredito.containsKey(baixaItem.getCartaoCredito())) {
						valorCartaoCredito.put(baixaItem.getCartaoCredito(), 0.0);
					}
					
					Double valor = valorCartaoCredito.get(baixaItem.getCartaoCredito());
					valor += baixaItem.getValor();
					valorCartaoCredito.put(baixaItem.getCartaoCredito(), valor);
				}
			
			}
			
			
			// Verifica saldo das contas
			for(Conta conta : valorConta.keySet()) {
				
				Double valor = valorConta.get(conta);
				
				if(valor > conta.getSaldoUtilizavel()) {
					if(!BooleanUtils.toBoolean(request.getParameter("isConfirmContaSaldoInsuficiente_" + conta.getId()))) {
						validator.add(new I18nMessage("isConfirmContaSaldoInsuficiente_" + conta.getId(), "confirm.conta.saldoInsuficiente", conta.getNome()));
					}
				}
				
			}
			
			// Verifica limite dos cartoes de credito
			for(CartaoCredito cartaoCredito : valorCartaoCredito.keySet()) {
				
				Double valor = valorCartaoCredito.get(cartaoCredito);
				
				if(valor > cartaoCredito.getLimiteDisponivel()) {
					if(!BooleanUtils.toBoolean(request.getParameter("isConfirmCartaoCreditoLimiteInsuficiente_" + cartaoCredito.getId()))) {
						validator.add(new I18nMessage("isConfirmCartaoCreditoLimiteInsuficiente_" + cartaoCredito.getId(), "confirm.cartaoCredito.limiteInsuficiente", cartaoCredito.getNome()));
					}
				}
				
			}
			
			
		}
		
		validator.onErrorUse(Results.jsonp()).withCallback("baixaCadastroError").withoutRoot().from(validator.getErrors()).serialize();
		
		// Remove arquivo em parcelas deletadas
		if(baixaItemDelete != null && !baixaItemDelete.isEmpty()) {
			
			List<BaixaItem> _baixaItemDeleteList = baixaItemRepository.findAll(baixaItemDelete);
			
			if(_baixaItemDeleteList != null && !_baixaItemDeleteList.isEmpty()) {
				
				for(BaixaItem _baixaItemDelete : _baixaItemDeleteList) {
					
					
					if(LancamentoStatus.COM.equals(_baixaItemDelete.getStatus())) { continue; }
					
					// Verifica se possui arquivo
					if(_baixaItemDelete.getArquivo() != null) {
						if(!_baixaItemDelete.getArquivo().equals(_baixaItemDelete.getLancamentoParcela().getArquivo())) {
							Arquivo arquivo = _baixaItemDelete.getArquivo();
							arquivoRepository.delete(arquivo);
						}
					}
				
					// Verifica se possui comprovante
					if(_baixaItemDelete.getComprovante() != null) {
						Arquivo comprovante = _baixaItemDelete.getComprovante();
						arquivoRepository.delete(comprovante);
					}
					
					// Verifica se possui cheque
					if(_baixaItemDelete.getCheque() != null && !_baixaItemDelete.getCheque().equals(_baixaItemDelete.getLancamentoParcela().getCheque())) {
						_baixaItemDelete.getCheque().setStatus(LancamentoStatus.CAN);
						chequeRepository.save(_baixaItemDelete.getCheque());
					}
					
					for(Arquivo arquivo : _baixaItemDelete.getArquivoList()) {
						arquivoRepository.delete(arquivo);
					}
					
				}
			}
		}
		
		
		
		_baixa.calculaTotalBaixaItem(baixaItemList);
		baixaRepository.save(_baixa);
		
		// Recupera Lista de Arquivo para Salvar
		List<Arquivo> arquivoList = arquivoSession.getArquivoList(viewid);
		if(arquivoList != null) {
			for(Arquivo arquivo : arquivoList) {
				arquivo.setBaixa(_baixa);
				arquivoRepository.save(arquivo);
			}
		}
		
		// Recupera Lista de Arquivo para Deletar
		List<Arquivo> arquivoDeleteList = arquivoSession.getArquivoDeleteList(viewid);
		if(arquivoDeleteList != null) {
			for(Arquivo arquivo : arquivoDeleteList) {
				arquivoRepository.delete(arquivo);
			}
		}
		arquivoSession.clear(viewid);
		
		
		for(BaixaItem baixaItem : baixaItemList) {
			baixaItem.setBaixa(_baixa);
			baixaItemRepository.save(baixaItem);
			
			// Recupera Lista de Arquivos para Salvar
			arquivoList = arquivoSession.getArquivoList(baixaItem.getViewid());
			for(Arquivo arquivo : arquivoList) {
				arquivo.setBaixaItem(baixaItem);
				arquivoRepository.save(arquivo);
			}
			
			// Recupera Lista de Arquivos para Deletar
			arquivoDeleteList = arquivoSession.getArquivoDeleteList(baixaItem.getViewid());
			for(Arquivo arquivo : arquivoDeleteList) {
				arquivoRepository.delete(arquivo);
			}
			
			arquivoSession.clear(baixaItem.getViewid());
		}
		
		cartaoCreditoRepository.atualizaLimiteDisponivel();
		
		chequeRepository.updateValor(baixaItemChequeMapList);
								
		session.removeAttribute("baixa_" + viewid);
		session.removeAttribute("baixaItemList_" + viewid);
				
		result.use(Results.jsonp()).withCallback("baixaCadastroSuccess").withoutRoot().from(_baixa).exclude("cliente").serialize();
		
	}
	
	
	private void validateBaixaItem(BaixaItem baixaItem) {
		
		// Verifica baixaItem
		validator.validateProperties(baixaItem, "meio", "boletoTipo", "boletoNumero", "valor");
		
		if(!validator.hasErrors()) {
			
			// Verifica se o cartao foi informado
			if(baixaItem.getCartaoCredito() != null && baixaItem.getCartaoCredito().getId() == null) {
				baixaItem.setCartaoCredito(null);
			}
			
			// Verifica se a conta foi informada
			if(baixaItem.getConta() != null && baixaItem.getConta().getId() == null) {
				baixaItem.setConta(null);
			}
			
			// Verifica se o cheque foi informado
			if(baixaItem.getCheque() != null && baixaItem.getCheque().getNumero() == null) {
				baixaItem.setCheque(null);
			}
			
			// Verifica se o boleto foi informado
			if(StringUtils.isBlank(baixaItem.getBoletoNumero())) {
				baixaItem.setBoletoTipo(null);
				baixaItem.setBoletoNumero(null);
			}
			
			// Verifica arquivo
			if(baixaItem.getArquivo() != null && StringUtils.isBlank(baixaItem.getArquivo().getNome())) {
				baixaItem.setArquivo(null);
			}
			
			// Verifica comprovante
			if(baixaItem.getComprovante() != null && StringUtils.isBlank(baixaItem.getComprovante().getNome())) {
				baixaItem.setComprovante(null);
			}
			
			if(baixaItem.getMotivo() != null && baixaItem.getMotivo().getId() == null) {
				baixaItem.setMotivo(null);
			}
		
			// Verifica se eh despesa ou receita
			if(baixaItem.getLancamentoParcela().getTipo().equals(ItemTipo.DES)) {
				
				switch(baixaItem.getMeio()) {
				case CCR:
					
					// Verifica se o cartao eh null
					if(baixaItem.getCartaoCredito() == null) {
						validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notNull"));
					} 
					
					// Verifica se o cartao eh valido
					else {
						try {
							baixaItem.setCartaoCredito(cartaoCreditoRepository.find(baixaItem.getCartaoCredito().getId()));
						} catch(NoResultException e) {
							validator.add(new I18nMessage("cartaoCredito", "cartaoCredito.notFound"));
						}
					}
					
					baixaItem.setConta(null);
					baixaItem.setCheque(null);
					
					break;
				case CHQ:
					
					// Verifica se o cheque eh null
					if(baixaItem.getCheque() == null) {
						validator.add(new I18nMessage("cheque", "cheque.notNull"));
					}
					
					break;
				default:
					
					// Verifica se a conta eh null
					if(baixaItem.getConta() == null) {
						validator.add(new I18nMessage("conta", "conta.notNull"));
					} else {
						baixaItem.setConta(contaRepository.find(baixaItem.getConta().getId()));
						if(baixaItem.getConta() == null) {
							validator.add(new I18nMessage("conta", "conta.notFound"));
						}
					}
					baixaItem.setCartaoCredito(null);
					baixaItem.setCheque(null);
				}
				
			} else {
				
				// Verifica se a conta eh null
				if(baixaItem.getConta() == null) {
					validator.add(new I18nMessage("conta", "conta.notNull"));
				} else {
					baixaItem.setConta(contaRepository.find(baixaItem.getConta().getId()));
					if(baixaItem.getConta() == null) {
						validator.add(new I18nMessage("conta", "conta.notFound"));
					}
				}
				
			}
			
			
			// Verifica motivo
			if(baixaItem.getMotivo() != null) {
				try {
					baixaItem.setMotivo(descricaoRepository.find(baixaItem.getMotivo().getId()));
				}catch(NoResultException e) {
					validator.add(new I18nMessage("motivo", "motivo.notFound"));
				}
			}
									
		}
	}
	
	
	@Post
	public void excluir(List<Long> ids) {
		
		if(ids != null && !ids.isEmpty()) {
			
			List<Baixa> baixaList = baixaRepository.findAll(ids);
			if(baixaList != null) {
				for(Baixa baixa : baixaList) {
					for(Arquivo arquivo : baixa.getArquivoList()) {
						arquivoRepository.delete(arquivo);
					}
				}
			}
			
			List<BaixaItem> baixaItemList = baixaItemRepository.findAllByBaixaId(ids);
			
			if(baixaItemList != null && !baixaItemList.isEmpty()) {
				
				for(BaixaItem baixaItem : baixaItemList) {
					
					if(LancamentoStatus.COM.equals(baixaItem.getStatus())) {
						validator.add(new I18nMessage("baixa_" + baixaItem.getBaixa().getId(), "baixaItem." + baixaItem.getTipo() + ".status." + baixaItem.getStatus()));
					} 
					
					for(Arquivo arquivo : baixaItem.getArquivoList()) {
						arquivoRepository.delete(arquivo);
					}
					
				}
							
				validator.onErrorUse(Results.jsonp()).withCallback("error").withoutRoot().from(validator.getErrors()).serialize();
			
				for(BaixaItem baixaItem : baixaItemList) {
					if(!LancamentoStatus.COM.equals(baixaItem.getStatus())) {
						
						if(baixaItem.getArquivo() != null && !baixaItem.getArquivo().equals(baixaItem.getLancamentoParcela().getArquivo())) {
							Arquivo arquivo = baixaItem.getArquivo();
							arquivoRepository.delete(baixaItem);
						}
					
						if(baixaItem.getComprovante() != null) {
							Arquivo comprovante = baixaItem.getComprovante();
							arquivoRepository.delete(comprovante);
						}
						
						if(baixaItem.getCheque() != null) {
							baixaItem.getCheque().setStatus(baixaItem.getCheque().equals(baixaItem.getLancamentoParcela().getCheque()) ? LancamentoStatus.LAN : LancamentoStatus.CAN);
							chequeRepository.save(baixaItem.getCheque());
						}
						
					}
				}
				
				baixaRepository.deleteList(ids).flush();
				
				cartaoCreditoRepository.atualizaLimiteDisponivel();
			
			}
		}
				
		result.use(Results.jsonp()).withCallback("baixaExcluirSuccess").withoutRoot().from(ids).serialize();
	}
	
}