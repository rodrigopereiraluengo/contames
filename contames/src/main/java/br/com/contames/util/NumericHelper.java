package br.com.contames.util;

import org.apache.commons.math3.util.Precision;


public class NumericHelper {

	public static Double around(Double value) {
		return around(value, 2);
	}
	
	public static Double around(Double value, Integer decimalPlaces) {
		if(value != null) {
			return Precision.round(value, decimalPlaces);
		}
		return null;
	}
	
}
