package br.com.contames.util;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;


public class Teste1 {

	public static void main(String[] args) throws ClientProtocolException, IOException {
		//recurring_payment_profile_cancel();
		//recurring_payment();
		
		System.out.println(Request.Put("http://loja.com.br/api/banco/999")
    	.bodyForm(
    		Form.form()
    		.add("status",  "INA")
    		.build()
		)
    	.execute()
    	.returnContent().asString());;
		
		
	}
	
	public static void recurring_payment_profile_created() throws ClientProtocolException, IOException {
				
		Request.Post("http://localhost:8080/assinatura/ipn")
	    	.bodyForm(
	    		Form.form()
	    		.add("residence_country",  "BR")
	    		.add("product_name",  "A cobran� ser�realizada automaticamente a cada m�!")
	    		.add("time_created",  "18:17:34 Jul 02, 2015 PDT")
	    		.add("next_payment_date",  "03:00:00 Aug 03, 2015 PDT")
	    		.add("outstanding_balance",  "0.00")
	    		.add("verify_sign",  "AEMlgVg..JxHIdHVF5H122jkyk0fA4MC2pFrUMKJAtQqaJd8di4ucf4m")
	    		.add("amount",  "19.99")
	    		.add("first_name",  "Test")
	    		.add("payer_id",  "S5X4EMBA399SQ")
	    		.add("shipping",  "0.00")
	    		.add("payer_email",  "paypal-buyer@areaform.com")
	    		.add("period_type",  "Regular")
	    		.add("receiver_email",  "paypal@areaform.com")
	    		.add("notify_version",  "3.8")
	    		.add("currency_code",  "BRL")
	    		.add("business",  "paypal@areaform.com")
	    		.add("txn_type",  "recurring_payment_profile_created")
	    		.add("test_ipn",  "1")
	    		.add("payer_status",  "verified")
	    		.add("initial_payment_amount",  "0.00")
	    		.add("charset",  "windows-1252")
	    		.add("product_type",  "1")
	    		.add("amount_per_cycle",  "19.99")
	    		.add("ipn_track_id",  "47f59db7eb60")
	    		.add("recurring_payment_id",  "I-95U0E42B7C1Y")
	    		.add("tax",  "0.00")
	    		.add("payment_cycle",  "Monthly")
	    		.add("last_name",  "Buyer")
	    		.add("profile_status",  "Active")
	    		.build())
	    .execute().returnContent();
		
	}
	
	public static void recurring_payment() throws ClientProtocolException, IOException {
		
		Request.Post("http://localhost:8080/assinatura/ipn")
	    .bodyForm(
	    		Form.form()
	    		.add("residence_country",  "BR")
	    		.add("time_created",  "18:17:34 Jul 02, 2015 PDT")
	    		.add("product_name",  "A cobran� ser�realizada automaticamente a cada m�!")
	    		.add("next_payment_date",  "03:00:00 Aug 03, 2015 PDT")
	    		.add("outstanding_balance",  "0.00")
	    		.add("verify_sign",  "AEMlgVg..JxHIdHVF5H122jkyk0fA4MC2pFrUMKJAtQqaJd8di4ucf4m")
	    		.add("payment_status",  "Completed")
	    		.add("business",  "paypal@areaform.com")
	    		.add("transaction_subject",  "A cobran� ser�realizada automaticamente a cada m�!")
	    		.add("protection_eligibility",  "Ineligible")
	    		.add("amount",  "19.99")
	    		.add("shipping",  "0.00")
	    		.add("payer_id",  "S5X4EMBA399SQ")
	    		.add("first_name",  "Test")
	    		.add("payer_email",  "paypal-buyer@areaform.com")
	    		.add("mc_fee",  "1.08")
	    		.add("txn_id",  "1JK62806EB022204J")
	    		.add("receiver_email",  "paypal@areaform.com")
	    		.add("period_type",  "Regular")
	    		.add("notify_version",  "3.8")
	    		.add("txn_type",  "recurring_payment")
	    		.add("currency_code",  "BRL")
	    		.add("test_ipn",  "1")
	    		.add("mc_currency",  "BRL")
	    		.add("mc_gross",  "19.99")
	    		.add("payer_status",  "verified")
	    		.add("payment_date",  "05:10:38 Jul 03, 2015 PDT")
	    		.add("payment_fee",  "")
	    		.add("initial_payment_amount",  "0.00")
	    		.add("charset",  "windows-1252")
	    		.add("product_type",  "1")
	    		.add("payment_gross",  "")
	    		.add("amount_per_cycle",  "19.99")
	    		.add("ipn_track_id",  "47f59db7eb60")
	    		.add("recurring_payment_id",  "I-95U0E42B7C1Y")
	    		.add("tax",  "0.00")
	    		.add("payment_cycle",  "Monthly")
	    		.add("last_name",  "Buyer")
	    		.add("profile_status",  "Active")
	    		.add("payment_type",  "instant")
	    		.add("receiver_id",  "SKR8532YZUPAN")
	    		.build())
	    .execute().returnContent();
		
	}
	
	
	public static void recurring_payment_profile_cancel() throws ClientProtocolException, IOException {
		
		Request.Post("http://localhost:8080/assinatura/ipn")
	    .bodyForm(
	    		Form.form()
	    		.add("residence_country",  "US")
	    		.add("product_name",  "Assinatura Plano 2")
	    		.add("time_created",  "11:14:55 Jul 09, 2015 PDT")
	    		.add("next_payment_date",  "N/A")
	    		.add("outstanding_balance",  "0.00")
	    		.add("verify_sign",  "AFcWxV21C7fd0v3bYYYRCpSSRl31AShQ3N3Mb0nrey2QT0S69yzzGmeg")
	    		.add("amount",  "19.99")
	    		.add("first_name",  "Test")
	    		.add("payer_id",  "AZ6LEB44L3PW6")
	    		.add("shipping",  "0.00")
	    		.add("payer_email",  "paypal-buyer@areaform.com")
	    		.add("period_type",  "Regular")
	    		.add("receiver_email",  "paypal@areaform.com")
	    		.add("notify_version",  "3.8")
	    		.add("currency_code",  "BRL")
	    		.add("txn_type",  "recurring_payment_profile_cancel")
	    		.add("test_ipn",  "1")
	    		.add("payer_status",  "verified")
	    		.add("initial_payment_amount",  "0.00")
	    		.add("charset",  "windows-1252")
	    		.add("product_type",  "1")
	    		.add("amount_per_cycle",  "19.99")
	    		.add("ipn_track_id",  "8e243364c1f08")
	    		.add("recurring_payment_id",  "I-W8KC2UWMNJNB")
	    		.add("tax",  "0.00")
	    		.add("payment_cycle",  "Monthly")
	    		.add("last_name",  "Buyer")
	    		.add("profile_status",  "Cancelled")
	    		.build())
	    .execute().returnContent();
		
	}
	
	
	public static void recurring_payment_skipped() throws ClientProtocolException, IOException {
		
		Request.Post("http://localhost:8080/assinatura/ipn")
	    .bodyForm(
	    		Form.form()
	    		.add("residence_country",  "BR")
	    		.add("product_name",  "A cobranï¿½ serï¿½realizada automaticamente a cada mï¿½!")
	    		.add("time_created",  "11:10:30 Jul 03, 2015 PDT")
	    		.add("next_payment_date",  "N/A")
	    		.add("outstanding_balance",  "0.00")
	    		.add("verify_sign",  "AElqXDTjs0kn010Be5VV-ETeph2aABSZnOTTswy-yT4mQEX6sn3xpwOg")
	    		.add("amount",  "19.99")
	    		.add("receiver_id",  "SKR8532YZUPAN")
	    		.add("first_name",  "Test")
	    		.add("payer_id",  "S5X4EMBA399SQ")
	    		.add("shipping",  "0.00")
	    		.add("payer_email",  "paypal-buyer@areaform.com")
	    		.add("period_type",  "Regular")
	    		.add("receiver_email",  "paypal@areaform.com")
	    		.add("notify_version",  "3.8")
	    		.add("currency_code",  "BRL")
	    		.add("txn_type",  "recurring_payment_profile_cancel")
	    		.add("test_ipn",  "1")
	    		.add("payer_status",  "verified")
	    		.add("initial_payment_amount",  "0.00")
	    		.add("charset",  "windows-1252")
	    		.add("product_type",  "1")
	    		.add("amount_per_cycle",  "19.99")
	    		.add("ipn_track_id",  "e8bfb529c06a")
	    		.add("recurring_payment_id",  "I-95U0E42B7C1Y")
	    		.add("tax",  "0.00")
	    		.add("payment_cycle",  "Monthly")
	    		.add("last_name",  "Buyer")
	    		.add("profile_status",  "Cancelled")
	    		.build())
	    .execute().returnContent();
		
	}

}
