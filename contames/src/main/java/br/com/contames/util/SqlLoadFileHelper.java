package br.com.contames.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class SqlLoadFileHelper {
	
	static final String PATH = "C:/Users/Rodrigo Luengo/git/contames/contames/src/main/resources";
	
	public static void main(String[] args) throws IOException {
		List<String> lines = new ArrayList<>();
        
		File load = new File(PATH + "/load.sql");
        if(load.exists()) {
        	load.delete();
        }
        String[] files = {
    		"constraint_criacao_alteracao.sql",
    		"constraint.sql",
    		"dados_padrao.sql",
    		"dados.sql",
    		"fn_baixaitem_delete.sql",
    		"fn_cartaocredito_baixaitem.sql",
    		"fn_cartaocredito_compensacaoitem.sql",
    		"fn_cartaocredito_lancamentoparcela.sql",
    		"fn_cartaocredito_transferencia.sql",
    		"fn_classificacao.sql",
    		"fn_compensacao_delete.sql",
    		"fn_compensacaoitem_delete.sql",
    		"fn_dataDiaUtil.sql",
    		"fn_diaVencimento.sql",
    		"fn_lancAuto.sql",
    		"fn_qtdeLancamentos.sql",
    		"fn_tablesSize.sql",
    		"last_day.sql",
    		"minDataCompensacao.sql",
    		"vw_BaixaItem.sql",
    		"vw_ClassificacaoItem.sql",
    		"vw_CompensacaoItem_Item.sql",
    		"vw_CompensacaoItem_Transferencia.sql",
    		"vw_CompensacaoItem.sql",
    		"vw_LancamentoParcela.sql",
    		"vw_Transferencia.sql",
    		"vw_Relatorio_Data.sql",
    		"vw_Relatorio_Favorecido.sql",
    		"vw_Relatorio_Rateio.sql",
    		"vw_Relatorio_Classificacao.sql"
		};
        for(String fileName : files) {
        	lines.add(StringUtils.normalizeSpace(StringUtils.replace(StringUtils.replace(FileUtils.readFileToString(new File(PATH + "/sql/" + fileName)), "\n", " "), "\r", " ")));
    	}
        
        FileUtils.writeStringToFile(load, StringUtils.join(lines.toArray(), "\n"));
        
        System.out.printf("Arquivo \"%s\" gerado com sucesso!", load.getName());
	}
}
