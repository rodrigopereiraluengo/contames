package br.com.contames.util;

import br.com.contames.enumeration.OrderByDirection;

public class OrderBy {

	private String column;
	
	private OrderByDirection direction;
	
	private Integer index;

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public OrderByDirection getDirection() {
		return direction;
	}

	public void setDirection(OrderByDirection diretion) {
		this.direction = diretion;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
}
