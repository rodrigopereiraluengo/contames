package br.com.contames.util;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.text.ParseException;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;


public class StringHelper {

	public static String decodeCharset(String str)
	{
		str = (String) ObjectUtils.defaultIfNull(str, "");
		try {
			return new String(str.getBytes("UTF-8"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return str;
		}
	}
	
	public static String unAccent(String s) {
		if(s != null && !s.isEmpty()) {
			String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
			Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
			return pattern.matcher(temp).replaceAll("");
		}
		return s;
	}
	
	public static String toJavaMask(String javaScriptMask) {
		return StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(javaScriptMask, "9", "#"), "z", "L"), "Z", "U"), "a", "?"), "@", "*");
	}
	
	public static String javascriptMask(String javaScriptMask, String value) throws ParseException {
		MaskFormatter formatter = new MaskFormatter(StringHelper.toJavaMask(javaScriptMask));
		formatter.setValueContainsLiteralCharacters(false);
		return (String) formatter.valueToString(value);
	}
	
	public static String javascriptUnMask(String javaScriptMask, String value) throws ParseException {
		MaskFormatter formatter = new MaskFormatter(StringHelper.toJavaMask(javaScriptMask));
		formatter.setValueContainsLiteralCharacters(false);
		return (String) formatter.stringToValue(value);
	}
	
	public static String cleanSqlField(String val) {
		return StringHelper.unAccent(val).replaceAll("[^a-zA-Z0-9_]", "");
	}
	
	
	public static String upperCase(String val) {
		if(val == null){
			return null;
		} else {
			return val.toUpperCase();
		}
	}
	
	public static String createViewId() {
		String viewid = new String(Base64.encodeBase64((String.valueOf(new DateTime().toDate().getTime()) + new Random().nextInt(1000)).getBytes()));
		return cleanSqlField(viewid);
	}
	
}
