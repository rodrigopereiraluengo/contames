package br.com.contames.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.view.JasperViewer;

import org.joda.time.LocalDateTime;







public class Teste {

	public static void main(String[] args) throws IOException, JRException, SQLException {
		
		/*StringBuilder constraint = new StringBuilder();
		
		String[] tabelas = {
				"arquivo", 
				"baixa", 
				"cartaocredito", 
				"cheque", 
				"compensacao", 
				"conta", 
				"descricao", 
				"item", 
				"lancamento",
				"pessoa",
				"talaocheque",
				"transferencia"}; 
		
		for(String tabela : tabelas) {
			constraint.append("ALTER TABLE " + StringUtils.capitalise(tabela) + " DROP CONSTRAINT fk_" + tabela + "_usualteracao_id;\n");
			constraint.append("ALTER TABLE " + StringUtils.capitalise(tabela) + " ADD CONSTRAINT fk_" + tabela + "_usualteracao_id FOREIGN KEY (usualteracao_id)");
			constraint.append("REFERENCES pessoa (id) MATCH SIMPLE ");
			constraint.append("ON UPDATE NO ACTION ON DELETE SET NULL;\n\n");
			
			constraint.append("ALTER TABLE " + StringUtils.capitalise(tabela) + " DROP CONSTRAINT fk_" + tabela + "_usucriacao_id;\n");
	    	constraint.append("ALTER TABLE " + StringUtils.capitalise(tabela) + " ADD CONSTRAINT fk_" + tabela + "_usucriacao_id FOREIGN KEY (usucriacao_id) ");
	    	constraint.append("REFERENCES pessoa (id) MATCH SIMPLE ");
	    	constraint.append("ON UPDATE NO ACTION ON DELETE SET NULL;\n\n");
		}
		
		System.out.println(constraint);*/
		
		//List<String> list = new ArrayList<String>();
		
		String jrxmlFileName = "C:/Users/Rodrigo Luengo/RelatorioData.jrxml";
		String jasperFileName = "C:/Users/Rodrigo Luengo/RelatorioData.jasper";
		String pdfFileName = "C:/Users/Rodrigo Luengo/Documents/RelatorioData_" + LocalDateTime.now().toString("yyyy_MM_dd_HH_mm_ss") + ".pdf";
	
		JasperCompileManager.compileReportToFile(jrxmlFileName);
		HashMap parameters = new HashMap();
		
		Locale locale = Locale.forLanguageTag("pt-BR");
		ResourceBundle bundle = ResourceBundle.getBundle("messages", locale);
		
		parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, bundle);
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		   
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");
		EntityManager entityManager = factory.createEntityManager();
		entityManager.getTransaction().begin();
		Connection connection = entityManager.unwrap(Connection.class);
		
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT _data, favorecido, valor FROM vw_Relatorio_Data");
		ResultSet resultSet = preparedStatement.executeQuery();
		
		//Query query = entityManager.createNativeQuery("SELECT _data, favorecido FROM vw_Relatorio_Data");
		
		
		// Generate jasper print
		JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperFileName, parameters, new JRResultSetDataSource(resultSet));
		JasperExportManager.exportReportToPdfFile(jprint, pdfFileName);
		JasperViewer jv = new JasperViewer(jprint);
		
	    //jv.setVisible(true);
		   
		   // Export pdf file
	    InputStream inputStream = new ByteArrayInputStream(JasperRunManager.runReportToPdf(jasperFileName, parameters, new JRResultSetDataSource(resultSet)));
	    
	    System.out.println(inputStream);
	    
	    File file = new File("/WEB-INF");
	    
	    System.out.println(file.getAbsolutePath());
	    System.out.println(file.getAbsolutePath());
	    
		   //
	    entityManager.getTransaction().commit();
	    entityManager.close();
		factory.close();
	}
	

}
