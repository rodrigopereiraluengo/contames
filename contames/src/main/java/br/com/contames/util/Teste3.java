package br.com.contames.util;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.MediaType;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;


public class Teste3 {

	public static void main(String[] args) throws ClientProtocolException, IOException, ClientHandlerException, UniformInterfaceException, JSONException {
		
		//String[] emails = StringUtils.split(FileUtils.readFileToString(new File("C:/Users/Rodrigo Luengo/Documents/results_as.txt")), "\n");
		
		//for(String email : emails) {
			
		// Enviar e-mail
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", "key-2iqmf1yy9p09mge7qhu1blep57h3rfu4"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/contames.com.br/messages");
		FormDataMultiPart form = new FormDataMultiPart();
		form.field("from", "contames.com.br <email@contames.com.br>");
		//form.field("to", email.replaceAll("\\r\\n|\\r|\\n", ""));
		form.field("to", "ropelue@gmail.com");
		form.field("to", "ropelue@yahoo.com.br");
		form.field("subject", "Contas a pagar e receber");
		form.field("o:campaign", "first");
		form.field("recipient-variables", "{\"ropelue@gmail.com\": {\"first\":\"Bob\", \"id\":1}, \"ropelue@yahoo.com.br\": {\"first\":\"Alice\", \"id\": 2}}");
		form.field("html", "<html><body style=\"text-align: center; font-family: Tahoma; background-color: #dadada\"><img src=\"cid:email_1.jpg\"><br/><img src=\"cid:email_2.jpg\"><br/><br/><br/><br/><center>Saiba mais em <strong><a href=\"http://www.contames.com.br\">www.contames.com.br</a></strong></center><br/><br/></body></html>");
		
		File jpgFile = new File("C:/Users/Rodrigo Luengo/Documents/Email campaign/email_1.jpg");
		form.bodyPart(new FileDataBodyPart("inline", jpgFile, MediaType.APPLICATION_OCTET_STREAM_TYPE));
		
		File jpgFile2 = new File("C:/Users/Rodrigo Luengo/Documents/Email campaign/email_2.jpg");
		form.bodyPart(new FileDataBodyPart("inline", jpgFile2, MediaType.APPLICATION_OCTET_STREAM_TYPE));
		
		ClientResponse response = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, form);
		System.out.println(response.toString());
		
		
		// 
		/*Client client = new Client();
		client.addFilter(new HTTPBasicAuthFilter("api", "pubkey-5ogiflzbnjrljiky49qxsiozqef5jxp7"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/address/validate");
		MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
		queryParams.add("address", "foo@mailgun.net");
		ClientResponse response = webResource.queryParams(queryParams).get(ClientResponse.class);
		
		System.out.println(response.getEntityTag().getValue());
			
			//String enviados = FileUtils.readFileToString(new File("C:/Users/Rodrigo Luengo/Documents/enviados.txt"));
			
			//enviados += email + ",\n";
			
			//FileUtils.writeStringToFile(new File("C:/Users/Rodrigo Luengo/Documents/enviados.txt"), enviados);
			
		//}*/
		
		
		//String email = FileUtils.readFileToString(new File("C:/Users/Rodrigo Luengo/Documents/results_as.txt")).replaceAll("\\r\\n|\\r|\\n", "','");
		
		//System.out.println(email);
		
		//
        // Create InternetAddress object and validated the supplied
        // address which is this case is an email address.
        
       
        
		/*String url = "http://my-addr.com/free-email-verification-tool/verify-email-address/reverse-email-lookup/verify_email.php?mail=ropelue@gmail.com&x=18&y=14";

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);
		
		// add header
		get.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");

		HttpResponse response = client.execute(get);
		System.out.println("Response Code : " 
	                + response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
		        new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}*/
		
		
	       
		/*ClientResponse response = GetEvents();
		JSONObject json = new JSONObject(response.getEntity(String.class));
		JSONArray results = json.getJSONArray("items");
		for(int i = 0; i < results.length(); i++) {
			try {
				System.out.println(((JSONObject) results.get(i)).getJSONObject("message").getJSONObject("headers").getString("to"));
			} catch(Exception e) {
				
			}
    	   
		}*/
		/*Long millis = Long.decode("1443668400000");
		LocalDateTime date = new LocalDateTime(millis);
		System.out.println(date.toDateTime());*/
		
	}
	
	public static ClientResponse GetLogs() {
	       Client client = new Client();
	       client.addFilter(new HTTPBasicAuthFilter("api",
	                       "key-2iqmf1yy9p09mge7qhu1blep57h3rfu4"));
	       WebResource webResource =
	               client.resource("https://api.mailgun.net/v3/contames.com.br/log");
	       MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
	       //queryParams.add("skip", 50);
	      // queryParams.add("limit", 1);
	       return webResource.queryParams(queryParams).get(ClientResponse.class);
	}
	
	public static ClientResponse GetBounces() {
		 Client client = new Client();
		 client.addFilter(new HTTPBasicAuthFilter("api", "key-2iqmf1yy9p09mge7qhu1blep57h3rfu4"));
		 WebResource webResource = client.resource("https://api.mailgun.net/v3/contames.com.br/bounces");
		 MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
		 queryParams.add("limit", 500);
		 return webResource.queryParams(queryParams).get(ClientResponse.class);
	}
	

	
	public static ClientResponse GetStats() {
	       Client client = new Client();
	       client.addFilter(new HTTPBasicAuthFilter("api",
	                       "key-2iqmf1yy9p09mge7qhu1blep57h3rfu4"));
	       WebResource webResource =
	               client.resource("https://api.mailgun.net/v3/contames.com.br" +
	                               "/stats");
	       MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
	       queryParams.add("event", "dropped");
	       //queryParams.add("event", "opened");
	       //queryParams.add("skip", 1);
	       queryParams.add("limit", 200);
	       return webResource.queryParams(queryParams).get(ClientResponse.class);
	}
	
	public static ClientResponse GetEvents() {
		 Client client = new Client();
		 client.addFilter(new HTTPBasicAuthFilter("api", "key-2iqmf1yy9p09mge7qhu1blep57h3rfu4"));
		 WebResource webResource = client.resource("https://api.mailgun.net/v3/contames.com.br/events");
		 MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
		 queryParams.add("limit", 300);
		 queryParams.add("event", "delivered");
		 /*queryParams.add("event", "failed-temporary");
		 queryParams.add("event", "complained");
		 queryParams.add("event", "rejected");*/
		 return webResource.queryParams(queryParams).get(ClientResponse.class);
	}


}