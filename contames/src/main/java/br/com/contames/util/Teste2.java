package br.com.contames.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Chart;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.charts.AxisCrosses;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.apache.poi.ss.usermodel.charts.ChartAxis;
import org.apache.poi.ss.usermodel.charts.ChartDataSource;
import org.apache.poi.ss.usermodel.charts.ChartLegend;
import org.apache.poi.ss.usermodel.charts.DataSources;
import org.apache.poi.ss.usermodel.charts.LegendPosition;
import org.apache.poi.ss.usermodel.charts.LineChartData;
import org.apache.poi.ss.usermodel.charts.ScatterChartData;
import org.apache.poi.ss.usermodel.charts.ValueAxis;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class Teste2 {

	public static void main(String[] args) throws IOException {
		FilenameUtils.isExtension("", new String[]{""});
		System.out.println(FileUtils.byteCountToDisplaySize(10000));
		
	}
	
	private static void template() throws IOException {
		
		XSSFWorkbook workbook = new XSSFWorkbook("C:/Users/Rodrigo Luengo/Desktop/workbook_change.xlsx");
		XSSFSheet sheet = workbook.getSheetAt(0);
		
		XSSFDrawing drawing = sheet.createDrawingPatriarch();
		
		sheet.getRow(0).getCell(1).setCellValue("TESTE");
		sheet.createRow(7).createCell(0).setCellValue("Jul");
		sheet.getRow(7).createCell(1).setCellValue(150);
		sheet.getRow(7).createCell(2).setCellValue(250);
		Chart chart = drawing.getCharts().get(0);
		
		Name rangeCell = workbook.getName("Data");
		rangeCell.setRefersToFormula("Chart!$A$2:$A$8");

		//drawing.getCharts().get(0).getCTChart().addNewTitle();
		
		// Write the output to a file
        FileOutputStream fileOut = new FileOutputStream("C:/Users/Rodrigo Luengo/Desktop/change.xlsx");
        workbook.write(fileOut);
        fileOut.close();
	}

	private static void teste() throws FileNotFoundException, IOException {
		final String arquivo_nome = "C:/Users/Rodrigo Luengo/Desktop/planilha_1.xlsx";
			
		XSSFWorkbook workbook = new XSSFWorkbook(); 
				
		XSSFSheet spreadsheet = workbook.createSheet("Relatório por Data");
		spreadsheet.addMergedRegion(new CellRangeAddress(0,0,0,7 ));
		
		// Titulo
		XSSFRow rowTitulo = spreadsheet.createRow(0);
		rowTitulo.setHeightInPoints(52);
		CellStyle cellStyleTitulo = workbook.createCellStyle();
		XSSFFont fontTitulo = workbook.createFont();
		fontTitulo.setBold(true);
		fontTitulo.setFontName("SansSerif");
		fontTitulo.setFontHeightInPoints((short) 20);
		cellStyleTitulo.setFont(fontTitulo);
		cellStyleTitulo.setAlignment(CellStyle.ALIGN_CENTER);
		Cell cellTitulo = rowTitulo.createCell(0);
		cellTitulo.setCellStyle(cellStyleTitulo);
		cellTitulo.setCellValue("RELATÓRIO POR DATA");
		
		
		
		
		
		// Titulos
		XSSFRow rowTitulos = spreadsheet.createRow(1);
		rowTitulos.setHeightInPoints(20);
		
		XSSFCellStyle cellStyleTitulos = workbook.createCellStyle();
		XSSFFont fontTitulos = workbook.createFont();
		fontTitulos.setBold(true);
		fontTitulos.setFontName("SansSerif");
		fontTitulos.setFontHeightInPoints((short) 8);
		cellStyleTitulos.setFont(fontTitulos);
		cellStyleTitulos.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyleTitulos.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyleTitulos.setBorderBottom((short) 1);
		cellStyleTitulos.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTitulos.setBorderTop((short) 1);
		cellStyleTitulos.setTopBorderColor(HSSFColor.BLACK.index);
		//cellStyleTitulos.setFillBackgroundColor(HSSFColor.BLUE.index);
		//cellStyleTitulos.setFillForegroundColor(HSSFColor.BLUE.index);
		
		cellStyleTitulos.setFillForegroundColor(new XSSFColor(new java.awt.Color(240, 240, 240)));
		cellStyleTitulos.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		
		// Data
		Cell cellTitulos = rowTitulos.createCell(0);
		cellTitulos.setCellStyle(cellStyleTitulos);
		cellTitulos.setCellValue("Data");
		
		// Descricao
		cellTitulos = rowTitulos.createCell(1);
		cellTitulos.setCellStyle(cellStyleTitulos);
		cellTitulos.setCellValue("Descrição");
		
		
		//XSSFDrawing xlsx_drawing = spreadsheet.createDrawingPatriarch();
		
		//XSSFClientAnchor anchor = xlsx_drawing.createAnchor(0, 0, 0, 0, 0, 5, 10, 15);
        /* Create the chart object based on the anchor point */
        //XSSFChart my_line_chart = xlsx_drawing.createChart(anchor);
        
       // XSSFChartLegend legend = my_line_chart.getOrCreateLegend();
       // legend.setPosition(LegendPosition.BOTTOM);     
        /* Create data for the chart */
        //LineChartData data = my_line_chart.getChartDataFactory().createLineChartData(); 
		
		FileOutputStream out = new FileOutputStream(new	File(arquivo_nome));
		//write operation workbook using file out object 
		workbook.write(out);
		out.close();
		
		
		System.out.println(arquivo_nome + " written successfully");
	}
	
	public static void chart() throws IOException {
		 	Workbook wb = new XSSFWorkbook();
	        Sheet sheet = wb.createSheet("Sheet 1");
	        final int NUM_OF_ROWS = 3;
	        final int NUM_OF_COLUMNS = 10;

	        // Create a row and put some cells in it. Rows are 0 based.
	        Row row;
	        Cell cell;
	        for (int rowIndex = 0; rowIndex < NUM_OF_ROWS; rowIndex++) {
	            row = sheet.createRow((short) rowIndex);
	            for (int colIndex = 0; colIndex < NUM_OF_COLUMNS; colIndex++) {
	                cell = row.createCell((short) colIndex);
	                cell.setCellValue(colIndex * (rowIndex + 1));
	            }
	        }

	        Drawing drawing = sheet.createDrawingPatriarch();
	        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 5, 10, 15);

	        Chart chart = drawing.createChart(anchor);
	        ChartLegend legend = chart.getOrCreateLegend();
	        legend.setPosition(LegendPosition.TOP_RIGHT);

	        ScatterChartData data = chart.getChartDataFactory().createScatterChartData();

	        ValueAxis bottomAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.BOTTOM);
	        ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
	        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

	        ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(0, 0, 0, NUM_OF_COLUMNS - 1));
	        ChartDataSource<Number> ys1 = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(1, 1, 0, NUM_OF_COLUMNS - 1));
	        ChartDataSource<Number> ys2 = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(2, 2, 0, NUM_OF_COLUMNS - 1));


	        data.addSerie(xs, ys1);
	        data.addSerie(xs, ys2);

	        chart.plot(data, bottomAxis, leftAxis);

	        // Write the output to a file
	        FileOutputStream fileOut = new FileOutputStream("C:/Users/Rodrigo Luengo/Desktop/chart.xlsx");
	        wb.write(fileOut);
	        fileOut.close();
	    }
	

		private static void linechart() throws IOException {
			 Workbook wb = new XSSFWorkbook();
		        Sheet sheet = wb.createSheet("linechart");
		        final int NUM_OF_ROWS = 3;
		        final int NUM_OF_COLUMNS = 10;

		        // Create a row and put some cells in it. Rows are 0 based.
		        Row row;
		        Cell cell;
		        for (int rowIndex = 0; rowIndex < NUM_OF_ROWS; rowIndex++) {
		            row = sheet.createRow((short) rowIndex);
		            for (int colIndex = 0; colIndex < NUM_OF_COLUMNS; colIndex++) {
		                cell = row.createCell((short) colIndex);
		                cell.setCellValue(colIndex * (rowIndex + 1));
		            }
		        }

		        Drawing drawing = sheet.createDrawingPatriarch();
		        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 5, 10, 15);

		        Chart chart = drawing.createChart(anchor);
		        ChartLegend legend = chart.getOrCreateLegend();
		        legend.setPosition(LegendPosition.TOP_RIGHT);

		        LineChartData data = chart.getChartDataFactory().createLineChartData();

		        // Use a category axis for the bottom axis.
		        ChartAxis bottomAxis = chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
		        ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
		        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

		        ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(0, 0, 0, NUM_OF_COLUMNS - 1));
		        ChartDataSource<Number> ys1 = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(1, 1, 0, NUM_OF_COLUMNS - 1));
		        ChartDataSource<Number> ys2 = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(2, 2, 0, NUM_OF_COLUMNS - 1));


		        data.addSeries(xs, ys1);
		        data.addSeries(xs, ys2);

		        chart.plot(data, bottomAxis, leftAxis);

		        // Write the output to a file
		        FileOutputStream fileOut = new FileOutputStream("C:/Users/Rodrigo Luengo/Desktop/ooxml-line-chart.xlsx");
		        wb.write(fileOut);
		        fileOut.close();
		}
	
}
