package br.com.contames.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Utf8ToAnsi {

	public static void main(String[] args) throws IOException {
		String origem = args[0];
		String destino = args[0];
		Files.write(Paths.get(destino), Charset.forName("windows-1252").encode(Charset.forName("UTF-8").decode(ByteBuffer.wrap(Files.readAllBytes(Paths.get(origem))))).array());
	}
	
	public static void teste() throws IOException {
		FileInputStream fis = null;
		InputStreamReader isr =null;
	      String s;
	      
	      try {
	         // new input stream reader is created 
	         fis = new FileInputStream("C:/Users/Rodrigo Luengo/Documents/original_1");
	         isr = new InputStreamReader(fis);
	         
	         // the name of the character encoding returned
	         s=isr.getEncoding();
	         System.out.print("Character Encoding: "+s);
	         if(1==1) {
	        	 return;
	         }
	      } catch (Exception e) {
	      
	         // print error
	         System.out.print("The stream is already closed");
	      } finally {
	         
	         // closes the stream and releases resources associated
	         if(fis!=null)
	            fis.close();
	         if(isr!=null)
	            isr.close();
	      }   
	      System.out.println("@@@@@@@");
	}
	
}
