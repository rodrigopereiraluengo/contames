package br.com.contames.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.contames.entity.Cheque;

public class JpaTest {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");
		EntityManager entityManager = factory.createEntityManager();
		
		List<Cheque> chequeList = entityManager.createQuery("SELECT DISTINCT c FROM Cheque c JOIN c.talaoCheque tc LEFT JOIN c.lancamentoParcela lp LEFT JOIN c.transferencia t JOIN FETCH c.baixaItemList").getResultList();
		
		for(Cheque cheque : chequeList) {
			System.out.println(cheque.getNumero());
		}
		
		entityManager.close();
		factory.close();
	}
	
}
