package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.entity.LancamentoRateio;
import br.com.contames.session.UsuarioLogged;

@Component
public class LancamentoRateioRepository extends Repository<LancamentoRateio, Long> {
	
	private final UsuarioLogged usuarioLogged;
	
	public LancamentoRateioRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(LancamentoRateio.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}

	public List<LancamentoRateio> findAll(Long id) {
		TypedQuery<LancamentoRateio> typedQuery = entityManager.createQuery("SELECT lr FROM LancamentoRateio lr WHERE lr.lancamento.id = ?1 ORDER BY lr.seq", LancamentoRateio.class);
		typedQuery.setParameter(1, id);
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	public List<LancamentoRateio> findAllWithCartaoCredito(Long lancamentoId, List<Long> ids) {
		TypedQuery<LancamentoRateio> typedQuery = entityManager.createQuery("SELECT lr FROM LancamentoRateio lr WHERE lr.lancamento.id = ?1 AND lr.id IN ?2 AND lr.lancamento.cliente = ?3 AND lr.cartaoCredito IS NOT NULL", LancamentoRateio.class);
		typedQuery.setParameter(1, lancamentoId);
		typedQuery.setParameter(2, ids);
		typedQuery.setParameter(3, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	@Override
	public LancamentoRateioRepository save(LancamentoRateio lancamentoRateio) {
		
		if(lancamentoRateio.getId() == null) {
			
			entityManager.persist(lancamentoRateio);
		
		} else {
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<LancamentoRateio> update = cb.createCriteriaUpdate(LancamentoRateio.class);
			Root<LancamentoRateio> root = update.from(LancamentoRateio.class);
			
			update.set("tipo", lancamentoRateio.getTipo());
			update.set("pessoa", lancamentoRateio.getPessoa());
			update.set("rateio", lancamentoRateio.getRateio());
			update.set("seq", lancamentoRateio.getSeq());
			update.set("perc", lancamentoRateio.getPerc());
			update.set("valor", lancamentoRateio.getValor());
			update.set("percCalc", lancamentoRateio.getPercCalc());
			update.where(cb.equal(root.get("id"), lancamentoRateio.getId()));
			entityManager.createQuery(update).executeUpdate();
		}
		
		return this;
	}
	
	
	public LancamentoRateioRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM LancamentoRateio lr WHERE lr.id IN ?1 AND lr.lancamento.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}

	public List<LancamentoRateio> findAll(List<Long> ids) {
		TypedQuery<LancamentoRateio> typedQuery = entityManager.createQuery("SELECT lr FROM LancamentoRateio lr WHERE lr.id IN ?1 AND lr.lancamento.cliente = ?2", LancamentoRateio.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}
