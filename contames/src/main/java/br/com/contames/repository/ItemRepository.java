package br.com.contames.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.component.Settings;
import br.com.contames.entity.Classificacao;
import br.com.contames.entity.Item;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.StringHelper;

@Component
public class ItemRepository extends Repository<Item, Long> {

	private final UsuarioLogged usuarioLogged;
	private final Settings settings;
	
	public ItemRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, Settings settings) {
		super(Item.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.settings = settings;
	}
	
	
	/**
	 * Verifica se existe outro Item com a mesma classificacao, nome e tipo
	 * @param item
	 * @return boolean
	 */
	public boolean exists(Item item) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
		Root<Item> root = cqCount.from(Item.class);
		
		cqCount.select(cb.count(root));
		List<Predicate> predicateList = new ArrayList<Predicate>();
		predicateList.add(cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
		predicateList.add(cb.equal(root.get("nome"), item.getNome()));
		predicateList.add(cb.equal(root.get("tipo"), item.getTipo()));
		
		if(item.getId() != null) {
			predicateList.add(cb.notEqual(root.get("id"), item.getId()));
		}
		
		predicateList.add(cb.equal(root.get("classificacao"), item.getClassificacao()));
		
		cqCount.where(predicateList.toArray(new Predicate[predicateList.size()]));
		
		TypedQuery<Long> typedQuery = entityManager.createQuery(cqCount);
		
		
		return typedQuery.getSingleResult() > 0;
	}
	
	
	/**
	 * Salva Item
	 * @param item
	 * @return ItemRepository
	 */
	@Override
	public ItemRepository save(Item item) {
		item.setCliente(usuarioLogged.getCliente());
		item.setCriacao(LocalDateTime.now());
		item.setUsuCriacao(usuarioLogged.getUsuario());
		if(item.getId() == null) {
			entityManager.persist(item);
		} else {
			
			item.setAlteracao(LocalDateTime.now());
			item.setUsuAlteracao(usuarioLogged.getUsuario());
			
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("UPDATE Item i SET ");
			sqlBuilder.append("i.classificacao = ?1, ");
			sqlBuilder.append("i.nome = ?2, ");
			sqlBuilder.append("i.status = ?3, ");
			sqlBuilder.append("i.descricao = ?4, ");
			sqlBuilder.append("i.unidade = ?5, ");
			sqlBuilder.append("i.valor = ?6, ");
			sqlBuilder.append("i.estoque = ?7, ");
			sqlBuilder.append("i.tipo = ?8, ");
			sqlBuilder.append("i.isLancAuto = ?9, ");
			sqlBuilder.append("i.favorecido = ?10, ");
			sqlBuilder.append("i.diaVencimento = ?11, ");
			sqlBuilder.append("i.meio = ?12, ");
			sqlBuilder.append("i.conta = ?13, ");
			sqlBuilder.append("i.cartaoCredito = ?14, ");
			sqlBuilder.append("i.isLimite = ?15, ");
			sqlBuilder.append("i.alteracao = ?16, ");
			sqlBuilder.append("i.usuAlteracao = ?17 ");
			sqlBuilder.append("WHERE i.id = ?18 AND i.cliente = ?19");
			Query query = entityManager.createQuery(sqlBuilder.toString());
			query.setParameter(1, item.getClassificacao());
			query.setParameter(2, item.getNome());
			query.setParameter(3, item.getStatus());
			query.setParameter(4, item.getDescricao());
			query.setParameter(5, item.getUnidade());
			query.setParameter(6, item.getValor());
			query.setParameter(7, item.getEstoque());
			query.setParameter(8, item.getTipo());
			query.setParameter(9, item.getIsLancAuto());
			query.setParameter(10, item.getFavorecido());
			query.setParameter(11, item.getDiaVencimento());
			query.setParameter(12, item.getMeio());
			query.setParameter(13, item.getConta());
			query.setParameter(14, item.getCartaoCredito());
			query.setParameter(15, item.getIsLimite());
			query.setParameter(16, item.getAlteracao());
			query.setParameter(17, item.getUsuAlteracao());
			query.setParameter(18, item.getId());
			query.setParameter(19, usuarioLogged.getCliente());
			query.executeUpdate();
		}
		return this;
	}
	
	
	/**
	 * Recupera Item
	 * @param id
	 * @return Item
	 */
	@Override
	public Item find(Long id) {
		Query query = entityManager.createQuery("SELECT i FROM Item i LEFT OUTER JOIN i.favorecido f WHERE i.id = ?1 AND i.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		try {
			return (Item) query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	/**
	 * Recupera Item com status ATI
	 * @param id
	 * @return Item
	 */
	public Item findActive(Long id) {
		Query query = entityManager.createQuery("SELECT i FROM Item i WHERE i.id = ?1 AND i.cliente = ?2 AND i.status = ?3");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		query.setParameter(3, Status.ATI);
		try {
			return (Item) query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	
	/**
	 * Verifica se existe LancamentoItem relacionado ao Item
	 * @param id
	 * @return
	 */
	public boolean existLancamentoItem(Long id) {
		return ((Long) entityManager
			.createQuery("SELECT COUNT(li) FROM LancamentoItem li WHERE li.item.id = ?1 AND li.lancamento.cliente = ?2")
			.setParameter(1, id)
			.setParameter(2, usuarioLogged.getCliente())
			.getSingleResult()) > 0;
	}
	
	
	/**
	 * Remove Item
	 * @param id
	 * @return ItemRepository
	 */
	@Override
	public ItemRepository delete(Long id) {
		entityManager
			.createQuery("DELETE FROM Item i WHERE i.id = ?1 AND i.cliente = ?2")
			.setParameter(1, id)
			.setParameter(2, usuarioLogged.getCliente())
			.executeUpdate();
		return this;
	}
	
	
	/**
	 * Remove Items
	 * @param ids
	 * @return ItemRepository
	 */
	public ItemRepository deleteList(List<Long> ids) {
		entityManager
			.createQuery("DELETE FROM Item i WHERE i.id IN ?1 AND i.cliente = ?2")
			.setParameter(1, ids)
			.setParameter(2, usuarioLogged.getCliente())
			.executeUpdate();
		return this;
	}
	
	
	/**
	 * Recupera uma List com Item
	 * @param ids
	 * @return List<Item>
	 */
	public List<Item> find(List<Long> ids) {
		TypedQuery<Item> typedQuery = entityManager.createQuery("SELECT i FROM Item i WHERE i.id IN ?1 AND i.cliente = ?2 ORDER BY i.nome", Item.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public List findAllActive(ItemTipo tipo, String search) {
		if(search == null) {
			return null;
		} else {
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder = new StringBuilder();
			sqlBuilder.append("SELECT ");
			sqlBuilder.append("i.id, ");
			sqlBuilder.append("COALESCE(Categ.nome || ' - ', '') || COALESCE(Fami.nome || ' - ', '') || COALESCE(Tipo.nome || ' - ', '') || COALESCE(Det.nome || ' - ', '') || i.nome AS nome, ");
			sqlBuilder.append("i.diaVencimento, ");
			sqlBuilder.append("i.favorecido_id favorecido, ");
			sqlBuilder.append("i.valor, ");
			sqlBuilder.append("i.meio, ");
			sqlBuilder.append("false isClassif ");
			sqlBuilder.append("FROM Item i ");
			sqlBuilder.append("LEFT JOIN Classificacao AS Det ON i.classificacao_id = Det.id ");
			sqlBuilder.append("LEFT JOIN Classificacao AS Tipo ON Det.classificacao_id = Tipo.id ");
			sqlBuilder.append("LEFT JOIN Classificacao AS Fami ON Tipo.classificacao_id = Fami.id ");
			sqlBuilder.append("LEFT JOIN Classificacao AS Categ ON Fami.classificacao_id = Categ.id ");
			sqlBuilder.append("LEFT JOIN Descricao AS Unidade ON i.unidade_id = Unidade.id ");
			sqlBuilder.append("WHERE i.cliente_id = ?1 ");
			
			if(tipo != null) {
				sqlBuilder.append("AND i.tipo = ?2 ");
			}
			
			if(StringUtils.isNotBlank(search)) {
				sqlBuilder.append(" AND (");
				sqlBuilder.append("unaccent(LOWER(COALESCE(Categ.nome || ' - ', '') || COALESCE(Fami.nome || ' - ', '') || COALESCE(Tipo.nome || ' - ', '') || COALESCE(Det.nome || ' - ', '') || unaccent(LOWER(i.nome)))) ILIKE ?3 ");
				sqlBuilder.append(" OR unaccent(LOWER(i.nome)) ILIKE ?3 ");
				sqlBuilder.append(")");
			}
			
			sqlBuilder.append(" AND i.status = ?4");
			sqlBuilder.append(" ORDER BY nome");
			Query query = entityManager.createNativeQuery(sqlBuilder.toString());
			query.setParameter(1, usuarioLogged.getCliente().getId());
			if(tipo != null) {
				query.setParameter(2, tipo.toString());
			}
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
			query.setParameter(4, Status.ATI.toString());
			query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
			query.setFirstResult(0);
			query.setMaxResults((int) settings.getConfiguration().getFloat("page.size"));
			try {
				return query.getResultList();
			} catch(NoResultException e) { 
				return null;
			}
		}
	}


	public boolean existCompensacaoItem(Long id) {
		return ((Long) entityManager
				.createQuery("SELECT COUNT(ci) FROM CompensacaoItem ci WHERE ci.item.id = ?1 AND ci.compensacao.cliente = ?2")
				.setParameter(1, id)
				.setParameter(2, usuarioLogged.getCliente())
				.getSingleResult()) > 0;
	}


	public List<Item> search(ItemTipo tipo, String search) {
		TypedQuery<Item> typedQuery = entityManager.createQuery("SELECT i FROM Item i WHERE i.cliente = ?1 AND i.tipo = ?2 AND LOWER(FUNC('unaccent', i.nome)) LIKE ?3 ORDER BY i.nome", Item.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, tipo);
		typedQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
}
