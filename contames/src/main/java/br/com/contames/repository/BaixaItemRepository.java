package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDate;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.Cheque;
import br.com.contames.entity.Conta;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class BaixaItemRepository extends Repository<BaixaItem, Long> {

	private final UsuarioLogged usuarioLogged;
	private final ChequeRepository chequeRepository;
	
	
	public BaixaItemRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, ChequeRepository chequeRepository) {
		super(BaixaItem.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.chequeRepository = chequeRepository;
	}
	
	
	public boolean exists(LancamentoParcela lancamentoParcela) {
		Query query = entityManager.createQuery("SELECT COUNT(bi) FROM BaixaItem bi WHERE bi.lancamentoParcela = ?1 AND bi.baixa.cliente = ?2");
		query.setParameter(1, lancamentoParcela);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	
	public List<BaixaItem> findAllBxd(List<Long> ids, LocalDate localDate) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi LEFT OUTER JOIN bi.cheque c WHERE bi.id IN ?1 AND bi.status = ?3 AND bi.baixa.cliente = ?2 AND bi.baixa.status = ?3 AND COALESCE(c.data, bi.baixa.data) <= ?4 ORDER BY COALESCE(c.data, bi.baixa.data)", BaixaItem.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		typedQuery.setParameter(3, LancamentoStatus.BXD);
		typedQuery.setParameter(4, localDate.toDate());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public BaixaItemRepository save(BaixaItem baixaItem) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		if(baixaItem.getCheque() != null) {
			baixaItem.getCheque().setStatus(baixaItem.getStatus());
			baixaItem.getCheque().setCliente(usuarioLogged.getCliente());
			baixaItem.getCheque().setFavorecido(baixaItem.getLancamentoParcela().getLancamento().getFavorecido());
			chequeRepository.save(baixaItem.getCheque());
		}
		
		if(baixaItem.getId() == null) {
			
			entityManager.persist(baixaItem);
		
		} else {
			
			CriteriaUpdate<BaixaItem> update = cb.createCriteriaUpdate(BaixaItem.class);
			Root<BaixaItem> root = update.from(BaixaItem.class);
			
			update.set("seq", baixaItem.getSeq());
			update.set("status", baixaItem.getStatus());
			if(!LancamentoStatus.COM.equals(baixaItem.getStatus())) {
				update.set("meio", baixaItem.getMeio());
				update.set("cartaoCredito", baixaItem.getCartaoCredito());
				update.set("isLimite", baixaItem.getIsLimite());
				update.set("conta", baixaItem.getConta());
				update.set("cheque", baixaItem.getCheque());
				update.set("valor", baixaItem.getValor());
				update.set("diferenca", baixaItem.getDiferenca());
				
			} else {
				
				if(baixaItem.getCheque() != null) {
					chequeRepository.saveCom(baixaItem.getCheque());
				}
				
			}
			
			update.set("boletoTipo", baixaItem.getBoletoTipo());
			update.set("boletoNumero", baixaItem.getBoletoNumero());
			update.set("arquivo", baixaItem.getArquivo());
			update.set("comprovante", baixaItem.getComprovante());
			update.set("motivo", baixaItem.getMotivo());
			update.set("obs", baixaItem.getObs());
			
			update.where(cb.equal(root.get("id"), baixaItem.getId()));
			entityManager.createQuery(update).executeUpdate();
		
		}
		
		// Atualiza Status do LancamentoParcela
		CriteriaUpdate<LancamentoParcela> update = cb.createCriteriaUpdate(LancamentoParcela.class);
		Root<LancamentoParcela> root = update.from(LancamentoParcela.class);
		if(baixaItem.getStatus().equals(LancamentoStatus.STD) || baixaItem.getStatus().equals(LancamentoStatus.CAN)) {
			update.set("status", LancamentoStatus.LAN);
		} else {
			update.set("status", baixaItem.getStatus());
		}
		update.where(cb.equal(root.get("id"), baixaItem.getLancamentoParcela().getId()));
		entityManager.createQuery(update).executeUpdate();
		
		return this;
	}
	
	
	public List<BaixaItem> findAll(Long id) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi LEFT OUTER JOIN bi.cheque c WHERE bi.baixa.id = ?1 ORDER BY bi.seq", BaixaItem.class);
		typedQuery.setParameter(1, id);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	public BaixaItemRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM BaixaItem bi WHERE bi.id IN ?1 AND bi.baixa.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}


	public List<BaixaItem> findWithArquivo(List<Long> ids) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi WHERE bi.id IN ?1 AND (bi.arquivo IS NOT NULL OR bi.comprovante IS NOT NULL) AND bi.baixa.cliente = ?2", BaixaItem.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	public List<BaixaItem> findAllByBaixaId(List<Long> ids) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi WHERE bi.baixa.id IN ?1 AND bi.baixa.cliente = ?2", BaixaItem.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}


	public BaixaItem findByChequeCom(Cheque cheque) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi WHERE bi.cheque = ?1 AND bi.baixa.cliente = ?2 AND bi.status = ?3", BaixaItem.class);
		typedQuery.setParameter(1, cheque);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		typedQuery.setParameter(3, LancamentoStatus.COM);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}


	public LocalDate minData(Conta conta) {
		Query query = entityManager.createQuery("SELECT MIN(COALESCE(c.data, bi.baixa.data)) FROM BaixaItem bi LEFT OUTER JOIN bi.cheque c WHERE bi.baixa.cliente = ?1 AND bi.status = ?2 AND bi.conta = ?3");
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, LancamentoStatus.BXD);
		query.setParameter(3, conta);
		try {
			return new LocalDate(query.getSingleResult());
		} catch(NoResultException e) {
			return null;
		}
	}


	public List<BaixaItem> findByCheque(Cheque cheque) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi WHERE bi.cheque.talaoCheque = ?1 AND bi.cheque.numero = ?2 AND bi.baixa.cliente = ?3", BaixaItem.class);
		typedQuery.setParameter(1, cheque.getTalaoCheque());
		typedQuery.setParameter(2, cheque.getNumero());
		typedQuery.setParameter(3, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}


	public List<BaixaItem> findAll(List<Long> baixaItemDelete) {
		TypedQuery<BaixaItem> typedQuery = entityManager.createQuery("SELECT bi FROM BaixaItem bi WHERE bi.id IN ?1 AND bi.baixa.cliente = ?2", BaixaItem.class);
		typedQuery.setParameter(1, baixaItemDelete);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
}
