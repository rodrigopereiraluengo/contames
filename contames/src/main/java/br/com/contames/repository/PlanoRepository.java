package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Plano;
import br.com.contames.enumeration.Status;

@Component
public class PlanoRepository extends Repository<Plano, Long> {

	public PlanoRepository(EntityManager entityManager) {
		super(Plano.class, entityManager);
	}
	
	public List<Plano> findAllActive() {
		TypedQuery<Plano> typedQuery = getEntityManager().createQuery("SELECT p FROM Plano p WHERE p.status = ?1 ORDER BY p.id", Plano.class);
		typedQuery.setParameter(1, Status.ATI);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	public Plano findActive(Long id) {
		TypedQuery<Plano> typedQuery = getEntityManager().createQuery("SELECT p FROM Plano p WHERE p.id = ?1 AND p.status = ?2", Plano.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, Status.ATI);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}
