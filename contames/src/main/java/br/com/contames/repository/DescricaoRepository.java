package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Descricao;
import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.StringHelper;

@Component
public class DescricaoRepository extends Repository<Descricao, Long> {

	private final UsuarioLogged usuarioLogged;
	
	public DescricaoRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(Descricao.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}
	
	
	public Descricao find(Long id, DescricaoTipo descricaoTipo) {
		TypedQuery<Descricao> typedQuery = entityManager.createQuery("SELECT d FROM Descricao d WHERE d.id = ?1 AND d.descricaoTipo = ?2 AND d.cliente = ?3", Descricao.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, descricaoTipo);
		typedQuery.setParameter(3, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
		
	}

	
	public boolean isNameExists(Descricao descricao) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT COUNT(d) FROM Descricao d WHERE d.cliente = ?1 AND d.nome = ?2 AND d.descricaoTipo = ?3");
		if(descricao.getId() != null) {
			sqlBuilder.append(" AND d.id <> ?4");
		}
		Query query = entityManager.createQuery(sqlBuilder.toString());
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, descricao.getNome());
		query.setParameter(3, descricao.getDescricaoTipo());
		if(descricao.getId() != null) {
			query.setParameter(4, descricao.getId());
		}
		Long count = (Long) query.getSingleResult();
		return count > 0;
	}
	
	
	@Override
	public DescricaoRepository save(Descricao descricao) {
		if(descricao.getId() == null) {
			descricao.setCliente(usuarioLogged.getCliente());
			descricao.setCriacao(LocalDateTime.now());
			descricao.setUsuCriacao(usuarioLogged.getUsuario());
			entityManager.persist(descricao);
		} else {
			Query query = entityManager.createQuery("UPDATE Descricao d SET d.nome = ?1, d.status = ?2, d.alteracao = ?3, d.usuAlteracao = ?4 WHERE d.id = ?5 AND d.cliente = ?6");
			query.setParameter(1, descricao.getNome());
			query.setParameter(2, descricao.getStatus());
			query.setParameter(3, LocalDateTime.now());
			query.setParameter(4, usuarioLogged.getUsuario());
			query.setParameter(5, descricao.getId());
			query.setParameter(6, usuarioLogged.getCliente());
			query.executeUpdate();
		}
		return this;
	}
	
	
	@Override
	public DescricaoRepository delete(Long id) {
		Query query = entityManager.createQuery("DELETE FROM Descricao d WHERE d.id = ?1 AND d.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	public DescricaoRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM Descricao d WHERE d.id IN ?1 AND d.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	
	public List<Descricao> findAllActive(DescricaoTipo descricaoTipo) {
		TypedQuery<Descricao> typedQuery = entityManager.createQuery("SELECT d FROM Descricao d WHERE d.cliente = ?1 AND d.descricaoTipo = ?2 AND d.status = ?3 ORDER BY d.nome", Descricao.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, descricaoTipo);
		typedQuery.setParameter(3, Status.ATI);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public boolean exists(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(d) FROM Descricao d WHERE d.cliente = ?1");
		query.setParameter(1, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}


	public List<Descricao> search(String search, DescricaoTipo descricaoTipo) {
		TypedQuery<Descricao> typedQuery = entityManager.createQuery("SELECT d FROM Descricao d WHERE d.cliente = ?1 AND d.descricaoTipo = ?2 AND LOWER(FUNC('unaccent', d.nome)) LIKE ?3 ORDER BY d.nome", Descricao.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, descricaoTipo);
		typedQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
}
