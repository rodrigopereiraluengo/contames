package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Classificacao;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.StringHelper;

@Component
public class ClassificacaoRepository extends Repository<Classificacao, Long> {
	
	private final UsuarioLogged usuarioLogged;
	
	public ClassificacaoRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(Classificacao.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}
	
	@Override
	public Classificacao find(Long id) {
		Query query = entityManager.createQuery("SELECT c FROM Classificacao c WHERE c.id = ?1 AND c.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		try {
			return (Classificacao) query.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	public boolean isNameExists(Classificacao classificacao) {
		
		Classificacao _classificacao = find(classificacao.getId());
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT COUNT(c) FROM Classificacao c WHERE c.tipo = ?1 AND c.nivel = ?2 AND c.nome = ?3 AND c.cliente = ?4 AND c.id <> ?5");
		if(_classificacao.getClassificacao() != null) {
			sqlBuilder.append(" AND c.classificacao = ?6");
		}
		
		Query query = getEntityManager().createQuery(sqlBuilder.toString());
		query.setParameter(1, classificacao.getTipo());
		query.setParameter(2, classificacao.getNivel());
		query.setParameter(3, classificacao.getNome());
		query.setParameter(4, usuarioLogged.getCliente());
		query.setParameter(5, classificacao.getId());
		if(_classificacao.getClassificacao() != null) {
			query.setParameter(6, _classificacao.getClassificacao());
		}
		
		return ((Long) query.getSingleResult()) > 0;
	}

	public void rename(Classificacao classificacao) {
		Query query = entityManager.createQuery("UPDATE Classificacao c SET c.nome = ?1 WHERE c.id = ?2 AND c.cliente = ?3");
		query.setParameter(1, classificacao.getNome());
		query.setParameter(2, classificacao.getId());
		query.setParameter(3, usuarioLogged.getCliente());
		query.executeUpdate();
	}

	public boolean isFoundItem(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(i) FROM Item i WHERE i.cliente = ?1 AND i.classificacao.id = ?2");
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, id);
		return (Long)query.getSingleResult() > 0;
	}

	public void remove(Long id) {
		entityManager
			.createQuery("DELETE FROM Classificacao c WHERE c.id = ?1 AND c.cliente = ?2")
			.setParameter(1, id)
			.setParameter(2, usuarioLogged.getCliente())
			.executeUpdate();
	}
	
	public List<Classificacao> findAll(ItemTipo tipo) {
		try {
			List<Classificacao> classificacaoList = entityManager
					.createQuery("SELECT c FROM Classificacao c WHERE c.tipo = ?1 AND c.cliente = ?2 AND c.classificacao IS NULL ORDER BY c.index", Classificacao.class)
					.setParameter(1, tipo)
					.setParameter(2, usuarioLogged.getCliente())
					.getResultList();
			return classificacaoList;
		} catch(NoResultException e) {
			return null;
		}
	}

	public List<Classificacao> findAll(Long id, ItemTipo tipo) {
		TypedQuery<Classificacao> query = entityManager.createQuery("SELECT c FROM Classificacao c WHERE c.tipo = ?1 AND c.classificacao.id = ?2 AND c.cliente = ?3 ORDER BY c.index", Classificacao.class);
		query.setParameter(1, tipo);
		query.setParameter(2, id);
		query.setParameter(3, usuarioLogged.getCliente());
		try {
			return query.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	public void move(Classificacao classificacao) {
		entityManager
			.createQuery("UPDATE Classificacao c SET c.classificacao = ?1, c.nivel = ?2, c.index = ?3 WHERE c.id = ?4 AND c.cliente = ?5")
			.setParameter(1, classificacao.getClassificacao())
			.setParameter(2, classificacao.getNivel())
			.setParameter(3, classificacao.getIndex())
			.setParameter(4, classificacao.getId())
			.setParameter(5, usuarioLogged.getCliente())
			.executeUpdate();
	}
	
	public void updateIndex(Classificacao classificacao) {
		entityManager
			.createQuery("UPDATE Classificacao c SET c.index = ?1 WHERE c.id = ?2 AND c.cliente = ?3")
			.setParameter(1, classificacao.getIndex())
			.setParameter(2, classificacao.getId())
			.setParameter(3, usuarioLogged.getCliente())
			.executeUpdate();
	}

	public List<Classificacao> search(String search) {
		TypedQuery<Classificacao> typedQuery = entityManager.createQuery("SELECT c FROM Classificacao c WHERE c.cliente = ?1 AND LOWER(FUNC('unaccent', c.nome)) LIKE ?2 ORDER BY c.nome", Classificacao.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, StringHelper.unAccent(search.toLowerCase()) + "%");
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}

}
