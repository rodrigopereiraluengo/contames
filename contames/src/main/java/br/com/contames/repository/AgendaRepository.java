package br.com.contames.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Agenda;
import br.com.contames.entity.Lancamento;
import br.com.contames.enumeration.DocTipo;
import br.com.contames.session.UsuarioLogged;

@Component
public class AgendaRepository extends Repository<Agenda, Long> {
	
	private final UsuarioLogged usuarioLogged;
	
	public AgendaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(Agenda.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}
	
	@Override
	public Agenda find(Long id) {
		TypedQuery<Agenda> typedQuery = getEntityManager().createQuery("SELECT a FROM Agenda a WHERE a.cliente = ?1 AND a.id = ?2", Agenda.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, id);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public AgendaRepository save(Agenda agenda) {

		if(agenda.getId() == null) {
			
			agenda.setCliente(usuarioLogged.getCliente());
			agenda.setCriacao(LocalDateTime.now());
			agenda.setUsuCriacao(usuarioLogged.getUsuario());
			
			entityManager.persist(agenda);
		
		} else {
			
			agenda.setAlteracao(LocalDateTime.now());
			agenda.setUsuAlteracao(usuarioLogged.getUsuario());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Agenda> update = cb.createCriteriaUpdate(Agenda.class);
			Root<Agenda> root = update.from(Agenda.class);
			
			update.set("nome", agenda.getNome());
			update.set("tipo", agenda.getTipo());
			update.set("inicio", agenda.getInicio());
			update.set("fim", agenda.getFim());
			update.set("pessoa", agenda.getPessoa());
			update.set("obs", agenda.getObs());
			update.set("usuario", agenda.getUsuario());
			update.set("alteracao", agenda.getAlteracao());
			update.set("usuAlteracao", agenda.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), agenda.getId()));
			
			entityManager.createQuery(update).executeUpdate();
		
		}
				
		return this;		
	}

}
