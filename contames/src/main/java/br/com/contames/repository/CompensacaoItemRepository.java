package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.CompensacaoItem;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.entity.Transferencia;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class CompensacaoItemRepository extends Repository<CompensacaoItem, Long> {

	private final UsuarioLogged usuarioLogged;
	private final CompensacaoRepository compensacaoRepository;
	private final ChequeRepository chequeRepository;
	
	public CompensacaoItemRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, CompensacaoRepository compensacaoRepository, ChequeRepository chequeRepository) {
		super(CompensacaoItem.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.compensacaoRepository = compensacaoRepository;
		this.chequeRepository = chequeRepository;
	}
	
	public boolean exists(BaixaItem baixaItem) {
		Query query = entityManager.createQuery("SELECT COUNT(ci) FROM CompensacaoItem ci WHERE ci.baixaItem = ?1 AND ci.compensacao.cliente = ?2");
		query.setParameter(1, baixaItem);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}

	public List<CompensacaoItem> findAll(Long id) {
		TypedQuery<CompensacaoItem> typedQuery = entityManager.createQuery("SELECT ci FROM CompensacaoItem ci WHERE ci.compensacao.id = ?1 ORDER BY ci.seq", CompensacaoItem.class);
		typedQuery.setParameter(1, id);
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public CompensacaoItemRepository save(CompensacaoItem compensacaoItem) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		boolean isClosed = false;
		
		if(compensacaoItem.getId() == null) {
			
			entityManager.persist(compensacaoItem);
		
		} else {

			isClosed = compensacaoRepository.isClosed(compensacaoItem.getCompensacao());
			
			CriteriaUpdate<CompensacaoItem> update = cb.createCriteriaUpdate(CompensacaoItem.class);
			Root<CompensacaoItem> root = update.from(CompensacaoItem.class);
			
			if(!isClosed) {
				
				update.set("seq", compensacaoItem.getSeq());
				update.set("data", compensacaoItem.getData());
				update.set("debito", compensacaoItem.getDebito());
				update.set("credito", compensacaoItem.getCredito());
				update.set("saldo", compensacaoItem.getSaldo());
				update.set("status", compensacaoItem.getStatus());
			}
			update.set("favorecido", compensacaoItem.getFavorecido());
			update.set("item", compensacaoItem.getItem());
			update.set("obs", compensacaoItem.getObs());
			update.where(cb.equal(root.get("id"), compensacaoItem.getId()));
			entityManager.createQuery(update).executeUpdate();
			
		}
		
		
		// Verifica se a compensacao nao esta fechada
		if(!isClosed) {
			
			if(compensacaoItem.getBaixaItem() != null) {
				
				// Atualiza Status do LancamentoParcela
				CriteriaUpdate<LancamentoParcela> updateLancamentoParcela = cb.createCriteriaUpdate(LancamentoParcela.class);
				Root<LancamentoParcela> rootLancamentoParcela = updateLancamentoParcela.from(LancamentoParcela.class);
				if(compensacaoItem.getStatus().equals(LancamentoStatus.STD) || compensacaoItem.getStatus().equals(LancamentoStatus.CAN)) {
					updateLancamentoParcela.set("status", LancamentoStatus.BXD);
				} else {
					updateLancamentoParcela.set("status", compensacaoItem.getStatus());
				}
				updateLancamentoParcela.where(cb.equal(rootLancamentoParcela.get("id"), compensacaoItem.getBaixaItem().getLancamentoParcela().getId()));
				entityManager.createQuery(updateLancamentoParcela).executeUpdate();
				
				// Atualiza Status do BaixaItem
				CriteriaUpdate<BaixaItem> updateBaixaItem = cb.createCriteriaUpdate(BaixaItem.class);
				Root<BaixaItem> rootBaixaItem = updateBaixaItem.from(BaixaItem.class);
				if(compensacaoItem.getStatus().equals(LancamentoStatus.STD) || compensacaoItem.getStatus().equals(LancamentoStatus.CAN)) {
					updateBaixaItem.set("status", LancamentoStatus.BXD);
					if(compensacaoItem.getBaixaItem().getCheque() != null) {
						compensacaoItem.getBaixaItem().getCheque().setStatus(LancamentoStatus.BXD);
						chequeRepository.save(compensacaoItem.getBaixaItem().getCheque());
					}
					
				} else {
					updateBaixaItem.set("status", compensacaoItem.getStatus());
					if(compensacaoItem.getBaixaItem().getCheque() != null) {
						compensacaoItem.getBaixaItem().getCheque().setStatus(compensacaoItem.getStatus());
						chequeRepository.save(compensacaoItem.getBaixaItem().getCheque());
					}
				}
				updateBaixaItem.where(cb.equal(rootBaixaItem.get("id"), compensacaoItem.getBaixaItem().getId()));
				entityManager.createQuery(updateBaixaItem).executeUpdate();
				
			}
			
			// Transferencia
			if(compensacaoItem.getTransferencia() != null) {
				
				Transferencia transferencia = compensacaoItem.getTransferencia();
				
				if(LancamentoStatus.COM.equals(compensacaoItem.getStatus())) {
					
					if(ItemTipo.DES.equals(compensacaoItem.getTipo())) {
						transferencia.setStatusOrigem(LancamentoStatus.COM);
						
						if(transferencia.getCheque() != null) {
							transferencia.getCheque().setStatus(LancamentoStatus.COM);
							chequeRepository.save(transferencia.getCheque());
						}
						
					} else {
						transferencia.setStatusDestino(LancamentoStatus.COM);
					}
					transferencia.setStatus(LancamentoStatus.COM);
				
				} else {
					
					if(ItemTipo.DES.equals(compensacaoItem.getTipo())) {
						transferencia.setStatusOrigem(LancamentoStatus.LAN);
						
						if(transferencia.getCheque() != null) {
							transferencia.getCheque().setStatus(LancamentoStatus.LAN);
							chequeRepository.save(transferencia.getCheque());
						}
						
					} else {
						transferencia.setStatusDestino(LancamentoStatus.LAN);
					}
					
					if(LancamentoStatus.LAN.equals(transferencia.getStatusOrigem()) && LancamentoStatus.LAN.equals(transferencia.getStatusDestino())) {
						transferencia.setStatus(LancamentoStatus.LAN);
					}
					
				}
				
				// Atualiza Status do Transferencia
				CriteriaUpdate<Transferencia> updateTransferencia = cb.createCriteriaUpdate(Transferencia.class);
				Root<Transferencia> rootBaixaItem = updateTransferencia.from(Transferencia.class);
				updateTransferencia.set("status", transferencia.getStatus());
				updateTransferencia.set("statusOrigem", transferencia.getStatusOrigem());
				updateTransferencia.set("statusDestino", transferencia.getStatusDestino());
				updateTransferencia.where(cb.equal(rootBaixaItem.get("id"), transferencia.getId()));
				entityManager.createQuery(updateTransferencia).executeUpdate();
			}
			
		}
		
		return this;
	}
	
	
	public CompensacaoItemRepository deleteList(List<Long> ids) {
		
		// Atualiza LancamentoParcela
		entityManager.createQuery("UPDATE LancamentoParcela lp SET lp.status = ?1 WHERE lp.id IN(SELECT ci.baixaItem.lancamentoParcela.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.compensacao.cliente = ?3)")
			.setParameter(1, LancamentoStatus.BXD)
			.setParameter(2, ids)
			.setParameter(3, usuarioLogged.getCliente())
			.executeUpdate();
		
		// Atualiza Cheque
		entityManager.createQuery("UPDATE Cheque c SET c.status = ?1 WHERE c.id IN(SELECT ci.baixaItem.cheque.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.compensacao.cliente = ?3)")
			.setParameter(1, LancamentoStatus.BXD)
			.setParameter(2, ids)
			.setParameter(3, usuarioLogged.getCliente())
			.executeUpdate();
		
		// Atualiza BaixaItem
		entityManager.createQuery("UPDATE BaixaItem bi SET bi.status = ?1 WHERE bi.id IN(SELECT ci.baixaItem.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.compensacao.cliente = ?3)")
			.setParameter(1, LancamentoStatus.BXD)
			.setParameter(2, ids)
			.setParameter(3, usuarioLogged.getCliente())
			.executeUpdate();
		
		
		// Atualiza Transferencia Cheque
		entityManager.createQuery("UPDATE Cheque c SET c.status = ?1 WHERE c.id IN(SELECT ci.transferencia.cheque.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.tipo = ?3 AND ci.compensacao.cliente = ?4)")
			.setParameter(1, LancamentoStatus.LAN)
			.setParameter(2, ids)
			.setParameter(3, ItemTipo.DES)
			.setParameter(4, usuarioLogged.getCliente())
			.executeUpdate();
		
		// Atualiza Transferencia Origem
		entityManager.createQuery("UPDATE Transferencia t SET t.statusOrigem = ?1 WHERE t.id IN(SELECT ci.transferencia.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.tipo = ?3 AND ci.compensacao.cliente = ?4)")
			.setParameter(1, LancamentoStatus.LAN)
			.setParameter(2, ids)
			.setParameter(3, ItemTipo.DES)
			.setParameter(4, usuarioLogged.getCliente())
			.executeUpdate();
		
		// Atualiza Transferencia Destino
		entityManager.createQuery("UPDATE Transferencia t SET t.statusDestino = ?1 WHERE t.id IN(SELECT ci.transferencia.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.tipo = ?3 AND ci.compensacao.cliente = ?4)")
			.setParameter(1, LancamentoStatus.LAN)
			.setParameter(2, ids)
			.setParameter(3, ItemTipo.REC)
			.setParameter(4, usuarioLogged.getCliente())
			.executeUpdate();
		
		// Atualiza Transferencia
		entityManager.createQuery("UPDATE Transferencia t SET t.status = ?1 WHERE t.statusOrigem = ?1 AND t.statusDestino = ?1 AND t.id IN(SELECT ci.transferencia.id FROM CompensacaoItem ci WHERE ci.id IN ?2 AND ci.compensacao.cliente = ?3)")
			.setParameter(1, LancamentoStatus.LAN)
			.setParameter(2, ids)
			.setParameter(3, usuarioLogged.getCliente())
			.executeUpdate();
		
		Query query = entityManager.createQuery("DELETE FROM CompensacaoItem ci WHERE ci.id IN ?1 AND ci.compensacao.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	
	public List<CompensacaoItem> findAllByCompensacaoIds(List<Long> ids) {
		if(ids == null || ids.isEmpty()) {
			return null;
		}
		TypedQuery<CompensacaoItem> typedQuery = entityManager.createQuery("SELECT ci FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?1 AND ci.compensacao.cliente = ?2 ORDER BY ci.seq", CompensacaoItem.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	public List<Long> findAllByCompensacaoIdsLong(List<Long> ids) {
		if(ids == null || ids.isEmpty()) {
			return null;
		}
		TypedQuery<Long> typedQuery = entityManager.createQuery("SELECT ci.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?1 AND ci.compensacao.cliente = ?2", Long.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
}
