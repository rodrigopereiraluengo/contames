package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Transferencia;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class TransferenciaRepository extends Repository<Transferencia, Long> {

	private final ChequeRepository chequeRepository;
	private final UsuarioLogged usuarioLogged;
		
	public TransferenciaRepository(EntityManager entityManager, ChequeRepository chequeRepository, UsuarioLogged usuarioLogged) {
		super(Transferencia.class, entityManager);
		this.chequeRepository = chequeRepository;
		this.usuarioLogged = usuarioLogged;
	}
	
	@Override
	public TransferenciaRepository save(Transferencia transferencia) {
		
		if(transferencia.getId() == null) {
			transferencia.setStatusDestino(transferencia.getStatusOrigem());
			transferencia.setCliente(usuarioLogged.getCliente());
			transferencia.setCriacao(LocalDateTime.now());
			transferencia.setUsuCriacao(usuarioLogged.getUsuario());
			transferencia.setStatusOrigem(transferencia.getStatus());
			transferencia.setStatusDestino(transferencia.getStatus());
			entityManager.persist(transferencia);
		} else {
			
			Transferencia transferenciaSalva = find(transferencia.getId());
			
			if(transferencia.getStatusDestino() == null) {
				transferencia.setStatusDestino(transferenciaSalva.getStatusDestino());
			}
			
			if(transferencia.getStatusOrigem() == null) {
				transferencia.setStatusOrigem(transferenciaSalva.getStatusOrigem());
			}
			
			// Verifica se a transferencia salva tinha Cheque
			if(transferencia.getCheque() == null && transferenciaSalva.getCheque() != null) {
				chequeRepository.delete(transferenciaSalva.getCheque());
			}

			// Verifica se a transferencia salva tem Cheque diferenca da transferencia
			if(transferencia.getCheque() != null && transferenciaSalva.getCheque() != null && !transferencia.getCheque().equals(transferenciaSalva.getCheque())) {
				chequeRepository.delete(transferenciaSalva.getCheque());
			}
			
			transferencia.setAlteracao(LocalDateTime.now());
			transferencia.setUsuAlteracao(usuarioLogged.getUsuario());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Transferencia> update = cb.createCriteriaUpdate(Transferencia.class);
			Root<Transferencia> root = update.from(Transferencia.class);
			
			if(!LancamentoStatus.COM.equals(transferencia.getStatus()) || !transferencia.getStatusOrigem().equals(LancamentoStatus.COM) || !transferencia.getStatusDestino().equals(LancamentoStatus.COM)) {
				update.set("status", transferencia.getStatusOrigem().equals(LancamentoStatus.COM) && transferencia.getStatusDestino().equals(LancamentoStatus.COM) ? LancamentoStatus.COM : transferencia.getStatus());
				update.set("statusOrigem", transferencia.getStatusOrigem());
				update.set("statusDestino", transferencia.getStatusDestino());
				update.set("tipo", transferencia.getTipo());
				update.set("conta", transferencia.getConta());
				update.set("cheque", transferencia.getCheque());
				update.set("cartaoCredito", transferencia.getCartaoCredito());
				update.set("isLimite", transferencia.getIsLimite());
				update.set("destino", transferencia.getDestino());
				update.set("valor", transferencia.getValor());
			}
			
			update.set("data", transferencia.getData());
			update.set("arquivo", transferencia.getArquivo());
			update.set("obs", transferencia.getObs());
			update.set("alteracao", transferencia.getAlteracao());
			update.set("usuAlteracao", transferencia.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), transferencia.getId()));
			
			entityManager.createQuery(update).executeUpdate();
			
		}
		
		return this;
	}
	
	
	public List<Transferencia> findAll(List<Long> ids) {
		return findAll(ids, null);
	}
	
	
	public List<Transferencia> findAll(List<Long> ids, LocalDate localDate) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT t FROM Transferencia t WHERE t.cliente = ?1 AND t.id IN ?2");
		if(localDate != null) {
			sqlBuilder.append(" AND t.data <= ?3");
		}
		TypedQuery<Transferencia> typedQuery = entityManager.createQuery(sqlBuilder.toString(), Transferencia.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, ids);
		if(localDate != null) {
			typedQuery.setParameter(3, localDate);
		}
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	public List<Transferencia> findAll(CartaoCredito cartaoCredito, List<Long> ids) {
		TypedQuery<Transferencia> typedQuery = entityManager.createQuery("SELECT t FROM Transferencia t WHERE t.cliente = ?1 AND t.cartaoCredito = ?2 AND t.id IN ?3", Transferencia.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, cartaoCredito);
		typedQuery.setParameter(3, ids);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public LocalDate minData(Conta conta) {
		Query query = entityManager.createQuery("SELECT MIN(t.data) FROM Transferencia t WHERE t.cliente = ?1 AND (t.statusOrigem = ?2 AND t.conta = ?3 OR t.statusDestino = ?2 AND t.destino = ?3)");
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, LancamentoStatus.LAN);
		query.setParameter(3, conta);
		try {
			return new LocalDate(query.getSingleResult());
		} catch(NoResultException e) {
			return null;
		}
	}
	
}
