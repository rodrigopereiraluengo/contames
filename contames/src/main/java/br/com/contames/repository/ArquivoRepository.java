package br.com.contames.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.component.ArquivoHelper;
import br.com.contames.entity.Arquivo;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.Lancamento;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.entity.Transferencia;
import br.com.contames.session.UsuarioLogged;

@Component
public class ArquivoRepository extends Repository<Arquivo, Long> {

	private final UsuarioLogged usuarioLogged;
	private final ArquivoHelper arquivoHelper;
	private final BaixaItemRepository baixaItemRepository;
	private final LancamentoParcelaRepository lancamentoParcelaRepository;
	
	public ArquivoRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, ArquivoHelper arquivoHelper, BaixaItemRepository baixaItemRepository, LancamentoParcelaRepository lancamentoParcelaRepository) {
		super(Arquivo.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.arquivoHelper = arquivoHelper;
		this.baixaItemRepository = baixaItemRepository;
		this.lancamentoParcelaRepository = lancamentoParcelaRepository;
	}
	
	
	@Override
	public Arquivo find(Long id) {
		TypedQuery<Arquivo> typedQuery = entityManager.createQuery("SELECT a FROM Arquivo a WHERE a.id = ?1 AND a.cliente = ?2", Arquivo.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public ArquivoRepository save(Arquivo arquivo) {
		
		if(arquivo.getId() == null) {
			arquivo.setCliente(usuarioLogged.getCliente());
			arquivo.setCriacao(LocalDateTime.now());
			arquivo.setUsuCriacao(usuarioLogged.getUsuario());
			entityManager.persist(arquivo);
		} else if(arquivoHelper.existsTemp(arquivo)) {
			arquivo.setAlteracao(LocalDateTime.now());
			arquivo.setUsuAlteracao(usuarioLogged.getUsuario());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Arquivo> update = cb.createCriteriaUpdate(Arquivo.class);
			Root<Arquivo> root = update.from(Arquivo.class);
			update.set("nome", arquivo.getNome());
			update.set("contentType", arquivo.getContentType());
			update.set("size", arquivo.getSize());
			update.set("alteracao", arquivo.getAlteracao());
			update.set("usuAlteracao", arquivo.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), arquivo.getId()), cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
			entityManager.createQuery(update).executeUpdate();
			
		}
		
		arquivoHelper.moveToContainer(arquivo);
		
		return this;
	}
	
	
	public ArquivoRepository save(Lancamento lancamento) {
		
		if(lancamento.getArquivoDoc() != null && lancamento.getArquivoDoc().getNome() != null) {
			save(lancamento.getArquivoDoc());
		} else if(lancamento.getId() != null) {
			Arquivo salvo = findBy(lancamento);
			if(salvo != null) {
				arquivoHelper.delete(salvo);
				delete(salvo);
			}
		}
		
		return this;
	}
	
	
	public Arquivo findBy(Lancamento lancamento) {
		TypedQuery<Arquivo> typedQuery = entityManager.createQuery("SELECT l.arquivoDoc FROM Lancamento l WHERE l.id = ?1 AND l.cliente = ?2", Arquivo.class);
		typedQuery.setParameter(1, lancamento.getId());
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}


	public ArquivoRepository save(LancamentoParcela lancamentoParcela) {
		if(lancamentoParcela.getArquivo() != null && lancamentoParcela.getArquivo().getNome() != null) {
			save(lancamentoParcela.getArquivo());
		} else if(lancamentoParcela.getId() != null) {
			Arquivo salvo = findBy(lancamentoParcela);
			if(salvo != null && (lancamentoParcela.getBaixaItem() == null || !salvo.equals(lancamentoParcela.getBaixaItem().getArquivo()))) {
				arquivoHelper.delete(salvo);
				delete(salvo);
			}
		}
		return this;
	}
	
	
	public Arquivo findBy(LancamentoParcela lancamentoParcela) {
		TypedQuery<Arquivo> typedQuery = entityManager.createQuery("SELECT lp.arquivo FROM LancamentoParcela lp WHERE lp.id = ?1 AND lp.lancamento.cliente = ?2", Arquivo.class);
		typedQuery.setParameter(1, lancamentoParcela.getId());
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	public void save(BaixaItem baixaItem) {
		
		if(baixaItem.getArquivo() != null) {
			save(baixaItem.getArquivo());
			if(!baixaItem.getArquivo().equals(baixaItem.getLancamentoParcela().getArquivo())) {
				baixaItem.getLancamentoParcela().setArquivo(baixaItem.getArquivo());
				lancamentoParcelaRepository.save(baixaItem.getLancamentoParcela());
			}
		}
		
		if(baixaItem.getComprovante() != null) {
			save(baixaItem.getComprovante());
		}
			
		if(baixaItem.getId() != null) {
			
			BaixaItem baixaItemSaldo = baixaItemRepository.find(baixaItem.getId());
			
			Arquivo salvo = null;
			if(baixaItem.getArquivo() == null) {
				salvo = baixaItemSaldo.getArquivo();
				if(salvo != null) {
					delete(baixaItem);
				}
			}
			
			if(baixaItem.getComprovante() == null) {
				salvo = baixaItemSaldo.getComprovante();
				if(salvo != null) {
					arquivoHelper.delete(salvo);
					delete(salvo);
				}
			}
		}
	}
	
	
	public ArquivoRepository save(Transferencia transferencia) {
		
		if(transferencia.getArquivo() != null) {
			save(transferencia.getArquivo());
		} else if(transferencia.getId() != null) {
			Arquivo salvo = findBy(transferencia);
			if(salvo != null) {
				arquivoHelper.delete(salvo);
				delete(salvo);
			}
		}
		
		return this;
	}
	
	
	public Arquivo findBy(Transferencia transferencia) {
		TypedQuery<Arquivo> typedQuery = entityManager.createQuery("SELECT t.arquivo FROM Transferencia t WHERE t.id = ?1 AND t.cliente = ?2", Arquivo.class);
		typedQuery.setParameter(1, transferencia.getId());
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	

	public boolean exists(Arquivo arquivo) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT id FROM Arquivo WHERE id = ?1 AND cliente_id = ?2)");
		query.setParameter(1, arquivo.getId());
		query.setParameter(2, usuarioLogged.getCliente().getId());
		return (boolean) query.getSingleResult();
	}
	
	
	@Override
	public ArquivoRepository delete(Arquivo arquivo) {
		arquivoHelper.delete(arquivo);
		super.delete(arquivo);
		return this;
	}


	public void delete(BaixaItem baixaItem) {
		if(!baixaItem.getArquivo().equals(baixaItem.getLancamentoParcela().getArquivo())) {
			delete(baixaItem.getArquivo());
		}
	}
	
}
