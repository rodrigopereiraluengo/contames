package br.com.contames.repository;

import java.util.EnumSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDate;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.LancamentoParcela;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class LancamentoParcelaRepository extends Repository<LancamentoParcela, Long> {

	private final UsuarioLogged usuarioLogged;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final ChequeRepository chequeRepository;
	
	public LancamentoParcelaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, CartaoCreditoRepository cartaoCreditoRepository, ChequeRepository chequeRepository) {
		super(LancamentoParcela.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.chequeRepository = chequeRepository;
	}
	
	
	/**
	 * Procupa parcela que possui arquivo
	 * @param id
	 * @return LancamentoParcela
	 */
	public List<LancamentoParcela> findWithArquivo(List<Long> ids) {
		TypedQuery<LancamentoParcela> typedQuery = entityManager.createQuery("SELECT lp FROM LancamentoParcela lp WHERE lp.id IN ?1 AND lp.arquivo IS NOT NULL AND lp.lancamento.cliente = ?2", LancamentoParcela.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	
	/**
	 * Procura parcelas
	 * @param ids
	 * @return
	 */
	public List<LancamentoParcela> findAllLan(List<Long> ids) {
		TypedQuery<LancamentoParcela> typedQuery = entityManager.createQuery("SELECT lp FROM LancamentoParcela lp WHERE lp.id IN ?1 AND lp.status = ?3 AND lp.lancamento.cliente = ?2 AND lp.lancamento.status = ?3 ORDER BY lp.data", LancamentoParcela.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		typedQuery.setParameter(3, LancamentoStatus.LAN);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public LancamentoParcelaRepository save(LancamentoParcela lancamentoParcela) {
		
		if(lancamentoParcela.getCheque() != null) {
			lancamentoParcela.getCheque().setStatus(lancamentoParcela.getStatus());
			lancamentoParcela.getCheque().setFavorecido(lancamentoParcela.getLancamento().getFavorecido());
			chequeRepository.save(lancamentoParcela.getCheque());
		}
				
		if(lancamentoParcela.getId() == null) {
			
			entityManager.persist(lancamentoParcela);
		
		} else {
			
			LancamentoParcela salvo = find(lancamentoParcela.getId());
			
			if(salvo.getStatus().equals(LancamentoStatus.LAN) && salvo.getCheque() != null && !salvo.getCheque().equals(lancamentoParcela.getCheque())) {
				chequeRepository.delete(salvo.getCheque());
			}
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<LancamentoParcela> update = cb.createCriteriaUpdate(LancamentoParcela.class);
			Root<LancamentoParcela> root = update.from(LancamentoParcela.class);
			
			update.set("seq", lancamentoParcela.getSeq());
			update.set("data", lancamentoParcela.getData());
			if(!LancamentoStatus.BXD.equals(lancamentoParcela.getStatus()) && !LancamentoStatus.COM.equals(lancamentoParcela.getStatus())) {
				update.set("meio", lancamentoParcela.getMeio());
				update.set("cartaoCredito", lancamentoParcela.getCartaoCredito());
				update.set("isLimite", lancamentoParcela.getIsLimite());
				update.set("conta", lancamentoParcela.getConta());
				update.set("cheque", lancamentoParcela.getCheque());
				update.set("valor", lancamentoParcela.getValor());
			}
			
			update.set("boletoTipo", lancamentoParcela.getBoletoTipo());
			update.set("boletoNumero", lancamentoParcela.getBoletoNumero());
			update.set("arquivo", lancamentoParcela.getArquivo());
			update.set("obs", lancamentoParcela.getObs());
			
			update.set("perc", lancamentoParcela.getPerc());
			update.set("status", lancamentoParcela.getStatus());
			
			update.where(cb.equal(root.get("id"), lancamentoParcela.getId()));
			entityManager.createQuery(update).executeUpdate();
		}
		
		return this;
	}
	
	
	public List<LancamentoParcela> findAll(Long id) {
		TypedQuery<LancamentoParcela> typedQuery = entityManager.createQuery("SELECT lp FROM LancamentoParcela lp WHERE lp.lancamento.id = ?1 ORDER BY lp.seq", LancamentoParcela.class);
		typedQuery.setParameter(1, id);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	/**
	 * Delete LancamentoParcela
	 * @param ids
	 * @return LancamentoParcelaRepository
	 */
	public LancamentoParcelaRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM LancamentoParcela lp WHERE lp.id IN ?1 AND lp.status IN ?2 AND lp.lancamento.cliente = ?3");
		query.setParameter(1, ids);
		query.setParameter(2, EnumSet.of(LancamentoStatus.STD, LancamentoStatus.LAN, LancamentoStatus.CAN));
		query.setParameter(3, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}


	public List<LancamentoParcela> findAll(CartaoCredito cartaoCredito, LocalDate data, List<Long> idsNotIn) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT lp ");
		sqlBuilder.append("FROM LancamentoParcela lp ");
		sqlBuilder.append("LEFT OUTER JOIN lp.baixaItem bi ");
		sqlBuilder.append("WHERE lp.lancamento.cliente = ?1 ");
		sqlBuilder.append("AND lp.status IN ?2 ");
		sqlBuilder.append("AND (lp.cartaoCredito = ?3 OR bi.cartaoCredito = ?3) ");
		
		if(data != null) {
			
			/*LocalDate dataDe = null;
			LocalDate dataAte = null;
			
			if(cartaoCredito.getDiasPagar() != null) {
				dataAte 
			}*/
			Integer month = data.getMonthOfYear();
			Integer year = data.getYear();
			Integer day = data.getDayOfMonth();
			
			if(cartaoCredito.getDiaVencimento() != null) {
				day = cartaoCredito.getDiaVencimento();
			}
			
			
			
		}
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			sqlBuilder.append("AND lp.id NOT IN ?6 ");
		}
		
		sqlBuilder.append("ORDER BY lp.data ");
		
		TypedQuery<LancamentoParcela> typedQuery = entityManager.createQuery(sqlBuilder.toString(), LancamentoParcela.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, EnumSet.of(LancamentoStatus.LAN, LancamentoStatus.BXD));
		typedQuery.setParameter(3, cartaoCredito);
		
		if(idsNotIn != null && !idsNotIn.isEmpty()) {
			typedQuery.setParameter(6, idsNotIn);
		}
		
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}


	public List<LancamentoParcela> findAll(CartaoCredito cartaoCredito, List<Long> ids) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT lp ");
		sqlBuilder.append("FROM LancamentoParcela lp ");
		sqlBuilder.append("LEFT OUTER JOIN lp.baixaItem bi ");
		sqlBuilder.append("WHERE lp.lancamento.cliente = ?1 ");
		sqlBuilder.append("AND lp.status IN ?2 ");
		sqlBuilder.append("AND (lp.cartaoCredito = ?3 OR bi.cartaoCredito = ?3) ");
		
		sqlBuilder.append("AND lp.id IN ?4 ");
		
		sqlBuilder.append("ORDER BY lp.data ");
		
		TypedQuery<LancamentoParcela> typedQuery = entityManager.createQuery(sqlBuilder.toString(), LancamentoParcela.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, EnumSet.of(LancamentoStatus.LAN, LancamentoStatus.BXD));
		typedQuery.setParameter(3, cartaoCredito);
		typedQuery.setParameter(4, ids);
		
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	
	}
	
}
