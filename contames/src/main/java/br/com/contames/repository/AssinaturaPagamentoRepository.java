package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Assinatura;
import br.com.contames.entity.AssinaturaPagamento;
import br.com.contames.enumeration.AssinaturaPagamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class AssinaturaPagamentoRepository extends Repository<AssinaturaPagamento, Long> {

	private final UsuarioLogged usuarioLogged;
	
	public AssinaturaPagamentoRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(AssinaturaPagamento.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}

	public AssinaturaPagamento findByToken(String token) {
		TypedQuery<AssinaturaPagamento> typedQuery = getEntityManager().createQuery("SELECT ap FROM AssinaturaPagamento ap WHERE ap.token = ?1", AssinaturaPagamento.class);
		typedQuery.setParameter(1, token);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;	
		}
	}
	
	public AssinaturaPagamento findByTxnId(String txn_id) {
		TypedQuery<AssinaturaPagamento> typedQuery = getEntityManager().createQuery("SELECT ap FROM AssinaturaPagamento ap WHERE ap.txnId = ?1", AssinaturaPagamento.class);
		typedQuery.setParameter(1, txn_id);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;	
		}
	}
	
	public AssinaturaPagamento findLastByStatus(Assinatura assinatura, AssinaturaPagamentoStatus status) {
		TypedQuery<AssinaturaPagamento> typedQuery = getEntityManager().createQuery("SELECT ap FROM AssinaturaPagamento ap WHERE ap.assinatura = ?1 AND ap.status = ?2 ORDER BY ap.id DESC", AssinaturaPagamento.class);
		typedQuery.setParameter(1, assinatura);
		typedQuery.setParameter(2, status);
		typedQuery.setFirstResult(0);
		typedQuery.setMaxResults(1);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;	
		}
	}

	public AssinaturaPagamento findLastStd(Assinatura assinatura) {
		return findLastByStatus(assinatura, AssinaturaPagamentoStatus.STD);
	}
	
	public AssinaturaPagamento findLastCon(Assinatura assinatura) {
		return findLastByStatus(assinatura, AssinaturaPagamentoStatus.CON);
	}
	
	public AssinaturaPagamento findLastByStatus(String profileId, AssinaturaPagamentoStatus status) {
		TypedQuery<AssinaturaPagamento> typedQuery = getEntityManager().createQuery("SELECT ap FROM AssinaturaPagamento ap WHERE ap.profileId = ?1 AND ap.status = ?2 ORDER BY ap.id DESC", AssinaturaPagamento.class);
		typedQuery.setParameter(1, profileId);
		typedQuery.setParameter(2, status);
		typedQuery.setFirstResult(0);
		typedQuery.setMaxResults(1);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;	
		}
	}

	public List<AssinaturaPagamento> findAllByClient() {
		TypedQuery<AssinaturaPagamento> typedQuery = getEntityManager().createQuery("SELECT ap FROM AssinaturaPagamento ap WHERE ap.assinatura.cliente = ?1 ORDER BY ap.id DESC", AssinaturaPagamento.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;	
		}
	}
	
}
