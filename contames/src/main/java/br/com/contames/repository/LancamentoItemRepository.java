package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Lancamento;
import br.com.contames.entity.LancamentoItem;
import br.com.contames.session.UsuarioLogged;

@Component
public class LancamentoItemRepository extends Repository<LancamentoItem, Long> {

	private final UsuarioLogged usuarioLogged;
	
	public LancamentoItemRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(LancamentoItem.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}

	public List<LancamentoItem> findAll(Long id) {
		TypedQuery<LancamentoItem> typedQuery = entityManager.createQuery("SELECT li FROM LancamentoItem li WHERE li.lancamento.id = ?1", LancamentoItem.class);
		typedQuery.setParameter(1, id);
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	@Override
	public LancamentoItemRepository save(LancamentoItem lancamentoItem) {
		
		if(lancamentoItem.getId() == null) {
			
			entityManager.persist(lancamentoItem);
		
		} else {
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<LancamentoItem> update = cb.createCriteriaUpdate(LancamentoItem.class);
			Root<LancamentoItem> root = update.from(LancamentoItem.class);
			
			update.set("seq", lancamentoItem.getSeq());
			update.set("obs", lancamentoItem.getObs());
			update.set("quant", lancamentoItem.getQuant());
			update.set("valorUnit", lancamentoItem.getValorUnit());
			update.set("valor", lancamentoItem.getValor());
			update.set("desconto", lancamentoItem.getDesconto());
			update.set("total", lancamentoItem.getTotal());
			update.set("perc", lancamentoItem.getPerc());
			update.where(cb.equal(root.get("id"), lancamentoItem.getId()));
			entityManager.createQuery(update).executeUpdate();
		}
		
		return this;
	}
	
	
	public LancamentoItemRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM LancamentoItem li WHERE li.id IN ?1 AND li.lancamento.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}

	public LancamentoItemRepository delete(Lancamento lancamento) {
		Query query = entityManager.createQuery("DELETE FROM LancamentoItem li WHERE li.lancamento = ?1");
		query.setParameter(1, lancamento);
		query.executeUpdate();
		return this;
	}
	
}
