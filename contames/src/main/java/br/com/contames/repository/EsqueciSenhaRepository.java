package br.com.contames.repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.EsqueciSenha;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.Status;

@Component
public class EsqueciSenhaRepository extends Repository<EsqueciSenha, String> {
	
	public EsqueciSenhaRepository(EntityManager entityManager) {
		super(EsqueciSenha.class, entityManager);
	}

	public void inativar(Pessoa usuario) {
		Query query = getEntityManager().createQuery("UPDATE EsqueciSenha es SET es.status = ?1, es.alteracao = ?2 WHERE es.usuario = ?3 AND es.status = ?4");
		query.setParameter(1, Status.INA);
		query.setParameter(2, LocalDateTime.now());
		query.setParameter(3, usuario);
		query.setParameter(4, Status.STD);
		query.executeUpdate();
	}

}
