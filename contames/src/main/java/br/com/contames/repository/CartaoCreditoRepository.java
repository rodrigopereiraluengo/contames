package br.com.contames.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.Status;
import br.com.contames.session.UsuarioLogged;

@Component
public class CartaoCreditoRepository extends Repository<CartaoCredito, Long> {
	
	private final UsuarioLogged usuarioLogged;
	
	public CartaoCreditoRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(CartaoCredito.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}
	
	
	@Override
	public CartaoCredito find(Long id) {
		TypedQuery<CartaoCredito> typedQuery = entityManager.createQuery("SELECT c FROM CartaoCredito c WHERE c.id = ?1 AND c.cliente = ?2", CartaoCredito.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public List<CartaoCredito> findAll() {
		TypedQuery<CartaoCredito> typedQuery = entityManager.createQuery("SELECT c FROM CartaoCredito c WHERE c.cliente = ?1 ORDER BY c.isPrincipal DESC, c.nome", CartaoCredito.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		return typedQuery.getResultList();
	}
	
	
	public CartaoCredito findPrincipal() {
		TypedQuery<CartaoCredito> typedQuery = entityManager.createQuery("SELECT c FROM CartaoCredito c WHERE c.cliente = ?1 AND c.isPrincipal = true AND c.status = ?2", CartaoCredito.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, Status.ATI);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	public boolean exists(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(c) FROM Cartao c WHERE c.cliente = ?1");
		query.setParameter(1, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	
	public boolean exists(CartaoCredito cartaoCredito) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
		Root<CartaoCredito> root = cqCount.from(CartaoCredito.class);
		
		cqCount.select(cb.count(root));
		List<Predicate> predicateList = new ArrayList<Predicate>();
		predicateList.add(cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
		predicateList.add(cb.equal(root.get("bandeira"), cartaoCredito.getBandeira()));
		predicateList.add(cb.equal(root.get("nome"), cartaoCredito.getNome()));
				
		if(cartaoCredito.getId() != null) {
			predicateList.add(cb.notEqual(root.get("id"), cartaoCredito.getId()));
		}
		
		cqCount.where(predicateList.toArray(new Predicate[predicateList.size()]));
		
		TypedQuery<Long> typedQuery = entityManager.createQuery(cqCount);
		
		return typedQuery.getSingleResult() > 0;
	}
	
	
	@Override
	public CartaoCreditoRepository save(CartaoCredito cartaoCredito) {
		cartaoCredito.setCliente(usuarioLogged.getCliente());
		cartaoCredito.setCriacao(LocalDateTime.now());
		cartaoCredito.setUsuCriacao(usuarioLogged.getUsuario());
		
		if(cartaoCredito.getId() == null) {
			entityManager.persist(cartaoCredito);
		} else {
			
			cartaoCredito.setAlteracao(LocalDateTime.now());
			cartaoCredito.setUsuAlteracao(usuarioLogged.getUsuario());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<CartaoCredito> update = cb.createCriteriaUpdate(CartaoCredito.class);
			Root<CartaoCredito> root = update.from(CartaoCredito.class);
			update.set("bandeira", cartaoCredito.getBandeira());
			update.set("status", cartaoCredito.getStatus());
			update.set("isPrincipal", cartaoCredito.getIsPrincipal());
			update.set("nome", cartaoCredito.getNome());
			update.set("titular", cartaoCredito.getTitular());
			update.set("limite", cartaoCredito.getLimite());
			if(!isUtilizado(cartaoCredito.getId())) {
				update.set("dataFatuAnt", cartaoCredito.getDataFatuAnt());
				update.set("valorFaturaAnt", cartaoCredito.getValorFaturaAnt());
				update.set("limiteDisponivelAnt", cartaoCredito.getLimiteDisponivelAnt());
			}
			update.set("limiteDisponivel", cartaoCredito.getLimiteDisponivel());
			update.set("limiteUtilizado", cartaoCredito.getLimiteUtilizado());
			update.set("diaVencimento", cartaoCredito.getDiaVencimento());
			update.set("diasPagar", cartaoCredito.getDiasPagar());
			update.set("obs", cartaoCredito.getObs());
			update.set("alteracao", cartaoCredito.getAlteracao());
			update.set("usuAlteracao", cartaoCredito.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), cartaoCredito.getId()), cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
			entityManager.createQuery(update).executeUpdate();
			
			atualizaLimiteDisponivel();
		}
		
		return this;
	}
	
	public Boolean isUtilizado(Long id) {
		if(id == null) {
			return false;
		}
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT EXISTS (");
		sqlBuilder.append("SELECT id FROM LancamentoParcela WHERE cartaoCredito_id = ?1 AND status = ?2");
		sqlBuilder.append(") OR EXISTS (");
		sqlBuilder.append("SELECT id FROM BaixaItem WHERE cartaoCredito_id = ?1 AND (status = ?3 OR status = ?4)");
		sqlBuilder.append(") OR EXISTS (");
		sqlBuilder.append("SELECT id FROM Transferencia WHERE cartaoCredito_id = ?1 AND status = ?2");
		sqlBuilder.append(")");
		Query query = entityManager.createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, id);
		query.setParameter(2, LancamentoStatus.LAN.toString());
		query.setParameter(3, LancamentoStatus.BXD.toString());
		query.setParameter(4, LancamentoStatus.COM.toString());
		return (Boolean) query.getSingleResult();
	}
	
	public boolean existLancamentoParcela(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(lp) FROM LancamentoParcela lp WHERE lp.cartaoCredito.id = ?1 AND lp.lancamento.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	
	public boolean existBaixaItem(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(bi) FROM BaixaItem bi WHERE bi.cartaoCredito.id = ?1 AND bi.baixa.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}

	
	public CartaoCreditoRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM CartaoCredito cc WHERE cc.id IN ?1 AND cc.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	
	public List<CartaoCredito> findAllActive() {
		TypedQuery<CartaoCredito> typedQuery = entityManager.createQuery("SELECT c FROM CartaoCredito c WHERE c.cliente = ?1 AND c.status = ?2 ORDER BY c.bandeira, c.nome", CartaoCredito.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, Status.ATI);
		return typedQuery.getResultList();
	}
	
	
	public CartaoCreditoRepository atualizaLimiteDisponivel() {
		
		// Atualiza Limite Disponivel
		TypedQuery<CartaoCredito> typedQuery = entityManager.createQuery("UPDATE CartaoCredito cc SET cc.limiteDisponivel = COALESCE(cc.limite, 0) - COALESCE(cc.limiteUtilizado, 0) WHERE cc.cliente = ?1", CartaoCredito.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.executeUpdate();
		
		return this;
	}


	public boolean existsItemLancAuto(Long id) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT i.id FROM Item i WHERE i.cartaoCredito_id = ?1 AND i.isLancAuto)");
		query.setParameter(1, id);
		return (boolean) query.getSingleResult();
	}
	
}
