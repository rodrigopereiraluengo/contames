package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.LancamentoFatura;
import br.com.contames.session.UsuarioLogged;

@Component
public class LancamentoFaturaRepository extends Repository<LancamentoFatura, Long> {

	private final UsuarioLogged usuarioLogged;
	
	public LancamentoFaturaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(LancamentoFatura.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}
	
	public LancamentoFaturaRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM LancamentoFatura lf WHERE lf.id IN ?1 AND li.lancamento.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}

	public List<LancamentoFatura> findAll(Long id, List<Long> lancamentoFaturaDelete) {
		TypedQuery<LancamentoFatura> typedQuery = entityManager.createQuery("SELECT lf FROM LancamentoFatura lf WHERE lf.lancamento.id = ?1 AND lf.lancamento.cliente = ?2 AND lf.id IN ?3", LancamentoFatura.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		typedQuery.setParameter(3, lancamentoFaturaDelete);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	public List<LancamentoFatura> findAll(Long id) {
		TypedQuery<LancamentoFatura> typedQuery = entityManager.createQuery("SELECT lf FROM LancamentoFatura lf WHERE lf.lancamento.id = ?1 AND lf.lancamento.cliente = ?2 ORDER BY lf.seq", LancamentoFatura.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
}
