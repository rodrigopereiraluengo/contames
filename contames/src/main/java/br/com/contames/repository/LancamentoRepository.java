package br.com.contames.repository;

import java.util.EnumSet;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.CartaoCredito;
import br.com.contames.entity.Lancamento;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.DocTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class LancamentoRepository extends Repository<Lancamento, Long> {

	private final UsuarioLogged usuarioLogged;
	private final PessoaRepository pessoaRepository;

	public LancamentoRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, PessoaRepository pessoaRepository) {
		super(Lancamento.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.pessoaRepository = pessoaRepository;
	}
	
	
	@Override
	public Lancamento find(Long id) {
		TypedQuery<Lancamento> typedQuery = entityManager.createQuery("SELECT l FROM Lancamento l WHERE l.id = ?1 AND l.cliente = ?2", Lancamento.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public LancamentoRepository delete(Long id) {
		Query query = entityManager.createQuery("DELETE FROM Lancamento l WHERE l.id = ?1 AND l.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}

	
	public boolean existsDoc(Long id, String numeroDoc, Pessoa favorecido) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT COUNT(l.id) FROM Lancamento l WHERE l.cliente = ?1 AND l.tipoDoc IN ?2 AND l.numeroDoc = ?3 AND l.favorecido = ?4");
		if(id != null) {
			sqlBuilder.append(" AND l.id <> ?5");
		}
		TypedQuery<Long> typedQuery = entityManager.createQuery(sqlBuilder.toString(), Long.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, EnumSet.of(DocTipo.NF, DocTipo.CF, DocTipo.RC));
		typedQuery.setParameter(3, numeroDoc);
		typedQuery.setParameter(4, favorecido);
		if(id != null) {
			typedQuery.setParameter(5, id);
		}
		return typedQuery.getSingleResult() > 0;
	}
	
	
	@Override
	public LancamentoRepository save(Lancamento lancamento) {
		
		if(lancamento.getId() == null) {
			
			lancamento.setCliente(usuarioLogged.getCliente());
			lancamento.setCriacao(LocalDateTime.now());
			lancamento.setUsuCriacao(usuarioLogged.getUsuario());
			
			entityManager.persist(lancamento);
		
		} else {
			
			lancamento.setAlteracao(LocalDateTime.now());
			lancamento.setUsuAlteracao(usuarioLogged.getUsuario());
			
			boolean isClosed = isClosed(lancamento.getId());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Lancamento> update = cb.createCriteriaUpdate(Lancamento.class);
			Root<Lancamento> root = update.from(Lancamento.class);
			
			update.set("data", lancamento.getData());
			update.set("favorecido", lancamento.getFavorecido());
			if(!isClosed) {
				update.set("status", lancamento.getStatus());
				update.set("tipoDoc", lancamento.getTipoDoc());
				update.set("valorDoc", lancamento.getValorDoc());
				update.set("cartaoCredito", lancamento.getCartaoCredito());
				update.set("diferenca", lancamento.getDiferenca());
				update.set("limite", lancamento.getLimite());
				update.set("limiteDisponivel", lancamento.getLimiteDisponivel());
				update.set("limiteUtilizado", lancamento.getLimiteUtilizado());
				update.set("totalLancamentoFatura", lancamento.getTotalLancamentoFatura());
			}
			
			update.set("totalLancamentoItemQuant", lancamento.getTotalLancamentoItemQuant());
			update.set("totalLancamentoItemValorUnit", lancamento.getTotalLancamentoItemValorUnit());
			update.set("totalLancamentoItemValor", lancamento.getTotalLancamentoItemValor());
			update.set("totalLancamentoItemDesconto", lancamento.getTotalLancamentoItemDesconto());
			update.set("totalLancamentoItem", lancamento.getTotalLancamentoItem());
			update.set("quantLancamentoItem", lancamento.getQuantLancamentoItem());
			
			update.set("arquivoDoc", lancamento.getArquivoDoc());
			update.set("dataDoc", lancamento.getDataDoc());
			update.set("numeroDoc", lancamento.getNumeroDoc());
			if(isClosed && !DocTipo.FT.equals(lancamento.getTipoDoc())) {
				update.set("valorDoc", lancamento.getValorDoc());
				update.set("tipoDoc", lancamento.getTipoDoc());
				
			}
			update.set("quantLancamentoFatura", lancamento.getQuantLancamentoFatura());
			update.set("totalLancamentoParcela", lancamento.getTotalLancamentoParcela());
			update.set("quantLancamentoParcela", lancamento.getQuantLancamentoParcela());
			update.set("percLancamentoRateio", lancamento.getPercLancamentoRateio());
			update.set("totalLancamentoRateio", lancamento.getTotalLancamentoRateio());
			update.set("quantLancamentoRateio", lancamento.getQuantLancamentoRateio());
			update.set("obs", lancamento.getObs());
			update.set("alteracao", lancamento.getAlteracao());
			update.set("usuAlteracao", lancamento.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), lancamento.getId()));
			entityManager.createQuery(update).executeUpdate();
		
		}
		
		return this;
	}
		
	
	public boolean isClosed(Long id) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT id FROM LancamentoParcela WHERE lancamento_id = ?1 AND status NOT IN('BXD', 'COM'))");
		query.setParameter(1, id);
		return !(boolean) query.getSingleResult();
	}


	public Lancamento ultimaFatura(CartaoCredito cartaoCredito) {
		Query query = entityManager.createNativeQuery("SELECT l.* FROM Lancamento l WHERE l.cartaoCredito_id = ?1 AND l.status = ?2 ORDER BY l.id DESC LIMIT 1", Lancamento.class);
		query.setParameter(1, cartaoCredito.getId());
		query.setParameter(2, LancamentoStatus.LAN.toString());
		try {
			return (Lancamento) query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}