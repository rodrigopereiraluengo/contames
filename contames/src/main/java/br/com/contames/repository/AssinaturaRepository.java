package br.com.contames.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.joda.time.LocalDate;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Assinatura;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.AssinaturaStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class AssinaturaRepository extends Repository<Assinatura, Long> {
	
	private final UsuarioLogged usuarioLogged;
	
	public AssinaturaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(Assinatura.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}

	public Assinatura findByProfileId(String profileId) {
		TypedQuery<Assinatura> typedQuery = getEntityManager().createQuery("SELECT a FROM Assinatura a WHERE a.profileId = ?1", Assinatura.class);
		typedQuery.setParameter(1, profileId);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public Long qtdeConta() {
		Query query = getEntityManager().createQuery("SELECT COUNT(c) FROM Conta c WHERE c.cliente = ?1");
		query.setParameter(1, usuarioLogged.getCliente());
		return (Long) query.getSingleResult();
	}
	
	public Long qtdeCartaoCredito() {
		Query query = getEntityManager().createQuery("SELECT COUNT(cc) FROM CartaoCredito cc WHERE cc.cliente = ?1");
		query.setParameter(1, usuarioLogged.getCliente());
		return (Long) query.getSingleResult();
	}
	
	public Integer  qtdeLancamento() {
		Query query = getEntityManager().createNativeQuery("SELECT fn_qtdeLancamentos(?1)");
		query.setParameter(1, usuarioLogged.getCliente().getId());
		return (Integer) query.getSingleResult();
	}

	public BigDecimal tablesSize() {
		Query query = getEntityManager().createNativeQuery("SELECT fn_tableSize(?1)");
		query.setParameter(1, usuarioLogged.getCliente().getId());
		return (BigDecimal) query.getSingleResult();
	}

	public Long qtdeUsuarios() {
		Query query = getEntityManager().createQuery("SELECT COUNT(u) FROM Pessoa u WHERE (u.cliente.id = ?1 OR u.id = ?1) AND u.email IS NOT NULL AND u.senha IS NOT NULL");
		query.setParameter(1, usuarioLogged.getCliente().getId());
		return (Long) query.getSingleResult();
	}
	
	public Long qtdeUsuarios(Long id) {
		Query query = getEntityManager().createQuery("SELECT COUNT(u) FROM Pessoa u WHERE (u.cliente.id = ?1 OR u.id = ?1) AND u.email IS NOT NULL AND u.senha IS NOT NULL AND u.id <> ?2");
		query.setParameter(1, usuarioLogged.getCliente().getId());
		query.setParameter(2, id);
		return (Long) query.getSingleResult();
	}

	public void cancelar() {
		getEntityManager()
			.createQuery("DELETE FROM Cliente c WHERE c.id = ?1")
			.setParameter(1, usuarioLogged.getCliente().getId())
			.executeUpdate();
	}

	public Assinatura findBy(Pessoa cliente) {
		TypedQuery<Assinatura> typedQuery = getEntityManager().createQuery("SELECT a FROM Assinatura a WHERE a.cliente = ?1", Assinatura.class);
		typedQuery.setParameter(1, cliente);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	public List<Assinatura> findAllCancelado20dias() {
		TypedQuery<Assinatura> typedQuery = getEntityManager().createQuery("SELECT a FROM Assinatura a WHERE a.status = ?1 AND a.canceladoEm = ?2", Assinatura.class);
		typedQuery.setParameter(1, AssinaturaStatus.CAN);
		typedQuery.setParameter(2, LocalDate.now().minusDays(20));
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	public List<Assinatura> findAllCancelado31dias() {
		TypedQuery<Assinatura> typedQuery = getEntityManager().createQuery("SELECT a FROM Assinatura a WHERE a.status = ?1 AND a.canceladoEm = ?2", Assinatura.class);
		typedQuery.setParameter(1, AssinaturaStatus.CAN);
		typedQuery.setParameter(2, LocalDate.now().minusDays(31));
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}
