package br.com.contames.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.CacheUsage;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.BaixaItem;
import br.com.contames.entity.Cheque;
import br.com.contames.entity.TalaoCheque;
import br.com.contames.session.UsuarioLogged;

@Component
public class ChequeRepository extends Repository<Cheque, Long> {

	private final TalaoChequeRepository talaoChequeRepository;
	private final UsuarioLogged usuarioLogged;
	
	public ChequeRepository(EntityManager entityManager, TalaoChequeRepository talaoChequeRepository, UsuarioLogged usuarioLogged) {
		super(Cheque.class, entityManager);
		this.talaoChequeRepository = talaoChequeRepository;
		this.usuarioLogged = usuarioLogged;
	}

	public boolean exists(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(c) FROM Cheque c WHERE c.id = ?1 AND (c.cliente = ?2 OR c.talao.cliente = ?2)");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		try {
			return (Long) query.getSingleResult() > 0;
		}catch(NoResultException e) {
			return false;
		}
	}
	
	public List<Cheque> talao(Long id) {
		TypedQuery<Cheque> typedQuery = entityManager.createQuery("SELECT c FROM Cheque c WHERE c.talaoCheque.conta.cliente = ?1 AND c.talaoCheque.id = ?2 ORDER BY c.numero", Cheque.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, id);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	public void saveCom(Cheque cheque) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaUpdate<Cheque> update = cb.createCriteriaUpdate(Cheque.class);
		Root<Cheque> root = update.from(Cheque.class);
		
		update.set("banco", cheque.getBanco());
		update.set("nome", cheque.getNome());
		update.set("obs", cheque.getObs());
		
		update.where(cb.equal(root.get("id"), cheque.getId()));
		entityManager.createQuery(update).executeUpdate();
	}
	
	public void deleteIfUnique(Cheque cheque) {
		
		Query query = entityManager.createNativeQuery("SELECT COUNT(lp.id) FROM LancamentoParcela lp WHERE lp.cheque_id = ?1 UNION SELECT COUNT(bi.id) FROM BaixaItem bi WHERE bi.cheque_id = ?1");
		query.setParameter(1, cheque.getId());
		
		Long count = (Long) query.getSingleResult();
		
		if(count.equals(1l)) {
			entityManager
				.createQuery("DELETE FROM Cheque c WHERE c.id = ?1 AND c.cliente = ?2")
				.setParameter(1, cheque.getId())
				.executeUpdate();
		}
		
	}
	
	@Override
	public ChequeRepository save(Cheque cheque) {
		
		if(cheque.getTalaoCheque() != null && cheque.getTalaoCheque().getId() != null) {
			cheque.setTalaoCheque(talaoChequeRepository.find(cheque.getTalaoCheque().getId()));
		}
		
		if(cheque.getId() == null) {
			cheque.setCliente(usuarioLogged.getCliente());
			cheque.setCriacao(LocalDateTime.now());
			cheque.setUsuCriacao(usuarioLogged.getUsuario());
		} else {
			cheque.setAlteracao(LocalDateTime.now());
			cheque.setUsuAlteracao(usuarioLogged.getUsuario());
		}
		super.save(cheque);
		return this;
	}

	public Cheque findEqual(Cheque cheque) {
		
		if(cheque != null && cheque.getId() == null) {
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Cheque> cq = cb.createQuery(Cheque.class);
			Root<Cheque> root = cq.from(Cheque.class);
			
			cq.select(root);
			List<Predicate> predicateList = new ArrayList<Predicate>();
			predicateList.add(cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
			predicateList.add(cb.equal(root.get("numero"), cheque.getNumero()));
					
			// Verifica Talao
			if(cheque.getTalaoCheque() != null) {
				predicateList.add(cb.equal(root.get("talaoCheque"), cheque.getTalaoCheque()));
			}
			
			// Verifica Banco
			if(cheque.getBanco() != null && cheque.getBanco().getId() != null) {
				predicateList.add(cb.equal(root.get("banco"), cheque.getBanco()));
			}
			
			// Verifica Pessoa
			if(cheque.getNome() != null && cheque.getNome().getId() != null) {
				predicateList.add(cb.equal(root.get("nome"), cheque.getNome()));
			}
			
			cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
			
			TypedQuery<Cheque> typedQuery = entityManager.createQuery(cq);
			try {
				return typedQuery.getSingleResult();	
			}catch(NoResultException e) {
				return null;
			}
			
		}
		
		return null;
	}

	public Cheque findBy(Integer numero, TalaoCheque talaoCheque) {
		TypedQuery<Cheque> typedQuery = entityManager.createQuery("SELECT c FROM Cheque c WHERE c.numero = ?1 AND c.talaoCheque = ?2", Cheque.class);
		typedQuery.setParameter(1, numero);
		typedQuery.setParameter(2, talaoCheque);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	public void updateValor(Map<Cheque, List<BaixaItem>> baixaItemChequeMapList) {
		if(!baixaItemChequeMapList.isEmpty()) {
			for(Cheque cheque : baixaItemChequeMapList.keySet()) {
				Double valor = 0.0;
				for(BaixaItem baixaItem : baixaItemChequeMapList.get(cheque)) {
					valor += baixaItem.getValor();
				}
				cheque.setValor(valor);
				save(cheque);
			}
		}
	}

	public List<Cheque> findAll(TalaoCheque talaoCheque) {
		TypedQuery<Cheque> typedQuery = entityManager.createQuery("SELECT c FROM Cheque c WHERE c.talaoCheque = ?1 ORDER BY c.numero DESC", Cheque.class);
		typedQuery.setHint(QueryHints.CACHE_USAGE, CacheUsage.NoCache);
		typedQuery.setParameter(1, talaoCheque);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	@Override
	public Cheque find(Long id) {
		TypedQuery<Cheque> typedQuery = entityManager.createQuery("SELECT c FROM Cheque c WHERE c.id = ?1 AND c.cliente = ?2", Cheque.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}