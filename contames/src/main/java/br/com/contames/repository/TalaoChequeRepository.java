package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Conta;
import br.com.contames.entity.TalaoCheque;
import br.com.contames.enumeration.Status;
import br.com.contames.session.UsuarioLogged;

@Component
public class TalaoChequeRepository extends Repository<TalaoCheque, Long> {

	private final UsuarioLogged usuarioLogged;
	
	public TalaoChequeRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(TalaoCheque.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}

	public List<TalaoCheque> findAll(Conta conta) {
		TypedQuery<TalaoCheque> typedQuery = entityManager.createQuery("SELECT t FROM TalaoCheque t WHERE t.conta = ?1 ORDER BY t.index", TalaoCheque.class);
		typedQuery.setParameter(1, conta);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public List<TalaoCheque> findAllActive() {
		TypedQuery<TalaoCheque> typedQuery = entityManager.createQuery("SELECT t FROM TalaoCheque t WHERE t.conta.cliente = ?1 AND t.status = ?2 ORDER BY t.conta.nome, t.index", TalaoCheque.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, Status.ATI);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	@Override
	public TalaoCheque find(Long id) {
		TypedQuery<TalaoCheque> typedQuery = entityManager.createQuery("SELECT t FROM TalaoCheque t WHERE t.id = ?1 AND t.conta.cliente = ?2", TalaoCheque.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	@Override
	public TalaoChequeRepository save(TalaoCheque talaoCheque) {
		
		talaoCheque.setCriacao(LocalDateTime.now());
		talaoCheque.setUsuCriacao(usuarioLogged.getUsuario());
		
		
		if(talaoCheque.getId() == null) {
			entityManager.persist(talaoCheque);
		} else {
			
			talaoCheque.setAlteracao(LocalDateTime.now());
			talaoCheque.setUsuAlteracao(usuarioLogged.getUsuario());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<TalaoCheque> update = cb.createCriteriaUpdate(TalaoCheque.class);
			Root<TalaoCheque> root = update.from(TalaoCheque.class);
			update.set("data", talaoCheque.getData());
			update.set("status", talaoCheque.getStatus());
			update.set("seqInicio", talaoCheque.getSeqInicio());
			update.set("folhas", talaoCheque.getFolhas());
			update.set("seqFim", talaoCheque.getSeqFim());
			update.set("obs", talaoCheque.getObs());
			update.set("alteracao", talaoCheque.getAlteracao());
			update.set("usuAlteracao", talaoCheque.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), talaoCheque.getId()));
			entityManager.createQuery(update).executeUpdate();
		}
		
		return this;
	}
	
	public TalaoChequeRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM TalaoCheque t WHERE t.id IN ?1 AND t.conta.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	public Conta getConta(Long id) {
		TypedQuery<Conta> query = entityManager.createQuery("SELECT t.conta FROM TalaoCheque t WHERE t.conta.cliente = ?1 AND t.id = ?2", Conta.class);
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, id);
		try {
			return query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public List<TalaoCheque> findAll(List<Long> ids) {
		TypedQuery<TalaoCheque> typedQuery = entityManager.createQuery("SELECT t FROM TalaoCheque t WHERE t.conta.cliente = ?1 AND t.id IN ?2", TalaoCheque.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, ids);
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public List<Long> findContaId(List<Long> ids) {
		TypedQuery<Long> query = entityManager.createQuery("SELECT t.conta.id FROM TalaoCheque t WHERE t.conta.cliente = ?1 AND t.id IN ?2", Long.class);
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, ids);
		try {
			return query.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public void movs(TalaoCheque talaoCheque) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");
		sqlBuilder.append("c.id, ");
		sqlBuilder.append("c.numero, ");
		sqlBuilder.append("COALESCE(lp.valor, bi.valor) valor, ");
		sqlBuilder.append("COALESCE(lp.status, bi.status, t.status) status ");
		sqlBuilder.append("FROM Cheque c ");
		sqlBuilder.append("LEFT JOIN BaixaItem bi ON c.id = bi.cheque_id AND bi.status IN('STD', 'BXD', 'CAN', 'COM') ");
		sqlBuilder.append("LEFT JOIN LancamentoParcela lp ON c.id = lp.cheque_id AND lp.status IN('STD', 'LAN', 'CAN') ");
		sqlBuilder.append("LEFT JOIN Transferencia t ON c.id = t.cheque_id ");
		
		
	}

	public boolean existsChequeForaIntervaloInicio(TalaoCheque talaoCheque) {
		return (boolean) entityManager
				.createNativeQuery("SELECT EXISTS(SELECT id FROM Cheque WHERE talaoCheque_id = ?1 AND numero < ?2)")
				.setParameter(1, talaoCheque.getId())
				.setParameter(2, talaoCheque.getSeqInicio())
				.getSingleResult();
		
	}

	public boolean existsChequeForaIntervaloFim(TalaoCheque talaoCheque) {
		return (boolean) entityManager
				.createNativeQuery("SELECT EXISTS(SELECT id FROM Cheque WHERE talaoCheque_id = ?1 AND numero > ?2)")
				.setParameter(1, talaoCheque.getId())
				.setParameter(2, talaoCheque.getSeqFim())
				.getSingleResult();
	}

	public Integer minCheque(TalaoCheque talaoCheque) {
		// TODO Auto-generated method stub
		return (Integer) entityManager
				.createNativeQuery("SELECT MIN(numero) FROM Cheque WHERE talaoCheque_id = ?1")
				.setParameter(1, talaoCheque.getId())
				.getSingleResult();
	}
	
	public Integer maxCheque(TalaoCheque talaoCheque) {
		// TODO Auto-generated method stub
		return (Integer) entityManager
				.createNativeQuery("SELECT MAX(numero) FROM Cheque WHERE talaoCheque_id = ?1")
				.setParameter(1, talaoCheque.getId())
				.getSingleResult();
	}
	
	
	public boolean existsCheque(TalaoCheque talaoCheque) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT c.id FROM Cheque c WHERE c.talaoCheque_id = ?1)");
		query.setParameter(1, talaoCheque.getId());
		return (boolean) query.getSingleResult();
	}
	
}
