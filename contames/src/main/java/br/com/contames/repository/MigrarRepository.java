package br.com.contames.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Migrar;

@Component
public class MigrarRepository extends Repository<Migrar, Long> {

	public MigrarRepository(EntityManager entityManager) {
		super(Migrar.class, entityManager);
	}

	public Migrar findByToken(String token) {
		TypedQuery<Migrar> typedQuery = getEntityManager().createQuery("SELECT m FROM Migrar m WHERE m.token = ?1", Migrar.class);
		typedQuery.setParameter(1, token);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;	
		}
	}

	public Migrar findByProfileId(String profileId) {
		TypedQuery<Migrar> typedQuery = getEntityManager().createQuery("SELECT m FROM Migrar m WHERE m.profileId = ?1", Migrar.class);
		typedQuery.setParameter(1, profileId);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public Migrar findByOldProfileId(String oldProfileId) {
		TypedQuery<Migrar> typedQuery = getEntityManager().createQuery("SELECT m FROM Migrar m WHERE m.oldProfileId = ?1", Migrar.class);
		typedQuery.setParameter(1, oldProfileId);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}
