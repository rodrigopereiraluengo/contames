package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Compensacao;
import br.com.contames.entity.Conta;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class CompensacaoRepository extends Repository<Compensacao, Long> {
	
	private final ContaRepository contaRepository;
	private final CartaoCreditoRepository cartaoCreditoRepository;
	private final UsuarioLogged usuarioLogged;
	
	public CompensacaoRepository(EntityManager entityManager, ContaRepository contaRepository, CartaoCreditoRepository cartaoCreditoRepository, UsuarioLogged usuarioLogged) {
		super(Compensacao.class, entityManager);
		this.contaRepository = contaRepository;
		this.cartaoCreditoRepository = cartaoCreditoRepository;
		this.usuarioLogged = usuarioLogged;
	}
	
	@Override
	public Compensacao find(Long id) {
		TypedQuery<Compensacao> typedQuery = entityManager.createQuery("SELECT c FROM Compensacao c WHERE c.id = ?1 AND c.cliente = ?2", Compensacao.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	
	@Override
	public CompensacaoRepository save(Compensacao compensacao) {
		
		boolean isClosed = false;
		
		if(compensacao.getId() == null) {
			
			compensacao.setCliente(usuarioLogged.getCliente());
			compensacao.setCriacao(LocalDateTime.now());
			compensacao.setUsuCriacao(usuarioLogged.getUsuario());
			
			entityManager.persist(compensacao);
			
		} else {
			
			compensacao.setAlteracao(LocalDateTime.now());
			compensacao.setUsuAlteracao(usuarioLogged.getUsuario());
			
			isClosed = isClosed(compensacao);
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Compensacao> update = cb.createCriteriaUpdate(Compensacao.class);
			Root<Compensacao> root = update.from(Compensacao.class);
			
			if(!isClosed) {
				update.set("data", compensacao.getData());
				update.set("saldoAnterior", compensacao.getSaldoAnterior());
				update.set("saldo", compensacao.getSaldo());
				update.set("diferenca", compensacao.getDiferenca());
				update.set("totalDebito", compensacao.getTotalDebito());
				update.set("totalCredito", compensacao.getTotalCredito());
				update.set("total", compensacao.getTotal());
				update.set("quantCompensacaoItem", compensacao.getQuantCompensacaoItem());
				update.set("status", compensacao.getStatus());
			}
			
			update.set("obs", compensacao.getObs());
			update.set("alteracao", compensacao.getAlteracao());
			update.set("usuAlteracao", compensacao.getUsuAlteracao());
			
			update.where(cb.equal(root.get("id"), compensacao.getId()));
			entityManager.createQuery(update).executeUpdate();
		
		}
		
		atualizaSaldo(compensacao.getConta(), compensacao);
		
		return this;
	}

	private void atualizaSaldo(Conta conta) {
		atualizaSaldo(conta, null);
	}
	
	private void atualizaSaldo(Conta conta, Compensacao compensacao) {
		
		Compensacao ultimaCompensacao = contaRepository.findLastCompensacao(conta);
		
		Double saldo = conta.getSaldoInicial();
		
		if(ultimaCompensacao != null) {
			saldo = ultimaCompensacao.getSaldo();
			
			if(compensacao != null && ultimaCompensacao.equals(compensacao)) {
				saldo = compensacao.getSaldo();
			}
			
		}
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaUpdate<Conta> update = cb.createCriteriaUpdate(Conta.class);
		Root<Conta> root = update.from(Conta.class);
		
		update.set("saldoAtual", saldo);
		update.set("ultimaCompensacao", ultimaCompensacao);
		if(conta.getSaldoLimite() != null) {
			update.set("saldoLimiteDisp", saldo < 0 ? conta.getSaldoLimite() + saldo : conta.getSaldoLimite());
		}
		update.where(cb.equal(root.get("id"), conta.getId()));
		entityManager.createQuery(update).executeUpdate();
	}
	
	
	
	/**
	 * Remove Compensacao
	 * @param ids
	 * @return CompensacaoRepository
	 */
	public CompensacaoRepository deleteList(List<Long> ids) {
		
		if(ids != null && !ids.isEmpty()) {
		
			// Atualizar LancamentoParcela
			entityManager
				.createQuery("UPDATE LancamentoParcela lp SET lp.status = ?1 WHERE lp.id IN(SELECT bi.lancamentoParcela.id FROM BaixaItem bi WHERE bi.id IN(SELECT ci.baixaItem.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.compensacao.cliente = ?3))")
				.setParameter(1, LancamentoStatus.BXD)
				.setParameter(2, ids)
				.setParameter(3, usuarioLogged.getCliente())
				.executeUpdate();
			
			// Atualizar Cheque
			entityManager
				.createQuery("UPDATE Cheque c SET c.status = ?1 WHERE c.id IN(SELECT ci.baixaItem.cheque.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.compensacao.cliente = ?3)")
				.setParameter(1, LancamentoStatus.BXD)
				.setParameter(2, ids)
				.setParameter(3, usuarioLogged.getCliente())
				.executeUpdate();
			
			
			// Atualizar BaixaItem
			entityManager
				.createQuery("UPDATE BaixaItem bi SET bi.status = ?1 WHERE bi.id IN(SELECT ci.baixaItem.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.compensacao.cliente = ?3)")
				.setParameter(1, LancamentoStatus.BXD)
				.setParameter(2, ids)
				.setParameter(3, usuarioLogged.getCliente())
				.executeUpdate();
			
			// Atualizar Transferencia Cheque
			entityManager
				.createQuery("UPDATE Cheque c SET c.status = ?1 WHERE c.id IN(SELECT ci.transferencia.cheque.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.compensacao.cliente = ?3)")
				.setParameter(1, LancamentoStatus.LAN)
				.setParameter(2, ids)
				.setParameter(3, usuarioLogged.getCliente())
				.executeUpdate();
			
			// Atualiza Transferencia Origem
			entityManager.createQuery("UPDATE Transferencia t SET t.statusOrigem = ?1 WHERE t.id IN(SELECT ci.transferencia.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.tipo = ?3 AND ci.compensacao.cliente = ?4)")
				.setParameter(1, LancamentoStatus.LAN)
				.setParameter(2, ids)
				.setParameter(3, ItemTipo.DES)
				.setParameter(4, usuarioLogged.getCliente())
				.executeUpdate();
			
			// Atualiza Transferencia Destino
			entityManager.createQuery("UPDATE Transferencia t SET t.statusDestino = ?1 WHERE t.id IN(SELECT ci.transferencia.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.tipo = ?3 AND ci.compensacao.cliente = ?4)")
				.setParameter(1, LancamentoStatus.LAN)
				.setParameter(2, ids)
				.setParameter(3, ItemTipo.REC)
				.setParameter(4, usuarioLogged.getCliente())
				.executeUpdate();
			
			// Atualiza Transferencia
			entityManager.createQuery("UPDATE Transferencia t SET t.status = ?1 WHERE t.statusOrigem = ?1 AND t.statusDestino = ?1 AND t.id IN(SELECT ci.transferencia.id FROM CompensacaoItem ci WHERE ci.compensacao.id IN ?2 AND ci.compensacao.cliente = ?3)")
				.setParameter(1, LancamentoStatus.LAN)
				.setParameter(2, ids)
				.setParameter(3, usuarioLogged.getCliente())
				.executeUpdate();
			
			Conta conta = findContaByCompensacaoId(ids.get(0));
			
			entityManager
				.createQuery("DELETE FROM Compensacao c WHERE c.id IN ?1 AND c.cliente = ?2")
				.setParameter(1, ids)
				.setParameter(2, usuarioLogged.getCliente())
				.executeUpdate();
			
			
			atualizaSaldo(conta);
		
			cartaoCreditoRepository.atualizaLimiteDisponivel();
		}
		
		
		return this;
	}

	
	
	private Conta findContaByCompensacaoId(Long id) {
		TypedQuery<Conta> typedQuery = entityManager.createQuery("SELECT c.conta co FROM Compensacao c WHERE c.id = ?1 AND c.cliente = ?2", Conta.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	
	
	public List<Compensacao> findAll(List<Long> ids) {
		TypedQuery<Compensacao> typedQuery = entityManager.createQuery("SELECT c FROM Compensacao c WHERE c.id IN ?1 AND c.cliente = ?2", Compensacao.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	
	public Compensacao findAnterior(Conta conta) {
		return findAnterior(conta, null);
	}
	
	
	
	public Compensacao findAnterior(Conta conta, Long compensacao) {
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT c ");
		sqlBuilder.append("FROM Compensacao c ");
		sqlBuilder.append("WHERE c.cliente = ?1 ");
		sqlBuilder.append("AND c.conta = ?2 ");
		if(compensacao != null) {
			sqlBuilder.append("AND c.id <> ?3 ");
		}
		sqlBuilder.append("ORDER BY c.id DESC");
		
		TypedQuery<Compensacao> typedQuery = entityManager.createQuery(sqlBuilder.toString(), Compensacao.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, conta);
		if(compensacao != null) {
			typedQuery.setParameter(3, compensacao);
		}
		typedQuery.setFirstResult(0);
		typedQuery.setMaxResults(1);
		
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	
	}
	
	
	
	public boolean isClosed(Compensacao compensacao) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT EXISTS(SELECT id FROM Compensacao WHERE conta_id = ?1 AND _data > ?2");
		if(compensacao.getId() != null) {
			sqlBuilder.append(" AND id <> ?3");	
		}
		sqlBuilder.append(")");
		Query query = entityManager.createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, compensacao.getConta().getId());
		query.setParameter(2, compensacao.getData().toDate());
		if(compensacao.getId() != null) {
			query.setParameter(3, compensacao.getId());
		}
		return (boolean) query.getSingleResult();
	}

	public LocalDate minData(Conta conta) {
		Query query = entityManager.createNativeQuery("SELECT minDataCompensacao(?1)");
		query.setParameter(1, conta.getId());
		LocalDate localDate = new LocalDate(query.getSingleResult());
		if(localDate.dayOfWeek().get() == 6) {
			localDate = localDate.plusDays(2);
		} else if(localDate.dayOfWeek().get() == 0) {
			localDate = localDate.plusDays(1);
		}
		return localDate;
	}
	

}
