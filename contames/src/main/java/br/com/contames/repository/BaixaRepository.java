package br.com.contames.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Baixa;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.session.UsuarioLogged;

@Component
public class BaixaRepository extends Repository<Baixa, Long> {

	private final UsuarioLogged usuarioLogged;
	
	
	public BaixaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(Baixa.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}
	
	
	@Override
	public Baixa find(Long id) {
		TypedQuery<Baixa> typedQuery = entityManager.createQuery("SELECT b FROM Baixa b WHERE b.id = ?1 AND b.cliente = ?2", Baixa.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public BaixaRepository save(Baixa baixa) {
		
		if(baixa.getId() == null) {
			
			baixa.setCliente(usuarioLogged.getCliente());
			baixa.setCriacao(LocalDateTime.now());
			baixa.setUsuCriacao(usuarioLogged.getUsuario());
			
			entityManager.persist(baixa);
		
		} else {
			
			baixa.setAlteracao(LocalDateTime.now());
			baixa.setUsuAlteracao(usuarioLogged.getUsuario());
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Baixa> update = cb.createCriteriaUpdate(Baixa.class);
			Root<Baixa> root = update.from(Baixa.class);
			update.set("data", baixa.getData());
			update.set("quantBaixaItem", baixa.getQuantBaixaItem());
			update.set("totalLancamentoParcela", baixa.getTotalLancamentoParcela());
			update.set("totalBaixaItem", baixa.getTotalBaixaItem());
			update.set("totalDiferenca", baixa.getTotalDiferenca());
			update.set("obs", baixa.getObs());
			update.set("status", baixa.getStatus());
			update.set("alteracao", baixa.getAlteracao());
			update.set("usuAlteracao", baixa.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), baixa.getId()));
			entityManager.createQuery(update).executeUpdate();
		
		}
		
		return this;
	}
	
	
	/**
	 * Busca List por id
	 * @param ids
	 * @return List<Baixa>
	 */
	public List<Baixa> findAll(List<Long> ids) {
		
		TypedQuery<Baixa> typedQuery = entityManager.createQuery("SELECT b FROM Baixa b WHERE b.id IN ?1 AND b.cliente = ?2", Baixa.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Remove Baixa
	 * @param ids
	 * @return BaixaRepository
	 */
	public BaixaRepository deleteList(List<Long> ids) {
		
		// Atualizar LancamentoParcela
		entityManager
			.createQuery("UPDATE LancamentoParcela lp SET lp.status = ?1 WHERE lp.id IN(SELECT bi.lancamentoParcela.id FROM BaixaItem bi WHERE bi.baixa.id IN ?2)")
			.setParameter(1, LancamentoStatus.LAN)
			.setParameter(2, ids)
			.executeUpdate();
		
		// Exclui Baixa
		entityManager
			.createQuery("DELETE FROM Baixa b WHERE b.id IN ?1 AND b.cliente = ?2")
			.setParameter(1, ids)
			.setParameter(2, usuarioLogged.getCliente())
			.executeUpdate();
		
		return this;
	}
	
	
	public boolean isClosed(Long id) {
		if(id == null) {
			return false;
		}
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT id FROM BaixaItem WHERE baixa_id = ?1 AND status != 'COM')");
		query.setParameter(1, id);
		return !(boolean) query.getSingleResult();
	}
	
}
