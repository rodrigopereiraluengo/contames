package br.com.contames.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Compensacao;
import br.com.contames.entity.Conta;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.Status;
import br.com.contames.session.UsuarioLogged;

@Component
public class ContaRepository extends Repository<Conta, Long> {

	private final UsuarioLogged usuarioLogged;
	
	public ContaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged) {
		super(Conta.class, entityManager);
		this.usuarioLogged = usuarioLogged;
	}

	public boolean exists(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(c) FROM Conta c WHERE c.cliente = ?1");
		query.setParameter(1, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	public boolean exists(Conta conta) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
		Root<Conta> root = cqCount.from(Conta.class);
		
		cqCount.select(cb.count(root));
		List<Predicate> predicateList = new ArrayList<Predicate>();
		predicateList.add(cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
		predicateList.add(cb.equal(root.get("nome"), conta.getNome()));
				
		if(conta.getId() != null) {
			predicateList.add(cb.notEqual(root.get("id"), conta.getId()));
		}
		
		cqCount.where(predicateList.toArray(new Predicate[predicateList.size()]));
		
		TypedQuery<Long> typedQuery = entityManager.createQuery(cqCount);
		
		return typedQuery.getSingleResult() > 0;
	}
	
	
	@Override
	public ContaRepository save(Conta conta) {
		conta.setCliente(usuarioLogged.getCliente());
		conta.setSaldoAtual(conta.getSaldoInicial());
		
		if(conta.getBanco() != null && conta.getBanco().getId() == null) {
			conta.setBanco(null);
		}
		
		if(conta.getTitular() != null && conta.getTitular().getId() == null) {
			conta.setTitular(null);
		}
		
		if(conta.getId() == null) {
			conta.setCriacao(LocalDateTime.now());
			conta.setUsuCriacao(usuarioLogged.getUsuario());
			entityManager.persist(conta);
		} else {
			
			conta.setAlteracao(LocalDateTime.now());
			conta.setUsuAlteracao(usuarioLogged.getUsuario());
			
			Boolean isCompensado = isCompensado(conta);
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaUpdate<Conta> update = cb.createCriteriaUpdate(Conta.class);
			Root<Conta> root = update.from(Conta.class);
			update.set("nome", conta.getNome());
			if(!isCompensado) {
				update.set("saldoInicial", conta.getSaldoInicial());
				update.set("saldoAtual", conta.getSaldoAtual());
			}
			update.set("saldoLimite", conta.getSaldoLimite());
			update.set("saldoLimiteDisp", conta.getSaldoLimiteDisp());
			update.set("titular", conta.getTitular());
			update.set("banco", conta.getBanco());
			update.set("agencia", conta.getAgencia());
			update.set("agenciaNum", conta.getAgenciaNum());
			update.set("contaNum", conta.getContaNum());
			update.set("isPrincipal", conta.getIsPrincipal());
			update.set("isPoupanca", conta.getIsPoupanca());
			update.set("obs", conta.getObs());
			update.set("status", conta.getStatus());
			update.set("alteracao", conta.getAlteracao());
			update.set("usuAlteracao", conta.getUsuAlteracao());
			update.where(cb.equal(root.get("id"), conta.getId()), cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
			entityManager.createQuery(update).executeUpdate();
			
			
			
			if(!isCompensado) {
				entityManager
					.createQuery("UPDATE Compensacao c SET c.saldoAnterior = ?1, c.diferenca = (?1 - c.saldo) WHERE c.conta = ?2")
					.setParameter(1, conta.getSaldoInicial())
					.setParameter(2, conta)
					.executeUpdate();
			}
			
			
		}
		
		updateTalaoFolhas(conta.getId());
		
		return this;
	}
	
	
	public boolean existsBaixa(Long id) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT bi.id FROM BaixaItem bi WHERE bi.conta_id = ?1)");
		query.setParameter(1, id);
		return (boolean) query.getSingleResult();
	}
	
	
	public boolean existsTransferencia(Long id) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT t.id FROM Transferencia t WHERE t.conta_id = ?1 OR t.destino_id = ?1)");
		query.setParameter(1, id);
		return (boolean) query.getSingleResult();
	}
	
	
	public boolean existsItemLancAuto(Long id) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT i.id FROM Item i WHERE i.conta_id = ?1 AND i.isLancAuto)");
		query.setParameter(1, id);
		return (boolean) query.getSingleResult();
	}
	
	
	@Override
	public Conta find(Long id) {
		TypedQuery<Conta> query = entityManager.createQuery("SELECT c FROM Conta c WHERE c.id = ?1 AND c.cliente = ?2", Conta.class);
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		try {
			return query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	public Conta findActive(Long id) {
		TypedQuery<Conta> query = entityManager.createQuery("SELECT c FROM Conta c WHERE c.id = ?1 AND c.cliente = ?2 AND c.status = ?3", Conta.class);
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		query.setParameter(3, Status.ATI);
		try {
			return query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public ContaRepository delete(Long id) {
		Query query = entityManager.createQuery("DELETE FROM Conta c WHERE c.id = ?1 AND c.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	public ContaRepository deleteList(List<Long> ids) {
		Query query = entityManager.createQuery("DELETE FROM Conta c WHERE c.id IN ?1 AND c.cliente = ?2");
		query.setParameter(1, ids);
		query.setParameter(2, usuarioLogged.getCliente());
		query.executeUpdate();
		return this;
	}
	
	public List<Conta> findAllActive() {
		return findAllActive(null);
	}
	
	public List<Conta> findAllActive(Boolean isPoupanca) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT c FROM Conta c WHERE c.cliente = ?1 AND c.status = ?2");
		if(isPoupanca != null) {
			sqlBuilder.append(" AND COALESCE(c.isPoupanca, false) = ?3");
		}
		sqlBuilder.append(" ORDER BY c.nome");
		TypedQuery<Conta> typedQuery = entityManager.createQuery(sqlBuilder.toString(), Conta.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, Status.ATI);
		if(isPoupanca != null) {
			typedQuery.setParameter(3, isPoupanca);
		}
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public Conta findPrincipal() {
		TypedQuery<Conta> typedQuery = entityManager.createQuery("SELECT c FROM Conta c WHERE c.cliente = ?1 AND c.status = ?2 AND c.isPrincipal = ?3 ORDER BY c.nome", Conta.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, Status.ATI);
		typedQuery.setParameter(3, true);
		try {
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public Conta findUnique() {
		
		TypedQuery<Long> countQuery = entityManager.createQuery("SELECT COUNT(c) FROM Conta c WHERE c.cliente = ?1 AND c.status = ?2", Long.class);
		countQuery.setParameter(1, usuarioLogged.getCliente());
		countQuery.setParameter(2, Status.ATI);
		
		Long count = countQuery.getSingleResult();
		
		if(count.intValue() == 1) {
			TypedQuery<Conta> typedQuery = entityManager.createQuery("SELECT c FROM Conta c WHERE c.cliente = ?1 AND c.status = ?2", Conta.class);
			typedQuery.setParameter(1, usuarioLogged.getCliente());
			typedQuery.setParameter(2, Status.ATI);
			
			try {
				return typedQuery.getSingleResult();
			} catch(NoResultException e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public Conta findUniqueContaCorrente() {
		TypedQuery<Long> countQuery = entityManager.createQuery("SELECT COUNT(c) FROM Conta c WHERE c.cliente = ?1 AND c.status = ?2 AND COALESCE(c.isPoupanca, false) = ?3", Long.class);
		countQuery.setParameter(1, usuarioLogged.getCliente());
		countQuery.setParameter(2, Status.ATI);
		countQuery.setParameter(3, false);
		Long count = countQuery.getSingleResult();
		
		if(count.intValue() == 1) {
			TypedQuery<Conta> typedQuery = entityManager.createQuery("SELECT c FROM Conta c WHERE c.cliente = ?1 AND c.status = ?2 AND COALESCE(c.isPoupanca, false) = ?3", Conta.class);
			typedQuery.setParameter(1, usuarioLogged.getCliente());
			typedQuery.setParameter(2, Status.ATI);
			typedQuery.setParameter(3, false);
			try {
				return typedQuery.getSingleResult();
			} catch(NoResultException e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	
	public Compensacao findLastCompensacao(Conta conta) {
		TypedQuery<Compensacao> typedQuery = entityManager.createQuery("SELECT c FROM Compensacao c WHERE c.conta = ?1 AND c.status = ?2 ORDER BY c.id DESC", Compensacao.class);
		typedQuery.setParameter(1, conta);
		typedQuery.setParameter(2, LancamentoStatus.COM);
		try {
			return typedQuery.setMaxResults(1).getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public Boolean isCompensado(Conta conta) {
		if(conta.getId() == null) {
			return false;
		} else {
			return isCompensado(conta.getId());
		}
	}
	
	public Boolean isCompensado(Long id) {
		Query query = entityManager.createNativeQuery("SELECT EXISTS(SELECT id FROM Compensacao WHERE conta_id = ?1 AND status = ?2)");
		query.setParameter(1, id);
		query.setParameter(2, LancamentoStatus.COM.toString());
		return (Boolean) query.getSingleResult();
	}
	
	public ContaRepository updateTalaoFolhas(Long id) {
		
		Map<String, Object> result = (Map<String, Object>) entityManager
				.createNativeQuery("SELECT COUNT(t.id) AS quantTalaoCheque, SUM(t.folhas) AS quantTalaoChequeFolhas FROM TalaoCheque t WHERE t.conta_id = ?1")
				.setParameter(1, id)
				.setHint(QueryHints.RESULT_TYPE, ResultType.Map)
				.getSingleResult();
		
		entityManager
			.createQuery("UPDATE Conta c SET c.quantTalaoCheque = ?1, c.quantTalaoChequeFolhas = ?2 WHERE c.id = ?3")
			.setParameter(1, result.get("quanttalaocheque"))
			.setParameter(2, result.get("quanttalaochequefolhas"))
			.setParameter(3, id)
			.executeUpdate();
		
		return this;
	}
	
	
	@Override
	public List<Conta> findAll() {
		TypedQuery<Conta> typedQuery = entityManager.createQuery("SELECT c FROM Conta c WHERE c.cliente = ?1 ORDER BY c.isPrincipal DESC, c.nome", Conta.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	public List<Conta> find(List<Long> ids) {
		TypedQuery<Conta> typedQuery = entityManager.createQuery("SELECT c FROM Conta c WHERE c.id IN ?1 AND c.cliente = ?2 ORDER BY c.nome", Conta.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

}
