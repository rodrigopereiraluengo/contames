package br.com.contames.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.component.Settings;
import br.com.contames.entity.Item;
import br.com.contames.entity.Pessoa;
import br.com.contames.enumeration.PessoaTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.StringHelper;

@Component
public class PessoaRepository extends Repository<Pessoa, Long> {

	private final UsuarioLogged usuarioLogged;
	private final Settings settings;
	private final Localization localization;
		
	public PessoaRepository(EntityManager entityManager, UsuarioLogged usuarioLogged, Settings settings, Localization localization) {
		super(Pessoa.class, entityManager);
		this.usuarioLogged = usuarioLogged;
		this.settings = settings;
		this.localization = localization;
	}
	
	public Boolean exists(String email) {
		Query query = getEntityManager().createNativeQuery("SELECT EXISTS(SELECT id FROM Pessoa WHERE email = ?1 AND email IS NOT NULL)");
		query.setParameter(1, email);
		return (Boolean) query.getSingleResult();
	}
	
	@Override
	public Pessoa find(Long id) {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE p.id = ?1 AND (p.cliente.id = ?2 OR p.id = ?2)", Pessoa.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente().getId());
		try {
			return typedQuery.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	public Pessoa findByEmail(String email) {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE p.email = ?1", Pessoa.class);
		typedQuery.setParameter(1, email);
		try {
			return typedQuery.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	
	
	public Pessoa findOrCliente(Long id) {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE (p.id = ?1 AND p.cliente.id = ?2) OR (p.id = ?1 AND p.id = ?2 AND p.cliente IS NULL)", Pessoa.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente().getId());
		try {
			return typedQuery.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}
		
	public boolean exists(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(p) FROM Pessoa p WHERE (p.id = ?1 AND p.cliente.id = ?2) OR p.id = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente().getId());
		return (Long) query.getSingleResult() > 0;
	}
	
	public boolean exists(Pessoa pessoa) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Pessoa> root = cq.from(Pessoa.class);
		cq.select(cb.count(root));
		
		List<Predicate> predicateList = new ArrayList<Predicate>();
		predicateList.add(cb.equal(root.get("cliente"), usuarioLogged.getCliente()));
		
		if(pessoa.getPessoaTipo().equals(PessoaTipo.J)) {
			if(StringUtils.isNotBlank(pessoa.getCnpj())) {
				predicateList.add(cb.or(cb.equal(root.get("cnpj"), pessoa.getCnpj()), cb.equal(root.get("razaoSocial"), pessoa.getRazaoSocial())));
			} else {
				predicateList.add(cb.equal(root.get("razaoSocial"), pessoa.getRazaoSocial()));
			}
		} else {
			if(StringUtils.isNotBlank(pessoa.getCpf())) {
				predicateList.add(cb.or(cb.equal(root.get("cpf"), pessoa.getCpf()), cb.equal(root.get("nome"), pessoa.getNome())));
			} else {
				predicateList.add(cb.equal(root.get("nome"), pessoa.getNome()));
			}
		}
		
		if(pessoa.getId() != null) {
			predicateList.add(cb.notEqual(root.get("id"), pessoa.getId()));
		}
		
		cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
		
		TypedQuery<Long> typedQuery = entityManager.createQuery(cq);
		
		Long count = typedQuery.getSingleResult();
		
		
		return count > 0;
	}
	
	@Override
	public PessoaRepository save(Pessoa pessoa) {
		save(pessoa, false);
		return this;
	}
		
	public PessoaRepository save(Pessoa pessoa, boolean isUsuario) {
		
		if(PessoaTipo.J.equals(pessoa.getPessoaTipo())) {
			pessoa.setSexo(null);
		}
		
		if(pessoa.getId() == null) {
			pessoa.setCliente(usuarioLogged.getCliente());
			pessoa.setCriacao(LocalDateTime.now());
			pessoa.setUsuCriacao(usuarioLogged.getUsuario());
			
			pessoa.setAlteracao(null);
			pessoa.setUsuAlteracao(null);
			
			if(!isUsuario && !usuarioLogged.getIsCliente()) {
				pessoa.setSenha(null);
			}
			
			entityManager.persist(pessoa);
		
		} else {
			
			pessoa.setAlteracao(LocalDateTime.now());
			pessoa.setUsuAlteracao(usuarioLogged.getUsuario());
			
			Pessoa pessoaSalva = find(pessoa.getId());
			
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("UPDATE Pessoa p SET ");
			sqlBuilder.append("p.pessoaTipo = ?1, ");
			sqlBuilder.append("p.nome = ?2, ");
			sqlBuilder.append("p.apelido = ?3, ");
			sqlBuilder.append("p.cpf = ?4, ");
			sqlBuilder.append("p.rg = ?5, ");
			sqlBuilder.append("p.sexo = ?6, ");
			sqlBuilder.append("p.tituloEleitor = ?7, ");
			sqlBuilder.append("p.razaoSocial = ?8, ");
			sqlBuilder.append("p.fantasia = ?9, ");
			sqlBuilder.append("p.cnpj = ?10, ");
			sqlBuilder.append("p.inscEst = ?11, ");
			sqlBuilder.append("p.inscMun = ?12, ");
			sqlBuilder.append("p.categoria = ?13, ");
			sqlBuilder.append("p.cep = ?14, ");
			sqlBuilder.append("p.endereco = ?15, ");
			sqlBuilder.append("p.numero = ?16, ");
			sqlBuilder.append("p.complemento = ?17, ");
			sqlBuilder.append("p.bairro = ?18, ");
			sqlBuilder.append("p.cidade = ?19, ");
			sqlBuilder.append("p.uf = ?20, ");
			sqlBuilder.append("p.fone1 = ?21, ");
			sqlBuilder.append("p.fone2 = ?22, ");
			sqlBuilder.append("p.fone3 = ?23, ");
			sqlBuilder.append("p.fone4 = ?24, ");
			
			if((isUsuario && !usuarioLogged.getUsuario().equals(pessoa)) || (!isUsuario && usuarioLogged.getIsCliente() && !usuarioLogged.getCliente().equals(pessoa))) {
				sqlBuilder.append("p.email = ?25, ");
				sqlBuilder.append("p.senha = ?26, ");
			} else if(!pessoaSalva.getIsUsuario()) {
				sqlBuilder.append("p.email = ?25, ");
			}
			
			sqlBuilder.append("p.site = ?27, ");
			sqlBuilder.append("p.obs = ?28, ");
			sqlBuilder.append("p.status = ?29, ");
			sqlBuilder.append("p.alteracao = ?30, ");
			sqlBuilder.append("p.usuAlteracao = ?31 ");
			sqlBuilder.append("WHERE (p.id = ?32 ");
			sqlBuilder.append("AND p.cliente.id = ?34) OR (p.id = ?32 AND p.id = ?33 AND p.cliente IS NULL) ");
			Query query = entityManager.createQuery(sqlBuilder.toString());
			query.setParameter(1, pessoa.getPessoaTipo());
			query.setParameter(2, pessoa.getNome());
			query.setParameter(3, pessoa.getApelido());
			query.setParameter(4, pessoa.getCpf());
			query.setParameter(5, pessoa.getRg());
			query.setParameter(6, pessoa.getSexo());
			query.setParameter(7, pessoa.getTituloEleitor());
			query.setParameter(8, pessoa.getRazaoSocial());
			query.setParameter(9, pessoa.getFantasia());
			query.setParameter(10, pessoa.getCnpj());
			query.setParameter(11, pessoa.getInscEst());
			query.setParameter(12, pessoa.getInscMun());
			query.setParameter(13, pessoa.getCategoria());
			query.setParameter(14, pessoa.getCep());
			query.setParameter(15, pessoa.getEndereco());
			query.setParameter(16, pessoa.getNumero());
			query.setParameter(17, pessoa.getComplemento());
			query.setParameter(18, pessoa.getBairro());
			query.setParameter(19, pessoa.getCidade());
			query.setParameter(20, pessoa.getUf());
			query.setParameter(21, pessoa.getFone1());
			query.setParameter(22, pessoa.getFone2());
			query.setParameter(23, pessoa.getFone3());
			query.setParameter(24, pessoa.getFone4());
			if((isUsuario && !usuarioLogged.getUsuario().equals(pessoa)) || (!isUsuario && usuarioLogged.getIsCliente() && (usuarioLogged.getCliente() == null || !usuarioLogged.getCliente().equals(pessoa)))) {
				query.setParameter(25, pessoa.getEmail());
				query.setParameter(26, pessoa.getSenha());
			} else if(!pessoaSalva.getIsUsuario()) {
				query.setParameter(25, pessoa.getEmail());
			}
			query.setParameter(27, pessoa.getSite());
			query.setParameter(28, pessoa.getObs());
			query.setParameter(29, pessoa.getStatus());
			query.setParameter(30, pessoa.getAlteracao());
			query.setParameter(31, pessoa.getUsuAlteracao());
			query.setParameter(32, pessoa.getId());
			query.setParameter(33, pessoa.getId());
			query.setParameter(34, usuarioLogged.getCliente().getId());
			query.executeUpdate();
			
			if(pessoa.getId().equals(usuarioLogged.getUsuario().getId())) {
				usuarioLogged.setUsuario(pessoa);
			}
		}
		
		return this;
	}

	public boolean existsLancamento(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(l) FROM Lancamento l WHERE l.favorecido.id = ?1 AND l.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	public boolean existsLancamentoRateio(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(lr) FROM LancamentoRateio lr WHERE lr.pessoa.id = ?1 AND lr.lancamento.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	public boolean existsContaTitular(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(c) FROM Conta c WHERE c.titular.id = ?1 AND c.cliente = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente());
		return (Long) query.getSingleResult() > 0;
	}
	
	@Override
	public PessoaRepository delete(Long id) {
		entityManager
			.createQuery("DELETE FROM Pessoa p WHERE p.id = ?1 AND p.cliente = ?2")
			.setParameter(1, id)
			.setParameter(2, usuarioLogged.getCliente())
			.executeUpdate();
		return this;
	}
	
	public PessoaRepository deleteList(List<Long> ids) {
		entityManager
			.createQuery("DELETE FROM Pessoa p WHERE p.id IN ?1 AND p.cliente = ?2")
			.setParameter(1, ids)
			.setParameter(2, usuarioLogged.getCliente())
			.executeUpdate();
		return this;
	}
	
	public List<Pessoa> findAllActiveWithCliente(PessoaTipo pessoaTipo, String categoria, String search) {
		if(search == null) {
			return null;
		} else {
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("SELECT p.id, ");
			sqlBuilder.append("p.pessoaTipo, ");
			sqlBuilder.append("COALESCE(p.apelido, p.nome, p.fantasia, p.razaoSocial) AS nome ");
			sqlBuilder.append("FROM Pessoa p ");
			sqlBuilder.append("LEFT JOIN Descricao AS Categoria ON p.categoria_id = Categoria.id ");
			sqlBuilder.append("WHERE (p.cliente_id = ?1 AND p.status = ?2 OR p.id = ?1) ");
			sqlBuilder.append("AND unaccent(LOWER(COALESCE(p.apelido, p.nome, p.fantasia, p.razaoSocial))) ILIKE ?3");
			if(pessoaTipo != null) {
				sqlBuilder.append(" AND p.pessoaTipo = ?4");
			}
			if(categoria != null) {
				sqlBuilder.append(" AND unaccent(LOWER(Categoria.nome)) = ?5");
			}
			sqlBuilder.append(" ORDER BY p.pessoaTipo DESC, COALESCE(p.apelido, p.nome, p.fantasia, p.razaoSocial)");
			Query query = entityManager.createNativeQuery(sqlBuilder.toString());
			query.setParameter(1, usuarioLogged.getCliente().getId());
			query.setParameter(2, Status.ATI.toString());
			query.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
			if(pessoaTipo != null) {
				query.setParameter(4, pessoaTipo.toString());
			}
			if(categoria != null) {
				query.setParameter(5, StringHelper.unAccent(categoria.toLowerCase()));
			}
			query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
			query.setFirstResult(0);
			query.setMaxResults((int) settings.getConfiguration().getFloat("page.size"));
			try {
				return (List<Pessoa>) query.getResultList();
			} catch(NoResultException e) { 
				return null;
			}
		}
	}
	
	public List<Pessoa> findAllActiveWithCliente() {
		TypedQuery<Pessoa> query = entityManager.createQuery("SELECT p FROM Pessoa p WHERE (p.cliente = ?1 AND p.status = ?2 OR p.id = ?3) ORDER BY p.pessoaTipo DESC, COALESCE(p.nome, p.razaoSocial)", Pessoa.class);
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, Status.ATI);
		query.setParameter(3, usuarioLogged.getCliente().getId());
		try {
			return query.getResultList();
		} catch(NoResultException e) { 
			return null;
		}
	}
	
	public List<Pessoa> findAllActive() {
		TypedQuery<Pessoa> query = entityManager.createQuery("SELECT p FROM Pessoa p WHERE p.cliente = ?1 AND p.status = ?2 ORDER BY p.pessoaTipo DESC, COALESCE(p.nome, p.razaoSocial)", Pessoa.class);
		query.setParameter(1, usuarioLogged.getCliente());
		query.setParameter(2, Status.ATI);
		try {
			return query.getResultList();
		} catch(NoResultException e) { 
			return null;
		}
	}
	
	public boolean existsOrCliente(Long id) {
		Query query = entityManager.createQuery("SELECT COUNT(p) FROM Pessoa p WHERE (p.id = ?1 AND p.cliente.id = ?2) OR p.id = ?2");
		query.setParameter(1, id);
		query.setParameter(2, usuarioLogged.getCliente().getId());
		return (Long) query.getSingleResult() > 0;
	}
	
	public Pessoa find(String email, String senha) {
		Query query = getEntityManager().createQuery("SELECT p FROM Pessoa p WHERE p.email = ?1 AND p.senha = ?2 AND p.senha IS NOT NULL");
		query.setParameter(1, email);
		query.setParameter(2, senha);
		return (Pessoa) query.getSingleResult();
	}

	public Pessoa findBanco(Long id) {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE p.id = ?1 AND p.cliente.id = ?2 AND p.pessoaTipo = ?3 AND p.categoria.nome = ?4", Pessoa.class);
		typedQuery.setParameter(1, id);
		typedQuery.setParameter(2, usuarioLogged.getCliente().getId());
		typedQuery.setParameter(3, PessoaTipo.J);
		typedQuery.setParameter(4, localization.getMessage("categoriaBanco"));
		try {
			return typedQuery.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}

	public List<Pessoa> search(String search) {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE  (p.cliente = ?1 AND p.status = ?2 OR p.id = ?4) AND LOWER(FUNC('unaccent', COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome))) LIKE ?3 ORDER BY LOWER(FUNC('unaccent', COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome)))", Pessoa.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente());
		typedQuery.setParameter(2, Status.ATI);
		typedQuery.setParameter(3, StringHelper.unAccent(search.toLowerCase()) + "%");
		typedQuery.setParameter(4, usuarioLogged.getCliente().getId());
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
		
	}

	public List<Pessoa> findAllUsuario() {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE (p.cliente.id = ?1 AND p.email IS NOT NULL AND p.senha IS NOT NULL) OR p.id = ?1 ORDER BY COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome)", Pessoa.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente().getId());
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}
	
	public List<Pessoa> findAllUsuarioActive() {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE (p.cliente.id = ?1 AND p.email IS NOT NULL AND p.senha IS NOT NULL) OR p.id = ?1 AND p.status = ?2 ORDER BY COALESCE(p.fantasia, p.razaoSocial, p.apelido, p.nome)", Pessoa.class);
		typedQuery.setParameter(1, usuarioLogged.getCliente().getId());
		typedQuery.setParameter(2, Status.ATI);
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}

	public Boolean exists(String email, Pessoa pessoa) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT EXISTS(SELECT id FROM Pessoa WHERE email = ?1 AND email IS NOT NULL");
		if(pessoa != null && pessoa.getId() != null) {
			sqlBuilder.append(" AND id <> ?2");
		}
		sqlBuilder.append(")");
		Query query = getEntityManager().createNativeQuery(sqlBuilder.toString());
		query.setParameter(1, email);
		if(pessoa != null && pessoa.getId() != null) {
			query.setParameter(2, pessoa.getId());
		}
		return (Boolean) query.getSingleResult();
	}
	
	public List<Pessoa> findAllClientes() {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE p.cliente IS NULL AND p.status = ?1", Pessoa.class);
		typedQuery.setParameter(1, Status.ATI);
		try {
			return typedQuery.getResultList();
		}catch(NoResultException e) {
			return null;
		}
	}

	public void atualizaSenha(Pessoa usuario, String senha) {
		Query query = entityManager.createQuery("UPDATE Pessoa p SET p.senha = ?1 WHERE p = ?2");
		query.setParameter(1, senha);
		query.setParameter(2, usuario);
		query.executeUpdate();		
	}

	/**
	 * Recupera uma List com Pessoa
	 * @param ids
	 * @return List<Pessoa>
	 */
	public List<Pessoa> find(List<Long> ids) {
		TypedQuery<Pessoa> typedQuery = entityManager.createQuery("SELECT p FROM Pessoa p WHERE p.id IN ?1 AND p.cliente = ?2 ORDER BY p.nome", Pessoa.class);
		typedQuery.setParameter(1, ids);
		typedQuery.setParameter(2, usuarioLogged.getCliente());
		try {
			return typedQuery.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	
}