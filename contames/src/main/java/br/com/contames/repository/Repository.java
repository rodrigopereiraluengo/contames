package br.com.contames.repository;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.TypedQuery;

import net.vidageek.mirror.dsl.Mirror;
import br.com.contames.session.UsuarioLogged;

public abstract class Repository <T, ID extends Serializable> {

	@Inject
	private UsuarioLogged usuarioLogged;
	protected final Class<T> clazz;
	protected final EntityManager entityManager;
		
	protected Repository(Class<T> clazz, EntityManager entityManager) {
		this.clazz = clazz;
		this.entityManager = entityManager;
	}
		
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public T find(ID id) {
		try {
			return entityManager.find(clazz, id);
		}catch(Exception e) {
			return null;
		}
	}
	
	public List<T> findAll() {
		TypedQuery<T> query = entityManager.createQuery("SELECT e FROM " + clazz.getName() + " e", clazz);
		return query.getResultList();
	}
	
	public Repository<T, ID> insert(T entity) {
		if(entity != null) {
			entityManager.persist(entity);
		}
		return this;
	}
	
	public Repository<T, ID> insert(List<T> entityList) {
		if(entityList != null) {
			for(T entity : entityList) {
				insert(entity);
			}
		}
		return this;
	}
	
	public Repository<T, ID> update(T entity) {
		if(entity != null) {
			entityManager.merge(entity);
		}
		return this;
	}
	
	public Repository<T, ID> update(List<T> entityList) {
		if(entityList != null) {
			for(T entity : entityList) {
				update(entity);
			}
		}
		return this;
	}
	
	public Repository<T, ID> save(T entity) {
    	if(entity != null) {
            ID id = getId(entity);
            if(id == null) {
                entityManager.persist(entity);
            } else {
            	entityManager.merge(entity);
            }
		}
    	return this;
	}
	
	public Repository<T, ID> save(List<T> entityList) {
		for(T entity : entityList) {
			save(entity);
		}
		return this;
	}
	
	public Repository<T, ID> delete(T entity) {
		if(entity != null){
			entityManager.remove(entityManager.merge(entity));
		}
		return this;
	}
	
	public Repository<T, ID> delete(List<T> entityList) {
		if(entityList != null) {
			for(T entity : entityList) {
				delete(entity);
			}
		}
		return this;
	}
	
	public Repository<T, ID> delete(ID id) {
		if(id != null) {
			delete(find(id));
		}
		return this;
	}
	
	public Repository<T, ID> flush() {
		entityManager.flush();
		return this;
	}
		
	private ID getId(T entity) {
		
		for(Field field : new Mirror().on(entity.getClass()).reflectAll().fields()) {
			for(Annotation ann : field.getDeclaredAnnotations()){
				if(ann instanceof Id){
					try{
						@SuppressWarnings("unchecked")
						ID id = (ID) new Mirror().on(entity).invoke().getterFor(field);
						return id;
					}catch(Exception e){
						return null;
					}
				}
			}
		}
		return null;
	}
	
}
