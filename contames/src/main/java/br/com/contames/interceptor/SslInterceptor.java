package br.com.contames.interceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.contames.annotation.ForceSSL;
import br.com.contames.annotation.NoSSL;
import br.com.contames.annotation.Private;

@Intercepts
public class SslInterceptor implements Interceptor {
	
	@Inject
	private HttpServletRequest request;
	
	@Inject
	private Result result;
	
	@Inject
	private Environment environment;

	@Override
	public boolean accepts(ResourceMethod method) {
		// TODO Auto-generated method stub
		return 
				(method.containsAnnotation(ForceSSL.class) 
				|| method.containsAnnotation(NoSSL.class) 
				|| method.getResource().getType().isAnnotationPresent(ForceSSL.class)
				|| method.getResource().getType().isAnnotationPresent(NoSSL.class)
				|| method.containsAnnotation(Private.class) 
				|| method.getResource().getType().isAnnotationPresent(Private.class)) && environment.supports("production");
	}

	@Override
	public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {

		if("http".equals(request.getHeader("X-Scheme"))) {
			
			if(
			method.containsAnnotation(ForceSSL.class) 
			|| 
			(method.getResource().getType().isAnnotationPresent(ForceSSL.class) && !method.containsAnnotation(NoSSL.class))
			|| method.containsAnnotation(Private.class) 
			|| method.getResource().getType().isAnnotationPresent(Private.class)
			) {
				String url = StringUtils.removeEnd(request.getRequestURL().toString(), "/");
				String queryString = request.getQueryString();
			    if (queryString != null) {
			    	url += "?" + StringUtils.remove(queryString,  request.getRequestURI().toString() + "=&");
			    }
				result.redirectTo(StringUtils.replaceOnce(url, "http://", "https://"));
			} else {
				stack.next(method, resourceInstance);
			}
			
		} else {
			
			if(method.containsAnnotation(NoSSL.class) || (method.getResource().getType().isAnnotationPresent(NoSSL.class) && !method.containsAnnotation(ForceSSL.class))) {
				String url = StringUtils.removeEnd(request.getRequestURL().toString(), "/");
				String queryString = request.getQueryString();
			    if (queryString != null) {
			    	url += "?" + StringUtils.remove(queryString,  request.getRequestURI().toString() + "=&");
			    }
				result.redirectTo(StringUtils.replaceOnce(url, "https://", "http://"));
			} else {
				stack.next(method, resourceInstance);
			}
			
		} 
		
	}

}
