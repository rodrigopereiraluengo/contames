package br.com.contames.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.view.Results;
import br.com.contames.annotation.AllowCliente;
import br.com.contames.annotation.AllowOnInactive;
import br.com.contames.annotation.Private;
import br.com.contames.annotation.Public;
import br.com.contames.controller.AdminController;
import br.com.contames.controller.LoginController;
import br.com.contames.enumeration.AssinaturaStatus;
import br.com.contames.repository.PessoaRepository;
import br.com.contames.session.UsuarioLogged;

@Intercepts
public class AuthenticationInterceptor implements Interceptor {

	private final UsuarioLogged usuarioLogged;
	private final Environment environment;
	private final PessoaRepository pessoaRepository;
	private final Result result;
	private final HttpServletRequest request;
			
	public AuthenticationInterceptor(UsuarioLogged usuarioLogged, Environment environment, PessoaRepository pessoaRepository, Result result, HttpServletRequest request) {
		this.usuarioLogged = usuarioLogged;
		this.environment = environment;
		this.pessoaRepository = pessoaRepository;
		this.result = result;
		this.request = request;
	}
		
	@Override
	public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {
		
		if(!usuarioLogged.getIsLogged() && environment.supports("development")) {
			usuarioLogged.setUsuario(pessoaRepository.find("rpl@outlook.com", "krirho52"));
		}
		
		if(!usuarioLogged.getIsLogged()) {
			
			if("true".equals(request.getHeader("Ajax"))) {
				result.use(Results.http()).sendError(401);
			} else {
				result.redirectTo(LoginController.class).login(getUrl(request));
			}
		} else if(!usuarioLogged.getCliente().getAssinatura().getStatus().equals(AssinaturaStatus.ATI) && !method.containsAnnotation(AllowOnInactive.class)) {
			result.redirectTo(AdminController.class).assinatura(false);
		} else if(method.containsAnnotation(AllowCliente.class) && !usuarioLogged.getIsCliente()) {
			result.notFound();
		} else {
			
			stack.next(method, resourceInstance);
		}
	}

	@Override
	public boolean accepts(ResourceMethod method) {
		
		// Verifica se o usuario esta logado e o recurso e account
		//if(usuarioLogged.getIsLogged() && method.getResource().getType().equals(AccountController.class)) {
		//	return false;
		//}
		
		// Verifica se o metodo e publico
		if(method.containsAnnotation(Public.class)) {
			return false;
		}
		
		// Verifica se o metodo e privado ou se o recurso e privado
		if(method.containsAnnotation(Private.class) || method.getResource().getType().isAnnotationPresent(Private.class)) {
			return true;
		}
				
		return false;
	}
		

	public static String getUrl(HttpServletRequest request) {
		String reqUrl = request.getRequestURI().toString();
	    String queryString = request.getQueryString();
	    if (queryString != null) {
	        reqUrl += "?" + StringUtils.remove(queryString,  request.getRequestURI().toString() + "=&");
	    }
	    return reqUrl;
	}
	
}
