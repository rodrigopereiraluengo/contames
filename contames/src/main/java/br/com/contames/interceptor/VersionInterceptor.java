package br.com.contames.interceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.contames.component.ApplicationVersion;

@Intercepts
public class VersionInterceptor implements Interceptor {

	@Inject
	private Result result;
	
	@Inject
	private ApplicationVersion applicationVersion;
	
	@Inject
	private HttpServletRequest request;
	
	@Override
	public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {
		result.include("applicationVersion", applicationVersion.getVersion());
		result.include("requestURL", AuthenticationInterceptor.getUrl(request));
		stack.next(method, resourceInstance);
	}

	@Override
	public boolean accepts(ResourceMethod method) {
		// TODO Auto-generated method stub
		return true;
	}

}
