package br.com.contames.component;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.xml.sax.SAXException;

import urn.ebay.api.PayPalAPI.CreateRecurringPaymentsProfileReq;
import urn.ebay.api.PayPalAPI.CreateRecurringPaymentsProfileRequestType;
import urn.ebay.api.PayPalAPI.CreateRecurringPaymentsProfileResponseType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.GetRecurringPaymentsProfileDetailsReq;
import urn.ebay.api.PayPalAPI.GetRecurringPaymentsProfileDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetRecurringPaymentsProfileDetailsResponseType;
import urn.ebay.api.PayPalAPI.ManageRecurringPaymentsProfileStatusReq;
import urn.ebay.api.PayPalAPI.ManageRecurringPaymentsProfileStatusRequestType;
import urn.ebay.api.PayPalAPI.ManageRecurringPaymentsProfileStatusResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionRequestType;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.ActivationDetailsType;
import urn.ebay.apis.eBLBaseComponents.AutoBillType;
import urn.ebay.apis.eBLBaseComponents.BillingAgreementDetailsType;
import urn.ebay.apis.eBLBaseComponents.BillingCodeType;
import urn.ebay.apis.eBLBaseComponents.BillingPeriodDetailsType;
import urn.ebay.apis.eBLBaseComponents.BillingPeriodType;
import urn.ebay.apis.eBLBaseComponents.CreateRecurringPaymentsProfileRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.FailedPaymentActionType;
import urn.ebay.apis.eBLBaseComponents.ManageRecurringPaymentsProfileStatusRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.RecurringPaymentsProfileDetailsType;
import urn.ebay.apis.eBLBaseComponents.RefundSourceCodeType;
import urn.ebay.apis.eBLBaseComponents.RefundType;
import urn.ebay.apis.eBLBaseComponents.ScheduleDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.StatusChangeActionType;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.AssinaturaPagamento;
import br.com.contames.enumeration.PaypalLocale;

import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;

@Component
public class PaypalBuilder {
	
	private final Environment environment;
	private final Localization localization;
	
	private AssinaturaPagamento assinaturaPagamento;
	
	private String confirmaURL;
	
	private String cancelaURL;
	
	private String nome;
	
	public PaypalBuilder(Environment environment, Localization localization) {
		this.environment = environment;
		this.localization = localization;
		this.confirmaURL = environment.get("paypal.assinatura.confirmaURL");
		this.cancelaURL = environment.get("paypal.assinatura.cancelaURL");
	}
	
	public String getConfirmaURL() {
		return confirmaURL;
	}

	public PaypalBuilder setConfirmaURL(String confirmaURL) {
		this.confirmaURL = confirmaURL;
		return this;
	}

	public String getCancelaURL() {
		return cancelaURL;
	}

	public PaypalBuilder setCancelaURL(String cancelaURL) {
		this.cancelaURL = cancelaURL;
		return this;
	}
	
	public String getNome() {
		return nome;
	}

	public PaypalBuilder setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public PaypalBuilder with(AssinaturaPagamento assinaturaPagamento) {
		this.assinaturaPagamento = assinaturaPagamento;
		return this;
	}
	
	public PaypalBuilder setExpressCheckout() {  
				
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
		SetExpressCheckoutRequestType checkout = new SetExpressCheckoutRequestType();
		SetExpressCheckoutReq checkoutRequest = new SetExpressCheckoutReq();
		SetExpressCheckoutResponseType checkoutResponse = new SetExpressCheckoutResponseType();
		SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();
		
		/*
		 *  (Required) URL to which the buyer's browser is returned after choosing 
		 *  to pay with PayPal. For digital goods, you must add JavaScript to this 
		 *  page to close the in-context experience.
		  Note:
			PayPal recommends that the value be the final review page on which the buyer 
			confirms the order and payment or billing agreement.
			Character length and limitations: 2048 single-byte characters
		 */
		details.setReturnURL(confirmaURL);
		
		details.setCancelURL(cancelaURL);
		
		DecimalFormat decimalFormat = getDecimalFormat();
		
		BasicAmountType amount = new BasicAmountType();
		amount.setCurrencyID(CurrencyCodeType.BRL);
		amount.setValue(decimalFormat.format(assinaturaPagamento.getValor()));
		
		PaymentDetailsItemType item = new PaymentDetailsItemType();
		item.setQuantity(1);
		item.setName(nome);
		item.setAmount(amount);
		
		// populate line item details
		List<PaymentDetailsItemType> itemList = new ArrayList<PaymentDetailsItemType>();
		itemList.add(item);
		
		
		
		PaymentDetailsType detail = new PaymentDetailsType();
		detail.setPaymentAction(PaymentActionCodeType.SALE);
		
		/*
		 *  (Optional) Description of items the buyer is purchasing.
			 Note:
			 The value you specify is available only if the transaction includes a purchase.
			 This field is ignored if you set up a billing agreement for a recurring payment 
			 that is not immediately charged.
			 Character length and limitations: 127 single-byte alphanumeric characters
		 */
		detail.setOrderDescription(StringUtils.trim(localization.getMessage("paypal.billingAgreementText", nome, assinaturaPagamento.getValor())));
		
		// No Shipping
		details.setNoShipping("1");
		details.setAllowNote("0");
		
		// Total dos itens
		BasicAmountType itemAmount = new BasicAmountType();
		itemAmount.setValue(decimalFormat.format(assinaturaPagamento.getValor()));
		itemAmount.setCurrencyID(CurrencyCodeType.BRL);
		detail.setItemTotal(itemAmount);
		
		// Total geral
		BasicAmountType totalAmount = new BasicAmountType();
		totalAmount.setValue(decimalFormat.format(assinaturaPagamento.getValor()));
		totalAmount.setCurrencyID(CurrencyCodeType.BRL);
		detail.setOrderTotal(totalAmount);
						
		// Adiciona itens
		detail.setPaymentDetailsItem(itemList);
		detail.setPaymentAction(PaymentActionCodeType.SALE);
		
		
		List<PaymentDetailsType> detailsList = new ArrayList<PaymentDetailsType>();
		detailsList.add(detail);
		details.setPaymentDetails(detailsList);
		
		/*
		 *  (Required) Type of billing agreement. For recurring payments,
		 *   this field must be set to RecurringPayments. 
		 *   In this case, you can specify up to ten billing agreements. 
		 *   Other defined values are not valid.
			 Type of billing agreement for reference transactions. 
			 You must have permission from PayPal to use this field. 
			 This field must be set to one of the following values:
				1. MerchantInitiatedBilling - PayPal creates a billing agreement 
				   for each transaction associated with buyer.You must specify 
				   version 54.0 or higher to use this option.
				2. MerchantInitiatedBillingSingleAgreement - PayPal creates a 
				   single billing agreement for all transactions associated with buyer.
				   Use this value unless you need per-transaction billing agreements. 
				   You must specify version 58.0 or higher to use this option.

		 */
		BillingAgreementDetailsType billingAgreement = new BillingAgreementDetailsType(BillingCodeType.RECURRINGPAYMENTS);
				
		/*
		 * Description of goods or services associated with the billing agreement. 
		 * This field is required for each recurring payment billing agreement.
		 *  PayPal recommends that the description contain a brief summary of 
		 *  the billing agreement terms and conditions. For example,
		 *   buyer is billed at "9.99 per month for 2 years".
		   Character length and limitations: 127 single-byte alphanumeric characters
		 */
		billingAgreement.setBillingAgreementDescription(StringUtils.trim(localization.getMessage("paypal.billingAgreementText", nome, assinaturaPagamento.getValor())));
		List<BillingAgreementDetailsType> billingDetailsList = new ArrayList<BillingAgreementDetailsType>();
		billingDetailsList.add(billingAgreement);
		details.setBillingAgreementDetails(billingDetailsList);
		
		
		details.setLandingPage(assinaturaPagamento.getLandingPageType());
		details.setLocaleCode(PaypalLocale.pt_BR.name());
		checkout.setSetExpressCheckoutRequestDetails(details);
		checkoutRequest.setSetExpressCheckoutRequest(checkout);
		
		try {
			checkoutResponse = service.setExpressCheckout(checkoutRequest);
			if(checkoutResponse.getAck().equals(AckCodeType.SUCCESS)) {
				assinaturaPagamento.setToken(checkoutResponse.getToken());
			}
		} catch (SSLConfigurationException | InvalidCredentialException
				| HttpErrorException | InvalidResponseDataException
				| ClientActionRequiredException | MissingCredentialException
				| OAuthException | IOException | InterruptedException
				| ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
		
		return this;
	}
	
	
	public GetExpressCheckoutDetailsResponseType getExpressCheckout() {
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
		GetExpressCheckoutDetailsReq getRequest = new GetExpressCheckoutDetailsReq();
		GetExpressCheckoutDetailsRequestType getRequestType = new GetExpressCheckoutDetailsRequestType(assinaturaPagamento.getToken());
		getRequest.setGetExpressCheckoutDetailsRequest(getRequestType);
		try {
			GetExpressCheckoutDetailsResponseType getResponse = service.getExpressCheckoutDetails(getRequest);
			if(getResponse.getAck().equals(AckCodeType.SUCCESS)) {
				assinaturaPagamento.getAssinatura().getCliente().setPayerId(getResponse.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayerID());
			}
			return getResponse;
		} catch (SSLConfigurationException | InvalidCredentialException
				| HttpErrorException | InvalidResponseDataException
				| ClientActionRequiredException | MissingCredentialException
				| OAuthException | IOException | InterruptedException
				| ParserConfigurationException | SAXException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public PaypalBuilder doExpressCheckout() {
		
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
		GetExpressCheckoutDetailsResponseType getResponse = getExpressCheckout();
		
		if (getResponse != null) {
			
			if (AckCodeType.SUCCESS.equals(getResponse.getAck())) {
				
				DoExpressCheckoutPaymentRequestType doRequestType = new DoExpressCheckoutPaymentRequestType();
				DoExpressCheckoutPaymentRequestDetailsType details = new DoExpressCheckoutPaymentRequestDetailsType();
				
				/*
				 * A timestamped token by which you identify to PayPal that you are processing
				 * this payment with Express Checkout. The token expires after three hours. 
				 * If you set the token in the SetExpressCheckout request, the value of the token
				 * in the response is identical to the value in the request.
				   Character length and limitations: 20 single-byte characters
				 */
				details.setToken(assinaturaPagamento.getToken());
				
				
				createRecurringPaymentsProfile();
				
				/*
				 * Unique PayPal Customer Account identification number.
				   Character length and limitations: 13 single-byte alphanumeric characters
				 */
				String PayerID = getResponse.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayerID();
				details.setPayerID(PayerID);
								
				
				/*
				 *  (Optional) How you want to obtain payment. If the transaction does not include
				 *  a one-time purchase, this field is ignored. 
				 *  It is one of the following values:
						Sale â€“ This is a final sale for which you are requesting payment (default).
						Authorization â€“ This payment is a basic authorization subject to settlement with PayPal Authorization and Capture.
						Order â€“ This payment is an order authorization subject to settlement with PayPal Authorization and Capture.
					Note:
					You cannot set this field to Sale in SetExpressCheckout request and then change 
					this value to Authorization or Order in the DoExpressCheckoutPayment request. 
					If you set the field to Authorization or Order in SetExpressCheckout, 
					you may set the field to Sale.
					Character length and limitations: Up to 13 single-byte alphabetic characters
					This field is deprecated. Use PaymentAction in PaymentDetailsType instead.
				 */
				details.setPaymentAction(PaymentActionCodeType.SALE);
				
				
				DecimalFormat decimalFormat = getDecimalFormat();
				
				PaymentDetailsType paymentDetails = new PaymentDetailsType();
				List<PaymentDetailsItemType> paymentItems = new ArrayList<PaymentDetailsItemType>();
				
				
				PaymentDetailsItemType paymentDetailsItem = new PaymentDetailsItemType();
				
				/*
				 * Item name. This field is required when you pass a value for ItemCategory.
				   Character length and limitations: 127 single-byte characters
				   This field is introduced in version 53.0. 
				 */
				paymentDetailsItem.setName(assinaturaPagamento.getAssinatura().getPlano().getNome());
				
				/*
				 * Item quantity. This field is required when you pass a value for ItemCategory. 
				 * For digital goods (ItemCategory=Digital), this field is required.
					Character length and limitations: Any positive integer
					This field is introduced in version 53.0. 
				 */
				paymentDetailsItem.setQuantity(1);
				BasicAmountType amountItem = new BasicAmountType();
				
				/*
				 * Cost of item. This field is required when you pass a value for ItemCategory.
				Note:
				You must set the currencyID attribute to one of the 3-character currency codes for
				any of the supported PayPal currencies.
				Character length and limitations: Value is a positive number which cannot 
				exceed $10,000 USD in any currency. It includes no currency symbol.
				It must have 2 decimal places, the decimal separator must be a period (.), 
				and the optional thousands separator must be a comma (,).
				This field is introduced in version 53.0.
				 */
				amountItem.setValue(decimalFormat.format(assinaturaPagamento.getValor()));
				//PayPal uses 3-character ISO-4217 codes for specifying currencies in fields and variables.
				amountItem.setCurrencyID(CurrencyCodeType.BRL);
				paymentDetailsItem.setAmount(amountItem);
				
				paymentItems.add(paymentDetailsItem);
				
				
				BasicAmountType totalAmount = new BasicAmountType();
				totalAmount.setValue(decimalFormat.format(assinaturaPagamento.getValor()));
				
				//PayPal uses 3-character ISO-4217 codes for specifying currencies in fields and variables.
				totalAmount.setCurrencyID(CurrencyCodeType.BRL);
				
				/*
				 *  Sum of cost of all items in this order. For digital goods, this field is 
				 *  required. PayPal recommends that you pass the same value in the call to 
				 *  DoExpressCheckoutPayment that you passed in the call to SetExpressCheckout.
				 Note:
					You must set the currencyID attribute to one of the 3-character currency 
					codes for any of the supported PayPal currencies.
					Character length and limitations: Value is a positive number which cannot 
					exceed $10,000 USD in any currency. It includes no currency symbol. 
					It must have 2 decimal places, the decimal separator must be a period (.), 
					and the optional thousands separator must be a comma (,).
				 */
				paymentDetails.setOrderTotal(totalAmount);
				
				BasicAmountType itemTotalAmount = new BasicAmountType();
				itemTotalAmount.setValue(decimalFormat.format(assinaturaPagamento.getValor()));
				
				//PayPal uses 3-character ISO-4217 codes for specifying currencies in fields and variables.
				itemTotalAmount.setCurrencyID(CurrencyCodeType.BRL);
				
				
				/*
				 *  Sum of cost of all items in this order. For digital goods, this field is 
				 *  required. PayPal recommends that you pass the same value in the call to 
				 *  DoExpressCheckoutPayment that you passed in the call to SetExpressCheckout.
				 Note:
					You must set the currencyID attribute to one of the 3-character currency 
					codes for any of the supported PayPal currencies.
					Character length and limitations: Value is a positive number which cannot 
					exceed $10,000 USD in any currency. It includes no currency symbol. 
					It must have 2 decimal places, the decimal separator must be a period (.), 
					and the optional thousands separator must be a comma (,).
				 */
				paymentDetails.setItemTotal(itemTotalAmount);
				
				
				/*
				 *  (Optional) Your URL for receiving Instant Payment Notification (IPN) 
				 *  about this transaction. If you do not specify this value in the request, 
				 *  the notification URL from your Merchant Profile is used, if one exists.
					Important:
					The notify URL applies only to DoExpressCheckoutPayment. 
					This value is ignored when set in SetExpressCheckout or GetExpressCheckoutDetails.
					Character length and limitations: 2,048 single-byte alphanumeric characters
				 */
				paymentDetails.setNotifyURL(environment.get("paypal.assinatura.ipnURL"));
				
				
				List<PaymentDetailsType> payDetailType = new ArrayList<PaymentDetailsType>();
				payDetailType.add(paymentDetails);
				
				/*
				 * When implementing parallel payments, you can create up to 10 sets of payment 
				 * details type parameter fields, each representing one payment you are hosting 
				 * on your marketplace.
				 */
				details.setPaymentDetails(payDetailType);
				
				
				
				doRequestType.setDoExpressCheckoutPaymentRequestDetails(details);
				DoExpressCheckoutPaymentReq doRequest = new DoExpressCheckoutPaymentReq();
				doRequest.setDoExpressCheckoutPaymentRequest(doRequestType);
				DoExpressCheckoutPaymentResponseType doResponse = null;
				try {
					doResponse = service.doExpressCheckoutPayment(doRequest);
				} catch (SSLConfigurationException | InvalidCredentialException
						| HttpErrorException | InvalidResponseDataException
						| ClientActionRequiredException
						| MissingCredentialException | OAuthException
						| IOException | InterruptedException
						| ParserConfigurationException | SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
		return this;
	}
	
	

	public CreateRecurringPaymentsProfileResponseType createRecurringPaymentsProfile() {
		
		// Creating service wrapper object to make an API call by loading configuration map.
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
						
		CreateRecurringPaymentsProfileReq req = new CreateRecurringPaymentsProfileReq();
		CreateRecurringPaymentsProfileRequestType reqType = new CreateRecurringPaymentsProfileRequestType();
		
		
		/**  (Required) The date when billing for this profile begins.
			Note:
			The profile may take up to 24 hours for activation.
			Character length and limitations: Must be a valid date, in UTC/GMT format
		*/
		RecurringPaymentsProfileDetailsType profileDetails = new RecurringPaymentsProfileDetailsType(LocalDate.now().toString("yyyy-MM-dd") + "T00:00:00:000Z");
		
				
		// Populate schedule details
		ScheduleDetailsType scheduleDetails = new ScheduleDetailsType();
					
		/**  (Required) Description of the recurring payment.
			Note:
			You must ensure that this field matches the corresponding billing agreement 
			description included in the SetExpressCheckout request.
			Character length and limitations: 127 single-byte alphanumeric characters
		*/
		scheduleDetails.setDescription(StringUtils.trim(localization.getMessage("paypal.billingAgreementText", nome, assinaturaPagamento.getValor())));
				
				
		 /**  (Required) Number of billing periods that make up one billing cycle.
			The combination of billing frequency and billing period must be less than 
			or equal to one year. For example, if the billing cycle is Month, 
			the maximum value for billing frequency is 12. Similarly, 
			if the billing cycle is Week, the maximum value for billing frequency is 52.
			Note:
			If the billing period is SemiMonth, the billing frequency must be 1.
		*/
				
				
		 /**  (Required) Billing amount for each billing cycle during this payment period. 
		 *  This amount does not include shipping and tax amounts.
			Note:
			All amounts in the CreateRecurringPaymentsProfile request must have the same 
			currency.
			Character length and limitations: Value is a positive number which cannot 
			exceed $10,000 USD in any currency. It includes no currency symbol. 
			It must have 2 decimal places, the decimal separator must be a period (.), 
			and the optional thousands separator must be a comma (,). 
		 */
		
		DecimalFormat decimalFormat = getDecimalFormat();
		
		BasicAmountType paymentAmount = new BasicAmountType(CurrencyCodeType.BRL, decimalFormat.format(assinaturaPagamento.getValor()));
				
				
		/**  (Required) Unit for billing during this subscription period. 
		 *  It is one of the following values:
			 [Day, Week, SemiMonth, Month, Year]
			For SemiMonth, billing is done on the 1st and 15th of each month.
			Note:
			The combination of BillingPeriod and BillingFrequency cannot exceed one year.
		 
		*/
		BillingPeriodDetailsType paymentPeriod = new BillingPeriodDetailsType(BillingPeriodType.MONTH, 1, paymentAmount);
					
		 
		scheduleDetails.setPaymentPeriod(paymentPeriod);
		
		ActivationDetailsType activationDetails = new ActivationDetailsType();
		activationDetails.setFailedInitialAmountAction(FailedPaymentActionType.CANCELONFAILURE);
		scheduleDetails.setActivationDetails(activationDetails);
		
		// Quantidade de tentativas de cobranca
		scheduleDetails.setMaxFailedPayments(3);
		
		
		// Cobrar montante no proximo ciclo caso falhe a cobranca
		scheduleDetails.setAutoBillOutstandingAmount(AutoBillType.NOAUTOBILL);
		
		CreateRecurringPaymentsProfileRequestDetailsType reqDetails = new CreateRecurringPaymentsProfileRequestDetailsType(profileDetails, scheduleDetails);
		
		
		 /** A timestamped token, the value of which was returned in the response to the 
		 * first call to SetExpressCheckout. You can also use the token returned in the
		 * SetCustomerBillingAgreement response.Each CreateRecurringPaymentsProfile request creates 
		 * a single recurring payments profile.
			Note:
			Tokens expire after approximately 3 hours.
		 */
		reqDetails.setToken(assinaturaPagamento.getToken());
						
		reqType.setCreateRecurringPaymentsProfileRequestDetails(reqDetails);
		req.setCreateRecurringPaymentsProfileRequest(reqType);
		CreateRecurringPaymentsProfileResponseType resp = null;
		try{
			resp = service.createRecurringPaymentsProfile(req);
			//if(resp.getAck().equals(AckCodeType.SUCCESS)) {
			//	assinaturaPagamento.getAssinatura().setProfileId(resp.getCreateRecurringPaymentsProfileResponseDetails().getProfileID());
			//}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return resp;
	}
	
	
	
	public GetRecurringPaymentsProfileDetailsResponseType getRecurringPaymentsProfile(String profileId) {
		
		GetRecurringPaymentsProfileDetailsRequestType request = new GetRecurringPaymentsProfileDetailsRequestType();
		request.setProfileID(profileId);
		
		GetRecurringPaymentsProfileDetailsReq wrapper = new GetRecurringPaymentsProfileDetailsReq();
		wrapper.setGetRecurringPaymentsProfileDetailsRequest(request);
		
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
		try {
			GetRecurringPaymentsProfileDetailsResponseType recurringPaymentsProfileDetailsResponse = service.getRecurringPaymentsProfileDetails(wrapper);
			return recurringPaymentsProfileDetailsResponse;
		} catch (SSLConfigurationException | InvalidCredentialException
				| HttpErrorException | InvalidResponseDataException
				| ClientActionRequiredException | MissingCredentialException
				| OAuthException | IOException | InterruptedException
				| ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
		
	public RefundTransactionResponseType refundProfilePayment(RefundType refundType) {
		return refundProfilePayment(refundType, null, null);
	}
	
	public RefundTransactionResponseType refundProfilePayment(RefundType refundType, String comment) {
		return refundProfilePayment(refundType, null, comment);
	}
	public RefundTransactionResponseType refundProfilePayment(RefundType refundType, Double amount) {
		return refundProfilePayment(refundType, amount, null);
	}
	public RefundTransactionResponseType refundProfilePayment(RefundType refundType, Double amount, String comment) {
		return refundProfilePayment(refundType, amount, assinaturaPagamento.getAssinatura().getCliente().getPayerId(), assinaturaPagamento.getTxnId(), comment);
	}
	
	public RefundTransactionResponseType refundProfilePayment(RefundType refundType, Double amount, String payer_id, String txn_id, String comment) {
		
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
		RefundTransactionReq refundTransactionReq = new RefundTransactionReq();
		RefundTransactionRequestType refundTransactionRequestType = new RefundTransactionRequestType();
		refundTransactionRequestType.setRefundType(refundType);
		refundTransactionRequestType.setTransactionID(txn_id);
		
		if(StringUtils.isNotBlank(comment)) {
			refundTransactionRequestType.setMemo(comment);
		}
				
		if(amount != null && refundType != null && !refundType.equals(RefundType.FULL)) {
			refundTransactionRequestType.setAmount(new BasicAmountType(CurrencyCodeType.BRL, getDecimalFormat().format(amount)));
		}
		
		refundTransactionRequestType.setRefundSource(RefundSourceCodeType.INSTANT);
		refundTransactionRequestType.setPayerID(payer_id);
		refundTransactionReq.setRefundTransactionRequest(refundTransactionRequestType);
		try {
			RefundTransactionResponseType refundTransactionResposeType = service.refundTransaction(refundTransactionReq);
			return refundTransactionResposeType;
		} catch (SSLConfigurationException | InvalidCredentialException
				| HttpErrorException | InvalidResponseDataException
				| ClientActionRequiredException | MissingCredentialException
				| OAuthException | IOException | InterruptedException
				| ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public ManageRecurringPaymentsProfileStatusResponseType changeStatusProfile(String profileId, StatusChangeActionType status) throws SSLConfigurationException, InvalidCredentialException, HttpErrorException, InvalidResponseDataException, ClientActionRequiredException, MissingCredentialException, OAuthException, IOException, InterruptedException, ParserConfigurationException, SAXException {
		
		PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getConfiguration());
		ManageRecurringPaymentsProfileStatusReq req = new ManageRecurringPaymentsProfileStatusReq();
		ManageRecurringPaymentsProfileStatusRequestType reqType = new ManageRecurringPaymentsProfileStatusRequestType();
		/*
		 *  (Required) Recurring payments profile ID returned in the 
		 *  CreateRecurringPaymentsProfile response.
			Character length and limitations: 14 single-byte alphanumeric characters. 
			19 character profile IDs are supported for compatibility with previous 
			versions of the PayPal API.
			------
			(Required) The action to be performed to the recurring payments profile. Must be one of the following:
			Cancel – Only profiles in Active or Suspended state can be canceled.
			Suspend – Only profiles in Active state can be suspended.
			Reactivate – Only profiles in a suspended state can be reactivated.

		 */
		ManageRecurringPaymentsProfileStatusRequestDetailsType reqDetails = new ManageRecurringPaymentsProfileStatusRequestDetailsType(profileId, status);
		/*
		 * (Optional) The reason for the change in status. 
		 * For profiles created using Express Checkout, this message is included 
		 * in the email notification to the buyer when the status of the profile is 
		 * successfully changed, and can also be seen by both you and the buyer on 
		 * the Status History page of the PayPal account. 
		 */
		reqDetails.setNote("change");
		reqType.setManageRecurringPaymentsProfileStatusRequestDetails(reqDetails);
		req.setManageRecurringPaymentsProfileStatusRequest(reqType);
		ManageRecurringPaymentsProfileStatusResponseType resp = service.manageRecurringPaymentsProfileStatus(req);
		return resp;
	}
	

	
	public Map<String, String> getConfiguration() {
		Map<String, String> configuration = new HashMap<String, String>();
		configuration.put("acct1.UserName", environment.get("acct1.UserName"));
		configuration.put("acct1.Password", environment.get("acct1.Password"));
		configuration.put("acct1.Signature", environment.get("acct1.Signature"));
		configuration.put("mode", environment.get("paypal.mode"));
		return configuration;
	}
	
	
	/**
	 * Recupera um formatador de decimais no padrao que o paypal exige
	 * @return DecimalFormat
	 */
	private static DecimalFormat getDecimalFormat() {
		DecimalFormat decimalFormat = new DecimalFormat("####.00");
		decimalFormat.setMaximumFractionDigits(2);
		decimalFormat.setMinimumFractionDigits(2);
		decimalFormat.setMinimumIntegerDigits(1);
		
		DecimalFormatSymbols custom = new DecimalFormatSymbols();
		custom.setDecimalSeparator('.');
		decimalFormat.setDecimalFormatSymbols(custom);
		return decimalFormat;
	}
	
}
