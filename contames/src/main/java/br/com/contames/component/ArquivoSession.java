package br.com.contames.component;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Arquivo;
import br.com.contames.session.UsuarioLogged;

@Component
public class ArquivoSession {

	private final HttpSession session;
	private final UsuarioLogged usuarioLogged;
	
	public ArquivoSession(HttpSession session, UsuarioLogged usuarioLogged) {
		this.session = session;
		this.usuarioLogged = usuarioLogged;
	}
	
	public List<Arquivo> getArquivoList(String viewid) {
		List<Arquivo> arquivoList = (List<Arquivo>) session.getAttribute("arquivos_" + usuarioLogged.getCliente().getId() + "_" + viewid);
		if(arquivoList == null) {
			session.setAttribute("arquivos_" + usuarioLogged.getCliente().getId() + "_" + viewid, new ArrayList<Arquivo>());
			arquivoList = (List<Arquivo>) session.getAttribute("arquivos_" + usuarioLogged.getCliente().getId() + "_" + viewid);
		}
		return arquivoList;
	}
	
	public void setAquivoList(String viewid, List<Arquivo> arquivoList) {
		session.setAttribute("arquivos_" + usuarioLogged.getCliente().getId() + "_" + viewid, arquivoList);
	}
	
	public void remove(String viewid, Arquivo arquivoRemover) {
		List<Arquivo> arquivoList = getArquivoList(viewid);
		List<Arquivo> newArquivoList = new ArrayList<Arquivo>();
		for(Arquivo arquivo : arquivoList) {
			if(arquivo.getTemp() != null) {
				if(!arquivo.getTemp().equals(arquivoRemover.getTemp())) {
					newArquivoList.add(arquivo);
				} else continue;
			} else {
				if(!arquivo.getId().equals(arquivoRemover.getId())) {
					newArquivoList.add(arquivo);
				} else continue;
			}
		}
		setAquivoList(viewid, newArquivoList);
	}
	
	
	public List<Arquivo> getArquivoDeleteList(String viewid) {
		List<Arquivo> arquivoDeleteList = (List<Arquivo>) session.getAttribute("arquivosDelete_" + usuarioLogged.getCliente().getId() + "_" + viewid);
		if(arquivoDeleteList == null) {
			session.setAttribute("arquivosDelete_" + usuarioLogged.getCliente().getId() + "_" + viewid, new ArrayList<Arquivo>());
			arquivoDeleteList = (List<Arquivo>) session.getAttribute("arquivosDelete_" + usuarioLogged.getCliente().getId() + "_" + viewid);
		}
		return arquivoDeleteList;
	}
	
	public ArquivoSession clear(String viewid) {
		session.removeAttribute("arquivos_" + usuarioLogged.getCliente().getId() + "_" + viewid);
		session.removeAttribute("arquivosDelete_" + usuarioLogged.getCliente().getId() + "_" + viewid);
		return this;
	}
	
}
