package br.com.contames.component;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

@Component
@ApplicationScoped
public class Settings {

	private final Configuration configuration;
	
	public Settings() throws ConfigurationException {
		this.configuration = new PropertiesConfiguration("settings.properties");
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}
}
