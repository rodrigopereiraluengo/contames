package br.com.contames.component;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.ioc.Component;

import com.thoughtworks.xstream.converters.SingleValueConverter;

@Component
public class ConvertLocalDateTimeJSON implements SingleValueConverter {

	private final Localization localization;
	
	public ConvertLocalDateTimeJSON(Localization localization) {
		this.localization = localization;
	}
	
	@Override    
    public String toString(Object value) {    
        LocalDateTime d = new LocalDateTime(value);    
        String data = d.toString(localization.getMessage("patternDateTime"));  
        return data;    
        
    }      
         
    @SuppressWarnings("rawtypes")    
    public boolean canConvert(Class clazz) {  
        return LocalDateTime.class.isAssignableFrom(clazz);    
    }    
    
    @Override    
    public Object fromString(String arg0) {    
        // TODO Auto-generated method stub    
        return null;    
    }      
	
}
