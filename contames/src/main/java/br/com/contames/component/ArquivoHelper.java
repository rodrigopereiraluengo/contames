package br.com.contames.component;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.interceptor.download.InputStreamDownload;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Arquivo;
import br.com.contames.session.UsuarioLogged;
import br.com.contames.util.StringHelper;

@Component
public class ArquivoHelper {

	private final CloudFiles cloudFiles;
	private final HttpSession httpSession;
	private final UsuarioLogged usuarioLogged;
	private final String container;
	private final String tempContainer;	
	
	public ArquivoHelper(CloudFiles cloudFiles, HttpSession httpSession, UsuarioLogged usuarioLogged) {
		this.cloudFiles = cloudFiles;
		this.httpSession = httpSession;
		this.usuarioLogged = usuarioLogged;
		this.container = "cliente_" + this.usuarioLogged.getCliente().getId();
		this.tempContainer = container + "_temp";
	}
	
	public void saveTemp(UploadedFile uploadedFile, Arquivo arquivo) {
		if(uploadedFile != null && arquivo != null) {
			arquivo.setNome(StringHelper.unAccent(uploadedFile.getFileName()).replaceAll("[^a-zA-Z0-9-_\\.]", "_"));
			arquivo.setContentType(uploadedFile.getContentType());
			arquivo.setSize(uploadedFile.getSize());
			cloudFiles.upload(tempContainer, httpSession.getId() + "/" + arquivo.getTemp() + "/" + arquivo.getNome(), uploadedFile.getFile());
		}
	}
	
	
	public void deleteTemp(Arquivo arquivo) {
		if(arquivo != null) {
			cloudFiles.removePath(tempContainer, httpSession.getId() + "/" + arquivo.getTemp());
			arquivo.setNome(null);
			arquivo.setContentType(null);
			arquivo.setSize(null);
		}
	}
	
	
	public void moveToContainer(Arquivo arquivo) {
		if(existsTemp(arquivo)) {
			cloudFiles.moveToContainer(tempContainer, httpSession.getId() + "/" + arquivo.getTemp() + "/" + arquivo.getNome(), container, arquivo.getId().toString());
		}
	}
	
	
	public boolean existsTemp(Arquivo arquivo) {
		return cloudFiles.fileExists(tempContainer, httpSession.getId() + "/" + arquivo.getTemp() + "/" + arquivo.getNome());
	}
	
	public boolean exists(Arquivo arquivo) {
		return cloudFiles.fileExists(container, arquivo.getId().toString());
	}
	
	
	public void delete(Arquivo arquivo) {
		
		if(existsTemp(arquivo)) {
			cloudFiles.removeFile(tempContainer, httpSession.getId() + "/" + arquivo.getTemp() + "/" + arquivo.getNome());
		}
		
		if(exists(arquivo)) {
			cloudFiles.removeFile(container, arquivo.getId().toString());
		}
	}
	
	
	public InputStreamDownload downloadTemp(Arquivo arquivo) {
		if(existsTemp(arquivo)) {
			return new InputStreamDownload(cloudFiles.download(tempContainer, httpSession.getId() + "/" + arquivo.getTemp() + "/" + arquivo.getNome()), arquivo.getContentType(), arquivo.getNome());
		} else if(arquivo.getId() != null){
			return download(arquivo);
		} else {
			return null;
		}
	}
	
	
	public InputStreamDownload download(Arquivo arquivo) {
		if(exists(arquivo)) {
			return new InputStreamDownload(cloudFiles.download(container, arquivo.getId().toString()), arquivo.getContentType(), arquivo.getNome());
		} else {
			return null;
		}
	}
	

}
