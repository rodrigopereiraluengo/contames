package br.com.contames.component.factory;

import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;

@Component
public class ConnectionFactory implements ComponentFactory<Connection> {

	private final EntityManager entityManager;
	
	public ConnectionFactory(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public Connection getInstance() {
		Connection connection = entityManager.unwrap(Connection.class);
		try {
			for(Object k : connection.getClientInfo().entrySet()) {
				System.out.println(k + " - " + connection.getClientInfo().get(k));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

}
