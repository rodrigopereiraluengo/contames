package br.com.contames.component;

import java.io.IOException;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.freemarker.Freemarker;
import br.com.caelum.vraptor.freemarker.Template;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.PrototypeScoped;
import br.com.caelum.vraptor.simplemail.AsyncMailer;
import freemarker.template.TemplateException;

@Component
@PrototypeScoped
public class EmailHelper {

	private final AsyncMailer mailer;
	private final Freemarker freemarker;
	private final Environment environment;
	private final Localization localization;
	private final Settings settings;
	
	public EmailHelper(AsyncMailer mailer, Freemarker freemarker, Environment environment, Localization localization, Settings settings) throws EmailException, IOException {
		this.mailer = mailer;
		this.freemarker = freemarker;
		this.environment = environment;
		this.localization = localization;
		this.settings = settings;
		build();
	}
	
	private HtmlEmail htmlemail;
	private Template template;
	private StringBuilder textMessage;
	
	public EmailHelper build() {
		
		// Envia email cadastro realizado com sucesso!
		// Mensagem HTML
		htmlemail = new HtmlEmail();
		htmlemail.setSubject(localization.getMessage("email.support.subject"));
		
		try {
			template = freemarker
				.use("email")
				.with("signature", localization.getMessage("email.support.htmlSignature"))
				.with("siteUrl", environment.get("siteUrl"))
				.with("siteTitle", localization.getMessage("site.titulo"))
				.with("noreply", localization.getMessage("email.noreply.htmlSignature"));
						
			textMessage = new StringBuilder();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public EmailHelper setFrom(String email, String name) {
		try {
			htmlemail.setFrom(email, name, "utf-8");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public EmailHelper addTo(String email) {
		try {
			htmlemail.addTo(email, email, "utf-8");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public EmailHelper addTo(String email, String name) {
		try {
			htmlemail.addTo(email, name, "utf-8");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public EmailHelper addBcc(String email, String name) {
		try {
			htmlemail.addBcc(email, name, "utf-8");
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public EmailHelper setSubject(String subject) {
		htmlemail.setSubject(subject);
		return this;
	}
	
	public EmailHelper setHtmlMsg(String htmlMsg) {
		template = template.with("msg", htmlMsg);
		return this;
	}
	
	public EmailHelper setTextMsg(String textMsg) {
		textMessage.append(textMsg);
		return this;
	}
	
	public void send() {
		try {
			htmlemail.setHtmlMsg(template.getContent());
			textMessage.append(localization.getMessage("email.support.textSignature") + localization.getMessage("email.noreply.textSignature"));
			htmlemail.setTextMsg(textMessage.toString());
			mailer.asyncSend(htmlemail);
		} catch (EmailException | IOException | TemplateException e) {
			e.printStackTrace();
		}
	}
}
