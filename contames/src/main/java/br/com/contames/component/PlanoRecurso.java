package br.com.contames.component;

import br.com.caelum.vraptor.ioc.Component;
import br.com.contames.entity.Plano;
import br.com.contames.repository.AssinaturaRepository;
import br.com.contames.session.UsuarioLogged;

@Component
public class PlanoRecurso {

	private int contas;
	
	private int contasUtil;
	
	private int contasDisp;
	
	private int cartoesCredito;
	
	private int cartoesCreditoUtil;
	
	private int cartoesCreditoDisp;
	
	private int lancamentos;
	
	private int lancamentosUtil;
	
	private int lancamentosDisp;
	
	private double bancoDados;
	
	private double bancoDadosUtil;
	
	private double bancoDadosDisp;
	
	private double armazenamento;
	
	private double armazenamentoUtil;
	
	private double armazenamentoDisp;
	
	private int usuarios;
	
	private int usuariosUtil;
	
	private int usuariosDisp;
	
	private final AssinaturaRepository assinaturaRepository;
	
	private final CloudFiles cloudFiles;
	
	private final UsuarioLogged usuarioLogged;
	
	private final Plano plano;
	
	public PlanoRecurso(AssinaturaRepository assinaturaRepository, CloudFiles cloudFiles, UsuarioLogged usuarioLogged) {
		this.assinaturaRepository = assinaturaRepository;
		this.cloudFiles = cloudFiles;
		this.usuarioLogged = usuarioLogged;
		this.plano = usuarioLogged.getCliente().getAssinatura().getPlano();
		
		// Contas
		this.contas = usuarioLogged.getCliente().getAssinatura().getPlano().getContas();
		this.contasUtil = assinaturaRepository.qtdeConta().intValue();
		this.contasDisp = this.contas - this.contasUtil;
		
		// Cartoes de Credito
		this.cartoesCredito = usuarioLogged.getCliente().getAssinatura().getPlano().getCartoesCredito();
		this.cartoesCreditoUtil = assinaturaRepository.qtdeCartaoCredito().intValue();
		this.cartoesCreditoDisp = this.cartoesCredito - this.cartoesCreditoUtil;
		
		// Lancamentos
		this.lancamentos = usuarioLogged.getCliente().getAssinatura().getPlano().getLancamentos();
		this.lancamentosUtil = assinaturaRepository.qtdeLancamento();
		this.lancamentosDisp = this.lancamentos - this.lancamentosUtil;
		
		// Banco de Dados
		this.bancoDados = usuarioLogged.getCliente().getAssinatura().getPlano().getBancoDados();
		this.bancoDadosUtil = assinaturaRepository.tablesSize().doubleValue();
		this.bancoDadosDisp = this.bancoDados - this.bancoDadosUtil;
		
		// Armazenamento de Arquivos
		this.armazenamento = usuarioLogged.getCliente().getAssinatura().getPlano().getDisco();
		Long sizeContainer = cloudFiles.getContainerSize("cliente_" + usuarioLogged.getCliente().getId());
		this.armazenamentoUtil = sizeContainer.equals(0l) ? 0 : sizeContainer.intValue() / 1024 / 1024;
		this.armazenamentoDisp = this.armazenamento - this.armazenamentoUtil;
		
		// Usuarios
		this.usuarios = usuarioLogged.getCliente().getAssinatura().getPlano().getUsuarios();
		this.usuariosUtil = assinaturaRepository.qtdeUsuarios().intValue();
		this.usuariosDisp = this.usuarios - this.usuariosUtil;
	}

	public int getContas() {
		return contas;
	}

	public int getContasUtil() {
		return contasUtil;
	}

	public int getContasDisp() {
		return contasDisp;
	}

	public int getCartoesCredito() {
		return cartoesCredito;
	}

	public int getCartoesCreditoUtil() {
		return cartoesCreditoUtil;
	}

	public int getCartoesCreditoDisp() {
		return cartoesCreditoDisp;
	}

	public int getLancamentos() {
		return lancamentos;
	}

	public int getLancamentosUtil() {
		return lancamentosUtil;
	}

	public int getLancamentosDisp() {
		return lancamentosDisp;
	}

	public double getBancoDados() {
		return bancoDados;
	}

	public double getBancoDadosUtil() {
		return bancoDadosUtil;
	}

	public double getBancoDadosDisp() {
		return bancoDadosDisp < 0.0 ? 0.0 : bancoDadosDisp;
	}

	public double getArmazenamento() {
		return armazenamento;
	}

	public double getArmazenamentoUtil() {
		return armazenamentoUtil;
	}

	public double getArmazenamentoDisp() {
		return armazenamentoDisp < 0.0 ? 0.0 : armazenamentoDisp;
	}

	public int getUsuarios(Long id) {
		return assinaturaRepository.qtdeUsuarios(id).intValue();
	}
	
	public int getUsuarios() {
		return usuarios;
	}

	public int getUsuariosUtil() {
		return usuariosUtil;
	}

	public int getUsuariosDisp() {
		return usuariosDisp;
	}

	public Plano getPlano() {
		return plano;
	}
	
}
