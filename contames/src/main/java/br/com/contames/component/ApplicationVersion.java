package br.com.contames.component;

import org.joda.time.LocalDateTime;

import br.com.caelum.vraptor.ioc.ApplicationScoped;

@ApplicationScoped
public class ApplicationVersion {

	private String version;
	
	public String getVersion() {
		return version;
	}
	
	public ApplicationVersion() {
		version = new LocalDateTime().toString("yyyyMMddHHmm");
	}
	
}
