package br.com.contames.component;

import org.joda.time.LocalDate;

import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.ioc.Component;

import com.thoughtworks.xstream.converters.SingleValueConverter;

@Component
public class ConvertLocalDataJSON implements SingleValueConverter {
	
	private final Localization localization;
	
	public ConvertLocalDataJSON(Localization localization) {
		this.localization = localization;
	}
	
	@Override    
    public String toString(Object value) {    
        LocalDate d = new LocalDate(value);    
        String data = d.toString(localization.getMessage("patternDate"));  
        return data;    
        
    }      
         
    @SuppressWarnings("rawtypes")    
    public boolean canConvert(Class clazz) {  
        return LocalDate.class.isAssignableFrom(clazz);    
    }    
    
    @Override    
    public Object fromString(String arg0) {    
        // TODO Auto-generated method stub    
        return null;    
    }      

}
