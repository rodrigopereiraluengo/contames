package br.com.contames.component;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.BooleanUtils;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.cloudfiles.CloudFilesClient;
import org.jclouds.rest.RestContext;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

@Component
@SessionScoped
public class CloudFiles {
	
	private final static Logger LOGGER = Logger.getLogger(CloudFiles.class.getCanonicalName());

	public static final String USERNAME = "ropelue";
	
	public static final String APIKEY = "f5cfc991ade498b3441dd2814316342b";
	
	public static final String PROVIDER = "cloudfiles-us";
	
	private final HttpSession httpSession;
	
	private final BlobStoreContext blobStoreContext;
	
	private final BlobStore blobStore;
	
	private final CloudFilesClient cloudFilesClient;
	
	private final ExecutorService executor;
	
	@SuppressWarnings("deprecation")
	public CloudFiles(HttpSession httpSession, ExecutorService executor) {
		this.httpSession = httpSession;
		blobStoreContext = ContextBuilder.newBuilder(CloudFiles.PROVIDER)
	            .credentials(CloudFiles.USERNAME, CloudFiles.APIKEY)
	            .buildView(BlobStoreContext.class);
		blobStore = blobStoreContext.getBlobStore();
		RestContext<org.jclouds.cloudfiles.CloudFilesClient, org.jclouds.cloudfiles.CloudFilesAsyncClient> restContext = blobStoreContext.unwrap();
		cloudFilesClient = restContext.getApi();
		this.executor = executor;
	}
	
	public void createContainer(String name) {
		LOGGER.log(Level.INFO, "Create container: {0}", name);
		blobStore.createContainerInLocation(null, name);
	}
	
	public void deleteContainer(String name) {
		LOGGER.log(Level.INFO, "Delete container: {0}", name);
		try {
			blobStore.deleteContainer(name);
		} catch(Exception e) {
			
		}
		
	}
	
	public void upload(String containerName, String name, InputStream inputStream) {
		// Verifica se o container existe
		if(!cloudFilesClient.containerExists(containerName)) {
			cloudFilesClient.createContainer(containerName);
		}
		LOGGER.log(Level.INFO, "Upload file container:{0}, name:{1}", new Object[]{containerName, name});
		Blob blob = blobStore.blobBuilder(name).payload(inputStream).build();
		blobStore.putBlob(containerName, blob);
	}
	
	public boolean fileExists(String container, String name) {
		return cloudFilesClient.objectExists(container, name);
	}
	
	public Future<Void> removeFile(final String container, final String name) {
		Callable<Void> task = new Callable<Void>() {
			@Override
			public Void call() {
				LOGGER.log(Level.INFO, "Remove file {0}/{1}", new Object[]{container, name});
				blobStore.removeBlob(container, name);
				return null;
			}
		};
		return this.executor.submit(task);
	}
	
	public Future<Void> moveToContainer(final String containerSource, final String fileNameSource, final String containerDest, final String fileNameDest, final Boolean isOnlyFilePerPath) {
		Callable<Void> task = new Callable<Void>() {
			@Override
			public Void call() {
		
				LOGGER.log(Level.INFO, "Move to container, source:{0}/{1}, destiny:{2}/{3}", new Object[]{containerSource, fileNameSource, containerDest, fileNameDest});
		
				// Verifica se o container existe
				if(!cloudFilesClient.containerExists(containerDest)) {
					cloudFilesClient.createContainer(containerDest);
				}
				
				// Verifica se o arquivo a ser copiado existe
				if(blobStore.blobExists(containerSource, fileNameSource)){
					
					//Limpa pasta
					if(BooleanUtils.isTrue(isOnlyFilePerPath)) {
						if(blobStore.directoryExists(containerDest, fileNameDest)) {
							blobStore.deleteDirectory(containerDest, fileNameDest);
						}
					}
					
					// copia arquivo
					cloudFilesClient.copyObject(containerSource, fileNameSource, containerDest, fileNameDest);
					
					// remove da origem
					blobStore.removeBlob(containerSource, fileNameSource);
				}
				
				return null;
				
			}
		};
		return this.executor.submit(task);
	}
	
	public void moveToContainer(String containerSource, String fileNameSource, String containerDest, String fileNameDest) {
		moveToContainer(containerSource, fileNameSource, containerDest, fileNameDest, false);
	}
	
	public void removeFile(String container, String... files) {
		LOGGER.log(Level.INFO, "Remove path container: {0}, files: {1}", new Object[]{container, files});
		for(String file : files) {
			if(blobStore.blobExists(container, file)) {
				blobStore.removeBlob(container, file);
			}
		}
	}
	
	public void removePath(String container, String... paths) {
		LOGGER.log(Level.INFO, "Remove path container: {0}, paths: {1}", new Object[]{container, paths});
		for(String path : paths) {
			if(blobStore.directoryExists(container, path)) {
				blobStore.deleteDirectory(container, path);
			}
		}
	}
	
	public Long getFileSize(String container, String name) {
		LOGGER.log(Level.INFO, "Get file size container: {0}, name: {1}", new Object[]{container, name});
		Blob blob = blobStore.getBlob(container, name);
		return blob.getMetadata().getContentMetadata().getContentLength();
	}
	
	public InputStream download(String container, String name) {
		try {
			return blobStore.getBlob(container, name).getPayload().openStream();
		} catch(IOException ioe) {
			return null;
		}
	}
	
	public Long getContainerSize(String containerName) {
		try {
			return cloudFilesClient.getContainerMetadata(containerName).getBytes();
		}catch(Exception e) {
			return 0l;
		}
	}
	
}
