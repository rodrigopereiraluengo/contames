package br.com.contames.jpa;

import java.sql.Timestamp;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.joda.time.LocalDateTime;

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(LocalDateTime value) {
		if(value == null) return null;
		return new Timestamp(value.toDateTime().getMillis());
	}

	@Override
	public LocalDateTime convertToEntityAttribute(Timestamp value) {
		if(value == null) return null;
		return LocalDateTime.fromDateFields(value);
	}
	
}
