package br.com.contames.jpa;

import java.sql.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.joda.time.LocalDate;

@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(LocalDate value) {
		if(value == null) return null;
		return new java.sql.Date(value.toDate().getTime());
	}

	@Override
	public LocalDate convertToEntityAttribute(Date value) {
		if(value == null) return null;
		return new LocalDate(value);
	}

}
