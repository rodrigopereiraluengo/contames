package br.com.contames.session;

import java.util.logging.Logger;

import javax.annotation.PreDestroy;

import net.vidageek.mirror.exception.ReflectionProviderException;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.contames.component.CloudFiles;
import br.com.contames.entity.Pessoa;

@Component
@SessionScoped
public class UsuarioLogged {
	
	private final static Logger LOGGER = Logger.getLogger(UsuarioLogged.class.getCanonicalName());

	private Pessoa usuario;
	private final CloudFiles cloudFiles;
		
	public UsuarioLogged(CloudFiles cloudFiles) {
		this.cloudFiles = cloudFiles;
	}
	
	public Pessoa getCliente() {
		if(usuario == null) {
			return null;
		} else if(usuario.getCliente() == null) {
			return usuario;
		} else {
			return usuario.getCliente();
		}
	}

	public Pessoa getUsuario() {
		return usuario;
	}

	public void setUsuario(Pessoa usuario) {
		this.usuario = usuario;
	}
	
	public Boolean getIsLogged() {
		return usuario != null;
	}
	
	public Boolean getIsCliente() {
		if(getIsLogged() && usuario.equals(getCliente())) {
			return true;
		}
		return false;
	}
	
	public void logout() {
		this.usuario = null;
	}
	
	@PreDestroy
	public void preDestroy() {
		try {
			if(getCliente() != null) {
				LOGGER.info(String.format("DESTROY TEMP CONTAINER %s", "cliente_" + getCliente().getId() + "_temp"));
				cloudFiles.deleteContainer("cliente_" + getCliente().getId() + "_temp");
			}
		} catch(ReflectionProviderException e) { }
	}
		
}