package br.com.contames.session;

import java.util.HashMap;
import java.util.Map;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

@Component
@SessionScoped
public class AssinaturaSession {
	
	private Map<String, String> codigoVerificador = new HashMap<String, String>();
	
	public Map<String, String> getCodigoVerificador() {
		return codigoVerificador;
	}
	
	public Boolean hasCodigoVerificador(String email, String code) {
		return codigoVerificador.containsKey(email) && codigoVerificador.get(email).equals(code);
	}
	
}
