package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import br.com.contames.enumeration.PerfilExibirTipo;
import br.com.contames.enumeration.Status;

@Entity
public class Perfil implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "perfil_seq", sequenceName = "perfil_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "perfil_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 50, message = "{nome.size}")
	@Column(length = 50, nullable = false)
	private String nome;
	
	@NotNull(message = "{painelAgenda.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private PerfilExibirTipo painelAgenda;
	
	@NotNull(message = "{painelConta.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private PerfilExibirTipo painelConta;
	
	@NotNull(message = "{painelCartaoCredito.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private PerfilExibirTipo painelCartaoCredito;
	
	private Boolean isLancamentoRapido;
	
	private Boolean isAcessoDom;
	
	private Boolean isAcessoSeg;
	
	private Boolean isAcessoTer;
	
	private Boolean isAcessoQua;
	
	private Boolean isAcessoQui;
	
	private Boolean isAcessoSex;
	
	private Boolean isAcessoSab;
	
	private LocalTime acessoHoraIni;
	
	private LocalTime acessoHoraFim;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PerfilExibirTipo getPainelAgenda() {
		return painelAgenda;
	}

	public void setPainelAgenda(PerfilExibirTipo painelAgenda) {
		this.painelAgenda = painelAgenda;
	}

	public PerfilExibirTipo getPainelConta() {
		return painelConta;
	}

	public void setPainelConta(PerfilExibirTipo painelConta) {
		this.painelConta = painelConta;
	}

	public PerfilExibirTipo getPainelCartaoCredito() {
		return painelCartaoCredito;
	}

	public void setPainelCartaoCredito(PerfilExibirTipo painelCartaoCredito) {
		this.painelCartaoCredito = painelCartaoCredito;
	}

	public Boolean getIsLancamentoRapido() {
		return isLancamentoRapido;
	}

	public void setIsLancamentoRapido(Boolean isLancamentoRapido) {
		this.isLancamentoRapido = isLancamentoRapido;
	}

	public Boolean getIsAcessoDom() {
		return isAcessoDom;
	}

	public void setIsAcessoDom(Boolean isAcessoDom) {
		this.isAcessoDom = isAcessoDom;
	}

	public Boolean getIsAcessoSeg() {
		return isAcessoSeg;
	}

	public void setIsAcessoSeg(Boolean isAcessoSeg) {
		this.isAcessoSeg = isAcessoSeg;
	}

	public Boolean getIsAcessoTer() {
		return isAcessoTer;
	}

	public void setIsAcessoTer(Boolean isAcessoTer) {
		this.isAcessoTer = isAcessoTer;
	}

	public Boolean getIsAcessoQua() {
		return isAcessoQua;
	}

	public void setIsAcessoQua(Boolean isAcessoQua) {
		this.isAcessoQua = isAcessoQua;
	}

	public Boolean getIsAcessoQui() {
		return isAcessoQui;
	}

	public void setIsAcessoQui(Boolean isAcessoQui) {
		this.isAcessoQui = isAcessoQui;
	}

	public Boolean getIsAcessoSex() {
		return isAcessoSex;
	}

	public void setIsAcessoSex(Boolean isAcessoSex) {
		this.isAcessoSex = isAcessoSex;
	}

	public Boolean getIsAcessoSab() {
		return isAcessoSab;
	}

	public void setIsAcessoSab(Boolean isAcessoSab) {
		this.isAcessoSab = isAcessoSab;
	}

	public LocalTime getAcessoHoraIni() {
		return acessoHoraIni;
	}

	public void setAcessoHoraIni(LocalTime acessoHoraIni) {
		this.acessoHoraIni = acessoHoraIni;
	}

	public LocalTime getAcessoHoraFim() {
		return acessoHoraFim;
	}

	public void setAcessoHoraFim(LocalTime acessoHoraFim) {
		this.acessoHoraFim = acessoHoraFim;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Perfil other = (Perfil) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}