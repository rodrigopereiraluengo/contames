package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.DocTipo;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.util.NumericHelper;

@Entity
public class Lancamento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "lancamento_seq", sequenceName = "lancamento_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lancamento_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "lancamento")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@NotNull(message = "{data.notNull}")
	@Column(name = "_data", nullable = false)
	private LocalDate data;
	
	@ManyToOne
	@NotNull(message = "{favorecido.notNull}")
	@JoinColumn(nullable = false)
	private Pessoa favorecido;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private ItemTipo tipo;
	
	@NotNull(message = "{totalLancamentoItemQuant.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoItemQuant;
	
	@NotNull(message = "{totalLancamentoItemValorUnit.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoItemValorUnit;
	
	@NotNull(message = "{totalLancamentoItemValor.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoItemValor;
	
	@NotNull(message = "{totalLancamentoItemDesconto.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoItemDesconto;
	
	@NotNull(message = "{totalLancamentoItem.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoItem;
	
	@NotNull(message = "{quantLancamentoItem.notNull}")
	@Column(nullable = false)
	private Integer quantLancamentoItem;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 2)
	private DocTipo tipoDoc;
	
	@Size(max = 30, message = "{numeroDoc.max}")
	@Column(length = 30)
	private String numeroDoc;
	
	private LocalDate dataDoc;
	
	private Double valorDoc;
	
	@ManyToOne
	private CartaoCredito cartaoCredito;
	
	private Double diferenca;
	
	private Double limite;
	
	private Double limiteDisponivel;
	
	private Double limiteUtilizado;
		
	@ManyToOne
	private Arquivo arquivoDoc;
	
	@NotNull(message = "{totalLancamentoParcela.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoParcela;
	
	@NotNull(message = "{quantLancamentoParcela.notNull}")
	@Column(nullable = false)
	private Integer quantLancamentoParcela;
	
	private Double percLancamentoRateio;
	
	private Double totalLancamentoRateio;
	
	private Integer quantLancamentoRateio;
	
	private Double totalLancamentoFatura;
	
	private Integer quantLancamentoFatura;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;

	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Pessoa getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(Pessoa favorecido) {
		this.favorecido = favorecido;
	}
	
	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}

	public ItemTipo getTipo() {
		return tipo;
	}

	public void setTipo(ItemTipo tipo) {
		this.tipo = tipo;
	}

	public Double getTotalLancamentoItemQuant() {
		return totalLancamentoItemQuant;
	}

	public void setTotalLancamentoItemQuant(Double totalLancamentoItemQuant) {
		this.totalLancamentoItemQuant = NumericHelper.around(totalLancamentoItemQuant, 3);
	}

	public Double getTotalLancamentoItemValorUnit() {
		return totalLancamentoItemValorUnit;
	}

	public void setTotalLancamentoItemValorUnit(Double totalLancamentoItemValorUnit) {
		this.totalLancamentoItemValorUnit = NumericHelper.around(totalLancamentoItemValorUnit);
	}

	public Double getTotalLancamentoItemValor() {
		return totalLancamentoItemValor;
	}

	public void setTotalLancamentoItemValor(Double totalLancamentoItemValor) {
		this.totalLancamentoItemValor = NumericHelper.around(totalLancamentoItemValor);
	}

	public Double getTotalLancamentoItemDesconto() {
		return totalLancamentoItemDesconto;
	}

	public void setTotalLancamentoItemDesconto(Double totalLancamentoItemDesconto) {
		this.totalLancamentoItemDesconto = NumericHelper.around(totalLancamentoItemDesconto);
	}

	public Double getTotalLancamentoItem() {
		return totalLancamentoItem;
	}

	public void setTotalLancamentoItem(Double totalLancamentoItem) {
		this.totalLancamentoItem = NumericHelper.around(totalLancamentoItem);
	}

	public Integer getQuantLancamentoItem() {
		return quantLancamentoItem;
	}

	public void setQuantLancamentoItem(Integer quantLancamentoItem) {
		this.quantLancamentoItem = quantLancamentoItem;
	}

	public DocTipo getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(DocTipo tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getNumeroDoc() {
		return numeroDoc;
	}

	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}

	public LocalDate getDataDoc() {
		return dataDoc;
	}

	public void setDataDoc(LocalDate dataDoc) {
		this.dataDoc = dataDoc;
	}

	public Double getValorDoc() {
		return valorDoc;
	}

	public void setValorDoc(Double valorDoc) {
		this.valorDoc = NumericHelper.around(valorDoc);
	}
	
	public Double getDiferenca() {
		return diferenca;
	}

	public void setDiferenca(Double diferenca) {
		this.diferenca = NumericHelper.around(diferenca);
	}

	public Arquivo getArquivoDoc() {
		return arquivoDoc;
	}

	public void setArquivoDoc(Arquivo arquivoDoc) {
		this.arquivoDoc = arquivoDoc;
	}
	
	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}
	
	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
		this.limite = NumericHelper.around(limite);
	}

	public Double getLimiteDisponivel() {
		return limiteDisponivel;
	}

	public void setLimiteDisponivel(Double limiteDisponivel) {
		this.limiteDisponivel = NumericHelper.around(limiteDisponivel);
	}

	public Double getLimiteUtilizado() {
		return limiteUtilizado;
	}

	public void setLimiteUtilizado(Double limiteUtilizado) {
		this.limiteUtilizado = NumericHelper.around(limiteUtilizado);
	}

	public Double getTotalLancamentoParcela() {
		return totalLancamentoParcela;
	}

	public void setTotalLancamentoParcela(Double totalLancamentoParcela) {
		this.totalLancamentoParcela = NumericHelper.around(totalLancamentoParcela);
	}

	public Integer getQuantLancamentoParcela() {
		return quantLancamentoParcela;
	}

	public void setQuantLancamentoParcela(Integer quantLancamentoParcela) {
		this.quantLancamentoParcela = quantLancamentoParcela;
	}
	
	public Double getPercLancamentoRateio() {
		return percLancamentoRateio;
	}

	public void setPercLancamentoRateio(Double percLancamentoRateio) {
		this.percLancamentoRateio = NumericHelper.around(percLancamentoRateio);
	}

	public Double getTotalLancamentoRateio() {
		return totalLancamentoRateio;
	}

	public void setTotalLancamentoRateio(Double totalLancamentoRateio) {
		this.totalLancamentoRateio = NumericHelper.around(totalLancamentoRateio);
	}

	public Integer getQuantLancamentoRateio() {
		return quantLancamentoRateio;
	}

	public void setQuantLancamentoRateio(Integer quantLancamentoRateio) {
		this.quantLancamentoRateio = quantLancamentoRateio;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public Double getTotalLancamentoFatura() {
		return totalLancamentoFatura;
	}

	public void setTotalLancamentoFatura(Double totalLancamentoFatura) {
		this.totalLancamentoFatura = NumericHelper.around(totalLancamentoFatura);
	}

	public Integer getQuantLancamentoFatura() {
		return quantLancamentoFatura;
	}

	public void setQuantLancamentoFatura(Integer quantLancamentoFatura) {
		this.quantLancamentoFatura = quantLancamentoFatura;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Lancamento other = (Lancamento) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	
	public Lancamento calculaTotal(List<LancamentoItem> lancamentoItemList, List<LancamentoParcela> lancamentoParcelaList, List<LancamentoRateio> lancamentoRateioList) {
		calculaTotalLancamentoItem(lancamentoItemList);
		calculaTotalLancamentoParcela(lancamentoParcelaList);
		calculaTotalLancamentoRateio(lancamentoRateioList);
		return this;
	}
	
	
	public Lancamento calculaTotalLancamentoItem(List<LancamentoItem> lancamentoItemList) {
		
		quantLancamentoItem = 0;
		
		totalLancamentoItemQuant = 0.0;
		totalLancamentoItemValorUnit = 0.0;
		totalLancamentoItemValor = 0.0;
		totalLancamentoItemDesconto = 0.0;
		totalLancamentoItem = 0.0;
		
		if(lancamentoItemList != null && !lancamentoItemList.isEmpty()) {
			
			quantLancamentoItem = lancamentoItemList.size();
			
			for(LancamentoItem lancamentoItem : lancamentoItemList) {
				
				Double quant 		= lancamentoItem.getQuant();
				Double valorUnit 	= lancamentoItem.getValorUnit();
				
				
				if(quant == null) { quant = 1.0; }
				if(valorUnit == null) { valorUnit = lancamentoItem.getItem().getValor(); }
				Double valor = 0.0;
				if(valorUnit != null && valorUnit != 0) { 
					valor = quant * valorUnit;
					totalLancamentoItemValorUnit += valorUnit;
					totalLancamentoItemValor += valor;
				}
				lancamentoItem.setValor(valor);
				
				totalLancamentoItemQuant += quant;
				
				Double desconto = lancamentoItem.getDesconto();
				if(desconto == null) { desconto = 0.0; }
				Double total = valor - desconto;
				
				totalLancamentoItemDesconto += desconto;
				totalLancamentoItem += total;
				
				lancamentoItem.setTotal(total);
				
			}
		}
		
		return this;
	
	}
	
	public Lancamento calculaTotalLancamentoParcela(List<LancamentoParcela> lancamentoParcelaList) {
		
		quantLancamentoParcela = 0;
		totalLancamentoParcela = 0.0;
		if(lancamentoParcelaList != null && !lancamentoParcelaList.isEmpty()) {
			quantLancamentoParcela = lancamentoParcelaList.size();
			for(LancamentoParcela lancamentoParcela : lancamentoParcelaList) {
				if(lancamentoParcela.getValor() != null) {
					totalLancamentoParcela += lancamentoParcela.getValor();
				}
			}
		}
		setTotalLancamentoParcela(totalLancamentoParcela);
		return this;
	}
	
	public Lancamento corrigeValoresParcela(DocTipo tipoDoc, Double valorDoc, List<LancamentoParcela> lancamentoParcelaList) {
		
		Double total = 0.0;
		if(totalLancamentoItem != null) {
			total += totalLancamentoItem;
		}
		if(DocTipo.FT.equals(tipoDoc) && valorDoc != null) {
			total += valorDoc;
		}
		
		// Corrige Valores
		if(total != 0.0 && !getTotalLancamentoParcela().equals(total)) {
			
			LancamentoParcela lastLancamentoParcela = lancamentoParcelaList.get(lancamentoParcelaList.size() - 1);
			
			// Valor
			Double totalLancamentoParcela = getTotalLancamentoParcela();
			Double diferenca = total - totalLancamentoParcela;
			totalLancamentoParcela += diferenca;
			setTotalLancamentoParcela(totalLancamentoParcela);
			Double lastValor = lastLancamentoParcela.getValor();
			lastValor += diferenca;
			lastLancamentoParcela.setValor(lastValor);
			
		}
		
		return this;
	}
	
	public Lancamento calculaTotalLancamentoRateio(List<LancamentoRateio> lancamentoRateioList) {
		this.percLancamentoRateio = 0.0;
		this.totalLancamentoRateio = 0.0;
		quantLancamentoRateio = 0;
		if(lancamentoRateioList != null && !lancamentoRateioList.isEmpty()) {
			quantLancamentoRateio = lancamentoRateioList.size();
			for(LancamentoRateio lancamentoRateio : lancamentoRateioList) {
				if(lancamentoRateio.getPerc() != null) {
					percLancamentoRateio += lancamentoRateio.getPerc();
				}
				if(lancamentoRateio.getValor() != null) {
					totalLancamentoRateio += lancamentoRateio.getValor();
				}
			}
		}
		
		setTotalLancamentoRateio(totalLancamentoRateio);
		
		return this;
	}
	
	public Lancamento corrigeValoresRateio(List<LancamentoRateio> lancamentoRateioList) {
		
		// Corrige Valores
		if(getTotalLancamentoRateio() != null && getTotalLancamentoRateio() != 0.0 && !getTotalLancamentoParcela().equals(getTotalLancamentoRateio())) {
			
			LancamentoRateio lastLancamentoRateio = lancamentoRateioList.get(lancamentoRateioList.size() - 1);
			
			// Perc
			Double percLancamentoRateio = getPercLancamentoRateio();
			Double diferencaPerc = 100 - percLancamentoRateio;
			percLancamentoRateio += diferencaPerc;
			setPercLancamentoRateio(percLancamentoRateio);
			Double lastPerc = lastLancamentoRateio.getPerc();
			lastPerc += diferencaPerc;
			lastLancamentoRateio.setPerc(lastPerc);
			
			// Valor
			Double totalLancamentoParcela = getTotalLancamentoParcela();
			Double totalLancamentoRateio = getTotalLancamentoRateio();
			Double diferenca = totalLancamentoParcela - totalLancamentoRateio;
			totalLancamentoRateio += diferenca;
			setTotalLancamentoRateio(totalLancamentoRateio);
			Double lastValor = lastLancamentoRateio.getValor();
			lastValor += diferenca;
			lastLancamentoRateio.setValor(lastValor);
			
		}
		
		return this;
	}
	
	
	public Lancamento calculaTotalLancamentoFatura(List<LancamentoFatura> lancamentoFaturaList) {
		this.totalLancamentoFatura = 0.0;
		quantLancamentoFatura = 0;
		if(lancamentoFaturaList != null && !lancamentoFaturaList.isEmpty()) {
			quantLancamentoFatura = lancamentoFaturaList.size();
			for(LancamentoFatura lancamentoFatura : lancamentoFaturaList) {
				if(lancamentoFatura.getLancamentoParcela() == null) {
					totalLancamentoFatura += lancamentoFatura.getTransferencia().getValor();
				} else {
					if(lancamentoFatura.getLancamentoParcela().getBaixaItem() == null) {
						totalLancamentoFatura += lancamentoFatura.getLancamentoParcela().getValor();
					} else {
						totalLancamentoFatura += lancamentoFatura.getLancamentoParcela().getBaixaItem().getValor();
					}
				}
			}
		}
		
		setTotalLancamentoFatura(totalLancamentoFatura);
		
		return this;
	}
	
}