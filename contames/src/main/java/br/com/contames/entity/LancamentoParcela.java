package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;

import br.com.contames.enumeration.BoletoTipo;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.MeioPagamento;
import br.com.contames.util.NumericHelper;
import br.com.contames.util.StringHelper;

@Entity
public class LancamentoParcela implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "lancamentoparcela_seq", sequenceName = "lancamentoparcela_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lancamentoparcela_seq")
    private Long id;

	@ManyToOne
	@NotNull(message = "{lancamento.notNull}")
	@JoinColumn(updatable = false)
	private Lancamento lancamento;
	
	@OneToMany(mappedBy = "lancamentoParcela")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@OneToOne(mappedBy = "lancamentoParcela")
	private BaixaItem baixaItem;
	
	@NotNull(message = "{seq.notNull}")
	@Min(value = 0, message = "{seq.min}")
	@Column(nullable = false)
	private Integer seq;
	
	@NotNull(message = "{data.notNull}")
	@Column(name = "_data", nullable = false)
	private LocalDate data;
	
	@NotNull(message = "{meio.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 7, nullable = false)
	private MeioPagamento meio;
	
	@ManyToOne
	private CartaoCredito cartaoCredito;
	
	private Boolean isLimite;
	
	@ManyToOne
	private Conta conta;
	
	@OneToOne
	private Cheque cheque;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 8)
	private BoletoTipo boletoTipo;
	
	@Size(max = 54, message = "{boletoNumero.size}")
	@Column(length = 54)
	private String boletoNumero;
	
	@ManyToOne
	private Arquivo arquivo;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{valor.notNull}")
	@Column(nullable = false)
	private Double valor;
	
	@NotNull(message = "{perc.notNull}")
	@Column(nullable = false)
	private Double perc;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, updatable = false)
	private ItemTipo tipo;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@Transient
	private String viewid;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public BaixaItem getBaixaItem() {
		return baixaItem;
	}

	public void setBaixaItem(BaixaItem baixaItem) {
		this.baixaItem = baixaItem;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public MeioPagamento getMeio() {
		return meio;
	}

	public void setMeio(MeioPagamento meio) {
		this.meio = meio;
	}
	
	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}
	
	public Boolean getIsLimite() {
		return isLimite;
	}

	public void setIsLimite(Boolean isLimite) {
		this.isLimite = isLimite;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Cheque getCheque() {
		return cheque;
	}

	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}

	public BoletoTipo getBoletoTipo() {
		return boletoTipo;
	}

	public void setBoletoTipo(BoletoTipo boletoTipo) {
		this.boletoTipo = boletoTipo;
	}

	public String getBoletoNumero() {
		return boletoNumero;
	}

	public void setBoletoNumero(String boletoNumero) {
		this.boletoNumero = boletoNumero;
	}
	
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = NumericHelper.around(valor);
	}
	
	public Double getPerc() {
		return perc;
	}

	public void setPerc(Double perc) {
		this.perc = perc;
	}

	public ItemTipo getTipo() {
		return tipo;
	}

	public void setTipo(ItemTipo tipo) {
		this.tipo = tipo;
	}
	
	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}
	
	public String getViewid() {
		if(viewid == null) {
			viewid = StringHelper.createViewId();
		}
		return viewid;
	}

	public void setViewid(String viewid) {
		this.viewid = viewid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final LancamentoParcela other = (LancamentoParcela) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	public String getParcelaDisplay() {
		
		if(lancamento != null) {
			if(lancamento.getQuantLancamentoParcela().equals(1)) {
				return "-";
			} else {
				return (getSeq() + 1) + " / " + lancamento.getQuantLancamentoParcela();
			}
		}
		
		return null;
	}
	
}
