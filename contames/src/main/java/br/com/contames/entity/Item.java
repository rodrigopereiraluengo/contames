package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.ClassificacaoNivel;
import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.MeioPagamento;
import br.com.contames.enumeration.Status;
import br.com.contames.util.NumericHelper;

@Entity
public class Item implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "item_seq", sequenceName = "item_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_seq")
    private Long id;
	
	@NotNull(message = "{cliente.notNull}")
	@ManyToOne
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@ManyToOne
	private Classificacao classificacao;
	
	@OneToMany(mappedBy = "item")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@OneToMany(mappedBy = "item")
	private List<ItemMidia> midiaList = new ArrayList<ItemMidia>();
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 100, message = "{nome.size}")
	@Column(length = 100, nullable = false)
	private String nome;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	@Size(max = 400, message = "{descricao.size}")
	@Column(length = 400)
	private String descricao;
	
	@ManyToOne
	private Descricao unidade;
	
	private Double valorCompra;
		
	private Double valor;
	
	private Double estoque;
		
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private ItemTipo tipo;
	
	private Boolean isLancAuto;
	
	@Min(value = 1, message = "{diaVencimento.min}")
	@Max(value = 31, message = "{diaVencimento.max}")
	private Integer diaVencimento;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn
	private Pessoa favorecido;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 7)
	private MeioPagamento meio;
	
	@ManyToOne
	private Conta conta;
	
	@ManyToOne
	private CartaoCredito cartaoCredito;
	
	private Boolean isLimite;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}

	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}

	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}
	
	public List<ItemMidia> getMidiaList() {
		return midiaList;
	}

	public void setMidiaList(List<ItemMidia> midiaList) {
		this.midiaList = midiaList;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Descricao getUnidade() {
		return unidade;
	}

	public void setUnidade(Descricao unidade) {
		this.unidade = unidade;
	}
	
	public Double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(Double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = NumericHelper.around(valor);
	}
	
	public Double getEstoque() {
		return estoque;
	}

	public void setEstoque(Double estoque) {
		this.estoque = estoque;
	}

	public ItemTipo getTipo() {
		return tipo;
	}

	public void setTipo(ItemTipo tipo) {
		this.tipo = tipo;
	}
	
	public Boolean getIsLancAuto() {
		return isLancAuto;
	}

	public void setIsLancAuto(Boolean isLancAuto) {
		this.isLancAuto = isLancAuto;
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}
	
	public Pessoa getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(Pessoa favorecido) {
		this.favorecido = favorecido;
	}

	public MeioPagamento getMeio() {
		return meio;
	}

	public void setMeio(MeioPagamento meio) {
		this.meio = meio;
	}
	
	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Boolean getIsLimite() {
		return isLimite;
	}

	public void setIsLimite(Boolean isLimite) {
		this.isLimite = isLimite;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}
	
	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}
	
	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Item other = (Item) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	public String getDisplayName() {
		
		if(classificacao != null) {
			
			if(classificacao.getNivel().equals(ClassificacaoNivel.CAT)) {
				return classificacao.getNome() + " - " + nome;
			} else if(classificacao.getNivel().equals(ClassificacaoNivel.FAM)) {
				return classificacao.getClassificacao().getNome() + " - " + classificacao.getNome() + " - " + nome;
			} else if(classificacao.getNivel().equals(ClassificacaoNivel.TIP)) {
				return classificacao.getClassificacao().getClassificacao().getNome() + " - " + classificacao.getClassificacao().getNome() + " - " + classificacao.getNome() + " - " + nome;
			} else {
				return classificacao.getClassificacao().getClassificacao().getClassificacao().getNome() + " - " + classificacao.getClassificacao().getClassificacao().getNome() + " - " + classificacao.getClassificacao().getNome() + " - " + classificacao.getNome() + " - " + nome;
			}
			
		}
		
		return nome;
	}
	
}