package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import br.com.contames.enumeration.LancamentoRateioTipo;
import br.com.contames.util.NumericHelper;
import br.com.contames.util.StringHelper;

@Entity
public class LancamentoRateio implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "lancamentorateio_seq", sequenceName = "lancamentorateio_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lancamentorateio_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{lancamento.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Lancamento lancamento;
	
	@OneToMany(mappedBy = "lancamentoRateio")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@NotNull(message = "{lancamentoRateio.tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoRateioTipo tipo;
	
	@ManyToOne
	private Pessoa pessoa;
	
	@ManyToOne
	private Descricao rateio;
	
	@NotNull(message = "{seq.notNull}")
	@Min(value = 0, message = "{seq.min}")
	@Column(nullable = false)
	private Integer seq;
	
	@NotNull(message = "{perc.notNull}")
	@DecimalMin(value = "0.00", message="{perc.min}")
	@DecimalMax(value = "100.00", message = "{perc.max}")
	@Column(nullable = false)
	private Double perc;
	
	@NotNull(message = "{valor.notNull}")
	@Column(nullable = false)
	private Double valor;
	
	@NotNull(message = "{perc.notNull}")
	@Column(nullable = false)
	private Double percCalc;
	
	@Transient
	private String viewid;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public LancamentoRateioTipo getTipo() {
		return tipo;
	}

	public void setTipo(LancamentoRateioTipo tipo) {
		this.tipo = tipo;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Descricao getRateio() {
		return rateio;
	}

	public void setRateio(Descricao rateio) {
		this.rateio = rateio;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Double getPerc() {
		return perc;
	}

	public void setPerc(Double perc) {
		this.perc = NumericHelper.around(perc);
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = NumericHelper.around(valor);
	}
	
	public Double getPercCalc() {
		return percCalc;
	}

	public void setPercCalc(Double percCalc) {
		this.percCalc = percCalc;
	}
	
	public String getViewid() {
		if(viewid == null) {
			viewid = StringHelper.createViewId();
		}
		return viewid;
	}

	public void setViewid(String viewid) {
		this.viewid = viewid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final LancamentoRateio other = (LancamentoRateio) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	public String getDisplayNome() {
		if(LancamentoRateioTipo.PES.equals(tipo)) {
			if(pessoa != null && pessoa.getDisplayNome() != null) {
				return pessoa.getDisplayNome();
			}
		} else if(LancamentoRateioTipo.RAT.equals(tipo)) {
			if(rateio != null && rateio.getNome() != null) {
				return rateio.getNome();
			}
		}
		return null;
	}
	
}