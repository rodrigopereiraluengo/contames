package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.com.contames.enumeration.MidiaTipo;
import br.com.contames.enumeration.Status;

@Entity
public class ItemMidia implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "itemmidia_seq", sequenceName = "itemmidia_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "itemmidia_seq")
    private Long id;
	
	@NotNull(message = "{item.notNull}")
	@ManyToOne
	@JoinColumn(updatable = false, nullable = false)
	private Item item;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private MidiaTipo tipo;
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 100, message = "{nome.size}")
	@Column(length = 100, nullable = false)
	private String nome;
	
	@Size(max = 4000, message = "{descricao.size}")
	@Column(length = 4000)
	private String descricao;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public MidiaTipo getTipo() {
		return tipo;
	}

	public void setTipo(MidiaTipo tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ItemMidia other = (ItemMidia) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}
