package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.util.NumericHelper;

@Entity
public class Baixa implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "baixa_seq", sequenceName = "baixa_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "baixa_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "baixa")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@NotNull(message = "{data.notNull}")
	@Column(name = "_data", nullable = false)
	private LocalDate data;
	
	@Min(value = 1, message = "{quantBaixaItem.min}")
	@NotNull(message = "{quantBaixaItem.notNull}")
	@Column(nullable = false)
	private Integer quantBaixaItem;
	
	@NotNull(message = "{totaLancamentoParcela.notNull}")
	@Column(nullable = false)
	private Double totalLancamentoParcela;
	
	@NotNull(message = "{totalBaixaItem.notNull}")
	@Column(nullable = false)
	private Double totalBaixaItem;
	
	@NotNull(message = "{diferenca.notNull}")
	@Column(nullable = false)
	private Double totalDiferenca;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private ItemTipo tipo;

	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Integer getQuantBaixaItem() {
		return quantBaixaItem;
	}

	public void setQuantBaixaItem(Integer quantBaixaItem) {
		this.quantBaixaItem = quantBaixaItem;
	}

	public Double getTotalLancamentoParcela() {
		return totalLancamentoParcela;
	}

	public void setTotalLancamentoParcela(Double totalLancamentoParcela) {
		this.totalLancamentoParcela = NumericHelper.around(totalLancamentoParcela);
	}

	public Double getTotalBaixaItem() {
		return totalBaixaItem;
	}

	public void setTotalBaixaItem(Double totalBaixaItem) {
		this.totalBaixaItem = NumericHelper.around(totalBaixaItem);
	}

	public Double getTotalDiferenca() {
		return totalDiferenca;
	}

	public void setTotalDiferenca(Double totalDiferenca) {
		this.totalDiferenca = NumericHelper.around(totalDiferenca);
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}
	
	public ItemTipo getTipo() {
		return tipo;
	}

	public void setTipo(ItemTipo tipo) {
		this.tipo = tipo;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Baixa other = (Baixa) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	
	public Baixa calculaTotalBaixaItem(List<BaixaItem> baixaItemList) {
		
		quantBaixaItem 			= 0; 
		totalLancamentoParcela 	= 0.0;
		totalBaixaItem 			= 0.0;
		totalDiferenca 			= 0.0;
		
		if(baixaItemList != null) { 
			quantBaixaItem = baixaItemList.size();
			for(BaixaItem baixaItem : baixaItemList) {
				LancamentoParcela lancamentoParcela = baixaItem.getLancamentoParcela();
				totalLancamentoParcela += lancamentoParcela.getValor();
				totalBaixaItem += baixaItem.getValor();
				totalDiferenca += lancamentoParcela.getValor() - baixaItem.getValor();
			}
		}
				
		return this;
	}
	
}
