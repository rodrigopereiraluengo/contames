package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.util.NumericHelper;

@Entity
public class Compensacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "compensacao_seq", sequenceName = "compensacao_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "compensacao_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "compensacao")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@ManyToOne
	@NotNull(message = "{conta.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Conta conta;
	
	@NotNull(message = "{data.notNull}")
	@Column(name = "_data", nullable = false)
	private LocalDate data;
	
	@NotNull(message = "{saldoAnterior.notNull}")
	@Column(nullable = false)
	private Double saldoAnterior;
	
	@NotNull(message = "{saldoFinal.notNull}")
	@Column(nullable = false)
	private Double saldo;
	
	@NotNull(message = "{diferenca.notNull}")
	@Column(nullable = false)
	private Double diferenca;
	
	@NotNull(message = "{totalDebito.notNull}")
	@Column(nullable = false)
	private Double totalDebito;
	
	@NotNull(message = "{totalCredito.notNull}")
	@Column(nullable = false)
	private Double totalCredito;
	
	@NotNull(message = "{total.notNull}")
	@Column(nullable = false)
	private Double total;
	
	@NotNull(message = "{quantCompensacaoItem.notNull}")
	@Column(nullable = false)
	private Integer quantCompensacaoItem;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;

	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Double getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(Double saldoAnterior) {
		this.saldoAnterior = NumericHelper.around(saldoAnterior);
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = NumericHelper.around(saldo);
	}

	public Double getDiferenca() {
		return diferenca;
	}

	public void setDiferenca(Double diferenca) {
		this.diferenca = NumericHelper.around(diferenca);
	}

	public Double getTotalDebito() {
		return totalDebito;
	}

	public void setTotalDebito(Double totalDebito) {
		this.totalDebito = NumericHelper.around(totalDebito);
	}

	public Double getTotalCredito() {
		return totalCredito;
	}

	public void setTotalCredito(Double totalCredito) {
		this.totalCredito = NumericHelper.around(totalCredito);
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = NumericHelper.around(total);
	}

	public Integer getQuantCompensacaoItem() {
		return quantCompensacaoItem;
	}

	public void setQuantCompensacaoItem(Integer quantCompensacaoItem) {
		this.quantCompensacaoItem = quantCompensacaoItem;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Compensacao other = (Compensacao) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	
	public Compensacao calculaTotalCompensacaoItem(List<CompensacaoItem> compensacaoItemList) {
		
		this.total = 0.0;
		this.totalCredito = 0.0;
		this.totalDebito = 0.0;
		this.diferenca = 0.0;
		Double _saldo = this.saldoAnterior;
		
		if(compensacaoItemList != null) {
			this.quantCompensacaoItem = compensacaoItemList.size();
			for(CompensacaoItem compensacaoItem : compensacaoItemList) {
				if(compensacaoItem.getDebito() != null) {
					this.totalDebito += compensacaoItem.getDebito();
					_saldo -= compensacaoItem.getDebito();
				} else if(compensacaoItem.getCredito() != null) {
					this.totalCredito += compensacaoItem.getCredito();
					_saldo += compensacaoItem.getCredito();
				}
				compensacaoItem.setSaldo(NumericHelper.around(_saldo));
			}
			this.total = NumericHelper.around(_saldo);
			if(getSaldo() != null) {
				this.diferenca = getSaldo() - NumericHelper.around(_saldo);
			}
		}
		
		return this;
	}
	
}
