package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.DescricaoTipo;
import br.com.contames.enumeration.Status;

@Entity
public class Descricao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "descricao_seq", sequenceName = "descricao_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "descricao_seq")
    private Long id;
	
	@ManyToOne
	@JoinColumn(updatable = false)
	private Pessoa cliente;
	
	@NotNull(message = "{descricao.descricaoTipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 16, nullable = false)
	private DescricaoTipo descricaoTipo;
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 50, message = "{nome.size}")
	@Column(length = 50, nullable = false)
	private String nome;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}

	public DescricaoTipo getDescricaoTipo() {
		return descricaoTipo;
	}

	public void setDescricaoTipo(DescricaoTipo descricaoTipo) {
		this.descricaoTipo = descricaoTipo;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Descricao other = (Descricao) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}