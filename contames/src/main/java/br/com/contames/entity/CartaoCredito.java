package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.Status;
import br.com.contames.util.NumericHelper;

@Entity
public class CartaoCredito implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "cartaocredito_seq", sequenceName = "cartaocredito_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cartaocredito_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "cartaoCredito")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@ManyToOne
	private Descricao bandeira;
	
	@NotBlank(message="{nome.notBlank}")
	@Size(max = 50, message = "{nome.size}")
	@Column(length = 50, nullable = false)
	private String nome;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	private Boolean isPrincipal;
	
	@ManyToOne
	private Pessoa titular;
	
	@NotNull(message = "{limite.notNull}")
	@Column(nullable = false)
	private Double limite;
	
	private LocalDate dataFatuAnt;
	
	private Double valorFaturaAnt;
	
	private Double limiteDisponivelAnt;
		
	@NotNull(message = "{limiteDisponivel.notNull}")
	@Column(nullable = false)
	private Double limiteDisponivel;
	
	@NotNull(message = "{limiteUtilizado.notNull}")
	@Column(nullable = false)
	private Double limiteUtilizado;
	
	@Min(value = 1, message = "{diaVencimento.min}")
	@Max(value = 31, message = "{diaVencimento.max}")
	private Integer diaVencimento;
	
	@Min(value = 30, message = "{diasPagar.min}")
	@Max(value = 100, message = "{diasPagar.max}")
	private Integer diasPagar;
		
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
			
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public Descricao getBandeira() {
		return bandeira;
	}

	public void setBandeira(Descricao bandeira) {
		this.bandeira = bandeira;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Boolean getIsPrincipal() {
		return isPrincipal;
	}

	public void setIsPrincipal(Boolean isPrincipal) {
		this.isPrincipal = isPrincipal;
	}

	public Pessoa getTitular() {
		return titular;
	}

	public void setTitular(Pessoa titular) {
		this.titular = titular;
	}

	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
		this.limite = NumericHelper.around(limite);
	}
	
	public LocalDate getDataFatuAnt() {
		return dataFatuAnt;
	}

	public void setDataFatuAnt(LocalDate dataFatuAnt) {
		this.dataFatuAnt = dataFatuAnt;
	}
	
	public Double getValorFaturaAnt() {
		return valorFaturaAnt;
	}

	public void setValorFaturaAnt(Double valorFaturaAnt) {
		this.valorFaturaAnt = NumericHelper.around(valorFaturaAnt);
	}

	public Double getLimiteDisponivelAnt() {
		return limiteDisponivelAnt;
	}

	public void setLimiteDisponivelAnt(Double limiteDisponivelAnt) {
		this.limiteDisponivelAnt = NumericHelper.around(limiteDisponivelAnt);
	}
	
	public Double getLimiteDisponivel() {
		return limiteDisponivel;
	}

	public void setLimiteDisponivel(Double limiteDisponivel) {
		this.limiteDisponivel = NumericHelper.around(limiteDisponivel);
	}

	public Double getLimiteUtilizado() {
		return limiteUtilizado;
	}

	public void setLimiteUtilizado(Double limiteUtilizado) {
		this.limiteUtilizado = NumericHelper.around(limiteUtilizado);
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}
	
	public Integer getDiasPagar() {
		return diasPagar;
	}

	public void setDiasPagar(Integer diasPagar) {
		this.diasPagar = diasPagar;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final CartaoCredito other = (CartaoCredito) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}
