package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.URL;
import org.joda.time.LocalDateTime;

import br.com.caelum.stella.bean.validation.CNPJ;
import br.com.caelum.stella.bean.validation.CPF;
import br.com.caelum.stella.bean.validation.TituloEleitoral;
import br.com.contames.enumeration.PessoaSexo;
import br.com.contames.enumeration.PessoaTipo;
import br.com.contames.enumeration.Status;
import br.com.contames.enumeration.Uf;

@Entity
public class Pessoa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "pessoa_seq", sequenceName = "pessoa_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_seq")
    private Long id;
	
	@Column(length = 13)
	private String payerId;
	
	@OneToOne(mappedBy = "cliente")
	private Assinatura assinatura;
	
	@ManyToOne
	@JoinColumn(updatable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "pessoa")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();

	@NotNull(message = "{pessoaTipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 1, nullable = false)
	private PessoaTipo pessoaTipo;
	
	/* Pessoa Fisica */
	@Size(max = 100, message = "{nome.size}")
	@Column(length = 100)
	private String nome;
	
	@Size(max = 50, message = "{apelido.size}")
	@Column(length = 50)
	private String apelido;
	
	@CPF(message = "{cpf.cpf}")
	@Size(max = 14, message = "{cpf.max}")
	@Column(length = 14)
	private String cpf;
	
	@Size(max = 20, message = "{rg.max}")
	@Column(length = 20)
	private String rg;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 1)
	private PessoaSexo sexo;
	
	@TituloEleitoral(message = "{tituloEleitor.tituloEleitoral}")
	@Size(max = 14, message = "{tituloEleitor.size}")
	@Column(length = 14)
	private String tituloEleitor;
		
	/* Pessoa Fisica */
	@Size(max = 100, message = "{razaoSocial.size}")
	@Column(length = 100)
	private String razaoSocial;
	
	@Size(max = 50, message = "{fantasia.size}")
	@Column(length = 50)
	private String fantasia;
	
	@CNPJ(message = "{cnpj.cnpj}")
	@Size(max = 18, message = "{cnpj.size}")
	@Column(length = 18)
	private String cnpj;
	
	@Size(max = 20, message = "{inscEst.size}")
	@Column(length = 20)
	private String inscEst;
	
	@Size(max = 20, message = "{inscMun.size}")
	@Column(length = 20)
	private String inscMun;
		
	@ManyToOne
	private Descricao categoria;
	
	@Size(max = 9, message = "{cep.size}")
	@Column(length = 9)
	private String cep;
	
	@Size(max = 100, message = "{endereco.size}")
	@Column(length = 100)
	private String endereco;
	
	@Size(max = 10, message = "{numero.size}")
	@Column(length = 10)
	private String numero;
	
	@Size(max = 100, message = "{complemento.size}")
	@Column(length = 100)
	private String complemento;
	
	@Size(max = 50, message = "{bairro.size}")
	@Column(length = 50)
	private String bairro;
	
	@Size(max = 50, message = "{cidade.size}")
	@Column(length = 50)
	private String cidade;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 2)
	private Uf uf;
	
	@Size(max = 25, message = "{fone1.size}")
	@Column(length = 25)
	private String fone1;
	
	@Size(max = 25, message = "{fone2.size}")
	@Column(length = 25)
	private String fone2;
	
	@Size(max = 25, message = "{fone3.size}")
	@Column(length = 25)
	private String fone3;
	
	@Size(max = 25, message = "{fone4.size}")
	@Column(length = 25)
	private String fone4;
	
	@Email(message = "{email.email}")
	@Size(max = 127, message = "{email.size}")
	@Column(length = 127)
	private String email;
	
	@URL(message = "{site.url}")
	@Size(max = 200, message = "{site.size}")
	@Column(length = 200)
	private String site;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
		
	@Size(min = 6, max = 20, message = "{senha.size}")
	@Column(length = 20)
	private String senha;
	
	private Boolean isReceberEmailVencimentos;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	
	public Assinatura getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(Assinatura assinatura) {
		this.assinatura = assinatura;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public PessoaTipo getPessoaTipo() {
		return pessoaTipo;
	}

	public void setPessoaTipo(PessoaTipo pessoaTipo) {
		this.pessoaTipo = pessoaTipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public PessoaSexo getSexo() {
		return sexo;
	}

	public void setSexo(PessoaSexo sexo) {
		this.sexo = sexo;
	}

	public String getTituloEleitor() {
		return tituloEleitor;
	}

	public void setTituloEleitor(String tituloEleitor) {
		this.tituloEleitor = tituloEleitor;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getFantasia() {
		return fantasia;
	}

	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscEst() {
		return inscEst;
	}

	public void setInscEst(String inscEst) {
		this.inscEst = inscEst;
	}

	public String getInscMun() {
		return inscMun;
	}

	public void setInscMun(String inscMun) {
		this.inscMun = inscMun;
	}

	public Descricao getCategoria() {
		return categoria;
	}

	public void setCategoria(Descricao categoria) {
		this.categoria = categoria;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public String getFone1() {
		return fone1;
	}

	public void setFone1(String fone1) {
		this.fone1 = fone1;
	}

	public String getFone2() {
		return fone2;
	}

	public void setFone2(String fone2) {
		this.fone2 = fone2;
	}

	public String getFone3() {
		return fone3;
	}

	public void setFone3(String fone3) {
		this.fone3 = fone3;
	}

	public String getFone4() {
		return fone4;
	}

	public void setFone4(String fone4) {
		this.fone4 = fone4;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Boolean getIsReceberEmailVencimentos() {
		return isReceberEmailVencimentos;
	}

	public void setIsReceberEmailVencimentos(Boolean isReceberEmailVencimentos) {
		this.isReceberEmailVencimentos = isReceberEmailVencimentos;
	}
	
	public boolean getIsUsuario() {
		return this.email != null && this.senha != null;
	}
	
	public boolean getIsCliente() {
		return this.cliente == null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Pessoa other = (Pessoa) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	
	
	public String getDisplayNome() {
		return PessoaTipo.J.equals(pessoaTipo) ? (fantasia == null ? razaoSocial : fantasia) : (apelido == null ? nome : apelido);
	}
}
