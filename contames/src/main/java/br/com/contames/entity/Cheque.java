package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.LancamentoStatus;

@Entity
public class Cheque implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "cheque_seq", sequenceName = "cheque_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cheque_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@ManyToOne
	private TalaoCheque talaoCheque;
	
	@ManyToOne
	private Pessoa banco;
	
	@NotNull(message = "{numero.notNull}")
	@Min(value = 1, message = "{numero.min}")
	@Column(nullable = false)
	private Integer numero;
	
	@ManyToOne
	private Pessoa nome;
	
	@ManyToOne
	private Pessoa favorecido;
	
	@OneToOne(mappedBy = "cheque")
	private Transferencia transferencia;
	
	@OneToOne(mappedBy = "cheque")
	private LancamentoParcela lancamentoParcela;
	
	@OneToOne(mappedBy = "cheque")
	private BaixaItem baixaItem;
	
	@Column(name = "_data")
	private LocalDate data;
	
	private Double valor;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@ManyToOne
	private Descricao motivo;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}

	public TalaoCheque getTalaoCheque() {
		return talaoCheque;
	}

	public void setTalaoCheque(TalaoCheque talaoCheque) {
		this.talaoCheque = talaoCheque;
	}

	public Pessoa getBanco() {
		return banco;
	}

	public void setBanco(Pessoa banco) {
		this.banco = banco;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Pessoa getNome() {
		return nome;
	}

	public void setNome(Pessoa nome) {
		this.nome = nome;
	}
	
	public Pessoa getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(Pessoa favorecido) {
		this.favorecido = favorecido;
	}
	
	public Transferencia getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(Transferencia transferencia) {
		this.transferencia = transferencia;
	}
	
	public LancamentoParcela getLancamentoParcela() {
		return lancamentoParcela;
	}

	public void setLancamentoParcela(LancamentoParcela lancamentoParcela) {
		this.lancamentoParcela = lancamentoParcela;
	}
	
	public BaixaItem getBaixaItem() {
		return baixaItem;
	}

	public void setBaixaItem(BaixaItem baixaItem) {
		this.baixaItem = baixaItem;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}
	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public Descricao getMotivo() {
		return motivo;
	}

	public void setMotivo(Descricao motivo) {
		this.motivo = motivo;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Cheque other = (Cheque) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	
	public LocalDate getDisplayData() {
		LocalDate displayData = data;
		
		if(transferencia == null) {
			switch(status) {
			case LAN:
			case CAN:
				if(data == null && lancamentoParcela != null) {
					displayData = lancamentoParcela.getData();
				}
				break;
			case BXD:
				if(data == null && baixaItem != null) {
					displayData = baixaItem.getBaixa().getData();
				}
				break;
			case COM:
				if(baixaItem != null) {
					displayData = baixaItem.getCompensacaoItem().getCompensacao().getData();
				}
			}
		} else {
			switch(status) {
			case LAN:
			case CAN:
				if(data == null && transferencia != null) {
					displayData = transferencia.getData();
				}
				break;
			case COM:
				if(data == null) {
					displayData = transferencia.getCompensacaoItem().getCompensacao().getData();
				}
			}
		}
		return displayData;
	}
	
	public String getDisplayDescricao() {
		if(transferencia == null) {
			return favorecido.getDisplayNome();
		} else {
			return transferencia.getTipo().toString();
		}
	}
}