package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.Status;

@Entity
public class Plano implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "plano_seq", sequenceName = "plano_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plano_seq")
    private Long id;

	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 40, message = "{nome.size}")
	@Column(length = 40, nullable = false)
	private String nome;
	
	@Size(max = 400, message = "{descricao.size}")
	@Column(length = 400)
	private String descricao;
	
	@NotNull(message = "{valor.notNull}")
	@Min(value = 0, message = "{valor.min}")
	@Column(nullable = false)
	private Double valor;
	
	@NotNull(message = "{contas.notNull}")
	@Min(value = 0, message = "{contas.min}")
	@Column(nullable = false)
	private Integer contas;
	
	@NotNull(message = "{cartoesCredito.notNull}")
	@Min(value = 0, message = "{cartoesCredito.min}")
	@Column(nullable = false)
	private Integer cartoesCredito;
	
	@NotNull(message = "{lancamentos.notNull}")
	@Min(value = 0, message = "{lancamentos.min}")
	@Column(nullable = false)
	private Integer lancamentos;
	
	@NotNull(message = "{disco.notNull}")
	@Min(value = 0, message = "{disco.min}")
	@Column(nullable = false)
	private Integer disco;
	
	@NotNull(message = "{bancoDados.notNull}")
	@Min(value = 0, message = "{bancoDados.min}")
	@Column(nullable = false)
	private Integer bancoDados;
	
	@NotNull(message = "{usuarios.notNull}")
	@Min(value = 0, message = "{usuarios.min}")
	@Column(nullable = false)
	private Integer usuarios;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	private Boolean isGratis;

	@NotNull(message = "{criacao.notNull}")
	@Column(insertable = true, updatable = false, nullable = false)
	private LocalDateTime criacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public Integer getContas() {
		return contas;
	}

	public void setContas(Integer contas) {
		this.contas = contas;
	}

	public Integer getCartoesCredito() {
		return cartoesCredito;
	}

	public void setCartoesCredito(Integer cartoesCredito) {
		this.cartoesCredito = cartoesCredito;
	}

	public Integer getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(Integer lancamentos) {
		this.lancamentos = lancamentos;
	}

	public Integer getDisco() {
		return disco;
	}

	public void setDisco(Integer disco) {
		this.disco = disco;
	}

	public Integer getBancoDados() {
		return bancoDados;
	}

	public void setBancoDados(Integer bancoDados) {
		this.bancoDados = bancoDados;
	}
	
	public Integer getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Integer usuarios) {
		this.usuarios = usuarios;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Boolean getIsGratis() {
		return isGratis;
	}

	public void setIsGratis(Boolean isGratis) {
		this.isGratis = isGratis;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Plano other = (Plano) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}
