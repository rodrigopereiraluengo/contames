package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDateTime;

import br.com.contames.util.StringHelper;

@Entity
public class Arquivo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "arquivo_seq", sequenceName = "arquivo_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "arquivo_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false)
	private Pessoa cliente;
	
	@Transient
	private String temp;
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 1000, message = "{arquivo.nome.size}")
	@Column(length = 1000)
	private String nome;
	
	@NotBlank(message = "{contentType.notBlank}")
	@Size(max = 100, message = "{contentType.size}")
	@Column(length = 100)
	private String contentType;
	
	@NotNull(message = "{size.notNull}")
	private Long size;
	
	@Min(value = 0, message = "{seq.min}")
	private Integer seq;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	
	// Relacionamentos
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Item item;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa pessoa;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Conta conta;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private CartaoCredito cartaoCredito;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private TalaoCheque talaoCheque;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Lancamento lancamento;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private LancamentoParcela lancamentoParcela;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private LancamentoItem lancamentoItem;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private LancamentoRateio lancamentoRateio;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Baixa baixa;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private BaixaItem baixaItem;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Transferencia transferencia;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Compensacao compensacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private CompensacaoItem compensacaoItem;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public String getTemp() {
		if(temp == null) {
			temp = StringHelper.createViewId();
		}
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getNome() {
		return nome;
	}
	
	public String getNomeClear() {
		return StringHelper.unAccent(nome).toLowerCase();
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}
	
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}
	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public TalaoCheque getTalaoCheque() {
		return talaoCheque;
	}

	public void setTalaoCheque(TalaoCheque talaoCheque) {
		this.talaoCheque = talaoCheque;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}
	
	public LancamentoParcela getLancamentoParcela() {
		return lancamentoParcela;
	}

	public void setLancamentoParcela(LancamentoParcela lancamentoParcela) {
		this.lancamentoParcela = lancamentoParcela;
	}
	
	public LancamentoItem getLancamentoItem() {
		return lancamentoItem;
	}

	public void setLancamentoItem(LancamentoItem lancamentoItem) {
		this.lancamentoItem = lancamentoItem;
	}
	
	public LancamentoRateio getLancamentoRateio() {
		return lancamentoRateio;
	}

	public void setLancamentoRateio(LancamentoRateio lancamentoRateio) {
		this.lancamentoRateio = lancamentoRateio;
	}

	public Baixa getBaixa() {
		return baixa;
	}

	public void setBaixa(Baixa baixa) {
		this.baixa = baixa;
	}
	
	public BaixaItem getBaixaItem() {
		return baixaItem;
	}

	public void setBaixaItem(BaixaItem baixaItem) {
		this.baixaItem = baixaItem;
	}

	public Transferencia getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(Transferencia transferencia) {
		this.transferencia = transferencia;
	}

	public Compensacao getCompensacao() {
		return compensacao;
	}

	public void setCompensacao(Compensacao compensacao) {
		this.compensacao = compensacao;
	}
	
	public CompensacaoItem getCompensacaoItem() {
		return compensacaoItem;
	}

	public void setCompensacaoItem(CompensacaoItem compensacaoItem) {
		this.compensacaoItem = compensacaoItem;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Arquivo other = (Arquivo) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }

}