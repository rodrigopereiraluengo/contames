package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;

import br.com.contames.enumeration.ItemTipo;
import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.util.NumericHelper;
import br.com.contames.util.StringHelper;

@Entity
public class CompensacaoItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "compensacaoitem_seq", sequenceName = "compensacaoitem_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "compensacaoitem_seq")
    private Long id;
	
	@OneToMany(mappedBy = "compensacaoItem")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@ManyToOne
	@NotNull(message = "{compensacao.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Compensacao compensacao;
	
	@OneToOne
	@JoinColumn(updatable = false)
	private BaixaItem baixaItem;
	
	@ManyToOne
	@JoinColumn
	private Pessoa favorecido;
	
	@ManyToOne
	@JoinColumn
	private Item item;
	
	@OneToOne
	@JoinColumn(updatable = false)
	private Transferencia transferencia;
	
	@NotNull(message = "{seq.notNull}")
	@Min(value = 0, message = "{seq.min}")
	@Column(nullable = false)
	private Integer seq;
	
	@Column(name = "_data")
	private LocalDate data;
	
	private Double debito;
	
	private Double credito;
	
	@NotNull(message = "{saldo.notNull}")
	@Column(nullable = false)
	private Double saldo;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false, updatable = false)
	private ItemTipo tipo;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@Transient
	private String viewid;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public Compensacao getCompensacao() {
		return compensacao;
	}

	public void setCompensacao(Compensacao compensacao) {
		this.compensacao = compensacao;
	}
	
	public BaixaItem getBaixaItem() {
		return baixaItem;
	}

	public void setBaixaItem(BaixaItem baixaItem) {
		this.baixaItem = baixaItem;
	}
	
	public Pessoa getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(Pessoa favorecido) {
		this.favorecido = favorecido;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	public Transferencia getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(Transferencia transferencia) {
		this.transferencia = transferencia;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Double getDebito() {
		return debito;
	}

	public void setDebito(Double debito) {
		this.debito = NumericHelper.around(debito);
	}

	public Double getCredito() {
		return credito;
	}

	public void setCredito(Double credito) {
		this.credito = NumericHelper.around(credito);
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = NumericHelper.around(saldo);
	}
	
	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public ItemTipo getTipo() {
		return tipo;
	}

	public void setTipo(ItemTipo tipo) {
		this.tipo = tipo;
	}

	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}
	
	public String getViewid() {
		if(viewid == null) {
			viewid = StringHelper.createViewId();
		}
		return viewid;
	}

	public void setViewid(String viewid) {
		this.viewid = viewid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final CompensacaoItem other = (CompensacaoItem) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}
