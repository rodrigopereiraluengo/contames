package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import urn.ebay.apis.eBLBaseComponents.RecurringPaymentsProfileStatusType;
import br.com.contames.enumeration.AssinaturaStatus;

@Entity
public class Assinatura implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "assinatura_seq", sequenceName = "assinatura_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "assinatura_seq")
    private Long id;
	
	@Column(length = 14)
	private String profileId;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 16)
	private RecurringPaymentsProfileStatusType profileStatus; 
	
	@OneToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(nullable = false, updatable = false)
	private Pessoa cliente;
	
	@ManyToOne
	@NotNull(message = "{plano.notNull}")
	@JoinColumn(nullable = false)
	private Plano plano;
	
	@OneToMany(mappedBy = "assinatura")
	private List<AssinaturaPagamento> assinaturaPagamentoList = new ArrayList<AssinaturaPagamento>();
	
	@AssertTrue(message = "{isAceitaTermos.assertTrue}")
	@Column(nullable = false)
	private Boolean isAceitaTermos;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private AssinaturaStatus status;
	
	@NotNull(message = "{vencimento.notNull}")
	@Column(nullable = false)
	private LocalDateTime vencimento;
	
	@Column(length = 30)
	private String sessionId;
	
	@NotNull(message = "{criacao.notNull}")
	@Column(insertable = true, updatable = false, nullable = false)
	private LocalDateTime criacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	private LocalDate canceladoEm;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	
	public RecurringPaymentsProfileStatusType getProfileStatus() {
		return profileStatus;
	}

	public void setProfileStatus(RecurringPaymentsProfileStatusType profileStatus) {
		this.profileStatus = profileStatus;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public List<AssinaturaPagamento> getAssinaturaPagamentoList() {
		return assinaturaPagamentoList;
	}

	public void setAssinaturaPagamentoList(List<AssinaturaPagamento> assinaturaPagamentoList) {
		this.assinaturaPagamentoList = assinaturaPagamentoList;
	}

	public Boolean getIsAceitaTermos() {
		return isAceitaTermos;
	}

	public void setIsAceitaTermos(Boolean isAceitaTermos) {
		this.isAceitaTermos = isAceitaTermos;
	}

	public AssinaturaStatus getStatus() {
		return status;
	}

	public void setStatus(AssinaturaStatus status) {
		this.status = status;
	}

	public LocalDateTime getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDateTime vencimento) {
		this.vencimento = vencimento;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}
	
	public LocalDate getCanceladoEm() {
		return canceladoEm;
	}

	public void setCanceladoEm(LocalDate canceladoEm) {
		this.canceladoEm = canceladoEm;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Assinatura other = (Assinatura) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }

}
