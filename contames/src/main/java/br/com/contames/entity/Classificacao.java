package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.com.contames.enumeration.ClassificacaoNivel;
import br.com.contames.enumeration.ItemTipo;

@Entity
public class Classificacao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "classificacao_seq", sequenceName = "classificacao_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "classificacao_seq")
    private Long id;
	
	@ManyToOne
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@ManyToOne
	private Classificacao classificacao;
	
	@OneToMany(mappedBy = "classificacao")
	private List<Classificacao> classificacaoList = new ArrayList<Classificacao>();
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 100, message = "{nome.size}")
	@Column(length = 100, nullable = false)
	private String nome;
	
	@NotNull(message = "{nivel.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private ClassificacaoNivel nivel;
	
	@NotNull(message = "{index.notNull}")
	@Min(value = 0, message = "{index.min}")
	@Column(name = "_index", nullable = false)
	private Integer index;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false, updatable = false)
	private ItemTipo tipo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}

	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}
	
	public List<Classificacao> getClassificacaoList() {
		return classificacaoList;
	}

	public void setClassificacaoList(List<Classificacao> classificacaoList) {
		if(classificacaoList != null) {
			for(Classificacao classificacao : classificacaoList) {
				classificacao.setClassificacao(this);
			}
		}
		this.classificacaoList = classificacaoList;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public ClassificacaoNivel getNivel() {
		return nivel;
	}

	public void setNivel(ClassificacaoNivel nivel) {
		this.nivel = nivel;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
	public ItemTipo getTipo() {
		return tipo;
	}

	public void setTipo(ItemTipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Classificacao other = (Classificacao) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}