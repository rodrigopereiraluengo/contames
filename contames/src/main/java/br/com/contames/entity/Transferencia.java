package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.LancamentoStatus;
import br.com.contames.enumeration.TransferenciaTipo;
import br.com.contames.util.NumericHelper;

@Entity
public class Transferencia implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "transferencia_seq", sequenceName = "transferencia_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transferencia_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "transferencia")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@NotNull(message = "{data.notNull}")
	@Column(name = "_data", nullable = false)
	private LocalDate data;
	
	@NotNull(message = "{tipo.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private TransferenciaTipo tipo;
	
	@OneToOne(mappedBy = "transferencia")
	private CompensacaoItem compensacaoItem;
	
	@ManyToOne
	private Conta conta;
	
	@OneToOne
	private Cheque cheque;
	
	@ManyToOne
	private Arquivo arquivo;
	
	@ManyToOne
	private CartaoCredito cartaoCredito;
	
	private Boolean isLimite;
	
	@ManyToOne
	@NotNull(message = "{conta.destino.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Conta destino;
	
	@NotNull(message = "{valor.notNull}")
	@DecimalMin(value = "0.01", message = "{transferencia.valor.min}")
	@Column(nullable = false)
	private Double valor;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus status;
	
	@NotNull(message = "{statusOrigem.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus statusOrigem;
	
	@NotNull(message = "{statusDestino.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private LancamentoStatus statusDestino;

	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}
	
	public TransferenciaTipo getTipo() {
		return tipo;
	}

	public void setTipo(TransferenciaTipo tipo) {
		this.tipo = tipo;
	}
	
	public CompensacaoItem getCompensacaoItem() {
		return compensacaoItem;
	}

	public void setCompensacaoItem(CompensacaoItem compensacaoItem) {
		this.compensacaoItem = compensacaoItem;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public Cheque getCheque() {
		return cheque;
	}

	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}
	
	public Boolean getIsLimite() {
		return isLimite;
	}

	public void setIsLimite(Boolean isLimite) {
		this.isLimite = isLimite;
	}

	public Conta getDestino() {
		return destino;
	}

	public void setDestino(Conta destino) {
		this.destino = destino;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = NumericHelper.around(valor);
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public LancamentoStatus getStatus() {
		return status;
	}

	public void setStatus(LancamentoStatus status) {
		this.status = status;
	}

	public LancamentoStatus getStatusOrigem() {
		return statusOrigem;
	}

	public void setStatusOrigem(LancamentoStatus statusOrigem) {
		this.statusOrigem = statusOrigem;
	}

	public LancamentoStatus getStatusDestino() {
		return statusDestino;
	}

	public void setStatusDestino(LancamentoStatus statusDestino) {
		this.statusDestino = statusDestino;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Transferencia other = (Transferencia) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}
