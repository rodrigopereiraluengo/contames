package br.com.contames.entity;
import static ch.lambdaj.Lambda.forEach;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.Status;
import br.com.contames.util.NumericHelper;

@Entity
public class Conta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "conta_seq", sequenceName = "conta_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conta_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{cliente.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Pessoa cliente;
	
	@OneToMany(mappedBy = "conta")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@ManyToOne
	private Compensacao ultimaCompensacao;
	
	@OrderBy(value = "index")
	@OneToMany(mappedBy="conta")
	private List<TalaoCheque> talaoChequeList = new ArrayList<TalaoCheque>();
	
	@NotBlank(message = "{nome.notBlank}")
	@Size(max = 50, message = "{nome.size}")
	@Column(length = 50, nullable = false)
	private String nome;
	
	@NotNull(message = "{saldoInicial.notNull}")
	@Column(nullable = false)
	private Double saldoInicial;
	
	@NotNull(message = "{saldoAtual.notNull}")
	@Column(nullable = false)
	private Double saldoAtual;
	
	@DecimalMin(value = "0.00", message = "{saldoLimit.min}")
	private Double saldoLimite;
	
	private Double saldoLimiteDisp;
	
	@ManyToOne
	private Pessoa titular;
	
	@ManyToOne
	private Pessoa banco;
	
	@Size(max = 100, message = "{agencia.size}")
	@Column(length = 100)
	private String agencia;
	
	@Size(max = 15, message = "{agenciaNum.size}")
	@Column(length = 15)
	private String agenciaNum;
	
	@Size(max = 25, message = "{contaNum.size}")
	@Column(length = 25)
	private String contaNum;
	
	private Boolean isPrincipal;
	
	private Boolean isPoupanca;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	private Integer quantTalaoCheque;
	
	private Integer quantTalaoChequeFolhas;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getCliente() {
		return cliente;
	}

	public void setCliente(Pessoa cliente) {
		this.cliente = cliente;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public Compensacao getUltimaCompensacao() {
		return ultimaCompensacao;
	}

	public void setUltimaCompensacao(Compensacao ultimaCompensacao) {
		this.ultimaCompensacao = ultimaCompensacao;
	}

	public List<TalaoCheque> getTalaoChequeList() {
		return talaoChequeList;
	}

	public void setTalaoChequeList(List<TalaoCheque> talaoChequeList) {
		this.talaoChequeList = talaoChequeList;
		if(this.talaoChequeList != null) {
			forEach(this.talaoChequeList).setConta(this);
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(Double saldoInicial) {
		this.saldoInicial = NumericHelper.around(saldoInicial);
	}

	public Double getSaldoAtual() {
		return saldoAtual;
	}

	public void setSaldoAtual(Double saldoAtual) {
		this.saldoAtual = NumericHelper.around(saldoAtual);
	}
	
	public Double getSaldoLimite() {
		return saldoLimite;
	}

	public void setSaldoLimite(Double saldoLimite) {
		this.saldoLimite = NumericHelper.around(saldoLimite);
	}
	
	public Double getSaldoLimiteDisp() {
		return saldoLimiteDisp;
	}

	public void setSaldoLimiteDisp(Double saldoLimiteDisp) {
		this.saldoLimiteDisp = NumericHelper.around(saldoLimiteDisp);
	}

	public Pessoa getTitular() {
		return titular;
	}

	public void setTitular(Pessoa titular) {
		this.titular = titular;
	}

	public Pessoa getBanco() {
		return banco;
	}

	public void setBanco(Pessoa banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getAgenciaNum() {
		return agenciaNum;
	}

	public void setAgenciaNum(String agenciaNum) {
		this.agenciaNum = agenciaNum;
	}

	public String getContaNum() {
		return contaNum;
	}

	public void setContaNum(String contaNum) {
		this.contaNum = contaNum;
	}
	
	public Boolean getIsPrincipal() {
		return isPrincipal;
	}

	public void setIsPrincipal(Boolean isPrincipal) {
		this.isPrincipal = isPrincipal;
	}

	public Boolean getIsPoupanca() {
		return isPoupanca;
	}

	public void setIsPoupanca(Boolean isPoupanca) {
		this.isPoupanca = isPoupanca;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Integer getQuantTalaoCheque() {
		return quantTalaoCheque;
	}

	public void setQuantTalaoCheque(Integer quantTalaoCheque) {
		this.quantTalaoCheque = quantTalaoCheque;
	}

	public Integer getQuantTalaoChequeFolhas() {
		return quantTalaoChequeFolhas;
	}

	public void setQuantTalaoChequeFolhas(Integer quantTalaoChequeFolhas) {
		this.quantTalaoChequeFolhas = quantTalaoChequeFolhas;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Conta other = (Conta) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }

	
	
	
	public Double getSaldoUtilizavel() {
		
		Double saldoUtilizavel = saldoAtual;
		if(saldoLimiteDisp != null) {
			saldoUtilizavel += saldoLimiteDisp;
		}
		
		return saldoUtilizavel;
	}
	
}
