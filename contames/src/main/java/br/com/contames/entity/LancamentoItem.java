package br.com.contames.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.contames.util.NumericHelper;
import br.com.contames.util.StringHelper;

@Entity
public class LancamentoItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "lancamentoitem_seq", sequenceName = "lancamentoitem_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lancamentoitem_seq")
    private Long id;
	
	@ManyToOne
	@NotNull(message = "{lancamento.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Lancamento lancamento;
	
	@OneToMany(mappedBy = "lancamentoItem")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@ManyToOne
	@NotNull(message = "{item.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Item item;
	
	private Boolean isLancAuto;
	
	@NotNull(message = "{seq.notNull}")
	@Min(value = 0, message = "{seq.min}")
	@Column(nullable = false)
	private Integer seq;
	
	@Size(max = 100, message = "{obs.size}")
	@Column(length = 100)
	private String obs;
	
	@NotNull(message = "{quant.notNull}")
	@DecimalMin(value = "0.001", message = "{quant.min}")
	@Column(nullable = false)
	private Double quant;
	
	@NotNull(message = "{valorUnit.notNull}")
	@Column(nullable = false)
	private Double valorUnit;
	
	@NotNull(message = "{valor.notNull}")
	@Column(nullable = false)
	private Double valor;
	
	private Double desconto;
	
	@NotNull(message = "{total.notNull}")
	@Column(nullable = false)
	private Double total;
	
	@NotNull(message = "{perc.notNull}")
	@Column(nullable = false)
	private Double perc;
	
	@Transient
	private String viewid;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	public Boolean getIsLancAuto() {
		return isLancAuto;
	}

	public void setIsLancAuto(Boolean isLancAuto) {
		this.isLancAuto = isLancAuto;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Double getQuant() {
		return quant;
	}

	public void setQuant(Double quant) {
		this.quant = NumericHelper.around(quant, 3);
	}

	public Double getValorUnit() {
		return valorUnit;
	}

	public void setValorUnit(Double valorUnit) {
		this.valorUnit = NumericHelper.around(valorUnit);
	}
	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = NumericHelper.around(valor);
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = NumericHelper.around(desconto);
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = NumericHelper.around(total);
	}
	
	public Double getPerc() {
		return perc;
	}

	public void setPerc(Double perc) {
		this.perc = perc;
	}
	
	public String getViewid() {
		if(viewid == null) {
			viewid = StringHelper.createViewId();
		}
		return viewid;
	}

	public void setViewid(String viewid) {
		this.viewid = viewid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final LancamentoItem other = (LancamentoItem) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
}
