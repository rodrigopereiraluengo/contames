package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDateTime;

import urn.ebay.apis.eBLBaseComponents.LandingPageType;
import br.com.contames.enumeration.AssinaturaPagamentoStatus;

@Entity
public class AssinaturaPagamento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "assinaturapagamento_seq", sequenceName = "assinaturapagamento_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "assinaturapagamento_seq")
    private Long id;
	
	@Size(max = 20)
	@Column(length = 20)
	private String token;
	
	@Column(length = 19)
	private String txnId;
	
	@Column(length = 14)
	private String profileId;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 7)
	private LandingPageType landingPageType;
	
	private Boolean isProcessado;
	
	@ManyToOne
	@NotNull(message = "{assinatura.notNull}")
	@JoinColumn(updatable = false, nullable = false)
	private Assinatura assinatura;
	
	@ManyToOne
	private Plano plano;
	
	@NotNull(message = "{assinaturaPagamento.valor.notNull}")
	private Double valor;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private AssinaturaPagamentoStatus status;
	
	private Integer tried;
	
	private LocalDateTime pagamento;
	
	private LocalDateTime proxPagamento;
	
	private Boolean isParcial;
	
	private Double valorDevolvido;
		
	@NotNull(message = "{criacao.notNull}")
	@Column(insertable = true, updatable = false, nullable = false)
	private LocalDateTime criacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public LandingPageType getLandingPageType() {
		return landingPageType;
	}

	public void setLandingPageType(LandingPageType landingPageType) {
		this.landingPageType = landingPageType;
	}

	public Boolean getIsProcessado() {
		return isProcessado;
	}

	public void setIsProcessado(Boolean isProcessado) {
		this.isProcessado = isProcessado;
	}

	public Assinatura getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(Assinatura assinatura) {
		this.assinatura = assinatura;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public AssinaturaPagamentoStatus getStatus() {
		return status;
	}

	public void setStatus(AssinaturaPagamentoStatus status) {
		this.status = status;
	}

	public Integer getTried() {
		return tried;
	}

	public void setTried(Integer tried) {
		this.tried = tried;
	}

	public LocalDateTime getPagamento() {
		return pagamento;
	}

	public void setPagamento(LocalDateTime pagamento) {
		this.pagamento = pagamento;
	}

	public LocalDateTime getProxPagamento() {
		return proxPagamento;
	}

	public void setProxPagamento(LocalDateTime proxPagamento) {
		this.proxPagamento = proxPagamento;
	}

	public Boolean getIsParcial() {
		return isParcial;
	}

	public void setIsParcial(Boolean isParcial) {
		this.isParcial = isParcial;
	}

	public Double getValorDevolvido() {
		return valorDevolvido;
	}

	public void setValorDevolvido(Double valorDevolvido) {
		this.valorDevolvido = valorDevolvido;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AssinaturaPagamento other = (AssinaturaPagamento) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }

}
