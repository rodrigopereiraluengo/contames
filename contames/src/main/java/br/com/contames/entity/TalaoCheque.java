package br.com.contames.entity;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.equalTo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.Status;
import br.com.contames.util.StringHelper;

@Entity
public class TalaoCheque implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "talaocheque_seq", sequenceName = "talaocheque_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "talaocheque_seq")
    private Long id;
	
	@NotNull(message = "{conta.notNull}")
	@ManyToOne
	@JoinColumn(nullable = false, updatable = false)
	private Conta conta;
	
	@OneToMany(mappedBy = "talaoCheque", fetch = FetchType.EAGER)
	private List<Cheque> chequeList;
	
	@OneToMany(mappedBy = "talaoCheque")
	private List<Arquivo> arquivoList = new ArrayList<Arquivo>();
	
	@Column(name = "_data")
	private LocalDate data;
	
	@NotNull(message = "{seqInicio.notNull}")
	@Min(value = 1, message = "{seqInicio.min}")
	@Column(nullable = false)
	private Integer seqInicio;
	
	@NotNull(message = "{folhas.notNull}")
	@Min(value = 1, message = "{folhas.min}")
	@Column(nullable = false)
	private Integer folhas;
	
	@NotNull(message = "{seqFim.notNull}")
	@Min(value = 1, message = "{seqFim.min}")
	@Column(nullable = false)
	private Integer seqFim;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	private Status status;
	
	@Size(max = 400, message = "{obs.size}")
	@Column(length = 400)
	private String obs;
	
	@NotNull(message = "{index.notNull}")
	@Column(name = "_index", nullable = false)
	private Integer index;
	
	@Column(updatable = false)
	private LocalDateTime criacao;
	
	@ManyToOne
	@JoinColumn(insertable = true, updatable = false)
	private Pessoa usuCriacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	@ManyToOne
	@JoinColumn(insertable = false)
	private Pessoa usuAlteracao;
	
	@Transient
	private String viewid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public List<Cheque> getChequeList() {
		return chequeList;
	}

	public void setChequeList(List<Cheque> chequeList) {
		this.chequeList = chequeList;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}
	
	public List<Arquivo> getArquivoList() {
		return arquivoList;
	}

	public void setArquivoList(List<Arquivo> arquivoList) {
		this.arquivoList = arquivoList;
	}

	public Integer getSeqInicio() {
		return seqInicio;
	}

	public void setSeqInicio(Integer seqInicio) {
		this.seqInicio = seqInicio;
	}

	public Integer getFolhas() {
		return folhas;
	}

	public void setFolhas(Integer folhas) {
		this.folhas = folhas;
	}

	public Integer getSeqFim() {
		return seqFim;
	}

	public void setSeqFim(Integer seqFim) {
		this.seqFim = seqFim;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public Pessoa getUsuCriacao() {
		return usuCriacao;
	}

	public void setUsuCriacao(Pessoa usuCriacao) {
		this.usuCriacao = usuCriacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	public Pessoa getUsuAlteracao() {
		return usuAlteracao;
	}

	public void setUsuAlteracao(Pessoa usuAlteracao) {
		this.usuAlteracao = usuAlteracao;
	}
	
	public String getViewid() {
		if(viewid == null) {
			viewid = StringHelper.createViewId();
		}
		return viewid;
	}

	public void setViewid(String viewid) {
		this.viewid = viewid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final TalaoCheque other = (TalaoCheque) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
	
	
	public Cheque getChequeByNumero(Integer numero) {
		if(chequeList == null || chequeList.isEmpty()) {
			return null;
		} else {
			Cheque cheque = selectFirst(chequeList, having(on(Cheque.class).getNumero(), equalTo(numero)));
			return cheque;
		}
	}
	
}