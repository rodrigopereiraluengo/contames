package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Entity implementation class for Entity: LancamentoFatura
 *
 */
@Entity
public class LancamentoFatura implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "lancamentofatura_seq", sequenceName = "lancamentofatura_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lancamentofatura_seq")
    private Long id;

	@ManyToOne
	@NotNull(message = "{lancamento.notNull}")
	@JoinColumn(updatable = false)
	private Lancamento lancamento;
	
	@NotNull(message = "{seq.notNull}")
	@Min(value = 0, message = "{seq.min}")
	@Column(nullable = false)
	private Integer seq;
	
	@OneToOne
	@JoinColumn(updatable = false)
	private LancamentoParcela lancamentoParcela;
	
	@OneToOne
	@JoinColumn(updatable = false)
	private Transferencia transferencia;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public LancamentoParcela getLancamentoParcela() {
		return lancamentoParcela;
	}

	public void setLancamentoParcela(LancamentoParcela lancamentoParcela) {
		this.lancamentoParcela = lancamentoParcela;
	}
	
	public Transferencia getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(Transferencia transferencia) {
		this.transferencia = transferencia;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final LancamentoFatura other = (LancamentoFatura) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
   
}
