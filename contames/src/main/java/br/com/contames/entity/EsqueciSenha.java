package br.com.contames.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.Status;

@Entity
public class EsqueciSenha implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	
	@NotNull(message = "{usuario.notNull}")
	@ManyToOne(fetch=FetchType.EAGER)
	private Pessoa usuario;
	
	@NotNull(message = "{status.notNull}")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@NotNull(message = "{criacao.notNull}")
	@Column(insertable = true, updatable = false, nullable = false)
	private LocalDateTime criacao;
	
	@Column(insertable = false, updatable = true)
	private LocalDateTime alteracao;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Pessoa getUsuario() {
		return usuario;
	}

	public void setUsuario(Pessoa usuario) {
		this.usuario = usuario;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public LocalDateTime getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}

	public LocalDateTime getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(LocalDateTime alteracao) {
		this.alteracao = alteracao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final EsqueciSenha other = (EsqueciSenha) obj;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}
	
	@Override
    public String toString() {
        return String.valueOf(id);
    }
	
}
