package br.com.contames.jsp;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.contames.enumeration.SubtotalTipo;
import br.com.contames.util.OrderBy;

public class Functions {

	
	public static String outHtml(String val) {
		if(val == null) {
			return null;
		}
		return StringEscapeUtils.escapeHtml4(val).replaceAll("(\r\n|\n)", "<br/>");
	}
	
	
	public static String escapeJS(String val) {
		if(val == null) {
			return null;
		}
		return StringEscapeUtils.escapeEcmaScript(val);
	}
	
	
	public static String uncode(String val) {
		try {
			return URLDecoder.decode(val, "utf-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	
	
	public static boolean orderByContains(String column, List<OrderBy> orderByList) {
		
		boolean isContains = false;

		for(OrderBy orderBy : orderByList) {
			if(orderBy.getColumn().equals(column)) {
				isContains = true;
				break;
			}
		}
		
		return isContains;
	}
	
	public static boolean contains(Object arg0, List<Object> list) {
		
		if(arg0 == null || list == null) {
			return false;
		}
		
		return list.contains(arg0);
	}
	
	
	public static Map<String, Double> subTotal(Map<String, Double> mapSubtotal, SubtotalTipo subtotal, java.sql.Date data, Double value) {

		if(mapSubtotal == null) {
			mapSubtotal = new HashMap<String, Double>();
		}
		
		LocalDate localDate = new LocalDate(data);
		
		String key = null;
				
		
		switch(subtotal) {
		case DIA:
			key = localDate.toString("yy_MM_dd");
			break;
		case MES:
			key = localDate.toString("yy_MM");
			break;
		case ANO:
			key = localDate.toString("yy");
			break;
		}
		
		Double total = mapSubtotal.containsKey(key) ? mapSubtotal.get(key) : 0;
		
		total += value;
		
		mapSubtotal.put(key, total);
		
		return mapSubtotal;
	}
	
	
	public static String formatSqlDate(java.sql.Date data, String format) {
		LocalDate localDate = new LocalDate(data);
		return localDate.toString(format);
	}
	
	
	public static long sqlTimestampToMillis(java.sql.Timestamp timestamp) {
		return new LocalDateTime(timestamp).toDateTime().getMillis();
	}
	
	public static boolean isImage(String name) {
		return FilenameUtils.isExtension(name, new String[]{"jpg", "jpeg", "png", "gif", "bmp", "svg"});
	}
			
}
