package br.com.contames.jsp;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class CustomTag extends TagSupport {
		
	public int doStartTag() {
		try {
			JspWriter out = pageContext.getOut();
			out.print("Olá mundo");
		} catch(IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
	
}
