package br.com.contames.enumeration;

public enum DescricaoTipo {
	PESSOA_CATEGORIA,
	UNIDADE,
	RATEIO,
	MOTIVO_DIFERENCA,
	CARTAO_BANDEIRA,
	MOTIVO_CAN_CHQ
}