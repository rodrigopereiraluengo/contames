package br.com.contames.enumeration;

public enum AgendaTipo {
	IMPORTANT, WARNING, INFO, INVERSE, SUCCESS, SPECIAL
}
