package br.com.contames.enumeration;

public enum PeriodoData {
	HOJE, 
	ONTEM, 
	AMANHA, 
	ONTEM_AMANHA, 
	SEMANA_ATUAL, 
	SEMANA_ANT, 
	SEMANA_PROX, 
	SEMANA_ANT_PROX,
	MES_ATUAL, 
	MES_ANT, 
	MES_PROX,
	MES_ANT_PROX,
	BIMESTRE_ATUAL,
	BIMESTRE_ANT,
	BIMESTRE_PROX,
	BIMESTRE_ANT_PROX,
	TRIMESTRE_ATUAL,
	TRIMESTRE_ANT,
	TRIMESTRE_PROX,
	TRIMESTRE_ANT_PROX,
	SEMESTRE_ATUAL,
	SEMESTRE_ANT,
	SEMESTRE_PROX,
	SEMESTRE_ANT_PROX,
	ANO_ATUAL,
	ANO_ANT,
	ANO_PROX,
	ANO_ANT_PROX,
	OUTRO
}
