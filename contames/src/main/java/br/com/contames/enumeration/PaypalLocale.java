package br.com.contames.enumeration;

public enum PaypalLocale { 
	 AU, // – Australia
	 AT, // – Austria
	 BE, // – Belgium
	 BR, // – Brazil
	 CA, // – Canada
	 CH, // – Switzerland
	 CN, // – China
	 DE, // – Germany
	 ES, // – Spain
	 GB, // – United Kingdom
	 FR, // – France
	 IT, // – Italy
	 NL, // – Netherlands
	 PL, // – Poland
	 PT, // – Portugal
	 RU, // – Russia
	 US, // – United States
	 // The following 5-character codes are also supported for languages in specific countries:

	     da_DK, // – Danish (for Denmark only)
	     he_IL, // – Hebrew (all)
	     id_ID, // – Indonesian (for Indonesia only)
	     ja_JP, // – Japanese (for Japan only)
	     no_NO, // – Norwegian (for Norway only)
	     pt_BR, // – Brazilian Portuguese (for Portugal and Brazil only)
	     ru_RU, // – Russian (for Lithuania, Latvia, and Ukraine only)
	     sv_SE, // – Swedish (for Sweden only)
	     th_TH, // – Thai (for Thailand only)
	     tr_TR, // – Turkish (for Turkey only)
	     zh_CN, // – Simplified Chinese (for China only)
	     zh_HK, // – Traditional Chinese (for Hong Kong only)
	     zh_TW, // – Traditional Chinese 
}
