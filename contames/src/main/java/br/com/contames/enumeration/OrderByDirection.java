package br.com.contames.enumeration;

public enum OrderByDirection {
	ASC, DESC
}
