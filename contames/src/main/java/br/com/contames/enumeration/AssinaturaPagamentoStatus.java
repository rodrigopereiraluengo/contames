package br.com.contames.enumeration;

public enum AssinaturaPagamentoStatus {
	STD, 
	CON, 
	CAN, 
	SUS, 
	EXP, 
	INA, 
	REF
}
