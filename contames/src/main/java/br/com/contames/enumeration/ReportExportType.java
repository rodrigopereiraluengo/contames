package br.com.contames.enumeration;

public enum ReportExportType {
	XLSX, PDF, PNG, DOC
}
