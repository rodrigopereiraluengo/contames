$('#dialogArquivoConsultar').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
	$(this).trigger('close').remove();
});

$('#frmArquivoConsultar').formSearch({
	deleteMessageConfirm: i18n.get('formSearch.delete.arquivo.confirm'),
	deleteMessageConfirmP: i18n.get('formSearch.delete.arquivo.confirm.p'),
	deleteMessageSuccess: i18n.get('formSearch.delete.arquivo.success'),
	deleteMessageSuccessP: i18n.get('formSearch.delete.arquivo.success.p')
});

var fnDialogAnexoArquivoClose = function(e, arquivo) {
	msgSuccess(i18n.get('arquivo.enviado.success', arquivo.nome));
	$('#frmArquivoConsultar').submit();
};

$(document).off('close', '#dialogAnexoArquivo', fnDialogAnexoArquivoClose);
$(document).on('close', '#dialogAnexoArquivo', fnDialogAnexoArquivoClose);


function arquivoConsultarExcluirSuccess(ids) {
	$('#frmArquivoConsultar').submit();
	var viewid = $('#frmArquivoConsultar').data('viewid');
	$.get(contextPath + '/arquivo/quantidade', {viewid: viewid}, function(quantidade) {
		$('span.quant', '.bt-dialogArquivo[data-viewid="' + viewid + '"]').text(quantidade);
	});
}