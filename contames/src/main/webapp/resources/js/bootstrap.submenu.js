/**
  * NAME: Bootstrap 3 Triple Nested Sub-Menus
  * This script will active Triple level multi drop-down menus in Bootstrap 3.*
  */
$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
    // Avoid following the href location when clicking
    event.preventDefault(); 
    // Avoid having the menu to close when clicking
    event.stopPropagation(); 
    // Re-add .open to parent sub-menu item
    $(this).parent().nextAll().removeClass('open').find('.open').removeClass('open');
    $(this).parent().prevAll().removeClass('open').find('.open').removeClass('open');
    $(this).parent().addClass('open');
    $(this).parent().find("ul").parent().find("li.dropdown").addClass('open');
});