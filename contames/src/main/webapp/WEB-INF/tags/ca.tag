<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="data" required="true" type="org.joda.time.LocalDateTime" %>
<%@ attribute name="create" required="false" %>
<%@ attribute name="pessoa" required="false" type="br.com.contames.entity.Pessoa" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<fmt:message key="patternDateTimeSec" var="patternDateTimeSec"/>
<c:if test="${data ne null}">
	<fmt:message key="${create}"/>&nbsp;
	<strong><c:if test="${pessoa ne null}"><c:out value="${pessoa.displayNome}"/> - </c:if> 
	<joda:format pattern="${patternDateTimeSec}" value="${data}" /></strong>
</c:if>