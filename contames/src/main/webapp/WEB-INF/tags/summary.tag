<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="count" required="true" type="java.lang.Long" %>
<%@ attribute name="firstResult" required="true" type="java.lang.Integer" %>
<%@ attribute name="page" required="true" type="java.lang.Integer" %>
<%@ attribute name="pageSize" required="true" type="java.lang.Integer" %>
<%@ attribute name="paginationSize" required="true" type="java.lang.Integer" %>
<%@ attribute name="isLazy" required="false" type="java.lang.Boolean" %>

<%-- Resumo --%>
<div class="summary">
	<div class="row">
		<div class="col-xs-4 text-center">
			<c:if test="${count eq 1}"><fmt:message key="registro"/>:</c:if>
			<c:if test="${count gt 1}"><fmt:message key="registros"/>:</c:if>
			<strong><fmt:formatNumber type="number" value="${count}" maxFractionDigits="0" minFractionDigits="0" /></strong>
		</div>
		<div class="col-xs-4 text-center">
			<fmt:message key="exibindo"/>:
			<strong><fmt:formatNumber type="number" value="${isLazy eq null or !isLazy ? firstResult + 1 : 1}" maxFractionDigits="0" minFractionDigits="0" /></strong> -
			<c:if test="${(page * pageSize) gt count}"> 
				<strong><fmt:formatNumber type="number" value="${count}" maxFractionDigits="0" minFractionDigits="0" /></strong>
			</c:if>
			<c:if test="${!((page * pageSize) gt count)}"> 
				<strong><fmt:formatNumber type="number" value="${page * pageSize}" maxFractionDigits="0" minFractionDigits="0" /></strong>
			</c:if>
		</div>
		<div class="col-xs-4 text-center">
			<fmt:message key="pagina"/>:
			<strong><fmt:formatNumber type="number" value="${page}" maxFractionDigits="0" minFractionDigits="0" /></strong> -
			<strong><fmt:formatNumber type="number" value="${paginationSize}" maxFractionDigits="0" minFractionDigits="0" /></strong>
		</div>
	</div>
</div>