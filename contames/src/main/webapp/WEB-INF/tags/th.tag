<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/functions.tld" prefix="f" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderByList" required="true" type="java.util.ArrayList" %>
<%@ attribute name="column" required="true" %>
<%@ attribute name="path" required="true" %>
<%@ attribute name="page" required="true" type="java.lang.Integer" %>
<%@ attribute name="index" required="true" type="java.lang.Integer" %>
<%@ attribute name="name" required="false" %>
<%@ attribute name="order" required="false" %>
<%@ attribute name="rowspan" required="false" %>
<%@ attribute name="colspan" required="false" %>

<c:set var="contains" value="false"/>

<c:if test="${name eq null}"><c:set var="name" value="${column}" /></c:if>
<c:if test="${order eq null}"><c:set var="order" value="${column}" /></c:if>

<c:url var="_path" value="${pageContext.request.contextPath}/${path}">
	<c:forEach var="orderBy" items="${orderByList}">
		<c:if test="${orderBy.column eq order}">
			<c:set var="contains" value="true"/>
			<c:set var="direction" value="${orderBy.direction}"/>
		</c:if>
		
		<c:if test="${orderBy.column ne order}">
			<c:param name="orderByList[].column" value="${orderBy.column}" />
			<c:param name="orderByList[].direction" value="${orderBy.direction}" />
			<c:param name="orderByList[].index" value="${orderBy.index}" />
		</c:if>
		
		<c:if test="${orderBy.column eq order and (orderBy.direction ne 'DESC' or fn:length(orderByList) eq 1) }">
			<c:param name="orderByList[].column" value="${orderBy.column}" />
			<c:param name="orderByList[].direction" value="${orderBy.direction eq 'ASC' ? 'DESC' : 'ASC'}" />
			<c:param name="orderByList[].index" value="${orderBy.index}" />
		</c:if>
		
	</c:forEach>
	<c:if test="${!contains}">
		<c:param name="orderByList[].column" value="${order}" />
		<c:param name="orderByList[].direction" value="ASC" />
		<c:param name="orderByList[].index" value="${index}" />
	</c:if>
	<c:param name="page" value="${page}" />
</c:url>
<th<c:if test="${rowspan ne null}"> rowspan="${rowspan}"</c:if><c:if test="${colspan ne null}"> colspan="${colspan}"</c:if>>
	<a class="${contains ? 'sort': ''}" href="${f:uncode(_path)}">
		<span><fmt:message key="${name}" /></span>
		<c:if test="${!contains}">
			<i class="fa fa-sort"></i>
		</c:if>
		<c:if test="${contains and direction eq 'ASC'}">
			<i class="fa fa-sort-asc"></i>
		</c:if>
		<c:if test="${contains and direction eq 'DESC'}">
			<i class="fa fa-sort-desc"></i>
		</c:if>
	</a>
</th>