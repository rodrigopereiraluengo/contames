<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/functions.tld" prefix="f" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="page" required="true" type="java.lang.Integer" %>
<%@ attribute name="pageSize" required="true" type="java.lang.Integer" %>
<%@ attribute name="paginationSize" required="true" type="java.lang.Integer" %>
<%@ attribute name="orderByList" required="true" type="java.util.ArrayList" %>
<%@ attribute name="path" required="true" %>
	
<c:url var="_path" value="${pageContext.request.contextPath}/${path}">
	<c:forEach var="orderBy" items="${orderByList}">
		<c:param name="orderByList[].column" value="${orderBy.column}" />
		<c:param name="orderByList[].direction" value="${orderBy.direction}" />
		<c:param name="orderByList[].index" value="${orderBy.index}" />
	</c:forEach>
</c:url>
	
<%-- Paginacao --%>
<nav class="cm-pagination">
  	<ul class="pagination">
  		
  		
  		<%-- Pagina Anterior Desabilitada --%>
  		<c:if test="${page eq 1}">
  			<c:if test="${paginationSize > 5}">
	  			<li class="disabled">
	  				<a href="#" aria-label="<fmt:message key="firstPage"/>"><span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span></a>
	  			</li>
  			</c:if>
    		<li class="disabled">
    			<a href="#" aria-label="<fmt:message key="previousPage"/>"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></a>
    		</li>
    	</c:if>
    	
    	
    	<%-- Pagina Anterior --%>
    	<c:if test="${page ne 1}">
    		
    		<c:if test="${paginationSize > 5}">
	    		<c:url var="__path" value="${_path}">
	    			<c:param name="page" value="1" />
	    		</c:url>
	    		<li>
	  				<a href="${f:uncode(__path)}" aria-label="<fmt:message key="firstPage"/>"><span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span></a>
	  			</li>
  			</c:if>
    		
    		<c:url var="__path" value="${_path}">
    			<c:param name="page" value="${page - 1}" />
    		</c:url>
		    <li>
		    	<a href="${f:uncode(__path)}" aria-label="<fmt:message key="previousPage"/>"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></a>
		    </li>
	    
	    </c:if>
	    
	    <c:set var="pageBegin" value="1" />
	    <c:set var="pageEnd" value="${paginationSize}" />
	    
	    <c:if test="${paginationSize > 5}">
	    	<c:set var="_pageBegin" value="${(page - 2) < 1 ? 1 : (page - 2)}" />
	    	<c:set var="pageEnd" value="${(_pageBegin + 4) > paginationSize ? paginationSize : _pageBegin + 4}" />
	    	<c:set var="pageBegin" value="${(_pageBegin + 4) > paginationSize ? paginationSize - 4 : _pageBegin}" />
	    </c:if>
	    
	    <%-- Paginas --%>
	    <c:forEach var="pageNum" begin="${pageBegin}" end="${pageEnd}">
	    	<c:if test="${page ne pageNum}">
	    		<c:url var="__path" value="${_path}">
    				<c:param name="page" value="${pageNum}" />
    			</c:url>
		    	<li><a href="${f:uncode(__path)}">${pageNum}</a></li>
    		</c:if>
    		<c:if test="${page eq pageNum}">
    			<li class="active"><span>${pageNum} <span class="sr-only"><fmt:message key="currentPage"/></span></span></li>
    		</c:if>
	    </c:forEach>
	    
	    
	    
	    <%-- Proxima Pagina --%>
	    <c:if test="${page ne paginationSize}">
	    	
	    	<c:url var="__path" value="${_path}">
   				<c:param name="page" value="${page + 1}" />
   			</c:url>
		    <li>
	      		<a href="${f:uncode(__path)}" aria-label="<fmt:message key="nextPage"/>"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
	    	</li>
	    	
	    	<c:if test="${paginationSize > 5}">
		    	<c:url var="__path" value="${_path}">
	   				<c:param name="page" value="${paginationSize}" />
	   			</c:url>
		    	<li>
		      		<a href="${f:uncode(__path)}" aria-label="<fmt:message key="lastPage"/>"><span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span></a>
		    	</li>
    		</c:if>
    	</c:if>
    	
    	
    	<%-- Proxima Pagina Desabilitada --%>
    	<c:if test="${page eq paginationSize}">
    		
    		<li class="disabled">
	      		<a href="#" aria-label="<fmt:message key="nextPage"/>"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
	    	</li>
	    	
	    	<c:if test="${paginationSize > 5}">
		    	<li class="disabled">
		      		<a href="#" aria-label="<fmt:message key="lastPage"/>"><span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span></a>
		    	</li>
	    	</c:if>
    	</c:if>
  	
  	</ul>
</nav>