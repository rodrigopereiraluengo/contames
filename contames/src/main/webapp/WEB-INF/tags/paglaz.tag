<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/functions.tld" prefix="f" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="page" required="true" type="java.lang.Integer" %>
<%@ attribute name="pageSize" required="true" type="java.lang.Integer" %>
<%@ attribute name="paginationSize" required="true" type="java.lang.Integer" %>
<%@ attribute name="orderByList" required="true" type="java.util.ArrayList" %>
<%@ attribute name="path" required="true" %>
<c:if test="${paginationSize gt 1 and page lt paginationSize}">
	<c:url var="_path" value="${pageContext.request.contextPath}/${path}">
		<c:forEach var="orderBy" items="${orderByList}">
			<c:param name="orderByList[].column" value="${orderBy.column}" />
			<c:param name="orderByList[].direction" value="${orderBy.direction}" />
			<c:param name="orderByList[].index" value="${orderBy.index}" />
		</c:forEach>
	</c:url>
	<c:url var="__path" value="${_path}">
		<c:param name="page" value="${page + 1}" />
	</c:url>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 pagination text-center">
    			<a href="${f:uncode(__path)}" class="lazipag"><img src="${pageContext.request.contextPath}/resources/css/throbber.gif" /></a>
    		</div>
		</div>
	</div>
	
</c:if>