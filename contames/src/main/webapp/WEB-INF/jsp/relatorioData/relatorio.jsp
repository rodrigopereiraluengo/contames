<fmt:message key="patternDate" var="patternDate" />
<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">
	
	<tiles:putAttribute name="head">
		<!--[if lte IE 8]>
        	<script src="${pageContext.request.contextPath}/resources/js/excanvas.js?v${applicationVersion}"></script>
    	<![endif]-->
    	<script src="${pageContext.request.contextPath}/resources/js/Chart.min.js?v${applicationVersion}"></script>
	</tiles:putAttribute>

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="relatorioPor.data"/></h1>
						<p class="description"><fmt:message key="relatorioPor.data.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="resumo" />
							</li>
				  			<li><fmt:message key="relatorios" /></li>
				  			<li><fmt:message key="data" /></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<form 
			id="frmRelatorioData" 
			action="${pageContext.request.contextPath}/relatorios/data" 
			class="data-table">
		
			<div class="panel panel-default">
			
				<div class="panel-heading cm-panel-heading-toolbar">
				
					<div class="container-fluid">
				
						<div class="row form-horizontal">
							
							<!-- Datas -->
							<div class="col-xs-12 col-sm-3">
								
								<!-- Periodo -->
								<div class="form-group">
									<label for="relatorioDataPeriodoData" class="col-sm-4 control-label"><fmt:message key="periodo"/></label>
								    <div class="col-sm-8">
							      		<select id="relatorioDataPeriodoData" name="periodoData" class="form-control">
							      			<optgroup label="<fmt:message key="dia"/>">
							      				<option value="HOJE"><fmt:message key="PeriodoData.HOJE"/></option>
							      				<option value="ONTEM_AMANHA"><fmt:message key="PeriodoData.ONTEM_AMANHA"/></option>
							      				<option value="ONTEM"><fmt:message key="PeriodoData.ONTEM"/></option>
							      				<option value="AMANHA"><fmt:message key="PeriodoData.AMANHA"/></option>
							      			</optgroup>
							      			<optgroup label="<fmt:message key="semana"/>">
							      				<option value="SEMANA_ATUAL"><fmt:message key="PeriodoData.SEMANA_ATUAL"/></option>
							      				<option value="SEMANA_ANT_PROX"><fmt:message key="PeriodoData.SEMANA_ANT_PROX"/></option>
							      				<option value="SEMANA_ANT"><fmt:message key="PeriodoData.SEMANA_ANT"/></option>
							      				<option value="SEMANA_PROX"><fmt:message key="PeriodoData.SEMANA_PROX"/></option>
							      			</optgroup>
							      			<optgroup label="<fmt:message key="mes"/>">
							      				<option value="MES_ATUAL"><fmt:message key="PeriodoData.MES_ATUAL"/></option>
							      				<option value="MES_ANT_PROX"><fmt:message key="PeriodoData.MES_ANT_PROX"/></option>
							      				<option value="MES_ANT"><fmt:message key="PeriodoData.MES_ANT"/></option>
							      				<option value="MES_PROX"><fmt:message key="PeriodoData.MES_PROX"/></option>
							      			</optgroup>
							      			<optgroup label="<fmt:message key="bimestre"/>">
							      				<option value="BIMESTRE_ATUAL"><fmt:message key="PeriodoData.BIMESTRE_ATUAL"/></option>
							      				<option value="BIMESTRE_ANT_PROX"><fmt:message key="PeriodoData.BIMESTRE_ANT_PROX"/></option>
							      				<option value="BIMESTRE_ANT"><fmt:message key="PeriodoData.BIMESTRE_ANT"/></option>
							      				<option value="BIMESTRE_PROX"><fmt:message key="PeriodoData.BIMESTRE_PROX"/></option>
							      			</optgroup>
							      			<optgroup label="<fmt:message key="trimestre"/>">
							      				<option value="TRIMESTRE_ATUAL"><fmt:message key="PeriodoData.TRIMESTRE_ATUAL"/></option>
							      				<option value="TRIMESTRE_ANT_PROX"><fmt:message key="PeriodoData.TRIMESTRE_ANT_PROX"/></option>
							      				<option value="TRIMESTRE_ANT"><fmt:message key="PeriodoData.TRIMESTRE_ANT"/></option>
							      				<option value="TRIMESTRE_PROX"><fmt:message key="PeriodoData.TRIMESTRE_PROX"/></option>
							      			</optgroup>
							      			<optgroup label="<fmt:message key="semestre"/>">
							      				<option value="SEMESTRE_ATUAL"><fmt:message key="PeriodoData.SEMESTRE_ATUAL"/></option>
							      				<option value="SEMESTRE_ANT_PROX"><fmt:message key="PeriodoData.SEMESTRE_ANT_PROX"/></option>
							      				<option value="SEMESTRE_ANT"><fmt:message key="PeriodoData.SEMESTRE_ANT"/></option>
							      				<option value="SEMESTRE_PROX"><fmt:message key="PeriodoData.SEMESTRE_PROX"/></option>
							      			</optgroup>
							      			<optgroup label="<fmt:message key="ano"/>">
							      				<option value="ANO_ATUAL" selected="selected"><fmt:message key="PeriodoData.ANO_ATUAL"/></option>
							      				<option value="ANO_ANT_PROX"><fmt:message key="PeriodoData.ANO_ANT_PROX"/></option>
							      				<option value="ANO_ANT"><fmt:message key="PeriodoData.ANO_ANT"/></option>
							      				<option value="ANO_PROX"><fmt:message key="PeriodoData.ANO_PROX"/></option>
							      			</optgroup>
							      			<option value="OUTRO"><fmt:message key="PeriodoData.OUTRO"/></option>
							      		</select>
								    </div>
								</div>
								
								<!-- Data de -->
								<div class="form-group">
									<label for="relatorioDataDataDe" class="col-sm-4 control-label"><fmt:message key="de"/></label>
								    <div class="col-sm-8">
								    	<div class="input-group">
								      		<input type="text" id="relatorioDataDataDe" name="dataDe" class="form-control" value="<joda:format pattern="${patternDate}" value="${dataDe}" />" readonly="readonly" />
								      		<span class="input-group-btn">
				  								<button class="btn btn-default" type="button" id="btRelatorioDataDataDe" disabled="disabled">
			       									<i class="fa fa-calendar-o"></i>
												</button>
											</span>
								      	</div>
								    </div>
								</div>
								
								<!-- Ate -->
								<div class="form-group">
									<label for="relatorioDataDataDe" class="col-sm-4 control-label"><fmt:message key="ate"/></label>
								    <div class="col-sm-8">
								    	<div class="input-group">
								      		<input type="text" id="relatorioDataDataAte" name="dataAte" class="form-control" value="<joda:format pattern="${patternDate}" value="${dataAte}" />" readonly="readonly" />
							      			<span class="input-group-btn">
				  								<button class="btn btn-default" type="button" id="btRelatorioDataDataAte" disabled="disabled">
				  									<i class="fa fa-calendar-o"></i>
			       								</button>
											</span>
							      		</div>
							      	</div>
								</div>
								
							</div>
							
							<!-- Favorecido, Descricao -->
							<div class="col-xs-12 col-sm-4">
								
								<div class="form-group">
									<label for="relatorioDataFavorecido" class="col-sm-4 control-label"><fmt:message key="pagarAReceverDe"/></label>
								    <div class="col-sm-8">
								      <input type="text" id="relatorioDataFavorecido" class="form-control" />
								    </div>
								</div>
								
								<div class="form-group">
									<label for="relatorioDataDescricao" class="col-sm-4 control-label"><fmt:message key="descricao"/></label>
								    <div class="col-sm-8">
								      <input type="text" id="relatorioDataDescricao" name="descricao" class="form-control" />
								    </div>
								</div>
								
								<div class="form-group">
									<label for="relatorioTipoDoc" class="col-sm-4 control-label"><fmt:message key="meio"/></label>
									<div class="col-sm-5">
										<select id="relatorioDataMeio" name="meioList[]" multiple="multiple" data-category="meio" class="form-control" style="display: none">
			  								<option value="CCR"><fmt:message key="MeioPagamento.CCR" /></option>
			  								<option value="CDB"><fmt:message key="MeioPagamento.CDB" /></option>
			  								<option value="INT"><fmt:message key="MeioPagamento.INT" /></option>
			  								<option value="DEA"><fmt:message key="MeioPagamento.DEA" /></option>
			  								<option value="DIN"><fmt:message key="MeioPagamento.DIN" /></option>
			  								<option value="CHQ"><fmt:message key="MeioPagamento.CHQ" /></option>
			  								<option value="TRA"><fmt:message key="MeioPagamento.TRA" /></option>
			  								<option value="DOC"><fmt:message key="MeioPagamento.DOC" /></option>
										</select>
									</div>
									<div class="col-sm-3">
										<input type="number" id="relatorioDataCheque" name="cheque" class="form-control" style="display: none" min="1" />
									</div>
								</div>
								
								<div class="form-group" id="layRelatorioCartaoCreditoConta" style="display: none">
									<label for="relatorioCartaoCreditoConta" id="lbRelatorioCartaoCreditoConta" class="col-sm-4 control-label text-nowrap"></label>
									<div class="col-sm-8">
										<select id="relatorioCartaoCreditoConta" multiple="multiple" class="form-control" style="display: none">
			  							</select>
									</div>
								</div>
							
							</div>
							
							<!-- Documento -->
							<div class="col-xs-12 col-sm-2">
							
								<div class="form-group">
									<label for="relatorioTipoDoc" class="col-sm-4 control-label"><fmt:message key="doc"/></label>
									<div class="col-sm-8">
										<select id="relatorioDataTipoDoc" name="tipoDocList[]" multiple="multiple" data-category="tipoDoc" class="form-control" style="display: none">
			  								<option value="NF"><fmt:message key="DocTipo.NF"/></option>
											<option value="CF"><fmt:message key="DocTipo.CF"/></option>
											<option value="RC"><fmt:message key="DocTipo.RC"/></option>
											<option value="FT"><fmt:message key="DocTipo.FT"/></option>
											<option value="CT"><fmt:message key="DocTipo.CT"/></option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="relatorioDataDescricao" class="col-sm-4 control-label"><fmt:message key="n_"/></label>
								    <div class="col-sm-8">
								      	<input type="text" id="relatorioDataNumeroDoc" name="numeroDoc" class="form-control" />
								    </div>
								</div>
								
								<div class="form-group">
									<label for="relatorioDataDescricao" class="col-sm-4 control-label"><fmt:message key="tipo"/></label>
								    <div class="col-sm-8">
								      	<select id="relatorioDataTipo" name="tipo" class="form-control">
								      		<option value=""><fmt:message key="todos___" /></option>
								      		<option value="DES"><fmt:message key="ItemTipo.DESs"/></option>
								      		<option value="REC"><fmt:message key="ItemTipo.RECs"/></option>
								      	</select>
								    </div>
								</div>
								
							</div>
							
							<!-- Status -->
							<div class="col-xs-12 col-sm-3">
								
								<div class="form-group">
									<label for="relatorioDataStatus" class="col-sm-4 control-label"><fmt:message key="status"/></label>
									<div class="col-sm-8">
										<select id="relatorioDataStatus" name="statusList[]" multiple="multiple" class="form-control" style="display: none">
											<option value="STD" selected="selected"><fmt:message key="LancamentoStatus.STD"/></option>
											<option value="LAN" selected="selected"><fmt:message key="LancamentoStatus.LAN"/></option>
											<option value="BXD" selected="selected"><fmt:message key="LancamentoStatus.BXD"/></option>
											<option value="COM" selected="selected"><fmt:message key="LancamentoStatus.COM"/></option>
											<option value="CAN"><fmt:message key="LancamentoStatus.CAN"/></option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="relatorioDataSubTotal" class="col-sm-4 control-label"><fmt:message key="subTotal"/></label>
									<div class="col-sm-8">
										<select id="relatorioDataSubTotal" name="subtotal" class="form-control">
											<option value=""></option>
											<option value="DIA"><fmt:message key="SubtotalTipo.DIA"/></option>
											<option value="MES" selected="selected"><fmt:message key="SubtotalTipo.MES"/></option>
											<option value="ANO"><fmt:message key="SubtotalTipo.ANO"/></option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-4">
										<input type="checkbox" id="isCCred" checked="checked" value="true" />
									</div>
									<div class="col-sm-8">
										<div class="btn-group" role="group">
										  	<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
										  	<a id="btExportXls" class="btn btn-default" title="<fmt:message key="exportarParaExcel"/>"><i class="fa fa-file-excel-o"></i></a>
										  	<a id="btExportPdf" class="btn btn-default" title="<fmt:message key="exportarParaPdf"/>"><i class="fa fa-file-pdf-o"></i></a>
										</div>
									</div>
								</div>
							
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div id="layChart" class="container-fluid">
					
					<div class="row">
						<div class="col-xs-12">
							<canvas id="chartRelatorioDataBarra" class="rel-canvas-chart"></canvas>
						</div>
					</div>
				
				</div>
				
				<div class="result search-table">
					<jsp:include page="relatorio_table.jsp" />
	   			</div>
			
			</div>
		
		</form>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/relatorioData.relatorio.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>