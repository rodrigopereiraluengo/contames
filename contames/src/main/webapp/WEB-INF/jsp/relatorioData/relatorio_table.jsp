<fmt:message key="patternDate" var="patternDate" />
<fmt:message var="subTotalTipoDiaPattern" key="SubtotalTipo.DIA.pattern" />
<fmt:message var="subTotalTipoMesPattern" key="SubtotalTipo.MES.pattern" />
<fmt:message var="subTotalTipoAnoPattern" key="SubtotalTipo.ANO.pattern" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumRegistroEncontrado"/>
		</strong>
	</div>
	<script type="text/javascript">
		$(function(){
			$('#layChart').hide();
		});
	</script>
</c:if>
<c:if test="${count > 0}">
	<script type="text/javascript">
		document.getElementById('layChart').style.display = '';
	</script>
	<c:set var="path" value="relatorios/data" />
	<div class="container-fluid">
		<div class="row cm-row-total-top">
			<div class="col-xs-3 text-center cm-col-total">
				<strong><fmt:message key="totalGeral" /></strong>
			</div>
			<div class="col-xs-3 text-center cm-col-total">
				<fmt:message key="despesas"/>:
				<span class="font-red"><fmt:formatNumber type="number" value="${sum_des}" maxFractionDigits="2" minFractionDigits="2" /></span>
			</div>
			<div class="col-xs-3 text-center cm-col-total">
				<fmt:message key="receitas"/>:
				<span class="font-blue"><fmt:formatNumber type="number" value="${sum_rec}" maxFractionDigits="2" minFractionDigits="2" /></span>
			</div>
			<div class="col-xs-3 text-center cm-col-total">
				<fmt:message key="total"/>:
				<span class="${sum lt 0 ? 'font-red' : 'font-blue'}">
					<fmt:formatNumber var="totalGeralTop" type="number" value="${sum}" maxFractionDigits="2" minFractionDigits="2" />
					${fn:replace(totalGeralTop, '-', '')}
				</span>
			</div>
		</div>
	</div>
	<c:if test="${page eq 1}">
		<script type="text/javascript">
			var labels = [];
			var total = [];
			var despesas = [];
			var receitas = [];
			var sum_des = ${sum_des};
			var sum_rec = ${sum_rec};
			<c:forEach var="mapSubTotal" items="${resultMapSubtotal}">
				total.push(${resultMapSubtotal[mapSubTotal.key]['_sum']});
				despesas.push(${resultMapSubtotal[mapSubTotal.key]['sum_des']});
				receitas.push(${resultMapSubtotal[mapSubTotal.key]['sum_rec']});
				<c:choose>
					<c:when test="${subtotal eq 'DIA'}">
						<fmt:parseDate pattern="yyyyMMdd" value="${mapSubTotal.key}" var="_date" />
						<fmt:formatDate var="label" pattern="${subTotalTipoDiaPattern}" value="${_date}" />
						labels.push('${label}');
					</c:when>
					<c:when test="${subtotal eq 'MES'}">
						<fmt:parseDate pattern="yyyyMM" value="${mapSubTotal.key}" var="_date" />
						<fmt:formatDate var="label" pattern="${subTotalTipoMesPattern}" value="${_date}" />
						labels.push('${label}');
					</c:when>
					<c:when test="${subtotal eq 'ANO'}">
						<fmt:parseDate pattern="yyyy" value="${mapSubTotal.key}" var="_date" />
						<fmt:formatDate var="label" pattern="${subTotalTipoAnoPattern}" value="${_date}" />
						labels.push('${label}');
					</c:when>
				</c:choose>
			</c:forEach>
			if(typeof loadChartRelatorioData != 'undefined') {
				loadChartRelatorioData();
			}
		</script>
	</c:if>
	
	<div class="table-responsive">
	
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="descricao" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" name="doc" column="numeroDoc_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" name="meio" column="meio_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" name="cCredConta" column="meio_conta" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="parcela" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" name="status" column="status_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" name="valor" column="valor" page="${page}" path="${path}" />
				</tr>
			</thead>
			<tbody id="tbyRelatorioData">
				<c:forEach var="rel" items="${relList}" varStatus="stRel">
					<tr 
						data-tipo="${rel['tipo']}"
						data-lancamento-parcela="${rel['lancamentoparcela_id']}" 
						data-lancamento="${rel['lancamento_id']}"
						data-baixa-item="${rel['baixaitem_id']}"
						data-baixa="${rel['baixa_id']}"
						data-transferencia="${rel['transferencia_id']}"
						data-compensacao-item="${rel['compensacaoitem_id']}"
						data-compensacao="${rel['compensacao_id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${rel['_data']}" /></td>
						<td data-favorecido="${rel['favorecido_id']}" data-item="${rel['item']}"<c:if test="${f:orderByContains('descricao', orderByList)}"> class="font-bold"</c:if>>${rel['descricao']}</td>
						<td<c:if test="${f:orderByContains('numeroDoc_clean', orderByList)}"> class="font-bold"</c:if>>
							${rel['numerodoc_nome']}
							<c:if test="${rel['fatcartaocredito_nome'] ne null}">
							&nbsp;<c:out value="${rel['fatcartaocredito_nome']}" />
							</c:if>
							<c:if test="${rel['arquivodoc_id'] ne null}">
								<a title="${rel['arquivodoc_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${rel['arquivodoc_id']}/download/${rel['arquivodoc_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('meio_nome', orderByList)}"> font-bold</c:if>">
							${rel['meio_nome']}
							<c:if test="${rel['boletotipo'] ne null and rel['boletonumero'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(rel['boletonumero']))}', title: '<fmt:message key="BoletoTipo.${rel['boletotipo']}" />', stopPropagation: true}, event)" role="button" title="<fmt:message key="codigoBarras"/>">
									<i class="fa fa-barcode"></i>
								</a>
							</c:if>
							<c:if test="${rel['arquivo_id'] ne null}">
								<a title="${rel['arquivo_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${rel['arquivo_id']}/download/${rel['arquivo_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td 
							data-conta="${rel['conta_id']}" 
							data-cartao-credito="${rel['cartaocredito_id']}"
							data-talao-cheque="${rel['talaocheque_id']}" 
							class="text-center<c:if test="${f:orderByContains('meio_conta', orderByList)}"> font-bold</c:if>">${rel['meio_conta']}</td>
						<td class="text-center<c:if test="${f:orderByContains('parcela', orderByList)}"> font-bold</c:if>">${rel['parcela']}</td>
						<td class="text-center<c:if test="${f:orderByContains('status_nome', orderByList)}"> font-bold</c:if>">${rel['status_nome']}</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>">
							<span class="${rel['valor'] lt 0 ? 'font-red' : 'font-blue'}">
								<fmt:formatNumber var="valor" type="number" value="${rel['valor']}" maxFractionDigits="2" minFractionDigits="2" />
								${fn:replace(valor, '-', '')}
								<c:if test="${rel['comprovante_id'] ne null}">
									<a title="${rel['comprovante_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${rel['comprovante_id']}/download/${rel['comprovante_nome']}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
							</span>
						</td>
					</tr>
					
					<c:choose>
						
						<%-- Subtotais por Dia --%>
						<c:when test="${subtotal eq 'DIA'}">
							<fmt:formatDate var="keyMapSubtotal" pattern="yyyyMMdd" value="${rel['_data']}" />
							<c:if test="${(stRel.last and paginationSize eq page) or (!stRel.last and relList[stRel.index + 1]['_data'] ne rel['_data'])}">
								<tr class="cm-tr-total">
									<td colspan="8">
										<div class="container-fluid">
											<div class="row cm-row-total">
												<div class="col-xs-3 text-center cm-col-total">
													<strong>
														<fmt:formatDate pattern="${subTotalTipoDiaPattern}" value="${rel['_data']}" />
													</strong>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="despesas"/>:
													<span class="font-red"><fmt:formatNumber type="number" value="${resultMapSubtotal[keyMapSubtotal]['sum_des']}" maxFractionDigits="2" minFractionDigits="2" /></span>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="receitas"/>:
													<span class="font-blue"><fmt:formatNumber type="number" value="${resultMapSubtotal[keyMapSubtotal]['sum_rec']}" maxFractionDigits="2" minFractionDigits="2" /></span>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="total"/>:
													<span class="${resultMapSubtotal[keyMapSubtotal]['_sum'] lt 0 ? 'font-red' : 'font-blue'}">
														<fmt:formatNumber var="subTotalDia" type="number" value="${resultMapSubtotal[keyMapSubtotal]['_sum']}" maxFractionDigits="2" minFractionDigits="2" />
														${fn:replace(subTotalDia, '-', '')}
													</span>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</c:if>
						</c:when>
						
						
						<%-- Subtotais por Mes --%>
						<c:when test="${subtotal eq 'MES'}">
							<fmt:formatDate var="keyMapSubtotal" pattern="yyyyMM" value="${rel['_data']}" />
							<c:if test="${(stRel.last and paginationSize eq page) or (!stRel.last and f:formatSqlDate(relList[stRel.index + 1]['_data'], 'yyyyMM') ne f:formatSqlDate(rel['_data'], 'yyyyMM'))}">
								<tr class="cm-tr-total">
									<td colspan="8">
										<div class="container-fluid">
											<div class="row cm-row-total">
												<div class="col-xs-3 text-center cm-col-total">
													<strong><fmt:formatDate pattern="${subTotalTipoMesPattern}" value="${rel['_data']}" /></strong>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="despesas"/>:
													<span class="font-red"><fmt:formatNumber type="number" value="${resultMapSubtotal[keyMapSubtotal]['sum_des']}" maxFractionDigits="2" minFractionDigits="2" /></span>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="receitas"/>:
													<span class="font-blue"><fmt:formatNumber type="number" value="${resultMapSubtotal[keyMapSubtotal]['sum_rec']}" maxFractionDigits="2" minFractionDigits="2" /></span>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="total"/>:
													<span class="${resultMapSubtotal[keyMapSubtotal]['_sum'] lt 0 ? 'font-red' : 'font-blue'}">
														<fmt:formatNumber var="subTotalMes" type="number" value="${resultMapSubtotal[keyMapSubtotal]['_sum']}" maxFractionDigits="2" minFractionDigits="2" />
														${fn:replace(subTotalMes, '-', '')}
													</span>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</c:if>
						</c:when>
						
						
						<%-- Subtotais por Ano --%>
						<c:when test="${subtotal eq 'ANO'}">
							<fmt:formatDate var="keyMapSubtotal" pattern="yyyy" value="${rel['_data']}" />
							<c:if test="${(stRel.last and paginationSize eq page) or (!stRel.last and f:formatSqlDate(relList[stRel.index + 1]['_data'], 'yyyy') ne f:formatSqlDate(rel['_data'], 'yyyy'))}">
								<tr class="cm-tr-total">
									<td colspan="8">
										<div class="container-fluid">
											<div class="row cm-row-total">
												<div class="col-xs-3 text-center cm-col-total">
													<strong><fmt:formatDate pattern="${subTotalTipoAnoPattern}" value="${rel['_data']}" /></strong>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="despesas"/>:
													<span class="font-red"><fmt:formatNumber type="number" value="${resultMapSubtotal[keyMapSubtotal]['sum_des']}" maxFractionDigits="2" minFractionDigits="2" /></span>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="receitas"/>:
													<span class="font-blue"><fmt:formatNumber type="number" value="${resultMapSubtotal[keyMapSubtotal]['sum_rec']}" maxFractionDigits="2" minFractionDigits="2" /></span>
												</div>
												<div class="col-xs-3 text-center cm-col-total">
													<fmt:message key="total"/>:
													<span class="${resultMapSubtotal[keyMapSubtotal]['_sum'] lt 0 ? 'font-red' : 'font-blue'}">
														<fmt:formatNumber var="subTotalAno" type="number" value="${resultMapSubtotal[keyMapSubtotal]['_sum']}" maxFractionDigits="2" minFractionDigits="2" />
														${fn:replace(subTotalAno, '-', '')}
													</span>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</c:if>
						</c:when>
					
					</c:choose>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr class="cm-tr-total">
					<td colspan="8">
						<div class="container-fluid">
							<div class="row cm-row-total">
								<div class="col-xs-3 text-center cm-col-total">
									<strong><fmt:message key="totalGeral" /></strong>
								</div>
								<div class="col-xs-3 text-center cm-col-total">
									<fmt:message key="despesas"/>:
									<span class="font-red"><fmt:formatNumber type="number" value="${sum_des}" maxFractionDigits="2" minFractionDigits="2" /></span>
								</div>
								<div class="col-xs-3 text-center cm-col-total">
									<fmt:message key="receitas"/>:
									<span class="font-blue"><fmt:formatNumber type="number" value="${sum_rec}" maxFractionDigits="2" minFractionDigits="2" /></span>
								</div>
								<div class="col-xs-3 text-center cm-col-total">
									<fmt:message key="total"/>:
									<span class="${sum lt 0 ? 'font-red' : 'font-blue'}">
										<fmt:formatNumber var="totalGeralBottom" type="number" value="${sum}" maxFractionDigits="2" minFractionDigits="2" />
										${fn:replace(totalGeralBottom, '-', '')}
									</span>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tfoot>		
		</table>
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" isLazy="true" />
  		<cmt:paglaz orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>

</c:if>