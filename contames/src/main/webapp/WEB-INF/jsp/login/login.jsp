<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="login" /> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="login"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="login" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body" cascade="true">
	
		<div class="container-xmini">
			<div class="panel panel-default">
				
				<div class="panel-heading">
				
					<h3><fmt:message key="login" /></h3>
				
				</div>
				
				<div class="panel-body">
				
					<form id="frmLogin" action="${pageContext.request.contextPath}/entrar" method="post">
						
						<input type="hidden" name="url" value="<c:out value="${url}"/>" />
						
						<div class="form-group">
							<label class="control-label" for="email"><fmt:message key="email" /></label>
							<input type="email" id="email" name="email" value="${email eq null ? '' : email}" class="form-control" data-category="login.email email" />
						</div>
						
						<div class="form-group">
							<label class="control-label" for="senha"><fmt:message key="senha" /></label>
							<input type="password" id="senha" name="senha" class="form-control" data-category="login.senha senha" />
						</div>
						
						<div class="checkbox">
			            	<label>
			            		<input type="checkbox" name="lembrar" id="lembrar" value="true"<c:if test="${lembrar ne null and lembrar}"> checked="checked"</c:if> /> <fmt:message key="login.lembrar" />
		            		</label>
			            </div>
			            
			            <button type="submit" class="btn btn-primary btn-block" disabled="disabled"><fmt:message key="login.entrar" /></button>
			            <a href="${pageContext.request.contextPath}/esqueci-minha-senha" class="btn btn-default btn-block"><fmt:message key="login.esqueciSenha" /></a>
						
					</form>
				
				</div>
			
			</div>
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="/resources/js/login.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>