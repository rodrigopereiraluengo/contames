<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="atualizarSenha"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="atualizarSenha" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="container-xmini">
		
			<div class="panel panel-default">
			
				<div class="panel-heading">
					<h3><fmt:message key="atualizarSenha"/></h3>
				</div>
				
				<div class="panel-body">
					<c:choose>
						<c:when test="${esqueciSenha.status eq 'STD'}">
						
							<form id="frmAtualizarSenha" action="${pageContext.request.contextPath}/atualizar-senha" method="post">
						
								<input type="hidden" name="id" value="${esqueciSenha.id}" />
							
								<div class="form-group">
									<label for="senha" class="control-label"><fmt:message key="novaSenha"/><span class="required">*</span></label>
									<input type="password" id="senha" name="senha" maxlength="40" class="form-control" data-category="senha atualizarSenha.senha" />
								</div>
								
								<div class="form-group">
									<label for="confirmeSenha" class="control-label"><fmt:message key="confirmeNovaSenha"/><span class="required">*</span></label>
									<input type="password" id="confirmeSenha" name="confirmeSenha" maxlength="40" class="form-control" data-category="confirmeSenha atualizarSenha.confirmeSenha" />
								</div>
								
								<button type="submit" class="btn btn-primary btn-block" disabled="disabled"><fmt:message key="atualizar"/></button>
							
							</form>
						
						</c:when>
						
						<c:when test="${esqueciSenha.status eq 'ATI'}">
						
							<div class="alert alert-info text-center" role="alert"><strong><fmt:message key="esqueciSenha.linkUtilizado" /></strong></div>
						
						</c:when>
						
						<c:when test="${esqueciSenha.status eq 'INA'}">
						
							<div class="alert alert-info text-center" role="alert"><strong><fmt:message key="esqueciSenha.linkExpirado" /></strong></div>
						
						</c:when>
						
					</c:choose>
					
				
				</div>
			
			</div>
		
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="/resources/js/login.atualizarSenha.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>