<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="login.esqueciSenha"/> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="login.esqueciSenha"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="login.esqueciSenha" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="container-xmini">
    		<div class="panel panel-default">
  				<div class="panel-heading"><h3><fmt:message key="login.esqueciSenha" /></h3></div>
  				<div class="panel-body">
	    			<form id="frmEsqueciSenha" action="${pageContext.request.contextPath}/esqueci-minha-senha" method="post">
	    				
	    				<div class="form-group">
			    			<label for="email" class="control-label"><fmt:message key="login.esqueciSenha.email" /><span class="required">*</span></label>
			            	<input name="email" id="email" type="email" value="${email}" class="form-control" data-category="esqueciSenha.email email" />
			            </div>
			    		
			    		<button class="btn btn-primary btn-block" type="submit" disabled="disabled"><fmt:message key="enviar" /></button>
		    	
	    			</form>
	    		</div>
			</div>
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="/resources/js/login.esqueciSenha.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>