<%@page trimDirectiveWhitespaces="true" contentType="application/json; charset=ISO-8859-1" %>
<json:array>
	<c:forEach var="categoria" items="${classificacaoList}">
		<json:object>
			<json:property name="id" value="classificacao_${categoria.id}" />			
			<json:property name="text" value="${categoria.nome}" />
			<json:object name="data">
				<json:property name="id" value="${categoria.id}" />	
				<json:property name="nome" value="${categoria.nome}" />
				<json:property name="nivel" value="${categoria.nivel}" />
				<json:property name="index" value="${categoria.index}" />
			</json:object>
			<json:object name="state">
				<json:property name="opened" value="${false}" />
				<json:property name="selected" value="${false}" />		
			</json:object>
			<json:property name="children" value="${true}" />
		</json:object>
	</c:forEach>
</json:array>