<%@page trimDirectiveWhitespaces="true" contentType="application/json; charset=ISO-8859-1" %>
<json:array var="classificacao" items="${classificacaoList}">
	<json:object>
		<json:property name="id" value="${classificacao.id}" />			
		<json:property name="name" value="${classificacao.nome}" />
	</json:object>
</json:array>