<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhuma${tipo eq 'DES' ? 'Despesa' : (tipo eq 'REC' ? 'Receita' : 'Produto')}Encontrada"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	
	<c:set var="path" value="cadastros/${tipo eq 'DES' ? 'despesas' : (tipo eq 'REC' ? 'receitas' : 'produtos')}"/>
	
	<div class="table-responsive">
 		<table id="tblItem" class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="classificacao" column="classificacao_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="descricao" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" name="unidade" column="unidade_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="valor" page="${page}" path="${path}" />
					<c:choose>
						<c:when test="${tipo eq 'PRD'}">
							<cmt:th orderByList="${orderByList}" index="6" column="estoque" page="${page}" path="${path}" />	
						</c:when>
						<c:otherwise>
							<cmt:th orderByList="${orderByList}" index="6" name="lancamentoAuto" column="isLancAuto" page="${page}" path="${path}" />
							<cmt:th orderByList="${orderByList}" index="7" name="vencimentoDia" column="diaVencimento" page="${page}" path="${path}" />
						</c:otherwise>
					</c:choose>
				</tr>
			</thead>
			<tbody id="tbyItem">
				<c:forEach var="item" items="${itemList}">
					<tr data-id="${item['id']}" data-category="item_${item['id']}" class="<c:if test="${item['fav_order'] eq 0}"> order</c:if>">
						<td<c:if test="${f:orderByContains('classificacao_nome', orderByList)}"> class="font-bold"</c:if>><c:out value="${item['classificacao_nome']}"/></td>
						<td<c:if test="${f:orderByContains('nome', orderByList)}"> class="font-bold"</c:if>><c:out value="${item['nome']}" /></td>
						<td class="text-center<c:if test="${item['_status'] eq 'INA'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${item['status']}</td>
						<td<c:if test="${f:orderByContains('descricao', orderByList)}"> class="font-bold"</c:if>>
							<c:if test="${item['descricao'] ne null}">
								${f:outHtml(item['descricao'])}
							</c:if>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('unidade_nome', orderByList)}"> font-bold</c:if>">
							<c:if test="${item['unidade_nome'] ne null}">
								<c:out value="${item['unidade_nome']}" />
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>">
							<c:if test="${item['valor'] ne null}">
								<span class="${item['tipo'] eq 'DES' ? 'font-red' : ' font-blue'}">
									<fmt:formatNumber type="number" value="${item['valor']}" maxFractionDigits="2" minFractionDigits="2" />
								</span>
							</c:if>
						</td>
						<c:choose>
							<c:when test="${tipo eq 'PRD'}">
								<td class="text-right<c:if test="${f:orderByContains('estoque', orderByList)}"> font-bold</c:if>"><fmt:formatNumber type="number" value="${item['estoque']}" maxFractionDigits="3" minFractionDigits="3" /></td>
							</c:when>
							<c:otherwise>
								<td class="text-center<c:if test="${f:orderByContains('isLancAuto', orderByList)}"> font-bold</c:if>">${item['islancauto']}</td>
								<td class="text-right<c:if test="${f:orderByContains('diaVencimento', orderByList)}"> font-bold</c:if>">${item['diavencimento']}</td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 	</div>

	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>

</c:if>