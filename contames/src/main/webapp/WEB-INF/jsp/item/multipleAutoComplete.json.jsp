<%@page trimDirectiveWhitespaces="true" contentType="application/json; charset=ISO-8859-1" %>
<json:array var="item" items="${itemList}">
	<json:object>
		<json:property name="id" value="${item.id}" />
		<json:property name="isClassif" value="${item.isclassif}" />			
		<json:property name="name" value="${item.nome}" />
	</json:object>
</json:array>