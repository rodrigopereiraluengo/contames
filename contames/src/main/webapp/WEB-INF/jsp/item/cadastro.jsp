<div id="dialogItemCadastro" class="modal" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<form 
				id="frmItemCadastro" 
				action="${pageContext.request.contextPath}/${item.tipo eq 'DES' ? 'despesa' : (item.tipo eq 'REC' ? 'receita' : 'produto')}/cadastro" 
				method="post" 
				data-tipo="${item.tipo}">
				
				<input type="hidden" name="viewid" value="${viewid}" />
			
				<input type="hidden" name="item.id" value="${item.id}" />
				
				<input type="hidden" id="itemConta" name="item.conta.id" value="${item.conta.id}" />
				<input type="hidden" id="itemCartaoCredito" name="item.cartaoCredito.id" value="${item.cartaoCredito.id}" />
				<input type="hidden" id="itemCartaoCreditoIsLimite" name="item.isLimite" value="${item.isLimite}" />
				
				<input type="hidden" id="itemClassificacaoId" name="item.classificacao.id"<c:if test="${item ne null and item.classificacao ne null}"> value="${item.classificacao.id}"</c:if> />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogItemCadastroTitle"><fmt:message key="ItemTipo.${item.tipo}"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div class="container-fluid">
					
						<div class="row">
						
							<div class="col-xs-12 col-md-5">
							
								<div class="panel panel-default ">
			
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 col-sm-4 text-nowrap">
												<p class="form-control-static font-bold">
													<fmt:message key="classificacao"/>
													<a class="info-help" data-message="item.${item.tipo}.classificacao.help">(?)</a>
												</p>
											</div>
											<div class="col-xs-12 col-sm-8 text-right">
												<div class="btn-group">
	  												<button type="button" class="btn btn-default" onclick="novaClassif()" title="<fmt:message key="title.adicione.classificacao"/>">
	  													<i class="fa fa-plus"></i>
	  												</button>
		  											<button type="button" class="btn btn-default" id="btItemEditarClassif" disabled="disabled" onclick="editarClassif()" title="<fmt:message key="title.editar.classificacao"/>">
		  												<i class="fa fa-pencil"></i>
		  											</button>
			  										<button type="button" class="btn btn-default" id="btItemExcluirClassif" disabled="disabled" onclick="excluirClassif()" title="<fmt:message key="title.excluir.classificacao"/>">
			  											<i class="fa fa-trash"></i>
			  										</button>
												</div>
											</div>
										</div>
					    			</div>
									
									<div id="trClassif"></div>
									
									
									
								</div>
							
							</div>	
							
							
							<div class="col-xs-12 col-md-7">
							
							
								<div class="panel panel-default">
			
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12 text-nowrap">
												<p class="form-control-static font-bold"><fmt:message key="cadastro"/></p>
											</div>
										</div>
					    			</div>
									
									<div class="panel-body">
									
										<div class="row">
										
											<div class="col-xs-12 col-sm-9 form-group">
											
												<label class="control-label" for="itemNome">
		    										<fmt:message key="nome"/>
		    										<span class="required">*</span>
		    										<a class="info-help" data-message="item.${item.tipo}.nome.help">(?)</a>
	    										</label>
	    						
		    									<input type="text" id="itemNome" name="item.nome"<c:if test="${item ne null}"> value="<c:out value="${item.nome}"/>"</c:if> maxlength="100" class="form-control required" data-category="nome" data-simple-auto-complete-is-selected="false" />
											
											</div>
											
											<div class="col-xs-12 col-sm-3 form-group">
											
												<label class="control-label" for="itemStatus">
			    									<fmt:message key="status"/>
			    									<span class="required">*</span>
			    									<a class="info-help" data-message="item.${item.tipo}.status.help">(?)</a>
		    									</label>
		    					
		    									<select id="itemStatus" name="item.status" class="form-control" data-category="status">
		    										<option value="ATI"<c:if test="${item ne null and item.status eq 'ATI'}"> selected="selected"</c:if>><fmt:message key="Status.ATI"/></option>
		    										<option value="INA"<c:if test="${item ne null and item.status eq 'INA'}"> selected="selected"</c:if>><fmt:message key="Status.INA"/></option>
		    									</select>
											
											</div>
											
										
										</div>
										
										<div class="form-group">
    										<label class="control-label" for="itemDescricao"><fmt:message key="descricao"/></label>
    										<textarea id="itemDescricao" name="item.descricao" class="form-control" rows="4" maxlength="400" data-category="descricao"><c:if test="${item ne null and item.descricao ne null}"><c:out value="${item.descricao}"/></c:if></textarea>
										</div>
										
										
										
										<c:if test="${item.tipo eq 'PRD'}">
										
											<div class="row">
											
												<div class="col-xs-12 col-sm-6 form-group">
												
													<label class="control-label" for="itemEstoque">
														<fmt:message key="estoque" />
														<span class="required">*</span>
													</label>
													<input type="text" id="itemEstoque" name="item.estoque"<c:if test="${item ne null and item.estoque ne null}"> value="<fmt:formatNumber type="number" value="${item.estoque}" maxFractionDigits="3" minFractionDigits="3" />"</c:if> data-category="estoque" class="form-control" />
												
												</div>
												
												<div class="col-xs-12 col-sm-6 form-group">
												
													<label class="control-label" for="itemValorCompra">
														<fmt:message key="valorCompra" />
													</label>
													<input type="text" id="itemValorCompra" name="item.valorCompra"<c:if test="${item ne null and item.valorCompra ne null}"> value="<fmt:formatNumber type="number" value="${item.valorCompra}" maxFractionDigits="2" minFractionDigits="2" />"</c:if> data-category="valorCompra" class="form-control" />
												
												</div>
												
											</div>
																					
										</c:if>
										
										
										
										<div class="row">
										
											<div class="col-xs-12 col-sm-7 form-group">
												
												<label class="control-label" for="itemUnidade"><fmt:message key="unidade"/></label>
						    					
						    					<div class="input-group">
							    					<select 
							    						id="itemUnidade" 
							    						name="item.unidade.id" 
							    						class="form-control"
							    						data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=UNIDADE"
							    						data-search="${pageContext.request.contextPath}/descricao/consultar?descricaoTipo=UNIDADE"
							    						data-load-options="${pageContext.request.contextPath}/descricao/carregaSelect?descricaoTipo=UNIDADE"
							    						data-category="unidade">
							    						<c:if test="${unidadeList eq null or empty unidadeList}">
							    							<option value=""><fmt:message key="nenhum.UNIDADE.encontrado"/></option>
							    						</c:if>
							    						<c:if test="${unidadeList ne null and !empty unidadeList}">
							    							<option value=""><fmt:message key="selecione___"/></option>
							    							<c:forEach var="unidade" items="${unidadeList}">
							    								<option value="${unidade.id}"<c:if test="${item ne null and item.unidade eq unidade}"> selected="selected"</c:if>><c:out value="${unidade.nome}"/></option>
							    							</c:forEach>
							    						</c:if>
							    					</select>
							    					<span class="input-group-btn">
							    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.unidade" />">
				        									<i class="fa fa-plus"></i>
														</button>
				        								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.unidade" />">
				        									<i class="fa fa-pencil"></i>
				        								</button>
														<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.unidade" />">
															<i class="fa fa-search"></i>
				        								</button>
				      								</span>
						    					</div>
											
											</div>
											
											<div class="col-xs-12 col-sm-5 form-group">
											
												<label class="control-label" for="itemValor">
													<fmt:message key="valor"/>
													<span class="required" id="reqItemValor" style="display: none">*</span>
												</label>
		    									<input type="text" id="itemValor" name="item.valor"<c:if test="${item ne null and item.valor ne null}"> value="<fmt:formatNumber type="number" value="${item.valor}" maxFractionDigits="2" minFractionDigits="2" />"</c:if> data-category="valor" class="form-control" />
											
											</div>
										
										</div>
										
										
										
										<c:if test="${item.tipo ne 'PRD'}">
										
											<div class="row">
											
												<div class="col-xs-12 col-sm-3 form-group">
													<label class="control-label" for="itemIsLancAuto">
														<fmt:message key="lancamentoAuto" />
														<a class="info-help" data-message="item.isLancAuto.help">(?)</a>
													</label>
													<select id="itemIsLancAuto" name="item.isLancAuto" data-category="isLancAuto" class="form-control">
						  								<option value="false"<c:if test="${item.isLancAuto eq null or !item.isLancAuto}"> selected="selected"</c:if>><fmt:message key="nao" /></option>
						  								<option value="true"<c:if test="${item.isLancAuto ne null and item.isLancAuto}"> selected="selected"</c:if>><fmt:message key="sim" /></option>
						  							</select>	
												</div>
												
												<div class="col-xs-12 col-sm-9 form-group">
													<label class="control-label" for="itemFavorecido">
														<c:if test="${item.tipo eq 'DES'}"><fmt:message key="pagarA" /></c:if>
														<c:if test="${item.tipo eq 'REC'}"><fmt:message key="receberDe" /></c:if>
														<span class="required" id="reqItemFavorecido">*</span>
													</label>
													<div class="input-group">
														<input 
						  									type="text" 
						  									id="itemFavorecido" 
						  									name="item.favorecido.id" 
						  									class="form-control"
						  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
								    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoas&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
								    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
								    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
								    						<c:if test="${item ne null and item.favorecido ne null}"> value="${item.favorecido.displayNome}" data-val="${item.favorecido.id}"</c:if>
								    						data-category="favorecido"
							    						/>
								  						
								    					<span class="input-group-btn">
								    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
						       									<i class="fa fa-plus"></i>
															</button>
						       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
						       									<i class="fa fa-pencil"></i>
						       								</button>
															<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
																<i class="fa fa-search"></i>
						       								</button>
						   								</span>
							  						</div>
												</div>
												
											</div>
											
											<div class="row">
												<div class="col-xs-12 col-sm-4 form-group">
												
													<label class="control-label" for="itemDiaVencimento">
														<fmt:message key="vencimentoDia"/>
														<span class="required" id="reqItemDiaVencimento">*</span>
													</label>
													<input type="number" id="itemDiaVencimento" name="item.diaVencimento"<c:if test="${item ne null and item.diaVencimento ne null}"> value="${item.diaVencimento}"</c:if> maxlength="2" min="1" max="31" data-category="diaVencimento" class="form-control text-right" />
												
												</div>
												<div class="col-xs-12 col-sm-8 form-group">
													<label class="control-label" for="itemMeio">
														<fmt:message key="meio" />
														<span class="required" id="reqItemMeio">*</span>
													</label>
													<div class="input-group">
														<select id="itemMeio" name="item.meio" data-category="meio" class="form-control">
							  								<option value=""><fmt:message key="selecione___" /></option>
							  								<option value="CDB"<c:if test="${item.meio eq 'CDB'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CDB" /></option>
							  								<option value="CCR"<c:if test="${item.meio eq 'CCR'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CCR" /></option>
							  								<option value="INT"<c:if test="${item.meio eq 'INT'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.INT" /></option>
							  								<option value="DEA"<c:if test="${item.meio eq 'DEA'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DEA" /></option>
							  								<option value="DIN"<c:if test="${item.meio eq 'DIN'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DIN" /></option>
							  								<option value="CHQ"<c:if test="${item.meio eq 'CHQ'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CHQ" /></option>
							  								<option value="TRA"<c:if test="${item.meio eq 'TRA'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.TRA" /></option>
							  								<option value="DOC"<c:if test="${item.meio eq 'DOC'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DOC" /></option>
							  							</select>
							  							<span class="input-group-btn">
							  								<button class="btn btn-default" type="button" id="btItemConta" title="<fmt:message key="conta"/>">
						       									<i class="fa fa-folder-open"></i>
															</button>
															<button class="btn btn-default" type="button" id="btItemCartaoCredito" title="<fmt:message key="cartaoCredito"/>">
						       									<i class="fa fa-credit-card"></i>
															</button>
														</span>	
						  							</div>
												</div>
											</div>
										
										</c:if>
																				
									</div>
									
								</div>
							
								
								<!-- Midias -->
								<div class="panel panel-default">
			
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-4">
						   						<p class="form-control-static font-bold"><fmt:message key="midias"/></p>
						   					</div>
						   					<div class="col-xs-8 text-right">
						   						<div class="btn-group">
						   							<button type="button" id="itemBtAdicionarProdutoMidia" class="btn btn-default" onclick="dialogAdicionarItemMidia()" title="<fmt:message key="adicionar"/>">
						   								<i class="fa fa-plus"></i>
						   							</button>
						   							<button type="button" id="itemBtEditarProdutoMidia" class="btn btn-default" disabled="disabled" onclick="editarItemMidia()" title="<fmt:message key="editar"/>">
					   									<i class="fa fa-pencil"></i>
						   							</button>
						   							<button type="button" id="itemBtExcluitProdutoMidia" class="btn btn-default" disabled="disabled" onclick="removerItemMidia()" title="<fmt:message key="remover"/>">
						   								<i class="fa fa-trash"></i>
						   							</button>
						   						</div>
						   					</div>
										</div>
					    			</div>
									
									<div class="data-table">
										<div class="result">
											<jsp:include page="itemMidiaList.jsp" />
			   							</div>
									</div>
									
								</div>
							
							</div>
						
						</div>
					
					</div>
				
				</div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<i class="fa fa-close"></i>
						<b><fmt:message key="cancelar"/></b>
					</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
				</div>
				
				<c:if test="${item ne null and item.id ne null}">
					<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${item.criacao}" create="create" pessoa="${item.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${item.alteracao}" create="update" pessoa="${item.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
			
			</form>
		</div>
	</div>
</div>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/jstree/themes/default/style.min.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jstree/jstree.min.js?v${applicationVersion}"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/item.cadastro.js?v${applicationVersion}"></script>
<c:if test="${item ne null and item.classificacao ne null}">
	<c:choose>
		<c:when test="${item.classificacao.nivel eq 'CAT'}">
			<script type="text/javascript">
				var arrNodeSelected = ['${item.classificacao.id}'];
			</script>
		</c:when>
		<c:when test="${item.classificacao.nivel eq 'FAM'}">
			<script type="text/javascript">
				var arrNodeSelected = ['${item.classificacao.classificacao.id}', '${item.classificacao.id}'];
			</script>
		</c:when>
		<c:when test="${item.classificacao.nivel eq 'TIP'}">
			<script type="text/javascript">
				var arrNodeSelected = ['${item.classificacao.classificacao.classificacao.id}', '${item.classificacao.classificacao.id}', '${item.classificacao.id}'];
			</script>
		</c:when>
		<c:when test="${item.classificacao.nivel eq 'DET'}">
			<script type="text/javascript">
				var arrNodeSelected = ['${item.classificacao.classificacao.classificacao.classificacao.id}', '${item.classificacao.classificacao.classificacao.id}', '${item.classificacao.classificacao.id}', '${item.classificacao.id}'];
			</script>
		</c:when>
	</c:choose>
	
</c:if>