<c:if test="${param.tpl eq null}">
	<c:set var="tpl" value="admin"/>
</c:if>
<c:if test="${param.tpl ne null}">
	<c:set var="tpl" value="${param.tpl}"/>
</c:if>
<tiles:insertTemplate template="/WEB-INF/jsp/tpl/${tpl}.jsp">
	
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="cadastroDe.${tipo}"/></h1>
						<p class="description"><fmt:message key="cadastroDe.${tipo}.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="resumo" />
							</li>
				  			<li><fmt:message key="cadastros" /></li>
				  			<li><fmt:message key="${tipo eq 'DES' ? 'despesas' : (tipo eq 'REC' ? 'receitas' : 'produtos')}"/></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="body">
		
		<form 
			id="frmItemConsultar"
			action="${pageContext.request.contextPath}/cadastros/${tipo eq 'DES' ? 'despesas' : (tipo eq 'REC' ? 'receitas' : 'produtos')}"
			data-form="${pageContext.request.contextPath}/${tipo eq 'DES' ? 'despesa' : (tipo eq 'REC' ? 'receita' : 'produto')}/cadastro"
			data-delete="${pageContext.request.contextPath}/${tipo eq 'DES' ? 'despesa' : (tipo eq 'REC' ? 'receita' : 'produto')}/excluir?tipo=${tipo}"
			data-tipo="${tipo}"
			class="data-table">
			
			<input type="hidden" name="favorecido_id" value="${param.favorecido_id}" />
			<input type="hidden" name="conta_id" value="${param.conta_id}" />
			
			<div class="panel panel-default">
			
				<c:if test="${status ne null}"><input type="hidden" name="status" value="${status}"/></c:if>
				<c:if test="${idsNotIn ne null and !empty idsNotIn}">
					<c:forEach var="idNotIn" items="${idsNotIn}">
						<input type="hidden" name="idsNotIn[]" value="${idNotIn}"/>
					</c:forEach>
				</c:if>
				
				<div class="panel-heading cm-panel-heading-toolbar">
					
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-4 form-group">
					  			<div class="btn-group">
						  			<button type="button" class="btn btn-default bt-new">
						  				<i class="fa fa-plus"></i>
						  				<fmt:message key="${tipo eq 'PRD' ? 'novo' : 'nova'}" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-edit" disabled="disabled">
						  				<i class="fa fa-pencil"></i>
						  				<fmt:message key="editar" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-delete" disabled="disabled">
						  				<i class="fa fa-trash"></i>
						  				<fmt:message key="excluir" />
					  				</button>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
								<div class="input-group">
									<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<i class="fa fa-search"></i>
										</button>
									</span>
				    			</div>		
							</div>
						</div>
					</div>
				
				</div>
				
				<div class="result search-table">
					
					<jsp:include page="/WEB-INF/jsp/item/consultar_table.jsp" />				
		   		
		   		</div>
		   		
	   		</div>
	
		</form>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/item.consultar.js?v${applicationVersion}"></script>
		
	</tiles:putAttribute>
	
</tiles:insertTemplate>