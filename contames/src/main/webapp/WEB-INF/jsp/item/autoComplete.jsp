<ul>
	<c:if test="${itemList eq null or empty itemList}">
		<c:if test="${query eq null}"><li class="text-info"><fmt:message key="autoComplete.typeanyForStartSeatch"/></li></c:if>
		<c:if test="${query ne null}"><li class="text-danger autoComplete-notFound"><fmt:message key="nenhumRegistroEncontrado"/></li></c:if>
	</c:if>
	<c:if test="${itemList ne null and !empty itemList}">
		<c:forEach var="item" items="${itemList}">
			<li data-id="${item.id}" data-dia-vencimento="${item.diavencimento}" data-favorecido="${item.favorecido}" data-valor="<fmt:formatNumber type="number" value="${item.valor}" maxFractionDigits="2" minFractionDigits="2" />" data-meio="${item.meio}">${item.nome}</li>
		</c:forEach>
	</c:if>
</ul>