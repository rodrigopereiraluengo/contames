<div id="dialogDescricaoCadastro" class="modal" style="display: none" data-target-select="${param.targetSelect}">
  	<div class="modal-dialog">
   		<div class="modal-content">
   		   		
   			<form id="frmDescricaoCadastro" action="${pageContext.request.contextPath}/descricao/cadastro" method="POST">
   				<c:if test="${descricao ne null}"><input type="hidden" name="descricao.id" value="${descricao.id}" /></c:if>
   				<input type="hidden" name="descricao.descricaoTipo" value="${param.descricaoTipo}" />
   		
	   			<div class="modal-header">
	   				<button type="button" class="close" data-dismiss="modal">
	   					<i class="fa fa-times"></i>
   					</button>
	        		<h4 class="modal-title"><fmt:message key="DescricaoTipo.${param.descricaoTipo}"/></h4>
	      		</div>
	      		
	      		<div class="modal-body">
	      			<div class="row">
			      		
			      		<div class="form-group col-xs-12 col-sm-8">
							<label class="control-label" for="descricaoNome">
								<fmt:message key="nome"/>
								<span class="required">*</span>
							</label>
							<input type="text" id="descricaoNome" name="descricao.nome"<c:if test="${descricao ne null}"> value="<c:out value="${descricao.nome}"/>"</c:if> maxlength="50" data-category="nome" class="form-control" />
						</div>	
						
						<div class="form-group col-xs-12 col-sm-4">
							<label class="control-label" for="descricaoStatus">
								<fmt:message key="status"/>
								<span class="required">*</span>
							</label>
							<select id="descricaoStatus" name="descricao.status" data-category="status" class="form-control required">
								<option value="ATI"<c:if test="${descricao ne null and descricao.status eq 'ATI'}"> selected="selected"</c:if>><fmt:message key="Status.ATI"/></option>
								<option value="INA"<c:if test="${descricao ne null and descricao.status eq 'INA'}"> selected="selected"</c:if>><fmt:message key="Status.INA"/></option>
							</select>
						</div>
						
	      			</div>
	      		</div>
	      		
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default" data-dismiss="modal">
	      				<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	      			</button>
	        		<button type="submit" class="btn btn-primary">
	        			<i class="fa fa-save"></i>
	        			<b><fmt:message key="salvar"/></b>
        			</button>
	      		</div>
	      		
	      		<c:if test="${descricao ne null and descricao.id ne null and (descricao.criacao ne null or descricao.alteracao ne null)}">
					<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${descricao.criacao}" create="create" pessoa="${descricao.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${descricao.alteracao}" create="update" pessoa="${descricao.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
      		
      		</form>
   		
   		</div>
 	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/descricao.cadastro.js?v${applicationVersion}"></script>