<c:if test="${descricaoList eq null or empty descricaoList}">
	<option value=""><fmt:message key="nenhum.${param.descricaoTipo}.encontrado"/></option>
</c:if>
<c:if test="${descricaoList ne null and !empty descricaoList}">
	<option value=""><fmt:message key="selecione___"/></option>
	<c:forEach var="descricao" items="${descricaoList}">
		<option value="${descricao.id}"<c:if test="${id ne null and id eq descricao.id}"> selected="selected"</c:if>><c:out value="${descricao.nome}"/></option>
	</c:forEach>
</c:if>