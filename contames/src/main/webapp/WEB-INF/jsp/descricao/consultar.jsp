<div id="dialogDescricaoConsultar" class="modal cm-modal-data-table" style="display: none">
  	<div class="modal-dialog">
   		<div class="modal-content">
   			
   			<form 
   				id="frmDescricaoConsultar" 
   				action="${pageContext.request.contextPath}/descricao/consultar"
   				data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=${param.descricaoTipo}"
   				data-delete="${pageContext.request.contextPath}/descricao/excluir"
   				data-tipo="${param.descricaoTipo}">
     			
     			<input type="hidden" id="descricaoTipo" name="descricaoTipo" value="${param.descricaoTipo}" />
     			
      			<div class="modal-header">
      				<button type="button" class="close" data-dismiss="modal">
      					<i class="fa fa-times"></i>
   					</button>
	        		<h4 class="modal-title"><fmt:message key="DescricaoTipo.${param.descricaoTipo}S"/></h4>
	      		</div>
	      		
	      		<div class="modal-body">
	      			
	      			<div class="data-table">
	      			
	      				<div class="panel panel-default">
	      			
			      			<div class="panel-heading cm-panel-heading-toolbar">
				      			
				      			<div class="container-fluid">
				      				
				      				<div class="row">
						      			
						      			<div class="col-xs-12 col-sm-6 form-group">
						      				<div class="btn-group">
								    			<button type="button" class="btn btn-default bt-new">
								    				<i class="fa fa-plus"></i>
								    				<fmt:message key="nova"/>
							    				</button>
						        				<button type="button" class="btn btn-default bt-edit" disabled="disabled">
						        					<i class="fa fa-pencil"></i>
						        					<fmt:message key="editar"/>
					        					</button>
						        				<button type="button" class="btn btn-default bt-delete" disabled="disabled">
						        					<i class="fa fa-trash"></i>
						        					<fmt:message key="excluir"/>
					        					</button>
						        			</div>
								  		</div>
								  		
								  		<div class="col-xs-12 col-sm-6 form-group">
								  			<div class="input-group">
												<input type="search" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
												<span class="input-group-btn">
													<button class="btn btn-default" type="submit">
														<i class="fa fa-search"></i>
													</button>
												</span>
											</div>
								    	</div>
					  				
					  				</div>
					  			
					  			</div>
					  		
					  		</div>
		        			
		        			<div class="result search-table">
		        				<jsp:include page="/WEB-INF/jsp/descricao/consultar_table.jsp" />
			        		</div>
	      			
	      				</div>
	      			
	      			</div>
	      		
      			</div>
      			
      			<c:if test="${param.dialogFootOk ne null}">
					<div class="modal-footer">
			        	<button type="button" class="btn btn-default bt-cancel" data-dismiss="modal">
			        		<i class="fa fa-close"></i>
			        		<b><fmt:message key="cancelar"/></b>
		        		</button>
			        	<button type="button" class="btn btn-primary bt-ok" onclick="${param.dialogFootOk}" disabled="disabled">
			        		<i class="fa fa-check"></i>
			        		<b><fmt:message key="ok"/></b>
		        		</button>
			       	</div>
		       	</c:if>
	      		    		
    		</form>
    	
    	</div>
  	</div>
</div>
<script type="text/javascript">
	var idComboDescricao = '${param.comboDescricao}';
	var descricaoTipo = '${param.descricaoTipo}';
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/descricao.consultar.js?v${applicationVersion}"></script>