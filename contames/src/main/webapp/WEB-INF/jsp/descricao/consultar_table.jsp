<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhum.${param.descricaoTipo}.encontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
  	
  		<c:set var="path" value="descricao/consultar"/>
  	
	  	<table class="table table-hover">
	  		<thead>
	  			<tr>
	  				<cmt:th orderByList="${orderByList}" index="0" column="nome" page="${page}" path="${path}" />
	  				<cmt:th orderByList="${orderByList}" index="1" column="status" page="${page}" path="${path}" />
	  			</tr>
	  		</thead>
			<tbody id="tbyDescricao">
				<c:forEach var="descricao" items="${descricaoList}">
					<tr data-id="${descricao.id}">
						<td<c:if test="${f:orderByContains('nome', orderByList)}"> class="font-bold"</c:if>><c:out value="${descricao.nome}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>"><fmt:message key="Status.${descricao.status}"/></td>
					</tr>
				</c:forEach>
			</tbody>   
	  	</table>
	
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
			
</c:if>