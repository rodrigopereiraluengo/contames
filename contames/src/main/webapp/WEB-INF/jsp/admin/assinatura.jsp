<fmt:message key="patternDateTime" var="patternDateTime" />
<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="assinatura"/></h1>
						<p class="description"><fmt:message key="assinatura.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-user"></i>
								<c:out value="${usuarioLogged.usuario.displayNome}" />
							</li>
				  			<li><fmt:message key="assinatura" /></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<c:if test="${isReativar ne null and isReativar and usuarioLogged.cliente.assinatura.status eq 'ATI'}">
		<script>$(function(){ msgSuccess(i18n.get('assinatura.reativar.sucesso')); })</script>
		</c:if>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-5">
				<c:set var="isMostPopular" value="${usuarioLogged.cliente.assinatura.plano.id eq 2}"/>
				<div class="panel ${isMostPopular ? 'panel-primary' : 'panel-success'}">
						
					<div class="panel-heading">
						<h3><c:out value="${usuarioLogged.cliente.assinatura.plano.nome}"/></h3>
						<c:if test="${isMostPopular}"><span class="label label-success"><fmt:message key="maisPopular"/></span></c:if>
					</div>
					
					<table class="table">
						<thead>
							<tr>
								<th class="text-center"><fmt:message key="descricao" /></th>
								<th class="text-center"><fmt:message key="plano" /></th>
								<th class="text-center"><fmt:message key="utilizado" /></th>
								<th class="text-center"><fmt:message key="disponivel" /></th>
							</tr>
						</thead>
						<tr>
							<th><fmt:message key="contas"/></th>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.contas}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.contasUtil}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.contasDisp}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="cartoesCredito"/></th>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.cartoesCredito}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.cartoesCreditoUtil}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.cartoesCreditoDisp}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="lancamentos"/></th>
							<td class="text-right text-nowrap">
								<fmt:formatNumber value="${planoRecurso.lancamentos}"/>&nbsp;
								<fmt:message key="porMes" />
							</td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.lancamentosUtil}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.lancamentosDisp}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="bancoDados"/></th>
							<td class="text-right">
								<fmt:formatNumber value="${planoRecurso.bancoDados}"/>
								<fmt:message key="megaBytesAbrev" />
							</td>
							<td class="text-right">
								<fmt:formatNumber value="${planoRecurso.bancoDadosUtil}" minFractionDigits="2" maxFractionDigits="2" />
								<fmt:message key="megaBytesAbrev" />
							</td>
							<td class="text-right">
								<fmt:formatNumber value="${planoRecurso.bancoDadosDisp}" minFractionDigits="2" maxFractionDigits="2" />
								<fmt:message key="megaBytesAbrev" />
							</td>
						</tr>
						<tr>
							<th><fmt:message key="envioArquivos"/></th>
							<td class="text-right">
								<fmt:formatNumber value="${planoRecurso.armazenamento}"/>
								<fmt:message key="megaBytesAbrev" />
							</td>
							<td class="text-right">
								<fmt:formatNumber value="${planoRecurso.armazenamentoUtil}" minFractionDigits="2" maxFractionDigits="2"/>
								<fmt:message key="megaBytesAbrev" />
							</td>
							<td class="text-right">
								<fmt:formatNumber value="${planoRecurso.armazenamentoDisp}" minFractionDigits="2" maxFractionDigits="2"/>
								<fmt:message key="megaBytesAbrev" />
							</td>
						</tr>
						<tr>
							<th><fmt:message key="usuarios"/></th>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.usuarios}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.usuariosUtil}"/></td>
							<td class="text-right"><fmt:formatNumber value="${planoRecurso.usuariosDisp}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="acessoCriptografadoSSL"/></th>
							<td class="text-right"><fmt:message key="sim" /></td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
						</tr>
						<tr>
							<th><fmt:message key="backupDiario"/></th>
							<td class="text-right"><fmt:message key="sim" /></td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
						</tr>
						<tr>
							<th><fmt:message key="suporteEmail"/></th>
							<td class="text-right"><fmt:message key="sim" /></td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
						</tr>
					</table>
					<div class="panel-footer text-center">
						<div>
							<h3>
								<fmt:message key="currencySymbol" />
								<fmt:formatNumber value="${usuarioLogged.cliente.assinatura.plano.valor}" minFractionDigits="2" />&nbsp;
								<fmt:message key="porMes"/>
							</h3>
						</div>
						<div>
							<a class="btn btn-primary btn-block" href="${pageContext.request.contextPath}/assinatura/plano/migrar"><fmt:message key="migrarOutroPlano" /></a>
							<c:if test="${usuarioLogged.cliente.assinatura.status ne 'CAN'}">
				    			<button type="button" class="btn btn-default btn-block" onclick="cancelarAssinatura()"><fmt:message key="cancelarAssinatura" /></button>
				    		</c:if>
				    		<c:if test="${usuarioLogged.cliente.assinatura.status ne 'ATI'}">
				    			<button type="button" class="btn btn-default btn-block" onclick="reativarAssinatura('${usuarioLogged.cliente.assinatura.status}')"><fmt:message key="${usuarioLogged.cliente.assinatura.status eq 'STD' ? 'fazerPagamentoAgora' : 'reativarAssinatura'}" /></button>
				    		</c:if>
				    	</div>
					</div>
				</div>
				
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-7">
			
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3><fmt:message key="pagamentos" /></h3>
					</div>
				  	<div class="panel-body">
				    	<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th class="text-center"><fmt:message key="data"/></th>
										<th class="text-center"><fmt:message key="descricao"/></th>
										<th class="text-center"><fmt:message key="valor"/></th>
										<th class="text-center"><fmt:message key="status"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="assinaturaPagamento" items="${assinaturaPagamentoList}">
										<tr>
											<td class="text-center"><joda:format pattern="${patternDateTime}" value="${assinaturaPagamento.pagamento eq null ? assinaturaPagamento.criacao : assinaturaPagamento.pagamento}" /></td>
											<td class="text-center"><c:out value="${assinaturaPagamento.plano.nome}" /></td>
											<td class="text-right">
												<fmt:formatNumber type="number" value="${assinaturaPagamento.valor}" maxFractionDigits="2" minFractionDigits="2" />
											</td>
											<td class="text-center">
												<fmt:message key="AssinaturaPagamentoStatus.${assinaturaPagamento.status}" />
												<c:if test="${assinaturaPagamento.isParcial ne null and assinaturaPagamento.isParcial}">
													<span>( -<fmt:formatNumber type="number" value="${assinaturaPagamento.valorDevolvido}" maxFractionDigits="2" minFractionDigits="2" /> )</span>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
				    </div>
				</div>
			
			</div>
			
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/admin.assinatura.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>