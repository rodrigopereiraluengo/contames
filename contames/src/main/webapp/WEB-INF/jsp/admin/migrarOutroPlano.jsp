<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="migrar"/></h1>
						<p class="description"><fmt:message key="migrar.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-user"></i>
								<c:out value="${usuarioLogged.usuario.displayNome}" />
							</li>
				  			<li><fmt:message key="assinatura" /></li>
				  			<li><fmt:message key="plano" /></li>
				  			<li><fmt:message key="migrar" /></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<div class="row">
		
			<c:forEach var="plano" items="${planoList}">
				
				<!-- Plano ${plano.id} -->
				<c:set var="isMostPopular" value="${plano.id eq 2}"/>
				<div class="col-xs-12 col-sm-6 col-md-3">
				
					<div class="panel ${isMostPopular ? 'panel-primary' : 'panel-success'}">
						
						<div class="panel-heading">
							<h3><c:out value="${plano.nome}"/></h3>
							<c:if test="${isMostPopular}"><span id="lbMostPopular" class="label label-success"><fmt:message key="maisPopular"/></span></c:if>
						</div>
						
						<table class="table">
							<tr>
								<th><fmt:message key="contas"/></th>
								<td class="text-right"><fmt:formatNumber value="${plano.contas}"/></td>
							</tr>
							<tr>
								<th><fmt:message key="cartoesCredito"/></th>
								<td class="text-right"><fmt:formatNumber value="${plano.cartoesCredito}"/></td>
							</tr>
							<tr>
								<th><fmt:message key="lancamentos"/></th>
								<td class="text-right text-nowrap">
									<fmt:formatNumber value="${plano.lancamentos}"/>&nbsp;
									<fmt:message key="porMes" />
								</td>
							</tr>
							<tr>
								<th><fmt:message key="bancoDados"/></th>
								<td class="text-right">
									<fmt:formatNumber value="${plano.bancoDados}"/>
									<fmt:message key="megaBytesAbrev" />
								</td>
							</tr>
							<tr>
								<th><fmt:message key="envioArquivos"/></th>
								<td class="text-right">
									<fmt:formatNumber value="${plano.disco}"/>
									<fmt:message key="megaBytesAbrev" />
								</td>
							</tr>
							<tr>
								<th><fmt:message key="usuarios"/></th>
								<td class="text-right"><fmt:formatNumber value="${plano.usuarios}"/></td>
							</tr>
							<tr>
								<th><fmt:message key="acessoCriptografadoSSL"/></th>
								<td class="text-right"><fmt:message key="sim" /></td>
							</tr>
							<tr>
								<th><fmt:message key="backupDiario"/></th>
								<td class="text-right"><fmt:message key="sim" /></td>
							</tr>
							<tr>
								<th><fmt:message key="suporteEmail"/></th>
								<td class="text-right"><fmt:message key="sim" /></td>
							</tr>
						</table>
						<div class="panel-body">
							<c:if test="${usuarioLogged.cliente.assinatura.plano eq plano}">
								<div class="text-center">
									<span class="label label-success"><fmt:message key="planoAtual"/></span>
									<h3>
										<fmt:message key="currencySymbol" />
										<fmt:formatNumber value="${usuarioLogged.cliente.assinatura.plano.valor}" minFractionDigits="2" />&nbsp;
										<fmt:message key="porMes"/>
									</h3>
								</div>
							</c:if>
							<c:if test="${usuarioLogged.cliente.assinatura.plano ne plano}">
								
								<c:set var="isDisabled" value="${false}"/>
								
								<%-- Verifica Contas --%>
								<c:if test="${planoRecurso.contasUtil > plano.contas}">
									<c:set var="isDisabled" value="${true}"/>
								</c:if>
								
								<%-- Verifica Cartao de Credito --%>
								<c:if test="${planoRecurso.cartoesCreditoUtil > plano.cartoesCredito}">
									<c:set var="isDisabled" value="${true}"/>
								</c:if>
								
								<%-- Verifica Lancamento --%>
								<c:if test="${planoRecurso.lancamentosUtil > plano.lancamentos}">
									<c:set var="isDisabled" value="${true}"/>
								</c:if>
								
								<%-- Verifica Banco de Dados --%>
								<c:if test="${planoRecurso.bancoDadosUtil > plano.bancoDados}">
									<c:set var="isDisabled" value="${true}"/>
								</c:if>
								
								<%-- Verifica Disco --%>
								<c:if test="${planoRecurso.armazenamentoUtil > plano.disco}">
									<c:set var="isDisabled" value="${true}"/>
								</c:if>
								
								<%-- Usuarios --%>
								<c:if test="${planoRecurso.usuariosUtil > plano.usuarios}">
									<c:set var="isDisabled" value="${true}"/>
								</c:if>
								
								<a href="${pageContext.request.contextPath}/assinatura/plano/${plano.id}/migrar" class="btn btn-<c:if test="${!isDisabled}">primary</c:if><c:if test="${isDisabled}">default</c:if> btn-block"<c:if test="${isDisabled}"> disabled="disabled"</c:if>>
									<span><fmt:message key="migrar"/></span>
									<br />
									<fmt:message key="currencySymbol" />
									<fmt:formatNumber value="${plano.valor}" minFractionDigits="2" />&nbsp;
									<fmt:message key="porMes"/>
								</a>
								
							</c:if>
							
						</div>
					</div>
				
				</div>
				
			</c:forEach>
		
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/admin.migrarOutroPlano.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>