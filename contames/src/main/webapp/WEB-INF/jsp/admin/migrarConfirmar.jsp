<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">
	<tiles:putAttribute name="body">
		
		<div class="container-xmini">
		
			<div id="pnConfirmado" class="panel<c:if test="${confirmado}"> panel-success</c:if><c:if test="${!confirmado}"> panel-warning</c:if>">
			
				<div class="panel-heading">
					<h3><fmt:message key="assinatura"/></h3>
				</div>
				
				<div class="panel-body">
					<div class="text-center">
						<p id="pMsg">
							<c:if test="${successo and !confirmado}"><fmt:message key="assinatura.aguardandoConfirmacao"/></c:if>
							<c:if test="${confirmado}"><fmt:message key="assinatura.confirmada"/></c:if>
						</p>
					</div>
					<div class="text-center">
						<img src="${pageContext.request.contextPath}/resources/img/ajax-loader.gif" />
					</div>
				</div>
			
			</div>
		
		</div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/admin.migrarConfirmar.js?v${applicationVersion}"></script>
		<c:if test="${successo and !confirmado}">
			<script type="text/javascript">
				var token = '${param.token}';
				$(isConfirmado);
			</script>
		</c:if>
		<c:if test="${confirmado}">
			<script type="text/javascript">redirecionarAssinatura();</script>
		</c:if>
	</tiles:putAttribute>
	
</tiles:insertTemplate>