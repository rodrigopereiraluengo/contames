<div id="layCartaoCredito" class="panel panel-default">
					
	<div class="panel-heading">
		<div class="row">
		
			<%-- Verifica se nao possui cartoes de credito --%>
			<c:if test="${empty cartaoCreditoList}">
				<div class="col-xs-12">
					<p class="form-control-static font-bold"><fmt:message key="cartoesCredito"/></p>
				</div>
			</c:if>
		
			<%-- Verifica se possui cartoes de credito --%>
			<c:if test="${!empty cartaoCreditoList}">
				<div class="col-xs-5">
					<p class="form-control-static font-bold"><fmt:message key="cartoesCredito"/></p>
				</div>
				<div class="col-xs-7 text-right">
				
					<%-- Multiple combobox com as Cartoes de Credito --%>
					<select id="cartoesCredito" name="cartoesCredito[]" multiple="multiple" data-category="cartoesCredito" class="form-control" style="display: none">
						<c:forEach var="cartaoCredito" items="${cartaoCreditoList}">
							<option value="${cartaoCredito.id}"<c:if test="${(cartaoCreditoPrincipal ne null and cartaoCredito eq cartaoCreditoPrincipal) or (cartoesCredito ne null and f:contains(cartaoCredito.id, cartoesCredito))}"> selected="selected"</c:if>><c:out value="${cartaoCredito.nome}"/></option>
						</c:forEach>
					</select>
				
				</div>
			</c:if>			
			
		</div>		

	</div>
	
	<%-- Verifica se nao possui cartoes de credito --%>
	<c:if test="${empty cartaoCreditoList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumCartaoCreditoEncontrada"/>
			</strong>
		</div>
	</c:if>
	
	<%-- Verifica se possui cartoes de credito --%>
	<c:if test="${!empty cartaoCreditoList}">
	
		<table id="layCartaoCreditoLimite" class="table">
			<tr>
				<td><fmt:message key="limiteUtilizado" /></td>
				<td id="cartaoCreditoLimiteUtilizado" class="text-right<c:if test="${cartaoCreditoSaldo.limiteutilizado gt 0}"> font-red</c:if><c:if test="${cartaoCreditoSaldo.limiteutilizado eq 0}"> font-blue</c:if>"><fmt:formatNumber type="number" value="${cartaoCreditoSaldo.limiteutilizado}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td><fmt:message key="limiteDisp" /></td>
				<td id="cartaoCreditoLimiteDisponivel" class="text-right<c:if test="${cartaoCreditoSaldo.limitedisponivel gt 0}"> font-blue</c:if><c:if test="${cartaoCreditoSaldo.limitedisponivel eq 0}"> font-red</c:if>"><fmt:formatNumber type="number" value="${cartaoCreditoSaldo.limitedisponivel}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
		</table>
		
		<%-- Mensagem: Nenhum cartoes de credito selecionado --%>
		<div id="laySelecioneCartaoCredito" class="alert alert-success text-center cm-table-notfound" role="alert" style="display: none">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumCartaoCreditoSelecionado"/>
			</strong>
		</div>
		
	</c:if>
	
</div>