<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">

	<tiles:putAttribute name="head">
		<!--[if lte IE 8]>
        	<script src="${pageContext.request.contextPath}/resources/js/excanvas.js?v${applicationVersion}"></script>
    	<![endif]-->
    	<script src="${pageContext.request.contextPath}/resources/js/Chart.min.js?v${applicationVersion}"></script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<div class="cm-page">
		
			<div class="row">
			
				<div class="col-xs-12 col-sm-8">
					
					<div class="row">
					
						
						<!-- Lancamento Rapido -->
						<div class="col-xs-12">
						
							<form 
								id="frmLancamentoRapido" 
								action="${pageContext.request.contextPath}/lancamento/rapido"
								method="post"
								data-tipo="DES" 
								class="data-table">
								
								<input type="hidden" id="lancamentoIsBaixa" name="isBaixa" />
								<input type="hidden" id="lancamentoRapidoConta" name="lancamentoParcela.conta.id" />
								<input type="hidden" id="lancamentoRapidoBoletoTipo" name="lancamentoParcela.boletoTipo" />
								<input type="hidden" id="lancamentoRapidoBoletoNumero" name="lancamentoParcela.boletoNumero" />
								<input type="hidden" id="lancamentoRapidoCartaoCredito" name="lancamentoParcela.cartaoCredito.id" />
								<input type="hidden" id="lancamentoRapidoCartaoCreditoIsLimite" name="lancamentoParcela.isLimite" />
								
								<input type="hidden" name="lancamentoParcela.cheque.talaoCheque.id" />
								<input type="hidden" name="lancamentoParcela.cheque.banco.id" />
								<input type="hidden" name="lancamentoParcela.cheque.numero" />
								<input type="hidden" name="lancamentoParcela.cheque.nome.id" />
								<input type="hidden" name="lancamentoParcela.cheque.data" />
								<input type="hidden" name="lancamentoParcela.cheque.obs" />
								
								<input type="hidden" id="lancamentoRapidoArquivoTemp" name="lancamentoParcela.arquivo.temp" />
								<input type="hidden" id="lancamentoRapidoArquivoNome" name="lancamentoParcela.arquivo.nome" />
								<input type="hidden" id="lancamentoRapidoArquivoContentType" name="lancamentoParcela.arquivo.contentType" />
								<input type="hidden" id="lancamentoRapidoArquivoSize" name="lancamentoParcela.arquivo.size" />
								
								<input type="hidden" id="lancamentoRapidoArquivoDocTemp" name="lancamento.arquivoDoc.temp" />
								<input type="hidden" id="lancamentoRapidoArquivoDocNome" name="lancamento.arquivoDoc.nome" />
								<input type="hidden" id="lancamentoRapidoArquivoDocContentType" name="lancamento.arquivoDoc.contentType" />
								<input type="hidden" id="lancamentoRapidoArquivoDocSize" name="lancamento.arquivoDoc.size" />
								
								<div class="panel panel-default">
								
									<div class="panel-heading">
									
										<div class="row">
										
											<div class="col-xs-12">
												<p class="form-control-static font-bold"><fmt:message key="lancamentoRapido"/></p>
											</div>
											
										</div>
										
									</div>
									
									<div class="panel-body">
									
										<div class="container-fluid">
										
											
											<!-- Data / Favorecido -->
											<div class="row form-horizontal">
											
												<div class="col-xs-12 col-sm-4">
													
													<div class="form-group">
														<label class="col-sm-3 control-label text-nowrap" for="lancamentoRapidoVenc">
							  								<fmt:message key="venc_" />
							  								<span class="required">*</span>
							  							</label>
							  							<div class="col-sm-9">
							  								<div class="input-group">
								  								<input type="text" id="lancamentoRapidoVenc" name="lancamentoParcela.data" data-category="lancamentoParcela.data" class="form-control" />
								  								<span class="input-group-btn">
									  								<button class="btn btn-default" type="button" id="btLancamentoParcelaData">
									  									<i class="fa fa-calendar-o"></i>
								       								</button>
																</span>
															</div>
						  								</div>
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-8">
													<div class="form-group">
														<label class="col-sm-3 control-label text-nowrap" for="lancamentoRapidoFavorecido">
							  								<span id="lbLancamentoRapidoFav"><fmt:message key="pagarA" /></span>
							  								<span class="required">*</span>
						  								</label>
														<div class="col-sm-9">
															<div class="input-group">
										  						
										  						<input 
								  									type="text" 
								  									id="lancamentoRapidoFavorecido" 
								  									name="lancamento.favorecido.id" 
								  									class="form-control"
								  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
										    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoa&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
										    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
										    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
										    						data-category="favorecido"
									    						/>
										  						
										    					<span class="input-group-btn">
										    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
										    							<i class="fa fa-plus"></i>
								       								</button>
								       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
								       									<i class="fa fa-pencil"></i>
								       								</button>
																	<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
																		<i class="fa fa-search"></i>
								       								</button>
								   								</span>
							   								</div>
								  						</div>
													</div>
												</div>
											
											</div>
											
											
											<!-- Documento -->
											<div class="row form-horizontal">
											
												<div class="col-xs-12 col-sm-4">
													<div class="form-group">
														<label class="col-sm-3 control-label text-nowrap" for="lancamentoRapidoTipoDoc">
							  								<fmt:message key="doc" />
							  							</label>
						  								<div class="col-sm-9">
						  									<div class="input-group">
									  							<select id="lancamentoRapidoTipoDoc" name="lancamento.tipoDoc" data-category="tipoDoc" class="form-control">
									  								<option value=""><fmt:message key="selecione___"/></option>
																	<option value="NF"><fmt:message key="DocTipo.NF"/></option>
																	<option value="CF"><fmt:message key="DocTipo.CF"/></option>
																	<option value="RC"><fmt:message key="DocTipo.RC"/></option>
																	<option value="CT"><fmt:message key="DocTipo.CT"/></option>
																</select>
																<span class="input-group-btn">
							        								<button class="btn btn-default" type="button" id="btLancamentoRapidoArquivoDoc" title="<fmt:message key="arquivo"/>">
							        									<i class="fa fa-paperclip"></i>
																	</button>
							      								</span>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-8" id="layLancNumeroDoc">
													<div class="form-group">
						   								<label class="col-sm-3 control-label" for="lancamentoRapidoNumeroDoc">
							  								<fmt:message key="n_" />
							  							</label>
							  							<div class="col-sm-9">
							  								<input type="text" id="lancamentoRapidoNumeroDoc" name="lancamento.numeroDoc" data-category="numeroDoc" class="form-control" />
							  							</div>
						  							</div>
					   							</div>
											
											</div>
											
											<!-- Despesa / Receita -->
											<div class="row">
											
												<div class="col-xs-12 col-sm-4 form-group">
													<select id="lancamentoTipo" name="lancamento.tipo" class="form-control">
														<option value="DES"><fmt:message key="ItemTipo.DES"/></option>
														<option value="REC"><fmt:message key="ItemTipo.REC"/></option>
													</select>
												</div>
												
												<!-- Despesa -->
												<div class="col-xs-12 col-sm-8 form-group" id="layDespesa">
												
													<div class="input-group">
								  						<input 
						  									type="text" 
						  									id="lancamentoDes" 
						  									name="lancamentoDes.item.id" 
						  									class="form-control"
						  									data-form="${pageContext.request.contextPath}/despesa/cadastro"
								    						data-search="${pageContext.request.contextPath}/cadastros/despesas?tpl=dialog&dialogTitle=ItemTipo.DES&dialogId=ItemConsultar"
								    						data-url-auto-complete="${pageContext.request.contextPath}/item/autoComplete?tipo=DES"
								    						data-reload="${pageContext.request.contextPath}/item/reload"
								    						data-category="lancamentoItem_DES"
							    						/>
								  						
								    					<span class="input-group-btn">
								    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.despesa"/>">
								    							<i class="fa fa-plus"></i>
						       								</button>
						       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.despesa"/>">
						       									<i class="fa fa-pencil"></i>
						       								</button>
															<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.despesa"/>">
																<i class="fa fa-search"></i>
						       								</button>
						   								</span>
					   								</div>
												
												</div>
												
												<!-- Receita -->
												<div class="col-xs-12 col-sm-8 form-group" id="layReceita" style="display: none">
												
													<div class="input-group">
								  						<input 
						  									type="text" 
						  									id="lancamentoRec" 
						  									name="lancamentoRec.item.id" 
						  									class="form-control"
						  									data-form="${pageContext.request.contextPath}/receita/cadastro"
								    						data-search="${pageContext.request.contextPath}/cadastros/receitas?tpl=dialog&dialogTitle=ItemTipo.REC&dialogId=ItemConsultar"
								    						data-url-auto-complete="${pageContext.request.contextPath}/item/autoComplete?tipo=REC"
								    						data-reload="${pageContext.request.contextPath}/item/reload"
								    						data-category="lancamentoItem_REC"
							    						/>
								  						
								    					<span class="input-group-btn">
								    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.receita"/>">
								    							<i class="fa fa-plus"></i>
						       								</button>
						       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.receita"/>">
						       									<i class="fa fa-pencil"></i>
						       								</button>
															<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.receita"/>">
																<i class="fa fa-search"></i>
						       								</button>
						   								</span>
					   								</div>
												
												</div>
											
											</div>
											
											<!-- Valor / Meio -->
											<div class="row form-horizontal">
											
												<div class="col-xs-12 col-sm-4">
													<div class="form-group">
														<label class="col-sm-3 control-label text-nowrap" for="lancamentoRapidoValor">
															<fmt:message key="valor"/>
															<span class="required">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" id="lancamentoRapidoValor" name="lancamentoParcela.valor" data-category="lancamentoParcela.valor" class="form-control text-right" />
														</div>
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-8">
													<div class="form-group">
														<label class="col-sm-3 control-label text-nowrap" for="lancamentoRapidoMeio">
															<fmt:message key="meio" />
															<span class="required">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group">
																<select id="lancamentoRapidoMeio" name="lancamentoParcela.meio" data-category="lancamentoParcela.meio" class="form-control">
									  								<option value=""><fmt:message key="selecione___" /></option>
									  								<option value="CDB"><fmt:message key="MeioPagamento.CDB" /></option>
									  								<option value="CCR"><fmt:message key="MeioPagamento.CCR" /></option>
									  								<option value="INT"><fmt:message key="MeioPagamento.INT" /></option>
									  								<option value="DEA"><fmt:message key="MeioPagamento.DEA" /></option>
									  								<option value="DIN"><fmt:message key="MeioPagamento.DIN" /></option>
									  								<option value="CHQ"><fmt:message key="MeioPagamento.CHQ" /></option>
									  								<option value="TRA"><fmt:message key="MeioPagamento.TRA" /></option>
									  								<option value="DOC"><fmt:message key="MeioPagamento.DOC" /></option>
									  							</select>
									  							<span class="input-group-btn">
									  								<button class="btn btn-default" type="button" id="btLancamentoRapidoConta" title="<fmt:message key="conta"/>">
								       									<i class="fa fa-folder-open"></i>
																	</button>
																	<button class="btn btn-default" type="button" id="btLancamentoRapidoCartaoCredito" title="<fmt:message key="cartaoCredito"/>">
								       									<i class="fa fa-credit-card"></i>
																	</button>
																	<button class="btn btn-default" type="button" id="btLancamentoRapidoCheque" title="<fmt:message key="cheque"/>">
								       									<i class="fa fa-check-circle"></i>
																	</button>
																	<button class="btn btn-default" type="button" id="btLancamentoRapidoBoleto" title="<fmt:message key="codigoBarras"/>">
																		<i class="fa fa-barcode"></i>
							        								</button>
																	<button class="btn btn-default" type="button" id="btLancamentoRapidoArquivo" title="<fmt:message key="arquivo"/>">
							        									<i class="fa fa-paperclip"></i>
																	</button>
																</span>	
								  							</div>
							  							</div>
						  							</div>
												</div>
												
												
											
											</div>
										
										</div>
									
									</div>
								
									<div class="modal-footer">
									
										<button type="button" id="lancamentoRapidoBtCancelar" class="btn btn-default">
									  		<i class="fa fa-close"></i>
									  		<b><fmt:message key="cancelar"/></b>
								  		</button>
										<button type="submit" id="lancamentoRapidoBtSalvar" class="btn btn-primary">
											<i class="fa fa-save"></i>
											<b><fmt:message key="salvar"/></b>
										</button>
										<button type="button" id="lancamentoRapidoBtBaixa" class="btn btn-primary">
											<i class="fa fa-arrow-right font-red"></i>
											<b><fmt:message key="btBaixa_DES"/></b>
										</button>
									
									</div>
								
								</div>
								
							</form>
						
						</div>
					
					</div>
					
					<div class="row">
					
						
						<!-- Resumo -->
						<div class="col-xs-12">
						
							<form 
								id="frmResumo" 
								action="${pageContext.request.contextPath}/resumo" 
								class="data-table">
								
								<input type="hidden" name="subtotal" value="MES" />
								
								<div class="panel panel-default">
								
									<div class="panel-heading cm-panel-heading-toolbar">
										
										<div class="container-fluid">
										
										
											<div class="row">
												
												<div class="col-xs-12 col-sm-3">
													<div class="form-group">
														<p class="form-control-static font-bold"><fmt:message key="movimentos"/></p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-1 text-right">
													<div class="form-group">
														<input type="checkbox" id="isCCred" checked="checked" value="true" />
													</div>
												</div>
												<div class="col-xs-12 col-sm-8 text-right">
													<div class="form-group">
														<div class="btn-group" data-toggle="buttons">
														  	
														  	<label class="btn btn-default">
														    	<input type="radio" name="periodoData" id="periodoDataHoje" value="HOJE"> <fmt:message key="PeriodoData.HOJE"/>
														  	</label>
														  	
														  	<label class="btn btn-default active">
														    	<input type="radio" name="periodoData" id="periodoDataSemanaAtual" value="SEMANA_ATUAL" checked="checked"> <fmt:message key="PeriodoData.SEMANA_ATUAL"/>
														  	</label>
														  	
														  	<label class="btn btn-default">
														    	<input type="radio" name="periodoData" id="periodoDataMesAtual" value="MES_ATUAL"> <fmt:message key="PeriodoData.MES_ATUAL"/>
														  	</label>
														  	
														  	<label class="btn btn-default">
														    	<input type="radio" name="periodoData" id="periodoDataAnoAtual" value="ANO_ATUAL"> <fmt:message key="PeriodoData.ANO_ATUAL"/>
														  	</label>
														
														</div>
													</div>
												</div>
											
											</div>
										
										
										</div>
									</div>
								
									<div class="result search-table">
										
										<jsp:include page="resumo_table.jsp" />
										
									</div>
									
								</div>
							
							</form>
						
						</div>
					
					</div>
					
				</div>
				
				<div class="col-xs-12 col-sm-4">
				
					<!-- Contas -->
					<jsp:include page="contas.jsp"></jsp:include>
					
					<!-- Cartoes de Credito -->
					<jsp:include page="cartoesCredito.jsp"></jsp:include>
					
					
					<div id="pnAgenda" class="panel panel-default">
					
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-5">
									<p class="form-control-static font-bold"><fmt:message key="agenda"/></p>
								</div>
								<div class="col-xs-7">
									<select id="usuarios" name="usuarios[]" multiple="multiple" data-category="usuarios" class="form-control" style="display: none">
										<c:forEach var="usuario" items="${usuarioList}">
											<option value="${usuario.id}"<c:if test="${usuarioLogged.usuario eq usuario}"> selected="selected"</c:if>><c:out value="${usuario.displayNome}"/></option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-12">
									<h3 class="text-center" id="lbAgendaAnoMes"><fmt:message key="agenda"/></h3>
								</div>
							</div>
						</div>
						
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-5">
									<div class="btn-group">
							  			<button type="button" class="btn btn-default bt-new">
							  				<i class="fa fa-plus"></i>
							  			</button>
							  			<button type="button" class="btn btn-default bt-edit" disabled="disabled">
							  				<i class="fa fa-pencil"></i>
							  			</button>
							  			<button type="button" class="btn btn-default bt-delete" disabled="disabled">
							  				<i class="fa fa-trash"></i>
							  			</button>
									</div>
								</div>
								<div class="col-xs-7">
									<div class="pull-right form-inline">
										<div class="btn-group">
											<button class="btn btn-default" data-calendar-view="year"><fmt:message key="ano" /></button>
											<button class="btn btn-default active" data-calendar-view="month"><fmt:message key="mes" /></button>
											<button class="btn btn-default" data-calendar-view="week"><fmt:message key="semana" /></button>
											<button class="btn btn-default" data-calendar-view="day"><fmt:message key="dia" /></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-body">
						
							<div id="agenda"></div>
						
						</div>
						
						<div class="panel-footer">
						
							<div class="text-center form-inline">
								<div class="btn-group">
									<button class="btn btn-default" data-calendar-nav="prev"><i class="fa fa-backward"></i> <fmt:message key="ant" /></button>
									<button class="btn btn-default" data-calendar-nav="today"><fmt:message key="hoje" /></button>
									<button class="btn btn-default" data-calendar-nav="next"><fmt:message key="prox" /> <i class="fa fa-forward"></i></button>
								</div>
							</div>
						
						</div>
					
					</div>
				
				</div>
			
			</div>
			
		</div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.calendar.min.js?v${applicationVersion}"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.calendar.pt-BR.js?v${applicationVersion}"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/admin.resumo.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>