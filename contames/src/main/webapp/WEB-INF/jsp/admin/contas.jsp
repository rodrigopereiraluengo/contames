<div id="layContas" class="panel panel-default">
						
	<div class="panel-heading">
		
		<div class="row">
		
			<%-- Verifica se nao possui contas --%>
			<c:if test="${empty contaList}">
				<div class="col-xs-12">
					<p class="form-control-static font-bold"><fmt:message key="contas"/></p>
				</div>
			</c:if>
			
			<%-- Verifica se possui contas --%>
			<c:if test="${!empty contaList}">
				<div class="col-xs-5">
					<p class="form-control-static font-bold"><fmt:message key="contas"/></p>
				</div>
				<div class="col-xs-7 text-right">
					
					<%-- Multiple combobox com as Contas --%>
					<select id="contas" name="contas[]" multiple="multiple" data-category="contas" class="form-control" style="display: none">
						<c:forEach var="conta" items="${contaList}">
							<option value="${conta.id}"<c:if test="${(contaPrincipal ne null and conta eq contaPrincipal) or (contas ne null and f:contains(conta.id, contas))}"> selected="selected"</c:if>><c:out value="${conta.nome}"/></option>
						</c:forEach>
					</select>
				
				</div>
			</c:if>
		</div>
	</div>
	
	<%-- Verifica se nao possui contas --%>
	<c:if test="${empty contaList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumaContaEncontrada"/>
			</strong>
		</div>
	</c:if>
	
	<%-- Verifica se possui contas --%>
	<c:if test="${!empty contaList}">
		
		<table id="layContaSaldo" class="table">
			<tr>
				<td><fmt:message key="saldoAtual" /></td>
				<td id="contaSaldo" class="text-right<c:if test="${contaSaldo.saldo lt 0}"> font-red</c:if><c:if test="${contaSaldo.saldo gt 0}"> font-blue</c:if>"><fmt:formatNumber type="number" value="${contaSaldo.saldo}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td><fmt:message key="limiteDisp" /></td>
				<td id="contaSaldoLimiteDisp" class="text-right<c:if test="${contaSaldo.saldolimitedisp lt contaSaldo.saldolimite}"> font-red</c:if><c:if test="${contaSaldo.saldolimitedisp eq contaSaldo.saldolimite}"> font-blue</c:if>"><fmt:formatNumber type="number" value="${contaSaldo.saldolimitedisp}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
		</table>
		
		<%-- Mensagem: Nenhuma conta selecionada --%>
		<div id="laySelecioneConta" class="alert alert-success text-center cm-table-notfound" role="alert" style="display: none">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumaContaSelecionada"/>
			</strong>
		</div>
		
	</c:if>
	
</div>