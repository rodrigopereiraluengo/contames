<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="planos" /> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="planos"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="planos" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="row">
		
			<c:forEach var="plano" items="${planoList}">
				
				<!-- Plano ${plano.id} -->
				<c:set var="isMostPopular" value="${plano.id eq 2}"/>
				<div class="col-xs-12 col-sm-6 col-md-3">
				
					<div class="panel ${isMostPopular ? 'panel-primary' : 'panel-success'}">
						
						<div class="panel-heading">
							<h3><c:out value="${plano.nome}"/></h3>
							<c:if test="${isMostPopular}"><span id="lbMostPopular" class="label label-success"><fmt:message key="maisPopular"/></span></c:if>
						</div>
						
						<table class="table">
							<tr>
								<th><fmt:message key="contas"/></th>
								<td class="text-right"><fmt:formatNumber value="${plano.contas}"/></td>
							</tr>
							<tr>
								<th><fmt:message key="cartoesCredito"/></th>
								<td class="text-right"><fmt:formatNumber value="${plano.cartoesCredito}"/></td>
							</tr>
							<tr>
								<th><fmt:message key="lancamentos"/></th>
								<td class="text-right text-nowrap">
									<fmt:formatNumber value="${plano.lancamentos}"/>&nbsp;
									<fmt:message key="porMes" />
								</td>
							</tr>
							<tr>
								<th><fmt:message key="bancoDados"/></th>
								<td class="text-right">
									<fmt:formatNumber value="${plano.bancoDados}"/>
									<fmt:message key="megaBytesAbrev" />
								</td>
							</tr>
							<tr>
								<th><fmt:message key="envioArquivos"/></th>
								<td class="text-right">
									<fmt:formatNumber value="${plano.disco}"/>
									<fmt:message key="megaBytesAbrev" />
								</td>
							</tr>
							<tr>
								<th><fmt:message key="usuarios"/></th>
								<td class="text-right"><fmt:formatNumber value="${plano.usuarios}"/></td>
							</tr>
							<tr>
								<th><fmt:message key="acessoCriptografadoSSL"/></th>
								<td class="text-right"><fmt:message key="sim" /></td>
							</tr>
							<tr>
								<th><fmt:message key="backupDiario"/></th>
								<td class="text-right"><fmt:message key="sim" /></td>
							</tr>
							<tr>
								<th><fmt:message key="suporteEmail"/></th>
								<td class="text-right"><fmt:message key="sim" /></td>
							</tr>
						</table>
						<div class="panel-body">
							<a href="${pageContext.request.contextPath}/plano/${plano.id}/assinatura" class="btn btn-primary btn-block">
								<span><fmt:message key="assinar"/></span>
								<br />
								<fmt:message key="de" />&nbsp;
								<span style="text-decoration: line-through;">
									<fmt:message key="currencySymbol" />
									<fmt:formatNumber value="${(plano.valor * 2) + .01}" minFractionDigits="2" />&nbsp;
								</span>
								<br />
								<fmt:message key="por" />&nbsp;
								<fmt:message key="currencySymbol" />
								<fmt:formatNumber value="${plano.valor}" minFractionDigits="2" />&nbsp;/&nbsp;
								<fmt:message key="mes"/>
							</a>
						</div>
					</div>
				
				</div>
				
			</c:forEach>
		
		</div>
	
	</tiles:putAttribute>
	
</tiles:insertTemplate>