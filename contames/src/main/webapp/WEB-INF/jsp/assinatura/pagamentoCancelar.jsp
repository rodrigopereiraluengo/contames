<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="body">
		
		<div class="container-xmini">
		
			<div id="pnCancel" class="panel panel-warning">
			
				<div class="panel-heading">
					<h3><fmt:message key="assinatura"/></h3>
				</div>
				
				<div class="panel-body">
					<p id="pMsg" class="text-center"><fmt:message key="assinatura.pagamento.cancelar.pagamentoNaoConfirmado"/></p>
				</div>
				
				<div class="panel-footer">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<a href="<env:get key="paypal.checkoutURL"/>${assinaturaPagamento.token}" class="btn btn-success btn-block"><fmt:message key="sim"/></a>
						</div>
						<div class="col-xs-12 col-sm-6">
							<a href="/${pageContext.request.contextPath}" class="btn btn-danger btn-block"><fmt:message key="nao"/></a>
						</div>
					</div>
				</div>
			
			</div>
		
		</div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/assinatura.pagamentoCancelar.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>