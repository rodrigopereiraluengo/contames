<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="confirmar" /> | </tiles:putAttribute>
	<tiles:putAttribute name="body">
		
		<div class="container-xmini">
		
			<div id="pnConfirmado" class="panel<c:if test="${confirmado}"> panel-success</c:if><c:if test="${!confirmado}"> panel-warning</c:if>">
			
				<div class="panel-heading">
					<h3><fmt:message key="assinatura"/></h3>
				</div>
				
				<div class="panel-body">
					<div class="text-center">
						<p id="pMsg">
							<c:if test="${successo and !confirmado}"><fmt:message key="assinatura.aguardandoConfirmacao"/></c:if>
							<c:if test="${confirmado}"><fmt:message key="assinatura.confirmada"/></c:if>
						</p>
					</div>
					<div class="text-center">
						<img src="${pageContext.request.contextPath}/resources/img/loading.gif" />
					</div>
				</div>
			
			</div>
		
		</div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/assinatura.confirmar.js?v${applicationVersion}"></script>
		<c:if test="${successo and !confirmado}">
			<script type="text/javascript">
				var token = '${param.token}';
				$(isConfirmado);
			</script>
		</c:if>
		<c:if test="${confirmado}">
			<script type="text/javascript">redirecionarLogin();</script>
		</c:if>
	</tiles:putAttribute>
	
</tiles:insertTemplate>