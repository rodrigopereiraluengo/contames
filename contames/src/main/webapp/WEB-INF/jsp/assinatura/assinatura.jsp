<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="assinatura" /> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="assinatura"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
							<li><c:out value="${plano.nome}" /></li>
				  			<li><fmt:message key="assinatura" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="row">
			
			<div class="col-xs-12 col-sm-9">
				
				<form id="frmAssinatura" action="${pageContext.request.contextPath}/plano/${plano.id}/assinatura" method="post">
					
					<div class="panel panel-default">
					
						<div class="panel-heading">
							<h3><fmt:message key="cadastro"/></h3>
						</div>
						
						<div class="panel-body">
						
							<div class="row">
								
								<div class="col-xs-12 col-sm-8 form-group">
									<label for="email" class="control-label">
										<fmt:message key="email"/>
										<span class="required">*</span>
									</label>
									<input type="email" id="email" name="cliente.email" maxlength="127" class="form-control" data-category="email codigoVerificador.email" />
								</div>
								
								<div class="col-xs-12 col-sm-4 form-group">
									<label for="codigoVerificador" class="control-label">
										<fmt:message key="codigoVerificador"/>
										<span class="required">*</span>
									</label>
									<input type="text" id="codigoVerificador" name="codigoVerificador" class="form-control" maxlength="6" data-category="codigoVerificador" />
								</div>
							
							</div>
							
							<div class="row">
							
								<div class="col-xs-12 col-sm-6 form-group">
									<label for="senha" class="control-label">
										<fmt:message key="senha" />
										<span class="required">*</span>
									</label>
									<input type="password" id="senha" name="cliente.senha" maxlength="20" class="form-control" data-category="senha" />
								</div>
								
								<div class="col-xs-12 col-sm-6 form-group">
									<label for="confirmeSenha" class="control-label">
										<fmt:message key="confirmeSenha"/>
										<span class="required">*</span>
									</label>
									<input type="password" id="confirmeSenha" name="confirmeSenha" maxlength="20" class="form-control" data-category="confirmeSenha assinatura.confirmeSenha" />
								</div>
							
							</div>
							
								
						</div>
						
					</div>
					
					<div class="panel panel-default">
					
						<div class="panel-heading">
							<h3><fmt:message key="termosUsoPrivacidade" /></h3>
						</div>
					
						<div class="panel-body" style="overflow: auto; height: 130px;">
						
							<h4>1. Aceita��o dos Termos do Servi�o</h4>
							
							<p>O presente servi�o oferecido pelo contames.com.br(doravante denominado sistema) oferece a voc�(doravante denominado usu�rio) no formato de aceita��o dos Termos de uso e Privacidade.</p>
							
							<h4>2. Descri��o do Servi�o</h4>
							
							<p>O servi�o comp�e-se de um sistema web acess�vel atrav�s de e-mail e senha cadastrados no momento da assinatura e pagamento mensal que permite a organiza��o financeira de uso pessoal ou empresarial.</p>
							
							<h4>3. Obriga��es de Registro do Usu�rio</h4>
							
							<p>O usu�rio � respons�vel pelas informa��es de seu cadastro, e responde totalmente pela exatid�o e veracidade das mesmas e de qualquer consequ�ncia que uma eventual inexatid�o possa provocar.</p>
							
							<h4>4. Pol�tica de Privacidade do contames.com.br</h4>
							
							<p>No ato da assinatura, o usu�rio afirma e concorda em liberar para uso do sistema de toda e qualquer informa��o inserida pelo usu�rio. O sistema poder� usar tais informa��es de qualquer maneira que julgar �til e para qualquer fim permitido pela lei. Isso inclui a��es de marketing, an�lise e distribui��o dos dados a empresas terceiras que tamb�m venham a utilizar dessas informa��es para fins sempre dentro da legalidade. O sistema assume adicionalmente o compromisso de n�o divulgar publicamente as informa��es de seus usu�rios, definido como tal a exposi��o de forma transparente e sem restri��es para quem quiser obter tais informa��es.</p>
							
							<h4>5. Conta do Usu�rio, senha e seguran�a</h4>
							
							<p>O usu�rio � respons�vel por criar sua assinatura e manter atualizados os dados cadastrais. A senha associada � de total responsabilidade do usu�rio, estando o sistema unicamente responsabilizado em permitir a atualiza��o da senha atrav�s do endere�o de e-mail cadastrado pelo usu�rio. O sistema usar� de todos os meios a seu alcance para garantir a seguran�a e privacidade de tais senhas em favor do usu�rio.</p>
							
							<h4>6. Modifica��es no Servi�o</h4>
							
							<p>Fica o usu�rio ciente e concorda que o sistema poder� promover atualiza��es no servi�o a qualquer momento em que julgar necess�rio, sem obrigatoriedade de prover comunica��o pr�via ou posterior ao usu�rio.</p>
							
							<h4>7. Cancelamento por parte do usu�rio</h4>
							
							<p>Fica o usu�rio ciente e livre para executar o cancelamento de sua assinatura a qualquer momento que julgar necess�rio, tendo que para isso executar a a��o de cancelamento atrav�s do menu Assinatura no sistema.</p>
							
							<h4>8. Cancelamento por parte do sistema</h4>
							
							<p>Fica o sistema livre para executar o cancelamento da assinatura do usu�rio a qualquer momento caso ocorra ilegalidade por parte do usu�rio, tendo que para isso n�o avisar o usu�rio previamente.</p>
							
							<h4>9. Limita��o de Responsabilidade</h4>
							
							<p>Fica o sistema e seus s�cios, administradores e funcion�rios isentos de qualquer responsabilidade vinda do uso dos servi�os pelos usu�rios finais. A finalidade deste servi�o � unicamente prover acesso ao sistema que ajuda a organizar finan�as pessoais, quaisquer dados inseridos pelo usu�rio, e qualquer decis�o decorrente deste acompanhamento por parte do usu�rio � de total responsabilidade do mesmo.</p>
							
							<h4>10. Suporte ao usu�rio</h4>
							
							<p>Fica estabelecido que o sistema prover� suporte ao usu�rio atrav�s apenas dos e-mails suporte@contames.com.br e contato@contames.com.br, a um prazo m�nimo de at� 24(Vinte e Quatro) horas podendo se estender a 48(Quarenta e oito Horas) de Segunda a Sexta-Feira, apenas dias �teis.</p>
							
							<h4>11. Avisos</h4>
							
							<p>Fica estabelecido que qualquer comunicado de cunho mais oficioso ser� enviado atrav�s de mensagem de correio eletr�nico para o endere�o cadastrado pelo pr�prio usu�rio no ato de assinatura ou posteriormente atualizado pelo mesmo.</p>
						
						</div>
						
						<div class="panel-footer">
						
							<div class="row">
								<div class="form-group col-xs-12 checkbox">
									<label>
										<input type="checkbox" name="isAceitoTermos" value="true" data-category="assinatura.isAceitoTermos" />
										<fmt:message key="aceitaTermos" />
									</label>
								</div>
							</div>
						
						</div>
						
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3><fmt:message key="formaPagamento" /></h3>
						</div>
					
						<div class="panel-body">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label>
												<input type="radio" name="landingPageType" value="BILLING" data-category="assinatura.landingPageType" />
												<img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png" alt="<fmt:message key="LandingPageType.BILLING"/>" />
											</label>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label>
												<input type="radio" name="landingPageType" value="LOGIN" data-category="assinatura.landingPageType" />
												<img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_150x38.png" alt="<fmt:message key="LandingPageType.LOGIN"/>" />
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-footer">
						
							<button type="submit" id="btSubmit" class="btn btn-success btn-block" disabled="disabled">
								<i class="fa fa-check"></i>
								<b><fmt:message key="prosseguir" /></b>
							</button>
						
						</div>
					</div>
				</form>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-3">
				
				<c:set var="isMostPopular" value="${plano.id eq 2}"/>
				<div class="panel ${isMostPopular ? 'panel-primary' : 'panel-success'}">
						
					<div class="panel-heading">
						<h3><c:out value="${plano.nome}"/></h3>
						<c:if test="${isMostPopular}"><span class="label label-success"><fmt:message key="maisPopular"/></span></c:if>
					</div>
					
					<table class="table">
						<tr>
							<th><fmt:message key="contas"/></th>
							<td class="text-right"><fmt:formatNumber value="${plano.contas}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="cartoesCredito"/></th>
							<td class="text-right"><fmt:formatNumber value="${plano.cartoesCredito}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="lancamentos"/></th>
							<td class="text-right text-nowrap">
								<fmt:formatNumber value="${plano.lancamentos}"/>&nbsp;
								<fmt:message key="porMes" />
							</td>
						</tr>
						<tr>
							<th><fmt:message key="bancoDados"/></th>
							<td class="text-right">
								<fmt:formatNumber value="${plano.bancoDados}"/>
								<fmt:message key="megaBytesAbrev" />
							</td>
						</tr>
						<tr>
							<th><fmt:message key="envioArquivos"/></th>
							<td class="text-right">
								<fmt:formatNumber value="${plano.disco}"/>
								<fmt:message key="megaBytesAbrev" />
							</td>
						</tr>
						<tr>
							<th><fmt:message key="usuarios"/></th>
							<td class="text-right"><fmt:formatNumber value="${plano.usuarios}"/></td>
						</tr>
						<tr>
							<th><fmt:message key="acessoCriptografadoSSL"/></th>
							<td class="text-right"><fmt:message key="sim" /></td>
						</tr>
						<tr>
							<th><fmt:message key="backupDiario"/></th>
							<td class="text-right"><fmt:message key="sim" /></td>
						</tr>
						<tr>
							<th><fmt:message key="suporteEmail"/></th>
							<td class="text-right"><fmt:message key="sim" /></td>
						</tr>
					</table>
					<div class="panel-footer text-center">
						<h3>
							<fmt:message key="currencySymbol" />
							<fmt:formatNumber value="${plano.valor}" minFractionDigits="2" />&nbsp;
							<fmt:message key="porMes"/>
						</h3>
					</div>
				</div>
				
			</div>
		
		</div>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/assinatura.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>