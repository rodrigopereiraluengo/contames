<ul>
	<c:if test="${pessoaList eq null or empty pessoaList}">
		<c:if test="${query eq null}"><li class="text-info"><fmt:message key="autoComplete.typeanyForStartSeatch"/></li></c:if>
		<c:if test="${query ne null}"><li class="text-danger autoComplete-notFound"><fmt:message key="nenhumRegistroEncontrado"/></li></c:if>
	</c:if>
	<c:if test="${pessoaList ne null and !empty pessoaList}">
		<c:forEach var="pessoa" items="${pessoaList}">
			<c:if test="${pessoaJ eq null and pessoa.pessoatipo eq 'J'}">
				<c:set var="pessoaJ" value="true"/>
				<li class="text-info"><fmt:message key="PessoaTipo.J"/></li>
			</c:if>
			<c:if test="${pessoaF eq null and pessoa.pessoatipo eq 'F'}">
				<c:set var="pessoaF" value="true"/>
				<li class="text-info"><fmt:message key="PessoaTipo.F"/></li>
			</c:if>
			<li data-id="${pessoa.id}">${pessoa.nome}</li>
		</c:forEach>
	</c:if>
</ul>