<c:if test="${param.tpl eq null}">
	<c:set var="tpl" value="admin"/>
</c:if>
<c:if test="${param.tpl ne null}">
	<c:set var="tpl" value="${param.tpl}"/>
</c:if>
<tiles:insertTemplate template="/WEB-INF/jsp/tpl/${tpl}.jsp">

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<c:if test="${isUsuario}">
							<h1 class="title"><fmt:message key="cadastroDe.usuarios"/></h1>
							<p class="description"><fmt:message key="cadastroDe.usuarios.description"/></p>
						</c:if>
						<c:if test="${!isUsuario}">
							<h1 class="title"><fmt:message key="cadastroDe.pessoas"/></h1>
							<p class="description"><fmt:message key="cadastroDe.pessoas.description"/></p>
						</c:if>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<c:if test="${isUsuario}">
							<ol class="breadcrumb">
								<li>
									<i class="fa fa-user"></i>
									<span id="breadcrumbUsuario"><c:out value="${usuarioLogged.cliente.displayNome}" /></span>
								</li>
					  			<li><fmt:message key="usuarios" /></li>
					  		</ol>
						</c:if>
						<c:if test="${!isUsuario}">
							<ol class="breadcrumb">
								<li>
									<i class="fa fa-home"></i>
									<fmt:message key="resumo" />
								</li>
					  			<li><fmt:message key="cadastros" /></li>
					  			<li><fmt:message key="pessoas"/></li>
							</ol>
						</c:if>
					</div>
				
				</div>
			</div>
		</div>
		
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		
	
		<form 
			id="frmPessoaConsultar" 
			action="${pageContext.request.contextPath}${requestURI}"
			data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=${param.dialogDetalhe}&pessoaTipo=${param.pessoaTipo}&categoria=${param.categoria}&isUsuario=${isUsuario}"
			data-delete="${pageContext.request.contextPath}/pessoa/excluir"
			data-usuario="${isUsuario}"
			class="data-table">
			
			<div class="panel panel-default">
			
				<input type="hidden" name="pessoaTipo" value="${param.pessoaTipo}" />
				<input type="hidden" name="categoria" value="${param.categoria}" />
				<input type="hidden" name="status" value="${param.status}" />
				
				<div class="panel-heading cm-panel-heading-toolbar">
					
					<div class="container-fluid">
						<div class="row">
					
							<div class="col-xs-12 col-sm-4 form-group">
					  			<div class="btn-group">
					  				<button type="button" class="btn btn-default bt-new">
					  					<i class="fa fa-plus"></i>
				  						<c:if test="${!isUsuario}">
					  						<fmt:message key="nova" />
					  					</c:if>
					  					<c:if test="${isUsuario}">
					  						<fmt:message key="novo" />
					  					</c:if>
				  					</button>
					  				<button type="button" class="btn btn-default bt-edit" disabled="disabled">
					  					<i class="fa fa-pencil"></i>
					  					<fmt:message key="editar" />
				  					</button>
					  				<button type="button" class="btn btn-default bt-delete" disabled="disabled">
					  					<i class="fa fa-trash"></i>
					  					<fmt:message key="excluir" />
				  					</button>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
								<div class="input-group">
									<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<i class="fa fa-search"></i>
										</button>
									</span>
					    		</div>			
							</div>
						
						</div>
					</div>
				</div>
				
				<div class="result search-table">
					<jsp:include page="/WEB-INF/jsp/pessoa/consultar_table.jsp" />	   			
	   			</div>
			
			</div>
		</form>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pessoa.consultar.js?v${applicationVersion}"></script>
	
	</tiles:putAttribute>
	
</tiles:insertTemplate>