<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<c:if test="${isUsuario}"><fmt:message key="nenhumUsuarioEncontrado"/></c:if>
			<c:if test="${!isUsuario}"><fmt:message key="nenhumaPessoaEncontrada"/></c:if>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
	
		<c:set var="path" value="${f:removeStart(requestURI, '/')}"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="pessoa" column="pessoaTipo" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="categoria" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="razaoNome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="sexo" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="fantasiaApelido" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" column="cnpjCpf" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" column="cep" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="8" column="cidade" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="9" column="uf" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="10" column="fone1" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyPessoa">
				<c:forEach var="pessoa" items="${pessoaList}">
					<tr 
						data-id="${pessoa['id']}"
						data-category="pessoa_${pessoa['id']}" 
						<c:if test="${param.id ne null and pessoa['id'] eq param.id}"> class="active"</c:if>>
						<td class="text-center<c:if test="${f:orderByContains('pessoaTipo', orderByList)}"> font-bold</c:if>">${pessoa['pessoatipo']}</td>
						<td class="text-center<c:if test="${f:orderByContains('categoria', orderByList)}"> font-bold</c:if>"><c:out value="${pessoa['categoria']}"/></td>
						<td class="text-center<c:if test="${pessoa['_status'] eq 'INA'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${pessoa['status']}</td>
						<td<c:if test="${f:orderByContains('razaoNome', orderByList)}"> class="font-bold"</c:if>><c:out value="${pessoa['razaonome']}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('sexo', orderByList)}"> font-bold</c:if>">${pessoa['sexo']}</td>
						<td<c:if test="${f:orderByContains('fantasiaApelido', orderByList)}"> class="font-bold"</c:if>><c:out value="${pessoa['fantasiaapelido']}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('cnpjCpf', orderByList)}"> font-bold</c:if>"><c:out value="${pessoa['cnpjcpf']}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('cep', orderByList)}"> font-bold</c:if>"><c:out value="${pessoa['cep']}"/></td>
						<td<c:if test="${f:orderByContains('cidade', orderByList)}"> class="font-bold"</c:if>><c:out value="${pessoa['cidade']}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('uf', orderByList)}"> font-bold</c:if>"><c:out value="${pessoa['uf']}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('fone1', orderByList)}"> font-bold</c:if>"><c:out value="${pessoa['fone1']}"/></td>
						<td class="text-center">
							
							<c:if test="${pessoa['email'] ne null}">
								<a class="btn btn-default btn-xs" href="mailto:${pessoa['email']}" role="button" title="<fmt:message key="email"/>: ${pessoa['email']}">
									<i class="fa fa-envelope"></i>
								</a>
							</c:if>
							
							<c:if test="${pessoa['site'] ne null}">
								<a class="btn btn-default btn-xs" href="${pessoa['site']}" role="button" target="_blank" title="<fmt:message key="site"/>">
									<i class="fa fa-external-link"></i>
								</a>
							</c:if>
							
							<c:if test="${pessoa['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(pessoa['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
		</div>
 			
		<div class="panel-footer">
 			<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
  			<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
		</div>
				
</c:if>