<%@page trimDirectiveWhitespaces="true" contentType="application/json; charset=ISO-8859-1" %>
<json:array var="pessoa" items="${pessoaList}">
	<json:object>
		<json:property name="id" value="${pessoa.id}" />			
		<json:property name="name" value="${pessoa.displayNome}" />
	</json:object>
</json:array>