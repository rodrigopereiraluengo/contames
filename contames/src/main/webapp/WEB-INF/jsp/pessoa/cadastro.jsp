<div id="dialogPessoaCadastro" class="modal" style="display: none" data-dialog-detalhe="${param.dialogDetalhe}">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmPessoaCadastro" data-usuario="${isUsuario}">
			
				<input type="hidden" name="isUsuario" value="${isUsuario}" />
				<input type="hidden" name="viewid" value="${viewid}" />
				
				<c:if test="${pessoa eq null or pessoa.pessoaTipo eq 'J'}">
				
					<fmt:message var="razaoNome" key="razaoSocial" />
					<c:set var="input_razaoNome" value="razaoSocial" />
					<c:if test="${pessoa ne null}">
						<c:set var="value_razaoNome" value="${pessoa.razaoSocial}" />
					</c:if>			
					
					<fmt:message var="fantasiaApelido" key="fantasia" />
					<c:set var="input_fantasiaApelido" value="fantasia" />
					<c:if test="${pessoa ne null}">
						<c:set var="value_fantasiaApelido" value="${pessoa.fantasia}" />
					</c:if>
					
					<fmt:message var="cnpjCpf" key="cnpj" />
					<c:set var="input_cnpjCpf" value="cnpj" />
					<c:if test="${pessoa ne null}">
						<c:set var="value_cnpjCpf" value="${pessoa.cnpj}" />
					</c:if>
					
					<fmt:message var="inscEstRg" key="inscEst" />
					<c:set var="input_inscEstRg" value="inscEst" />
					<c:if test="${pessoa ne null}">
						<c:set var="value_inscEstRg" value="${pessoa.inscEst}" />
					</c:if>
					
					<fmt:message var="inscMunTituloEleitor" key="inscMun" />
					<c:set var="input_inscMunTituloEleitor" value="inscMun" />
					<c:if test="${pessoa ne null}">
						<c:set var="value_inscMunTituloEleitor" value="${pessoa.inscMun}" />
					</c:if>
				
				</c:if>
			
				<c:if test="${pessoa ne null and pessoa.pessoaTipo eq 'F'}">
				
					<fmt:message var="razaoNome" key="nome" />
					<c:set var="input_razaoNome" value="nome" />
					<c:set var="value_razaoNome" value="${pessoa.nome}" />
					
					<fmt:message var="fantasiaApelido" key="apelido" />
					<c:set var="input_fantasiaApelido" value="apelido" />
					<c:set var="value_fantasiaApelido" value="${pessoa.apelido}" />
					
					<fmt:message var="cnpjCpf" key="cpf" />
					<c:set var="input_cnpjCpf" value="cpf" />
					<c:set var="value_cnpjCpf" value="${pessoa.cpf}" />
					
					<fmt:message var="inscEstRg" key="rg" />
					<c:set var="input_inscEstRg" value="rg" />
					<c:set var="value_inscEstRg" value="${pessoa.rg}" />
					
					<fmt:message var="inscMunTituloEleitor" key="tituloEleitor" />
					<c:set var="input_inscMunTituloEleitor" value="tituloEleitor" />
					<c:set var="value_inscMunTituloEleitor" value="${pessoa.tituloEleitor}" />
				
				</c:if>
		
				<c:if test="${pessoa ne null}">
					<input type="hidden" name="pessoa.id" value="${pessoa.id}" />
				</c:if>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogPessoaCadastroTitle"><fmt:message key="pessoa"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div class="container-fluid" data-lay-detalhe="minus" style="display: none">
					
						<div class="row">
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="pessoaTipo"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="pessoaCnpj"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="pessoaFone1"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="pessoaEmail"></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 form-group" data-input-detalhe="pessoaRazaoSocial"></div>
							<div class="col-xs-12 col-sm-6 form-group" data-input-detalhe="pessoaFantasia"></div>
						</div>
					
					</div>
					
					<div class="container-fluid" data-lay-detalhe="plus">
		
						<div class="row">
								  						
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="pessoaTipo">
									
								<label class="control-label" for="pessoaTipo">
									<fmt:message key="tipo" />
									<span class="required">*</span>
								</label>
								<c:if test="${empty param.pessoaTipo}">
									<select id="pessoaTipo" name="pessoa.pessoaTipo" data-category="pessoaTipo" class="form-control required">
										<option value="J"<c:if test="${pessoa ne null and pessoa.pessoaTipo eq 'J'}"> selected="selected"</c:if>><fmt:message key="PessoaTipo.J" /></option>
										<option value="F"<c:if test="${pessoa ne null and pessoa.pessoaTipo eq 'F'}"> selected="selected"</c:if>><fmt:message key="PessoaTipo.F" /></option>
									</select>
								</c:if>
								<c:if test="${!empty param.pessoaTipo}">
									<input type="hidden" id="pessoaTipo" name="pessoa.pessoaTipo" value="${param.pessoaTipo}" />
									<p class="form-control-static"><fmt:message key="PessoaTipo.${pessoa.pessoaTipo}" /></p>
								</c:if>
									
							</div>
									
							<div class="col-xs-12 col-sm-6 form-group">
									
								<label class="control-label" for="pessoaCategoria">
									<fmt:message key="categoria" />
								</label>
								<div<c:if test="${!empty param.categoria}"> style="display: none"</c:if> class="input-group">
									<select 
			    						id="pessoaCategoria" 
			    						name="pessoa.categoria.id" 
			    						class="form-control"
			    						data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=PESSOA_CATEGORIA"
			    						data-search="${pageContext.request.contextPath}/descricao/consultar?descricaoTipo=PESSOA_CATEGORIA"
			    						data-load-options="${pageContext.request.contextPath}/descricao/carregaSelect?descricaoTipo=PESSOA_CATEGORIA"
			    						data-category="categoria">
			    						<c:if test="${categoriaList eq null or empty categoriaList}">
			    							<option value=""><fmt:message key="nenhum.PESSOA_CATEGORIA.encontrado"/></option>
			    						</c:if>
			    						<c:if test="${categoriaList ne null and !empty categoriaList}">
			    							<option value=""><fmt:message key="selecione___"/></option>
			    							<c:forEach var="categoria" items="${categoriaList}">
			    								<option value="${categoria.id}"<c:if test="${pessoa ne null and pessoa.categoria eq categoria or param.categoria eq categoria.nome}"> selected="selected"</c:if>><c:out value="${categoria.nome}"/></option>
			    							</c:forEach>
			    						</c:if>
			    					</select>
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.categoria" />">
        									<i class="fa fa-plus"></i>
										</button>
        								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.categoria" />">
        									<i class="fa fa-pencil"></i>
										</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.categoria" />">
        									<i class="fa fa-search"></i>
										</button>
      								</span>
									
								</div>
								<c:if test="${!empty param.categoria}"><p class="form-control-static"><c:out value="${param.categoria}"/></p></c:if>
							</div>
									
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="pessoaStatus">
			   						<fmt:message key="status"/>
			   						<span class="required">*</span>
			   						<a class="info-help" data-message="pessoa.status.help">(?)</a>
								</label>
			   					<select id="pessoaStatus" name="pessoa.status" data-category="status" class="form-control required">
			   						<option value="ATI"<c:if test="${pessoa ne null and pessoa.status eq 'ATI'}"> selected="selected"</c:if>><fmt:message key="Status.ATI"/></option>
			   						<option value="INA"<c:if test="${pessoa ne null and pessoa.status eq 'INA'}"> selected="selected"</c:if>><fmt:message key="Status.INA"/></option>
			   					</select>
									
							</div>
								
						</div>
								
						<div class="row">
								
							<div id="layPessoaRazaoSocial" class="col-xs-12 col-sm-${pessoa ne null and pessoa.pessoaTipo eq 'F' ? '5' : '6'} form-group" data-input-detalhe="pessoaRazaoSocial">
									
								<label class="control-label" for="pessoaRazaoSocial">
									${razaoNome}
									<span class="required">*</span>
								</label>
								<input type="text" id="pessoaRazaoSocial" name="pessoa.${input_razaoNome}"<c:if test="${pessoa ne null and value_razaoNome ne null}"> value="<c:out value="${value_razaoNome}"/>"</c:if> maxlength="100" data-category="razaoSocial nome" class="form-control required"<c:if test="${pessoa.id eq null and !empty param.val}"> data-val="<c:out value="${param.val}"/>"</c:if> />
									
							</div>
							
							<div id="layPessoaSexo" class="col-xs-12 col-sm-2 form-group"<c:if test="${pessoa ne null and pessoa.pessoaTipo eq 'J'}"> style="display: none"</c:if>>
								
								<label class="control-label" for="pessoaSexo"><fmt:message key="sexo" /></label>
								
								<select id="pessoaSexo" name="pessoa.sexo" class="form-control" data-category="sexo">
									<option></option>
									<option value="M"<c:if test="${pessoa ne null and pessoa.sexo eq 'M'}"> selected="selected"</c:if>><fmt:message key="PessoaSexo.M" /></option>
									<option value="F"<c:if test="${pessoa ne null and pessoa.sexo eq 'F'}"> selected="selected"</c:if>><fmt:message key="PessoaSexo.F" /></option>
								</select>
							
							</div>
									
							<div id="layPessoaFantasia" class="col-xs-12 col-sm-${pessoa ne null and pessoa.pessoaTipo eq 'F' ? '5' : '6'} form-group" data-input-detalhe="pessoaFantasia">
									
								<label class="control-label" for="pessoaFantasia">${fantasiaApelido}</label>
								<input type="text" id="pessoaFantasia" name="pessoa.${input_fantasiaApelido}"<c:if test="${pessoa ne null and value_fantasiaApelido ne null}"> value="<c:out value="${value_fantasiaApelido}"/>"</c:if> maxlength="100" data-category="fantasia apelido" class="form-control" />
									
							</div>
									
						</div>
								
						<div class="row">
									
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="pessoaCnpj">
								<label class="control-label" for="pessoaCnpj">${cnpjCpf}</label>
			   					<input type="text" id="pessoaCnpj" name="pessoa.${input_cnpjCpf}"<c:if test="${pessoa ne null and value_cnpjCpf ne null}"> value="<c:out value="${value_cnpjCpf}"/>"</c:if> maxlength="18" data-category="cnpj cpf" class="form-control" />
							</div>
									
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="pessoaInscEst">${inscEstRg}</label>
			   					<input type="text" id="pessoaInscEst" name="pessoa.${input_inscEstRg}"<c:if test="${pessoa ne null and value_inscEstRg ne null}"> value="<c:out value="${value_inscEstRg}"/>"</c:if> maxlength="20" data-category="inscEst rg" class="form-control" />
							</div>
									
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="pessoaInscMun">${inscMunTituloEleitor}</label>
			   					<input type="text" id="pessoaInscMun" name="pessoa.${input_inscMunTituloEleitor}"<c:if test="${pessoa ne null and value_inscMunTituloEleitor ne null}"> value="<c:out value="${value_inscMunTituloEleitor}"/>"</c:if> maxlength="20" data-category="inscMun tituloEleitor" class="form-control" />
							</div>
								
						</div>
							
						<div class="row">
								
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="pessoaCep"><fmt:message key="cep"/></label>
								<div class="input-group">
									<input type="text" id="pessoaCep" name="pessoa.cep"<c:if test="${pessoa ne null and pessoa.cep ne null}"> value="<c:out value="${pessoa.cep}"/>"</c:if> maxlength="9" data-category="cep buscar.cep" class="form-control" />
			   						<span class="input-group-btn">
			       						<button id="btBuscarCep" class="btn btn-default" type="button" title="<fmt:message key="pesquisar___"/>">
			       							<i class="fa fa-search"></i>
			       						</button>
			   						</span>
								</div>
							</div>
								
						</div>
								
						<div class="row">
								
							<div class="col-xs-12 col-sm-9 form-group">
								<label class="control-label" for="pessoaEndereco"><fmt:message key="endereco"/></label>
								<input type="text" id="pessoaEndereco" name="pessoa.endereco"<c:if test="${pessoa ne null and pessoa.endereco ne null}"> value="<c:out value="${pessoa.endereco}"/>"</c:if> maxlength="100" data-category="endereco" class="form-control" />
							</div>
							
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="pessoaNumero"><fmt:message key="numero"/></label>
								<input type="text" id="pessoaNumero" name="pessoa.numero"<c:if test="${pessoa ne null and pessoa.numero ne null}"> value="<c:out value="${pessoa.numero}"/>"</c:if> maxlength="10" data-category="numero" class="form-control" />
							</div>
						
						</div>
								
						<div class="row">
						
							<div class="col-xs-12 form-group">
								<label class="control-label" for="pessoaComplemento"><fmt:message key="complemento"/></label>
								<input type="text" id="pessoaComplemento" name="pessoa.complemento"<c:if test="${pessoa ne null and pessoa.complemento ne null}"> value="<c:out value="${pessoa.complemento}"/>"</c:if> maxlength="100" data-category="complemento" class="form-control" />
							</div>
						
						</div>
								
						<div class="row">
								
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="pessoaBairro"><fmt:message key="bairro"/></label>
								<input type="text" id="pessoaBairro" name="pessoa.bairro"<c:if test="${pessoa ne null and pessoa.bairro ne null}"> value="<c:out value="${pessoa.bairro}"/>"</c:if> maxlength="50" data-category="bairro" class="form-control" />
							</div>
									
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="pessoaCidade"><fmt:message key="cidade"/></label>
								<input type="text" id="pessoaCidade" name="pessoa.cidade"<c:if test="${pessoa ne null and pessoa.cidade ne null}"> value="<c:out value="${pessoa.cidade}"/>"</c:if> maxlength="50" data-category="cidade" class="form-control" />
							</div>
									
						</div>
								
						<div class="row">
								
							<div class="col-xs-12 col-sm-2 form-group">
								<label class="control-label" for="pessoaUf"><fmt:message key="uf"/></label>
								<select id="pessoaUf" name="pessoa.uf" data-category="uf" class="form-control">
									<option value=""><fmt:message key="selecione___"/></option>
									<option value="AC"<c:if test="${pessoa ne null and pessoa.uf eq 'AC'}"> selected="selected"</c:if>>AC</option>
									<option value="AL"<c:if test="${pessoa ne null and pessoa.uf eq 'AL'}"> selected="selected"</c:if>>AL</option>
									<option value="AP"<c:if test="${pessoa ne null and pessoa.uf eq 'AP'}"> selected="selected"</c:if>>AP</option>
									<option value="AM"<c:if test="${pessoa ne null and pessoa.uf eq 'AM'}"> selected="selected"</c:if>>AM</option>
									<option value="BA"<c:if test="${pessoa ne null and pessoa.uf eq 'BA'}"> selected="selected"</c:if>>BA</option>
									<option value="CE"<c:if test="${pessoa ne null and pessoa.uf eq 'CE'}"> selected="selected"</c:if>>CE</option>
									<option value="DF"<c:if test="${pessoa ne null and pessoa.uf eq 'DF'}"> selected="selected"</c:if>>DF</option>
									<option value="ES"<c:if test="${pessoa ne null and pessoa.uf eq 'ES'}"> selected="selected"</c:if>>ES</option>
									<option value="GO"<c:if test="${pessoa ne null and pessoa.uf eq 'GO'}"> selected="selected"</c:if>>GO</option>
									<option value="MA"<c:if test="${pessoa ne null and pessoa.uf eq 'MA'}"> selected="selected"</c:if>>MA</option>
									<option value="MS"<c:if test="${pessoa ne null and pessoa.uf eq 'MS'}"> selected="selected"</c:if>>MS</option>
									<option value="MT"<c:if test="${pessoa ne null and pessoa.uf eq 'MT'}"> selected="selected"</c:if>>MT</option>
									<option value="MG"<c:if test="${pessoa ne null and pessoa.uf eq 'MG'}"> selected="selected"</c:if>>MG</option>
									<option value="PA"<c:if test="${pessoa ne null and pessoa.uf eq 'PA'}"> selected="selected"</c:if>>PA</option>
									<option value="PB"<c:if test="${pessoa ne null and pessoa.uf eq 'PB'}"> selected="selected"</c:if>>PB</option>
									<option value="PR"<c:if test="${pessoa ne null and pessoa.uf eq 'PR'}"> selected="selected"</c:if>>PR</option>
									<option value="PE"<c:if test="${pessoa ne null and pessoa.uf eq 'PE'}"> selected="selected"</c:if>>PE</option>
									<option value="PI"<c:if test="${pessoa ne null and pessoa.uf eq 'PI'}"> selected="selected"</c:if>>PI</option>
									<option value="RJ"<c:if test="${pessoa ne null and pessoa.uf eq 'RJ'}"> selected="selected"</c:if>>RJ</option>
									<option value="RN"<c:if test="${pessoa ne null and pessoa.uf eq 'RN'}"> selected="selected"</c:if>>RN</option>
									<option value="RS"<c:if test="${pessoa ne null and pessoa.uf eq 'RS'}"> selected="selected"</c:if>>RS</option>
									<option value="RO"<c:if test="${pessoa ne null and pessoa.uf eq 'RO'}"> selected="selected"</c:if>>RO</option>
									<option value="RR"<c:if test="${pessoa ne null and pessoa.uf eq 'RR'}"> selected="selected"</c:if>>RR</option>
									<option value="SC"<c:if test="${pessoa ne null and pessoa.uf eq 'SC'}"> selected="selected"</c:if>>SC</option>
									<option value="SP"<c:if test="${pessoa ne null and pessoa.uf eq 'SP'}"> selected="selected"</c:if>>SP</option>
									<option value="SE"<c:if test="${pessoa ne null and pessoa.uf eq 'SE'}"> selected="selected"</c:if>>SE</option>
									<option value="TO"<c:if test="${pessoa ne null and pessoa.uf eq 'TO'}"> selected="selected"</c:if>>TO</option>
								</select>
							</div>
								
						</div>
								
						<div class="row">
						
							<div class="col-xs-12 col-sm-6 col-md-3 form-group" data-input-detalhe="pessoaFone1">
								<label class="control-label" for="pessoaFone1"><fmt:message key="fone1" /></label>
								<input type="text" id="pessoaFone1" name="pessoa.fone1"<c:if test="${pessoa ne null and pessoa.fone1 ne null}"> value="<c:out value="${pessoa.fone1}"/>"</c:if> maxlength="25" data-category="fone1" class="form-control"/>
							</div>
							
							<div class="col-xs-12 col-sm-6 col-md-3 form-group">
								<label class="control-label" for="pessoaFone2"><fmt:message key="fone2" /></label>
								<input type="text" id="pessoaFone2" name="pessoa.fone2"<c:if test="${pessoa ne null and pessoa.fone2 ne null}"> value="<c:out value="${pessoa.fone2}"/>"</c:if> maxlength="25" data-category="fone2" class="form-control"/>
							</div>
							
							<div class="col-xs-12 col-sm-6 col-md-3 form-group">
								<label class="control-label" for="pessoaFone1"><fmt:message key="fone3" /></label>
								<input type="text" id="pessoaFone3" name="pessoa.fone3"<c:if test="${pessoa ne null and pessoa.fone3 ne null}"> value="<c:out value="${pessoa.fone3}"/>"</c:if> maxlength="25" data-category="fone3" class="form-control"/>
							</div>
							
							<div class="col-xs-12 col-sm-6 col-md-3 form-group">
								<label class="control-label" for="pessoaFone4"><fmt:message key="fone4" /></label>
								<input type="text" id="pessoaFone4" name="pessoa.fone4"<c:if test="${pessoa ne null and pessoa.fone4 ne null}"> value="<c:out value="${pessoa.fone4}"/>"</c:if> maxlength="25" data-category="fone4" class="form-control" />
							</div>
						
						</div>
								
						<div class="row">
							
							<div class="col-xs-12 col-sm-6 form-group" data-input-detalhe="pessoaEmail">
								<label class="control-label" for="pessoaEmail">
									<fmt:message key="email" />
									<c:if test="${isUsuario}">
										<span class="required">*</span>
									</c:if>
								</label>
								<c:choose>
									<c:when test="${pessoa.id eq null or !pessoa.isUsuario or (pessoa ne usuarioLogged.cliente and usuarioLogged.isCliente)}">
										<input type="email" id="pessoaEmail" name="pessoa.email"<c:if test="${pessoa ne null and pessoa.email ne null}"> value="<c:out value="${pessoa.email}"/>"</c:if> maxlength="127" data-category="email" class="form-control" disabled="disabled" />		
									</c:when>
									<c:otherwise>
										<p class="form-control-static"><c:out value="${pessoa.email}"/></p>
									</c:otherwise>
								</c:choose>
								
							</div>
							
							<c:if test="${isUsuario or usuarioLogged.isCliente}">
								<div class="col-xs-12 col-sm-6 form-group">
									<label class="control-label" for="pessoaSenha">
										<fmt:message key="senha" />
										<c:if test="${isUsuario}">
											<span class="required">*</span>
										</c:if>
									</label>
									
									<c:choose>
										<c:when test="${pessoa.id eq null or (pessoa ne usuarioLogged.cliente and usuarioLogged.isCliente)}">
											<input type="password" id="pessoaSenha" name="pessoa.senha"<c:if test="${pessoa ne null and pessoa.senha ne null}"> value="<c:out value="${pessoa.senha}"/>"</c:if> maxlength="20" data-category="senha" class="form-control" disabled="disabled" />		
										</c:when>
										<c:otherwise>
											<p class="form-control-static">********</p>
										</c:otherwise>
									</c:choose>
									
									
								</div>
							</c:if>
													
						</div>
						
						<div class="form-group">
							<label class="control-label" for="pessoaSite"><fmt:message key="site" /></label>
							<input type="text" id="pessoaSite" name="pessoa.site"<c:if test="${pessoa ne null and pessoa.site ne null}"> value="<c:out value="${pessoa.site}"/>"</c:if> maxlength="200" data-category="site" class="form-control"/>
						</div>
								
						<div class="form-group">
							<label class="control-label" for="pessoaObs"><fmt:message key="obs" /></label>
							<textarea id="pessoaObs" name="pessoa.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${pessoa ne null and pessoa.obs ne null}"><c:out value="${pessoa.obs}"/></c:if></textarea>
						</div>
  				
 					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-dialog-detalhe" style="float: left">
						<span class="glyphicon" aria-hidden="true"></span>
						<fmt:message key="detalhes"/>
					</button>
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
		       	</div>
		       	
		       	<c:if test="${pessoa ne null and pessoa.id ne null}">
		       		<div class="container-fluid">
				       	<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${pessoa.criacao}" create="create" pessoa="${pessoa.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${pessoa.alteracao}" create="update" pessoa="${pessoa.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pessoa.cadastro.js?v${applicationVersion}"></script>