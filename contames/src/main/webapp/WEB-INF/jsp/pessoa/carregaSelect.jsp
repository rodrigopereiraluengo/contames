<c:if test="${(pessoaFList eq null or empty pessoaFList) and (pessoaJList eq null or empty pessoaJList)}">
	<option value=""><fmt:message key="nenhumRegistroEncontrado"/></option>
</c:if>
<c:if test="${(pessoaJList ne null and !empty pessoaJList) or (pessoaFList ne null and !empty pessoaFList)}">
	<option value=""><fmt:message key="selecione___"/></option>
</c:if>
<c:if test="${(pessoaJList ne null and !empty pessoaJList)}">
	<optgroup label="<fmt:message key="PessoaTipo.J" />">
		<c:forEach var="pessoa" items="${pessoaJList}">
			<option data-tipo="${pessoa.pessoaTipo}" value="${pessoa.id}"<c:if test="${param.id ne null and param.id eq pessoa.id}"> selected="selected"</c:if>><c:out value="${pessoa.displayNome}"/></option>
		</c:forEach>
	</optgroup>
</c:if>
<c:if test="${(pessoaFList ne null and !empty pessoaFList)}">
	<optgroup label="<fmt:message key="PessoaTipo.F" />">
		<c:forEach var="pessoa" items="${pessoaFList}">
			<option data-tipo="${pessoa.pessoaTipo}" value="${pessoa.id}"<c:if test="${param.id ne null and param.id eq pessoa.id}"> selected="selected"</c:if>><c:out value="${pessoa.displayNome}"/></option>
		</c:forEach>
	</optgroup>
</c:if>