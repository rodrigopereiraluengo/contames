<div id="dialogContaCadastro" class="modal" style="display: none" data-dialog-detalhe="${param.dialogDetalhe}">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmContaCadastro">
			
				<input type="hidden" id="contaViewid" name="viewid" value="${viewid}"/>
			
				<c:if test="${conta ne null}">
					<input type="hidden" name="conta.id" value="${conta.id}"/>
				</c:if>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogItemCadastroTitle"><fmt:message key="conta"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid" data-lay-detalhe="minus" style="display: none">
						
						<div class="row">
							<div class="col-xs-12 col-sm-6 form-group" data-input-detalhe="contaNome"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaAgenciaNum"></div>
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaNum"></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoInicial"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoAtual"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoLimite"></div>
							<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoLimiteDisp"></div>
						</div>
					
					</div>
				
					<div class="container-fluid" data-lay-detalhe="plus">
					
						<div class="row">
  					
	  						<div class="col-xs-12 col-sm-5 form-group">
	  							<label class="control-label" for="contaBanco">
	  								<fmt:message key="banco" />
	  								<a class="info-help" data-message="conta.banco.help">(?)</a>
  								</label>
	  							<div class="input-group">
	  								
	  								<input 
	  									type="text" 
	  									id="contaBanco" 
	  									name="conta.banco.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus&pessoaTipo=J&categoria=<fmt:message key="categoriaBanco"/>"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoas&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI&pessoaTipo=J&categoria=<fmt:message key="categoriaBanco"/>"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete?pessoaTipo=J&categoria=<fmt:message key="categoriaBanco"/>"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${conta ne null and conta.banco ne null}"> value="${conta.banco.displayNome}" data-val="${conta.banco.id}"</c:if>
			    						/>
	  								<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
			    							<i class="fa fa-plus"></i>
	       								</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
	       									<i class="fa fa-pencil"></i>
	       								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
											<i class="fa fa-search"></i>
	       								</button>
	   								</span>
	  							</div>
	  							
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-2 form-group">
	  							<label class="control-label" for="contaIsPrincipal"><fmt:message key="principal" /></label>
	  							<select id="contaIsPrincipal" name="conta.isPrincipal" data-category="isPrincipal" class="form-control">
									<option value="true"<c:if test="${conta ne null and conta.isPrincipal}"> selected="selected"</c:if>><fmt:message key="sim"/></option>
									<option value="false"<c:if test="${conta ne null and !conta.isPrincipal}"> selected="selected"</c:if>><fmt:message key="nao"/></option>
								</select>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-2 form-group">
	  							<label class="control-label" for="contaIsPoupanca"><fmt:message key="poupanca" /></label>
	  							<c:if test="${param.isPoupanca eq null or empty param.isPoupanca}">
		  							<select id="contaIsPoupanca" name="conta.isPoupanca" data-category="isPoupanca" class="form-control">
										<option value="true"<c:if test="${conta ne null and conta.isPoupanca}"> selected="selected"</c:if>><fmt:message key="sim"/></option>
										<option value="false"<c:if test="${conta ne null and !conta.isPoupanca or conta eq null}"> selected="selected"</c:if>><fmt:message key="nao"/></option>
									</select>
								</c:if>
								<c:if test="${!empty param.isPoupanca or !empty talaoChequeList}">
									<p class="form-control-static"><fmt:message key="nao"/></p>
								</c:if>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-3 form-group">
	  							<label class="control-label" for="contaStatus">
	  								<fmt:message key="status" />
	  								<span class="required">*</span>
	  								<a class="info-help" data-message="conta.status.help">(?)</a>
								</label>
	  							<select id="contaStatus" name="conta.status" data-category="status" class="form-control required">
									<option value="ATI"<c:if test="${conta ne null and conta.status eq 'ATI'}"> selected="selected"</c:if>><fmt:message key="Status.ATI"/></option>
									<option value="INA"<c:if test="${conta ne null and conta.status eq 'INA'}"> selected="selected"</c:if>><fmt:message key="Status.INA"/></option>
								</select>
	  						</div>
	  						
	  					</div>
  					
	  					<div class="row">
	  					
	  						<div class="col-xs-12 col-sm-6 form-group" data-input-detalhe="contaNome">
	  							<label class="control-label" for="contaNome">
	  								<fmt:message key="nome" />
	  								<span class="required">*</span>
	  								<a class="info-help" data-message="conta.nome.help">(?)</a>
								</label>
	  							<input type="text" id="contaNome" name="conta.nome"<c:if test="${conta ne null}"> value="<c:out value="${conta.nome}"/>"</c:if> maxlength="50" data-category="nome" class="form-control required"/>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-6 form-group">
		  						
		  						<label class="control-label" for="contaTitular">
									<fmt:message key="titular" />
								</label>
								<div class="input-group">
									<input 
	  									type="text" 
	  									id="contaTitular" 
	  									name="conta.titular.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoa&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${conta ne null and conta.titular ne null}"> value="${conta.titular.displayNome}" data-val="${conta.titular.id}"</c:if>
			    						/>
			  						
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
	       									<i class="fa fa-plus"></i>
										</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
	       									<i class="fa fa-pencil"></i>
										</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
	       									<i class="fa fa-search"></i>
										</button>
	   								</span>
		  						</div>
		  					</div>
	  					
  						</div>
  					
	  					<div class="row">
	  					
	  						<div class="col-xs-12 col-sm-6 form-group">
		  						<label class="control-label" for="contaAgencia">
									<fmt:message key="agencia" />
									<a class="info-help" data-message="conta.agencia.help">(?)</a>
								</label>
								<input type="text" id="contaAgencia" name="conta.agencia"<c:if test="${conta ne null and conta.agencia ne null}"> value="<c:out value="${conta.agencia}"/>"</c:if> maxlength="100" data-category="agencia" class="form-control" />
							</div>
	  					
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaAgenciaNum">
	  							<label class="control-label" for="contaAgenciaNum">
	  								<fmt:message key="agenciaNum" />
	  							</label>
	  							<input type="text" id="contaAgenciaNum" name="conta.agenciaNum"<c:if test="${conta ne null and conta.agenciaNum ne null}"> value="<c:out value="${conta.agenciaNum}"/>"</c:if> maxlength="15" data-category="agenciaNum" class="form-control"/>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaNum">
	  							<label class="control-label" for="contaNum">
	  								<fmt:message key="contaNum" />
	  							</label>
	  							<input type="text" id="contaNum" name="conta.contaNum"<c:if test="${conta ne null and conta.contaNum ne null}"> value="<c:out value="${conta.contaNum}"/>"</c:if> maxlength="25" data-category="contaNum" class="form-control"/>
	  						</div>
	  					
	  					</div>
  					
	  					<div class="row">
	  					
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoInicial">
	  							<label class="control-label" for="contaSaldoInicial">
	  								<fmt:message key="saldoInicial" />
	  								<span class="required">*</span>
	  							</label>
	  							<c:if test="${!isCompensacao}">
	  								<input type="text" id="contaSaldoInicial" name="conta.saldoInicial"<c:if test="${conta ne null}"> value="${conta.saldoInicial}"</c:if> data-category="saldoInicial" class="form-control required"/>
	  							</c:if>
	  							<c:if test="${isCompensacao}">
	  								<p class="form-control-static text-right">
	  									<span class="font-bold${conta.saldoInicial lt 0 ? ' font-red' : ' font-blue'}">
	  										<fmt:formatNumber type="number" value="${conta.saldoInicial}" maxFractionDigits="2" minFractionDigits="2" />
	  									</span>
  									</p>
	  							</c:if>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoAtual">
	  							<label class="control-label" for="contaSaldoAtual">
	  								<fmt:message key="saldoAtual" />
	  							</label>
	  							<p class="form-control-static text-right" id="contaSaldoAtual">
	  								<c:if test="${conta.id eq null}">
	  									<span class="text-danger"><strong>[&nbsp;<fmt:message key="conta.saldoAtual.blank"/>&nbsp;]</strong></span>
	  								</c:if>
	  								<c:if test="${conta.id ne null}">
	  									<span class="font-bold${conta.saldoAtual lt 0 ? ' font-red' : ' font-blue'}">
	  										<fmt:formatNumber type="number" value="${conta.saldoAtual}" maxFractionDigits="2" minFractionDigits="2" />
	  									</span>
  									</c:if>
								</p>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoLimite">
	  							<div id="layContaSaldoLimite">
		  							<label class="control-label" for="contaSaldoLimite">
		  								<fmt:message key="limite" />
		  							</label>
		  							<input type="text" id="contaSaldoLimite" name="conta.saldoLimite"<c:if test="${conta ne null and conta.saldoLimite ne null}"> value="${conta.saldoLimite}"</c:if> data-category="saldoLimite" class="form-control"/>
	  							</div>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-3 form-group" data-input-detalhe="contaSaldoLimiteDisp">
	  							<div id="layContaSaldoLimiteDisp">
		  							<label class="control-label" for="contaSaldoLimiteDisp">
		  								<fmt:message key="limiteDisp" />
		  							</label>
		  							<p class="form-control-static text-right" id="contaSaldoLimiteDisp">
		  								<c:if test="${conta.id eq null or conta.saldoLimite eq null}">
		  									<span class="text-danger"><strong>[&nbsp;<fmt:message key="conta.saldoLimite.blank"/>&nbsp;]</strong></span>
		  								</c:if>
		  								<c:if test="${conta.id ne null and conta.saldoLimite ne null}">
		  									<span class="font-bold${conta.saldoLimite eq conta.saldoLimiteDisp ? ' font-blue' : ' font-red'}">
		  										<fmt:formatNumber type="number" value="${conta.saldoLimiteDisp}" maxFractionDigits="2" minFractionDigits="2" />
		  									</span>
	  									</c:if>
		  							</p>
	  							</div>
	  						</div>
	  					
	  					</div>
	  					
	  					<!-- Talao de Cheque -->
						<div class="panel panel-default" id="layContaTalaoCheque">
				
							<div class="panel-heading">
			    				<div class="row">
									<div class="col-xs-12 col-sm-4 text-nowrap">
				   						<p class="form-control-static font-bold"><fmt:message key="taloesCheque"/></p>
				   					</div>
				   					
				   					<div class="col-xs-12 col-sm-8 text-right">
				   						<div class="btn-group">
					   						<button type="button" id="contaBtAdicionarTalaoCheque" class="btn btn-default" onclick="dialogAdicionarTalaoCheque()" title="<fmt:message key="title.adicione.talaoCheque"/>">
					   							<i class="fa fa-plus"></i>
					   						</button>
					   						<button type="button" id="contaBtEditarTalaoCheque" class="btn btn-default" disabled="disabled" onclick="editarTalaoCheque()" title="<fmt:message key="title.editar.talaoCheque"/>">
					   							<i class="fa fa-pencil"></i>
					   						</button>
					   						<button type="button" id="contaBtExcluitTalaoCheque" class="btn btn-default" disabled="disabled" onclick="removerTalaoCheque()" title="<fmt:message key="title.remover.talaoCheque"/>">
					   							<i class="fa fa-trash"></i>
					   						</button>
				   						</div>
				   					</div>
				   				</div>
			  				</div>
							
							<div class="data-table">
								<div class="result">
									<jsp:include page="talaoChequeList.jsp" />
								</div>
							</div>
							
						</div>
  					
	  					<div class="form-group">
	  						<label class="control-label" for="contaObs">
								<fmt:message key="obs" />
							</label>
							<textarea id="contaObs" name="conta.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${conta ne null and conta.obs ne null}"><c:out value="${conta.obs}"/></c:if></textarea>
	  					</div>
					
					</div>
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-dialog-detalhe" style="float: left">
						<span class="glyphicon" aria-hidden="true"></span>
						<fmt:message key="detalhes"/>
					</button>
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
		       	</div>
		       	
		       	<c:if test="${conta ne null and conta.id ne null}">
			       	<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${conta.criacao}" create="create" pessoa="${conta.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${conta.alteracao}" create="update" pessoa="${conta.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/conta.cadastro.js?v${applicationVersion}"></script>