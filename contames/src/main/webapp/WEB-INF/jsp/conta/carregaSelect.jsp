<c:if test="${contaList eq null or empty contaList}">
	<option value=""><fmt:message key="nenhumaContaEncontrada"/></option>
</c:if>
<c:if test="${contaList ne null and !empty contaList}">
	<option value=""><fmt:message key="selecione___"/></option>
	<c:forEach var="conta" items="${contaList}">
		<option value="${conta.id}"<c:if test="${conta.id eq id or id eq null and (conta.isPrincipal ne null and conta.isPrincipal and (empty param.isPrincipal or param.isPrincipal eq 'true'))}"> selected="selected"</c:if> data-isPoupanca="${conta.isPoupanca}"><c:out value="${conta.nome}"/></option>
	</c:forEach>
</c:if>