<fmt:message key="patternDate" var="patternDate" />
<div id="layContaTalaoChequeList">
	<c:if test="${talaoChequeList eq null or empty talaoChequeList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumTalaoChequeEncontrado"/>
			</strong>
		</div>
	</c:if>
	<c:if test="${talaoChequeList ne null and !empty talaoChequeList}">
		<div class="table-responsive data-table">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="data"/></th>
						<th class="cm-th"><fmt:message key="seqInicio"/></th>
						<th class="cm-th"><fmt:message key="folhas"/></th>
						<th class="cm-th"><fmt:message key="seqFim"/></th>
						<th class="cm-th"><fmt:message key="status"/></th>
					</tr>
				</thead>
				<tbody id="tbyContaTalaoChequeList">
					<c:forEach var="talaoCheque" items="${talaoChequeList}">
						<tr 
							data-id="${talaoCheque.id}" 
							data-index="${talaoCheque.index}"
							data-category="talaoCheque_${talaoCheque.id}" 
							onclick="selecionaTalaoCheque(this)" 
							ondblclick="editarTalaoCheque(this)">
							<td class="text-center"><joda:format pattern="${patternDate}" value="${talaoCheque.data}" /></td>
							<td class="text-right">${talaoCheque.seqInicio}</td>
							<td class="text-right">${talaoCheque.folhas}</td>
							<td class="text-right">${talaoCheque.seqFim}</td>
							<td class="text-center"><fmt:message key="Status.${talaoCheque.status}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:if>
</div>