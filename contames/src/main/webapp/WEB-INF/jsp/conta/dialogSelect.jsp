<div id="dialogSelectConta" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmDialogSelectConta">
			
				<input type="hidden" name="isRequired" value="${isRequired}"/>
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogSelectContaTitle"><fmt:message key="conta"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div class="form-group">
						<div class="input-group">
							<c:if test="${isReadOnly}">
								<p class="form-control-static">
									<c:if test="${conta ne null}"><c:out value="${conta.nome}" /></c:if>
									<c:if test="${conta eq null}"><fmt:message key="conta.nothing.selected" /></c:if>
								</p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<select 
		    						id="dialogSelectContaId" 
		    						name="conta.id" 
		    						class="form-control"
		    						data-form="${pageContext.request.contextPath}/conta/cadastro?dialogDetalhe=minus&isPoupanca=${isPoupanca}"
		    						data-search="${pageContext.request.contextPath}/cadastros/contas?tpl=dialog&dialogId=ContaConsultar&dialogTitle=conta&dialogDetalhe=minus&status=ATI&isPoupanca=${isPoupanca}"
		    						data-load-options="${pageContext.request.contextPath}/conta/carregaSelect?isPoupanca=${isPoupanca}"
		    						data-category="conta">
		    						<jsp:include page="/WEB-INF/jsp/conta/carregaSelect.jsp" />
		    					</select>
		    					<span class="input-group-btn">
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.conta" />">
										<i class="fa fa-plus"></i>
									</button>
									<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.conta" />">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.conta" />">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</c:if>
						</div>
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<c:if test="${!isReadOnly}">
		        		<button type="button" class="btn btn-default" data-dismiss="modal">
		        			<i class="fa fa-close"></i>
		        			<b><fmt:message key="cancelar"/></b>
	        			</button>
	        		</c:if>
		        	<button type="${isReadOnly ? 'button' : 'submit'}" class="btn btn-primary"<c:if test="${isReadOnly}"> data-dismiss="modal"</c:if>>
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
				
			</form>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/conta.dialogSelect.js?v${applicationVersion}"></script>