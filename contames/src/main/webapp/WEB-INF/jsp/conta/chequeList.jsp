<fmt:message key="patternDate" var="patternDate" />
<c:if test="${chequeList eq null or empty chequeList}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumRegistroEncontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${chequeList ne null and !empty chequeList}">
	<div class="table-responsive data-table">
		<table id="tblChequeList" class="table">
			<thead>
				<tr>
					<th class="cm-th"><fmt:message key="n_" /></th>
					<th class="cm-th"><fmt:message key="data" /></th>
					<th class="cm-th"><fmt:message key="descricao" /></th>
					<th class="cm-th"><fmt:message key="status" /></th>
					<th class="cm-th"><fmt:message key="valor" /></th>
					<th class="cm-th">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyChequeList">
				<c:forEach var="cheque" items="${chequeList}">
					<tr>
						<td class="text-right">${cheque.numero}</td>
						<td class="text-center"><joda:format pattern="${patternDate}" value="${cheque.displayData}" /></td>
						<td>
							<c:if test="${cheque.transferencia eq null and cheque.favorecido ne null}">
								<c:out value="${cheque.favorecido.displayNome}" />
							</c:if>
							<c:if test="${cheque.transferencia ne null}">
								<fmt:message key="transferencia"/> - <c:out value="${cheque.transferencia.destino.nome}" />
							</c:if>
						</td>
						<td class="text-center">
							<fmt:message key="LancamentoStatus.${cheque.status eq 'BXD' ? 'DES.BXD' : cheque.status}" />
						</td>
						<td class="text-right"><fmt:formatNumber type="number" value="${cheque.valor}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-center">
							<c:if test="${cheque.status eq 'CAN'}">
								<button type="button" class="btn btn-default btn-xs" onclick="talaoChequeRestaurarCheque(${cheque.id}, ${cheque.numero})" title="<fmt:message key="title.restaurar.cheque" />">
									<i class="fa fa-arrow-right"></i>
								</button>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</c:if>