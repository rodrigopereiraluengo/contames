<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumaContaEncontrada"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">

	<c:set var="path" value="cadastros/contas"/>

	<div class="table-responsive">
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" column="banco" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" name="principal" column="isPrincipal" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" name="poupanca" column="isPoupanca" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="titular" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" column="agencia" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" column="agenciaNum" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="8" column="contaNum" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="9" column="saldoInicial" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="10" column="saldoAtual" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="11" name="limite" column="saldoLimite" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="12" name="limiteDisp" column="saldoLimiteDisp" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyConta">
				<c:forEach var="conta" items="${contaList}">
					<tr data-id="${conta['id']}" data-category="conta_${conta['id']}">
						<td class="text-center<c:if test="${f:orderByContains('banco', orderByList)}"> font-bold</c:if>"><c:out value="${conta['banco']}"/></td>
						<td class="text-center<c:if test="${f:orderByContains('isPrincipal', orderByList)}"> font-bold</c:if>">${conta['isprincipal']}</td>
						<td class="text-center<c:if test="${f:orderByContains('isPoupanca', orderByList)}"> font-bold</c:if>">${conta['ispoupanca']}</td>
						<td class="text-center<c:if test="${conta['_status'] eq 'INA'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${conta['status']}</td>
						<td<c:if test="${f:orderByContains('nome', orderByList)}"> class="font-bold"</c:if>><c:out value="${conta['nome']}" /></td>
						<td<c:if test="${f:orderByContains('titular', orderByList)}"> class="font-bold"</c:if>><c:out value="${conta['titular']}" /></td>
						<td<c:if test="${f:orderByContains('agencia', orderByList)}"> class="font-bold"</c:if>><c:out value="${conta['agencia']}" /></td>
						<td class="text-center<c:if test="${f:orderByContains('agenciaNum', orderByList)}"> font-bold</c:if>"><c:out value="${conta['agencianum']}" /></td>
						<td class="text-center<c:if test="${f:orderByContains('contaNum', orderByList)}"> font-bold</c:if>"><c:out value="${conta['contanum']}" /></td>
						<td class="text-right<c:if test="${f:orderByContains('saldoInicial', orderByList)}"> font-bold</c:if>">
							<c:if test="${conta['saldoinicial'] ne null}">
								<span class="font-bold${conta['saldoinicial'] lt 0 ? ' font-red' : ' font-blue'}">
									<fmt:formatNumber type="number" value="${conta['saldoinicial']}" maxFractionDigits="2" minFractionDigits="2" />
								</span>
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('saldoAtual', orderByList)}"> font-bold</c:if>">
							<c:if test="${conta['saldoatual'] ne null}">
								<span class="font-bold${conta['saldoatual'] lt 0 ? ' font-red' : ' font-blue'}">
									<fmt:formatNumber type="number" value="${conta['saldoatual']}" maxFractionDigits="2" minFractionDigits="2" />
								</span>
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('saldoLimite', orderByList)}"> font-bold</c:if>">
							<c:if test="${conta['saldolimite'] ne null}">
								<fmt:formatNumber type="number" value="${conta['saldolimite']}" maxFractionDigits="2" minFractionDigits="2" />
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('saldoLimiteDisp', orderByList)}"> font-bold</c:if>">
							<c:if test="${conta['saldolimitedisp'] ne null}">
								<span class="font-bold${conta['saldolimite'] eq conta['saldolimitedisp'] ? ' font-blue' : ' font-red'}">
									<fmt:formatNumber type="number" value="${conta['saldolimitedisp']}" maxFractionDigits="2" minFractionDigits="2" />
								</span>
							</c:if>
						</td>
						<td class="text-center">
							<c:if test="${conta['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(conta['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
	
	<div class="panel-footer">
 		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
  		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>

</c:if>