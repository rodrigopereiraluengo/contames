<c:if test="${talaoCheque.data ne null}">
	<fmt:message key="patternDate" var="patternDate"/>
	<joda:format pattern="${patternDate}" value="${talaoCheque.data}" var="data" />
</c:if>
<div id="dialogContaTalaoCheque" class="modal" style="display: none">
  	<div class="modal-dialog">
   		<div class="modal-content">
   		   		
   			<form id="frmContaTalaoCheque" data-adicionar="${param.isAdicionar}">
   				
   				<input type="hidden" id="talaoChequeId" name="talaoCheque.id" value="${talaoCheque.id}" />
   				<input type="hidden" name="conta.id" value="${talaoCheque.conta.id}" />
   				<input type="hidden" name="talaoCheque.index" value="${talaoCheque.index}" />
   				<input type="hidden" name="talaoCheque.viewid" value="${talaoCheque.viewid}" />
   				<input type="hidden" name="viewid" value="${viewid}" />
   				   		
	   			<div class="modal-header">
	   				<button type="button" class="close" data-dismiss="modal">
	   					<i class="fa fa-times"></i>
   					</button>
   					<button onclick="dialogArquivoConsultar('${talaoCheque.viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${talaoCheque.viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
	        		<h4 class="modal-title"><fmt:message key="talaoCheque"/></h4>
	      		</div>
	      		
	      		<div class="modal-body">
	      			
	      			<c:if test="${viewid eq null}">
		      			<div class="form-group">
		      				<label class="control-label" for="contaTalaoChequeData">
								<fmt:message key="conta"/>
							</label>
							<p class="form-control-static">${talaoCheque.conta.nome}</p>
		      			</div>
	      			</c:if>
	      		
	      			<div class="row">
	      				
	      				<div class="col-xs-12 col-sm-4 form-group">
							<label class="control-label" for="contaTalaoChequeData">
								<fmt:message key="data"/>
							</label>
							<div class="input-group">
								<input type="text" id="contaTalaoChequeData" name="talaoCheque.data"<c:if test="${talaoCheque ne null}"> value="<c:out value="${data}"/>"</c:if> data-category="data" class="form-control" />
								<span class="input-group-btn">
	  								<button class="btn btn-default" type="button" id="btLancamentoParcelaData">
       									<i class="fa fa-calendar-o"></i>
									</button>
								</span>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
							<label class="control-label" for="contaTalaoChequeStatus">
								<fmt:message key="status"/>
								<span class="required">*</span>
  								<a class="info-help" data-message="talaoCheque.status.help">(?)</a>
							</label>
							<select name="talaoCheque.status" data-category="status" class="form-control required">
								<option value="ATI"<c:if test="${talaoCheque ne null and talaoCheque.status eq 'ATI'}"> selected="selected"</c:if>><fmt:message key="Status.ATI"/></option>
								<option value="INA"<c:if test="${talaoCheque ne null and talaoCheque.status eq 'INA'}"> selected="selected"</c:if>><fmt:message key="Status.INA"/></option>
							</select>
						</div>
	      			
	      			</div>
	      		
		      		<div class="row">
		      			
		      			<div class="col-xs-12 col-sm-4 form-group">
							<label class="control-label" for="contaTalaoChequeSeqInicio">
								<fmt:message key="seqInicio"/>
								<span class="required">*</span>
							</label>
							<input type="number" id="contaTalaoChequeSeqInicio" name="talaoCheque.seqInicio"<c:if test="${talaoCheque ne null}"> value="<c:out value="${talaoCheque.seqInicio}"/>"</c:if> min="1" data-category="seqInicio" class="form-control text-right required" />
						</div>
						
						<div class="col-xs-12 col-sm-4 form-group">
							<label class="control-label" for="contaTalaoChequeFolhas">
								<fmt:message key="folhas"/>
								<span class="required">*</span>
							</label>
							<input type="number" id="contaTalaoChequeFolhas" name="talaoCheque.folhas"<c:if test="${talaoCheque ne null}"> value="<c:out value="${talaoCheque.folhas}"/>"</c:if> min="1"  data-category="folhas seqFim" class="form-control text-right required" />
						</div>
						
						<div class="col-xs-12 col-sm-4 form-group">
							<label class="control-label" for="contaTalaoChequeSeqFim">
								<fmt:message key="seqFim"/>
							</label>
							<p class="form-control-static text-center" id="lbContaTalaoChequeSeqFim">
								<c:if test="${talaoCheque ne null}"><c:out value="${talaoCheque.seqFim}"/></c:if>
							</p>
						</div>
					
					</div>
					
					<div class="form-group">
  						<label class="control-label" for="contaTalaoChequeObs">
							<fmt:message key="obs" />
						</label>
						<textarea id="contaTalaoChequeObs" name="talaoCheque.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${talaoCheque ne null and talaoCheque.obs ne null}"><c:out value="${talaoCheque.obs}"/></c:if></textarea>
  					</div>	
	      			
	      			<c:if test="${talaoCheque ne null and talaoCheque.id ne null}">
	      				<div class="panel panel-default">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-12 col-sm-4 text-nowrap">
										<p class="form-control-static font-bold">
											<fmt:message key="cheques"/>
										</p>
									</div>
									<div class="col-xs-12 col-sm-8 text-right">
										<button type="button" class="btn btn-default" onclick="talaoChequeCancelarCheque()">
											<i class="fa fa-times"></i>
											<fmt:message key="cancelar"/>
										</button>
									</div>
								</div>
							</div>
			      			<div class="data-table">
								<div class="result" id="pnChequeList">
									<jsp:include page="chequeList.jsp"/>
								</div>
							</div>
						</div>
	      			</c:if>
	      			
	      		</div>
	      		
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default" data-dismiss="modal">
	      				<i class="fa fa-close"></i>
	      				<b><fmt:message key="cancelar"/></b>
      				</button>
	        		<button type="submit" class="btn btn-primary">
	        			<c:choose>
	        				<c:when test="${param.isAdicionar ne null and param.isAdicionar}">
	        					<i class="fa fa-check"></i>
	        					<b><fmt:message key="ok"/></b>
	        				</c:when>
	        				<c:otherwise>
	        					<i class="fa fa-save"></i>
	        					<b><fmt:message key="salvar"/></b>
	        				</c:otherwise>
	        			</c:choose>
	        		</button>
	      		</div>
	      		
	      		<c:if test="${talaoCheque ne null and talaoCheque.id ne null}">
	      			<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${talaoCheque.criacao}" create="create" pessoa="${talaoCheque.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${talaoCheque.alteracao}" create="update" pessoa="${talaoCheque.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
      		
      		</form>
   		
   		</div>
 	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/conta.talaoCheque.js?v${applicationVersion}"></script>