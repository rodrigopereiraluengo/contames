<div id="dialogArquivoConsultar" class="modal cm-modal-data-table" style="display: none" data-viewid="${param.viewid}">
  	<div class="modal-dialog">
   		<div class="modal-content">
   			
   			<form 
   				id="frmArquivoConsultar" 
   				action="${pageContext.request.contextPath}/arquivo/consultar?viewid=${param.viewid}"
   				data-form="${pageContext.request.contextPath}/arquivo/dialogAnexo?viewid=${param.viewid}"
   				data-delete="${pageContext.request.contextPath}/arquivo/excluir?viewid=${param.viewid}"
   				data-viewid="${param.viewid}">
     			
      			<div class="modal-header">
      				<button type="button" class="close" data-dismiss="modal">
      					<i class="fa fa-times"></i>
   					</button>
	        		<h4 class="modal-title"><fmt:message key="arquivos"/></h4>
	      		</div>
	      		
	      		<div class="modal-body">
	      			
	      			<div class="data-table">
	      			
	      				<div class="panel panel-default">
	      			
			      			<div class="panel-heading cm-panel-heading-toolbar">
				      			
				      			<div class="container-fluid">
				      				
				      				<div class="row">
						      			
						      			<div class="col-xs-12 col-sm-6 form-group">
						      				<div class="btn-group">
								    			<button type="button" class="btn btn-default bt-new">
								    				<i class="fa fa-plus"></i>
								    				<fmt:message key="enviar"/>
							    				</button>
						        				<button type="button" class="btn btn-default bt-delete" disabled="disabled">
						        					<i class="fa fa-trash"></i>
						        					<fmt:message key="excluir"/>
					        					</button>
						        			</div>
								  		</div>
								  		
								  		<div class="col-xs-12 col-sm-6 form-group">
								  			<div class="input-group">
												<input type="search" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
												<span class="input-group-btn">
													<button class="btn btn-default" type="submit">
														<i class="fa fa-search"></i>
													</button>
												</span>
											</div>
								    	</div>
					  				
					  				</div>
					  			
					  			</div>
					  		
					  		</div>
		        			
		        			<div class="result search-table">
		        				<jsp:include page="/WEB-INF/jsp/arquivo/consultar_table.jsp" />
			        		</div>
	      			
	      				</div>
	      			
	      			</div>
	      		
      			</div>
      			    		
    		</form>
    	
    	</div>
  	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/arquivo.consultar.js?v${applicationVersion}"></script>