<%@page trimDirectiveWhitespaces="true" contentType="text/html; charset=ISO-8859-1" %>
<script type="text/javascript">
window.parent.anexoArquivoError(
<json:array>
	<c:forEach var="error" items="${errors}">
		<json:object>
			<json:property name="category" value="${error.category}" />			
			<json:property name="message" value="${error.message}" />
		</json:object>
	</c:forEach>
</json:array>
);
</script>