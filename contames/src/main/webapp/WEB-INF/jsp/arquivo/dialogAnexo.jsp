<div id="dialogAnexoArquivo" class="modal" style="display: none" data-viewid="${viewid}">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form 
				id="frmDialogAnexoArquivo" 
				action="${pageContext.request.contextPath}/arquivo/dialogAnexoUpload" 
				method="post"
				target="iframeDialogAnexoArquivo" 
				enctype="multipart/form-data">
				
				<input type="hidden" name="arquivo.temp" value="${arquivo.temp}" />
				<input type="hidden" name="arquivo.contentType" value="<c:out value="${arquivo.contentType}"/>" />
				<input type="hidden" name="arquivo.size" value="<c:out value="${arquivo.size}"/>" />
				<input type="hidden" name="viewid" value="${viewid}" />
				<input type="hidden" name="isExcluir" />
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogAnexoArquivoTitle"><fmt:message key="arquivo"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div id="layDialogAnexoArquivoInput" class="form-group"<c:if test="${arquivo.nome ne null}"> style="display: none"</c:if>>
						<input type="file" id="dialogAnexoArquivoNome" name="uploadedFile" class="form-control" data-category="arquivo.nome" />
					</div>
										
					<div id="layDialogAnexoArquivoNome" class="input-group"<c:if test="${arquivo.nome eq null}"> style="display: none"</c:if>>
						<input type="text" name="arquivo.nome" class="form-control" value="<c:out value="${arquivo.nome}"/>" data-category="nome" readonly="readonly" />
						<span class="input-group-btn">
							<a class="btn btn-default" href="${pageContext.request.contextPath}/download/${arquivo.temp}<c:if test="${arquivo.id ne null}">/${arquivo.id}</c:if>/${arquivo.nome}" role="button">
								<i class="fa fa-download"></i>
							</a>
							<button class="btn btn-default" type="button" id="btDialogAnexoArquivoEditar">
								<i class="fa fa-pencil"></i>
							</button>
						</span>
					</div>
						
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
				
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/arquivo.dialogAnexo.js?v${applicationVersion}"></script>
<script type="text/javascript">
	function dialogAnexoArquivoSuccess(arquivo) {
		$('#dialogAnexoArquivo').trigger('close', [arquivo]).modal('hide');
		loading(false);
	}
</script>