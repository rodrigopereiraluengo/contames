<c:if test="${empty arquivoList}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhum.arquivo.encontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${!empty arquivoList}">
	<div class="table-responsive">
	
		<c:set var="path" value="arquivo/consultar?viewid=${param.viewid}"/>
  	
	  	<table class="table table-hover">
	  		<thead>
	  			<tr>
	  				<cmt:th orderByList="${orderByList}" index="0" column="nome" page="${page}" path="${path}" />
	  				<cmt:th orderByList="${orderByList}" index="1" name="tamanho" column="size" page="${page}" path="${path}" />
	  				<th class="cm-th-empty">&nbsp;</th>
	  			</tr>
	  		</thead>
			<tbody id="tbyArquivo">
				<c:forEach var="arquivo" items="${arquivoList}">
					<tr data-id="${arquivo.id eq null ? arquivo.temp : arquivo.id}">
						<td<c:if test="${f:orderByContains('nome', orderByList)}"> class="font-bold"</c:if>><c:out value="${arquivo.nome}"/></td>
						<td class="text-right<c:if test="${f:orderByContains('size', orderByList)}"> font-bold</c:if>">${f:byteCountToDisplaySize(arquivo.size)}</td>
						<td class="text-center">
							<a class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/download/${arquivo.temp}<c:if test="${arquivo.id ne null}">/${arquivo.id}</c:if>/${arquivo.nome}" role="button">
								<i class="fa fa-download"></i>
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>   
	  	</table>
	
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>