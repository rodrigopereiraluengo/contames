<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumaConciliacaoEncontrada"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
	
		<c:set var="path" value="lancamentos/conciliacao"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="conta" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" name="movimentos" column="quantCompensacaoItem" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="saldoAnterior" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" name="credito" column="totalCredito" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" name="debito" column="totalDebito" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" column="saldo" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="8" column="status" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyCompensacao">
				<c:forEach var="compensacao" items="${compensacaoList}">
					<tr data-id="${compensacao['id']}" data-category="compensacao_${compensacao['id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${compensacao['_data']}" /></td>
						<td class="text-center<c:if test="${f:orderByContains('conta', orderByList)}"> font-bold</c:if>">${compensacao['conta']}</td>
						<td class="text-right<c:if test="${f:orderByContains('quantCompensacaoItem', orderByList)}"> font-bold</c:if>">${compensacao['quantcompensacaoitem']}</td>
						<td class="text-right<c:if test="${f:orderByContains('saldoAnterior', orderByList)}"> font-bold</c:if>">
							<span class="font-bold<c:if test="${compensacao['saldoanterior'] < 0}"> font-red</c:if><c:if test="${compensacao['saldoanterior'] >= 0}"> font-blue</c:if>">
								<fmt:formatNumber type="number" value="${compensacao['saldoanterior']}" maxFractionDigits="2" minFractionDigits="2" />
							</span>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('totalCredito', orderByList)}"> font-bold</c:if> font-blue"><fmt:formatNumber type="number" value="${compensacao['totalcredito']}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right<c:if test="${f:orderByContains('totalDebito', orderByList)}"> font-bold</c:if> font-red"><fmt:formatNumber type="number" value="${compensacao['totaldebito']}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right<c:if test="${f:orderByContains('saldo', orderByList)}"> font-bold</c:if>">
							<span class="font-bold<c:if test="${compensacao['saldo'] < 0}"> font-red</c:if><c:if test="${compensacao['saldo'] >= 0}"> font-blue</c:if>">
								<fmt:formatNumber type="number" value="${compensacao['saldo']}" maxFractionDigits="2" minFractionDigits="2" />
							</span>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${compensacao['status']}</td>
						<td class="text-center">
							<c:if test="${compensacao['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(compensacao['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
 			
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>

</c:if>