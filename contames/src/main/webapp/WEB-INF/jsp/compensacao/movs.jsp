<fmt:message key="patternDate" var="patternDate" />
<div id="dialogCompensacaoMovs" class="modal cm-modal-data-table" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form 
				id="frmCompensacaoMovs" action="${pageContext.request.contextPath}/compensacao/movs"
				class="data-table">
			
				<input type="hidden" name="conta" value="${param.conta}" />
				<input type="hidden" name="dataAte"value="${param.dataAte}" />
						
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogCompensacaoMovsTitle"><fmt:message key="movimentos"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="panel panel-default">
						
						<div class="panel-heading cm-panel-heading-toolbar">
				
							<div class="container-fluid">
							
								<div class="row">
						
									<div class="col-xs-12 col-sm-1">
										<label class="form-control-static text-nowrap" for="compensacaoMovsDataDe"><fmt:message key="dataDe" /></label>
									</div>
									
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="input-group">
			  								<input type="text" id="compensacaoMovsDataDe" name="dataDe" class="form-control" value="${param.dataDe}" data-category="dataDe movs.dataDe" />
			  								<span class="input-group-btn">
				  								<button class="btn btn-default" type="button">
				  									<i class="fa fa-calendar-o"></i>
			       								</button>
											</span>
		  								</div>
									</div>
							
									<div class="col-xs-12 col-sm-1">
										<label class="form-control-static" for="compensacaoMovsDataAte"><fmt:message key="ate" /></label>
									</div>
									
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="input-group">
											<p class="form-control-static">${param.dataAte}</p>
			  							</div>
									</div>
									
									<div class="col-xs-12 col-sm-4 form-group">
										<div class="input-group">
											<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
											<span class="input-group-btn">
												<button class="btn btn-default" type="submit">
													<i class="fa fa-search"></i>
												</button>
											</span>
						    			</div>
									</div>
									
								</div>
							</div>
						
						</div>
						
						<div class="result search-table">
							<jsp:include page="/WEB-INF/jsp/compensacao/movs_table.jsp" />
	   					</div>
						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="button" id="btCompensacaoMovsOk" class="btn btn-primary" disabled="disabled">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/compensacao.movs.js?v${applicationVersion}"></script>