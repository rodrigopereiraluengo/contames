<fmt:message key="patternDate" var="patternDate" />
<div id="dialogEditarCompensacaoItem" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmEditarCompensacaoItem" data-tipo="${compensacaoItem.tipo}">
			
				<input type="hidden" name="compensacaoItem.seq" value="${compensacaoItem.seq}"/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${compensacaoItem.viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${compensacaoItem.viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogEditarCompensacaoItemTitle"><fmt:message key="movimento"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid">
						<div class="row">
						
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="compensacaoItemData">
	  								<fmt:message key="data" />
	  								<span class="required">*</span>
  								</label>
  								<div class="input-group">
  									<c:if test="${isClosed}">
  										<p class="form-control-static"><joda:format pattern="${patternDate}" value="${compensacaoItem.data}" /></p>
  									</c:if>
  									<c:if test="${!isClosed}">
		  								<input type="text" id="compensacaoItemData" name="compensacaoItem.data" value="<joda:format pattern="${patternDate}" value="${compensacaoItem.data}" />" data-category="data" class="form-control" />
		  								<span class="input-group-btn">
			  								<button class="btn btn-default" type="button">
			  									<i class="fa fa-calendar-o"></i>
		       								</button>
										</span>
									</c:if>
  								</div>
							</div>
							
							<div class="col-xs-12 col-sm-8 form-group">
								<label class="control-label" for="compensacaoItemFavorecido">
									<c:if test="${compensacaoItem.tipo eq 'DES'}"><fmt:message key="pagarA" /></c:if>
									<c:if test="${compensacaoItem.tipo eq 'REC'}"><fmt:message key="receberDe" /></c:if>
								</label>
								<div class="input-group">
									<input 
	  									type="text" 
	  									id="compensacaoItemFavorecido" 
	  									name="compensacaoItem.favorecido.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoas&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${compensacaoItem ne null and compensacaoItem.favorecido ne null}"> value="${compensacaoItem.favorecido.displayNome}" data-val="${compensacaoItem.favorecido.id}"</c:if>
			    						data-category="favorecido"
		    						/>
			  						
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
			    							<i class="fa fa-plus"></i>
	       								</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
	       									<i class="fa fa-pencil"></i>
	       								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
											<i class="fa fa-search"></i>
	       								</button>
	   								</span>
		  						</div>
							</div>
						
						</div>
						
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-8 form-group">
								<label class="control-label" for="compensacaoItemItem">
									<fmt:message key="ItemTipo.${compensacaoItem.item.tipo}" />
									<span class="required">*</span>
								</label>
								<div class="input-group">
									
	  								<input 
		  								type="text" 
		  								id="compensacaoItemItem" 
		  								name="compensacaoItem.item.id" 
		  								class="form-control"
		  								data-form="${pageContext.request.contextPath}/despesa/cadastro"
			    						data-search="${pageContext.request.contextPath}/cadastros/${compensacaoItem.item.tipo eq 'DES' ? 'despesas' : 'receitas'}?tpl=dialog&dialogTitle=${compensacaoItem.item.tipo eq 'DES' ? 'despesa' : 'receita'}&dialogId=ItemConsultar"
			    						data-url-auto-complete="${pageContext.request.contextPath}/item/autoComplete?tipo=${compensacaoItem.item.tipo}"
			    						data-reload="${pageContext.request.contextPath}/item/reload"
			    						<c:if test="${compensacaoItem.item ne null}"> value="${compensacaoItem.item.displayName}" data-val="${compensacaoItem.item.id}"</c:if>
			    						data-category="item"
		    						 />
		    						<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.${compensacaoItem.item.tipo}"/>">
	       									<i class="fa fa-plus"></i>
										</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.${compensacaoItem.item.tipo}"/>">
	       									<i class="fa fa-pencil"></i>
										</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.${compensacaoItem.item.tipo}"/>">
	       									<i class="fa fa-search"></i>
										</button>
	   								</span>
	   								
	    						</div>
							</div>
							
							<c:if test="${compensacaoItem.item.tipo eq 'DES'}">
								<div class="col-xs-12 col-sm-4 form-group">
									<label class="control-label" for="compensacaoItemDebito">
										<fmt:message key="debito" />
										<span class="required">*</span>
									</label>
									<c:if test="${isClosed}">
  										<p class="form-control-static"><fmt:formatNumber type="number" value="${compensacaoItem.debito}" maxFractionDigits="2" minFractionDigits="2" /></p>
  									</c:if>
  									<c:if test="${!isClosed}">
										<input type="text" id="compensacaoItemDebito" name="compensacaoItem.debito" value="${compensacaoItem.debito}" data-category="debito" class="form-control" />
									</c:if>
								</div>
							</c:if>
							<c:if test="${compensacaoItem.item.tipo eq 'REC'}">
								<div class="col-xs-12 col-sm-4 form-group">
									<label class="control-label" for="compensacaoItemCredito">
										<fmt:message key="credito" />
										<span class="required">*</span>
									</label>
									<c:if test="${isClosed}">
  										<p class="form-control-static"><fmt:formatNumber type="number" value="${compensacaoItem.credito}" maxFractionDigits="2" minFractionDigits="2" /></p>
  									</c:if>
  									<c:if test="${!isClosed}">
										<input type="text" id="compensacaoItemCredito" name="compensacaoItem.credito" value="${compensacaoItem.credito}" data-category="credito" class="form-control" />
									</c:if>
								</div>
							</c:if>
							
						</div>
						
						
						<div class="row">
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="compensacaoItemSaldo">
									<fmt:message key="saldo" />
								</label>
								<p id="compensacaoItemSaldo" class="form-control-static text-right<c:if test="${compensacaoItem.saldo ne null and compensacaoItem.saldo < 0}"> font-red</c:if><c:if test="${compensacaoItem.saldo ne null and compensacaoItem.saldo >= 0}"> font-blue</c:if>" data-saldo="${compensacaoItem.saldo}"><fmt:formatNumber type="number" value="${compensacaoItem.saldo}" maxFractionDigits="2" minFractionDigits="2" /></p>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="control-label" for="compensacaoItemObs"><fmt:message key="obs"/></label>
							<textarea id="compensacaoItemObs" name="compensacaoItem.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:out value="${compensacaoItem.obs}"/></textarea>
						</div>
						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/compensacao.editarCompensacaoItem.js?v${applicationVersion}"></script>