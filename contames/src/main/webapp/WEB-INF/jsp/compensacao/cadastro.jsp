<fmt:message key="patternDate" var="patternDate" />
<div id="dialogCompensacaoCadastro" class="modal" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmCompensacaoCadastro">
				
				<input type="hidden" id="compensacaoViewid" name="viewid" value="${viewid}" />
				<input type="hidden" id="compensacaoContaId" value="${compensacao.conta.id}" />
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogCompensacaoCadastroTitle"><fmt:message key="conciliacao"/></h4>
				</div>
				
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="compensacaoData">
	  								<fmt:message key="data" />
	  								<span class="required">*</span>
  								</label>
  								<div class="input-group">
  									<c:if test="${isClosed}">
  										<p class="form-control-static"><joda:format pattern="${patternDate}" value="${compensacao.data}" /></p>
  										<input type="hidden" id="compensacaoData" name="compensacao.data" value="<joda:format pattern="${patternDate}" value="${compensacao.data}" />" />
  									</c:if>
  									<c:if test="${!isClosed}">
		  								<input type="text" id="compensacaoData" name="compensacao.data" value="<joda:format pattern="${patternDate}" value="${compensacao.data}" />" data-category="data" class="form-control" />
		  								<span class="input-group-btn">
			  								<button class="btn btn-default" type="button" id="btCompensacaoData">
			  									<i class="fa fa-calendar-o"></i>
		       								</button>
										</span>
									</c:if>
  								</div>
							</div>
							
							<div class="col-xs-12 col-sm-3 col-sm-offset-6 form-group">
								<label class="control-label" for="compensacaoStatus">
	  								<fmt:message key="status" />
	  								<span class="required">*</span>
	  								<a class="info-help" data-message="compensacao.status.help">(?)</a>
								</label>
								<c:if test="${isClosed}">
									<p class="form-control-static"><fmt:message key="LancamentoStatus.${compensacao.status}"/></p>
								</c:if>
								<c:if test="${!isClosed}">
		  							<select id="compensacaoStatus" name="compensacao.status" data-category="status" class="form-control">
										<option value="STD"<c:if test="${compensacao.status eq 'STD'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.STD"/></option>
										<option value="COM"<c:if test="${compensacao.status eq 'COM'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.COM"/></option>
									</select>
								</c:if>
							</div>
									
						</div>
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="compensacaoContaId">
	  								<fmt:message key="conta" />
	  							</label>
								<p id="compensacaoContaId" class="form-control-static">${compensacao.conta.nome}</p>
							</div>
							
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="compensacaoSaldoAnterior"><fmt:message key="saldoAnterior" /></label>
								<p id="compensacaoSaldoAnterior" class="form-control-static text-right<c:if test="${compensacao.saldoAnterior < 0}"> font-red</c:if><c:if test="${compensacao.saldoAnterior >= 0}"> font-blue</c:if>"><fmt:formatNumber type="number" value="${compensacao.saldoAnterior}" maxFractionDigits="2" minFractionDigits="2" /></p>
							</div>
							
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="compensacaoSaldo">
									<fmt:message key="saldo" />
									<span class="required">*</span>
									<a class="info-help" data-message="compensacao.saldo.help">(?)</a>
								</label>
								<c:if test="${isClosed}">
									<p class="form-control-static text-right font-bold<c:if test="${compensacao.saldo < 0}"> font-red</c:if><c:if test="${compensacao.saldo >= 0}"> font-blue</c:if>"><fmt:formatNumber type="number" value="${compensacao.saldo}" maxFractionDigits="2" minFractionDigits="2" /></p>
								</c:if>
								<c:if test="${!isClosed}">
									<input type="text" id="compensacaoSaldo" name="compensacao.saldo" value="${compensacao.saldo}" data-category="saldo" class="form-control" />
								</c:if>
							</div>
							
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="compensacaoDiferenca">
									<fmt:message key="diferenca" />
									<a class="info-help" data-message="compensacao.diferenca.help">(?)</a>
								</label>
								<p id="compensacaoDiferenca" class="form-control-static text-right font-bold<c:if test="${compensacao.diferenca < 0}"> font-red</c:if><c:if test="${compensacao.diferenca >= 0}"> font-blue</c:if>"><fmt:formatNumber type="number" value="${compensacao.diferenca}" maxFractionDigits="2" minFractionDigits="2" /></p>
							</div>
							
						</div>
						
						<!-- Movimentos -->
						<div class="panel panel-default">
							
							<div class="panel-heading cm-toolbar">
			    				<div class="row">
									<div class="col-xs-4 text-nowrap">
				   						<p class="form-control-static font-bold"><fmt:message key="movimentos"/></p>
				   					</div>
				   					<div class="col-xs-8 text-right">
				   						<div class="btn-group">
					   						<button type="button" id="compensacaoBtSelecionarMovs" class="btn btn-default bt-select" title="<fmt:message key="selecionarMovimentos"/>">
					   							<i class="fa fa-search"></i>
				   							</button>
					   						<button type="button" id="compensacaoBtAdicionarDebCompensacaoItem" data-tipo="DES" class="btn btn-default bt-new font-red" title="<fmt:message key="adicionarDeb"/>">
					   							<i class="fa fa-minus"></i>
				   							</button>
					   						<button type="button" id="compensacaoBtAdicionarCredCompensacaoItem" data-tipo="REC" class="btn btn-default bt-new font-blue" title="<fmt:message key="adicionarCred"/>">
					   							<i class="fa fa-plus"></i>
				   							</button>
					   						<button type="button" id="compensacaoBtEditarCompensacaoItem" class="btn btn-default bt-edit" disabled="disabled" title="<fmt:message key="editar"/>">
					   							<i class="fa fa-pencil"></i>
				   							</button>
					   						<button type="button" id="compensacaoBtExcluitCompensacaoItem" class="btn btn-default bt-delete" disabled="disabled" title="<fmt:message key="remover"/>">
					   							<i class="fa fa-trash"></i>
				   							</button>
				   						</div>
				   					</div>
				   				</div>
			  				</div>
			  				
			  				<div class="data-table">
								<div class="result">
									<jsp:include page="compensacaoItemList.jsp"/>
								</div>
							</div>
			  									
						</div>
						
						<div class="form-group">
							<label class="control-label" for="compensacaoObs"><fmt:message key="obs" /></label>
							<textarea id="compensacaoObs" name="compensacao.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${compensacao ne null and compensacao.obs ne null}"><c:out value="${compensacao.obs}"/></c:if></textarea>
						</div>
						
					</div>
					
					
					
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<fmt:message key="cancelar"/>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
		       	</div>
		       	
		       	<c:if test="${compensacao ne null and compensacao.id ne null}">
			       	<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${compensacao.criacao}" create="create" pessoa="${compensacao.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${compensacao.alteracao}" create="update" pessoa="${compensacao.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/compensacao.cadastro.js?v${applicationVersion}"></script>