<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumMovimentoEncontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
	
		<c:set var="path" value="compensacao/movs"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" name="descricao" column="descricao" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" name="valor" column="valor" page="${page}" path="${path}" />
				</tr>
			</thead>
			<tbody id="tbyMovs">
							
				<c:forEach var="mov" items="${movsList}" varStatus="st">
					<tr 
						data-index="${st.index}"
						data-baixa-item="${mov['baixaitemid']}" 
						data-transferencia="${mov['transferenciaid']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${mov['_data']}" /></td>
						<td <c:if test="${f:orderByContains('descricao', orderByList)}"> class="font-bold"</c:if>>${mov['descricao']}</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>">
							<span class="${mov['tipo'] eq 'DES' ? 'font-red' : 'font-blue'}">
								<fmt:formatNumber type="number" value="${mov['valor']}" maxFractionDigits="2" minFractionDigits="2" />
							</span>
						</td>
					</tr>
				</c:forEach>
				
			</tbody>
		</table>
				
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>