<fmt:message key="patternDate" var="patternDate" />
<div id="layCompensacaoItem">
	<c:if test="${compensacaoItemList eq null or empty compensacaoItemList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumMovimentoEncontrado"/>
			</strong>
		</div>
	</c:if>
	<c:if test="${compensacaoItemList ne null and !empty compensacaoItemList}">
		<div class="table-responsive">
			<table id="tblCompensacaoItem" class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="n_" /></th>
						<th class="cm-th"><fmt:message key="data" /></th>
						<th class="cm-th"><fmt:message key="descricao" /></th>
						<th class="cm-th"><fmt:message key="debito" /></th>
						<th class="cm-th"><fmt:message key="credito" /></th>
						<th class="cm-th"><fmt:message key="saldo" /></th>
					</tr>
				</thead>
				<tbody id="tbyCompensacaoItem">
					<c:forEach var="compensacaoItem" items="${compensacaoItemList}">
						<c:if test="${compensacaoItem.item eq null}">
							<tr data-id="${compensacaoItem.id}" data-index="${compensacaoItem.seq}"<c:if test="${compensacaoItem.baixaItem ne null}"> data-baixa-item="${compensacaoItem.baixaItem.id}"</c:if><c:if test="${compensacaoItem.transferencia ne null}"> data-transferencia="${compensacaoItem.transferencia.id}"</c:if> data-tipo="${compensacaoItem.tipo}">
								<td class="text-right">${compensacaoItem.seq + 1}</td>
								<td class="text-center"><joda:format pattern="${patternDate}" value="${compensacaoItem.baixaItem eq null ? compensacaoItem.transferencia.data : compensacaoItem.baixaItem.baixa.data}" /></td>
								<td>
									
									<%-- Lancamento --%>
									<c:if test="${compensacaoItem.baixaItem ne null}">
										<c:out value="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.favorecido.displayNome}" />
									</c:if>
									
									<%-- Transferencia --%>
									<c:if test="${compensacaoItem.transferencia ne null}">
										
										<fmt:message key="transf_" />
										<c:if test="${compensacaoItem.transferencia.conta ne null}">
											&nbsp;<c:out value="${compensacaoItem.transferencia.conta.nome}"/>
										</c:if>
										<c:if test="${compensacaoItem.transferencia.cartaoCredito ne null}">
											&nbsp;<c:out value="${compensacaoItem.transferencia.cartaoCredito.nome}"/>
										</c:if>
										<c:if test="${compensacaoItem.transferencia.cheque ne null}">
											&nbsp;<fmt:message key="MeioPagamento.CHQ"/>
											&nbsp;<c:out value="${compensacaoItem.transferencia.cheque.numero}"/>
										</c:if>
										&nbsp;<fmt:message key="p_" />
										&nbsp;<c:out value="${compensacaoItem.transferencia.destino.nome}"/>
									</c:if>
									
									<%-- Nota Fiscal --%>
									<c:if test="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.tipoDoc ne null}">
										&nbsp;-&nbsp;<fmt:message key="DocTipo.${compensacaoItem.baixaItem.lancamentoParcela.lancamento.tipoDoc}" />
										<c:if test="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.numeroDoc ne null}">
											&nbsp;-&nbsp;<c:out value="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.numeroDoc}" />
										</c:if>
										<c:if test="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.cartaoCredito ne null}">
											&nbsp;<c:out value="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.cartaoCredito.nome}" />
										</c:if>
									</c:if>
									
									<%-- Meio --%>
									<c:if test="${compensacaoItem.baixaItem ne null}">
										&nbsp;-&nbsp;<fmt:message key="MeioPagamento.${compensacaoItem.baixaItem.meio}" />
										
										<c:if test="${compensacaoItem.baixaItem.cheque ne null}">
											&nbsp;${compensacaoItem.baixaItem.cheque.numero}
										
											<c:if test="${compensacaoItem.baixaItem.cheque.banco ne null}">
												&nbsp;<c:out value="${compensacaoItem.baixaItem.cheque.banco.displayNome}" />
											</c:if>
										
										</c:if>
										
										<c:if test="${compensacaoItem.baixaItem.lancamentoParcela.lancamento.quantLancamentoParcela gt 1}">
											&nbsp;-&nbsp;${compensacaoItem.baixaItem.lancamentoParcela.seq + 1}&nbsp;/&nbsp;${compensacaoItem.baixaItem.lancamentoParcela.lancamento.quantLancamentoParcela}
										</c:if>
										
										
									
									</c:if>
								
								</td>
								<td class="text-right font-red"><fmt:formatNumber type="number" value="${compensacaoItem.debito}" maxFractionDigits="2" minFractionDigits="2" /></td>
								<td class="text-right font-blue"><fmt:formatNumber type="number" value="${compensacaoItem.credito}" maxFractionDigits="2" minFractionDigits="2" /></td>
								<td class="text-right">
									<c:choose>
										<c:when test="${compensacaoItem.saldo lt 0}">
											<span class="font-red"><fmt:formatNumber type="number" value="${compensacaoItem.saldo}" maxFractionDigits="2" minFractionDigits="2" /></span>		
										</c:when>
										<c:otherwise>
											<span class="font-blue"><fmt:formatNumber type="number" value="${compensacaoItem.saldo}" maxFractionDigits="2" minFractionDigits="2" /></span>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:if>
						<c:if test="${compensacaoItem.item ne null}">
							<tr data-id="${compensacaoItem.id}" data-index="${compensacaoItem.seq}" data-item="${compensacaoItem.item.id}" data-tipo="${compensacaoItem.tipo}">
								<td class="text-right">${compensacaoItem.seq + 1}</td>
								<td class="text-center"><joda:format pattern="${patternDate}" value="${compensacaoItem.data}" /></td>
								<td>${compensacaoItem.item.displayName}</td>
								<td class="text-right font-red"><fmt:formatNumber type="number" value="${compensacaoItem.debito}" maxFractionDigits="2" minFractionDigits="2" /></td>
								<td class="text-right font-blue"><fmt:formatNumber type="number" value="${compensacaoItem.credito}" maxFractionDigits="2" minFractionDigits="2" /></td>
								<td class="text-right">
									<c:choose>
										<c:when test="${compensacaoItem.saldo lt 0}">
											<span class="font-red"><fmt:formatNumber type="number" value="${compensacaoItem.saldo}" maxFractionDigits="2" minFractionDigits="2" /></span>		
										</c:when>
										<c:otherwise>
											<span class="font-blue"><fmt:formatNumber type="number" value="${compensacaoItem.saldo}" maxFractionDigits="2" minFractionDigits="2" /></span>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr class="cm-tr-total">
						<td class="text-right" colspan="3"><fmt:message key="total"/> :</td>
						<td class="text-right font-red">
							<c:choose>
								<c:when test="${compensacao.totalDebito eq 0}">&nbsp;</c:when>
								<c:otherwise>
									<fmt:formatNumber type="number" value="${compensacao.totalDebito}" maxFractionDigits="2" minFractionDigits="2" />		
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text-right font-blue">
							<c:choose>
								<c:when test="${compensacao.totalCredito eq 0}">&nbsp;</c:when>
								<c:otherwise>
									<fmt:formatNumber type="number" value="${compensacao.totalCredito}" maxFractionDigits="2" minFractionDigits="2" />
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text-right" id="lbSaldo">
							<c:choose>
								<c:when test="${compensacao.total lt 0}">
									<span class="font-red"><fmt:formatNumber type="number" value="${compensacao.total}" maxFractionDigits="2" minFractionDigits="2" /></span>
								</c:when>
								<c:otherwise>
									<span class="font-blue"><fmt:formatNumber type="number" value="${compensacao.total}" maxFractionDigits="2" minFractionDigits="2" /></span>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:if>
</div>