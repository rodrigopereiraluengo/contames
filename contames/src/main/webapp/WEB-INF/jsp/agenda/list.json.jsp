<%@page trimDirectiveWhitespaces="true" contentType="application/json; charset=ISO-8859-1" %>
<json:object>
	<json:property name="success" value="1" />
	<json:array name="result" var="agenda" items="${agendaList}">
		<json:object>
			<json:property name="id" value="${agenda.id}" />			
			
			<c:if test="${agenda.id ne null}">
				<json:property name="title" value="${agenda.nome}" />
				<json:property name="url" value="javascript:selecionarEvento(${agenda.id})" />
			</c:if>
			<c:if test="${agenda.id eq null}">
				<fmt:formatNumber var="valor" type="number" value="${agenda.valor}" maxFractionDigits="2" minFractionDigits="2" />
				<json:property name="title" value="${agenda.nome}: ${fn:replace(valor, '-', '')}" />
				<json:property name="url" value="javascript:relatorioDataOpenDialogLink({'tipo': '${agenda._tipo}', 'lancamentoParcela ': '${agenda.lancamentoparcela_id}', 'lancamento': '${agenda.lancamento_id}', 'baixaItem': '${agenda.baixaitem_id}', 'baixa': '${agenda.baixa_id}', 'transferencia': '${agenda.transferencia_id}', 'compensacaoItem': '${agenda.compensacaoitem_id}', 'compensacao': '${agenda.compensacao_id}', 'favorecido': '${agenda.favorecido_id}', 'conta': '${agenda.conta_id}', 'cartaoCredito': '${agenda.cartaocredito_id}', 'talaoCheque': '${agenda.talaocheque_id}'})" />
			</c:if>
			<json:property name="class" value="event-${fn:toLowerCase(agenda.tipo)}" />
			<json:property name="start" value="${f:sqlTimestampToMillis(agenda.inicio)}" />
			<json:property name="end" value="${f:sqlTimestampToMillis(agenda.fim)}" />
		</json:object>
	</json:array>
</json:object>