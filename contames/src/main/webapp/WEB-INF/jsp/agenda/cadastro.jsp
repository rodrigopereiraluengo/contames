<fmt:message key="patternDateTime" var="patternDateTime" />
<div id="dialogAgenda" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmAgenda">
			
				<input type="hidden" name="agenda.id" value="${agenda.id}" />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogAgendaTitle"><fmt:message key="agenda"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid">
					
						<div class="row">
						
							<div class="col-xs-12 col-sm-8 form-group">
							
								<label class="control-label" for="agendaNome">
	  								<fmt:message key="nome" />
	  								<span class="required">*</span>
	  							</label>
	  							<input type="text" id="agendaNome" name="agenda.nome" value="<c:out value="${agenda.nome}"/>" data-category="nome agenda.nome" class="form-control" />
							
							</div>
							
							<div class="col-xs-12 col-sm-4 form-group">
							
								<label class="control-label" for="agendaTipo">
	  								<fmt:message key="cor" />
	  								<span class="required">*</span>
	  							</label>
	  							<select id="agendaTipo" name="agenda.tipo" data-category="tipo" class="form-control">
	  								<option value="SUCCESS"<c:if test="${agenda.tipo eq 'SUCCESS'}"> selected="selected"</c:if>><fmt:message key="AgendaTipo.SUCCESS" /></option>
	  								<option value="WARNING"<c:if test="${agenda.tipo eq 'WARNING'}"> selected="selected"</c:if>><fmt:message key="AgendaTipo.WARNING" /></option>
	  								<option value="IMPORTANT"<c:if test="${agenda.tipo eq 'IMPORTANT'}"> selected="selected"</c:if>><fmt:message key="AgendaTipo.IMPORTANT" /></option>
	  								<option value="INFO"<c:if test="${agenda.tipo eq 'INFO'}"> selected="selected"</c:if>><fmt:message key="AgendaTipo.INFO" /></option>
	  								<option value="INVERSE"<c:if test="${agenda.tipo eq 'INVERSE'}"> selected="selected"</c:if>><fmt:message key="AgendaTipo.INVERSE" /></option>
	  								<option value="SPECIAL"<c:if test="${agenda.tipo eq 'SPECIAL'}"> selected="selected"</c:if>><fmt:message key="AgendaTipo.SPECIAL" /></option>
	  							</select>
							
							</div>
						
						</div>
						
						<div class="row">
						
							<div class="col-xs-12 col-sm-6 form-group">
							
								<label class="control-label" for="agendaInicio">
	  								<fmt:message key="inicio" />
	  								<span class="required">*</span>
	  							</label>
	  							<div class="input-group">
	  								<input type="text" id="agendaInicio" name="agenda.inicio" value="<joda:format pattern="${patternDateTime}" value="${agenda.inicio}"/>" data-category="inicio agenda.inicio" class="form-control" />
	  								<span class="input-group-btn">
		  								<button class="btn btn-default" type="button" id="btAgendaInicio">
		  									<i class="fa fa-calendar-o"></i>
	       								</button>
									</span>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-6 form-group">
							
								<label class="control-label" for="agendaFim">
	  								<fmt:message key="fim" />
	  								<span class="required">*</span>
	  							</label>
	  							<div class="input-group">
	  								<input type="text" id="agendaFim" name="agenda.fim" value="<joda:format pattern="${patternDateTime}" value="${agenda.fim}"/>" data-category="fim agenda.fim" class="form-control" />
	  								<span class="input-group-btn">
		  								<button class="btn btn-default" type="button" id="btAgendaFim">
		  									<i class="fa fa-calendar-o"></i>
	       								</button>
									</span>
								</div>
							</div>
						
						</div>
						
						<div class="row">
							<div class="col-xs-12 form-group">
								
								<label class="control-label" for="agendaPessoa">
	  								<fmt:message key="pessoa" />
	  							</label>
								<div class="input-group">
									<input 
	  									type="text" 
	  									id="agendaPessoa" 
	  									name="agenda.pessoa.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoas&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${agenda ne null and agenda.pessoa ne null}"> value="${agenda.pessoa.displayNome}" data-val="${agenda.pessoa.id}"</c:if>
			    						data-category="pessoa"
		    						/>
			  						<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
	       									<i class="fa fa-plus"></i>
										</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
	       									<i class="fa fa-pencil"></i>
	       								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
											<i class="fa fa-search"></i>
	       								</button>
	   								</span>
		  						</div>
							</div>
						</div>
						
						<div class="form-group">
  							<label class="control-label" for="agendaObs"><fmt:message key="obs" /></label>
  							<textarea id="agendaObs" name="agenda.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${agenda ne null and agenda.obs ne null}"><c:out value="${agenda.obs}"/></c:if></textarea>
  						</div>
  						
  						<div class="form-group">
  						
  							<label class="control-label" for="agendaUsuario">
  								<fmt:message key="salvarAgendaDe" />
  							</label>
  							<select id="agendaUsuario" name="agenda.usuario.id" data-category="usuario" class="form-control">
  								<option value=""><fmt:message key="todos___" /></option>
  								<c:forEach var="usuario" items="${usuarioList}">
  									<option value="${usuario.id}"<c:if test="${(agenda eq null and usuario eq usuarioLogged.usuario) or (agenda ne null and agenda.usuario eq usuario)}"> selected="selected"</c:if>><c:out value="${usuario.displayNome}" /></option>
  								</c:forEach>
  							</select>
  							
  						</div>
					
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
		        </div>
		       	
		       	<c:if test="${agenda ne null and agenda.id ne null}">
			       	<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${agenda.criacao}" create="create" pessoa="${agenda.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${agenda.alteracao}" create="update" pessoa="${agenda.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/agenda.cadastro.js?v${applicationVersion}"></script>