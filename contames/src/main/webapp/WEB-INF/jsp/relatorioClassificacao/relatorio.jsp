<fmt:message key="patternDate" var="patternDate" />
<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">
	
	<tiles:putAttribute name="head">
		<!--[if lte IE 8]>
        	<script src="${pageContext.request.contextPath}/resources/js/excanvas.js?v${applicationVersion}"></script>
    	<![endif]-->
    	<script src="${pageContext.request.contextPath}/resources/js/Chart.min.js?v${applicationVersion}"></script>
    	<script type="text/javascript">
			var labelsDespesas;
			var labelsReceitas;
			var despesas;
			var receitas;
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="relatorioPor.classificacao"/></h1>
						<p class="description"><fmt:message key="relatorioPor.classificacao.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="resumo" />
							</li>
				  			<li><fmt:message key="relatorios" /></li>
				  			<li><fmt:message key="classificacao" /></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<form 
			id="frmRelatorioClassificacao" 
			action="${pageContext.request.contextPath}/relatorios/classificacao" 
			class="data-table">
			
			<div class="panel panel-default">
			
				<div class="panel-heading cm-panel-heading-toolbar">
				
					<div class="container-fluid">
					
						<div class="row form-horizontal">
						
							<!-- Ano -->
							<div class="col-xs-12 col-sm-2">
						
								<div class="form-group">
									<label for="relatorioClassificacaoAno" class="col-sm-4 control-label"><fmt:message key="ano"/></label>
								    <div class="col-sm-8">
							      		<select id="relatorioClassificacaoAno" name="ano" class="form-control">
							      			<c:forEach var="mapAno" items="${resultAnoMap}">
							      				<option value="${mapAno['ano']}"<c:if test="${ano eq mapAno['ano']}"> selected="selected"</c:if>>${mapAno['ano']}</option>
							      			</c:forEach>
							      		</select>
								    </div>
								</div>
								
							</div>
							
							<!-- Classificacao -->
							<div class="col-xs-12 col-sm-3">
								
								<div class="form-group">
									<label for="relatorioClassificacaoClassificacao" class="col-sm-4 control-label"><fmt:message key="classificacao"/></label>
								    <div class="col-sm-8">
								      	<input type="text" id="relatorioClassificacaoClassificacao" class="form-control" />
								    </div>
								</div>
								
							</div>
							
							<!-- Tipo -->
							<div class="col-xs-12 col-sm-2">
							
								<div class="form-group">
									<label for="relatorioClassificacaoTipo" class="col-sm-4 control-label"><fmt:message key="tipo"/></label>
								    <div class="col-sm-8">
								      	<select id="relatorioClassificacaoTipo" name="tipo" class="form-control">
								      		<option value=""><fmt:message key="todos___" /></option>
								      		<option value="DES"><fmt:message key="ItemTipo.DESs"/></option>
								      		<option value="REC"><fmt:message key="ItemTipo.RECs"/></option>
								      	</select>
								    </div>
								</div>
							
							</div>
							
							<div class="col-xs-12 col-sm-1">
								<div class="form-group col-xs-12">
									<input type="checkbox" id="isCCred" checked="checked" value="true" />
								</div>
							</div>
							
							<!-- Status -->
							<div class="col-xs-12 col-sm-2">
								
								<div class="form-group">
									<label for="relatorioClassificacaoStatus" class="col-sm-4 control-label"><fmt:message key="status"/></label>
									<div class="col-sm-8">
										<select id="relatorioClassificacaoStatus" name="statusList[]" multiple="multiple" class="form-control" style="display: none">
											<option value="STD"><fmt:message key="LancamentoStatus.STD"/></option>
											<option value="LAN" selected="selected"><fmt:message key="LancamentoStatus.LAN"/></option>
											<option value="BXD" selected="selected"><fmt:message key="LancamentoStatus.BXD"/></option>
											<option value="COM" selected="selected"><fmt:message key="LancamentoStatus.COM"/></option>
											<option value="CAN"><fmt:message key="LancamentoStatus.CAN"/></option>
										</select>
									</div>
								</div>
								
							</div>
							
							<!-- Botoes -->
							<div class="col-xs-12 col-sm-2">
								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										<div class="btn-group" role="group">
										  	<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
										  	<a id="btExportXls" class="btn btn-default" title="<fmt:message key="exportarParaExcel"/>"><i class="fa fa-file-excel-o"></i></a>
										  	<a id="btExportPdf" class="btn btn-default" title="<fmt:message key="exportarParaPdf"/>"><i class="fa fa-file-pdf-o"></i></a>
										</div>
									</div>
								</div>
							</div>
						
						</div>
						
					</div>
					
				</div>
				
				<div id="layChart" class="container-fluid" style="display: none">
					<div class="row">
						<div class="col-xs-6"><p class="form-control-static text-center font-bold font-red"><fmt:message key="despesas" /></p></div>
						<div class="col-xs-6"><p class="form-control-static text-center font-bold font-blue"><fmt:message key="receitas" /></p></div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<canvas id="chartRelatorioClassificacaoDes" class="rel-canvas-chart"></canvas>
						</div>
						<div class="col-xs-6">
							<canvas id="chartRelatorioClassificacaoRec" class="rel-canvas-chart"></canvas>
						</div>
					</div>
					<div class="row">
						<p class="form-control-static text-center font-bold font-red"><fmt:message key="grafico.dados.apenasCarregado" /></p>
					</div>
				</div>
				
				<div class="result search-table">
					<jsp:include page="relatorio_table.jsp" />
	   			</div>
				
			</div>
				
		</form>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/relatorioClassificacao.relatorio.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>