<fmt:message key="patternDate" var="patternDate" />
<div id="dialogRelatorioClassificacaoMovs" class="modal cm-modal-data-table" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmRelatorioClassificacaoMovs" action="${pageContext.request.contextPath}/relatorioClassificacao/movs">
			
				<input type="hidden" name="item_id" value="${param.item_id}" />
				<input type="hidden" name="ano" value="${param.ano}" />
				<input type="hidden" name="tipo" value="${param.tipo}" />
				<input type="hidden" name="mes"value="${param.mes}" />
				
				<c:forEach var="classificacao" items="${classificacaoList}" varStatus="st">
					<input type="hidden" name="classificacaoList[${st.index}]" value="${classificacao}" />
				</c:forEach>
				
				<c:forEach var="status" items="${statusList}" varStatus="st">
					<input type="hidden" name="statusList[${st.index}]" value="${status}"/>
				</c:forEach>
						
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogRelatorioClassificacaoMovsTitle"><fmt:message key="movimentos"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="data-table panel panel-default">
						
						<div class="result search-table">
							<jsp:include page="movs_table.jsp" />
	   					</div>
						
					</div>
				
				</div>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/relatorioClassificacao.movs.js?v${applicationVersion}"></script>