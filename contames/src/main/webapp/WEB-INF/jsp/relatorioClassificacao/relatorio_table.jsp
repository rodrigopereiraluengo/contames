<fmt:message key="patternDate" var="patternDate" />
<script type="text/javascript">
	labelsDespesas = [];
	labelsReceitas = [];
	despesas = [];
	receitas = [];
</script>
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumRegistroEncontrado"/>
		</strong>
	</div>
	<script type="text/javascript">
		$(function(){ $('#layChart').hide(); });
	</script>
</c:if>
<c:if test="${count > 0}">
	<c:set var="path" value="relatorios/classificacao" />
	
	<div class="table-responsive">
		
		<table class="table table-bordered">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="classificacao" column="classificacao_nome" page="${page}" path="${path}" />
					<c:forEach var="i" begin="1" end="12">
						<cmt:th orderByList="${orderByList}" index="${i}" name="mesAbr_${i}" column="valor_${i}" page="${page}" path="${path}" />
					</c:forEach>
					<cmt:th orderByList="${orderByList}" index="12" name="total" column="valor" page="${page}" path="${path}" />
				</tr>
			</thead>
			<tbody id="tbyRelatorioClassificacao">
				<jsp:include page="relatorio_table_tr.jsp" />
			</tbody>
			
			<!-- RODAPE -->
			<tfoot>
				
				<!-- TOTAL -->
				<tr id="trTotal" class="cm-tr-total">
					<td class="text-right lt2"><fmt:message key="total"/></td>
					<c:forEach var="i" begin="1" end="12">
						<c:set var="key_valor" value="valor_${i}"/>
						<c:set var="key_des" value="despesas_${i}"/>
						<c:set var="key_rec" value="receitas_${i}"/>
						<td class="text-right lb-valor lt2">
							<c:if test="${resultTotalMap[key_valor] ne 0}">
								<fmt:formatNumber var="valor" type="number" value="${resultTotalMap[key_valor]}" maxFractionDigits="2" minFractionDigits="2" />
								<a href="javascript:;" onclick="relatorioClassificacaoOpenMovs({mes: ${i}, classificacao_id: 0})" class="${resultTotalMap[key_valor] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
							</c:if>
						</td>
					</c:forEach>
					<td class="text-right lt2">
						<c:if test="${resultTotalMap['valor'] ne 0}">
							<fmt:formatNumber var="valor" type="number" value="${resultTotalMap['valor']}" maxFractionDigits="2" minFractionDigits="2" />
							<a href="javascript:;" onclick="relatorioClassificacaoOpenMovs({classificacao_id: 0})" class="${resultTotalMap['valor'] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
						</c:if>
					</td>
				</tr>
											
			</tfoot>
		</table>
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" isLazy="true" />
  		<cmt:paglaz orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>

	<script type="text/javascript">
		if(typeof relatorioClassificacaoDesOpenChart != 'undefined') {
			relatorioClassificacaoDesOpenChart();
			relatorioClassificacaoRecOpenChart();
		}
	</script>

</c:if>