<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumMovimentoEncontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
	
		<c:set var="path" value="relatorioClassificacao/movs"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="descricao" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" name="doc" column="numeroDoc_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" name="meio" column="meio_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" name="cCredConta" column="meio_conta" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="parcela" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" name="status" column="status_nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" name="valor" column="valor" page="${page}" path="${path}" />
				</tr>
			</thead>
			<tbody id="tbyMovs">
							
				<c:forEach var="rel" items="${relList}" varStatus="stRel">
					<tr 
						data-tipo="${rel['tipo']}"
						data-lancamento-parcela="${rel['lancamentoparcela_id']}" 
						data-lancamento="${rel['lancamento_id']}"
						data-baixa-item="${rel['baixaitem_id']}"
						data-baixa="${rel['baixa_id']}"
						data-transferencia="${rel['transferencia_id']}"
						data-compensacao-item="${rel['compensacaoitem_id']}"
						data-compensacao="${rel['compensacao_id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${rel['_data']}" /></td>
						<td data-favorecido="${rel['favorecido_id']}" data-item="${rel['item']}"<c:if test="${f:orderByContains('descricao', orderByList)}"> class="font-bold"</c:if>>${rel['descricao']}</td>
						<td<c:if test="${f:orderByContains('numeroDoc_clean', orderByList)}"> class="font-bold"</c:if>>
							${rel['numerodoc_nome']}
							<c:if test="${rel['fatcartaocredito_nome'] ne null}">
							&nbsp;<c:out value="${rel['fatcartaocredito_nome']}" />
							</c:if>
							<c:if test="${rel['arquivodoc_id'] ne null}">
								<a title="${rel['arquivodoc_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${rel['arquivodoc_id']}/download/${rel['arquivodoc_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('meio_nome', orderByList)}"> font-bold</c:if>">
							${rel['meio_nome']}
							<c:if test="${rel['boletotipo'] ne null and rel['boletonumero'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(rel['boletonumero']))}', title: '<fmt:message key="BoletoTipo.${rel['boletotipo']}" />', stopPropagation: true}, event)" role="button" title="<fmt:message key="codigoBarras"/>">
									<i class="fa fa-barcode"></i>
								</a>
							</c:if>
							<c:if test="${rel['arquivo_id'] ne null}">
								<a title="${rel['arquivo_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${rel['arquivo_id']}/download/${rel['arquivo_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td 
							data-conta="${rel['conta_id']}" 
							data-cartao-credito="${rel['cartaocredito_id']}"
							data-talao-cheque="${rel['talaocheque_id']}" 
							class="text-center<c:if test="${f:orderByContains('meio_conta', orderByList)}"> font-bold</c:if>">${rel['meio_conta']}</td>
						<td class="text-center<c:if test="${f:orderByContains('parcela', orderByList)}"> font-bold</c:if>">${rel['parcela']}</td>
						<td class="text-center<c:if test="${f:orderByContains('status_nome', orderByList)}"> font-bold</c:if>">${rel['status_nome']}</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>">
							<fmt:formatNumber var="valor" type="number" value="${rel['valor']}" maxFractionDigits="2" minFractionDigits="2" />
							<span class="${rel['valor'] lt 0 ? 'font-red' : 'font-blue'}">
								${fn:replace(valor, '-', '')}
								<c:if test="${rel['comprovante_id'] ne null}">
									<a title="${rel['comprovante_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${rel['comprovante_id']}/download/${rel['comprovante_nome']}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
							</span>
						</td>
					</tr>
				</c:forEach>
				
			</tbody>
			<tr class="cm-tr-total">
				<td class="text-right" colspan="7"><fmt:message key="total"/></td>
				<td class="text-right">
					<fmt:formatNumber var="valor" type="number" value="${sum}" maxFractionDigits="2" minFractionDigits="2" />
					<span class="${sum lt 0 ? 'font-red' : 'font-blue'}">
						${fn:replace(valor, '-', '')}
					</span>
				</td>
			</tr>
		</table>
				
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>