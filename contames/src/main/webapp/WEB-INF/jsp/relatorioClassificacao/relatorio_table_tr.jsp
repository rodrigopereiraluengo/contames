<c:url var="_path" value="${pageContext.request.contextPath}/relatorios/classificacao">
	<c:forEach var="orderBy" items="${orderByList}">
		<c:param name="orderByList[].column" value="${orderBy.column}" />
		<c:param name="orderByList[].direction" value="${orderBy.direction}" />
		<c:param name="orderByList[].index" value="${orderBy.index}" />
	</c:forEach>
	<c:param name="page" value="${page}" />
	<c:param name="isTable" value="true" />
</c:url>
<c:forEach var="rel" items="${relList}" varStatus="stRel">
	<tr>
		<td class="text-nowrap nivel-${nivel}">
			<c:if test="${rel['classificacao_id'] ne null}">
				<c:url var="pathClassif" value="${_path}">
					<c:param name="classificacao_id" value="${rel['classificacao_id']}" />
					<c:choose>
						<c:when test="${nivel eq 'CAT'}">
							<c:param name="nivel" value="FAM" />
						</c:when>
						<c:when test="${nivel eq 'FAM'}">
							<c:param name="nivel" value="TIP" />
						</c:when>
						<c:when test="${nivel eq 'TIP'}">
							<c:param name="nivel" value="DET" />
						</c:when>
					</c:choose>
				</c:url>
				<a href="javascript:;" onclick="relatorioClassificacaoOpenClassif(this, '${pathClassif}')">
					<i class="fa fa-plus-square"></i>
					<c:out value="${rel['classificacao_nome']}"/>
				</a>
			</c:if>
			<c:if test="${rel['classificacao_id'] eq null and rel['item_id'] eq null}">
				<span><c:out value="${rel['classificacao_nome']}"/></span>
			</c:if>
			<c:if test="${rel['item_id'] ne null}">
				<a href="javascript:;" onclick="relatorioClassificacaoOpenItem(${rel['item_id']})">
					<c:out value="${rel['classificacao_nome']}"/>
				</a>
			</c:if>
			
			<c:if test="${nivel eq 'CAT' or rel['classificacao_id'] eq null}">
				<c:if test="${rel['valor'] < 0}">
					<script type="text/javascript">
						labelsDespesas.push('<c:out value="${rel['classificacao_nome']}"/>');
						despesas.push(${-rel['valor']});
					</script>
				</c:if>
				<c:if test="${rel['valor'] > 0}">
					<script type="text/javascript">
						labelsReceitas.push('<c:out value="${rel['classificacao_nome']}"/>');
						receitas.push(${rel['valor']});
					</script>
				</c:if>
			</c:if>
		</td>
		<c:forEach var="i" begin="1" end="12">
			<c:set var="key_valor" value="valor_${i}"/>
			<fmt:formatNumber var="valor" type="number" value="${rel[key_valor]}" maxFractionDigits="2" minFractionDigits="2" />
			<td class="text-right lb-valor lr2">
				<c:if test="${rel[key_valor] ne 0}">
					<a href="javascript:;" onclick="relatorioClassificacaoOpenMovs({classificacao_id: '${rel['classificacao_id']}', item_id: '${rel['item_id']}', mes: ${i}})" class="${rel[key_valor] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
				</c:if>
			</td>
		</c:forEach>
		<td class="text-right">
			<fmt:formatNumber var="valor" type="number" value="${rel['valor']}" maxFractionDigits="2" minFractionDigits="2" />
			<c:if test="${rel['valor'] ne 0}">
				<a href="javascript:;" onclick="relatorioClassificacaoOpenMovs({classificacao_id: '${rel['classificacao_id']}', item_id: '${rel['item_id']}'})" class="${rel['valor'] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
			</c:if>
		</td>
	</tr>
</c:forEach>