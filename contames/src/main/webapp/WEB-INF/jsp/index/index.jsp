<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	
	<tiles:putAttribute name="body">
	
		<div class="jumbotron home-main">
				
			<div class="home-main-description">
				<h1>Organize suas contas a pagar e receber.</h1>
				<br />
				<br />
				<p><i class="fa fa-calendar"></i>&nbsp;&nbsp;Agende pagamentos e recebimentos.</p>
				
				<p><i class="fa fa-credit-card"></i>&nbsp;&nbsp;Gerencie contas banc�rias, cart�es de cr�dito e cheques.</p>
				
				<p><i class="fa fa-sitemap"></i>&nbsp;&nbsp;Saiba como voc� gasta seu dinheiro e economize.</p>
				
				<p><i class="fa fa-check-square"></i>&nbsp;&nbsp;Concilie seus saldos cont�beis.</p>
			</div>
			
			<div class="container">
				<div class="row row-thumb-features cm-thumb-descr">
				
					<div class="col-xs-12 col-sm-6 col-md-3">
					    <div class="thumbnail">
					    	<div class="cm-thumb-icon">
					    		<i class="fa fa-arrow-up font-red"></i>
					    		<i class="fa fa-arrow-down font-blue"></i>
				    		</div>
					      	<div class="caption">
					        	<h3>Despesas e Receitas</h3>
					        	<p>Organize suas Despesas e Receitas classificando por diferentes n�veis.</p>
					        	<p><a href="${pageContext.request.contextPath}/como-funciona#despesas-receitas" class="btn btn-primary" role="button">Saiba mais</a></p>
					      	</div>
					    </div>
				  	</div>
			  	
				  	<div class="col-xs-12 col-sm-6 col-md-3">
					    <div class="thumbnail">
					    	<div class="cm-thumb-icon"><i class="fa fa-paperclip"></i></div>
					      	<div class="caption">
					        	<h3>Arquivos</h3>
					        	<p>Anexe Notas Fiscais, Recibos, Faturas de Cart�o de Cr�dito ou outros documentos.</p>
					        	<p><a href="${pageContext.request.contextPath}/como-funciona#lancamentos" class="btn btn-primary" role="button">Saiba mais</a></p>
					      	</div>
					    </div>
				  	</div>
				  	
				  	<div class="col-xs-12 col-sm-6 col-md-3">
					    <div class="thumbnail">
					    	<div class="cm-thumb-icon">%</div>
					      	<div class="caption">
					        	<h3>Evite multa e juros</h3>
					        	<p>Receba diariamente um e-mail contendo todos os pagamentos e recebimentos que ocorrer�o no dia.</p>
					        	<p><a href="${pageContext.request.contextPath}/como-funciona#avisos" class="btn btn-primary" role="button">Saiba mais</a></p>
					      	</div>
					    </div>
				  	</div>
			  	
				  	<div class="col-xs-12 col-sm-6 col-md-3">
					    <div class="thumbnail">
					    	<div class="cm-thumb-icon"><i class="fa fa-bar-chart"></i></div>
					      	<div class="caption">
					        	<h3>Relat�rios</h3>
					        	<p>Utilize os diversos relat�rios para rastrear como seu dinheiro � gasto, identifique desperd�cios e economize.</p>
					        	<p><a href="${pageContext.request.contextPath}/como-funciona#relatorios" class="btn btn-primary" role="button">Saiba mais</a></p>
					      	</div>
					    </div>
				  	</div>
				
				</div>
			</div>
		</div>
		
	</tiles:putAttribute>
	
</tiles:insertTemplate>