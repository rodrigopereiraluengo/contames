<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="demonstracao" /> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="demonstracao"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="demonstracao" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="panel panel-default">
		  	<div class="panel-body">
		    	
		   		<div class="container-fluid cm-container-tutorial">
						
					<h2>Lan�amento</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tAAsGGcm2g4" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Pagamento</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PJEG1Q0zPFU" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Transfer�ncia</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uYUS5ajP59k" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Concilia��o</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uiVQRGpDQtU" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Conciliando Cart�o de Cr�dito</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dxRCRxnTGI4" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Cadastrar Despesas</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/K-ILojmvMNU" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Cadastrar Pessoas</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/3mLASRnkVBc" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Cadastrar Contas</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tCO7ivSmn7c" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Cadastrar Tal�o de Cheque</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CMlexUbFBQo" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Cadastrar Cart�o de Cr�dito</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/kZ0bh33Rzuk" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
					
					<h2>Lan�amento R�pido</h2>
					<br />
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/u6HZ33l8W7M" frameborder="0" allowfullscreen></iframe>
					</div>
					<br />
						   		
		   		</div>
			
			</div>
		
		</div>
		
	</tiles:putAttribute>
	
</tiles:insertTemplate>