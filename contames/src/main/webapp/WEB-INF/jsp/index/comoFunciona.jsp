<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="comoFunciona" /> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="comoFunciona"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="comoFunciona" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="panel panel-default">
		  	<div class="panel-body">
		    	
		   		<div class="container-fluid cm-container-tutorial">
		   			
		   			<h2>Organize contas a pagar e receber</h2>
		   			
		   			<p>O contames.com.br � a ferramenta ideal para quem precisa organizar finan�as de maneira r�pida e simples. Com ele voc� conseguir� identificar exatamente como seu dinheiro � gasto.</p>
		   			
		   			<h2>Administre em tr�s etapas principais</h2>
					<br/>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="thumbnail">
						    	<div class="caption">
						        	<h3>1� Lan�amentos de Despesas ou Receitas</h3>
						        	<p>Esta etapa funciona como uma agenda, selecione Despesas ou Receitas e informe quais Pagamentos ou Recebimentos ocorrer�o no futuro.</p>
						        </div>
						    </div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="thumbnail">
						    	<div class="caption">
						        	<h3>2� Pagamentos ou Recebimentos</h3>
						        	<p>Esta etapa acontece quando o Pagamento ou Recebimento de fato ocorreu.</p>
						        </div>
						    </div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="thumbnail">
						    	<div class="caption">
						        	<h3>3� Concilia��o</h3>
						        	<p>Esta etapa basicamente consiste em conferir se o que foi pago ou recebido est� no saldo da sua conta ou na fatura do cart�o de cr�dito.</p>
						        </div>
						    </div>
						</div>
					</div>
					
					<h3><a id="despesas-receitas">Despesas ou Receitas</a></h3>
				
					<p>Por padr�o j� existem v�rias despesas e receitas cadastradas, altere ou exclua caso necess�rio.</p>
					
					<p>Utilize a classifica��o para organizar hierarquicamente em at� 4 n�veis, altere as classifica��es existentes conforme necess�rio.</p>
				
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-despesas.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-despesas.jpg" /></a>
					<br />
					<br />
					
					<h3><a id="lancamentos">Lan�amentos</a></h3>
					
					<p>Informe a Data, Favorecido, Despesas ou Receitas, informa��es de Pagamentos ou Recebimentos e Rateios</p>
					
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-lancamento.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-lancamento.jpg" /></a>
					
					<h3><a id="pagamentos-recebimentos">Pagamentos ou Recebimentos</a></h3>
					
					<p>Selecione os Pagamentos ou Recebimentos que foram pagos ou recebidos.</p>
					
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-pagamento.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-pagamento.jpg" /></a>
					<br />
					<br />
					
					<p>Informe o meio de pagamento, selecione a conta, cart�o de cr�dito ou cheque.</p>
					
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-pagamento-2.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-pagamento-2.jpg" /></a>
					<br/>
					<br/>
					
					<h3><a id="conciliacao">Concilia��o</a></h3>
					
					<p>Selecione os movimentos que foram pagos ou recebidos, se necess�rio, adicione Despesas ou Receitas diretamente na Concilia��o</p>
					
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-conciliacao.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-conciliacao.jpg" /></a>
					<br/>
					<br/>
					
					<h3><a id="avisos">Aviso de Vencimento</a></h3>
					
					<p>Ao fazer um Lan�amento de Despesa ou Receita, no dia do vencimento voc� receber� um e-mail contendo todos os Pagamentos ou Recebimentos, com isso evite pagar multa e juros por atraso.</p>
					
				 	<a href="${pageContext.request.contextPath}/resources/img/como-funciona-aviso.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-aviso.jpg" /></a>
				 	<br /><br />
				 	<p>Se o dia do vencimento for s�bado ou domingo, voc� ser� avisado na sexta-feira.</p>
					<br/>
					<br/>
					
					<h3><a id="relatorios">Relat�rios</a></h3>
					<br/>
					<h4>Relat�rio por Data</h4>
					
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-relatorio-data.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-relatorio-data.jpg" /></a>
					<br/>
					<br/>
					<br/>
					<h4>Relat�rio por Despesas</h4>
					<a href="${pageContext.request.contextPath}/resources/img/como-funciona-relatorio-despesas.jpg"><img src="${pageContext.request.contextPath}/resources/img/como-funciona-relatorio-despesas.jpg" /></a>
					<br/>
					<br/>
		   			
		   			<h2>Acesse de qualquer lugar</h2>
		   			
		   			<p>Voc� n�o precisa instalar nada pois o contames.com.br � 100% online, acesse de qualquer lugar inclusive do seu smartphone, notebook etc, n�o se preocupe com planilhas complicadas e nem corra o risco de perde-las.</p>
		   			
		   			<h2>Sempre atualizado</h2>
		   			
		   			<p>O contames.com.br est� sempre em evolu��o, por ser online voc� receber� atualiza��o automaticamente aperfei�oando as funcionalidades existenstes e acrescentando novas funcionalidades.</p>
		   			
					<br/>
					Ainda tem muito mais recursos, aproveite, selecione um <a href="${pageContext.request.contextPath}/planos">Plano</a>, fa�a sua assinatura e comece a utilizar agora mesmo.
					<br/>
					<br/>
				</div>

		  	</div>
		</div>
	
	</tiles:putAttribute>
	
</tiles:insertTemplate>