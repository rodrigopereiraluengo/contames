<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="Organize suas contas a pagar e receber, saiba como seu dinheiro � utilizado e economize, f�cil, r�pido e seguro"/>
   		<meta name="keyword" content="contas a pagar, contas a receber, contas, contas banc�rias, cart�o de cr�dito, pagamentos e recebimentos, pagamentos, recebimentos, pagamento, recebimento, concilia��o, economizar, dinheiro, economizar dinheiro, relatorio financeiro, financeiro, finan�as, finan�as pessoais, finan�a pessoal, gerenciador financeiro, gest�o financeira, sistema financeiro, rateio, rastreie dinheiro, poupar, planilha despesas, vida financeira, f�cil, online, software, mobile, sistema web, aplicativo web"/>
   		<meta name="robots" content="index"/>
    	<!--[if IE]><link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico?v${applicationVersion}"><![endif]-->
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon.png?v${applicationVersion}" sizes="16x16">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-24.png?v${applicationVersion}" sizes="24x24">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-32.png?v${applicationVersion}" sizes="32x32">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-48.png?v${applicationVersion}" sizes="48x48">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-64.png?v${applicationVersion}" sizes="64x64">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-128.png?v${applicationVersion}" sizes="128x128">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-152.png?v${applicationVersion}" sizes="152x152">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-144.png?v${applicationVersion}" sizes="144x144">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-120.png?v${applicationVersion}" sizes="120x120">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-114.png?v${applicationVersion}" sizes="114x114">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-76.png?v${applicationVersion}" sizes="76x76">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-72.png?v${applicationVersion}" sizes="72x72">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-32.png?v${applicationVersion}" sizes="32x32">
		<meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon-144.png?v${applicationVersion}"/>
		<meta name="msapplication-TileColor" content="#177122"/>
		<title><tiles:insertAttribute name="title" flush="true" ignore="true"/><fmt:message key="site.titulo"/></title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css?v${applicationVersion}">
		<!--[if lt IE 9]>
      	<script src="${pageContext.request.contextPath}/resources/js/html5shiv.min.js?v${applicationVersion}"></script>
      	<script src="${pageContext.request.contextPath}/resources/js/respond.min.js?v${applicationVersion}"></script>
    	<![endif]-->
	</head>
	<body>
	
		<nav class="navbar navbar-default navbar-static-top cm-navbar">
  			<div class="container-fluid">
    			
    			<!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
			      	</button>
			      	<a class="navbar-brand home-logo" href="${pageContext.request.contextPath}/"></a>
			    </div>
			    
			    <!-- Collect the nav links, forms, and other content for toggling -->
    			<div class="collapse navbar-collapse" id="bs-navbar-collapse">
	      			<ul class="nav navbar-nav">
	        			<li<c:if test="${requestURL eq '/'}"> class="active"</c:if>>
	        				<a href="${pageContext.request.contextPath}/">
	        					<i class="fa fa-home"></i>
	        					<fmt:message key="home"/>
        					</a>
       					</li>
	        			<li<c:if test="${requestURL eq '/como-funciona'}"> class="active"</c:if>>
	        				<a href="${pageContext.request.contextPath}/como-funciona">
	        					<i class="fa fa-exclamation-circle"></i>
	        					<fmt:message key="comoFunciona"/>
        					</a>
        				</li>
        				<li<c:if test="${requestURL eq '/demonstracao'}"> class="active"</c:if>>
	        				<a href="${pageContext.request.contextPath}/demonstracao">
	        					<i class="fa fa-youtube-play"></i>
	        					<fmt:message key="demonstracao"/>
        					</a>
        				</li>
	        			<li<c:if test="${requestURL eq '/planos'}"> class="active"</c:if>>
	        				<a href="${pageContext.request.contextPath}/planos">
	        					<i class="fa fa-toggle-on"></i>
	        					<fmt:message key="planos"/>
        					</a>
       					</li>
	        			<li<c:if test="${requestURL eq '/contato'}"> class="active"</c:if>>
	        				<a href="${pageContext.request.contextPath}/contato">
	        					<i class="fa fa-envelope"></i>
	        					<fmt:message key="contato"/>
        					</a>
       					</li>
	        		</ul>
	      			<ul class="nav navbar-nav navbar-right">
	        			<li<c:if test="${requestURL eq '/entrar'}"> class="active"</c:if>>
	        				<a href="${pageContext.request.contextPath}/entrar">
	        					<i class="fa fa-sign-in"></i>
	        					<fmt:message key="login"/>
        					</a>
        				</li>
	        		</ul>
	   			</div><!-- /.navbar-collapse -->

  			</div>
		</nav>
		
		<tiles:insertAttribute name="breadcrumb" flush="true" ignore="true"/>
		
		<div class="container-fluid<c:if test="${requestURL eq '/'}"> home-container</c:if>">
		
			<tiles:insertAttribute name="body" flush="true" />
		
		</div>
		
		<div class="cm-footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-6 text-center"><p><fmt:message key="site.footer"/></p></div>
					<div class="col-xs-12 col-md-6 text-center"><p><fmt:message key="site.footer.suporte"/></p></div>
				</div>
			</div>
		</div>
		
		<env:supports key="development">
			<script type="text/javascript" src="${pageContext.request.contextPath}/dynamic/resources/js/i18n.js?v${applicationVersion}"></script>
		</env:supports>
		<env:supports key="production">
			<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/i18n.js?v${applicationVersion}"></script>
		</env:supports>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/components.js?v${applicationVersion}"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/dynamic/resources/js/vars.js?v${applicationVersion}"></script>
		<tiles:insertAttribute name="script" ignore="true" flush="true" />
		<env:supports key="production">
		<!-- Piwik -->
		<script type="text/javascript">
		  var _paq = _paq || [];
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
		    var u="//sio.contames.com.br/piwik/";
		    _paq.push(['setTrackerUrl', u+'piwik.php']);
		    _paq.push(['setSiteId', 1]);
		    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		  })();
		</script>
		<noscript><p><img src="//sio.contames.com.br/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
		<!-- End Piwik Code -->
		</env:supports>
	</body>
</html>