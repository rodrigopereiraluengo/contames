<div id="dialog${param.dialogId}" class="modal cm-modal-data-table" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-times"></i>
				</button>
				<h4 class="modal-title" id="dialog${param.dialogId}Title"><fmt:message key="${param.dialogTitle}"/></h4>
			</div>
			
			<div class="modal-body">
				<tiles:insertAttribute name="body" flush="true"/>
			</div>
			
			<c:if test="${param.dialogFootOk ne null}">
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default bt-cancel" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="button" class="btn btn-primary bt-ok" onclick="${param.dialogFootOk}" disabled="disabled">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
	       	</c:if>
			
       	</div>
   	</div>
</div>
<script type="text/javascript">
	
	$('#dialog${param.dialogId}').modal({show: true, keyboard: false, backdrop: 'static'}).on('hidden.bs.modal', function() {
		$(this).trigger('close').remove();
	});
	
</script>
<tiles:insertAttribute name="script" ignore="true" flush="true" />