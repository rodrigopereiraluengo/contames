<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="site.titulo"/></title>
	<!--[if IE]><link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico?v${applicationVersion}"><![endif]-->
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon.png?v${applicationVersion}">
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon.png?v${applicationVersion}" sizes="16x16">
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-24.png?v${applicationVersion}" sizes="24x24">
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-32.png?v${applicationVersion}" sizes="32x32">
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-48.png?v${applicationVersion}" sizes="48x48">
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-64.png?v${applicationVersion}" sizes="64x64">
	<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-128.png?v${applicationVersion}" sizes="128x128">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-152.png?v${applicationVersion}" sizes="152x152">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-144.png?v${applicationVersion}" sizes="144x144">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-120.png?v${applicationVersion}" sizes="120x120">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-114.png?v${applicationVersion}" sizes="114x114">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-76.png?v${applicationVersion}" sizes="76x76">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-72.png?v${applicationVersion}" sizes="72x72">
	<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-32.png?v${applicationVersion}" sizes="32x32">
	<meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon-144.png?v${applicationVersion}"/>
	<meta name="msapplication-TileColor" content="#177122"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css?v${applicationVersion}">
	<!--[if lt IE 9]>
		<script src="${pageContext.request.contextPath}/resources/js/html5shiv.min.js?v${applicationVersion}"></script>
		<script src="${pageContext.request.contextPath}/resources/js/respond.min.js?v${applicationVersion}"></script>
	<![endif]-->
	<tiles:insertAttribute name="head" ignore="true" flush="true" />
</head>
<body>

	<nav class="navbar navbar-default navbar-static-top cm-navbar">
 			
		<div class="container-fluid">
   			
			<!-- Small Menu -->
		    <div class="navbar-header">
		      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#cm-navbar">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
		      	</button>
		      	<a class="navbar-brand cm-logo-peq" href="${pageContext.request.contextPath}/resumo"></a>
		    </div>
			    
	    	
	    	<!-- Full Menu -->
			<div class="collapse navbar-collapse" id="cm-navbar">
	   			
	   			
	   			<!-- Left Menu -->
	   			<ul class="nav navbar-nav">
	   				
	   				<!-- Resumo -->
	   				<li<c:if test="${requestURL eq '/resumo'}"> class="active"</c:if>>
	   					<a href="${pageContext.request.contextPath}/resumo">
	   						<i class="fa fa-home"></i>
	   						<fmt:message key="resumo"/>
   						</a>
   					</li>
	      			
	      			<!-- Cadastros -->
	      			<li class="dropdown">
	      				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
	      					<i class="fa fa-pencil"></i>
	      					<fmt:message key="cadastros"/> <span class="caret"></span>
      					</a>
     					<ul class="dropdown-menu" role="menu">
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/despesas">
     								<i class="fa fa-minus font-red"></i>
     								<fmt:message key="despesas"/>
    							</a>
   							</li>
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/receitas">
     								<i class="fa fa-plus font-blue"></i>
     								<fmt:message key="receitas"/>
   								</a>
   							</li>
     						<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/produtos">
     								<i class="fa fa-tag"></i>
     								<fmt:message key="produtos" />
   								</a>
   							</li>
   							<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/pessoas">
     								<i class="fa fa-users"></i>
     								<fmt:message key="pessoas"/>
   								</a>
   							</li>
     						<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/contas">
     								<i class="fa fa-folder-open"></i>
     								<fmt:message key="contas"/>
   								</a>
   							</li>
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/taloes">
     								<i class="fa fa-check-circle"></i>
     								<fmt:message key="taloesCheque"/>
   								</a>
   							</li>
     						<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/cadastros/cartoes">
     								<i class="fa fa-credit-card"></i>
     								<fmt:message key="cartoesCredito"/>
   								</a>
							</li>
     					</ul>
     				</li>
     				
     				<!-- Lancamentos -->
	      			<li class="dropdown">
	      				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
	      					<i class="fa fa-rocket"></i>
	      					<fmt:message key="lancamentos"/> <span class="caret"></span>
      					</a>
	      				<ul class="dropdown-menu" role="menu">
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/despesas">
     								<i class="fa fa-arrow-up font-red"></i>
     								<fmt:message key="despesas"/>
   								</a>
   							</li>
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/receitas">
     								<i class="fa fa-arrow-down font-blue"></i>
     								<fmt:message key="receitas"/>
   								</a>
   							</li>
   							<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/compras">
     								<i class="fa fa-arrow-down font-red"></i>
     								<fmt:message key="compras"/>
   								</a>
   							</li>
   							<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/vendas">
     								<i class="fa fa-arrow-up font-blue"></i>
     								<fmt:message key="vendas"/>
   								</a>
   							</li>
     						<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/pagamentos">
     								<i class="fa fa-arrow-right font-red"></i>
     								<fmt:message key="pagamentos"/>
   								</a>
   							</li>
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/recebimentos">
     								<i class="fa fa-arrow-left font-blue"></i>
     								<fmt:message key="recebimentos"/>
   								</a>
   							</li>
     						<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/transferencias">
     								<i class="fa fa-exchange"></i>
     								<fmt:message key="transferencias"/>
   								</a>
   							</li>
     						<li class="divider"></li>
     						<li>
     							<a href="${pageContext.request.contextPath}/lancamentos/conciliacao">
     								<i class="fa fa-check-square"></i>
     								<fmt:message key="conciliacao"/>
   								</a>
   							</li>
     					</ul>
     				</li>
     				
     				<!-- Relatorios -->
	      			<li>
	      				
	      				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
	      					<i class="fa fa-bar-chart"></i>
	      					<fmt:message key="relatorios"/> <span class="caret"></span>
      					</a>
	      				
	      				<ul class="dropdown-menu" role="menu">
	      				
	      					<!-- Data -->
	    					<li>
	    						<a href="${pageContext.request.contextPath}/relatorios/data">
	    							<i class="fa fa-calendar-o"></i>
	    							<fmt:message key="data"/>
    							</a>
   							</li>
   							
   							<!-- Despesas -->
   							<li class="divider"></li>
   							<li class="dropdown-header">
                            	<i class="fa fa-arrow-up font-red"></i>
   								<fmt:message key="despesas"/>
   							</li>
   							<li>
	   							<a href="${pageContext.request.contextPath}/relatorios/despesas/pagara">
	   								<i class="fa fa-user"></i>
	   								<fmt:message key="pagarA"/>
   								</a>
   							</li>
   							<li>
	   							<a href="${pageContext.request.contextPath}/relatorios/despesas/classificacao">
	   								<i class="fa fa-sitemap"></i>
	   								<fmt:message key="classificacao"/>
   								</a>
   							</li>
   							<li>
	   							<a href="${pageContext.request.contextPath}/relatorios/despesas/rateio">
	   								<i class="fa fa-share-alt"></i>
	   								<fmt:message key="rateio"/>
   								</a>
   							</li>
   							<!-- Receitas -->
   							<li class="divider"></li>
   							<li class="dropdown-header">
                            	<i class="fa fa-arrow-down font-blue"></i>
   								<fmt:message key="receitas"/>
   							</li>
                            <li>
	   							<a href="${pageContext.request.contextPath}/relatorios/receitas/receberde">
	   								<i class="fa fa-user"></i>
	   								<fmt:message key="receberDe"/>
   								</a>
   							</li>
   							<li>
	   							<a href="${pageContext.request.contextPath}/relatorios/receitas/classificacao">
	   								<i class="fa fa-sitemap"></i>
	   								<fmt:message key="classificacao"/>
   								</a>
   							</li>
   							<li>
	   							<a href="${pageContext.request.contextPath}/relatorios/receitas/rateio">
	   								<i class="fa fa-share-alt"></i>
	   								<fmt:message key="rateio"/>
   								</a>
   							</li>
							
	   					</ul>
	   				</li>
	   			
	   			</ul>
	   			
	   			
	   			<!-- Right Menu -->
	   			<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
		          		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span id="menuUsuario_${usuarioLogged.usuario.id}">${usuarioLogged.usuario.displayNome}</span> <span class="caret"></span></a>
		          		<ul class="dropdown-menu" role="menu">
			          		<c:if test="${usuarioLogged.isCliente}">
			            		<li>
			            			<a href="${pageContext.request.contextPath}/assinatura">
			            				<i class="fa fa-toggle-on"></i>
			            				<fmt:message key="assinatura"/>
		            				</a>
		            			</li>
			            		<li class="divider"></li>
			            		<li>
			            			<a href="${pageContext.request.contextPath}/usuarios">
			            				<i class="fa fa-users"></i>
			            				<fmt:message key="usuarios"/>
		            				</a>
		            			</li>
			            		<li class="divider"></li>
			            	</c:if>
			            	<li>
			            		<a id="lbAtualizarSenha" href="${pageContext.request.contextPath}/usuario/atualizarSenha">
			            			<i class="fa fa-asterisk"></i>
			            			<fmt:message key="atualizarSenha"/>
		            			</a>
		            		</li>
			            	<li class="divider"></li>
			            	<li>
			            		<a href="${pageContext.request.contextPath}/sair" onclick="return confirmarSaida(this)">
			            			<i class="fa fa-power-off"></i>
			            			<fmt:message key="sair"/>
		            			</a>
		            		</li>
		          		</ul>
		        	</li>
				</ul>
			
			</div>
		</div>
 	
 	</nav>
 	
 	<tiles:insertAttribute name="breadcrumb" flush="true" ignore="true"/>
		
	<div class="container-fluid">
		<tiles:insertAttribute name="body" flush="true" />
	</div>
	
	<div class="cm-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-6 text-center"><p><fmt:message key="site.footer"/></p></div>
				<div class="col-xs-12 col-md-6 text-center"><p><fmt:message key="site.footer.suporte"/></p></div>
			</div>
		</div>
	</div>
	  
	<env:supports key="development">
		<script type="text/javascript" src="${pageContext.request.contextPath}/dynamic/resources/js/i18n.js?v${applicationVersion}"></script>
	</env:supports>
	<env:supports key="production">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/i18n.js?v${applicationVersion}"></script>
	</env:supports>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/components.js?v${applicationVersion}"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/dynamic/resources/js/vars.js?v${applicationVersion}"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/admin.js?v${applicationVersion}"></script>
	
	<tiles:insertAttribute name="script" ignore="true" flush="true" />
	
</body>
</html>