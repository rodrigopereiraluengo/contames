<fmt:message key="patternDate" var="patternDate" />
<div id="dialogRelatorioItemMovs" class="modal cm-modal-data-table" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmRelatorioItemMovs" action="${pageContext.request.contextPath}/relatorioItem/movs">
			
				<input type="hidden" name="item_id" value="${param.item_id}" />
				<input type="hidden" name="ano" value="${param.ano}" />
				<input type="hidden" name="tipo" value="${param.tipo}" />
				<input type="hidden" name="mes"value="${param.mes}" />
				<input type="hidden" name="isCCred" value="${param.isCCred}" />
				
				<c:forEach var="item" items="${itemList}" varStatus="st">
					<input type="hidden" name="itemList[${st.index}]" value="${item}" />
				</c:forEach>
				
				<c:forEach var="status" items="${statusList}" varStatus="st">
					<input type="hidden" name="statusList[${st.index}]" value="${status}"/>
				</c:forEach>
						
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogRelatorioItemMovsTitle"><fmt:message key="movimentos"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="data-table panel panel-default">
						
						<div class="result search-table">
							<jsp:include page="movs_table.jsp" />
	   					</div>
						
					</div>
				
				</div>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/relatorioItem.movs.js?v${applicationVersion}"></script>