<div id="dialogUsuarioAtualizarSenha" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmUsuarioAtualizarSenha">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogUsuarioAtualizarSenhaTitle"><fmt:message key="atualizarSenha"/></h4>
				</div>
				
				<div class="modal-body">
				
					<!-- Senha Atual -->
					<div class="form-group">
						<label for="senhaAtual" class="control-label">
							<fmt:message key="senhaAtual" />
							<span class="required">*</span>
						</label>
						<input type="password" id="senhaAtual" name="senhaAtual" data-category="atualizarSenha.senhaAtual senhaAtual" class="form-control" />
					</div>
					
					<!-- Nova Senha -->
					<div class="form-group">
						<label for="senha" class="control-label">
							<fmt:message key="novaSenha" />
							<span class="required">*</span>
						</label>
						<input type="password" id="senha" name="senha" data-category="atualizarSenha.senha senha" class="form-control" />
					</div>
					
					<!-- Confirme Nova Senha -->
					<div class="form-group">
						<label for="confirmeSenha" class="control-label">
							<fmt:message key="confirmeSenha" />
							<span class="required">*</span>
						</label>
						<input type="password" id="confirmeSenha" name="confirmeSenha" data-category="atualizarSenha.confirmeSenha confirmeSenha" class="form-control" />
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		        </div>
			
			</form>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/usuario.atualizarSenha.js?v${applicationVersion}"></script>