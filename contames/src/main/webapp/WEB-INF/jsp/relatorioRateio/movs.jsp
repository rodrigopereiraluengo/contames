<fmt:message key="patternDate" var="patternDate" />
<div id="dialogRelatorioRateioMovs" class="modal cm-modal-data-table" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmRelatorioRateioMovs" action="${pageContext.request.contextPath}/relatorioRateio/movs">
			
				<input type="hidden" name="ano" value="${param.ano}" />
				<input type="hidden" name="tipo" value="${param.tipo}" />
				<input type="hidden" name="mes"value="${param.mes}" />
				
				<c:forEach var="rateio" items="${rateioList}">
					<input type="hidden" name="rateioList[]" value="${rateio}" />
				</c:forEach>
				
				<c:forEach var="pessoa" items="${pessoaList}">
					<input type="hidden" name="pessoaList[]" value="${pessoa}" />
				</c:forEach>
				
				<c:forEach var="status" items="${statusList}">
					<input type="hidden" name="statusList[]" value="${status}"/>
				</c:forEach>
						
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogRelatorioRateioMovsTitle"><fmt:message key="movimentos"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="data-table panel panel-default">
						
						<div class="result search-table">
							<jsp:include page="/WEB-INF/jsp/relatorioRateio/movs_table.jsp" />
	   					</div>
						
					</div>
				
				</div>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/relatorioRateio.movs.js?v${applicationVersion}"></script>