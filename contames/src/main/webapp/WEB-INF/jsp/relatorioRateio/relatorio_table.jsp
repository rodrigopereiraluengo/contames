<fmt:message key="patternDate" var="patternDate" />
<script type="text/javascript">
	labels = [];
	values = [];
</script>
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumRegistroEncontrado"/>
		</strong>
	</div>
	<script type="text/javascript">
		$(function(){ $('#layChart').hide(); });
	</script>
</c:if>
<c:if test="${count > 0}">
	<c:set var="path" value="relatorios/${tipo eq 'DES' ? 'despesas' : 'receitas'}/rateio" />
	<div class="table-responsive">
		
		<table class="table table-bordered">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="rateio" column="rateio_nome" page="${page}" path="${path}" />
					<c:forEach var="i" begin="1" end="12">
						<cmt:th orderByList="${orderByList}" index="${i}" name="mesAbr_${i}" column="valor_${i}" page="${page}" path="${path}" />
					</c:forEach>
					<cmt:th orderByList="${orderByList}" index="12" name="total" column="valor" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyRelatorioRateio">
				<c:forEach var="rel" items="${relList}" varStatus="stRel">
					<tr>
						<td class="text-nowrap">
							<c:if test="${rel['rateio_pessoa_id'] ne null}">
								<a href="javascript:;" onclick="relatorioRateioOpenPes('${rel['rateio_pessoa_id']}')"><c:out value="${rel['rateio_nome']}"/></a>
							</c:if>
							<c:if test="${rel['rateio_pessoa_id'] eq null}">
								<span><c:out value="${rel['rateio_nome']}"/></span>
							</c:if>
							<c:if test="${rel['valor'] < 0}">
								<script type="text/javascript">
									labels.push('<c:out value="${rel['rateio_nome']}"/>');
									values.push(${-rel['valor']});
								</script>
							</c:if>
							<c:if test="${rel['valor'] > 0}">
								<script type="text/javascript">
									labels.push('<c:out value="${rel['rateio_nome']}"/>');
									values.push(${rel['valor']});
								</script>
							</c:if>
						</td>
						<c:forEach var="i" begin="1" end="12">
							<c:set var="key_valor" value="valor_${i}"/>
							<fmt:formatNumber var="valor" type="number" value="${rel[key_valor]}" maxFractionDigits="2" minFractionDigits="2" />
							<td class="text-right lb-valor lr2">
								<c:if test="${rel[key_valor] ne 0}">
									<a href="javascript:;" onclick="relatorioRateioOpenMovs({rateio_id: ${rel['rateio_id'] eq null ? 0 : rel['rateio_id']}, pessoa_id: ${rel['rateio_pessoa_id'] eq null ? 0 : rel['rateio_pessoa_id']}, mes: ${i}})" class="${rel[key_valor] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
								</c:if>
							</td>
						</c:forEach>
						<td class="cm-td-total text-right">
							<c:if test="${rel['valor'] ne 0}">
								<fmt:formatNumber var="valor" type="number" value="${rel['valor']}" maxFractionDigits="2" minFractionDigits="2" />
								<a href="javascript:;" onclick="relatorioRateioOpenMovs({rateio_id: ${rel['rateio_id'] eq null ? 0 : rel['rateio_id']}, pessoa_id: ${rel['rateio_pessoa_id'] eq null ? 0 : rel['rateio_pessoa_id']}})" class="${rel['valor'] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
							</c:if>
						</td>
						<td class="text-center lr2">
							<button type="button" class="btn btn-default btn-xs" onclick="openRelatorioChartLine(this);"><i class="fa fa-line-chart"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
			
			
			<!-- RODAPE -->
			<tfoot>
				
				<!-- TOTAL -->
				<tr id="trTotal" class="cm-tr-total">
					<td class="text-right lt2"><fmt:message key="total"/></td>
					<c:forEach var="i" begin="1" end="12">
						<c:set var="key_valor" value="valor_${i}"/>
						<c:set var="key_des" value="despesas_${i}"/>
						<c:set var="key_rec" value="receitas_${i}"/>
						<td class="text-right lb-valor lt2">
							<c:if test="${resultTotalMap[key_valor] ne 0 and resultTotalMap[key_valor] ne null}">
								<fmt:formatNumber var="valor" type="number" value="${resultTotalMap[key_valor]}" maxFractionDigits="2" minFractionDigits="2" />
								<a href="javascript:;" onclick="relatorioRateioOpenMovs({mes: ${i}, rateio_id: 0})" class="${resultTotalMap[key_valor] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
							</c:if>
						</td>
					</c:forEach>
					<td class="text-right lt2">
						<c:if test="${resultTotalMap['valor'] ne 0}">
							<fmt:formatNumber var="valor" type="number" value="${resultTotalMap['valor']}" maxFractionDigits="2" minFractionDigits="2" />
							<a href="javascript:;" onclick="relatorioRateioOpenMovs({rateio_id: 0})" class="${resultTotalMap['valor'] lt 0 ? 'font-red' : 'font-blue'}">${fn:replace(valor, '-', '')}</a>
						</c:if>
					</td>
					<td class="text-center lr2">
						<button type="button" class="btn btn-default btn-xs" onclick="openRelatorioChartLine(this);"><i class="fa fa-line-chart"></i></button>
					</td>
				</tr>
											
			</tfoot>
		</table>
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" isLazy="true" />
  		<cmt:paglaz orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
	<script type="text/javascript">
	// Grafico
	if(typeof(relatorioChart) != 'undefined') {
		relatorioChart();
	}
	</script>

</c:if>