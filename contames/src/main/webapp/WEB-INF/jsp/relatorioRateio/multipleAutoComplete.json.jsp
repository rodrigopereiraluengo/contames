<%@page trimDirectiveWhitespaces="true" contentType="application/json; charset=ISO-8859-1" %>
<json:array>
	<c:forEach var="pessoa" items="${pessoaList}">
		<json:object>
			<json:property name="id" value="${pessoa.id}" />			
			<json:property name="name" value="${pessoa.displayNome}" />
			<json:property name="tipo" value="PES" />
		</json:object>
	</c:forEach>
	<c:forEach var="rateio" items="${rateioList}">
		<json:object>
			<json:property name="id" value="${rateio.id}" />			
			<json:property name="name" value="${rateio.nome}" />
			<json:property name="tipo" value="RAT" />
		</json:object>
	</c:forEach>
</json:array>