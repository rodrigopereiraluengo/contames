<fmt:message key="patternDate" var="patternDate"/>
<c:if test="${cheque ne null and cheque.data ne null}">
	<joda:format pattern="${patternDate}" value="${cheque.data}" var="data" />
</c:if>
<div id="dialogSelectTalaoCheque" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmDialogSelectTalaoCheque">
			
				<input type="hidden" id="dialogSelectTalaoChequeId" name="talaoCheque.id"<c:if test="${cheque ne null and cheque.talaoCheque ne null and cheque.talaoCheque.id ne null}"> value="${cheque.talaoCheque.id}"</c:if> />
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogSelectTalaoChequeTitle"><fmt:message key="cheque"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid">
					
						<div class="row">
						
							<div class="col-xs-12 col-sm-8 form-group">
	  							<label class="control-label" for="dialogSelectTalaoChequeNumero">
	  								<fmt:message key="numero" />
	  								<span class="required">*</span>
  								</label>
  								<div class="input-group">
  									<c:if test="${isReadOnly}">
  										<p class="form-control-static">
  											<c:out value="${cheque.talaoCheque.conta.nome}"/>
  											&nbsp;${cheque.numero}
  											<input type="hidden" name="cheque.id" value="${cheque.id}" />
  										</p>
  									</c:if>
  									<c:if test="${!isReadOnly}">
  										<select 
				    						id="dialogSelectTalaoChequeNumero" 
				    						name="cheque.numero" 
				    						class="form-control"
				    						data-conta="${param.conta}"
				    						data-form="${pageContext.request.contextPath}/talaoCheque/cadastro"
				    						data-search="${pageContext.request.contextPath}/cadastros/taloes?tpl=dialog&dialogId=TalaoChequeConsultar&dialogTitle=taloesCheque"
				    						data-load-options="${pageContext.request.contextPath}/talaoCheque/carregaSelect"
				    						data-category="numero">
				    						<jsp:include page="/WEB-INF/jsp/talaoCheque/carregaSelect.jsp" />
				    					</select>
				    					<span class="input-group-btn">
				    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.talaoCheque" />">
				    							<i class="fa fa-plus"></i>
			   								</button>
			   								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.talaoCheque" />">
			   									<i class="fa fa-pencil"></i>
			   								</button>
											<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.talaoCheque" />">
												<i class="fa fa-search"></i>
			   								</button>
										</span>
  									</c:if>
  								</div>
	  						</div>
	  						
	  						<c:if test="${empty param.isData or param.isData eq 'true'}">
		  						<div class="col-xs-12 col-sm-4 form-group">
		  							<label class="control-label" for="dialogSelectTalaoChequeData"><fmt:message key="preDatado"/></label>
	  								<div class="input-group">
	  									<c:if test="${isReadOnly}">
	  										<p class="form-control-static">${data}</p>
	  									</c:if>
	  									<c:if test="${!isReadOnly}">
			  								<input type="text" id="dialogSelectTalaoChequeData" name="cheque.data"<c:if test="${cheque ne null and cheque.data ne null}"> value="${data}"</c:if> data-category="data" class="form-control" />
			  								<span class="input-group-btn">
				  								<button class="btn btn-default" type="button" id="btDialogSelectTalaoChequeData">
			       									<i class="fa fa-calendar-o"></i>
												</button>
											</span>
										</c:if>
	  								</div>
								</div>
							</c:if>
						
						</div>
						
						<div class="form-group">
							<label class="control-label" for="dialogSelectTalaoChequeObs"><fmt:message key="obs"/></label>
							<textarea id="dialogSelectTalaoChequeObs" name="cheque.obs" class="form-control" rows="4"><c:if test="${cheque ne null and cheque.obs ne null}"><c:out value="${cheque.obs}"/></c:if></textarea>
						</div>
						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
				
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/talaoCheque.dialogSelect.js?v${applicationVersion}"></script>