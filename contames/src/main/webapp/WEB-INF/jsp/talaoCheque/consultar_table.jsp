<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumTalaoChequeEncontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
		
		<c:set var="path" value="cadastros/taloes"/>	
 				
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="conta" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="seqInicio" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="folhas" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="seqFim" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyTalaoCheque">
				<c:forEach var="talaoCheque" items="${talaoChequeList}">
					<tr 
						data-id="${talaoCheque['id']}"
						data-category="talaoCheque_${talaoCheque['id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${talaoCheque['_data']}" /></td>
						<td class="text-center<c:if test="${f:orderByContains('conta', orderByList)}"> font-bold</c:if>"><c:out value="${talaoCheque['conta']}"/></td>
						<td class="text-center<c:if test="${talaoCheque['_status'] eq 'INA'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${talaoCheque['status']}</td>
						<td class="text-right<c:if test="${f:orderByContains('seqInicio', orderByList)}"> font-bold</c:if>">${talaoCheque['seqinicio']}</td>
						<td class="text-right<c:if test="${f:orderByContains('folhas', orderByList)}"> font-bold</c:if>">${talaoCheque['folhas']}</td>
						<td class="text-right<c:if test="${f:orderByContains('seqFim', orderByList)}"> font-bold</c:if>">${talaoCheque['seqfim']}</td>
						<td class="text-center">
							<c:if test="${talaoCheque['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(talaoCheque['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
 			
 	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>