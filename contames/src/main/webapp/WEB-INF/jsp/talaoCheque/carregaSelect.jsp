<c:if test="${talaoChequeList eq null or empty talaoChequeList}">
	<option value=""><fmt:message key="nenhumTalaoChequeEncontrado"/></option>
</c:if>
<c:if test="${talaoChequeList ne null and !empty talaoChequeList}">
	<option value="" data-talao-id="" data-talao-conta-id=""><fmt:message key="selecione___"/></option>
	<c:forEach var="talaoCheque" items="${talaoChequeList}">
		<optgroup data-conta="${talaoCheque.conta.id}" label="${talaoCheque.conta.nome}&nbsp;${talaoCheque.seqInicio} - ${talaoCheque.seqFim}">
			<c:forEach var="_numero" begin="${talaoCheque.seqInicio}" end="${talaoCheque.seqFim}">
				<c:set var="_cheque" value="${talaoCheque.getChequeByNumero(_numero)}" />
				<option 
					value="${_numero}"
					<c:if test="${_cheque ne null and (_numero ne _cheque.numero or _cheque.lancamentoParcela ne lancamentoParcela or lancamentoParcela eq null)}"> disabled="disabled"</c:if> 
					data-talao-id="${talaoCheque.id}"<c:if test="${(cheque ne null and talaoCheque.id eq cheque.talaoCheque.id and cheque.numero eq _numero) or talaoId eq talaoCheque.id and _numero eq numero}"> selected="selected"</c:if>>
					${_numero}
					<c:if test="${_cheque ne null}"> - <fmt:message key="LancamentoStatus.${_cheque.status eq 'BXD' ? 'DES.BXD' : _cheque.status}"/></c:if>
				</option>
			</c:forEach>
		</optgroup>
	</c:forEach>
</c:if>