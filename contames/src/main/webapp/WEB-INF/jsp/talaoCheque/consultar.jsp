<c:if test="${param.tpl eq null}">
	<c:set var="tpl" value="admin"/>
</c:if>
<c:if test="${param.tpl ne null}">
	<c:set var="tpl" value="${param.tpl}"/>
</c:if>
<tiles:insertTemplate template="/WEB-INF/jsp/tpl/${tpl}.jsp">

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="cadastroDe.taloesCheque"/></h1>
						<p class="description"><fmt:message key="cadastroDe.taloesCheque.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="resumo" />
							</li>
				  			<li><fmt:message key="cadastros" /></li>
				  			<li><fmt:message key="taloesCheque" /></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
		
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<form 
			id="frmTalaoChequeConsultar" 
			action="${pageContext.request.contextPath}/cadastros/taloes"
			data-form="${pageContext.request.contextPath}/talaoCheque/cadastro"
			data-delete="${pageContext.request.contextPath}/conta/removerTalaoCheque"
			class="data-table">
			
			<div class="panel panel-default">
			
			
				<input type="hidden" name="status" value="${param.status}"/>
				
				<div class="panel-heading cm-panel-heading-toolbar">
						
					<div class="container-fluid">
						
						<div class="row">
				
							<div class="col-xs-12 col-sm-4 form-group">
					  			<div class="btn-group">
						  			<button type="button" class="btn btn-default bt-new">
						  				<i class="fa fa-plus"></i>
						  				<fmt:message key="nova" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-edit" disabled="disabled">
						  				<i class="fa fa-pencil"></i>
						  				<fmt:message key="editar" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-delete" disabled="disabled">
						  				<i class="fa fa-trash"></i>
						  				<fmt:message key="excluir" />
					  				</button>
								</div>
							</div>
				
							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
								<div class="input-group">
									<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<i class="fa fa-search"></i>
										</button>
									</span>
				    			</div>		
							</div>
							
						</div>
					
					</div>
					
				</div>
				
				<div class="result search-table">
					<jsp:include page="/WEB-INF/jsp/talaoCheque/consultar_table.jsp" />	   			
	   			</div>
	   			
   			</div>
   			
		</form>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/talaoCheque.consultar.js?v${applicationVersion}"></script>
	
	</tiles:putAttribute>
	
</tiles:insertTemplate>