<fmt:message key="patternDate" var="patternDate" />
<div id="dialogCartaoCreditoCadastro" class="modal" style="display: none" data-dialog-detalhe="${param.dialogDetalhe}">
  	<div class="modal-dialog">
   		<div class="modal-content">
   		   		
   			<form 
   				id="frmCartaoCreditoCadastro" 
   				data-is-utilizado="${isUtilizado}">
   				
   				<input type="hidden" name="viewid" value="${viewid}" />
   				
   				<c:if test="${cartaoCredito ne null}"><input type="hidden" name="cartaoCredito.id" value="${cartaoCredito.id}" /></c:if>
   				
	   			<div class="modal-header">
	   				<button type="button" class="close" data-dismiss="modal">
	   					<i class="fa fa-times"></i>
   					</button>
   					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
	        		<h4 class="modal-title"><fmt:message key="cartaoCredito"/></h4>
	      		</div>
	      		
	      		<div class="modal-body">
	      			
	      			<div class="container-fluid" data-lay-detalhe="minus" style="display: none">
					
						<div class="form-group" data-input-detalhe="cartaoCreditoNome"></div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimite"></div>
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimiteDisponivel"></div>
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimiteUtilizado"></div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoDataFatuAnt"></div>
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoValorFaturaAnt"></div>
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimiteDisponivelAnt"></div>
						</div>
					
					</div>
	      		
	      			<div class="container-fluid" data-lay-detalhe="plus">
					
						<div class="row">
  					
	  						<div class="col-xs-12 col-sm-8 form-group">
	  							<label class="control-label" for="cartaoCreditoBandeira" class="form-control"><fmt:message key="bandeira"/></label>
	  							<div class="input-group">
									<select 
			    						id="cartaoCreditoBandeira" 
			    						name="cartaoCredito.bandeira.id" 
			    						class="form-control"
			    						data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=CARTAO_BANDEIRA"
			    						data-search="${pageContext.request.contextPath}/descricao/consultar?descricaoTipo=CARTAO_BANDEIRA"
			    						data-load-options="${pageContext.request.contextPath}/descricao/carregaSelect?descricaoTipo=CARTAO_BANDEIRA"
			    						data-category="categoria">
			    						<c:if test="${bandeiraList eq null or empty bandeiraList}">
			    							<option value=""><fmt:message key="nenhumRegistroEncontrado"/></option>
			    						</c:if>
			    						<c:if test="${bandeiraList ne null and !empty bandeiraList}">
			    							<option value=""><fmt:message key="selecione___"/></option>
			    							<c:forEach var="bandeira" items="${bandeiraList}">
			    								<option value="${bandeira.id}"<c:if test="${cartaoCredito ne null and cartaoCredito.bandeira eq bandeira}"> selected="selected"</c:if>><c:out value="${bandeira.nome}"/></option>
			    							</c:forEach>
			    						</c:if>
			    					</select>
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.bandeira"/>">
			    							<i class="fa fa-plus"></i>
        								</button>
        								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.bandeira"/>">
        									<i class="fa fa-pencil"></i>
        								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.bandeira"/>">
											<i class="fa fa-search"></i>
        								</button>
      								</span>
   								</div>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-4 form-group">
	  							<label class="control-label" for="cartaoCreditoStatus">
									<fmt:message key="status"/>
									<span class="required">*</span>
									<a class="info-help" data-message="cartaoCredito.status.help">(?)</a>
								</label>
								<select id="cartaoCreditoStatus" name="cartaoCredito.status" data-category="status" class="form-control required">
									<option value="ATI"<c:if test="${cartaoCredito ne null and cartaoCredito.status eq 'ATI'}"> selected="selected"</c:if>><fmt:message key="Status.ATI"/></option>
									<option value="INA"<c:if test="${cartaoCredito ne null and cartaoCredito.status eq 'INA'}"> selected="selected"</c:if>><fmt:message key="Status.INA"/></option>
								</select>
	  						</div>
  						
  						</div>
  						
  						<div class="row">
	  						<div class="col-xs-12 col-sm-8 form-group" data-input-detalhe="cartaoCreditoNome">
	  							<label class="control-label" for="cartaoCreditoNome">
									<fmt:message key="nome"/>
									<span class="required">*</span>
									<a class="info-help" data-message="cartaoCredito.nome.help">(?)</a>
								</label>
								<input type="text" id="cartaoCreditoNome" name="cartaoCredito.nome"<c:if test="${cartaoCredito ne null}"> value="<c:out value="${cartaoCredito.nome}"/>"</c:if> maxlength="50" data-category="nome" class="form-control required"/>
	  						</div>
	  						<div class="col-xs-12 col-sm-4 form-group">
	  							<label class="control-label" for="cartaoCreditoIsPrincipal"><fmt:message key="principal" /></label>
	  							<c:if test="${empty param.isPrincipal}">
		  							<select id="cartaoCreditoIsPrincipal" name="cartaoCredito.isPrincipal" data-category="isPrincipal" class="form-control">
										<option value="true"<c:if test="${cartaoCredito ne null and cartaoCredito.isPrincipal}"> selected="selected"</c:if>><fmt:message key="sim"/></option>
										<option value="false"<c:if test="${cartaoCredito ne null and !cartaoCredito.isPrincipal or cartaoCredito eq null}"> selected="selected"</c:if>><fmt:message key="nao"/></option>
									</select>
								</c:if>
								<c:if test="${!empty param.isPrincipal}">
									<p class="form-control-static"><fmt:message key="nao"/></p>
								</c:if>
	  						</div>
  						</div>
  						
  						<div class="form-group">
							<label class="control-label" for="cartaoCreditoTitular">
								<fmt:message key="titular" />
							</label>
							<div class="input-group">
								<input 
  									type="text" 
  									id="cartaoCreditoTitular" 
  									name="cartaoCredito.titular.id" 
  									class="form-control"
  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
		    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoas&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
		    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
		    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
		    						<c:if test="${cartaoCredito.titular ne null}"> value="${cartaoCredito.titular.displayNome}" data-val="${cartaoCredito.titular.id}"</c:if>
		    						data-category="titular"
	    						/>
		  						
		    					<span class="input-group-btn">
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
		    							<i class="fa fa-plus"></i>
       								</button>
       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
       									<i class="fa fa-pencil"></i>
       								</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
										<i class="fa fa-search"></i>
       								</button>
   								</span>
	  						</div>
						</div>
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimite">
  								<label class="control-label" for="cartaoCreditoLimite">
  									<fmt:message key="limite"/>
  									<span class="required">*</span>
  								</label>
								<input type="text" id="cartaoCreditoLimite" name="cartaoCredito.limite"<c:if test="${cartaoCredito ne null}"> value="${cartaoCredito.limite}"</c:if> data-category="limite" class="form-control required"/>
  							</div>
  							
  							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimiteDisponivel">
  								<label class="control-label" for="cartaoCreditoLimiteDisponivel">
  									<fmt:message key="limiteDisp"/>
  									<span class="required" id="cartaoCreditoLimiteDisponivelReq" style="display: none">*</span>
  								</label>
  								<c:if test="${isUtilizado}">
  									<input type="text" id="cartaoCreditoLimiteDisponivel" name="cartaoCredito.limiteDisponivel" value="${cartaoCredito.limiteDisponivel}" class="form-control text-right ${cartaoCredito.limiteDisponivel eq 0 ? 'font-red' : 'font-blue'}" />
  								</c:if>
  								<c:if test="${!isUtilizado}">
  									<p id="cartaoCreditoLimiteDisponivel" class="form-control-static text-right ${cartaoCredito.limiteDisponivel eq 0 ? 'font-red' : 'font-blue'}"><fmt:formatNumber type="number" value="${cartaoCredito.limiteDisponivel}" maxFractionDigits="2" minFractionDigits="2" /></p>
  								</c:if>
							</div>
							
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimiteUtilizado">
  								<label class="control-label" for="cartaoCreditoLimiteUtilizado">
  									<fmt:message key="limiteUtilizado"/>
  									<span class="required" id="cartaoCreditoLimiteUtilizadoReq" style="display: none">*</span>
  								</label>
  								<c:if test="${isUtilizado}">
  									<input type="text" id="cartaoCreditoLimiteUtilizado" name="cartaoCredito.limiteUtilizado" value="${cartaoCredito.limiteUtilizado}" class="form-control text-right ${cartaoCredito.limiteUtilizado gt 0 ? 'font-red' : 'font-blue'}" />
  								</c:if>
  								<c:if test="${!isUtilizado}">
  									<p id="cartaoCreditoLimiteUtilizado" class="form-control-static text-right ${cartaoCredito.limiteUtilizado gt 0 ? 'font-red' : 'font-blue'}"><fmt:formatNumber type="number" value="${cartaoCredito.limiteUtilizado}" maxFractionDigits="2" minFractionDigits="2" /></p>
  								</c:if>
  							</div>
						
						</div>
  						
  						<div class="row">
  							
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoDataFatuAnt">
  								<label class="control-label" for="cartaoCreditoDataFatuAnt">
  									<fmt:message key="dataFatuAnt"/>
  									<span class="required" id="cartaoCreditoDataFatuAntReq" style="display: none">*</span>
  									<a class="info-help" data-message="cartaoCredito.dataFatuAnt.help">(?)</a>
  								</label>
  								<div class="input-group">
  									<c:if test="${isUtilizado}">
  										<p id="cartaoCreditoValorFaturaAnt" class="form-control-static"><joda:format pattern="${patternDate}" value="${ultimaFatura == null ? cartaoCredito.dataFatuAnt : ultimaFatura.dataDoc}" /></p>
  									</c:if>
  									<c:if test="${!isUtilizado}">
										<input type="text" id="cartaoCreditoDataFatuAnt" name="cartaoCredito.dataFatuAnt"<c:if test="${cartaoCredito ne null and cartaoCredito.dataFatuAnt ne null}"> value="<joda:format pattern="${patternDate}" value="${cartaoCredito.dataFatuAnt}" />"</c:if> data-category="dataFatuAnt" class="form-control"/>
										<span class="input-group-btn">
			  								<button class="btn btn-default" type="button" id="btLancData">
		       									<i class="fa fa-calendar-o"></i>
											</button>
										</span>
									</c:if>
								</div>
  							</div>
  							
  							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoValorFaturaAnt">
  								<label class="control-label" for="cartaoCreditoValorFaturaAnt">
  									<fmt:message key="valorFaturaAnt"/>
  									<span class="required" id="cartaoCreditoValorFaturaAntReq" style="display: none">*</span>
  									<a class="info-help" data-message="cartaoCredito.valorFaturaAnt.help">(?)</a>
  								</label>
  								<c:if test="${!isUtilizado}">
  									<input type="text" id="cartaoCreditoValorFaturaAnt" name="cartaoCredito.valorFaturaAnt"<c:if test="${cartaoCredito ne null}"> value="<fmt:formatNumber type="number" value="${cartaoCredito.valorFaturaAnt}" maxFractionDigits="2" minFractionDigits="2" />"</c:if> data-category="valorFaturaAnt" class="form-control" />
  								</c:if>
  								<c:if test="${isUtilizado}">
  									<p id="cartaoCreditoValorFaturaAnt" class="form-control-static text-right"><fmt:formatNumber type="number" value="${ultimaFatura eq null ? cartaoCredito.valorFaturaAnt : ultimaFatura.valorDoc}" maxFractionDigits="2" minFractionDigits="2" /></p>
  								</c:if>
							</div>
							
							<div class="col-xs-12 col-sm-4 form-group" data-input-detalhe="cartaoCreditoLimiteDisponivelAnt">
  								<label class="control-label" for="cartaoCreditoLimiteDisponivelAnt">
  									<fmt:message key="limiteDispAnt"/>
  									<span class="required" id="cartaoCreditoLimiteDisponivelAntReq" style="display: none">*</span>
  									<a class="info-help" data-message="cartaoCredito.limiteDispAnt.help">(?)</a>
  								</label>
  								<c:if test="${!isUtilizado}">
  									<input type="text" id="cartaoCreditoLimiteDisponivelAnt" name="cartaoCredito.limiteDisponivelAnt"<c:if test="${cartaoCredito ne null}"> value="<fmt:formatNumber type="number" value="${cartaoCredito.limiteDisponivelAnt}" maxFractionDigits="2" minFractionDigits="2" />"</c:if> data-category="limiteDisponivelAnt" class="form-control" />
  								</c:if>
  								<c:if test="${isUtilizado}">
  									<p id="cartaoCreditoLimiteDisponivelAnt" class="form-control-static text-right"><fmt:formatNumber type="number" value="${ultimaFatura eq null ? cartaoCredito.limiteDisponivelAnt : ultimaFatura.limiteDisponivel}" maxFractionDigits="2" minFractionDigits="2" /></p>
  								</c:if>
							</div>
  						
  						</div>
  						  						
  						<div class="row">
  							<div class="col-xs-12 col-sm-4 form-group">
  								<label class="control-label" for="cartaoCreditoDiaVencimento">
  									<fmt:message key="vencimentoDia"/>
  								</label>
								<input type="number" id="cartaoCreditoDiaVencimento" name="cartaoCredito.diaVencimento"<c:if test="${cartaoCredito ne null}"> value="${cartaoCredito.diaVencimento}"</c:if> maxlength="2" min="1" max="31" data-category="diaVencimento" class="form-control text-right"/>
  							</div>
  							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
  								<label class="control-label" for="cartaoCreditoDiasPagar">
  									<fmt:message key="diasPagar"/>
  								</label>
								<input type="number" id="cartaoCreditoDiasPagar" name="cartaoCredito.diasPagar"<c:if test="${cartaoCredito ne null}"> value="${cartaoCredito.diasPagar}"</c:if> maxlength="3" min="30" max="100" data-category="diasPagar" class="form-control text-right"/>
  							</div>
  						</div>
  						
  						<div class="form-group">
  							<label class="control-label" for="cartaoCreditoObs"><fmt:message key="obs" /></label>
  							<textarea id="cartaoCreditoObs" name="cartaoCredito.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${cartaoCredito ne null and cartaoCredito.obs ne null}"><c:out value="${cartaoCredito.obs}"/></c:if></textarea>
  						</div>
					
					</div>
	      		
	      		</div>
	      		
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default btn-dialog-detalhe" style="float: left">
						<span class="glyphicon" aria-hidden="true"></span>
						<fmt:message key="detalhes"/>
					</button>
	      			<button type="button" class="btn btn-default" data-dismiss="modal">
	      				<i class="fa fa-close"></i>
	      				<b><fmt:message key="cancelar"/></b>
      				</button>
	        		<button type="submit" class="btn btn-primary">
	        			<i class="fa fa-save"></i>
	        			<b><fmt:message key="salvar"/></b>
        			</button>
	      		</div>
	      		
	      		<c:if test="${cartaoCredito ne null and cartaoCredito.id ne null}">
		      		<div class="container-fluid">
				       	<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${cartaoCredito.criacao}" create="create" pessoa="${cartaoCredito.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${cartaoCredito.alteracao}" create="update" pessoa="${cartaoCredito.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
      		
      		</form>
   		
   		</div>
 	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cartaoCredito.cadastro.js?v${applicationVersion}"></script>