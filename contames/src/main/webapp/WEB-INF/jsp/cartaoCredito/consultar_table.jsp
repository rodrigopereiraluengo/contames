<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumCartaoCreditoEncontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
			
		<c:set var="path" value="cadastros/cartoes"/>	
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" column="bandeira" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" name="principal" column="isPrincipal" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="nome" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="titular" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" name="vencimentoDia" column="diaVencimento" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" column="diasPagar" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" column="limite" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="8" column="limiteUtilizado" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="9" name="limiteDisp" column="limiteDisponivel" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyCartaoCredito">
				<c:forEach var="cartaoCredito" items="${cartaoCreditoList}">
					<tr 
						data-id="${cartaoCredito['id']}"
						data-category="cartaoCredito_${cartaoCredito['id']}"
						>
						<td class="text-center<c:if test="${f:orderByContains('bandeira', orderByList)}"> font-bold</c:if>"><c:out value="${cartaoCredito['bandeira']}"/></td>
						<td class="text-center<c:if test="${cartaoCredito['_status'] eq 'INA'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${cartaoCredito['status']}</td>
						<td class="text-center<c:if test="${f:orderByContains('isPrincipal', orderByList)}"> font-bold</c:if>">${cartaoCredito['isprincipal']}</td>
						<td<c:if test="${f:orderByContains('nome', orderByList)}"> class="font-bold"</c:if>><c:out value="${cartaoCredito['nome']}" /></td>
						<td<c:if test="${f:orderByContains('titular', orderByList)}"> class="font-bold"</c:if>><c:out value="${cartaoCredito['titular']}" /></td>
						<td class="text-center<c:if test="${f:orderByContains('diaVencimento', orderByList)}"> font-bold</c:if>">${cartaoCredito['diavencimento']}</td>
						<td class="text-center<c:if test="${f:orderByContains('diasPagar', orderByList)}"> font-bold</c:if>">${cartaoCredito['diaspagar']}</td>
						<td class="text-right<c:if test="${f:orderByContains('limite', orderByList)}"> font-bold</c:if>">
							<c:if test="${cartaoCredito['limite'] ne null}">
								<fmt:formatNumber type="number" value="${cartaoCredito['limite']}" maxFractionDigits="2" minFractionDigits="2" />
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('limiteUtilizado', orderByList)}"> font-bold</c:if>">
							<c:if test="${cartaoCredito['limiteutilizado'] ne null}">
								<span class="font-bold<c:if test="${cartaoCredito['limiteutilizado'] eq 0}"> font-blue</c:if><c:if test="${cartaoCredito['limitedisponivel'] gt 0}"> font-red</c:if>">
									<fmt:formatNumber type="number" value="${cartaoCredito['limiteutilizado']}" maxFractionDigits="2" minFractionDigits="2" />
								</span>
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('limiteDisponivel', orderByList)}"> font-bold</c:if>">
							<c:if test="${cartaoCredito['limitedisponivel'] ne null}">
								<span class="font-bold<c:if test="${cartaoCredito['limitedisponivel'] eq 0}"> font-red</c:if><c:if test="${cartaoCredito['limitedisponivel'] gt 0}"> font-blue</c:if>">
									<fmt:formatNumber type="number" value="${cartaoCredito['limitedisponivel']}" maxFractionDigits="2" minFractionDigits="2" />
								</span>
							</c:if>
						</td>
						<td class="text-center">
							<c:if test="${cartaoCredito['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(cartaoCredito['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
			
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>