<fmt:message key="patternDate" var="patternDate" />
<c:if test="${cartaoCreditoList eq null or empty cartaoCreditoList}">
	<option value=""><fmt:message key="nenhumCartaoCreditoEncontrado"/></option>
</c:if>
<c:if test="${cartaoCreditoList ne null and !empty cartaoCreditoList}">
	<option value=""><fmt:message key="selecione___"/></option>
	<c:forEach var="cartaoCredito" items="${cartaoCreditoList}">
		<option 
			data-dia-vencimento="${cartaoCredito.diaVencimento}"
			data-dias-pagar="${cartaoCredito.diasPagar}"
			data-data-fatu-ant="<joda:format pattern="${patternDate}" value="${cartaoCredito.dataFatuAnt}" />"
			data-limite="<fmt:formatNumber type="number" value="${cartaoCredito.limite}" maxFractionDigits="2" minFractionDigits="2" />"
			data-limite-disponivel="<fmt:formatNumber type="number" value="${cartaoCredito.limiteDisponivel}" maxFractionDigits="2" minFractionDigits="2" />"
			data-limite-utilizado="<fmt:formatNumber type="number" value="${cartaoCredito.limiteUtilizado}" maxFractionDigits="2" minFractionDigits="2" />"
			value="${cartaoCredito.id}"<c:if test="${cartaoCredito.id eq param.id or empty param.id and cartaoCredito.isPrincipal}"> selected="selected"</c:if>><c:out value="${cartaoCredito.nome}"/></option>
	</c:forEach>
</c:if>