<fmt:message key="patternDate" var="patternDate" />
<div id="dialogSelectCartaoCredito" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmDialogCartaoCredito">
			
				<input type="hidden" id="dialogSelectCartaoCreditoData" value="<joda:format pattern="${patternDate}" value="${data}" />" />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogSelectCartaoCreditoTitle"><fmt:message key="cartaoCredito"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="form-group">
						<div class="input-group">
							<c:if test="${isReadOnly}">
								<p class="form-control-static">
									<c:if test="${cartaoCredito ne null}"><c:out value="${cartaoCredito.nome}" /></c:if>
									<c:if test="${cartaoCredito eq null}"><fmt:message key="cartaoCredito.nothing.selected" /></c:if>
								</p>
							</c:if>
							<input type="hidden" id="dialogSelectCartaoCreditoIsLimite" name="isLimite" value="${isLimite}" />
							<c:if test="${!isReadOnly}">
								
		    					<select 
		    						id="dialogSelectCartaoCreditoNome" 
		    						name="cartaoCredito.id" 
		    						class="form-control"
		    						data-form="${pageContext.request.contextPath}/cartaoCredito/cadastro?dialogDetalhe=minus"
		    						data-search="${pageContext.request.contextPath}/cadastros/cartoes?tpl=dialog&dialogId=CartaoCreditoConsultar&?dialogDetalhe=minus&dialogTitle=cartaoCredito&status=ATI"
		    						data-load-options="${pageContext.request.contextPath}/cartaoCredito/carregaSelect"
		    						data-category="cartaoCredito">
		    						<jsp:include page="/WEB-INF/jsp/cartaoCredito/carregaSelect.jsp" />
		    					</select>
		    					<span class="input-group-btn">
		    						<button id="btDialogSelectCartaoCreditoIsLimite" class="btn btn-default" type="button" title="<fmt:message key="title.utilizarLimite.cartaoCredito" />">
	   									<i class="fa fa-minus-square"></i>
									</button>
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.cartaoCredito" />">
		    							<i class="fa fa-plus"></i>
	   								</button>
	   								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.cartaoCredito" />">
   										<i class="fa fa-pencil"></i>
	   								</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.cartaoCredito" />">
										<i class="fa fa-search"></i>
	   								</button>
								</span>
	    					</c:if>
						</div>
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<c:if test="${!isReadOnly}">
		        		<button type="button" class="btn btn-default" data-dismiss="modal">
		        			<i class="fa fa-close"></i>
		        			<b><fmt:message key="cancelar"/></b>
	        			</button>
	        		</c:if>
		        	<button type="${isReadOnly ? 'button' : 'submit'}" class="btn btn-primary"<c:if test="${isReadOnly}"> data-dismiss="modal"</c:if>>
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cartaoCredito.dialogSelect.js?v${applicationVersion}"></script>