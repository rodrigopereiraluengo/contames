<fmt:message key="patternDate" var="patternDate"/>
<c:if test="${cheque ne null and cheque.data ne null}">
	<joda:format pattern="${patternDate}" value="${cheque.data}" var="data" />
</c:if>
<div id="dialogPropsCheque" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmDialogPropsCheque">
			
				<c:if test="${cheque ne null and cheque.id ne null}">
					<input type="hidden" name="cheque.id" value="${cheque.id}" />
				</c:if>
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogPropsChequeTitle"><fmt:message key="cheque"/></h4>
				</div>
				
				<div class="modal-body">
					<div class="container-fluid">
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-3 form-group">
	  							<label class="control-label" for="dialogPropsChequeNumero">
	  								<fmt:message key="numero" />
	  								<span class="required">*</span>
  								</label>
  								<c:if test="${isReadOnly}">
  									<p class="form-control-static text-right">${cheque.numero}</p>
  								</c:if>
  								<c:if test="${!isReadOnly}">
  									<input type="number" id="dialogPropsChequeNumero" name="cheque.numero"<c:if test="${cheque ne null}"> value="${cheque.numero}"</c:if> data-category="numero" min="1" class="form-control text-right" />
  								</c:if>
							</div>
							
							<div class="col-xs-12 col-sm-5 form-group">
							
								<label class="control-label" for="dialogPropsChequeBanco"><fmt:message key="banco" /></label>
	  							<div class="input-group">
	  								
	  								<input 
	  									type="text" 
	  									id="dialogPropsChequeBanco" 
	  									name="cheque.banco.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus&pessoaTipo=J&categoria=<fmt:message key="categoriaBanco"/>"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=bancos&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI&pessoaTipo=J&categoria=<fmt:message key="categoriaBanco"/>"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete?pessoaTipo=J&categoria=<fmt:message key="categoriaBanco"/>"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${cheque ne null and cheque.banco ne null}"> value="${cheque.banco.displayNome}" data-val="${cheque.banco.id}"</c:if>
		    						/>
	  								<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.banco"/>">
			    							<i class="fa fa-plus"></i>
	       								</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.banco"/>">
	       									<i class="fa fa-pencil"></i>
	       								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.banco"/>">
											<i class="fa fa-search"></i>
	       								</button>
	   								</span>
	  							</div>
							
							</div>
							
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="dialogPropsChequeData"><fmt:message key="preDatado" /></label>
								<div class="input-group">
									<c:if test="${isReadOnly}">
										<p class="form-control-static">${data}</p>
									</c:if>
									<c:if test="${!isReadOnly}">
		  								<input type="text" id="dialogSelectTalaoChequeData" name="cheque.data"<c:if test="${cheque ne null and cheque.data ne null}"> value="${data}"</c:if> class="form-control" />
		  								<span class="input-group-btn">
			  								<button class="btn btn-default" type="button" id="btDialogPropsChequeData">
			  									<i class="fa fa-calendar-o"></i>
		       								</button>
										</span>
									</c:if>
  								</div>
							</div>
									
						</div>
						
						<div class="form-group">
							<label class="control-label" for="dialogPropsChequeNome"><fmt:message key="nome" /></label>
							
							<div class="input-group">
								<input 
  									type="text" 
  									id="dialogPropsChequeNome" 
  									name="cheque.nome.id" 
  									class="form-control"
  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
		    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoa&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
		    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
		    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
		    						<c:if test="${cheque ne null and cheque.nome ne null}"> value="${cheque.nome.displayNome}" data-val="${cheque.nome.id}"</c:if>
	    						/>
		  						
		    					<span class="input-group-btn">
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
		    							<i class="fa fa-plus"></i>
       								</button>
       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
       									<i class="fa fa-pencil"></i>
       								</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
										<i class="fa fa-search"></i>
       								</button>
   								</span>
		  						
	  						</div>
							
						</div>
					
						<div class="form-group">
							<label class="control-label" for="dialogPropsChequeObs"><fmt:message key="obs"/></label>
							<textarea id="dialogPropsChequeObs" name="cheque.obs" class="form-control" rows="4"><c:if test="${cheque.obs ne null}"><c:out value="${cheque.obs}"/></c:if></textarea>
						</div>
					
					</div>
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cheque.dialogProps.js?v${applicationVersion}"></script>