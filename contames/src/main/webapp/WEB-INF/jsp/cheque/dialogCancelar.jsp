<div id="dialogChequeCancelar" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmDialogChequeCancelar">
			
				<input type="hidden" name="talaoChequeId" value="${talaoCheque.id}" />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogPropsChequeTitle"><fmt:message key="cancelarCheque"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="row">
							
						<div class="col-xs-12 col-sm-3 form-group">
  							<label class="control-label" for="dialogChequeCancelarNumero">
  								<fmt:message key="numero" />
  								<span class="required">*</span>
							</label>
							<input type="number" id="dialogChequeCancelarNumero" name="cheque.numero"<c:if test="${cheque ne null}"> value="${cheque.numero}"</c:if> data-category="numero" min="1" class="form-control text-right" />
						</div>
							
						<div class="col-xs-12 col-sm-9 form-group">
						
							<label class="control-label" for="dialogChequeCancelarMotivo"><fmt:message key="motivo" /></label>
  							<div class="input-group">
  								
  								<select 
		    						id="dialogChequeCancelarMotivo" 
		    						name="cheque.motivo.id" 
		    						class="form-control"
		    						data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=MOTIVO_CAN_CHQ"
		    						data-search="${pageContext.request.contextPath}/descricao/consultar?descricaoTipo=MOTIVO_CAN_CHQ"
		    						data-load-options="${pageContext.request.contextPath}/descricao/carregaSelect?descricaoTipo=MOTIVO_CAN_CHQ"
		    						data-category="motivo">
		    						<c:if test="${motivoList eq null or empty motivoList}">
		    							<option value=""><fmt:message key="nenhumRegistroEncontrado"/></option>
		    						</c:if>
		    						<c:if test="${motivoList ne null and !empty motivoList}">
		    							<option value=""><fmt:message key="selecione___"/></option>
		    							<c:forEach var="motivo" items="${motivoList}">
		    								<option value="${motivo.id}"<c:if test="${cheque.motivo eq motivo}"> selected="selected"</c:if>><c:out value="${motivo.nome}"/></option>
		    							</c:forEach>
		    						</c:if>
		    					</select>
		    					<span class="input-group-btn">
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.motivo"/>">
		    							<i class="fa fa-plus"></i>
       								</button>
       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.motivo"/>">
       									<i class="fa fa-pencil"></i>
       								</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.motivo"/>">
										<i class="fa fa-search"></i>
       								</button>
   								</span>
  							
  							</div>
						
						</div>
						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cheque.dialogCancelar.js?v${applicationVersion}"></script>