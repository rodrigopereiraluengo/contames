<fmt:message key="patternDate" var="patternDate" />
<c:set var="isReadOnly" value="${baixaItem.status eq 'COM'}" />
<div id="dialogBaixaItem" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmBaixaItem" data-is-read-only="${isReadOnly}" data-tipo="${baixaItem.lancamentoParcela.tipo}">
			
				<input type="hidden" name="baixaItem.id" value="${baixaItem.id}"/>
				<input type="hidden" id="lancamentoParcelaId" value="${baixaItem.lancamentoParcela}" />
				<input type="hidden" id="lancamentoParcelaTipo" value="${baixaItem.lancamentoParcela.tipo}" />
				
				<input type="hidden" id="baixaItemParcelaSeq" name="baixaItem.seq" value="${baixaItem.seq}"/>
				<input type="hidden" id="baixaItemBoletoTipo" name="baixaItem.boletoTipo" value="${baixaItem.boletoTipo}"/>
				<input type="hidden" id="baixaItemBoletoNumero" name="baixaItem.boletoNumero" value="${baixaItem.boletoNumero}"/>
				<input type="hidden" id="baixaItemCartaoCredito" name="baixaItem.cartaoCredito.id"<c:if test="${baixaItem.cartaoCredito ne null}"> value="${baixaItem.cartaoCredito.id}"</c:if>/>
				<input type="hidden" id="baixaItemIsLimite" name="baixaItem.isLimite" value="${baixaItem.isLimite eq null ? true : baixaItem.isLimite}" />
				<input type="hidden" id="baixaItemConta" name="baixaItem.conta.id"<c:if test="${baixaItem.conta ne null}"> value="${baixaItem.conta.id}"</c:if>/>
				
				<input type="hidden" name="baixaItem.cheque.id"<c:if test="${baixaItem.cheque ne null}"> value="${baixaItem.cheque.id}"</c:if> />
				<input type="hidden" name="baixaItem.cheque.talaoCheque.id"<c:if test="${baixaItem.cheque ne null and baixaItem.cheque.talaoCheque ne null}"> value="${baixaItem.cheque.talaoCheque.id}"</c:if> />
				<input type="hidden" name="baixaItem.cheque.banco.id"<c:if test="${baixaItem.cheque ne null and baixaItem.cheque.banco ne null}"> value="${baixaItem.cheque.banco.id}"</c:if> />
				<input type="hidden" name="baixaItem.cheque.numero"<c:if test="${baixaItem.cheque ne null}"> value="${baixaItem.cheque.numero}"</c:if> />
				<input type="hidden" name="baixaItem.cheque.nome.id"<c:if test="${baixaItem.cheque ne null and baixaItem.cheque.nome ne null}"> value="<c:out value="${baixaItem.cheque.nome.id}"/>"</c:if> />
				<input type="hidden" name="baixaItem.cheque.data"<c:if test="${baixaItem.cheque ne null}"> value="<joda:format pattern="${patternDate}" value="${baixaItem.cheque.data}" />"</c:if> />
				<input type="hidden" name="baixaItem.cheque.obs"<c:if test="${baixaItem.cheque ne null}"> value="<c:out value="${baixaItem.cheque.obs}"/>"</c:if> />
				
				<input type="hidden" id="baixaItemArquivoId" name="baixaItem.arquivo.id"<c:if test="${baixaItem ne null and baixaItem.arquivo ne null and baixaItem.arquivo.id ne null}"> value="${baixaItem.arquivo.id}"</c:if> />
				<input type="hidden" id="baixaItemArquivoTemp" name="baixaItem.arquivo.temp"<c:if test="${baixaItem ne null}"> value="${viewid}_baixaItem_${baixaItem.seq}"</c:if> />
				<input type="hidden" id="baixaItemArquivoNome" name="baixaItem.arquivo.nome"<c:if test="${baixaItem ne null and baixaItem.arquivo ne null and baixaItem.arquivo.nome ne null}"> value="<c:out value="${baixaItem.arquivo.nome}"/>"</c:if> />
				<input type="hidden" id="baixaItemArquivoContentType" name="baixaItem.arquivo.contentType"<c:if test="${baixaItem ne null and baixaItem.arquivo ne null and baixaItem.arquivo.contentType ne null}"> value="<c:out value="${baixaItem.arquivo.contentType}"/>"</c:if> />
				<input type="hidden" id="baixaItemArquivoSize" name="baixaItem.arquivo.size"<c:if test="${baixaItem ne null and baixaItem.arquivo ne null and baixaItem.arquivo.size ne null}"> value="${baixaItem.arquivo.size}"</c:if> />
				
				<input type="hidden" id="baixaItemComprovanteId" name="baixaItem.comprovante.id"<c:if test="${baixaItem ne null and baixaItem.comprovante ne null and baixaItem.comprovante.id ne null}"> value="${baixaItem.comprovante.id}"</c:if> />
				<input type="hidden" id="baixaItemComprovanteTemp" name="baixaItem.comprovante.temp"<c:if test="${baixaItem ne null}"> value="<c:out value="${viewid}_baixaItem_comprovante_${baixaItem.seq}"/>"</c:if> />
				<input type="hidden" id="baixaItemComprovanteNome" name="baixaItem.comprovante.nome"<c:if test="${baixaItem ne null and baixaItem.comprovante ne null and baixaItem.comprovante.nome ne null}"> value="<c:out value="${baixaItem.comprovante.nome}"/>"</c:if> />
				<input type="hidden" id="baixaItemComprovanteContentType" name="baixaItem.comprovante.contentType"<c:if test="${baixaItem ne null and baixaItem.comprovante ne null and baixaItem.comprovante.contentType ne null}"> value="<c:out value="${baixaItem.comprovante.contentType}"/>"</c:if> />
				<input type="hidden" id="baixaItemComprovanteSize" name="baixaItem.comprovante.size"<c:if test="${baixaItem ne null and baixaItem.comprovante ne null and baixaItem.comprovante.size ne null}"> value="${baixaItem.comprovante.size}"</c:if> />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${baixaItem.viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${baixaItem.viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogBaixaItemTitle"><fmt:message key="${param.tipo eq 'DES' ? 'pagamento' : 'recebimento'}"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div class="container-fluid">
						
						<div class="row">
						
							<div class="col-xs-12 col-sm-4 form-group">
	  							<label class="control-label" for="baixaItemData"><fmt:message key="data" /></label>
  								<p class="form-control-static"><joda:format pattern="${patternDate}" value="${baixaItem.lancamentoParcela.data}" /></p>
  							</div>
  							
  							<div class="col-xs-12 col-sm-8 form-group">
  								<label class="control-label" for="baixaItemMeio">
	  								<fmt:message key="meio" />
	  								<span class="required">*</span>
  								</label>
	  							<div class="input-group">
	  								<c:if test="${isReadOnly}">
	  									<p class="form-control-static"><fmt:message key="MeioPagamento.${baixaItem.meio}" /></p>
	  									<input type="hidden" id="baixaItemMeio" name="baixaItem.meio" value="${baixaItem.meio}" />
	  								</c:if>
	  								<c:if test="${!isReadOnly}">
			  							<select id="baixaItemMeio" name="baixaItem.meio" class="form-control">
			  								<option value=""><fmt:message key="selecione___" /></option>
			  								<option value="CDB"<c:if test="${baixaItem.meio eq 'CDB'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CDB" /></option>
			  								<option value="CCR"<c:if test="${baixaItem.meio eq 'CCR'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CCR" /></option>
			  								<option value="INT"<c:if test="${baixaItem.meio eq 'INT'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.INT" /></option>
			  								<option value="DEA"<c:if test="${baixaItem.meio eq 'DEA'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DEA" /></option>
			  								<option value="DIN"<c:if test="${baixaItem.meio eq 'DIN'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DIN" /></option>
			  								<option value="CHQ"<c:if test="${baixaItem.meio eq 'CHQ'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CHQ" /></option>
			  								<option value="TRA"<c:if test="${baixaItem.meio eq 'TRA'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.TRA" /></option>
			  								<option value="DOC"<c:if test="${baixaItem.meio eq 'DOC'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DOC" /></option>
			  							</select>
		  							</c:if>
		  							<span class="input-group-btn">
		  								<button class="btn btn-default" type="button" id="btBaixaItemConta" title="<fmt:message key="conta"/>">
	       									<i class="fa fa-folder-open"></i>
										</button>
										<button class="btn btn-default" type="button" id="btBaixaItemCartaoCredito" title="<fmt:message key="cartaoCredito"/>">
		  									<i class="fa fa-credit-card"></i>
	       								</button>
										<button class="btn btn-default" type="button" id="btBaixaItemCheque" title="<fmt:message key="cheque"/>">
	       									<i class="fa fa-check-circle"></i>
										</button>
										<button class="btn btn-default" type="button" id="btBaixaItemBoleto" title="<fmt:message key="codigoBarras"/>">
        									<i class="fa fa-barcode"></i>
										</button>
										<button class="btn btn-default" type="button" id="btBaixaItemArquivo" title="<fmt:message key="arquivo"/>">
        									<i class="fa fa-paperclip"></i>
										</button>
									</span>
								</div>
  							</div>
						
						</div>
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="baixaItemLancamentoParcelaValor">
	  								<fmt:message key="${param.tipo eq 'DES' ? 'aPagar' : 'aReceber'}" />
	  							</label>
								<p id="baixaItemLancamentoParcelaValor" class="form-control-static text-right"><fmt:formatNumber type="number" value="${baixaItem.lancamentoParcela.valor}" maxFractionDigits="2" minFractionDigits="2" /></p>
							</div>
							
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="baixaItemValor">
	  								<fmt:message key="${param.tipo eq 'DES' ? 'pago' : 'recebido'}" />
	  								<span class="required">*</span>
	  							</label>
	  							<div class="input-group">
	  								<c:if test="${isReadOnly}">
	  									<input type="hidden" id="baixaItemValor" name="baixaItem.valor" value="${baixaItem.valor}" />
	  									<p class="form-control-static text-right"><fmt:formatNumber type="number" value="${baixaItem.valor}" maxFractionDigits="2" minFractionDigits="2" />&nbsp;</p>
	  								</c:if>
	  								<c:if test="${!isReadOnly}">
	  									<input type="text" id="baixaItemValor" name="baixaItem.valor" value="${baixaItem.valor}" class="form-control" />
	  								</c:if>
	  								<span class="input-group-btn">
	  									<button class="btn btn-default" type="button" id="btBaixaItemComprovante" title="<fmt:message key="arquivo"/>">
        									<i class="fa fa-paperclip"></i>
										</button>
	  								</span>
  								</div>
							</div>
						
						</div>
						
						<div class="row">
						
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="baixaItemDiferenca">
	  								<fmt:message key="diferenca" />
	  							</label>
	  							<c:if test="${isReadOnly}">
	  								<input type="hidden" id="baixaItemDiferenca" name="baixaItem.diferenca" value="${baixaItem.diferenca}" class="form-control" />
	  								<p class="form-control-static text-right"><fmt:formatNumber type="number" value="${baixaItem.diferenca}" maxFractionDigits="2" minFractionDigits="2" /></p>
	  							</c:if>
	  							<c:if test="${!isReadOnly}">
	  								<input type="text" id="baixaItemDiferenca" name="baixaItem.diferenca" value="${baixaItem.diferenca}" class="form-control" />
	  							</c:if>
							</div>
							
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="baixaItemMotivo"><fmt:message key="motivo" /></label>
	  							<div class="input-group">
	  								<select 
			    						id="baixaItemMotivo" 
			    						name="baixaItem.motivo.id" 
			    						class="form-control"
			    						data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=MOTIVO_DIFERENCA"
			    						data-search="${pageContext.request.contextPath}/descricao/consultar?descricaoTipo=MOTIVO_DIFERENCA"
			    						data-load-options="${pageContext.request.contextPath}/descricao/carregaSelect?descricaoTipo=MOTIVO_DIFERENCA"
			    						data-category="motivo">
			    						<c:if test="${motivoList eq null or empty motivoList}">
			    							<option value=""><fmt:message key="nenhumRegistroEncontrado"/></option>
			    						</c:if>
			    						<c:if test="${motivoList ne null and !empty motivoList}">
			    							<option value=""><fmt:message key="selecione___"/></option>
			    							<c:forEach var="motivo" items="${motivoList}">
			    								<option value="${motivo.id}"<c:if test="${baixaItem.motivo eq motivo}"> selected="selected"</c:if>><c:out value="${motivo.nome}"/></option>
			    							</c:forEach>
			    						</c:if>
			    					</select>
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.motivo"/>">
			    							<i class="fa fa-plus"></i>
        								</button>
        								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.motivo"/>">
        									<i class="fa fa-pencil"></i>
        								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.motivo"/>">
											<i class="fa fa-search"></i>
        								</button>
      								</span>
	  							</div>
							</div>
						
						</div>
						
						<div class="form-group">
  							<label class="control-label" for="baixaItemObs"><fmt:message key="obs" /></label>
  							<textarea id="baixaItemObs" name="baixaItem.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${baixaItem ne null and baixaItem.obs ne null}"><c:out value="${baixaItem.obs}"/></c:if></textarea>
  						</div>
						
					</div>
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/baixa.baixaItem.js?v${applicationVersion}"></script>