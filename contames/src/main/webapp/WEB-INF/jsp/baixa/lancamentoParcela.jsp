<fmt:message key="patternDate" var="patternDate" />
<div id="dialogBaixaLancamentoParcela" class="modal cm-modal-data-table" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form 
				id="frmBaixaLancamentoParcela" 
				action="${pageContext.request.contextPath}/baixa/lancamentoParcela"
				class="data-table">
			
				<input type="hidden" id="baixaLancamentoParcelaTipo" name="tipo" value="${param.tipo}" />
				<input type="hidden" name="cartaoCredito" value="${param.cartaoCredito}" />
				<c:forEach var="_status" items="${status}"><input type="hidden" name="status[]" value="${_status}" /></c:forEach>
				<c:if test="${idsNotIn ne null}">
					<c:forEach var="_idNotIn" items="${idsNotIn}">
						<input type="hidden" name="idsNotIn[]" value="${_idNotIn}" />
					</c:forEach>
				</c:if>
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogBaixaLancamentoParcelaTitle"><fmt:message key="${param.tipo eq 'DES' ? 'pagamentos' : 'recebimentos'}"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div class="panel panel-default">
						
						<div class="panel-heading cm-panel-heading-toolbar">
				
							<div class="container-fluid">
							
								<div class="row">
					
									<div class="col-xs-12 col-sm-1">
										<label class="form-control-static text-nowrap" for="baixaLancamentoParcelaDataDe"><fmt:message key="dataDe" /></label>
									</div>
							
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="input-group">
			  								<input type="text" id="baixaLancamentoParcelaDataDe" name="dataDe" class="form-control" value="${param.dataDe}" data-category="dataDe lancamentoParcela.dataDe" />
			  								<span class="input-group-btn">
				  								<button class="btn btn-default" type="button" id="btBaixaLancamentoParcelaDataDe">
				  									<i class="fa fa-calendar-o"></i>
			       								</button>
											</span>
		  								</div>
									</div>
							
									<div class="col-xs-12 col-sm-1">
										<label class="form-control-static" for="baixaLancamentoParcelaDataAte"><fmt:message key="ate" /></label>
									</div>
							
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="input-group">
			  								<input type="text" id="baixaLancamentoParcelaDataAte" name="dataAte" class="form-control" value="${param.dataAte}" data-category="dataAte lancamentoParcela.dataAte" />
			  								<span class="input-group-btn">
				  								<button class="btn btn-default" type="button" id="btBaixaLancamentoParcelaDataAte">
				  									<i class="fa fa-calendar-o"></i>
			       								</button>
											</span>
		  								</div>
									</div>
							
									<div class="col-xs-12 col-sm-4 form-group">
										<div class="input-group">
											<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
											<span class="input-group-btn">
												<button class="btn btn-default" type="submit">
													<i class="fa fa-search"></i>
												</button>
											</span>
						    			</div>
									</div>
									
								</div>
								
							</div>
							
						</div>
					
					
						<div class="result search-table">
							<jsp:include page="/WEB-INF/jsp/baixa/lancamentoParcela_table.jsp"/>
	   					</div>
					
					</div>
						
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="button" id="btBaixaLancamentoParcelaOk" class="btn btn-primary" disabled="disabled">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/baixa.lancamentoParcela.js?v${applicationVersion}"></script>