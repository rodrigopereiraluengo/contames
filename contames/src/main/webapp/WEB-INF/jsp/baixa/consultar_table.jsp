<fmt:message key="patternDate" var="patternDate" />

<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhum${tipo eq 'DES' ? 'Pagamento' : 'Recebimento'}Encontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
			
		<c:set var="path" value="lancamentos/${tipo eq 'DES' ? 'pagamentos' : 'recebimentos'}"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" name="${tipo eq 'DES' ? 'pagamentos' : 'recebimentos'}" column="quantBaixaItem" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" name="${tipo eq 'DES' ? 'aPagar' : 'aReceber'}" column="totalLancamentoParcela" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" name="${tipo eq 'DES' ? 'pago' : 'recebido'}" column="totalBaixaItem" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" name="diferenca" column="totalDiferenca" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="status" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyBaixa">
				<c:forEach var="baixa" items="${baixaList}">
					<tr data-id="${baixa['id']}" data-category="baixa_${baixa['id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${baixa['_data']}" /></td>
						<td class="text-right<c:if test="${f:orderByContains('quantBaixaItem', orderByList)}"> font-bold</c:if>">${baixa['quantbaixaitem']}</td>
						<td class="text-right<c:if test="${f:orderByContains('totalLancamentoParcela', orderByList)}"> font-bold</c:if>"><fmt:formatNumber type="number" value="${baixa['totallancamentoparcela']}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right<c:if test="${f:orderByContains('totalBaixaItem', orderByList)}"> font-bold</c:if>"><fmt:formatNumber type="number" value="${baixa['totalbaixaitem']}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right<c:if test="${f:orderByContains('totalDiferenca', orderByList)}"> font-bold</c:if>"><fmt:formatNumber type="number" value="${baixa['totaldiferenca']}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-center<c:if test="${baixa['status'] eq 'CAN'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${baixa['status']}</td>
						<td class="text-center">
							<c:if test="${baixa['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(baixa['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>