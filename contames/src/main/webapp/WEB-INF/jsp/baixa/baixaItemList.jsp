<fmt:message key="patternDate" var="patternDate" />
<div id="layBaixaItem">
	<c:if test="${baixaItemList eq null or empty baixaItemList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhum${tipo eq 'DES' ? 'Pagamento' : 'Recebimento'}Encontrado"/>
			</strong>
		</div>
	</c:if>
	<c:if test="${baixaItemList ne null and !empty baixaItemList}">
		<div class="table-responsive">
			<table id="tblBaixaItem" class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="n_" /></th>
						<th class="cm-th"><fmt:message key="data" /></th>
						<th class="cm-th"><fmt:message key="${baixa.tipo eq 'DES' ? 'pagarA' : 'receberDe'}" /></th>
						<th class="cm-th"><fmt:message key="documento" /></th>
						<th class="cm-th"><fmt:message key="meio" /></th>
						<th class="cm-th"><fmt:message key="${baixa.tipo eq 'DES' ? 'aPagar' : 'aReceber'}" /></th>
						<th class="cm-th"><fmt:message key="${baixa.tipo eq 'DES' ? 'pago' : 'recebido'}" /></th>
						<th class="cm-th"><fmt:message key="diferenca" /></th>
						<th class="cm-th"><fmt:message key="motivo" /></th>
					</tr>
				</thead>
				<tbody id="tbyBaixaItem">
					<c:forEach var="baixaItem" items="${baixaItemList}">
						<tr 
							data-index="${baixaItem.seq}" 
							data-id="${baixaItem.id}" 
							data-lancamento-parcela="${baixaItem.lancamentoParcela.id}"
							data-status="${baixaItem.status}"
							data-category="lancamentoParcela_${baixaItem.lancamentoParcela.id}">
							<td class="text-right">${baixaItem.seq + 1}</td>
							<td class="text-center"><joda:format pattern="${patternDate}" value="${baixaItem.lancamentoParcela.data}" /></td>
							<td><c:out value="${baixaItem.lancamentoParcela.lancamento.favorecido.displayNome}"/></td>
							<td>
							
								<c:if test="${baixaItem.conta ne null and baixaItem.conta.id ne null }">
									<input type="hidden" id="isConfirmContaSaldoInsuficiente_${baixaItem.conta.id}" name="isConfirmContaSaldoInsuficiente_${baixaItem.conta.id}" value="${baixaItem.id eq null  ? false : true}" />
								</c:if>
								<c:if test="${baixaItem.cartaoCredito ne null and baixaItem.cartaoCredito.id ne null }">
									<input type="hidden" id="isConfirmCartaoCreditoLimiteInsuficiente_${baixaItem.cartaoCredito.id}" name="isConfirmCartaoCreditoLimiteInsuficiente_${baixaItem.cartaoCredito.id}" value="${baixaItem.id eq null  ? false : true}" />
								</c:if>
							
								<c:if test="${baixaItem.lancamentoParcela.lancamento.tipoDoc ne null}">
									<fmt:message key="DocTipo.${baixaItem.lancamentoParcela.lancamento.tipoDoc}"/>
								</c:if>
								<c:if test="${baixaItem.lancamentoParcela.lancamento.arquivoDoc ne null}">
									<a class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${baixaItem.lancamentoParcela.lancamento.arquivoDoc.id}/download/${baixaItem.lancamentoParcela.lancamento.arquivoDoc.nome}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
								<c:if test="${baixaItem.lancamentoParcela.lancamento.tipoDoc ne null and baixaItem.lancamentoParcela.lancamento.numeroDoc ne null}">
								&nbsp;-&nbsp;
								</c:if>
								<c:if test="${baixaItem.lancamentoParcela.lancamento.numeroDoc ne null}">
									<c:out value="${baixaItem.lancamentoParcela.lancamento.numeroDoc}"/>
								</c:if>
							</td>
							<td class="text-center">
								<fmt:message key="MeioPagamento.${baixaItem.meio}"/>
								<c:if test="${baixaItem.conta ne null and baixa.tipo eq 'DES'}">
									&nbsp;<c:out value="${baixaItem.conta.nome}"/>
								</c:if>
								
								<c:if test="${baixaItem.cartaoCredito ne null}">
									&nbsp;<c:out value="${baixaItem.cartaoCredito.nome}"/>
								</c:if>
								
								<c:if test="${baixaItem.cheque ne null}">
									<c:if test="${baixaItem.cheque.talaoCheque ne null}">
										&nbsp;<c:out value="${baixaItem.cheque.talaoCheque.conta.nome}" />
									</c:if>
									<c:if test="${baixaItem.cheque.banco ne null}">
										&nbsp;<c:out value="${baixaItem.cheque.banco.displayNome}" />
									</c:if>
									&nbsp;<c:out value="${baixaItem.cheque.numero}"/>
								</c:if>
								
								
								<!-- Boleto LancamentoParcela -->
								<c:if test="${baixaItem.boletoTipo eq null and baixaItem.boletoNumero eq null and baixaItem.lancamentoParcela.boletoTipo ne null and baixaItem.lancamentoParcela.boletoNumero ne null}">
									<a title="${f:escapeJS(f:outHtml(baixaItem.lancamentoParcela.boletoNumero))}" class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(baixaItem.lancamentoParcela.boletoNumero))}', title: '<fmt:message key="BoletoTipo.${baixaItem.lancamentoParcela.boletoTipo}" />', stopPropagation: true}, event)" role="button" title="<fmt:message key="codigoBarras"/>">
										<i class="fa fa-barcode"></i>
									</a>
								</c:if>
								
								<!-- Boleto BaixaItem -->
								<c:if test="${baixaItem.boletoTipo ne null and baixaItem.boletoNumero ne null}">
									<a title="${f:escapeJS(f:outHtml(baixaItem.boletoNumero))}" class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(baixaItem.boletoNumero))}', title: '<fmt:message key="BoletoTipo.${baixaItem.boletoTipo}" />', stopPropagation: true}, event)" role="button" title="<fmt:message key="codigoBarras"/>">
										<i class="fa fa-barcode"></i>
									</a>
								</c:if>
								
								
								<!-- Arquivo LancamentoParcela -->
								<c:if test="${baixaItem.arquivo eq null and baixaItem.lancamentoParcela.arquivo ne null}">
									<a title="${baixaItem.lancamentoParcela.arquivo.nome}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${baixaItem.lancamentoParcela.arquivo.id}/download/${baixaItem.lancamentoParcela.arquivo.nome}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
								
								<!-- Arquivo BaixaItem -->
								<c:if test="${baixaItem.arquivo ne null and baixaItem.arquivo.nome ne null}">
									<a title="${baixaItem.arquivo.nome}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/download/${viewid}_baixaItem_${baixaItem.seq}<c:if test="${baixaItem.arquivo.id ne null}">/${baixaItem.arquivo.id}</c:if>/${baixaItem.arquivo.nome}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
							
							</td>
							<td class="text-right"><fmt:formatNumber type="number" value="${baixaItem.lancamentoParcela.valor}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td class="text-right">
								<fmt:formatNumber type="number" value="${baixaItem.valor}" maxFractionDigits="2" minFractionDigits="2" />
								<!-- Arquivo BaixaItem -->
								<c:if test="${baixaItem.comprovante ne null and baixaItem.comprovante.nome ne null}">
									<a title="${baixaItem.comprovante.nome}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/download/${viewid}_baixaItem_comprovante_${baixaItem.seq}<c:if test="${baixaItem.arquivo.id ne null}">/${baixaItem.comprovante.id}</c:if>/${baixaItem.comprovante.nome}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
							</td>
							<td class="text-right"><c:if test="${baixaItem.diferenca ne null}"><fmt:formatNumber type="number" value="${baixaItem.diferenca}" maxFractionDigits="2" minFractionDigits="2" /></c:if></td>
							<td class="text-center"><c:if test="${baixaItem.motivo ne null}"><c:out value="${baixaItem.motivo.nome}"/></c:if></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr class="cm-tr-total">
						<td class="text-right" colspan="5"><fmt:message key="total"/> :</td>
						<td class="text-right"><fmt:formatNumber type="number" value="${baixa.totalLancamentoParcela}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right"><fmt:formatNumber type="number" value="${baixa.totalBaixaItem}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right">
							<c:choose>
								<c:when test="${baixa.totalDiferenca eq 0}">&nbsp;</c:when>
								<c:otherwise>
									<fmt:formatNumber type="number" value="${baixa.totalDiferenca}" maxFractionDigits="2" minFractionDigits="2" />		
								</c:otherwise>
							</c:choose>
						</td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:if>
</div>