<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhum${tipo eq 'DES' ? 'Pagamento' : 'Recebimento'}Encontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
			
		<c:set var="path" value="baixa/lancamentoParcela"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="${tipo eq 'DES' ? 'pagarA' : 'receberDe'}" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="documento" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="meio" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="parcela" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="valor" page="${page}" path="${path}" />
				</tr>
			</thead>
			<tbody id="tbyLancamentoParcela">
				<c:forEach var="lancamentoParcela" items="${lancamentoParcelaList}">
					<tr data-id="${lancamentoParcela['id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${lancamentoParcela['_data']}" /></td>
						<td <c:if test="${f:orderByContains('favorecido', orderByList)}"> class="font-bold"</c:if>>${lancamentoParcela['favorecido']}</td>
						<td class="text-center<c:if test="${f:orderByContains('documento', orderByList)}"> font-bold</c:if>">
							${lancamentoParcela['documento']}
							<c:if test="${lancamentoParcela['fatcartaocredito_nome'] ne null}">
							&nbsp;<c:out value="${lancamentoParcela['fatcartaocredito_nome']}" />
							</c:if>
							<c:if test="${lancamentoParcela['arquivodoc_id'] ne null}">
								<a title="${lancamentoParcela['arquivodoc_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${lancamentoParcela['arquivodoc_id']}/download/${lancamentoParcela['arquivodoc_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('meio', orderByList)}"> font-bold</c:if>">
							${lancamentoParcela['meio']}
							<c:if test="${lancamentoParcela['boletotipo'] ne null and lancamentoParcela['boletonumero'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(lancamentoParcela['boletonumero']))}', title: '<fmt:message key="BoletoTipo.${lancamentoParcela['boletotipo']}" />', stopPropagation: true}, event)" role="button" title="<fmt:message key="codigoBarras"/>">
									<i class="fa fa-barcode"></i>
								</a>
							</c:if>
							<c:if test="${lancamentoParcela['arquivo_id'] ne null}">
								<a title="${lancamentoParcela['arquivo_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${lancamentoParcela['arquivo_id']}/download/${lancamentoParcela['arquivo_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('parcela', orderByList)}"> font-bold</c:if>">${lancamentoParcela['parcela']}</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>">
							<fmt:formatNumber type="number" value="${lancamentoParcela['valor']}" maxFractionDigits="2" minFractionDigits="2" />
							<c:if test="${lancamentoParcela['comprovante_id'] ne null}">
								<a title="${lancamentoParcela['comprovante_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${lancamentoParcela['comprovante_id']}/download/${lancamentoParcela['comprovante_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
				
	</div>
	
	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>