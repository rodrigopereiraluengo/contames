<fmt:message key="patternDate" var="patternDate"/>
<div id="dialogBaixaCadastro" class="modal" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<form id="frmBaixaCadastro" data-tipo="${baixa.tipo}">
			
				<input type="hidden" id="baixaViewid" name="viewid" value="${viewid}" />
				<input type="hidden" name="baixa.id" value="${baixa.id}" />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogBaixaCadastroTitle"><fmt:message key="${baixa.tipo eq 'DES' ? 'pagamento' : 'recebimento'}"/></h4>
				</div>
				
				<div class="modal-body">
					
					<div class="container-fluid">
						
						<div class="row">
						
							<div class="col-xs-12 col-sm-3 form-group">
								<label class="control-label" for="baixaData">
	  								<fmt:message key="data" />
	  								<span class="required">*</span>
  								</label>
  								<div class="input-group">
	  								<input type="text" id="baixaData" name="baixa.data" value="<joda:format pattern="${patternDate}" value="${baixa.data}" />" class="form-control" />
	  								<span class="input-group-btn">
		  								<button class="btn btn-default" type="button" id="btBaixaData">
		  									<i class="fa fa-calendar-o"></i>
	       								</button>
									</span>
  								</div>
							</div>
							
							<div class="col-xs-12 col-sm-3 col-sm-offset-6 form-group">
	  							<label class="control-label" for="baixaStatus">
	  								<fmt:message key="status" />
	  								<span class="required">*</span>
								</label>
								<c:if test="${isClosed}">
									<c:if test="${baixa.status ne 'BXD'}"><p class="form-control-static"><fmt:message key="LancamentoStatus.${baixa.status}"/></p></c:if>
									<c:if test="${baixa.status eq 'BXD'}"><p class="form-control-static"><fmt:message key="LancamentoStatus.${baixa.tipo}.BXD"/></p></c:if>
								</c:if>
								<c:if test="${!isClosed}">
		  							<select id="baixaStatus" name="baixa.status" class="form-control">
										<option value="STD"<c:if test="${baixa.status eq 'STD'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.STD"/></option>
										<option value="BXD"<c:if test="${baixa.status eq 'BXD'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.${baixa.tipo}.BXD"/></option>
										<c:if test="${baixa.id ne null}">
											<option value="CAN"<c:if test="${baixa.status eq 'CAN'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.CAN"/></option>
										</c:if>
									</select>
								</c:if>
	  						</div>
						
						</div>
						
						
						<!-- Pagamentos Recebimentos -->
						<div class="panel panel-default">
			
							<div class="panel-heading">
			    				<div class="row">
									<div class="col-xs-4 text-nowrap">
				   						<p class="form-control-static font-bold"><fmt:message key="${baixa.tipo eq 'DES' ? 'pagamentos' : 'recebimentos'}"/></p>
				   					</div>
				   					<div class="col-xs-8 text-right">
				   						<div class="btn-group">
				   							<button type="button" id="baixaBtAdicionarBaixaItem" class="btn btn-default bt-new" title="<fmt:message key="selecionar"/>">
				   								<i class="fa fa-search"></i>
				   							</button>
				   							<button type="button" id="baixaBtEditarBaixaItem" class="btn btn-default bt-edit" disabled="disabled" title="<fmt:message key="editar"/>">
				   								<i class="fa fa-pencil"></i>
				   							</button>
				   							<button type="button" id="baixaBtExcluitBaixaItem" class="btn btn-default bt-delete" disabled="disabled" title="<fmt:message key="remover"/>">
				   								<i class="fa fa-trash"></i>
				   							</button>
				   						</div>
				   					</div>
				   				</div>
			  				</div>
			  				
			  				<div class="data-table">
								<div class="result search-table">
									<jsp:include page="baixaItemList.jsp"/>
								</div>
							</div>
							
						</div>
						
						<div class="form-group">
  							<label class="control-label" for="baixaObs"><fmt:message key="obs" /></label>
  							<textarea id="baixaObs" name="baixa.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${baixa ne null and baixa.obs ne null}"><c:out value="${baixa.obs}"/></c:if></textarea>
  						</div>
						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
		       	</div>
		       	
		       	<c:if test="${baixa ne null and baixa.id ne null}">
			       	<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${baixa.criacao}" create="create" pessoa="${baixa.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${baixa.alteracao}" create="update" pessoa="${baixa.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
			
			</form>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/baixa.cadastro.js?v${applicationVersion}"></script>