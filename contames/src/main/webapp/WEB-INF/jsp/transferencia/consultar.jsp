<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="transferencias"/></h1>
						<p class="description"><fmt:message key="transferencias.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="resumo" />
							</li>
				  			<li><fmt:message key="lancamentos" /></li>
				  			<li><fmt:message key="transferencias"/></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<form 
			id="frmTransferenciaConsultar" 
			action="${pageContext.request.contextPath}/lancamentos/transferencias"
			data-form="${pageContext.request.contextPath}/transferencia/cadastro"
			data-delete="${pageContext.request.contextPath}/transferencia/excluir"
			class="data-table">
			
			<div class="panel panel-default">
			
				<div class="panel-heading cm-panel-heading-toolbar">
					
					<div class="container-fluid">
						<div class="row">
				
							<div class="col-xs-12 col-sm-4 form-group">
					  			<div class="btn-group">
						  			<button type="button" class="btn btn-default bt-new">
						  				<i class="fa fa-plus"></i>
						  				<fmt:message key="nova" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-edit" disabled="disabled">
						  				<i class="fa fa-pencil"></i>
						  				<fmt:message key="editar" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-delete" disabled="disabled">
						  				<i class="fa fa-trash"></i>
						  				<fmt:message key="excluir" />
					  				</button>
								</div>
							</div>
				
							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
								<div class="input-group">
									<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<i class="fa fa-search"></i>
										</button>
									</span>
				    			</div>		
							</div>
			
						</div>
					</div>
				</div>
			
				<div class="result search-table">
					<jsp:include page="/WEB-INF/jsp/transferencia/consultar_table.jsp" />	   			
   				</div>
   				
			</div>
   			
		</form>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/transferencia.consultar.js?v${applicationVersion}"></script>
	
	</tiles:putAttribute>
	
</tiles:insertTemplate>