<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumaTransferenciaEncontrada"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
		
		<c:set var="path" value="lancamentos/transferencias"/>
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="tipo" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="origem" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" column="destino" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="valor" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyTransferencia">
				<c:forEach var="transferencia" items="${transferenciaList}">
					<tr data-id="${transferencia['id']}" data-category="transferencia_${transferencia['id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${transferencia['_data']}" /></td>
						<td class="text-center<c:if test="${f:orderByContains('tipo', orderByList)}"> font-bold</c:if>">
							${transferencia['tipo']}
							<c:if test="${transferencia['arquivo_id'] ne null}">
								<a  class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${transferencia['arquivo_id']}/download/${transferencia['arquivo_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td class="text-center<c:if test="${f:orderByContains('origem', orderByList)}"> font-bold</c:if>">${transferencia['origem']}</td>
						<td class="text-center<c:if test="${f:orderByContains('destino', orderByList)}"> font-bold</c:if>">${transferencia['destino']}</td>
						<td class="text-center<c:if test="${transferencia['status'] eq 'CAN'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${transferencia['status']}</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>">
							<c:if test="${transferencia['valor'] ne null}">
								<fmt:formatNumber type="number" value="${transferencia['valor']}" maxFractionDigits="2" minFractionDigits="2" />
							</c:if>
						</td>
						<td class="text-center">
							<c:if test="${transferencia['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(transferencia['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
 			
 	<div class="panel-footer">
		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
	
</c:if>