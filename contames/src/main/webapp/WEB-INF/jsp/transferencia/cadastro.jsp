<fmt:message key="patternDate" var="patternDate" />
<c:set var="isReadOnly" value="${transferencia ne null and transferencia.status eq 'COM' or transferencia.statusOrigem eq 'COM' or transferencia.statusDestino eq 'COM'}"/>
<div id="dialogTransferenciaCadastro" class="modal" style="display: none">
  	<div class="modal-dialog">
   		<div class="modal-content">
   		   		
   			<form id="frmTransferenciaCadastro">
   				
   				<c:if test="${transferencia ne null}"><input type="hidden" name="transferencia.id" value="${transferencia.id}" /></c:if>
   				<input type="hidden" id="transferenciaViewid" name="viewid" value="${viewid}" />
   				<input type="hidden" name="transferencia.cheque.id"<c:if test="${transferencia.cheque ne null}"> value="${transferencia.cheque.id}"</c:if> />
				<input type="hidden" name="transferencia.cheque.talaoCheque.id"<c:if test="${transferencia.cheque ne null and transferencia.cheque.talaoCheque ne null}"> value="${transferencia.cheque.talaoCheque.id}"</c:if> />
   				<input type="hidden" name="transferencia.cheque.numero"<c:if test="${transferencia.cheque ne null}"> value="${transferencia.cheque.numero}"</c:if> />
   				<input type="hidden" name="transferencia.cheque.obs"<c:if test="${transferencia.cheque ne null}"> value="<c:out value="${transferencia.cheque.obs}"/>"</c:if> />
   				
   				<input type="hidden" id="transferenciaIsLimite" name="transferencia.isLimite" value="${transferencia.isLimite eq null ? true : transferencia.isLimite}" />
   				
   				<input type="hidden" id="transferenciaArquivoId" name="transferencia.arquivo.id"<c:if test="${transferencia ne null and transferencia.arquivo ne null and transferencia.arquivo.id ne null}"> value="${transferencia.arquivo.id}"</c:if> />
				<input type="hidden" id="transferenciaArquivoTemp" name="transferencia.arquivo.temp"<c:if test="${transferencia ne null}"> value="${viewid}_transferencia"</c:if> />
				<input type="hidden" id="transferenciaArquivoNome" name="transferencia.arquivo.nome"<c:if test="${transferencia ne null and transferencia.arquivo ne null and transferencia.arquivo.nome ne null}"> value="<c:out value="${transferencia.arquivo.nome}"/>"</c:if> />
				<input type="hidden" id="transferenciaArquivoContentType" name="transferencia.arquivo.contentType"<c:if test="${transferencia ne null and transferencia.arquivo ne null and transferencia.arquivo.contentType ne null}"> value="<c:out value="${transferencia.arquivo.contentType}"/>"</c:if> />
				<input type="hidden" id="transferenciaArquivoSize" name="transferencia.arquivo.size"<c:if test="${transferencia ne null and transferencia.arquivo ne null and transferencia.arquivo.size ne null}"> value="${transferencia.arquivo.size}"</c:if> />
				
	   			<div class="modal-header">
	   				<button type="button" class="close" data-dismiss="modal">
	   					<i class="fa fa-times"></i>
	   				</button>
	   				<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
	        		<h4 class="modal-title"><fmt:message key="transferencia"/></h4>
	      		</div>
	      		
	      		<div class="modal-body">
	      			
	      			<div class="row">
	      				<div class="col-xs-12 col-sm-4 form-group">
	      					<label class="control-label" for="transferenciaData">
								<fmt:message key="data"/>
								<span class="required">*</span>
							</label>
							<div class="input-group">
  								<input type="text" id="transferenciaData" name="transferencia.data"<c:if test="${transferencia ne null and transferencia.data ne null}"> value="<joda:format pattern="${patternDate}" value="${transferencia.data}" />"</c:if> data-category="data" class="form-control" />
  								<span class="input-group-btn">
	  								<button class="btn btn-default" type="button" id="btTransferenciaData">
	  									<i class="fa fa-calendar-o"></i>
       								</button>
								</span>
							</div>
	      				</div>
	      				<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
	      					<label class="control-label" for="transferenciaStatus">
								<fmt:message key="status"/>
								<span class="required">*</span>
							</label>
							<c:if test="${isReadOnly}">
								<p class="form-control-static"><fmt:message key="LancamentoStatus.${transferencia.status}"/></p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<select id="transferenciaStatus" name="transferencia.status" class="form-control">
									<option value="STD"<c:if test="${transferencia ne null and transferencia.status eq 'STD'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.STD"/></option>
									<option value="LAN"<c:if test="${transferencia ne null and transferencia.status eq 'LAN' or transferencia.status eq null}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.LAN"/></option>
									<c:if test="${transferencia.id ne null}">
										<option value="CAN"<c:if test="${transferencia.status eq 'CAN'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.CAN"/></option>
									</c:if>
								</select>
							</c:if>
	      				</div>
      				</div>
      				
      				<div class="form-group">
      					<label class="control-label" for="transferenciaTipo">
							<fmt:message key="tipo"/>
							<span class="required">*</span>
						</label>
						<div class="input-group">
							<c:if test="${isReadOnly}">
								<p class="form-control-static"><fmt:message key="TransferenciaTipo.${transferencia.tipo}" /></p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<select id="transferenciaTipo" name="transferencia.tipo" data-category="tipo" class="form-control">
									<option value="CON"<c:if test="${transferencia ne null and transferencia.tipo eq 'CON'}"> selected="selected"</c:if>><fmt:message key="TransferenciaTipo.CON" /></option>
									<option value="CCR"<c:if test="${transferencia ne null and transferencia.tipo eq 'CCR'}"> selected="selected"</c:if>><fmt:message key="TransferenciaTipo.CCR" /></option>
								</select>
							</c:if>
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" id="btTransferenciaArquivo" title="<fmt:message key="arquivo" />">
  									<i class="fa fa-paperclip"></i>
								</button>
							</span>
						</div>
      				</div>
      				
      				<div class="form-group" id="layTransferenciaConta"<c:if test="${isReadOnly and transferencia.conta eq null}"> style="display: none"</c:if>>
						<label class="control-label" for="transferenciaConta">
							<fmt:message key="origem"/>
							<span class="required">*</span>
						</label>
						<div class="input-group">
							<c:if test="${isReadOnly and transferencia.conta ne null}">
								<p class="form-control-static">
									<c:out value="${transferencia.conta.nome}"/>
									<c:if test="${transferencia.cheque ne null}">
										&nbsp;<fmt:message key="MeioPagamento.CHQ"/>
										&nbsp;<c:out value="${transferencia.cheque.numero}"/>
									</c:if>
								</p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<select 
		    						id="transferenciaConta" 
		    						name="transferencia.conta.id" 
		    						class="form-control"
		    						data-form="${pageContext.request.contextPath}/conta/cadastro?dialogDetalhe=minus"
		    						data-search="${pageContext.request.contextPath}/cadastros/contas?tpl=dialog&dialogId=ContaConsultar&dialogTitle=conta&dialogDetalhe=minus"
		    						data-load-options="${pageContext.request.contextPath}/conta/carregaSelect?isPrincipal=false"
		    						data-category="conta">
		    						<c:if test="${contaList eq null or empty contaList}">
										<option value=""><fmt:message key="nenhumaContaEncontrada"/></option>
									</c:if>
									<c:if test="${contaList ne null and !empty contaList}">
										<option value=""><fmt:message key="selecione___"/></option>
										<c:forEach var="conta" items="${contaList}">
											<option value="${conta.id}"<c:if test="${conta.id eq transferencia.conta.id or transferencia.conta eq null and (conta.isPrincipal ne null and conta.isPrincipal)}"> selected="selected"</c:if> data-is-poupanca="${conta.isPoupanca}"><c:out value="${conta.nome}"/></option>
										</c:forEach>
									</c:if>
		    					</select>
		    					<span class="input-group-btn">
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.conta" />">
		    							<i class="fa fa-plus"></i>
									</button>
									<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.conta" />">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.conta" />">
										<i class="fa fa-search"></i>
									</button>
									<button class="btn btn-default" type="button" id="btTransferenciaCheque" title="<fmt:message key="cheque" />">
	   									<i class="fa fa-check-circle"></i>
									</button>
								</span>
							</c:if>
						</div>
					</div>
					
					<div class="form-group" id="layTransferenciaCartaoCredito"<c:if test="${isReadOnly and transferencia.cartaoCredito eq null}"> style="display: none"</c:if>>
						<label class="control-label" for="transferenciaCartaoCredito">
							<fmt:message key="origem"/>
							<span class="required">*</span>
						</label>
						<div class="input-group">
							<c:if test="${isReadOnly and transferencia.cartaoCredito ne null}">
								<p class="form-control-static"><c:out value="${transferencia.cartaoCredito.nome}"/></p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<select 
		    						id="transferenciaCartaoCredito" 
		    						name="transferencia.cartaoCredito.id" 
		    						class="form-control"
		    						data-form="${pageContext.request.contextPath}/cartaoCredito/cadastro?dialogDetalhe=minus"
		    						data-search="${pageContext.request.contextPath}/cadastros/cartoes?tpl=dialog&dialogId=CartaoCreditoConsultar&?dialogDetalhe=minus&dialogTitle=cartaoCredito"
		    						data-load-options="${pageContext.request.contextPath}/cartaoCredito/carregaSelect"
		    						data-category="cartaoCredito">
		    						<c:if test="${cartaoCreditoList eq null or empty cartaoCreditoList}">
										<option value=""><fmt:message key="nenhumCartaoCreditoEncontrada"/></option>
									</c:if>
									<c:if test="${cartaoCreditoList ne null and !empty cartaoCreditoList}">
										<option value=""><fmt:message key="selecione___"/></option>
										<c:forEach var="cartaoCredito" items="${cartaoCreditoList}">
											<option 
												data-dia-vencimento="${cartaoCredito.diaVencimento}"
												data-dias-pagar="${cartaoCredito.diasPagar}"
												data-limite="<fmt:formatNumber type="number" value="${cartaoCredito.limite}" maxFractionDigits="2" minFractionDigits="2" />"
												data-limite-disponivel="<fmt:formatNumber type="number" value="${cartaoCredito.limiteDisponivel}" maxFractionDigits="2" minFractionDigits="2" />"
												data-limite-utilizado="<fmt:formatNumber type="number" value="${cartaoCredito.limiteUtilizado}" maxFractionDigits="2" minFractionDigits="2" />"
												value="${cartaoCredito.id}"<c:if test="${(transferencia.cartaoCredito eq null and cartaoCredito.isPrincipal) or transferencia.cartaoCredito eq cartaoCredito}"> selected="selected"</c:if>><c:out value="${cartaoCredito.nome}"/></option>
										</c:forEach>
									</c:if>
		    					</select>
		    					<span class="input-group-btn">
		    						<button id="btTransferenciaIsLimite" class="btn btn-default" type="button" title="<fmt:message key="title.utilizarLimite.cartaoCredito" />">
	   									<i class="fa fa-minus-square"></i>
									</button>
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.cartaoCredito" />">
		    							<i class="fa fa-plus"></i>
	   								</button>
	   								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.cartaoCredito" />">
	   									<i class="fa fa-pencil"></i>
	   								</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.cartaoCredito" />">
										<i class="fa fa-search"></i>
	   								</button>
								</span>
							</c:if>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="control-label" for="transferenciaDestino">
							<fmt:message key="destino"/>
							<span class="required">*</span>
						</label>
						<div class="input-group">
							<c:if test="${isReadOnly and transferencia.destino ne null}">
								<p class="form-control-static"><c:out value="${transferencia.destino.nome}"/></p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<select 
		    						id="transferenciaDestino" 
		    						name="transferencia.destino.id" 
		    						class="form-control"
		    						data-form="${pageContext.request.contextPath}/conta/cadastro?dialogDetalhe=minus"
		    						data-search="${pageContext.request.contextPath}/cadastros/contas?tpl=dialog&dialogId=ContaConsultar&dialogTitle=conta&dialogDetalhe=minus"
		    						data-load-options="${pageContext.request.contextPath}/conta/carregaSelect?isPrincipal=false"
		    						data-category="destino">
		    						<c:if test="${contaList eq null or empty contaList}">
										<option value=""><fmt:message key="nenhumaContaEncontrada"/></option>
									</c:if>
									<c:if test="${contaList ne null and !empty contaList}">
										<option value=""><fmt:message key="selecione___"/></option>
										<c:forEach var="conta" items="${contaList}">
											<option value="${conta.id}"<c:if test="${conta.id eq transferencia.destino.id}"> selected="selected"</c:if>><c:out value="${conta.nome}"/></option>
										</c:forEach>
									</c:if>
		    					</select>
		    					<span class="input-group-btn">
		    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.conta" />">
		    							<i class="fa fa-plus"></i>
									</button>
									<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.conta" />">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.conta" />">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</c:if>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-sm-offset-6 form-group">
	      					<label class="control-label" for="transferenciaValor">
								<fmt:message key="valor"/>
								<span class="required">*</span>
							</label>
							<c:if test="${isReadOnly}">
								<p class="form-control-static text-right"><fmt:formatNumber type="number" value="${transferencia.valor}" maxFractionDigits="2" minFractionDigits="2" /></p>
							</c:if>
							<c:if test="${!isReadOnly}">
								<input type="text" id="transferenciaValor" name="transferencia.valor"<c:if test="${transferencia.valor ne null}"> value="${transferencia.valor}"</c:if> data-category="valor" class="form-control" />
							</c:if>
						</div>
	      			</div>
	      		
	      			<div class="form-group">
						<label class="control-label" for="transferenciaObs"><fmt:message key="obs" /></label>
						<textarea id="transferenciaObs" name="transferencia.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${transferencia ne null and transferencia.obs ne null}"><c:out value="${transferencia.obs}"/></c:if></textarea>
					</div>
	      			
	      		</div>
	      		
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default" data-dismiss="modal">
	      				<i class="fa fa-close"></i>
	      				<b><fmt:message key="cancelar"/></b>
      				</button>
	        		<button type="submit" class="btn btn-primary">
	        			<i class="fa fa-save"></i>
	        			<b><fmt:message key="salvar"/></b>
        			</button>
	      		</div>
	      		
	      		<c:if test="${transferencia ne null and transferencia.id ne null}">
			       	<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${transferencia.criacao}" create="create" pessoa="${transferencia.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${transferencia.alteracao}" create="update" pessoa="${transferencia.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
      		
      		</form>
   		
   		</div>
 	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/transferencia.cadastro.js?v${applicationVersion}"></script>