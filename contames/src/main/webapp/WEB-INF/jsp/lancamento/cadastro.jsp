<fmt:message key="patternDate" var="patternDate" />
<div id="dialogLancamentoCadastro" class="modal" style="display: none">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<form id="frmLancamentoCadastro" data-tipo="${lancamento.tipo}" data-is-closed="${isClosed}">
			
				<input type="hidden" id="lancViewid" name="viewid" value="${viewid}" />
				<input type="hidden" id="lancId" name="lancamento.id" value="${lancamento.id}" />
				
				<input type="hidden" id="lancamentoArquivoDocId" name="lancamento.arquivoDoc.id" value="${lancamento.arquivoDoc.id}" />
				<input type="hidden" id="lancamentoArquivoDocTemp" name="lancamento.arquivoDoc.temp" value="${viewid}_lancamento_arquivoDoc" />
				<input type="hidden" id="lancamentoArquivoDocNome" name="lancamento.arquivoDoc.nome" value="${lancamento.arquivoDoc.nome}" />
				<input type="hidden" id="lancamentoArquivoDocContentType" name="lancamento.arquivoDoc.contentType" value="${lancamento.arquivoDoc.contentType}" />
				<input type="hidden" id="lancamentoArquivoDocSize" name="lancamento.arquivoDoc.size" value="${lancamento.arquivoDoc.size}" />
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogLancamentoCadastroTitle">
						<c:choose>
							<c:when test="${lancamento.tipo eq 'CPR'}"><fmt:message key="compra"/></c:when>
							<c:when test="${lancamento.tipo eq 'VEN'}"><fmt:message key="venda"/></c:when>
							<c:otherwise><fmt:message key="lancamentoDe.${lancamento.tipo}"/></c:otherwise>
						</c:choose>
					</h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid">
					
						<div class="row">
  							<div class="col-xs-12 col-sm-3 form-group">
  								<label class="control-label" for="lancData">
	  								<fmt:message key="data" />
	  								<span class="required">*</span>
	  							</label>
	  							<div class="input-group">
	  								<input type="text" id="lancData" name="lancamento.data"<c:if test="${lancamento ne null and lancamento.data ne null}"> value="<joda:format pattern="${patternDate}" value="${lancamento.data}" />"</c:if> data-category="data" class="form-control" />
	  								<span class="input-group-btn">
		  								<button class="btn btn-default" type="button" id="btLancData">
		  									<i class="fa fa-calendar-o"></i>
	       								</button>
									</span>
  								</div>
   							</div>
	  						<div class="col-xs-12 col-sm-6 form-group">
	  						
	  							<label class="control-label" for="lancFavorecido">
	  								<c:if test="${lancamento.tipo eq 'DES' or lancamento.tipo eq 'CPR'}"><fmt:message key="pagarA" /></c:if>
	  								<c:if test="${lancamento.tipo eq 'REC' or lancamento.tipo eq 'VEN'}"><fmt:message key="receberDe" /></c:if>
	  								<span class="required">*</span>
  								</label>
								<div class="input-group">
			  						<input 
	  									type="text" 
	  									id="lancFavorecido" 
	  									name="lancamento.favorecido.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoa&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${lancamento.favorecido ne null}"> value="${lancamento.favorecido.displayNome}" data-val="${lancamento.favorecido.id}"</c:if>
			    						data-category="favorecido"
		    						/>
			  						
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
			    							<i class="fa fa-plus"></i>
	       								</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
	       									<i class="fa fa-pencil"></i>
	       								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
											<i class="fa fa-search"></i>
	       								</button>
	   								</span>
		  						</div>
	  						
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-3 form-group">
	  							
	  							<label class="control-label" for="lancStatus">
	  								<fmt:message key="status" />
	  								<span class="required">*</span>
								</label>
	  							<c:if test="${isClosed}">
	  								<p class="form-control-static"><fmt:message key="LancamentoStatus.${lancamento.status}"/></p>
	  								<input type="hidden" name="lancamento.status" value="${lancamento.status}" />
	  							</c:if>
	  							<c:if test="${!isClosed}">
		  							<select id="lancStatus" name="lancamento.status" class="form-control" data-category="status">
										<option value="STD"<c:if test="${lancamento ne null and lancamento.status eq 'STD'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.STD"/></option>
										<option value="LAN"<c:if test="${lancamento ne null and lancamento.status eq 'LAN' or lancamento eq null}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.LAN"/></option>
										<c:if test="${lancamento.id ne null}">
											<option value="CAN"<c:if test="${lancamento.status eq 'CAN'}"> selected="selected"</c:if>><fmt:message key="LancamentoStatus.CAN"/></option>
										</c:if>
									</select>
	  							</c:if>
	  						</div>
	  						
  						</div>
  						
  						
  						<!-- Documento -->
  						<div class="panel panel-default">
			
							<div class="panel-heading">
			    				<div class="row">
									<div class="col-xs-12">
				   						<p class="form-control-static font-bold"><fmt:message key="documento"/></p>
				   					</div>
			   					</div>
		   					</div>
		   					
		   					<div class="panel-body">
		   						<div class="row">
		   							<div class="col-xs-12 col-sm-3 form-group">
		   								<label class="control-label" for="lancTipoDoc">
			  								<fmt:message key="tipo" />
			  								<a class="info-help" data-message="lancamento.${lancamento.tipo}.tipoDoc.help">(?)</a>
			  							</label>
			  							<div class="input-group">
			  								<c:if test="${isClosed and lancamento.tipoDoc eq 'FT'}">
	  											<p class="form-control-static"><fmt:message key="DocTipo.${lancamento.tipoDoc}"/></p>
	  											<input type="hidden" id="lancTipoDoc" name="lancamento.tipoDoc" value="${lancamento.tipoDoc}" />
	  										</c:if>
	  										<c:if test="${!isClosed or (isClosed and lancamento.tipoDoc ne 'FT')}">
					  							<select id="lancTipoDoc" name="lancamento.tipoDoc" data-category="tipoDoc" class="form-control">
					  								<option value=""><fmt:message key="selecione___"/></option>
													<option value="NF"<c:if test="${lancamento ne null and lancamento.tipoDoc eq 'NF'}"> selected="selected"</c:if>><fmt:message key="DocTipo.NF"/></option>
													<option value="CF"<c:if test="${lancamento ne null and lancamento.tipoDoc eq 'CF'}"> selected="selected"</c:if>><fmt:message key="DocTipo.CF"/></option>
													<option value="RC"<c:if test="${lancamento ne null and lancamento.tipoDoc eq 'RC'}"> selected="selected"</c:if>><fmt:message key="DocTipo.RC"/></option>
													<c:if test="${lancamento.tipo eq 'DES'}"><option value="FT"<c:if test="${lancamento ne null and lancamento.tipoDoc eq 'FT'}"> selected="selected"</c:if>><fmt:message key="DocTipo.FT"/></option></c:if>
													<option value="CT"<c:if test="${lancamento ne null and lancamento.tipoDoc eq 'CT'}"> selected="selected"</c:if>><fmt:message key="DocTipo.CT"/></option>
												</select>
											</c:if>
											<span class="input-group-btn">
		        								<button class="btn btn-default" type="button" id="btLancamentoArquivoDoc" title="<fmt:message key="arquivo"/>">
		        									<i class="fa fa-paperclip"></i>
												</button>
		      								</span>
										</div>
		   							</div>
		   							<div class="col-xs-12 col-sm-3 form-group">
		   								<label class="control-label" for="lancDataDoc">
			  								<fmt:message key="data" />
			  								<span class="required" id="lancDataDocReq" style="display: none">*</span>
			  							</label>
			  							<div class="input-group">
			  								<input type="text" id="lancDataDoc" name="lancamento.dataDoc"<c:if test="${lancamento ne null and lancamento.dataDoc ne null}"> value="<joda:format pattern="${patternDate}" value="${lancamento.dataDoc}" />"</c:if> data-category="dataDoc" class="form-control" />
			  								<span class="input-group-btn">
				  								<button class="btn btn-default" type="button" id="btLancDataDoc">
			       									<i class="fa fa-calendar-o"></i>
												</button>
											</span>
		  								</div>
		   							</div>
		   							<div class="col-xs-12 col-sm-3 form-group" id="layLancNumeroDoc">
		   								<label class="control-label" for="lancNumeroDoc">
			  								<fmt:message key="n_" />
			  							</label>
			  							<input type="text" id="lancNumeroDoc" name="lancamento.numeroDoc"<c:if test="${lancamento ne null and lancamento.numeroDoc ne null}"> value="<c:out value="${lancamento.numeroDoc}"/>"</c:if> data-category="numeroDoc" class="form-control" />
		   							</div>
		   							<div class="col-xs-12 col-sm-3 form-group">
		   								<label class="control-label" for="lancValorDoc">
			  								<fmt:message key="valor" />
			  								<span id="lancValorDocReq" class="required" style="display: none">*</span>
			  							</label>
			  							<c:if test="${isClosed and lancamento.tipoDoc eq 'FT'}">
			  								<p class="form-control-static text-right"><fmt:formatNumber type="number" value="${lancamento.valorDoc}" maxFractionDigits="2" minFractionDigits="2" /></p>
			  								<input type="hidden" id="lancValorDoc" name="lancamento.valorDoc" value="<fmt:formatNumber type="number" value="${lancamento.valorDoc}" maxFractionDigits="2" minFractionDigits="2" />" />
			  							</c:if>
			  							<c:if test="${!isClosed or (isClosed and lancamento.tipoDoc ne 'FT')}">
			  								<input type="text" id="lancValorDoc" name="lancamento.valorDoc"<c:if test="${lancamento ne null and lancamento.valorDoc ne null}"> value="<fmt:formatNumber type="number" value="${lancamento.valorDoc}" maxFractionDigits="2" minFractionDigits="2" />"</c:if> class="form-control" />
			  							</c:if>
		   							</div>
		   						</div>
		   						
		   						<c:if test="${lancamento.tipo eq 'DES'}">
			   						<div class="row" id="layLancCartaoCredito" style="display: none">
			   							<div class="col-xs-12 col-sm-6 form-group">
			   								<label class="control-label" for="lancCartaoCredito">
				  								<fmt:message key="cartaoCredito" />
				  								<span class="required">*</span>
				  							</label>
				  							<div class="input-group">
				  								<c:if test="${isClosed and lancamento.cartaoCredito ne null}">
				  									<p class="form-control-static text-right"><c:out value="${lancamento.cartaoCredito.nome}"/></p>
				  									<input type="hidden" id="lancCartaoCredito" name="lancamento.cartaoCredito.id" value="${lancamento.cartaoCredito.id}" />
				  								</c:if>
				  								<c:if test="${!isClosed}">
					  								<select 
							    						id="lancCartaoCredito" 
							    						name="lancamento.cartaoCredito.id" 
							    						class="form-control"
							    						data-form="${pageContext.request.contextPath}/cartaoCredito/cadastro?dialogDetalhe=minus"
							    						data-search="${pageContext.request.contextPath}/cadastros/cartoes?tpl=dialog&dialogId=CartaoCreditoConsultar&?dialogDetalhe=minus&dialogTitle=cartoesCredito&status=ATI"
							    						data-load-options="${pageContext.request.contextPath}/cartaoCredito/carregaSelect"
							    						data-category="cartaoCredito">
							    						<c:if test="${cartaoCreditoList eq null or empty cartaoCreditoList}">
															<option value=""><fmt:message key="nenhumCartaoCreditoEncontrado"/></option>
														</c:if>
														<c:if test="${cartaoCreditoList ne null and !empty cartaoCreditoList}">
															<option value=""><fmt:message key="selecione___"/></option>
															<c:forEach var="cartaoCredito" items="${cartaoCreditoList}">
																<option 
																	data-dia-vencimento="${cartaoCredito.diaVencimento}"
																	data-dias-pagar="${cartaoCredito.diasPagar}"
																	data-limite="<fmt:formatNumber type="number" value="${cartaoCredito.limite}" maxFractionDigits="2" minFractionDigits="2" />"
																	data-limite-disponivel="<fmt:formatNumber type="number" value="${cartaoCredito.limiteDisponivel}" maxFractionDigits="2" minFractionDigits="2" />"
																	data-limite-utilizado="<fmt:formatNumber type="number" value="${cartaoCredito.limiteUtilizado}" maxFractionDigits="2" minFractionDigits="2" />"
																	value="${cartaoCredito.id}"<c:if test="${lancamento.cartaoCredito eq null and cartaoCredito.isPrincipal or lancamento.cartaoCredito eq cartaoCredito}"> selected="selected"</c:if>><c:out value="${cartaoCredito.nome}"/></option>
															</c:forEach>
														</c:if>
							    					</select>
							    					<span class="input-group-btn">
							    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.cartaoCredito" />">
						   									<i class="fa fa-plus"></i>
														</button>
						   								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.cartaoCredito" />">
						   									<i class="fa fa-pencil"></i>
						   								</button>
														<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.cartaoCredito" />">
															<i class="fa fa-search"></i>
						   								</button>
													</span>
												</c:if>
				  							</div>
			   							</div>
			   							<div class="col-xs-12 col-sm-3 col-sm-offset-3 form-group">
			  								<label class="control-label" for="lancCartaoCreditoDiferenca">
			  									<fmt:message key="diferenca"/>
			  								</label>
											<p id="lancCartaoCreditoDiferenca" class="form-control-static text-right font-bold<c:if test="${lancamento.diferenca eq 0}"> font-blue</c:if><c:if test="${lancamento.diferenca gt 0}"> font-red</c:if>">
												<c:if test="${lancamento.diferenca eq null}"><span class="text-danger"><strong>[&nbsp;<fmt:message key="informeoValor" />&nbsp;]</strong></span></c:if>
												<fmt:formatNumber type="number" value="${lancamento.diferenca}" maxFractionDigits="2" minFractionDigits="2" />
											</p>
			  							</div>
			   						</div>
			   								   						
			   						<div class="row" id="layLancCartaoCreditoLimite" style="display: none">
			  							<div class="col-xs-12 col-sm-4 form-group">
			  								<label class="control-label" for="lancCartaoCreditoLimite">
			  									<fmt:message key="limite"/>
			  								</label>
											<p id="lancCartaoCreditoLimite" class="form-control-static text-right"><fmt:formatNumber type="number" value="${lancamento.limite}" maxFractionDigits="2" minFractionDigits="2" /></p>
			  							</div>
			  							<div class="col-xs-12 col-sm-4 form-group">
			  								<label class="control-label" for="lancCartaoCreditoLimiteDisponivel">
			  									<fmt:message key="limiteDisp"/>
			  								</label>
			  								<p id="lancCartaoCreditoLimiteDisponivel" class="form-control-static text-right font-bold<c:if test="${lancamento.limiteDisponivel gt 0}"> font-blue</c:if><c:if test="${lancamento.limiteDisponivel eq 0}"> font-red</c:if>"><fmt:formatNumber type="number" value="${lancamento.limiteDisponivel}" maxFractionDigits="2" minFractionDigits="2" /></p>
										</div>
										<div class="col-xs-12 col-sm-4 form-group">
			  								<label class="control-label" for="lancCartaoCreditoLimiteUtilizado">
			  									<fmt:message key="limiteUtilizado"/>
			  								</label>
			  								<p id="lancCartaoCreditoLimiteUtilizado" class="form-control-static text-right font-bold<c:if test="${lancamento.limiteUtilizado eq 0}"> font-blue</c:if><c:if test="${lancamento.limiteUtilizado gt 0}"> font-red</c:if>"><fmt:formatNumber type="number" value="${lancamento.limiteUtilizado}" maxFractionDigits="2" minFractionDigits="2" /></p>
										</div>
			  						</div>
			   						
		   						</c:if>
		   						
		   					</div>
		   					
	   					</div>
	   					
	   					<c:if test="${lancamento.tipo eq 'DES'}">
		   					
		   					<!-- Historico Cartao de Credito -->
	  						<div class="panel panel-default" id="layLancCartaoCreditoMovs" style="display: none">
				
								<div class="panel-heading">
				    				<div class="row">
										<div class="col-xs-4 text-nowrap">
					   						<p class="form-control-static font-bold"><fmt:message key="movimentos"/></p>
					   					</div>
					   					<div class="col-xs-8 text-right">
					   						<div class="btn-group">
					   							<button type="button" id="lancBtSelecionarLancamentoFatura" class="btn btn-default" onclick="dialogSelecionarLancamentoFatura()" title="<fmt:message key="selecionar"/>">
					   								<i class="fa fa-search"></i>
					   							</button>
					   							<button type="button" id="lancBtRemoverLancamentoFatura" class="btn btn-default" disabled="disabled" onclick="removerLancamentoFatura()" title="<fmt:message key="remover"/>">
					   								<i class="fa fa-trash"></i>
					   							</button>
					   						</div>
					   					</div>
					   				</div>
				  				</div>
								
								<div class="data-table">
									<div class="result">
										<jsp:include page="lancamentoFaturaList.jsp"/>
									</div>
								</div>
								
							</div>
  						</c:if>
  						
  						<!-- Depesas / Receitas -->
  						<div class="panel panel-default">
			
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-6">
				   						<p class="form-control-static font-bold">
				   							<c:choose>
				   								<c:when test="${lancamento.tipo eq 'CPR' or lancamento.tipo eq 'VEN'}"><fmt:message key="produtos"/></c:when>
				   								<c:otherwise><fmt:message key="ItemTipo.${lancamento.tipo}s"/></c:otherwise>
				   							</c:choose>
				   						</p>
				   					</div>
				   					<div class="col-xs-6 text-right">
				   						<div class="btn-group">
				   							<button type="button" class="btn btn-default" onclick="dialogAdicionarLancamentoItem('${lancamento.tipo}')" title="<fmt:message key="title.pesquisar.${lancamento.tipo eq 'DES' ? 'despesa' : 'receita'}"/>">
				   								<i class="fa fa-search"></i>
				   							</button>
				   							<button type="button" id="lancBtEditarLancamentoItem" class="btn btn-default" disabled="disabled" onclick="editarLancamentoItem()" title="<fmt:message key="editar"/>">
				   								<i class="fa fa-pencil"></i>
				   							</button>
				   							<button type="button" id="lancBtExcluitLancamentoItem" class="btn btn-default" disabled="disabled" onclick="removerLancamentoItem()" title="<fmt:message key="remover"/>">
				   								<i class="fa fa-trash"></i>
				   							</button>
				   						</div>
				   					</div>
				   				</div>
			  				</div>
							
							<div class="data-table">
								<div class="result">
									<jsp:include page="lancamentoItemList.jsp"/>
								</div>
							</div>
							
						</div>
						
						
						<!-- Pagamentos / Recebimentos -->
  						<div class="panel panel-default">
			
							<div class="panel-heading">
			    				<div class="row">
									<div class="col-xs-4 text-nowrap">
				   						<p class="form-control-static font-bold">
				   							<c:if test="${lancamento.tipo eq 'DES' or lancamento.tipo eq 'CPR'}"><fmt:message key="pagamentos"/></c:if>
				   							<c:if test="${lancamento.tipo eq 'REC' or lancamento.tipo eq 'VEN'}"><fmt:message key="recebimentos"/></c:if>
			   							</p>
				   					</div>
				   					<div class="col-xs-8 text-right">
				   						<div class="btn-group">
				   							<button type="button" id="lancBtAdicionarLancamentoParcela" class="btn btn-default" onclick="dialogAdicionarLancamentoParcela()" title="<fmt:message key="adicionar"/>">
				   								<i class="fa fa-plus"></i>
				   							</button>
				   							<button type="button" id="lancBtEditarLancamentoParcela" class="btn btn-default" disabled="disabled" onclick="editarLancamentoParcela()" title="<fmt:message key="editar"/>">
				   								<i class="fa fa-pencil"></i>
			   								</button>
				   							<button type="button" id="lancBtExcluitLancamentoParcela" class="btn btn-default" disabled="disabled" onclick="removerLancamentoParcela()" title="<fmt:message key="remover"/>">
				   								<i class="fa fa-trash"></i>
				   							</button>
				   							<button type="button" id="lancBtDividirLancamentoParcela" class="btn btn-default" disabled="disabled" onclick="dividirLancamentoParcela()" title="<fmt:message key="dividir"/>">
				   								<i class="fa fa-ellipsis-v"></i>
				   							</button>
				   						</div>
				   					</div>
				   				</div>
			  				</div>
							
							<div class="data-table">
								<div class="result">
									<jsp:include page="lancamentoParcelaList.jsp"/>
								</div>
							</div>
							
						</div>
						
						
						<!-- Rateio -->
  						<div class="panel panel-default">
			
							<div class="panel-heading cm-toolbar">
			    				<div class="row">
									<div class="col-xs-4">
				   						<p class="form-control-static font-bold"><fmt:message key="rateio"/></p>
				   					</div>
				   					<div class="col-xs-8 text-right">
				   						<div class="btn-group">
				   							<button type="button" id="lancBtAdicionarLancamentoRateio" class="btn btn-default" onclick="dialogAdicionarLancamentoRateio()" title="<fmt:message key="adicionar"/>">
				   								<i class="fa fa-plus"></i>
				   							</button>
				   							<button type="button" id="lancBtEditarLancamentoRateio" class="btn btn-default" disabled="disabled" onclick="editarLancamentoRateio()" title="<fmt:message key="editar"/>">
			   									<i class="fa fa-pencil"></i>
				   							</button>
				   							<button type="button" id="lancBtExcluitLancamentoRateio" class="btn btn-default" disabled="disabled" onclick="removerLancamentoRateio()" title="<fmt:message key="remover"/>">
				   								<i class="fa fa-trash"></i>
				   							</button>
				   							<button type="button" id="lancBtDividirLancamentoRateio" class="btn btn-default" disabled="disabled" onclick="dividirLancamentoRateio()" title="<fmt:message key="dividir"/>">
				   								<i class="fa fa-ellipsis-v"></i>
				   							</button>
				   						</div>
				   					</div>
				   				</div>
			  				</div>
							
							<div class="data-table">
								<div class="result">
									<jsp:include page="lancamentoRateioList.jsp" />
	   							</div>
							</div>
							
						</div>
  						
  						<div class="form-group">
  							<label class="control-label" for="lancObs"><fmt:message key="obs" /></label>
  							<textarea id="lancObs" name="lancamento.obs" maxlength="400" data-category="obs" class="form-control" rows="4"><c:if test="${lancamento ne null and lancamento.obs ne null}"><c:out value="${lancamento.obs}"/></c:if></textarea>
  						</div>
  						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-save"></i>
		        		<b><fmt:message key="salvar"/></b>
	        		</button>
		        	<button type="button" id="lancBtBaixa" class="btn btn-primary">
		        		<i class="fa fa-arrow-${lancamento.tipo eq 'DES' or lancamento.tipo eq 'CPR' ? 'right font-red' : 'left font-blue'}"></i>
		        		<b><fmt:message key="btBaixa_${lancamento.tipo}"/></b>
	        		</button>
		       	</div>
		       	
		       	<c:if test="${lancamento ne null and lancamento.id ne null}">
			       	<div class="container-fluid">
						<div class="row cm-ca">
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${lancamento.criacao}" create="create" pessoa="${lancamento.usuCriacao}" /></p></div>
							<div class="col-sm-6"><p class="form-control-static text-center"><cmt:ca data="${lancamento.alteracao}" create="update" pessoa="${lancamento.usuAlteracao}" /></p></div>
						</div>
					</div>
				</c:if>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/lancamento.cadastro.js?v${applicationVersion}"></script>