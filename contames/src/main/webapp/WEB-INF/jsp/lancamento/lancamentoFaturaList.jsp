<fmt:message key="patternDate" var="patternDate" />
<div id="layLancamentoFaturaList">
	<c:if test="${lancamentoFaturaList eq null or empty lancamentoFaturaList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumMovimentoEncontrado"/>
			</strong>
		</div>
	</c:if>
	<c:if test="${lancamentoFaturaList ne null and !empty lancamentoFaturaList}">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="n_"/></th>
						<th class="cm-th"><fmt:message key="data"/></th>
						<th class="cm-th"><fmt:message key="descricao"/></th>
						<th class="cm-th"><fmt:message key="parcela"/></th>
						<th class="cm-th"><fmt:message key="valor"/></th>
					</tr>
				</thead>
				<tbody id="tbyLancamentoFaturaList">
					<c:forEach var="lancamentoFatura" items="${lancamentoFaturaList}">
						<c:if test="${lancamentoFatura.lancamentoParcela ne null}">
							<tr 
								data-id="${lancamentoFatura.id}" 
								data-index="${lancamentoFatura.seq}" 
								data-lancamento-parcela="${lancamentoFatura.lancamentoParcela.id}"
								onclick="selecionarLancamentoFatura(this)">
								<td class="text-right">${lancamentoFatura.seq + 1}</td>
								<td class="text-center"><joda:format pattern="${patternDate}" value="${lancamentoFatura.lancamentoParcela.data}" /></td>
								<td><c:out value="${lancamentoFatura.lancamentoParcela.lancamento.favorecido.displayNome}" /></td>
								<td class="text-center">${lancamentoFatura.lancamentoParcela.parcelaDisplay}</td>
								<td class="text-right"><fmt:formatNumber type="number" value="${lancamentoFatura.lancamentoParcela.baixaItem eq null ? lancamentoFatura.lancamentoParcela.valor : lancamentoFatura.lancamentoParcela.baixaItem.valor}" maxFractionDigits="2" minFractionDigits="2" /></td>
							</tr>
						</c:if>
						<c:if test="${lancamentoFatura.transferencia ne null}">
							<tr 
								data-id="${lancamentoFatura.id}" 
								data-index="${lancamentoFatura.seq}" 
								data-transferencia="${lancamentoFatura.transferencia.id}"
								onclick="selecionarLancamentoFatura(this)">
								<td class="text-right">${lancamentoFatura.seq + 1}</td>
								<td class="text-center"><joda:format pattern="${patternDate}" value="${lancamentoFatura.transferencia.data}" /></td>
								<td><fmt:message key="transf_" />&nbsp;<c:out value="${lancamentoFatura.transferencia.cartaoCredito.nome}"/>&nbsp;<fmt:message key="p_" />&nbsp;<c:out value="${lancamentoFatura.transferencia.destino.nome}"/></td>
								<td class="text-center">-</td>
								<td class="text-right"><fmt:formatNumber type="number" value="${lancamentoFatura.transferencia.valor}" maxFractionDigits="2" minFractionDigits="2" /></td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr class="cm-tr-total">
						<td class="text-right" colspan="4"><fmt:message key="total"/> :</td>
						<td class="text-right" id="lbTotalLancamentoFatura"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoFatura}" maxFractionDigits="2" minFractionDigits="2" /></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:if>
</div>