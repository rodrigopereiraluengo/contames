<fmt:message key="patternDate" var="patternDate" />

<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumLancamento.${tipo}.encontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">

	<div class="table-responsive">
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="${tipo eq 'DES' ? 'pagarA' : 'receberDe'}" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="2" column="documento" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="3" name="${tipo eq 'DES' ? 'pagamentos' : 'recebimentos'}" column="quantLancamentoParcela" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="4" name="1_vencimento" column="primeiroVencimento" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="status" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="6" column="valor" page="${page}" path="${path}" />
					<th class="cm-th-empty">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="tbyLancamento">
				<c:forEach var="lancamento" items="${lancamentoList}">
					<tr 
						data-id="${lancamento['id']}"
						data-category="lancamento_${lancamento['id']}">
						<td class="text-center<c:if test="${f:orderByContains('_data', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${lancamento['_data']}" /></td>
						<td<c:if test="${f:orderByContains('favorecido', orderByList)}"> class="font-bold"</c:if>>${lancamento['favorecido']}</td>
						<td<c:if test="${f:orderByContains('documento', orderByList)}"> class="font-bold"</c:if>>
							${lancamento['documento']}
							<c:if test="${lancamento['arquivo_id'] ne null}">
								<a title="${lancamento['arquivo_nome']}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${lancamento['arquivo_id']}/download/${lancamento['arquivo_nome']}">
									<i class="fa fa-paperclip"></i>
								</a>
							</c:if>
						</td>
						<td class="text-right<c:if test="${f:orderByContains('quantLancamentoParcela', orderByList)}"> font-bold</c:if>">${lancamento['quantlancamentoparcela']}</td>
						<td class="text-center<c:if test="${f:orderByContains('primeiroVencimento', orderByList)}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${lancamento['primeirovencimento']}" /></td>
						<td class="text-center<c:if test="${lancamento['_status'] eq 'CAN'}"> font-red</c:if><c:if test="${f:orderByContains('status', orderByList)}"> font-bold</c:if>">${lancamento['status']}</td>
						<td class="text-right<c:if test="${f:orderByContains('valor', orderByList)}"> font-bold</c:if>"><fmt:formatNumber type="number" value="${lancamento['valor']}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-center">
							<c:if test="${lancamento['obs'] ne null}">
								<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(lancamento['obs']))}', textAlign: 'text-left'})" role="button" title="<fmt:message key="obs"/>">
									<i class="fa fa-comment"></i>
								</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
 			
	</div>
			
	<div class="panel-footer">
  		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>

</c:if>