<fmt:message key="patternDate" var="patternDate" />
<div id="layLancamentoItemList">
	<c:if test="${lancamentoItemList eq null or empty lancamentoItemList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<c:choose>
					<c:when test="${lancamento.tipo eq 'DES'}">
						<fmt:message key="nenhumaDespesaEncontrada"/>
					</c:when>
					<c:when test="${lancamento.tipo eq 'REC'}">
						<fmt:message key="nenhumaReceitaEncontrada"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="nenhumProdutoEncontrado"/>
					</c:otherwise>
				</c:choose>
			</strong>
		</div>
	</c:if>
	<c:if test="${lancamentoItemList ne null and !empty lancamentoItemList}">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="n_"/></th>
						<th class="cm-th"><fmt:message key="descricao"/></th>
						<th class="cm-th"><fmt:message key="obs"/></th>
						<th class="cm-th"><fmt:message key="qtde"/></th>
						<th class="cm-th"><fmt:message key="vlrUnit"/></th>
						<th class="cm-th"><fmt:message key="valor"/></th>
						<th class="cm-th"><fmt:message key="desconto"/></th>
						<th class="cm-th"><fmt:message key="total"/></th>
					</tr>
				</thead>
				<tbody id="tbyLancamentoItemList">
					<c:forEach var="lancamentoItem" items="${lancamentoItemList}" varStatus="st">
						<tr data-id="${lancamentoItem.id}" data-index="${lancamentoItem.seq}" data-item-id="${lancamentoItem.item.id}" onclick="selecionaLancamentoItem(this)" ondblclick="editarLancamentoItem(this)">
							<td class="text-right">${lancamentoItem.seq + 1}</td>
							<td>${lancamentoItem.item.displayName}</td>
							<td class="text-right"><c:out value="${lancamentoItem.obs}"/></td>
							<td class="text-right"><c:if test="${lancamentoItem.quant ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.quant}" maxFractionDigits="3" minFractionDigits="3"/></c:if></td>
							<td class="text-right"><c:if test="${lancamentoItem.valorUnit ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.valorUnit}" maxFractionDigits="2" minFractionDigits="2" /></c:if></td>
							<td class="text-right"><c:if test="${lancamentoItem.valor ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.valor}" maxFractionDigits="2" minFractionDigits="2" /></c:if></td>
							<td class="text-right"><c:if test="${lancamentoItem.desconto ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.desconto}" maxFractionDigits="2" minFractionDigits="2" /></c:if></td>
							<td class="text-right"><c:if test="${lancamentoItem.total ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.total}" maxFractionDigits="2" minFractionDigits="2" /></c:if></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr class="cm-tr-total">
						<td colspan="3" class="text-right"><fmt:message key="total"/> :</td>
						<td class="text-right"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoItemQuant}" maxFractionDigits="3" minFractionDigits="3" /></td>
						<td class="text-right"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoItemValorUnit}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoItemValor}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right">
							<c:choose>
								<c:when test="${lancamento.totalLancamentoItemDesconto eq 0}">&nbsp;</c:when>
								<c:otherwise>
									<fmt:formatNumber type="number" value="${lancamento.totalLancamentoItemDesconto}" maxFractionDigits="2" minFractionDigits="2" />		
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text-right" id="lbTotalLancamentoItem"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoItem}" maxFractionDigits="2" minFractionDigits="2" /></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:if>
</div>
<c:if test="${lancamentoItemList ne null and !empty lancamentoItemList and fn:length(lancamentoItemList) eq 1 and lancamentoItemList[0].item.favorecido ne null}">
	<script type="text/javascript">
		$(function() {
			if(!$('#lancFavorecido').data('val')) {
				$('#lancFavorecido')
					.data('val', '${lancamentoItemList[0].item.favorecido.id}')
					.val('${lancamentoItemList[0].item.favorecido.displayNome}')
					.prev().val('${lancamentoItemList[0].item.favorecido.id}');
			}
		});
	</script>
</c:if>