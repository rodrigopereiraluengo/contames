<c:choose>
	<c:when test="${tipo eq 'DES'}"><c:set var="path" value="lancamentos/despesas" /></c:when>
	<c:when test="${tipo eq 'REC'}"><c:set var="path" value="lancamentos/receitas" /></c:when>
	<c:when test="${tipo eq 'CPR'}"><c:set var="path" value="lancamentos/compras" /></c:when>
	<c:when test="${tipo eq 'VEN'}"><c:set var="path" value="lancamentos/vendas" /></c:when>
</c:choose>

<tiles:insertTemplate template="/WEB-INF/jsp/tpl/admin.jsp">

	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title">
							<c:choose>
								<c:when test="${tipo eq 'CPR'}"><fmt:message key="compras"/></c:when>
								<c:when test="${tipo eq 'VEN'}"><fmt:message key="vendas"/></c:when>
								<c:otherwise><fmt:message key="lancamentoDe.${tipo}"/></c:otherwise>
							</c:choose>
						</h1>
						<p class="description"><fmt:message key="lancamentoDe.${tipo}.description"/></p>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="resumo" />
							</li>
				  			<li><fmt:message key="lancamentos" /></li>
				  			<li><fmt:message key="ItemTipo.${tipo}s" /></li>
						</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<form 
			id="frmLancamentoConsultar" 
			action="${pageContext.request.contextPath}/${path}"
			data-form="${pageContext.request.contextPath}/lancamento/cadastro?tipo=${tipo}"
			data-delete="${pageContext.request.contextPath}/lancamento/excluir"
			class="data-table"
			data-tipo="${tipo}">
			
			<input type="hidden" name="status" value="${param.status}" />
			
			<div class="panel panel-default">
			
				<div class="panel-heading cm-panel-heading-toolbar">
				
					<div class="container-fluid">
							
						<div class="row">
						
							<div class="col-xs-12 col-sm-4 form-group">
					  			<div class="btn-group">
						  			<button type="button" class="btn btn-default bt-new">
						  				<i class="fa fa-plus"></i>
						  				<c:choose>
											<c:when test="${tipo eq 'CPR' or tipo eq 'VEN'}"><fmt:message key="nova" /></c:when>
											<c:otherwise><fmt:message key="novo" /></c:otherwise>
										</c:choose>
						  			</button>
						  			<button type="button" class="btn btn-default bt-edit" disabled="disabled">
						  				<i class="fa fa-pencil"></i>
						  				<fmt:message key="editar" />
					  				</button>
						  			<button type="button" class="btn btn-default bt-delete" disabled="disabled">
						  				<i class="fa fa-trash"></i>
						  				<fmt:message key="excluir" />
					  				</button>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
								<div class="input-group">
									<input type="text" name="search" class="form-control" placeholder="<fmt:message key="pesquisar___"/>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<i class="fa fa-search"></i>
										</button>
									</span>
				    			</div>		
							</div>
						
						</div>
						
					</div>
				
				</div>
			
				<div class="result search-table">
					<jsp:include page="/WEB-INF/jsp/lancamento/consultar_table.jsp" />
		   		</div>
		   		
	   		</div>
		
		</form>
	
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/lancamento.consultar.js?v${applicationVersion}"></script>
	
	</tiles:putAttribute>
	
</tiles:insertTemplate>