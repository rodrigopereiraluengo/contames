<div id="layLancamentoRateioList">
	<c:if test="${lancamentoRateioList eq null or empty lancamentoRateioList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<fmt:message key="nenhumRateioEncontrado"/>
			</strong>
		</div>
	</c:if>
	<c:if test="${lancamentoRateioList ne null and !empty lancamentoRateioList}">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="n_"/></th>
						<th class="cm-th"><fmt:message key="nome"/></th>
						<th class="cm-th">%</th>
						<th class="cm-th"><fmt:message key="valor"/></th>
					</tr>
				</thead>
				<tbody id="tbyLancamentoRateioList">
					<c:forEach var="lancamentoRateio" items="${lancamentoRateioList}">
						<tr data-id="${lancamentoRateio.id}" data-index="${lancamentoRateio.seq}" onclick="selecionaLancamentoRateio(this)" ondblclick="editarLancamentoRateio(this)">
							<td class="text-right">${lancamentoRateio.seq + 1}</td>
							<td><c:out value="${lancamentoRateio.displayNome}"/></td>
							<td class="text-right"><fmt:formatNumber type="number" value="${lancamentoRateio.perc}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td class="text-right"><fmt:formatNumber type="number" value="${lancamentoRateio.valor}" maxFractionDigits="2" minFractionDigits="2" /></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr class="cm-tr-total">
						<td colspan="2" class="text-right"><fmt:message key="total"/> :</td>
						<td class="text-right"><fmt:formatNumber type="number" value="${lancamento.percLancamentoRateio}" maxFractionDigits="2" minFractionDigits="2" /></td>
						<td class="text-right"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoRateio}" maxFractionDigits="2" minFractionDigits="2" /></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:if>
</div>