<fmt:message key="patternDate" var="patternDate"/>
<c:if test="${lancamentoParcela.data ne null}">
	<joda:format pattern="${patternDate}" value="${lancamentoParcela.data}" var="data" />
</c:if>
<c:set var="isReadOnly" value="${lancamentoParcela.status eq 'BXD' or lancamentoParcela.status eq 'COM'}"  />

<div id="dialogLancamentoParcela" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmLancamentoParcela" data-is-read-only="${isReadOnly}" data-tipo="${lancamentoParcela.tipo}">
				
				<input type="hidden" id="lancamentoParcelaId" name="lancamentoParcela.id" value="${lancamentoParcela.id}"/>
				<input type="hidden" id="lancamentoParcelaTipo" name="lancamentoParcela.tipo" value="${lancamentoParcela.tipo}"/>
				
				<input type="hidden" id="lancamentoParcelaSeq" name="lancamentoParcela.seq" value="${lancamentoParcela.seq}"/>
				<input type="hidden" id="lancamentoParcelaBoletoTipo" name="lancamentoParcela.boletoTipo" value="${lancamentoParcela.boletoTipo}"/>
				<input type="hidden" id="lancamentoParcelaBoletoNumero" name="lancamentoParcela.boletoNumero" value="${lancamentoParcela.boletoNumero}"/>
				<input type="hidden" id="lancamentoParcelaCartaoCredito" name="lancamentoParcela.cartaoCredito.id"<c:if test="${lancamentoParcela.cartaoCredito ne null}"> value="${lancamentoParcela.cartaoCredito.id}"</c:if>/>
				<input type="hidden" id="lancamentoParcelaCartaoCreditoIsLimite" name="lancamentoParcela.isLimite"<c:if test="${lancamentoParcela ne null}"> value="${lancamentoParcela.isLimite}"</c:if>/>
				<input type="hidden" id="lancamentoParcelaConta" name="lancamentoParcela.conta.id"<c:if test="${lancamentoParcela.conta ne null}"> value="${lancamentoParcela.conta.id}"</c:if>/>
				
				<input type="hidden" name="lancamentoParcela.cheque.id"<c:if test="${lancamentoParcela.cheque ne null}"> value="${lancamentoParcela.cheque.id}"</c:if> />
				<input type="hidden" name="lancamentoParcela.cheque.talaoCheque.id"<c:if test="${lancamentoParcela.cheque ne null and lancamentoParcela.cheque.talaoCheque ne null}"> value="${lancamentoParcela.cheque.talaoCheque.id}"</c:if> />
				<input type="hidden" name="lancamentoParcela.cheque.banco.id"<c:if test="${lancamentoParcela.cheque ne null and lancamentoParcela.cheque.banco ne null}"> value="${lancamentoParcela.cheque.banco.id}"</c:if> />
				<input type="hidden" name="lancamentoParcela.cheque.numero"<c:if test="${lancamentoParcela.cheque ne null}"> value="${lancamentoParcela.cheque.numero}"</c:if> />
				<input type="hidden" name="lancamentoParcela.cheque.nome.id"<c:if test="${lancamentoParcela.cheque ne null and lancamentoParcela.cheque.nome ne null}"> value="<c:out value="${lancamentoParcela.cheque.nome.id}"/>"</c:if> />
				<input type="hidden" name="lancamentoParcela.cheque.data"<c:if test="${lancamentoParcela.cheque ne null}"> value="<joda:format pattern="${patternDate}" value="${lancamentoParcela.cheque.data}" />"</c:if> />
				<input type="hidden" name="lancamentoParcela.cheque.obs"<c:if test="${lancamentoParcela.cheque ne null}"> value="<c:out value="${lancamentoParcela.cheque.obs}"/>"</c:if> />
				
				<input type="hidden" id="lancamentoParcelaArquivoId" name="lancamentoParcela.arquivo.id"<c:if test="${lancamentoParcela ne null and lancamentoParcela.arquivo ne null and lancamentoParcela.arquivo.id ne null}"> value="${lancamentoParcela.arquivo.id}"</c:if> />
				<input type="hidden" id="lancamentoParcelaArquivoTemp" name="lancamentoParcela.arquivo.temp"<c:if test="${lancamentoParcela ne null}"> value="<c:out value="${viewid}_lancamentoParcela_${lancamentoParcela.seq}"/>"</c:if> />
				<input type="hidden" id="lancamentoParcelaArquivoNome" name="lancamentoParcela.arquivo.nome"<c:if test="${lancamentoParcela ne null and lancamentoParcela.arquivo ne null and lancamentoParcela.arquivo.nome ne null}"> value="<c:out value="${lancamentoParcela.arquivo.nome}"/>"</c:if> />
				<input type="hidden" id="lancamentoParcelaArquivoContentType" name="lancamentoParcela.arquivo.contentType"<c:if test="${lancamentoParcela ne null and lancamentoParcela.arquivo ne null and lancamentoParcela.arquivo.contentType ne null}"> value="<c:out value="${lancamentoParcela.arquivo.contentType}"/>"</c:if> />
				<input type="hidden" id="lancamentoParcelaArquivoSize" name="lancamentoParcela.arquivo.size"<c:if test="${lancamentoParcela ne null and lancamentoParcela.arquivo ne null and lancamentoParcela.arquivo.size ne null}"> value="${lancamentoParcela.arquivo.size}"</c:if> />
				
				<input type="hidden" name="lancamentoParcela.viewid" value="${lancamentoParcela.viewid}" />
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${lancamentoParcela.viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${lancamentoParcela.viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogLancamentoParcelaTitle">
						<c:if test="${lancamentoParcela.tipo eq 'DES' or lancamentoParcela.tipo eq 'CPR'}"><fmt:message key="pagamento"/></c:if>
						<c:if test="${lancamentoParcela.tipo eq 'REC' or lancamentoParcela.tipo eq 'VEN'}"><fmt:message key="recebimento"/></c:if>
					</h4>
				</div>
				
				<div class="modal-body">
					
					<div class="container-fluid">
					
						<div class="row">
  					
	  						<div class="col-xs-12 col-sm-4 form-group">
	  							<label class="control-label" for="lancamentoParcelaData">
	  								<fmt:message key="data" />
	  								<span class="required">*</span>
  								</label>
  								<div class="input-group">
	  								<input type="text" id="lancamentoParcelaData" name="lancamentoParcela.data"<c:if test="${lancamentoParcela.data ne null}"> value="${data}"</c:if> data-category="data" class="form-control" />
	  								<span class="input-group-btn">
		  								<button class="btn btn-default" type="button" id="btLancamentoParcelaData">
	       									<i class="fa fa-calendar-o"></i>
										</button>
									</span>
  								</div>
	  						</div>
	  						
	  						<div class="col-xs-12 col-sm-8 form-group">
	  							<label class="control-label" for="lancamentoParcelaMeio">
	  								<fmt:message key="meio" />
	  								<span class="required">*</span>
  								</label>
	  							<div class="input-group">
	  								<c:if test="${isReadOnly}">
	  									<input type="hidden" id="lancamentoParcelaMeio" value="${lancamentoParcela.meio}" />
	  									<p class="form-control-static"><fmt:message key="MeioPagamento.${lancamentoParcela.meio}" /></p>
	  									<span class="input-group-btn">
			  								<button class="btn btn-default" type="button" id="btLancamentoParcelaConta" title="<fmt:message key="conta"/>">
		       									<i class="fa fa-folder-open"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaCartaoCredito" title="<fmt:message key="cartaoCredito"/>">
		       									<i class="fa fa-credit-card"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaCheque" title="<fmt:message key="cheque"/>">
		       									<i class="fa fa-check-circle"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaBoleto" title="<fmt:message key="codigoBarras"/>">
												<i class="fa fa-barcode"></i>
	        								</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaArquivo" title="<fmt:message key="arquivo"/>">
	        									<i class="fa fa-paperclip"></i>
											</button>
										</span>
	  								</c:if>
	  								<c:if test="${!isReadOnly}">
		  								<select id="lancamentoParcelaMeio" name="lancamentoParcela.meio" data-category="meio" class="form-control">
			  								<option value=""><fmt:message key="selecione___" /></option>
			  								<option value="CDB"<c:if test="${lancamentoParcela.meio eq 'CDB'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CDB" /></option>
			  								<option value="CCR"<c:if test="${lancamentoParcela.meio eq 'CCR'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CCR" /></option>
			  								<option value="INT"<c:if test="${lancamentoParcela.meio eq 'INT'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.INT" /></option>
			  								<option value="DEA"<c:if test="${lancamentoParcela.meio eq 'DEA'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DEA" /></option>
			  								<option value="DIN"<c:if test="${lancamentoParcela.meio eq 'DIN'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DIN" /></option>
			  								<option value="CHQ"<c:if test="${lancamentoParcela.meio eq 'CHQ'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.CHQ" /></option>
			  								<option value="TRA"<c:if test="${lancamentoParcela.meio eq 'TRA'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.TRA" /></option>
			  								<option value="DOC"<c:if test="${lancamentoParcela.meio eq 'DOC'}"> selected="selected"</c:if>><fmt:message key="MeioPagamento.DOC" /></option>
			  							</select>
			  							<span class="input-group-btn">
			  								<button class="btn btn-default" type="button" id="btLancamentoParcelaConta" title="<fmt:message key="conta"/>">
		       									<i class="fa fa-folder-open"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaCartaoCredito" title="<fmt:message key="cartaoCredito"/>">
		       									<i class="fa fa-credit-card"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaCheque" title="<fmt:message key="cheque"/>">
		       									<i class="fa fa-check-circle"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaBoleto" title="<fmt:message key="codigoBarras"/>">
	        									<i class="fa fa-barcode"></i>
											</button>
											<button class="btn btn-default" type="button" id="btLancamentoParcelaArquivo" title="<fmt:message key="arquivo"/>">
	        									<i class="fa fa-paperclip"></i>
											</button>
										</span>
									</c:if>
		  						</div>
	  						</div>
	  						
	  					</div>
	  					
	  					<div class="form-group">
							<label class="control-label" for="lancamentoParcelaObs" maxlength="400" data-category="obs"><fmt:message key="obs"/></label>
							<textarea id="lancamentoParcelaObs" name="lancamentoParcela.obs" class="form-control" rows="4"><c:if test="${lancamentoParcela.obs ne null}"><c:out value="${lancamentoParcela.obs}"/></c:if></textarea>
						</div>
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-6 col-sm-offset-6 form-group">
								<label class="control-label" for="lancamentoParcelaValor">
									<fmt:message key="valor"/>
									<span class="required">*</span>
								</label>
								<c:if test="${isReadOnly}">
									<p class="form-control-static text-right"><fmt:formatNumber type="number" value="${lancamentoParcela.valor}" maxFractionDigits="2" minFractionDigits="2" /></p>
								</c:if>
								<c:if test="${!isReadOnly}">
									<input type="text" id="lancamentoParcelaValor" name="lancamentoParcela.valor"<c:if test="${lancamentoParcela.valor ne null}"> value="${lancamentoParcela.valor}"</c:if> data-category="valor" class="form-control" />
								</c:if>
							</div>
													
						</div>
						
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/lancamento.lancamentoParcela.js?v${applicationVersion}"></script>