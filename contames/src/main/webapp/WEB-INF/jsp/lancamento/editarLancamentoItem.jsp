<div id="dialogEditarLancamentoItem" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmEditarLancamentoItem" data-tipo="${param.tipo}">
			
				<input type="hidden" name="lancamentoItem.seq" value="${lancamentoItem.seq}"/>
				<input type="hidden" name="lancamentoItem.viewid" value="${lancamentoItem.viewid}"/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${lancamentoItem.viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${lancamentoItem.viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogEditarLancamentoItemTitle"><fmt:message key="ItemTipo.${param.tipo}"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid">
					
						<div class="row">
  					
	  						<div class="col-xs-12 form-group">
	  							<label class="control-label" for="lancamentoItemDescricao"><fmt:message key="descricao" /></label>
	  							<p class="form-control-static" id="lancamentoItemDescricao">${lancamentoItem.item.displayName}</p>
	  						</div>
	  						
	  					</div>
	  					
	  					<div class="form-group">
							<label class="control-label" for="lancamentoItemObs"><fmt:message key="obs"/></label>
							<textarea id="lancamentoItemObs" name="lancamentoItem.obs" class="form-control" maxlength="100" rows="4" data-category="obs"><c:if test="${lancamentoItem.obs ne null}"><c:out value="${lancamentoItem.obs}"/></c:if></textarea>
						</div>
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="lancamentoItemQuant">
									<fmt:message key="qtde"/>
									<span class="required">*</span>
								</label>
								<input type="text" id="lancamentoItemQuant" name="lancamentoItem.quant"<c:if test="${lancamentoItem.quant ne null}"> value="${lancamentoItem.quant}"</c:if> data-category="quant" class="form-control" />
							</div>
							
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="lancamentoItemValorUnit">
									<fmt:message key="vlrUnit"/>
									<span class="required">*</span>
								</label>
								<input type="text" id="lancamentoItemValorUnit" name="lancamentoItem.valorUnit"<c:if test="${lancamentoItem.valorUnit ne null}"> value="${lancamentoItem.valorUnit}"</c:if> data-category="valorUnit" class="form-control" />
							</div>
							
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="lancamentoItemValor"><fmt:message key="valor"/></label>
								<p class="form-control-static text-right" id="lancamentoItemValor"><c:if test="${lancamentoItem.valor ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.valor}" maxFractionDigits="2" minFractionDigits="2" /></c:if></p>
							</div>
						
						</div>
						
						<div class="row">
						
							<div class="col-xs-12 col-sm-4 form-group">
								<label class="control-label" for="lancamentoItemDesconto"><fmt:message key="desconto"/></label>
								<input type="text" id="lancamentoItemDesconto" name="lancamentoItem.desconto"<c:if test="${lancamentoItem.desconto ne null}"> value="<fmt:formatNumber type="number" value="${lancamentoItem.desconto}" maxFractionDigits="2" minFractionDigits="2" />"</c:if> data-category="desconto" class="form-control" />
							</div>
							
							<div class="col-xs-12 col-sm-4 col-sm-offset-4 form-group">
								<label class="control-label" for="lancamentoItemTotal"><fmt:message key="total"/></label>
								<p class="form-control-static text-right" id="lancamentoItemTotal"><c:if test="${lancamentoItem.total ne null}"><fmt:formatNumber type="number" value="${lancamentoItem.total}" maxFractionDigits="2" minFractionDigits="2" /></c:if></p>
							</div>
						
						</div>
  					
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
		
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/lancamento.editarLancamentoItem.js?v${applicationVersion}"></script>