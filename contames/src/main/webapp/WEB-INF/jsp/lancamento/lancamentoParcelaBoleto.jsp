<div id="dialogLancamentoParcelaBoleto" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmLancamentoParcelaBoleto">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<h4 class="modal-title" id="dialogLancamentoParcelaBoletoTitle"><fmt:message key="codigoBarras"/></h4>
				</div>
					
				<div class="modal-body">
				
					<div class="container-fluid">
					
						<div class="row">
	 					
	  						<div class="col-xs-12 col-sm-4 form-group">
	  							<label class="control-label" for="lancamentoParcelaBoletoTipo">
	  								<fmt:message key="tipo" />
	  								<span class="required">*</span>
								</label>
								<select id="lancamentoParcelaBoletoTipo" name="lancamentoParcela.boletoTipo" data-category="boletoTipo" class="form-control">
									<option value="COB"<c:if test="${boletoTipo eq 'COB'}"> selected="selected"</c:if>><fmt:message key="BoletoTipo.COB" /></option>
									<option value="CON"<c:if test="${boletoTipo eq 'CON'}"> selected="selected"</c:if>><fmt:message key="BoletoTipo.CON" /></option>
								</select>
	  						</div>
	  						
	  					</div>
	  					
	  					<div class="row">
	  						
	  						<div class="col-xs-12 form-group">
	  							<input type="text" id="lancamentoParcelaBoletoNumero" name="lancamentoParcela.boletoNumero"<c:if test="${boletoNumero ne null}"> value="<c:out value="${boletoNumero}"/>"</c:if> maxlength="54" data-category="boletoNumero" class="form-control text-center" />
	  						</div>
	  						
	  					</div>
	  					
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
	       	
	       	</form>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/lancamento.lancamentoParcelaBoleto.js?v${applicationVersion}"></script>