<div id="dialogLancamentoRateio" class="modal" style="display: none">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<form id="frmLancamentoRateio" data-tipo="${lancamento.tipo}">
			
				<input type="hidden" name="viewid" value="${viewid}"/>
				<input type="hidden" name="lancamentoRateio.id" value="${lancamentoRateio.id}" />
				<input type="hidden" name="lancamentoRateio.seq" value="${lancamentoRateio.seq}" />
				<input type="hidden" name="lancamentoRateio.viewid" value="${lancamentoRateio.viewid}" />
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times"></i>
					</button>
					<button onclick="dialogArquivoConsultar('${lancamentoRateio.viewid}')" type="button" title="<fmt:message key="arquivos" />" class="btn btn-default bt-dialogArquivo" data-viewid="${lancamentoRateio.viewid}">
						<span class="glyphicon glyphicon-file"></span>
						<span class="quant"></span>
					</button>
					<h4 class="modal-title" id="dialogLancamentoRateioTitle"><fmt:message key="rateio"/></h4>
				</div>
				
				<div class="modal-body">
				
					<div class="container-fluid">
					
						<div class="row">
							
							<div class="col-xs-12 col-sm-4 form-group">
								<select id="lancamentoRateioTipo" name="lancamentoRateio.tipo" data-category="tipo" class="form-control">
	  								<option value="RAT"<c:if test="${lancamentoRateio.tipo eq 'RAT'}"> selected="selected"</c:if>><fmt:message key="rateio" /></option>
	  								<option value="PES"<c:if test="${lancamentoRateio.tipo eq 'PES'}"> selected="selected"</c:if>><fmt:message key="pessoa" /></option>
	  							</select>
							</div>
							
							
							<!-- Pessoa -->
							<div id="layLancamentoRateioPessoa" style="display: none" class="col-xs-12 col-sm-8 form-group">
								<div class="input-group">
									<input 
	  									type="text" 
	  									id="lancamentoRateioPessoa" 
	  									name="lancamentoRateio.pessoa.id" 
	  									class="form-control"
	  									data-form="${pageContext.request.contextPath}/pessoa/cadastro?dialogDetalhe=minus"
			    						data-search="${pageContext.request.contextPath}/cadastros/pessoas?tpl=dialog&dialogTitle=pessoa&dialogId=PessoaConsultar&dialogDetalhe=minus&status=ATI"
			    						data-url-auto-complete="${pageContext.request.contextPath}/pessoa/autoComplete"
			    						data-reload="${pageContext.request.contextPath}/pessoa/reload"
			    						<c:if test="${lancamentoRateio.pessoa ne null}"> value="${lancamentoRateio.pessoa.displayNome}" data-val="${lancamentoRateio.pessoa.id}"</c:if>
			    						data-category="pessoa"
		    						/>
			  						
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.pessoa"/>">
	       									<i class="fa fa-plus"></i>
										</button>
	       								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.pessoa"/>">
	       									<i class="fa fa-pencil"></i>
										</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.pessoa"/>">
											<i class="fa fa-search"></i>
	       								</button>
	   								</span>
									
								</div>
							</div>
							
							<!-- Rateio -->
							<div id="layLancamentoRateioRateio" style="display: none" class="col-xs-12 col-sm-8 form-group">
								<div class="input-group">
									
									<select 
			    						id="lancamentoRateioRateio" 
			    						name="lancamentoRateio.rateio.id" 
			    						class="form-control"
			    						data-form="${pageContext.request.contextPath}/descricao/cadastro?descricaoTipo=RATEIO"
			    						data-search="${pageContext.request.contextPath}/descricao/consultar?descricaoTipo=RATEIO"
			    						data-load-options="${pageContext.request.contextPath}/descricao/carregaSelect?descricaoTipo=RATEIO"
			    						data-category="rateio">
			    						<c:if test="${rateioList eq null or empty rateioList}">
			    							<option value=""><fmt:message key="nenhumRegistroEncontrado"/></option>
			    						</c:if>
			    						<c:if test="${rateioList ne null and !empty rateioList}">
			    							<option value=""><fmt:message key="selecione___"/></option>
			    							<c:forEach var="rateio" items="${rateioList}">
			    								<option value="${rateio.id}"<c:if test="${lancamentoRateio ne null and lancamentoRateio.rateio eq rateio}"> selected="selected"</c:if>><c:out value="${rateio.nome}"/></option>
			    							</c:forEach>
			    						</c:if>
			    					</select>
			    					<span class="input-group-btn">
			    						<button class="btn btn-default bt-new" type="button" title="<fmt:message key="title.adicione.rateio" />">
        									<i class="fa fa-plus"></i>
										</button>
        								<button class="btn btn-default bt-edit" type="button" title="<fmt:message key="title.editar.rateio" />">
        									<i class="fa fa-pencil"></i>
        								</button>
										<button class="btn btn-default bt-search" type="button" title="<fmt:message key="title.pesquisar.rateio" />">
											<i class="fa fa-search"></i>
        								</button>
      								</span>
									
								</div>
							</div>
							
						</div>
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="lancamentoRateioPerc">
									%
									<span class="required">*</span>
								</label>
								<input type="text" id="lancamentoRateioPerc" name="lancamentoRateio.perc"<c:if test="${lancamentoRateio ne null and lancamentoRateio.perc ne null}"> value="${lancamentoRateio.perc}"</c:if> data-category="perc" class="form-control" />
							</div>
							
							<div class="col-xs-12 col-sm-6 form-group">
								<label class="control-label" for="lancamentoRateioValor">
									<fmt:message key="valor"/>
									<span class="required">*</span>
								</label>
								<input type="text" id="lancamentoRateioValor" name="lancamentoRateio.valor"<c:if test="${lancamentoRateio ne null and lancamentoRateio.valor ne null}"> value="${lancamentoRateio.valor}"</c:if> data-category="valor" class="form-control" />
							</div>
							
						</div>
					
					</div>
				
				</div>
				
				<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<i class="fa fa-close"></i>
		        		<b><fmt:message key="cancelar"/></b>
	        		</button>
		        	<button type="submit" class="btn btn-primary">
		        		<i class="fa fa-check"></i>
		        		<b><fmt:message key="ok"/></b>
	        		</button>
		       	</div>
			
			</form>
			
		</div>
	</div>
</div>
<script type="text/javascript">var totalLancamentoParcela = ${lancamento.totalLancamentoParcela}</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/lancamento.lancamentoRateio.js?v${applicationVersion}"></script>