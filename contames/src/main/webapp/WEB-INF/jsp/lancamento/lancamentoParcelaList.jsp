<fmt:message key="patternDate" var="patternDate" />
<div id="layLancamentoParcelaList">
	<c:if test="${lancamentoParcelaList eq null or empty lancamentoParcelaList}">
		<div class="alert alert-success text-center cm-table-notfound" role="alert">
			<strong>
				<i class="fa fa-info-circle"></i>
				<c:choose>
					<c:when test="${lancamento.tipo eq 'DES' or lancamento.tipo eq 'CPR'}">
				 		<fmt:message key="nenhumPagamentoEncontrado"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="nenhumRecebimentoEncontrado"/>
					</c:otherwise>
				</c:choose>
			</strong>
		</div>
	</c:if>
	<c:if test="${lancamentoParcelaList ne null and !empty lancamentoParcelaList}">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="cm-th"><fmt:message key="n_"/></th>
						<th class="cm-th"><fmt:message key="data"/></th>
						<th class="cm-th"><fmt:message key="meio"/></th>
						<th class="cm-th"><fmt:message key="obs"/></th>
						<th class="cm-th"><fmt:message key="valor"/></th>
					</tr>
				</thead>
				<tbody id="tbyLancamentoParcelaList">
					<c:forEach var="lancamentoParcela" items="${lancamentoParcelaList}">
						<tr 
							data-id="${lancamentoParcela.id}" 
							data-index="${lancamentoParcela.seq}" 
							onclick="selecionaLancamentoParcela(this)" 
							ondblclick="editarLancamentoParcela(this)"
							data-conta="${lancamentoParcela.conta}"
							data-cartao-credito="${lancamentoParcela.cartaoCredito}"
							data-cheque="${lancamentoParcela.cheque eq null ? '' : lancamentoParcela.cheque.numero}"
							data-status="${lancamentoParcela.status}"
							data-category="lancamentoParcela_${lancamentoParcela.id}">
							<td class="text-right">${lancamentoParcela.seq + 1}</td>
							<td class="text-center"><joda:format pattern="${patternDate}" value="${lancamentoParcela.data}" /></td>
							<td class="text-center">
								
								<c:if test="${lancamentoParcela.conta ne null and lancamentoParcela.conta.id ne null }">
									<input type="hidden" id="isConfirmContaSaldoInsuficiente_${lancamentoParcela.conta.id}" name="isConfirmContaSaldoInsuficiente_${lancamentoParcela.conta.id}" value="${lancamentoParcela.id eq null  ? false : true}" />
								</c:if>
								<c:if test="${lancamentoParcela.cartaoCredito ne null and lancamentoParcela.cartaoCredito.id ne null }">
									<input type="hidden" id="isConfirmCartaoCreditoLimiteInsuficiente_${lancamentoParcela.cartaoCredito.id}" name="isConfirmCartaoCreditoLimiteInsuficiente_${lancamentoParcela.cartaoCredito.id}" value="${lancamentoParcela.id eq null  ? false : true}" />
								</c:if>
								
								<fmt:message key="MeioPagamento.${lancamentoParcela.meio}"/>
								
								<c:if test="${lancamentoParcela.conta ne null and lancamento.tipo eq 'DES'}">
									&nbsp;<c:out value="${lancamentoParcela.conta.nome}"/>
								</c:if>
								
								<c:if test="${lancamentoParcela.cartaoCredito ne null}">
									&nbsp;<c:out value="${lancamentoParcela.cartaoCredito.nome}"/>
								</c:if>
								
								<c:if test="${lancamentoParcela.cheque ne null}">
									<c:if test="${lancamentoParcela.cheque.talaoCheque ne null}">
										&nbsp;<c:out value="${lancamentoParcela.cheque.talaoCheque.conta.nome}" />
									</c:if>
									<c:if test="${lancamentoParcela.cheque.banco ne null}">
										&nbsp;<c:out value="${lancamentoParcela.cheque.banco.displayNome}" />
									</c:if>
									&nbsp;<c:out value="${lancamentoParcela.cheque.numero}"/>
								</c:if>
								<c:if test="${lancamentoParcela.boletoTipo ne null and lancamentoParcela.boletoNumero ne null}">
									<a class="btn btn-default btn-xs" href="javascript:;" onclick="msgAlert({message:'${f:escapeJS(f:outHtml(lancamentoParcela.boletoNumero))}', title: '<fmt:message key="BoletoTipo.${lancamentoParcela.boletoTipo}" />', stopPropagation: true}, event)" role="button" title="<fmt:message key="codigoBarras"/>">
										<i class="fa fa-barcode"></i>
									</a>
								</c:if>
								<c:if test="${lancamentoParcela.arquivo ne null and lancamentoParcela.arquivo.nome ne null}">
									<a title="${lancamentoParcela.arquivo.nome}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/download/${viewid}_lancamentoParcela_${lancamentoParcela.seq}<c:if test="${lancamentoParcela.arquivo.id ne null}">/${lancamentoParcela.arquivo.id}</c:if>/${lancamentoParcela.arquivo.nome}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
							</td>
							<td><c:out value="${lancamentoParcela.obs}" /></td>
							<td class="text-right">
								<fmt:formatNumber type="number" value="${lancamentoParcela.valor}" maxFractionDigits="2" minFractionDigits="2" />
								<c:if test="${lancamentoParcela.status eq 'BXD' or lancamentoParcela.status eq 'COM'}">
									&nbsp;(${lancamentoParcela.status})
								</c:if>
								<!-- Arquivo LancamentoParcela -->
								<c:if test="${lancamentoParcela.baixaItem.comprovante ne null}">
									<a title="${lancamentoParcela.baixaItem.comprovante.nome}" class="btn btn-default btn-xs" href="${pageContext.request.contextPath}/arquivo/${lancamentoParcela.baixaItem.comprovante.id}/download/${lancamentoParcela.baixaItem.comprovante.nome}">
										<i class="fa fa-paperclip"></i>
									</a>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr class="cm-tr-total">
						<td class="text-right" colspan="4"><fmt:message key="total"/> :</td>
						<td class="text-right"><fmt:formatNumber type="number" value="${lancamento.totalLancamentoParcela}" maxFractionDigits="2" minFractionDigits="2" /></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:if>
</div>