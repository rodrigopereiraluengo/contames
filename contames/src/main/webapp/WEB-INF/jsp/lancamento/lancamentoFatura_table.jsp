<fmt:message key="patternDate" var="patternDate" />
<c:if test="${count eq 0}">
	<div class="alert alert-success text-center cm-table-notfound" role="alert">
		<strong>
			<i class="fa fa-info-circle"></i>
			<fmt:message key="nenhumMovimentoEncontrado"/>
		</strong>
	</div>
</c:if>
<c:if test="${count > 0}">
	<div class="table-responsive">
			
		<c:set var="path" value="lancamento/lancamentoFatura" />
			
		<table class="table table-hover">
			<thead>
				<tr>
					<cmt:th orderByList="${orderByList}" index="0" name="data" column="_data" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="1" column="descricao" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="5" column="parcela" page="${page}" path="${path}" />
					<cmt:th orderByList="${orderByList}" index="7" name="valor" column="valor" page="${page}" path="${path}" />
				</tr>
			</thead>
			<tbody id="tbyLancamentoParcela">
				<c:forEach var="lancamentoParcela" items="${lancamentoParcelaList}">
					<tr data-lancamento-parcela-id="${lancamentoParcela['id']}" data-transferencia-id="${lancamentoParcela['transferencia_id']}">
						<td class="text-center<c:if test="${order eq '_data'}"> font-bold</c:if>"><fmt:formatDate pattern="${patternDate}" value="${lancamentoParcela['_data']}" /></td>
						<td <c:if test="${order eq 'descricao'}"> class="font-bold"</c:if>><c:out value="${lancamentoParcela['descricao']}"/></td>
						<td class="text-center<c:if test="${order eq 'parcela'}"> font-bold</c:if>"><c:out value="${lancamentoParcela['parcela']}"/></td>
						<td class="text-right<c:if test="${order eq 'valor'}"> font-bold</c:if>"><fmt:formatNumber type="number" value="${lancamentoParcela['valor']}" maxFractionDigits="2" minFractionDigits="2" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
				
	</div>
	<div class="panel-footer">
  		<cmt:summary pageSize="${pageSize}" paginationSize="${paginationSize}" firstResult="${firstResult}" count="${count}" page="${page}" />
		<cmt:pag orderByList="${orderByList}" pageSize="${pageSize}" paginationSize="${paginationSize}" path="${path}" page="${page}" />
	</div>
</c:if>