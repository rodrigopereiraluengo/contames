<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="robots" content="noindex">
    	<!--[if IE]><link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico"><![endif]-->
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon.png?v${applicationVersion}" sizes="16x16">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-24.png?v${applicationVersion}" sizes="24x24">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-32.png?v${applicationVersion}" sizes="32x32">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-48.png?v${applicationVersion}" sizes="48x48">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-64.png?v${applicationVersion}" sizes="64x64">
		<link rel="icon" href="${pageContext.request.contextPath}/resources/img/favicon-128.png?v${applicationVersion}" sizes="128x128">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-152.png?v${applicationVersion}" sizes="152x152">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-144.png?v${applicationVersion}" sizes="144x144">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-120.png?v${applicationVersion}" sizes="120x120">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-114.png?v${applicationVersion}" sizes="114x114">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-76.png?v${applicationVersion}" sizes="76x76">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-72.png?v${applicationVersion}" sizes="72x72">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/img/favicon-32.png?v${applicationVersion}" sizes="32x32">
		<meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon-144.png?v${applicationVersion}"/>
		<meta name="msapplication-TileColor" content="#f1f1f1"/>
		<title><fmt:message key="404paginaNaoEncontrada"/></title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
		<!--[if lt IE 9]>
      	<script src="${pageContext.request.contextPath}/resources/js/html5shiv.min.js?v${applicationVersion}"></script>
      	<script src="${pageContext.request.contextPath}/resources/js/respond.min.js?v${applicationVersion}"></script>
    	<![endif]-->
	</head>
	<body class="text-center" style="padding-top: 30px">
		<div class="panel panel-default">
		  	<div class="panel-body">
				<a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/resources/img/logo-email.png"/></a>
				<h1><fmt:message key="404paginaNaoEncontrada"/></h1>
			</div>
		</div>
	</body>
</html>