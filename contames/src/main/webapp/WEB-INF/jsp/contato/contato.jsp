<tiles:insertTemplate template="/WEB-INF/jsp/tpl/index.jsp">
	<tiles:putAttribute name="title"><fmt:message key="contato" /> | </tiles:putAttribute>
	<tiles:putAttribute name="breadcrumb">
	
		<div class="cm-page-title">
			<div class="container-fluid">
				<div class="row">
					
					<div class="title-env col-xs-12 col-sm-6 form-group">
						<h1 class="title"><fmt:message key="contato"/></h1>
					</div>
				
					<div class="cm-breadcrumb col-xs-12 col-sm-6 form-group">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-home"></i>
								<fmt:message key="home" />
							</li>
				  			<li><fmt:message key="contato" /></li>
				  		</ol>
					</div>
				
				</div>
			</div>
		</div>
	
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<div class="container-xmini">
			<div class="panel panel-default">
  				<div class="panel-heading"><h3><fmt:message key="contato" /></h3></div>
  				<div class="panel-body">
			    	<form id="frmContato" action="${pageContext.request.contextPath}/contato" method="post">
					
						<div class="form-group">
							<label for="nome" class="control-label"><fmt:message key="nome" /><span class="required">*</span></label>
							<input type="text" id="nome" name="nome" class="form-control" data-category="contato.nome" maxlength="64" />
						</div>
						
						<div class="form-group">
							<label for="email" class="control-label"><fmt:message key="email" /><span class="required">*</span></label>
							<input type="email" id="email" name="email" class="form-control" data-category="contato.email" maxlength="127" />
						</div>
						<div class="form-group">
							<label for="assunto" class="control-label"><fmt:message key="assunto" /><span class="required">*</span></label>
							<input type="text" id="subject" name="assunto" class="form-control" data-category="contato.assunto" maxlength="64" />
						</div>
						
						<div class="form-group">
							<label for="mensagem" class="control-label"><fmt:message key="mensagem" /><span class="required">*</span></label>
							<textarea id="mensagem" name="mensagem" class="form-control" data-category="contato.mensagem"></textarea>
						</div>
						
						<div class="form-group" id="reCaptcha">
							
						</div>
						<div class="form-group" id="reCaptchaInput">
						</div>
						
						<button type="submit" class="btn btn-primary btn-block" disabled="disabled"><fmt:message key="enviar" /></button>
					
					</form>
				</div>
			</div>
		</div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="script">
		<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js?v${applicationVersion}"></script>
		<script type="text/javascript">
			var publickey = '<env:get key="recapcha.publickey" />';
		</script>
		<script type="text/javascript" src="/resources/js/index.contato.js?v${applicationVersion}"></script>
	</tiles:putAttribute>
	
</tiles:insertTemplate>