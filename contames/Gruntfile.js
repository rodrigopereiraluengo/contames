module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      //options: {
      //  banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      //},
		/*options: {
			mangle: false,
			compress: false,
			beautify: true
		},*/
      target: {
          files: {
            'src/main/webapp/resources/js/components.js': 
            	[
                   'bower_components/jquery/dist/jquery.js',
                   'bower_components/moment/moment.js',
                   'bower_components/moment/locale/pt-br.js',
                   'bower_components/underscore/underscore.js',
                   /*'bower_components/bootstrap-calendar/js/language/pt-BR.js',
                   'bower_components/bootstrap-calendar/js/calendar.js',*/
                   'bower_components/jquery-ui/jquery-ui.js',
                   'bower_components/others/jquery.browser.js',
                   'bower_components/others/jquery.meiomask.js',
                   'bower_components/others/jquery.constrain.js',
                   'bower_components/others/jquery.extends.js',
                   'bower_components/others/jshashtable-3.0.js',
                   'bower_components/others/jquery.numberformatter-1.2.4.js',
                   'bower_components/others/jquery.ui.touch-punch.min.js',
                   'bower_components/datejs/build/date.js',
                   'bower_components/others/object.extends.js',
                   'bower_components/bootstrap/dist/js/bootstrap.js',
                   'bower_components/bootstrap-growl/jquery.bootstrap-growl.js',
                   'bower_components/others/bootstrap-checkbox.js',
                   'bower_components/bootstrap-rating-input/src/bootstrap-rating-input.js',
                   'bower_components/jquery-form/jquery.form.js',
                   'bower_components/jquery-validation/dist/jquery.validate.js',
                   'bower_components/jquery-validation/dist/additional-methods.js',
                   'bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
                   'bower_components/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js',
                   'bower_components/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
                   'bower_components/autoComplete/simpleAutoComplete.js',
                   'bower_components/async-waterfall/index.js',
                   'bower_components/others/boleto.js',
                   'bower_components/others/libs.js',
                   'bower_components/token-input/jquery.tokeninput.js',
                   'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
                   'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect-collapsible-groups.js',
                   'bower_components/jquery-visible/jquery.visible.js',
                   'bower_components/jquery.maskMoney/jquery.maskMoney.min.js',
                   'bower_components/jquery.hotkeys/jquery.hotkeys.js',
                   'bower_components/others/form.search.js',
                   'bower_components/jquery.calculator/jquery.plugin.js',
                   'bower_components/jquery.calculator/jquery.calculator.js',
                   'bower_components/jquery.calculator/jquery.calculator-pt-BR.js',
                   'bower_components/bootstrap-toggle/js/bootstrap-toggle.js'
                   
                   ],
           'src/main/webapp/resources/js/index.js': [
                 'bower_components/jquery/dist/jquery.js',
                 'bower_components/others/jquery.browser.js',
                 'bower_components/others/jquery.constrain.js',
                 'bower_components/others/jquery.extends.js',
                 'bower_components/others/jquery.meiomask.js',
                 'bower_components/bootstrap/dist/js/bootstrap.js',
                 'bower_components/bootstrap-growl/jquery.bootstrap-growl.js',
                 'bower_components/others/libs.js'
             ],
             'src/main/webapp/resources/js/admin.assinatura.js': 'bower_components/resources/admin.assinatura.js',
             'src/main/webapp/resources/js/admin.js': 'bower_components/resources/admin.js',
             'src/main/webapp/resources/js/admin.migrarConfirmar.js': 'bower_components/resources/admin.migrarConfirmar.js',
             'src/main/webapp/resources/js/admin.migrarOutroPlano.js': 'bower_components/resources/admin.migrarOutroPlano.js',
             'src/main/webapp/resources/js/admin.reativarConfirmar.js': 'bower_components/resources/admin.reativarConfirmar.js',
             'src/main/webapp/resources/js/admin.resumo.js': 'bower_components/resources/admin.resumo.js',
             'src/main/webapp/resources/js/agenda.cadastro.js': 'bower_components/resources/agenda.cadastro.js',
             'src/main/webapp/resources/js/arquivo.dialogAnexo.js': 'bower_components/resources/arquivo.dialogAnexo.js',
             'src/main/webapp/resources/js/assinatura.confirmar.js': 'bower_components/resources/assinatura.confirmar.js',
             'src/main/webapp/resources/js/assinatura.js': 'bower_components/resources/assinatura.js',
             'src/main/webapp/resources/js/baixa.baixaItem.js': 'bower_components/resources/baixa.baixaItem.js',
             'src/main/webapp/resources/js/baixa.cadastro.js': 'bower_components/resources/baixa.cadastro.js',
             'src/main/webapp/resources/js/baixa.consultar.js': 'bower_components/resources/baixa.consultar.js',
             'src/main/webapp/resources/js/baixa.lancamentoParcela.js': 'bower_components/resources/baixa.lancamentoParcela.js',
             'src/main/webapp/resources/js/cartaoCredito.cadastro.js': 'bower_components/resources/cartaoCredito.cadastro.js',
             'src/main/webapp/resources/js/cartaoCredito.consultar.js': 'bower_components/resources/cartaoCredito.consultar.js',
             'src/main/webapp/resources/js/cartaoCredito.dialogSelect.js': 'bower_components/resources/cartaoCredito.dialogSelect.js',
             'src/main/webapp/resources/js/cheque.dialogCancelar.js': 'bower_components/resources/cheque.dialogCancelar.js',
             'src/main/webapp/resources/js/cheque.dialogProps.js': 'bower_components/resources/cheque.dialogProps.js',
             'src/main/webapp/resources/js/compensacao.cadastro.js': 'bower_components/resources/compensacao.cadastro.js',
             'src/main/webapp/resources/js/compensacao.consultar.js': 'bower_components/resources/compensacao.consultar.js',
             'src/main/webapp/resources/js/compensacao.editarCompensacaoItem.js': 'bower_components/resources/compensacao.editarCompensacaoItem.js',
             'src/main/webapp/resources/js/compensacao.movs.js': 'bower_components/resources/compensacao.movs.js',
             'src/main/webapp/resources/js/conta.cadastro.js': 'bower_components/resources/conta.cadastro.js',
             'src/main/webapp/resources/js/conta.consultar.js': 'bower_components/resources/conta.consultar.js',
             'src/main/webapp/resources/js/conta.dialogSelect.js': 'bower_components/resources/conta.dialogSelect.js',
             'src/main/webapp/resources/js/conta.talaoCheque.js': 'bower_components/resources/conta.talaoCheque.js',
             'src/main/webapp/resources/js/descricao.cadastro.js': 'bower_components/resources/descricao.cadastro.js',
             'src/main/webapp/resources/js/descricao.consultar.js': 'bower_components/resources/descricao.consultar.js',
             'src/main/webapp/resources/js/index.contato.js': 'bower_components/resources/index.contato.js',
             'src/main/webapp/resources/js/index.js': 'bower_components/resources/index.js',
             'src/main/webapp/resources/js/item.cadastro.js': 'bower_components/resources/item.cadastro.js',
             'src/main/webapp/resources/js/item.consultar.js': 'bower_components/resources/item.consultar.js',
             'src/main/webapp/resources/js/lancamento.cadastro.js': 'bower_components/resources/lancamento.cadastro.js',
             'src/main/webapp/resources/js/lancamento.consultar.js': 'bower_components/resources/lancamento.consultar.js',
             'src/main/webapp/resources/js/lancamento.editarLancamentoItem.js': 'bower_components/resources/lancamento.editarLancamentoItem.js',
             'src/main/webapp/resources/js/lancamento.lancamentoFatura.js': 'bower_components/resources/lancamento.lancamentoFatura.js',
             'src/main/webapp/resources/js/lancamento.lancamentoParcela.js': 'bower_components/resources/lancamento.lancamentoParcela.js',
             'src/main/webapp/resources/js/lancamento.lancamentoParcelaBoleto.js': 'bower_components/resources/lancamento.lancamentoParcelaBoleto.js',
             'src/main/webapp/resources/js/lancamento.lancamentoRateio.js': 'bower_components/resources/lancamento.lancamentoRateio.js',
             'src/main/webapp/resources/js/login.atualizarSenha.js': 'bower_components/resources/login.atualizarSenha.js',
             'src/main/webapp/resources/js/login.esqueciSenha.js': 'bower_components/resources/login.esqueciSenha.js',
             'src/main/webapp/resources/js/login.js': 'bower_components/resources/login.js',
             'src/main/webapp/resources/js/pessoa.cadastro.js': 'bower_components/resources/pessoa.cadastro.js',
             'src/main/webapp/resources/js/pessoa.consultar.js': 'bower_components/resources/pessoa.consultar.js',
             'src/main/webapp/resources/js/relatorioClassificacao.movs.js': 'bower_components/resources/relatorioClassificacao.movs.js',
             'src/main/webapp/resources/js/relatorioClassificacao.relatorio.js': 'bower_components/resources/relatorioClassificacao.relatorio.js',
             'src/main/webapp/resources/js/relatorioData.relatorio.js': 'bower_components/resources/relatorioData.relatorio.js',
             'src/main/webapp/resources/js/relatorioFavorecido.movs.js': 'bower_components/resources/relatorioFavorecido.movs.js',
             'src/main/webapp/resources/js/relatorioFavorecido.relatorio.js': 'bower_components/resources/relatorioFavorecido.relatorio.js',
             'src/main/webapp/resources/js/relatorioItem.movs.js': 'bower_components/resources/relatorioItem.movs.js',
             'src/main/webapp/resources/js/relatorioItem.relatorio.js': 'bower_components/resources/relatorioItem.relatorio.js',
             'src/main/webapp/resources/js/relatorioRateio.movs.js': 'bower_components/resources/relatorioRateio.movs.js',
             'src/main/webapp/resources/js/relatorioRateio.relatorio.js': 'bower_components/resources/relatorioRateio.relatorio.js',
             'src/main/webapp/resources/js/talaoCheque.consultar.js': 'bower_components/resources/talaoCheque.consultar.js',
             'src/main/webapp/resources/js/talaoCheque.dialogSelect.js': 'bower_components/resources/talaoCheque.dialogSelect.js',
             'src/main/webapp/resources/js/transferencia.cadastro.js': 'bower_components/resources/transferencia.cadastro.js',
             'src/main/webapp/resources/js/transferencia.consultar.js': 'bower_components/resources/transferencia.consultar.js',
             'src/main/webapp/resources/js/usuario.atualizarSenha.js': 'bower_components/resources/usuario.atualizarSenha.js',
             'src/main/webapp/resources/js/Chart.min.js': 'bower_components/chartjs/Chart.js'
          }
      }
    },
    
    stylus: {
    	  compile: {
    		  files: [{
    		      'bower_components/stylus/style.css': ['bower_components/stylus/style.styl', 'bower_components/stylus/form.build.styl'] // compile and concat into single file
    		  }, {
    			  'bower_components/stylus/form.css': ['bower_components/stylus/form.styl']
    		  }]
    	  }
    },
    
    less: {
	  development: {
	    options: {
	      paths: ["assets/css"]
	    },
	    files: {
	      "bower_components/theme/default.css": 
	    	  [
	    	   "bower_components/theme/default.less",
	    	   "bower_components/autoComplete/simpleAutoComplete.less",
	    	   "bower_components/token-input/token-input.less",
	    	   "bower_components/jquery.calculator/jquery.calculator.less",
	    	   "bower_components/bootstrap-calendar/less/calendar.less"
	    	   ]
	    }
	  }/*,
	  production: {
	    options: {
	      paths: ["assets/css"],
	      plugins: [
	        new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
	        new (require('less-plugin-clean-css'))(cleanCssOptions)
	      ],
	      modifyVars: {
	        imgPath: '"http://mycdn.com/path/to/images"',
	        bgColor: 'red'
	      }
	    },
	    files: {
	      "path/to/result.css": "path/to/source.less"
	    }
	  }*/
	},
    
    cssmin: {
	  combine: {
		  options: {
			  keepSpecialComments: 0
		  },
		  files: [{
	      'src/main/webapp/resources/css/style.css': [
              'bower_components/bootstrap/dist/css/bootstrap.css',
              'bower_components/bootstrap/dist/css/bootstrap-theme.css',
              'bower_components/bootstrap-datepicker/css/bootstrap-datepicker3.css',
              'bower_components/font-awesome-4.4.0/css/font-awesome.css',
              'bower_components/stylus/bootstrap-checkbox.css',
              'bower_components/qtip/jquery.qtip.min.css',	
              'bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
              'bower_components/jstree-bootstrap-theme-master/dist/themes/proton/style.css',
              'bower_components/theme/default.css',
              'bower_components/bootstrap-toggle/css/bootstrap-toggle.css',
              'bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
          ]
	    }]
	  }
	},
	watch: {
        target: {
            files: [
                    'bower_components/bootstrap/dist/css/*', 
                    'bower_components/theme/*', 
                    'bower_components/autoComplete/*',
                    'bower_components/token-input/*',
                    'bower_components/bootstrap-multiselect/dist/*',
                    'bower_components/stylus/*',
                    'bower_components/others/*',
                    'bower_components/resources/*',
                    'bower_components/jstree-bootstrap-theme-master/dist/themes/proton/*',
                    'bower_components/jquery.calculator/*',
                    'bower_components/bootstrap-toggle/*',
                    'bower_components/chartjs/*',
                    'bower_components/moment/*',
                    'bower_components/bootstrap-calendar/less/*',
                    'Gruntfile.js'
                    ],
            tasks: ['uglify', 'less', /*'stylus', */'cssmin'],
            options: {
                /*livereload: true,
                spawn: false,
                interrupt: true,*/
                debounceDelay: 1100
                /*interval: 500,*/
            }
        }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');

  // Default task(s).
  grunt.registerTask('default', ['watch']);

};